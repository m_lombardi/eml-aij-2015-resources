# System Models

This folder contains the system models employed in [1].

In particular:

* The ANN0 model corresponds to the file "scc_ann1.txt"
* The ANN1 model corresponds to the file "scc_ann2.txt"
* The ACPI model corresponds to the file "scc_acpi0.txt"
* The wACPI model corresponds to the file "scc_acpi1.txt"
* The FEAT model corresponds to the file "scc_acpi4.txt"
* The DT model corresponds to the file "ptf_feat3.txt"

## Format of the Linear Model Files

The files containing the linear models (i.e. ACPI, wACPI, FEAT) have the following format:

```
{<weight of the avgcpi feature for core k>}

{<weight of the mincpi feature for core k>}

{<weight of the ngbcpi feature for core k>}

{<weight of the otrcpi feature for core k>}
```

For ease of understanding, the weight values are arranged in a 8$\times$6 matrix that mimics the shape of the SCC platform: however, a parser can safely ignore the arrangement and focus on the sequence of values.

See [the training readme file](../training/README.md) for information about the except formula of each feature.

## Format of the Artificial Neural Network Files

The files containing Artificial Neural Networks are "scc_ann1.txt" and "scc_ann2.txt"

Each line in the file describes the ANN used to predict the efficiency of a core. The first line is for core 0, the second line for core 1 and so on.

Each line is formatted as follows:

```
<n. inputs>,<n. hidden neurons>,<n. output neurons><hidden layer weights><output layer weights>
```

where:

```
<hidden layer weights> ::= {{,<weight of input i for hidden neuron j>},<bias of hidden neuron j>},
<output layer weights> ::= {{,<weight of hidden neurong i for output neuron j>},<bias of output neuron j>},
```

In other words, the lines start with the size of each network layer, then report the weights for the hidden and the output layer. Weights are given neuron-by-neuron, and the bias for each neuron is reported after its weights.

The components of the input of the ANN0 networks for core $k$ are, in order:

* Average CPI of jobs mapped on core $k$
* Minimum CPI of the jobs mapped on core $k$
* Average of the average CPI of the neighbor cores of $k$
* Average of the average CPI of all the other cores on the platform

The ANN1 networks use the same order, but they lack the minimum CPI input.

## Format for the Decision Tree files

The only file that contains Decision Trees is "ptf_feat3.txt".

The format is the one used by C4.5 (and J48 in Weka) to output Decision Trees, which is quite self-explaining.


## References

[1] M. Lombardi, A. Bartolini, M. Milano: "Empirical Decision Model Learning" -- submitted to Artificial Intelligence (2015)"