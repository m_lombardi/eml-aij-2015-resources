# Empirical Decision Model Learning -- Resources

## Empirical Model Learning

This repository contains resources to the paper titled "Empirical Decision Model Learning" [1], which provides an overview of a technique to enable Combinatorial Optimization and decision making over high-complexity systems.

The technique is called _Empirical Model Learning (EML)_ and is based on the (relatively simple) idea of:

1) Using a Machine Learning (ML) model to approximate the input/output behavior of a system that is hard to model by conventional means.
2) Embedding such _Empirical Model_ into a Combinatorial Optimization model. 

The emphasis of EML is mostly on the embedding techniques, which should be designed so that the optimization engine can exploit the structure of the empirical model to boost the search process.

More information, including a thorough presentation of the method, a number of embedding techniques, and application example on two workload dispatching problems can be found in [1].

So far, the EML approach has been applied using:

* Artificial Neural Networks, Decision Trees, and Random Forests on the Machine Learning side
* Local Search, Mixed (Non) Linear Integer Programming, Constraint Programming, and SAT Modulo Theories on the optimization side.

## Applications

The range of _potential_ applications of EML is quite vast and includes:

1) Applying Combinatorial Optimization to Complex Systems (in the proper sense), or systems that are too complicated to obtain an expert-design, hand-crafted model.
2) Enabling _prescriptive_ analytics by taking advantage of a pre-extracted _predictive_ analytics model.
3) Enable indirect interaction between a high-level optimizer and a lower-level optimizer (whose approximate behavior can be captured via Machine Learning).
4) Based on 3, it is possible to use EML to enable multi-level optimization and therefore optimization over large scale systems.
5) Self adapting systems, that could be obtained by retraining the Empirical Model

The _current_ applications of EML are much more limited, and are those presented in [1]. Basically, the method has been applied to _two thermal-aware workload dispatching problems_. Both problems are defined over a multi-core CPU by Intel (called SCC) featuring thermal controllers that abruptly reduce the operating frequency of each core if a threshold temperature is exceeding. In practice, our target system is an SCC simulator, developed by researchers at the University of Bologna with the aim to test temperature control policies.

Obtaining a hand-crafted model for this system is very difficult since:

1) The temperature of each core depends on a number of complex factors
2) The platform is subject to the action of low-level controllers (beside the thermal controller, each core is managed by an on-line, thermal-aware, scheduler).

EML allows to extract an approximate model from data and hence to define Combinatorial Optimization problems over this platform. In particular, we define:

1) The $WDP_{bal}$, where a number of jobs must be mapped on the cores so as to maximize the worst-case efficiency
2) The $WDP_{max}$, where the goal is to maximize the number of cores that operate above a given efficiency threshold.

As usual, more details on the problems can be found in [1].

## Content of this repository

This repository contains resources related to the extraction of the Empirical Models and the optimization problems discussed in [1]. In particular, we provide:

* The _training and test sets_ used for extracting the Empirical Models. The related data can be found in the "training" folder (see the [related description](training/README.md))
* _All our system models_, which include the Empirical Models (Artificial Neural Networks and Decision Trees) and the simplified model used for comparison. The related data can be found in the "empirical_models" folder (see [the related description](empirical_models/README.md))
* _The workloads_ (i.e. list of CPI values) used in our experimentation. The related data can be found in the "workloads" folder (see the [related description](workloads/README.md))
* _The results_ (the output of the optimizers) of all the approaches we have tested. The related data can be found in the "results" folder (see the [related description](results/README.md))

The platform simulator that we employ as target system is not easy to publish, for several reasons, including the fact that it is based on licensed software (Matlab).

The code of all our solution approaches will be made available through the [recomputation.org](http://www.recomputation.org). Some of them are based on licensed software, which will have to obtained separately.


## Publications related to Empirical Model Learning

[1] M. Lombardi, A. Bartolini, M. Milano: "Empirical Decision Model Learning" -- submitted to Artificial Intelligence (2015)"  

[2] Andrea Bartolini, Michele Lombardi, Michela Milano, Luca Benini: "Neuron Constraints to Model Complex Real-World Problems". CP 2011: 115-129  

[3] Andrea Bartolini, Michele Lombardi, Michela Milano, Luca Benini: "Optimization and Controlled Systems: A Case Study on Thermal Aware Workload Dispatching". AAAI 2012  

[4] Michele Lombardi, Stefano Gualandi: "A New Propagator for Two-Layer Neural Networks in Empirical Model Learning". CP 2013: 448-463  

[5] Alessio Bonfietti, Michele Lombardi, Michela Milano: "Embedding Decision Trees and Random Forests in Constraint Programming". CPAIOR 2015: 74-90  
