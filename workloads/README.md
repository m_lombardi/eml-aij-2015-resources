# Workload data

This folder contains a single file, with the 20 workloads (i.e. job sets) employed for the experimentation in [1].

Each line in the file reports the CPI values of a set of 288 jobs.



## References

[1] M. Lombardi, A. Bartolini, M. Milano: "Empirical Decision Model Learning" -- submitted to Artificial Intelligence (2015)"