# Training Data

This folder contains the training and validation sets used to obtain our Empirical Models, plus the results of the evaluation of our Artificial Neural Network models. For the details on the construction of the sets, we refer to [1].

We recall that we always learn a separate Machine Learning to predict the efficiency of each core.

## Input Features

The considered input features are:

1) The average CPI of the jobs on core $k$:  
   $$avgcpi_k = \frac{1}{m} \sum_{\text{job } j \text{ on } k} cpi_j$$
2) The minimum CPI of the jobs on core $k$:  
   $$mincpi_k = \min_{\text{job }j \text{ on } k} \left( cpi_j \right)$$
3) The average of the average CPI of the neighboring cores:  
   $$neighcpi_k = \frac{1}{|N(k)|} \sum_{\text{core } h \in N(k)} avgcpi_h$$
4) The average of the average CPI of all the other cores:  
   $$othercpi_k = \frac{1}{m-1 - |N(k)|} \sum_{\text{core } h \neq k, h \notin N(k)} avgcpi_h$$

where $N(k)$ is the set of cores having $H_\infty$ (i.e. max coordinate) distance equal to one from the target $k$.

## Training and Test Sets (Pre Feature Extraction)

The folder "ts_raw_text" contains the training and validation sets priori to the feature extraction and normalization steps.

Each training set file in the folders is named "ts*N*.txt", where *N* is the core number. Similarly, the test set files are named "vs*N".txt".

Each file is in the following format:

```
<number of tuples> <number of items per tuple>
{<CPI value>} <eff value>
```

Therefore, all lines except the first contain a sequence of CPI values (each referring to one job), and then the efficiency of the target core.

The job-to-core mapping is implicitly given by the sequence. In particular, if N is the total number of jobs, then the first N/48 jobs are mapped on the first core, the second N/48 on the second core and so on.


## Training and Test Sets (Post Feature Extraction)

The folder "ts_ann1" contains the training and test sets used to obtain the "full" Artificial Neural Network and the Decision Trees considered in [1]. *Note that the ANN0 network in [1] is in fact called ann1 in this repository*: the networks have been renamed for sake of simplicity in the paper, but their original name is maintained here since it is employed in some safety checks in the code.

The folder "ts_ann1" contains the training and test sets used for the "reduced" Artificial Neural Network from [1].

Each training set file in the folders is named "ts*N*.txt", where *N* is the core number. Similarly, the test set files are named "vs*N*".txt".

The file format is the same for all files. Namely, we have:

```
<number of tuples> <number of items per tuple>
{<D normalization factor>}
{<S normalization factor>}
{<tuple>}
```

Some notes:

* For the files in "ts_ann1", the first four items in each tuple correspond to the input features 1-4.
* For the files in "ts_ann2", the first three items in each tuple correspond to the input features 1, 3, 4
* The last element in each tuple is always the core efficiency
* The D and S factors are used for normalizing the corresponding value in each tuple, according to the formula: $(val - D) / S$. The files contain already normalized values (the normalization paramters are meant to be used for de-normlization).

## Evaluation Data

The folders "train_res_ann1" and "tra_res_ann2" contain the result of the evaluation of the ANN0 and ANN1 networks on the training and the test sets. Each result in the evalution is in the form:

```
[ts|vs]#<core number> <pred eff>,<real eff>,<gap>
```


## References

[1] M. Lombardi, A. Bartolini, M. Milano: "Empirical Decision Model Learning" -- submitted to Artificial Intelligence (2015)"