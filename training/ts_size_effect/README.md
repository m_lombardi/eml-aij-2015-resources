# Effect of changing the Training Set Size

This folder contains plot reporting the outcomes of a small experimentation for investigating the effect of changing the training set size. In detail:

* Figure "./pred_error_ann1.pdf" shows the average error for the ANN0 network (which is referred to as ANN1 in this repository). Each column refers to a different core.
* Figures "./pred_error_ann1_rXX.pdf" show the same kind of plot, after the size of each training set has been reduced by removing a portion (1 - XX) of the examples.
* Figures "./pred_error_ann1_pXX.pdf" show the same kind of plot, after the size of each training set has been increased by adding XX examples from the training sets of the other 47 cores on the platform