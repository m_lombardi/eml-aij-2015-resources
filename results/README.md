# Results

This folder contains the results of all the optimization aproaches from the experimentation in [1].

The solution quality is always _measured in terms of predicted cost_, which implies tha _only the approaches operating on the same system model can be directly compared_.

For this reason, the results are organized in folders according the considered system model. For a list of the system model, see the [corresponding readme file](../system_models/README.md).

Note that the "ptf_feat3" model is the only one for the $WDP_{max}$ problem. All the other resutls refer to the $WDP_{bal}$.

Each files in each folder contains the output logs of a solution approach for a certain instance (i.e. a job set, see the [workload readme file](../workloads/README.md)). Each file is named as follows:

```
<workload file name>-<job set index>[-<seed>]-<sol. approach>.txt
```

When the seed does not appear it is assumed to be equal to 1.

The solution approaches are denoted as follows:

- cp = Constraint Programming (based on Google or-tools)
- ls = Local Search (Localsolver)
- ilp = Mixed Integer Linear Programming (IBM ILOG Cplex)
- nlp = Non Linear Programming (BARON)
- z3 = SAT Modulo Theories (Z3)

All the log files start with a line that recall the main configuration parameters for the solution approach.

All the lof file report the final solution as a sequence of pairs:

```
<job idx>-><core idx>
```

## References

[1] M. Lombardi, A. Bartolini, M. Milano: "Empirical Decision Model Learning" -- submitted to Artificial Intelligence (2015)"