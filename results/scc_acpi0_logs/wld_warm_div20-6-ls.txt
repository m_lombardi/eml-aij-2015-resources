LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Weights for the platform:
 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1

Workload Information:
3.1185, 0.5, 0.607702, 3.19726, 2.97682, 1.59972, 1.2667, 0.5, 1.84807, 2.67677, 4.31718, 1.39128, 1.38609, 0.740994, 2.87739, 1.35274, 0.5, 5.14279, 2.03451, 1.58177, 1.99084, 2.24611, 2.47489, 1.67187, 2.29591, 1.95594, 2.15617, 2.23197, 2.12958, 1.23042, 1.90417, 0.952039, 2.77433, 3.27355, 2.59592, 0.5, 2.30129, 2.03152, 1.79238, 2.58658, 2.56517, 0.723053, 1.15921, 2.5037, 3.06571, 0.5, 4.01994, 0.751436, 2.07061, 2.85755, 2.15705, 2.13, 0.5, 2.28479, 0.5, 1.89212, 1.03681, 4.97361, 1.26166, 2.3358, 2.69817, 2.25878, 2.13461, 1.27949, 2.41042, 1.21854, 0.5, 2.91184, 1.75848, 0.602156, 2.03868, 4.18885, 3.17262, 0.836678, 1.13818, 2.52222, 3.8303, 0.5, 2.28779, 1.48897, 1.85023, 2.37186, 2.18497, 1.81617, 3.47157, 0.5, 1.6529, 3.37429, 1.13977, 1.86148, 0.659983, 3.33868, 3.06632, 0.5, 1.68691, 2.7481, 3.04099, 1.37412, 0.5, 1.30755, 2.75621, 3.02113, 2.33381, 1.36575, 1.99757, 2.25412, 2.02993, 2.01883, 23.3234, 11.3647, 20.8728, 35, 31.9616, 9.47742, 17.4741, 30.6184, 1.65074, 35, 12.6022, 34.6546, 22.2602, 32.7888, 24.5756, 28.6538, 20.0375, 3.68398, 1.50353, 1.90833, 2.17959, 2.2546, 2.10717, 2.04678, 1.21772, 0.5, 3.54066, 2.46042, 0.735172, 3.54603, 0.553124, 0.5, 2.27941, 1.90978, 3.50422, 3.25347, 22.0891, 18.8224, 24.2305, 25.6473, 27.3808, 13.83, 3.17025, 3.30664, 2.79629, 0.593973, 1.61461, 0.51823, 18.4125, 26.3241, 23.8043, 11.2281, 24.5957, 27.6354, 1.65389, 0.636179, 3.22803, 3.47817, 2.50373, 0.5, 2.31526, 2.39553, 2.21396, 0.5, 2.08559, 2.48966, 3.2208, 0.708539, 1.15484, 2.55069, 2.18064, 2.18449, 5.55644, 18.2864, 34.1855, 29.3457, 16.0332, 28.5927, 35, 16.9505, 11.0546, 10.5481, 25.3906, 33.0562, 14.4612, 22.5954, 35, 19.9472, 21.931, 18.0652, 2.45174, 1.45843, 2.87827, 3.2761, 0.5, 1.43546, 3.29898, 2.95099, 1.35285, 1.76898, 1.68086, 0.947335, 0.73712, 4.07143, 3.22793, 2.51851, 0.813986, 0.631026, 3.64731, 2.31815, 2.46721, 1.19628, 0.5, 1.87105, 3.39384, 2.72169, 0.935464, 0.5, 2.88208, 1.56692, 3.40934, 1.73601, 2.65717, 2.75577, 0.5, 0.941706, 2.62079, 0.5, 2.58771, 2.08297, 1.81875, 2.38979, 2.53707, 2.16432, 2.21632, 1.37434, 1.3439, 2.36405, 3.73156, 1.77974, 0.728544, 0.5, 2.90481, 2.35535, 0.5, 1.41785, 6.76133, 0.581924, 2.05896, 0.679942, 1.02313, 0.99413, 2.11886, 3.93538, 3.4285, 0.5, 2.09843, 1.83453, 2.19878, 2.24731, 1.88714, 1.7338, 2.19896, 0.845662, 3.11492, 0.865433, 2.44446, 2.53056, 1.83412, 3.42751, 0.87598, 2.87187, 1.11936, 1.87116, 3.03655, 0.5, 2.99289, 1.82857, 2.1193, 1.5227
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 28728, operands = 70032
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 0, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 2.90557


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 183034 itr]: obj = (-0.763712), mov = 183034, inf = 28.4%, acc = 20.2%, imp = 505

worst case avg CPI: 4.13503
[2 sec, 373892 itr]: obj = (-0.760795), mov = 373892, inf = 19.6%, acc = 20.3%, imp = 532

worst case avg CPI: 4.18609
[3 sec, 570810 itr]: obj = (-0.760795), mov = 570810, inf = 16.4%, acc = 20.3%, imp = 532

worst case avg CPI: 4.18609
[4 sec, 770040 itr]: obj = (-0.760795), mov = 770040, inf = 14.7%, acc = 20.3%, imp = 532

worst case avg CPI: 4.18609
[5 sec, 969818 itr]: obj = (-0.760795), mov = 969818, inf = 13.5%, acc = 20.7%, imp = 728

worst case avg CPI: 4.18609
[6 sec, 1176119 itr]: obj = (-0.760795), mov = 1176119, inf = 12.7%, acc = 20.8%, imp = 730

worst case avg CPI: 4.18609
[7 sec, 1384364 itr]: obj = (-0.760795), mov = 1384364, inf = 12.2%, acc = 20.9%, imp = 898

worst case avg CPI: 4.18609
[8 sec, 1591624 itr]: obj = (-0.760795), mov = 1591624, inf = 11.8%, acc = 20.9%, imp = 959

worst case avg CPI: 4.18609
[9 sec, 1800944 itr]: obj = (-0.760795), mov = 1800944, inf = 11.4%, acc = 20.9%, imp = 961

worst case avg CPI: 4.18609
[10 sec, 2008143 itr]: obj = (-0.760795), mov = 2008143, inf = 11.1%, acc = 20.9%, imp = 961

worst case avg CPI: 4.18609
[11 sec, 2216529 itr]: obj = (-0.760795), mov = 2216529, inf = 10.9%, acc = 20.9%, imp = 961

worst case avg CPI: 4.18609
[12 sec, 2427118 itr]: obj = (-0.760795), mov = 2427118, inf = 10.7%, acc = 20.9%, imp = 961

worst case avg CPI: 4.18609
[13 sec, 2639096 itr]: obj = (-0.760795), mov = 2639096, inf = 10.5%, acc = 21%, imp = 1145

worst case avg CPI: 4.18609
[14 sec, 2855734 itr]: obj = (-0.760795), mov = 2855734, inf = 10.4%, acc = 21%, imp = 1172

worst case avg CPI: 4.18609
[15 sec, 3071569 itr]: obj = (-0.760758), mov = 3071569, inf = 10.3%, acc = 21%, imp = 1178

worst case avg CPI: 4.18674
[16 sec, 3281539 itr]: obj = (-0.760758), mov = 3281539, inf = 10.1%, acc = 21%, imp = 1178

worst case avg CPI: 4.18674
[17 sec, 3494186 itr]: obj = (-0.760758), mov = 3494186, inf = 10%, acc = 21%, imp = 1178

worst case avg CPI: 4.18674
[18 sec, 3710479 itr]: obj = (-0.760758), mov = 3710479, inf = 9.9%, acc = 21%, imp = 1178

worst case avg CPI: 4.18674
[19 sec, 3926262 itr]: obj = (-0.760758), mov = 3926262, inf = 9.9%, acc = 21.1%, imp = 1356

worst case avg CPI: 4.18674
[20 sec, 4141087 itr]: obj = (-0.760758), mov = 4141087, inf = 9.8%, acc = 21.1%, imp = 1559

worst case avg CPI: 4.18674
[21 sec, 4359543 itr]: obj = (-0.760758), mov = 4359543, inf = 9.7%, acc = 21.1%, imp = 1566

worst case avg CPI: 4.18674
[22 sec, 4575460 itr]: obj = (-0.760758), mov = 4575460, inf = 9.6%, acc = 21.1%, imp = 1566

worst case avg CPI: 4.18674
[23 sec, 4796644 itr]: obj = (-0.760758), mov = 4796644, inf = 9.6%, acc = 21.2%, imp = 1757

worst case avg CPI: 4.18674
[24 sec, 5016762 itr]: obj = (-0.760758), mov = 5016762, inf = 9.5%, acc = 21.3%, imp = 1925

worst case avg CPI: 4.18674
[25 sec, 5233367 itr]: obj = (-0.760758), mov = 5233367, inf = 9.5%, acc = 21.4%, imp = 2120

worst case avg CPI: 4.18674
[26 sec, 5453994 itr]: obj = (-0.760758), mov = 5453994, inf = 9.4%, acc = 21.4%, imp = 2131

worst case avg CPI: 4.18674
[27 sec, 5663400 itr]: obj = (-0.760758), mov = 5663400, inf = 9.4%, acc = 21.5%, imp = 2169

worst case avg CPI: 4.18674
[28 sec, 5886586 itr]: obj = (-0.760706), mov = 5886586, inf = 9.3%, acc = 21.4%, imp = 2174

worst case avg CPI: 4.18765
[29 sec, 6111380 itr]: obj = (-0.760706), mov = 6111380, inf = 9.3%, acc = 21.4%, imp = 2174

worst case avg CPI: 4.18765
[30 sec, 6313508 itr]: obj = (-0.760706), mov = 6313508, inf = 9.2%, acc = 21.4%, imp = 2174

worst case avg CPI: 4.18765
[31 sec, 6513424 itr]: obj = (-0.760706), mov = 6513424, inf = 9.2%, acc = 21.4%, imp = 2174

worst case avg CPI: 4.18765
[32 sec, 6719160 itr]: obj = (-0.760706), mov = 6719160, inf = 9.2%, acc = 21.4%, imp = 2174

worst case avg CPI: 4.18765
[33 sec, 6935358 itr]: obj = (-0.760706), mov = 6935358, inf = 9.1%, acc = 21.4%, imp = 2174

worst case avg CPI: 4.18765
[34 sec, 7150362 itr]: obj = (-0.760706), mov = 7150362, inf = 9.1%, acc = 21.4%, imp = 2174

worst case avg CPI: 4.18765
[35 sec, 7360120 itr]: obj = (-0.760706), mov = 7360120, inf = 9.1%, acc = 21.3%, imp = 2174

worst case avg CPI: 4.18765
[36 sec, 7571725 itr]: obj = (-0.760706), mov = 7571725, inf = 9.1%, acc = 21.4%, imp = 2495

worst case avg CPI: 4.18765
[37 sec, 7787563 itr]: obj = (-0.760706), mov = 7787563, inf = 9%, acc = 21.4%, imp = 2719

worst case avg CPI: 4.18765
[38 sec, 7999920 itr]: obj = (-0.760706), mov = 7999920, inf = 9%, acc = 21.4%, imp = 2928

worst case avg CPI: 4.18765
[39 sec, 8215100 itr]: obj = (-0.760706), mov = 8215100, inf = 9%, acc = 21.4%, imp = 2928

worst case avg CPI: 4.18765
[40 sec, 8433095 itr]: obj = (-0.760706), mov = 8433095, inf = 9%, acc = 21.5%, imp = 3146

worst case avg CPI: 4.18765
[41 sec, 8654279 itr]: obj = (-0.760706), mov = 8654279, inf = 9%, acc = 21.4%, imp = 3150

worst case avg CPI: 4.18765
[42 sec, 8878397 itr]: obj = (-0.760706), mov = 8878397, inf = 8.9%, acc = 21.4%, imp = 3150

worst case avg CPI: 4.18765
[43 sec, 9096491 itr]: obj = (-0.760706), mov = 9096491, inf = 8.9%, acc = 21.4%, imp = 3150

worst case avg CPI: 4.18765
[44 sec, 9315068 itr]: obj = (-0.760706), mov = 9315068, inf = 8.9%, acc = 21.5%, imp = 3517

worst case avg CPI: 4.18765
[45 sec, 9537655 itr]: obj = (-0.760706), mov = 9537655, inf = 8.9%, acc = 21.5%, imp = 3579

worst case avg CPI: 4.18765
[46 sec, 9748570 itr]: obj = (-0.760706), mov = 9748570, inf = 8.9%, acc = 21.5%, imp = 3777

worst case avg CPI: 4.18765
[47 sec, 9963305 itr]: obj = (-0.760706), mov = 9963305, inf = 8.9%, acc = 21.5%, imp = 3786

worst case avg CPI: 4.18765
[48 sec, 10175001 itr]: obj = (-0.760706), mov = 10175001, inf = 8.8%, acc = 21.5%, imp = 3947

worst case avg CPI: 4.18765
[49 sec, 10384249 itr]: obj = (-0.760706), mov = 10384249, inf = 8.8%, acc = 21.5%, imp = 4155

worst case avg CPI: 4.18765
[50 sec, 10599450 itr]: obj = (-0.760706), mov = 10599450, inf = 8.8%, acc = 21.5%, imp = 4159

worst case avg CPI: 4.18765
[51 sec, 10818737 itr]: obj = (-0.760706), mov = 10818737, inf = 8.8%, acc = 21.5%, imp = 4159

worst case avg CPI: 4.18765
[52 sec, 11036212 itr]: obj = (-0.760706), mov = 11036212, inf = 8.8%, acc = 21.5%, imp = 4159

worst case avg CPI: 4.18765
[53 sec, 11255349 itr]: obj = (-0.760706), mov = 11255349, inf = 8.8%, acc = 21.5%, imp = 4347

worst case avg CPI: 4.18765
[54 sec, 11479023 itr]: obj = (-0.760706), mov = 11479023, inf = 8.8%, acc = 21.5%, imp = 4586

worst case avg CPI: 4.18765
[55 sec, 11707212 itr]: obj = (-0.760657), mov = 11707212, inf = 8.7%, acc = 21.5%, imp = 4591

worst case avg CPI: 4.1885
[56 sec, 11934024 itr]: obj = (-0.760657), mov = 11934024, inf = 8.7%, acc = 21.5%, imp = 4591

worst case avg CPI: 4.1885
[57 sec, 12153992 itr]: obj = (-0.760657), mov = 12153992, inf = 8.7%, acc = 21.5%, imp = 4591

worst case avg CPI: 4.1885
[58 sec, 12381164 itr]: obj = (-0.760657), mov = 12381164, inf = 8.7%, acc = 21.5%, imp = 4726

worst case avg CPI: 4.1885
[59 sec, 12606294 itr]: obj = (-0.760657), mov = 12606294, inf = 8.7%, acc = 21.5%, imp = 4785

worst case avg CPI: 4.1885
[60 sec, 12824658 itr]: obj = (-0.760657), mov = 12824658, inf = 8.7%, acc = 21.5%, imp = 4785

worst case avg CPI: 4.1885
[61 sec, 13049735 itr]: obj = (-0.760657), mov = 13049735, inf = 8.7%, acc = 21.5%, imp = 4977

worst case avg CPI: 4.1885
[62 sec, 13265711 itr]: obj = (-0.760657), mov = 13265711, inf = 8.7%, acc = 21.5%, imp = 5160

worst case avg CPI: 4.1885
[63 sec, 13481858 itr]: obj = (-0.760657), mov = 13481858, inf = 8.6%, acc = 21.5%, imp = 5383

worst case avg CPI: 4.1885
[64 sec, 13709142 itr]: obj = (-0.760657), mov = 13709142, inf = 8.6%, acc = 21.5%, imp = 5383

worst case avg CPI: 4.1885
[65 sec, 13928838 itr]: obj = (-0.760657), mov = 13928838, inf = 8.6%, acc = 21.5%, imp = 5584

worst case avg CPI: 4.1885
[66 sec, 14154896 itr]: obj = (-0.760657), mov = 14154896, inf = 8.6%, acc = 21.5%, imp = 5590

worst case avg CPI: 4.1885
[67 sec, 14374544 itr]: obj = (-0.760657), mov = 14374544, inf = 8.6%, acc = 21.5%, imp = 5590

worst case avg CPI: 4.1885
[68 sec, 14589231 itr]: obj = (-0.760657), mov = 14589231, inf = 8.6%, acc = 21.5%, imp = 5818

worst case avg CPI: 4.1885
[69 sec, 14815521 itr]: obj = (-0.760657), mov = 14815521, inf = 8.6%, acc = 21.5%, imp = 5819

worst case avg CPI: 4.1885
[70 sec, 15040009 itr]: obj = (-0.760657), mov = 15040009, inf = 8.6%, acc = 21.5%, imp = 6126

worst case avg CPI: 4.1885
[71 sec, 15265039 itr]: obj = (-0.760657), mov = 15265039, inf = 8.6%, acc = 21.5%, imp = 6159

worst case avg CPI: 4.1885
[72 sec, 15485946 itr]: obj = (-0.760657), mov = 15485946, inf = 8.6%, acc = 21.5%, imp = 6317

worst case avg CPI: 4.1885
[73 sec, 15713889 itr]: obj = (-0.760657), mov = 15713889, inf = 8.5%, acc = 21.5%, imp = 6320

worst case avg CPI: 4.1885
[74 sec, 15903819 itr]: obj = (-0.760657), mov = 15903819, inf = 8.5%, acc = 21.5%, imp = 6339

worst case avg CPI: 4.1885
[75 sec, 16106125 itr]: obj = (-0.760657), mov = 16106125, inf = 8.5%, acc = 21.5%, imp = 6341

worst case avg CPI: 4.1885
[76 sec, 16318529 itr]: obj = (-0.760657), mov = 16318529, inf = 8.5%, acc = 21.5%, imp = 6341

worst case avg CPI: 4.1885
[77 sec, 16542452 itr]: obj = (-0.760657), mov = 16542452, inf = 8.5%, acc = 21.5%, imp = 6341

worst case avg CPI: 4.1885
[78 sec, 16755267 itr]: obj = (-0.760657), mov = 16755267, inf = 8.5%, acc = 21.5%, imp = 6341

worst case avg CPI: 4.1885
[79 sec, 16961480 itr]: obj = (-0.760657), mov = 16961480, inf = 8.5%, acc = 21.5%, imp = 6341

worst case avg CPI: 4.1885
[80 sec, 17122295 itr]: obj = (-0.760657), mov = 17122295, inf = 8.5%, acc = 21.5%, imp = 6341

worst case avg CPI: 4.1885
[81 sec, 17296996 itr]: obj = (-0.760657), mov = 17296996, inf = 8.5%, acc = 21.5%, imp = 6341

worst case avg CPI: 4.1885
[82 sec, 17462552 itr]: obj = (-0.760657), mov = 17462552, inf = 8.5%, acc = 21.5%, imp = 6522

worst case avg CPI: 4.1885
[83 sec, 17607043 itr]: obj = (-0.760657), mov = 17607043, inf = 8.5%, acc = 21.5%, imp = 6547

worst case avg CPI: 4.1885
[84 sec, 17804881 itr]: obj = (-0.760657), mov = 17804881, inf = 8.5%, acc = 21.5%, imp = 6550

worst case avg CPI: 4.1885
[85 sec, 18019514 itr]: obj = (-0.760657), mov = 18019514, inf = 8.5%, acc = 21.5%, imp = 6550

worst case avg CPI: 4.1885
[86 sec, 18240915 itr]: obj = (-0.760657), mov = 18240915, inf = 8.5%, acc = 21.5%, imp = 6550

worst case avg CPI: 4.1885
[87 sec, 18466716 itr]: obj = (-0.760657), mov = 18466716, inf = 8.5%, acc = 21.5%, imp = 6773

worst case avg CPI: 4.1885
[88 sec, 18687011 itr]: obj = (-0.760657), mov = 18687011, inf = 8.4%, acc = 21.5%, imp = 6957

worst case avg CPI: 4.1885
[89 sec, 18900870 itr]: obj = (-0.760657), mov = 18900870, inf = 8.4%, acc = 21.5%, imp = 6969

worst case avg CPI: 4.1885
[90 sec, 19111638 itr]: obj = (-0.760657), mov = 19111638, inf = 8.4%, acc = 21.5%, imp = 6969

worst case avg CPI: 4.1885
[90 sec, 19111638 itr]: obj = (-0.760657), mov = 19111638, inf = 8.4%, acc = 21.5%, imp = 6969

worst case avg CPI: 4.1885
19111638 iterations, 19111638 moves performed in 90 seconds
Feasible solution: obj = (-0.760657)

Run output...
FINAL SOLUTION
Worst case avg CPI: 4.1885
Mapping: 0->29, 1->30, 2->31, 3->47, 4->24, 5->26, 6->38, 7->16, 8->41, 9->20, 10->28, 11->9, 12->14, 13->25, 14->36, 15->39, 16->17, 17->28, 18->12, 19->2, 20->29, 21->29, 22->2, 23->25, 24->11, 25->13, 26->11, 27->31, 28->6, 29->8, 30->30, 31->24, 32->35, 33->3, 34->17, 35->19, 36->35, 37->44, 38->37, 39->7, 40->42, 41->7, 42->7, 43->7, 44->36, 45->34, 46->5, 47->16, 48->0, 49->34, 50->7, 51->30, 52->45, 53->21, 54->17, 55->27, 56->32, 57->10, 58->21, 59->22, 60->36, 61->42, 62->4, 63->8, 64->42, 65->40, 66->44, 67->15, 68->19, 69->6, 70->33, 71->28, 72->36, 73->31, 74->24, 75->18, 76->10, 77->32, 78->4, 79->2, 80->44, 81->32, 82->1, 83->11, 84->5, 85->9, 86->19, 87->20, 88->16, 89->1, 90->41, 91->46, 92->35, 93->38, 94->13, 95->46, 96->15, 97->21, 98->21, 99->17, 100->47, 101->22, 102->35, 103->27, 104->40, 105->29, 106->8, 107->33, 108->27, 109->20, 110->14, 111->2, 112->33, 113->15, 114->44, 115->43, 116->25, 117->29, 118->4, 119->13, 120->12, 121->21, 122->9, 123->32, 124->38, 125->10, 126->33, 127->30, 128->9, 129->19, 130->24, 131->40, 132->26, 133->8, 134->10, 135->42, 136->6, 137->10, 138->26, 139->21, 140->37, 141->13, 142->5, 143->46, 144->42, 145->19, 146->11, 147->1, 148->6, 149->3, 150->47, 151->22, 152->4, 153->6, 154->23, 155->24, 156->30, 157->37, 158->18, 159->22, 160->24, 161->39, 162->12, 163->37, 164->20, 165->28, 166->46, 167->43, 168->38, 169->29, 170->43, 171->43, 172->12, 173->2, 174->15, 175->26, 176->11, 177->34, 178->9, 179->34, 180->10, 181->35, 182->23, 183->34, 184->0, 185->40, 186->45, 187->7, 188->46, 189->36, 190->26, 191->25, 192->47, 193->31, 194->17, 195->8, 196->16, 197->41, 198->22, 199->27, 200->14, 201->15, 202->40, 203->43, 204->41, 205->20, 206->23, 207->20, 208->17, 209->1, 210->18, 211->28, 212->4, 213->4, 214->14, 215->12, 216->5, 217->11, 218->39, 219->27, 220->6, 221->27, 222->3, 223->12, 224->0, 225->23, 226->0, 227->30, 228->33, 229->23, 230->31, 231->39, 232->26, 233->45, 234->39, 235->1, 236->16, 237->38, 238->33, 239->46, 240->45, 241->47, 242->42, 243->34, 244->32, 245->25, 246->5, 247->45, 248->2, 249->14, 250->44, 251->37, 252->13, 253->0, 254->5, 255->31, 256->41, 257->23, 258->9, 259->47, 260->0, 261->28, 262->15, 263->40, 264->43, 265->3, 266->18, 267->41, 268->35, 269->1, 270->25, 271->3, 272->38, 273->14, 274->19, 275->16, 276->18, 277->3, 278->13, 279->36, 280->45, 281->37, 282->22, 283->18, 284->8, 285->44, 286->39, 287->32
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	4.24301	5.47914	7.29397	4.43418	4.26123	4.18932
	5.32361	4.34669	4.66333	5.30837	4.1885	5.66196
	5.23116	6.93053	4.55277	4.22594	4.90674	6.93072
	5.2661	4.57213	4.22729	6.45156	4.22999	6.67815
	5.38135	6.94713	4.99495	5.18451	4.18897	7.83418
	4.40365	4.91847	5.90485	7.12512	6.4688	5.10821
	4.20565	5.87642	4.88623	6.49187	5.80927	4.69641
	5.66669	6.22771	4.43153	6.97964	4.21473	4.45723
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 6 --global-tlim 90 --seed 0 --use-acpi-model

Starting the Optimization Process
