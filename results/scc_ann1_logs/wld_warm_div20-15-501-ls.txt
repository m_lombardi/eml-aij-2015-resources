LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
ANNs For the Platform:
4,2,1,6.45294,0.0174524,1.58234,2.29305,2.12057,3.95113,12.7206,-19.3278,5.58789,9.38298,0.00306445,0.00353919,0.795949
4,2,1,3.10684,7.56359,0.502116,0.637049,12.0356,-2.9013,-3.22211,-0.368567,-0.562298,-7.37831,2.38595,1.22203,-0.360292
4,2,1,4.88387,44.8399,1.27345,1.24496,49.1775,-8.83277,-3.12983,-1.00946,11.7,-3.01002,0.132635,0.00372271,0.673996
4,2,1,4.73045,19.3982,0.449241,0.753865,25.7092,17.3605,0.0558085,-8.91516,0.49993,10.3396,4.06312,-0.0136297,-3.24599
4,2,1,-7.16849,-10.0957,-0.518885,-0.00359459,5.93351,5.62684,52.0516,1.48603,1.30274,57.2311,0.250578,0.137157,0.417379
4,2,1,0.412706,-2.89716,0.137754,-2.57826,11.0947,121.374,858.444,-136.26,-14917.8,-1308.92,-3.36558,-0.000403784,4.17997
4,2,1,-8.8269,-0.482244,-0.0820712,-0.0562713,-10.1617,9.78118,-7.01882,-0.428151,-0.0606718,2.56012,-8.54699,-0.509991,-7.23704
4,2,1,2.19881,4.94147,0.541452,0.107587,6.45213,1.0452,0.405805,0.555763,3.64728,1.95727,0.425683,-0.00188273,0.383643
4,2,1,-6.13316,1.92598,-0.0562274,-0.0731719,-4.60606,0.509071,7.79918,0.393802,0.0114563,7.9345,-1.13416,0.310582,-0.637365
4,2,1,1.73462,2.97854,0.276167,0.0859465,3.6912,3.09203,3.25403,0.324282,0.112495,4.06123,0.954718,-0.48836,0.344765
4,2,1,1.57339,3.84603,-3.4467,0.7429,2.75709,2.1802,5.09535,0.482172,0.106131,6.49189,-0.0251827,0.433535,0.393482
4,2,1,4.13514,21.6229,1.1931,0.354612,24.1372,25.5916,0.432728,-12.6725,-0.45345,15.3428,0.194819,-0.0364695,0.647465
4,2,1,-0.728632,5.60079,1.78098,1.8429,-5.74354,-3.26566,-13.0026,-0.907163,-0.31822,-15.3707,-0.427666,-0.224298,0.154329
4,2,1,3.81854,-4.98219,-0.293924,-0.103604,-6.7034,2.28654,4.72568,0.469524,0.115005,6.05901,-0.511241,0.421809,-0.121549
4,2,1,-1.23773,5.98591,0.224555,-0.359337,6.05767,2.3852,3.4706,0.220977,0.179377,4.88772,0.199751,0.432231,0.177791
4,2,1,1.12189,4.09326,0.850801,0.171671,-5.17646,-1.9656,-4.07995,-0.210811,-0.113853,-5.17242,-2.70452,-0.46094,-2.36326
4,2,1,2.08858,4.80174,0.367584,0.109336,6.02292,4.32969,4.35157,0.772172,10.015,-0.96022,0.43933,4.17055e-05,0.365228
4,2,1,1.18806,3.00188,0.124753,0.00191203,3.75084,1.03049,2.7808,0.137818,0.0250058,4.01619,-1.05018,2.49176,-0.635083
4,2,1,3.11788,10.9247,0.699295,0.170953,13.0959,-11.4646,-5.88946,11.0137,8.74787,-11.701,0.236706,0.0216102,0.59131
4,2,1,7.53568,1.94998,-2.27776,-1.91765,0.451708,2.13206,4.27547,0.333162,0.0864645,5.45063,-0.0153486,0.447774,0.366202
4,2,1,-2.12436,-3.29848,-0.0965154,-0.0454545,-3.86293,4.27042,3.64797,0.106703,0.057602,4.81479,-1.28015,-0.815344,0.346524
4,2,1,2.06313,3.99765,-9.94001e-06,0.200591,5.05136,-0.024567,-3.22765,-1.84842,1.73493,-3.87425,0.446216,-0.0350173,0.320411
4,2,1,3.3952,3.19239,0.219932,0.0459907,4.0724,1.85184,2.9352,0.236295,0.0535068,3.62882,-0.516849,0.990723,0.337742
4,2,1,-6.32145,2.68844,0.160066,0.0633176,-3.81442,9.03888,1.31396,0.253255,-0.0684612,6.93971,1.83579,0.440423,2.20116
4,2,1,-4.54148,-0.217309,1.86369,-0.409034,0.0885675,-3.10698,-12.3715,-0.753639,-0.275832,-14.544,0.00344558,-0.233405,0.57337
4,2,1,-0.743367,6.86662,-0.19496,-0.0184317,7.54898,2.69258,3.24789,0.437511,0.121848,4.97423,0.830361,0.39615,-0.417087
4,2,1,2.07654,3.2533,0.103762,0.0837251,4.40922,1.95615,-2.57949,-0.10147,-0.0485499,-3.72005,0.479113,-0.433879,-0.0966803
4,2,1,-0.480501,-0.00331725,1.67452,0.764966,-1.53387,1.99113,3.67475,0.0228188,0.0287235,4.64739,0.15721,0.465135,0.487915
4,2,1,1.11584,-5.79333,-0.212675,0.376981,-6.64888,2.45003,3.54368,0.313683,0.144659,5.05472,-0.777178,0.423599,-0.389995
4,2,1,2.39093,1.70258,-0.183288,-0.314006,4.06496,2.34881,2.91375,-0.0230153,-0.18977,5.26307,-1.33645,1.84807,0.292237
4,2,1,0.88022,3.23795,0.179315,0.0205209,3.57684,0.764441,2.74982,0.156077,0.0273373,3.62313,-0.980719,2.16912,-0.37872
4,2,1,-1.27932,7.09751,-0.0490235,0.455487,7.14709,-2.57722,-4.06233,-0.410784,-0.0433739,-5.6171,0.164894,-0.408714,0.23583
4,2,1,0.623158,5.01876,0.194867,0.0365944,4.99458,-16.0815,2.94826,-1.82463,-1.90388,-9.56395,0.291148,-0.155001,0.360694
4,2,1,1.86251,3.92056,0.158781,0.0575693,4.93436,0.681285,4.09563,1.09826,-3.65606,2.09524,0.482473,-0.00448292,0.322397
4,2,1,3.33939,3.52877,0.262622,0.0607689,4.45842,2.23218,3.37107,0.256541,0.0516206,4.05014,-1.2701,1.7162,0.365737
4,2,1,5.0585,4.68315,2.53887,0.128578,8.9709,3.62833,11.0789,1.32555,0.190392,13.8026,-0.126366,0.350541,0.580103
4,2,1,4.99986,2.75557,1.33036,-0.0119951,6.91685,4.23025,5.34568,1.08928,0.0379928,8.80363,-0.700785,0.958812,0.547325
4,2,1,1.95024,3.24732,0.229322,0.0270089,3.53624,1.62575,3.04977,0.230194,0.0319079,3.46966,-1.34015,1.81817,0.333213
4,2,1,2.11055,5.16581,0.225183,0.0767342,6.2013,3.43647,-3.10756,1.22453,-6.27082,5.92438,0.428242,2.12665,-1.75474
4,2,1,10.028,2.55152,-0.0579427,-0.0186516,-2.98054,2.14055,4.33639,0.203596,0.057269,5.47668,0.0078111,0.436528,0.371941
4,2,1,1.80421,3.45533,0.27065,0.0337725,4.14172,3.00203,3.65885,0.26995,0.0620062,4.37924,0.892686,-0.440136,0.355377
4,2,1,-2.76424,-10.7675,-0.741451,-0.141046,-12.927,13.6995,2.51611,0.90934,27.76,15.1973,-0.260938,-0.00175312,0.546274
4,2,1,2.26659,3.88332,0.083697,0.287373,-5.89957,3.07548,7.5942,0.285535,0.141703,9.90558,1.94369,0.273352,2.47758
4,2,1,5.41472,-1.35343,-0.0808605,2.27411,0.928252,2.55844,6.15687,0.338903,0.0735359,7.47381,-0.0148119,0.388691,0.423615
4,2,1,4.53285,3.66527,0.130055,0.0722232,4.95325,-2.1632,-3.34312,-0.11633,-0.0577237,-3.98867,-0.759785,-1.19124,0.379449
4,2,1,2.36669,5.75119,0.132783,0.0812059,6.8845,-8.68015,-0.488627,0.811521,2.16444,0.277436,0.40839,0.00790296,0.400784
4,2,1,0.583762,8.58805,0.307497,0.0165495,8.61973,-7.32584,1.34397,-0.0108636,-0.0260377,-6.96836,0.266022,-5.2323,-4.69173
4,2,1,1.76683,-0.336959,-0.116305,-0.0696435,-1.62311,2.51027,5.65249,0.180271,0.0657051,7.97995,-0.0500539,0.457869,0.308072
Workload Information:
0.5, 0.793797, 3.01448, 2.89409, 2.97166, 1.82598, 1.46422, 1.02987, 4.33222, 3.15438, 0.5, 1.51931, 2.09408, 1.95425, 1.74965, 2.13165, 2.02029, 2.05007, 2.40715, 0.751014, 2.93856, 1.5686, 1.81978, 2.51489, 0.810358, 3.37159, 2.01811, 0.5, 3.8527, 1.44724, 1.43318, 2.33024, 1.63724, 1.9352, 2.17271, 2.49144, 2.62915, 2.43595, 1.70695, 1.6611, 2.1134, 1.45345, 2.13604, 2.94479, 1.10006, 2.21292, 2.90612, 0.700066, 2.24627, 2.07499, 0.712951, 3.80092, 1.44741, 1.71746, 1.98396, 2.25489, 2.0251, 1.72142, 1.96386, 2.05077, 0.724777, 5.76477, 1.8992, 2.38764, 0.513167, 0.710449, 31.2107, 27.2561, 18.7677, 35, 11.0826, 8.68286, 1.51957, 3.17254, 0.5, 1.02075, 1.81243, 3.9747, 1.72133, 0.667203, 2.55119, 2.14292, 2.30777, 2.60958, 0.631906, 2.19898, 4.19696, 1.52562, 0.851563, 2.59497, 3.36286, 3.496, 0.975857, 1.57842, 2.08687, 0.5, 28.3048, 15.5874, 35, 25.0792, 12.9663, 15.0623, 1.94871, 2.30428, 2.28502, 1.8023, 1.39475, 2.26493, 3.11261, 1.48502, 2.7242, 1.50319, 2.67498, 0.5, 0.94498, 1.14016, 2.84732, 3.11272, 2.697, 1.25782, 1.49547, 2.60328, 2.37214, 1.91276, 1.0462, 2.57016, 0.556693, 2.96248, 3.93222, 0.5, 1.54412, 2.50448, 20.8662, 35, 16.4223, 16.63, 24.1447, 18.937, 35, 18.8404, 13.9087, 21.4826, 32.0696, 10.6987, 2.03447, 3.84498, 3.47505, 0.840083, 1.07748, 0.727933, 3.5496, 3.73055, 1.03076, 0.5, 2.33398, 0.855113, 1.93585, 0.915647, 0.553338, 1.61452, 1.35227, 5.62837, 3.13578, 3.41479, 0.603717, 1.40872, 0.5, 2.93699, 1.64791, 2.04324, 0.971774, 0.682902, 1.77925, 4.87493, 0.679022, 1.01475, 0.78205, 2.11252, 0.933492, 6.47816, 1.34254, 1.35093, 0.5, 3.04181, 3.12938, 2.63533, 0.5, 1.80789, 4.25635, 2.83402, 0.859458, 1.74228, 2.29634, 1.85749, 1.87562, 2.42096, 1.42836, 2.12123, 3.7915, 2.21285, 0.575129, 0.780089, 4.14043, 0.5, 0.5, 0.855756, 1.6246, 2.73087, 3.7941, 2.49466, 3.98145, 0.5, 1.82333, 1.23156, 2.84167, 1.62198, 4.5138, 2.24199, 1.19583, 2.07482, 0.739494, 1.23407, 1.85496, 0.557577, 2.51568, 1.41475, 4.12757, 1.52947, 1.92157, 2.25752, 1.90951, 2.10541, 1.70878, 2.09721, 0.957288, 0.997952, 2.95043, 3.22083, 3.20122, 0.672285, 3.6616, 3.14231, 2.4321, 1.26004, 0.5, 1.00395, 0.996237, 3.61131, 0.889852, 2.02482, 2.91463, 1.56314, 0.5, 2.30284, 5.09568, 1.76483, 0.697285, 1.63936, 2.35738, 1.92499, 1.23851, 2.49156, 2.46238, 1.52517, 2.20721, 2.20715, 1.75975, 2.13094, 1.83949, 1.85546, 2.53771, 1.97268, 2.60817, 0.5, 2.55362, 1.82783, 0.623567, 3.18571, 0.742433, 2.03774, 0.739147, 4.6714, 1.30779, 2.98554, 3.00575, 2.50208, 1.69884, 0.5
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 44729, operands = 116736
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 501, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.664125


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case efficiency: 0.998464
[1 sec, 4274 itr]: obj = (0.0137467), mov = 4274, inf = 74.9%, acc = 22.6%, imp = 133

worst case efficiency: 0.608248
[2 sec, 24041 itr]: obj = (0.0591676), mov = 24041, inf = 95.4%, acc = 4.2%, imp = 171

worst case efficiency: 0.635501
[3 sec, 37969 itr]: obj = (0.0849967), mov = 37969, inf = 97%, acc = 2.8%, imp = 212

worst case efficiency: 0.650998
[4 sec, 45463 itr]: obj = (0.0908446), mov = 45463, inf = 97.4%, acc = 2.5%, imp = 255

worst case efficiency: 0.654507
[5 sec, 51021 itr]: obj = (0.104664), mov = 51021, inf = 97.6%, acc = 2.3%, imp = 296

worst case efficiency: 0.662799
[6 sec, 56700 itr]: obj = (0.124668), mov = 56700, inf = 97.7%, acc = 2.2%, imp = 345

worst case efficiency: 0.674801
[7 sec, 61653 itr]: obj = (0.132884), mov = 61653, inf = 97.8%, acc = 2.1%, imp = 391

worst case efficiency: 0.67973
[8 sec, 66447 itr]: obj = (0.145938), mov = 66447, inf = 97.9%, acc = 2%, imp = 427

worst case efficiency: 0.687563
[9 sec, 71130 itr]: obj = (0.168993), mov = 71130, inf = 98%, acc = 1.9%, imp = 480

worst case efficiency: 0.701396
[10 sec, 75751 itr]: obj = (0.180591), mov = 75751, inf = 98%, acc = 1.9%, imp = 532

worst case efficiency: 0.708355
[11 sec, 80297 itr]: obj = (0.185833), mov = 80297, inf = 98.1%, acc = 1.8%, imp = 573

worst case efficiency: 0.7115
[12 sec, 84799 itr]: obj = (0.194864), mov = 84799, inf = 98.1%, acc = 1.8%, imp = 615

worst case efficiency: 0.716919
[13 sec, 89563 itr]: obj = (0.199559), mov = 89563, inf = 98.2%, acc = 1.7%, imp = 647

worst case efficiency: 0.719735
[14 sec, 93918 itr]: obj = (0.200775), mov = 93918, inf = 98.3%, acc = 1.7%, imp = 670

worst case efficiency: 0.720465
[15 sec, 98137 itr]: obj = (0.202593), mov = 98137, inf = 98.3%, acc = 1.6%, imp = 692

worst case efficiency: 0.721556
[16 sec, 102698 itr]: obj = (0.204927), mov = 102698, inf = 98.4%, acc = 1.6%, imp = 714

worst case efficiency: 0.722956
[17 sec, 107157 itr]: obj = (0.206198), mov = 107157, inf = 98.4%, acc = 1.6%, imp = 738

worst case efficiency: 0.723719
[18 sec, 111065 itr]: obj = (0.209404), mov = 111065, inf = 98.4%, acc = 1.5%, imp = 766

worst case efficiency: 0.725642
[19 sec, 115398 itr]: obj = (0.21654), mov = 115398, inf = 98.5%, acc = 1.5%, imp = 798

worst case efficiency: 0.729924
[20 sec, 119844 itr]: obj = (0.218038), mov = 119844, inf = 98.5%, acc = 1.5%, imp = 823

worst case efficiency: 0.730823
[21 sec, 123866 itr]: obj = (0.219768), mov = 123866, inf = 98.5%, acc = 1.4%, imp = 843

worst case efficiency: 0.731861
[22 sec, 128146 itr]: obj = (0.220054), mov = 128146, inf = 98.6%, acc = 1.4%, imp = 848

worst case efficiency: 0.732033
[23 sec, 131748 itr]: obj = (0.220054), mov = 131748, inf = 98.6%, acc = 1.4%, imp = 848

worst case efficiency: 0.732033
[24 sec, 136567 itr]: obj = (0.220054), mov = 136567, inf = 98.7%, acc = 1.3%, imp = 848

worst case efficiency: 0.732033
[25 sec, 141258 itr]: obj = (0.220054), mov = 141258, inf = 98.7%, acc = 1.3%, imp = 848

worst case efficiency: 0.732033
[26 sec, 146152 itr]: obj = (0.220054), mov = 146152, inf = 98.8%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[27 sec, 151719 itr]: obj = (0.220054), mov = 151719, inf = 98.8%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[28 sec, 157584 itr]: obj = (0.220054), mov = 157584, inf = 98.8%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[29 sec, 163689 itr]: obj = (0.220054), mov = 163689, inf = 98.9%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[30 sec, 170880 itr]: obj = (0.220054), mov = 170880, inf = 98.9%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[31 sec, 179053 itr]: obj = (0.220054), mov = 179053, inf = 98.9%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[32 sec, 189722 itr]: obj = (0.220054), mov = 189722, inf = 99%, acc = 1%, imp = 848

worst case efficiency: 0.732033
[33 sec, 202569 itr]: obj = (0.220054), mov = 202569, inf = 99%, acc = 1%, imp = 848

worst case efficiency: 0.732033
[34 sec, 218509 itr]: obj = (0.220054), mov = 218509, inf = 99%, acc = 0.9%, imp = 848

worst case efficiency: 0.732033
[35 sec, 239863 itr]: obj = (0.220054), mov = 239863, inf = 99.1%, acc = 0.9%, imp = 848

worst case efficiency: 0.732033
[36 sec, 264549 itr]: obj = (0.220054), mov = 264549, inf = 99.1%, acc = 0.9%, imp = 848

worst case efficiency: 0.732033
[37 sec, 287908 itr]: obj = (0.220054), mov = 287908, inf = 99.1%, acc = 0.9%, imp = 848

worst case efficiency: 0.732033
[38 sec, 313997 itr]: obj = (0.220054), mov = 313997, inf = 99%, acc = 0.9%, imp = 848

worst case efficiency: 0.732033
[39 sec, 346282 itr]: obj = (0.220054), mov = 346282, inf = 99%, acc = 0.9%, imp = 848

worst case efficiency: 0.732033
[40 sec, 377990 itr]: obj = (0.220054), mov = 377990, inf = 99%, acc = 1%, imp = 848

worst case efficiency: 0.732033
[41 sec, 413105 itr]: obj = (0.220054), mov = 413105, inf = 98.9%, acc = 1%, imp = 848

worst case efficiency: 0.732033
[42 sec, 445533 itr]: obj = (0.220054), mov = 445533, inf = 98.9%, acc = 1%, imp = 848

worst case efficiency: 0.732033
[43 sec, 481114 itr]: obj = (0.220054), mov = 481114, inf = 98.8%, acc = 1%, imp = 848

worst case efficiency: 0.732033
[44 sec, 517044 itr]: obj = (0.220054), mov = 517044, inf = 98.8%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[45 sec, 552840 itr]: obj = (0.220054), mov = 552840, inf = 98.8%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[46 sec, 585280 itr]: obj = (0.220054), mov = 585280, inf = 98.8%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[47 sec, 617476 itr]: obj = (0.220054), mov = 617476, inf = 98.8%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[48 sec, 651208 itr]: obj = (0.220054), mov = 651208, inf = 98.7%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[49 sec, 687527 itr]: obj = (0.220054), mov = 687527, inf = 98.7%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[50 sec, 724283 itr]: obj = (0.220054), mov = 724283, inf = 98.7%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[51 sec, 762074 itr]: obj = (0.220054), mov = 762074, inf = 98.7%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[52 sec, 796051 itr]: obj = (0.220054), mov = 796051, inf = 98.7%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[53 sec, 831973 itr]: obj = (0.220054), mov = 831973, inf = 98.7%, acc = 1.1%, imp = 848

worst case efficiency: 0.732033
[54 sec, 866034 itr]: obj = (0.220054), mov = 866034, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[55 sec, 901328 itr]: obj = (0.220054), mov = 901328, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[56 sec, 938457 itr]: obj = (0.220054), mov = 938457, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[57 sec, 975946 itr]: obj = (0.220054), mov = 975946, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[58 sec, 1013406 itr]: obj = (0.220054), mov = 1013406, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[59 sec, 1050132 itr]: obj = (0.220054), mov = 1050132, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[60 sec, 1086492 itr]: obj = (0.220054), mov = 1086492, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[61 sec, 1123402 itr]: obj = (0.220054), mov = 1123402, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[62 sec, 1159819 itr]: obj = (0.220054), mov = 1159819, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[63 sec, 1197025 itr]: obj = (0.220054), mov = 1197025, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[64 sec, 1235076 itr]: obj = (0.220054), mov = 1235076, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[65 sec, 1271758 itr]: obj = (0.220054), mov = 1271758, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[66 sec, 1310112 itr]: obj = (0.220054), mov = 1310112, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[67 sec, 1348564 itr]: obj = (0.220054), mov = 1348564, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[68 sec, 1383532 itr]: obj = (0.220054), mov = 1383532, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[69 sec, 1420785 itr]: obj = (0.220054), mov = 1420785, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[70 sec, 1457760 itr]: obj = (0.220054), mov = 1457760, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[71 sec, 1490323 itr]: obj = (0.220054), mov = 1490323, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[72 sec, 1522253 itr]: obj = (0.220054), mov = 1522253, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[73 sec, 1556224 itr]: obj = (0.220054), mov = 1556224, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[74 sec, 1587242 itr]: obj = (0.220054), mov = 1587242, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[75 sec, 1617466 itr]: obj = (0.220054), mov = 1617466, inf = 98.7%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[76 sec, 1648641 itr]: obj = (0.220054), mov = 1648641, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[77 sec, 1675304 itr]: obj = (0.220054), mov = 1675304, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[78 sec, 1700532 itr]: obj = (0.220054), mov = 1700532, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[79 sec, 1724599 itr]: obj = (0.220054), mov = 1724599, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[80 sec, 1754449 itr]: obj = (0.220054), mov = 1754449, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[81 sec, 1788206 itr]: obj = (0.220054), mov = 1788206, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[82 sec, 1822327 itr]: obj = (0.220054), mov = 1822327, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[83 sec, 1855032 itr]: obj = (0.220054), mov = 1855032, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[84 sec, 1887997 itr]: obj = (0.220054), mov = 1887997, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[85 sec, 1922500 itr]: obj = (0.220054), mov = 1922500, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[86 sec, 1956526 itr]: obj = (0.220054), mov = 1956526, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[87 sec, 1990768 itr]: obj = (0.220054), mov = 1990768, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[88 sec, 2024460 itr]: obj = (0.220054), mov = 2024460, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[89 sec, 2056422 itr]: obj = (0.220054), mov = 2056422, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[90 sec, 2087048 itr]: obj = (0.220054), mov = 2087048, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
[90 sec, 2087048 itr]: obj = (0.220054), mov = 2087048, inf = 98.6%, acc = 1.2%, imp = 848

worst case efficiency: 0.732033
2087048 iterations, 2087048 moves performed in 90 seconds
Feasible solution: obj = (0.220054)

Run output...
FINAL SOLUTION
Worst case efficiency: 0.732033
Mapping: 0->47, 1->39, 2->27, 3->24, 4->16, 5->7, 6->5, 7->41, 8->33, 9->33, 10->6, 11->37, 12->38, 13->43, 14->22, 15->40, 16->40, 17->12, 18->33, 19->0, 20->37, 21->42, 22->37, 23->33, 24->36, 25->14, 26->45, 27->1, 28->22, 29->35, 30->3, 31->43, 32->42, 33->7, 34->31, 35->22, 36->25, 37->19, 38->31, 39->11, 40->4, 41->3, 42->29, 43->19, 44->24, 45->16, 46->33, 47->4, 48->38, 49->32, 50->0, 51->18, 52->42, 53->6, 54->38, 55->34, 56->16, 57->10, 58->11, 59->16, 60->3, 61->28, 62->36, 63->19, 64->42, 65->41, 66->7, 67->38, 68->31, 69->22, 70->20, 71->27, 72->26, 73->7, 74->0, 75->46, 76->28, 77->45, 78->46, 79->2, 80->14, 81->8, 82->18, 83->37, 84->24, 85->29, 86->13, 87->7, 88->39, 89->43, 90->21, 91->22, 92->29, 93->10, 94->18, 95->1, 96->39, 97->11, 98->19, 99->34, 100->20, 101->34, 102->9, 103->15, 104->41, 105->44, 106->35, 107->21, 108->23, 109->19, 110->8, 111->17, 112->32, 113->35, 114->24, 115->17, 116->11, 117->14, 118->26, 119->20, 120->31, 121->25, 122->40, 123->43, 124->39, 125->18, 126->1, 127->26, 128->8, 129->41, 130->8, 131->45, 132->32, 133->15, 134->27, 135->10, 136->33, 137->46, 138->15, 139->26, 140->15, 141->40, 142->37, 143->21, 144->36, 145->44, 146->32, 147->0, 148->47, 149->40, 150->25, 151->14, 152->2, 153->0, 154->40, 155->46, 156->25, 157->6, 158->47, 159->5, 160->39, 161->25, 162->13, 163->31, 164->3, 165->26, 166->1, 167->27, 168->38, 169->34, 170->2, 171->12, 172->5, 173->9, 174->47, 175->2, 176->42, 177->18, 178->47, 179->25, 180->0, 181->12, 182->5, 183->19, 184->13, 185->28, 186->30, 187->8, 188->28, 189->23, 190->41, 191->45, 192->16, 193->9, 194->4, 195->20, 196->26, 197->44, 198->28, 199->45, 200->35, 201->7, 202->3, 203->5, 204->4, 205->17, 206->23, 207->32, 208->9, 209->45, 210->22, 211->2, 212->34, 213->47, 214->30, 215->30, 216->13, 217->21, 218->24, 219->44, 220->36, 221->17, 222->17, 223->35, 224->31, 225->4, 226->28, 227->6, 228->15, 229->27, 230->23, 231->23, 232->3, 233->13, 234->11, 235->10, 236->29, 237->14, 238->27, 239->2, 240->16, 241->32, 242->13, 243->46, 244->5, 245->12, 246->20, 247->15, 248->35, 249->37, 250->8, 251->30, 252->24, 253->38, 254->29, 255->4, 256->1, 257->17, 258->21, 259->34, 260->12, 261->10, 262->6, 263->39, 264->36, 265->20, 266->11, 267->23, 268->30, 269->43, 270->14, 271->9, 272->43, 273->18, 274->6, 275->44, 276->42, 277->21, 278->1, 279->9, 280->41, 281->44, 282->36, 283->29, 284->10, 285->12, 286->30, 287->46
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	0.774432	0.582735	0.809463	1.67739	1.39478	1.05967
	1.6131	6.74169	2.511	2.74761	4.40418	4.12945
	1.47141	3.25087	3.08743	15.291	2.5364	1.37125
	2.22971	7.88253	5.15518	4.0186	8.42854	2.28618
	1.21114	3.80407	4.80943	6.08589	3.73133	2.72375
	1.67752	5.01222	5.82739	6.57657	8.03132	0.894092
	1.49975	7.1636	6.25519	5.64564	5.1781	1.02066
	1.09534	2.20931	2.72376	2.49118	4.04903	0.829149
Efficiency map:
	0.99722	0.84585	0.908972	0.899844	0.912055	1.00307
	0.732731	0.777125	0.744369	0.733879	0.738963	0.869931
	0.829484	0.732033	0.737286	0.962117	0.732127	0.821502
	0.826123	0.823781	0.7376	0.757197	0.863838	0.842511
	0.819952	0.733215	0.732691	0.818717	0.732033	0.874987
	0.792648	0.753583	0.733252	0.845081	0.879811	0.819533
	0.827682	0.825452	0.80061	0.739085	0.73313	0.809658
	0.812912	0.738709	0.73686	0.732164	0.813633	0.779439
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 15 --global-tlim 90 --seed 501

Starting the Optimization Process
