LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
ANNs For the Platform:
4,2,1,6.45294,0.0174524,1.58234,2.29305,2.12057,3.95113,12.7206,-19.3278,5.58789,9.38298,0.00306445,0.00353919,0.795949
4,2,1,3.10684,7.56359,0.502116,0.637049,12.0356,-2.9013,-3.22211,-0.368567,-0.562298,-7.37831,2.38595,1.22203,-0.360292
4,2,1,4.88387,44.8399,1.27345,1.24496,49.1775,-8.83277,-3.12983,-1.00946,11.7,-3.01002,0.132635,0.00372271,0.673996
4,2,1,4.73045,19.3982,0.449241,0.753865,25.7092,17.3605,0.0558085,-8.91516,0.49993,10.3396,4.06312,-0.0136297,-3.24599
4,2,1,-7.16849,-10.0957,-0.518885,-0.00359459,5.93351,5.62684,52.0516,1.48603,1.30274,57.2311,0.250578,0.137157,0.417379
4,2,1,0.412706,-2.89716,0.137754,-2.57826,11.0947,121.374,858.444,-136.26,-14917.8,-1308.92,-3.36558,-0.000403784,4.17997
4,2,1,-8.8269,-0.482244,-0.0820712,-0.0562713,-10.1617,9.78118,-7.01882,-0.428151,-0.0606718,2.56012,-8.54699,-0.509991,-7.23704
4,2,1,2.19881,4.94147,0.541452,0.107587,6.45213,1.0452,0.405805,0.555763,3.64728,1.95727,0.425683,-0.00188273,0.383643
4,2,1,-6.13316,1.92598,-0.0562274,-0.0731719,-4.60606,0.509071,7.79918,0.393802,0.0114563,7.9345,-1.13416,0.310582,-0.637365
4,2,1,1.73462,2.97854,0.276167,0.0859465,3.6912,3.09203,3.25403,0.324282,0.112495,4.06123,0.954718,-0.48836,0.344765
4,2,1,1.57339,3.84603,-3.4467,0.7429,2.75709,2.1802,5.09535,0.482172,0.106131,6.49189,-0.0251827,0.433535,0.393482
4,2,1,4.13514,21.6229,1.1931,0.354612,24.1372,25.5916,0.432728,-12.6725,-0.45345,15.3428,0.194819,-0.0364695,0.647465
4,2,1,-0.728632,5.60079,1.78098,1.8429,-5.74354,-3.26566,-13.0026,-0.907163,-0.31822,-15.3707,-0.427666,-0.224298,0.154329
4,2,1,3.81854,-4.98219,-0.293924,-0.103604,-6.7034,2.28654,4.72568,0.469524,0.115005,6.05901,-0.511241,0.421809,-0.121549
4,2,1,-1.23773,5.98591,0.224555,-0.359337,6.05767,2.3852,3.4706,0.220977,0.179377,4.88772,0.199751,0.432231,0.177791
4,2,1,1.12189,4.09326,0.850801,0.171671,-5.17646,-1.9656,-4.07995,-0.210811,-0.113853,-5.17242,-2.70452,-0.46094,-2.36326
4,2,1,2.08858,4.80174,0.367584,0.109336,6.02292,4.32969,4.35157,0.772172,10.015,-0.96022,0.43933,4.17055e-05,0.365228
4,2,1,1.18806,3.00188,0.124753,0.00191203,3.75084,1.03049,2.7808,0.137818,0.0250058,4.01619,-1.05018,2.49176,-0.635083
4,2,1,3.11788,10.9247,0.699295,0.170953,13.0959,-11.4646,-5.88946,11.0137,8.74787,-11.701,0.236706,0.0216102,0.59131
4,2,1,7.53568,1.94998,-2.27776,-1.91765,0.451708,2.13206,4.27547,0.333162,0.0864645,5.45063,-0.0153486,0.447774,0.366202
4,2,1,-2.12436,-3.29848,-0.0965154,-0.0454545,-3.86293,4.27042,3.64797,0.106703,0.057602,4.81479,-1.28015,-0.815344,0.346524
4,2,1,2.06313,3.99765,-9.94001e-06,0.200591,5.05136,-0.024567,-3.22765,-1.84842,1.73493,-3.87425,0.446216,-0.0350173,0.320411
4,2,1,3.3952,3.19239,0.219932,0.0459907,4.0724,1.85184,2.9352,0.236295,0.0535068,3.62882,-0.516849,0.990723,0.337742
4,2,1,-6.32145,2.68844,0.160066,0.0633176,-3.81442,9.03888,1.31396,0.253255,-0.0684612,6.93971,1.83579,0.440423,2.20116
4,2,1,-4.54148,-0.217309,1.86369,-0.409034,0.0885675,-3.10698,-12.3715,-0.753639,-0.275832,-14.544,0.00344558,-0.233405,0.57337
4,2,1,-0.743367,6.86662,-0.19496,-0.0184317,7.54898,2.69258,3.24789,0.437511,0.121848,4.97423,0.830361,0.39615,-0.417087
4,2,1,2.07654,3.2533,0.103762,0.0837251,4.40922,1.95615,-2.57949,-0.10147,-0.0485499,-3.72005,0.479113,-0.433879,-0.0966803
4,2,1,-0.480501,-0.00331725,1.67452,0.764966,-1.53387,1.99113,3.67475,0.0228188,0.0287235,4.64739,0.15721,0.465135,0.487915
4,2,1,1.11584,-5.79333,-0.212675,0.376981,-6.64888,2.45003,3.54368,0.313683,0.144659,5.05472,-0.777178,0.423599,-0.389995
4,2,1,2.39093,1.70258,-0.183288,-0.314006,4.06496,2.34881,2.91375,-0.0230153,-0.18977,5.26307,-1.33645,1.84807,0.292237
4,2,1,0.88022,3.23795,0.179315,0.0205209,3.57684,0.764441,2.74982,0.156077,0.0273373,3.62313,-0.980719,2.16912,-0.37872
4,2,1,-1.27932,7.09751,-0.0490235,0.455487,7.14709,-2.57722,-4.06233,-0.410784,-0.0433739,-5.6171,0.164894,-0.408714,0.23583
4,2,1,0.623158,5.01876,0.194867,0.0365944,4.99458,-16.0815,2.94826,-1.82463,-1.90388,-9.56395,0.291148,-0.155001,0.360694
4,2,1,1.86251,3.92056,0.158781,0.0575693,4.93436,0.681285,4.09563,1.09826,-3.65606,2.09524,0.482473,-0.00448292,0.322397
4,2,1,3.33939,3.52877,0.262622,0.0607689,4.45842,2.23218,3.37107,0.256541,0.0516206,4.05014,-1.2701,1.7162,0.365737
4,2,1,5.0585,4.68315,2.53887,0.128578,8.9709,3.62833,11.0789,1.32555,0.190392,13.8026,-0.126366,0.350541,0.580103
4,2,1,4.99986,2.75557,1.33036,-0.0119951,6.91685,4.23025,5.34568,1.08928,0.0379928,8.80363,-0.700785,0.958812,0.547325
4,2,1,1.95024,3.24732,0.229322,0.0270089,3.53624,1.62575,3.04977,0.230194,0.0319079,3.46966,-1.34015,1.81817,0.333213
4,2,1,2.11055,5.16581,0.225183,0.0767342,6.2013,3.43647,-3.10756,1.22453,-6.27082,5.92438,0.428242,2.12665,-1.75474
4,2,1,10.028,2.55152,-0.0579427,-0.0186516,-2.98054,2.14055,4.33639,0.203596,0.057269,5.47668,0.0078111,0.436528,0.371941
4,2,1,1.80421,3.45533,0.27065,0.0337725,4.14172,3.00203,3.65885,0.26995,0.0620062,4.37924,0.892686,-0.440136,0.355377
4,2,1,-2.76424,-10.7675,-0.741451,-0.141046,-12.927,13.6995,2.51611,0.90934,27.76,15.1973,-0.260938,-0.00175312,0.546274
4,2,1,2.26659,3.88332,0.083697,0.287373,-5.89957,3.07548,7.5942,0.285535,0.141703,9.90558,1.94369,0.273352,2.47758
4,2,1,5.41472,-1.35343,-0.0808605,2.27411,0.928252,2.55844,6.15687,0.338903,0.0735359,7.47381,-0.0148119,0.388691,0.423615
4,2,1,4.53285,3.66527,0.130055,0.0722232,4.95325,-2.1632,-3.34312,-0.11633,-0.0577237,-3.98867,-0.759785,-1.19124,0.379449
4,2,1,2.36669,5.75119,0.132783,0.0812059,6.8845,-8.68015,-0.488627,0.811521,2.16444,0.277436,0.40839,0.00790296,0.400784
4,2,1,0.583762,8.58805,0.307497,0.0165495,8.61973,-7.32584,1.34397,-0.0108636,-0.0260377,-6.96836,0.266022,-5.2323,-4.69173
4,2,1,1.76683,-0.336959,-0.116305,-0.0696435,-1.62311,2.51027,5.65249,0.180271,0.0657051,7.97995,-0.0500539,0.457869,0.308072
Workload Information:
0.822202, 1.03693, 0.576976, 3.61605, 3.89835, 2.0495, 0.5, 1.27889, 3.11919, 0.751166, 3.15822, 3.19253, 1.85188, 1.784, 1.98891, 4.63716, 0.555712, 1.18234, 0.5, 1.71343, 0.994135, 3.09443, 2.69763, 3.00038, 1.98211, 2.27377, 1.91712, 1.88123, 1.78375, 2.16201, 2.39605, 2.3869, 1.20719, 1.56076, 2.60248, 1.84663, 2.62657, 2.37566, 0.721892, 0.73276, 2.79826, 2.74486, 0.5, 1.84905, 4.00113, 2.00266, 1.86305, 1.7841, 3.3957, 0.5, 3.37115, 1.71734, 2.01574, 1.00006, 2.33235, 0.907894, 1.92003, 2.49172, 1.97915, 2.36887, 0.897262, 3.45424, 2.84769, 2.21824, 2.08257, 0.5, 0.639426, 0.5, 3.79278, 2.74457, 1.31356, 3.00967, 2.02057, 1.82647, 0.823463, 1.45008, 2.80056, 3.07885, 1.79288, 2.02898, 1.89328, 2.17166, 2.45822, 1.65498, 1.8358, 2.39425, 1.43032, 1.78828, 2.37552, 2.17583, 0.5, 0.731085, 0.861213, 2.84868, 3.67892, 3.3801, 3.35293, 1.06079, 2.39516, 2.00835, 1.33842, 1.84435, 0.588556, 2.83028, 3.69538, 1.36061, 3.02517, 0.5, 0.550459, 0.602651, 0.931703, 2.75636, 4.2002, 2.95863, 3.38172, 2.35425, 0.5, 2.04737, 2.57186, 1.1448, 0.5, 2.83378, 2.97744, 2.05238, 2.88634, 0.750057, 4.17159, 1.33515, 0.690573, 2.8313, 0.5, 2.47139, 21.2536, 9.71935, 26.2397, 24.4538, 24.0133, 26.3202, 12.9733, 15.8205, 16.4545, 35, 19.1206, 32.631, 0.5, 2.30977, 1.00696, 1.80931, 1.56134, 4.81262, 1.98746, 2.25654, 1.56531, 1.89278, 2.19437, 2.10355, 4.06932, 2.63585, 2.36012, 0.674881, 0.5, 1.75983, 11.395, 22.7298, 21.6593, 35, 22.4994, 18.7165, 25.1279, 24.7518, 3.96816, 29.1898, 26.6711, 22.2912, 19.041, 19.8969, 14.5628, 30.3099, 20.8656, 27.3238, 0.5, 2.58027, 1.39463, 1.89467, 3.44538, 2.18505, 2.995, 0.779683, 1.10151, 2.0619, 1.94578, 3.11613, 2.22775, 2.20418, 1.64147, 1.65098, 2.42005, 1.85557, 26.2354, 16.8492, 25.7157, 23.4695, 23.8466, 15.8835, 19.4906, 35, 20.7529, 22.1737, 16.3682, 18.2145, 1.42512, 34.7831, 9.95029, 34.7868, 16.0547, 35, 1.85093, 3.66256, 1.23621, 1.03314, 1.64496, 2.57219, 2.51038, 1.97016, 2.43242, 2.53065, 2.05639, 0.5, 1.36001, 2.31327, 1.90728, 2.14544, 2.20865, 2.06535, 1.45245, 2.34827, 2.21373, 2.3091, 1.90416, 1.77229, 1.6424, 2.36952, 2.78981, 1.2392, 1.02584, 2.93322, 1.57511, 3.56747, 0.785944, 4.13503, 0.5, 1.43644, 2.34065, 1.07513, 2.40504, 2.00058, 2.25765, 1.92094, 1.29166, 0.5, 0.871675, 2.43035, 3.92671, 2.9796, 1.26283, 3.57321, 0.705942, 0.5, 5.06186, 0.89616, 2.37925, 2.398, 1.24377, 0.762687, 2.21961, 2.99668, 3.19817, 0.643531, 1.36463, 0.604851, 3.6371, 2.55172, 2.36529, 2.23317, 2.33473, 0.659972, 2.24374, 2.16309
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 44729, operands = 116736
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 401, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.664125


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case efficiency: 0.998464
[1 sec, 2353 itr]: infeas = (2, 2, 4), mov = 2353, inf = 63.4%, acc = 33.3%, imp = 137

worst case efficiency: 0.472474
[2 sec, 17207 itr]: obj = (0.0901068), mov = 17207, inf = 94.7%, acc = 4.9%, imp = 181

worst case efficiency: 0.654064
[3 sec, 29762 itr]: obj = (0.109959), mov = 29762, inf = 96.8%, acc = 3%, imp = 226

worst case efficiency: 0.665975
[4 sec, 39784 itr]: obj = (0.11903), mov = 39784, inf = 97.5%, acc = 2.4%, imp = 272

worst case efficiency: 0.671418
[5 sec, 46620 itr]: obj = (0.125006), mov = 46620, inf = 97.8%, acc = 2.1%, imp = 311

worst case efficiency: 0.675004
[6 sec, 51584 itr]: obj = (0.129285), mov = 51584, inf = 97.9%, acc = 2%, imp = 340

worst case efficiency: 0.677571
[7 sec, 55914 itr]: obj = (0.180012), mov = 55914, inf = 98%, acc = 1.9%, imp = 383

worst case efficiency: 0.708007
[8 sec, 59701 itr]: obj = (0.195357), mov = 59701, inf = 98.1%, acc = 1.9%, imp = 414

worst case efficiency: 0.717214
[9 sec, 63414 itr]: obj = (0.226384), mov = 63414, inf = 98.2%, acc = 1.8%, imp = 451

worst case efficiency: 0.735831
[10 sec, 67134 itr]: obj = (0.251152), mov = 67134, inf = 98.2%, acc = 1.8%, imp = 490

worst case efficiency: 0.750691
[11 sec, 70598 itr]: obj = (0.281172), mov = 70598, inf = 98.2%, acc = 1.7%, imp = 526

worst case efficiency: 0.768703
[12 sec, 74177 itr]: obj = (0.296571), mov = 74177, inf = 98.3%, acc = 1.7%, imp = 565

worst case efficiency: 0.777943
[13 sec, 77787 itr]: obj = (0.314214), mov = 77787, inf = 98.3%, acc = 1.7%, imp = 595

worst case efficiency: 0.788528
[14 sec, 81448 itr]: obj = (0.32334), mov = 81448, inf = 98.4%, acc = 1.6%, imp = 625

worst case efficiency: 0.794004
[15 sec, 85084 itr]: obj = (0.326227), mov = 85084, inf = 98.4%, acc = 1.6%, imp = 640

worst case efficiency: 0.795736
[16 sec, 88645 itr]: obj = (0.33331), mov = 88645, inf = 98.5%, acc = 1.6%, imp = 662

worst case efficiency: 0.799986
[17 sec, 92265 itr]: obj = (0.340063), mov = 92265, inf = 98.5%, acc = 1.5%, imp = 687

worst case efficiency: 0.804038
[18 sec, 95839 itr]: obj = (0.348572), mov = 95839, inf = 98.5%, acc = 1.5%, imp = 720

worst case efficiency: 0.809143
[19 sec, 99469 itr]: obj = (0.35816), mov = 99469, inf = 98.5%, acc = 1.5%, imp = 755

worst case efficiency: 0.814896
[20 sec, 103173 itr]: obj = (0.368363), mov = 103173, inf = 98.6%, acc = 1.5%, imp = 777

worst case efficiency: 0.821018
[21 sec, 106853 itr]: obj = (0.37166), mov = 106853, inf = 98.6%, acc = 1.4%, imp = 804

worst case efficiency: 0.822996
[22 sec, 110549 itr]: obj = (0.372651), mov = 110549, inf = 98.6%, acc = 1.4%, imp = 816

worst case efficiency: 0.823591
[23 sec, 114163 itr]: obj = (0.375786), mov = 114163, inf = 98.7%, acc = 1.4%, imp = 840

worst case efficiency: 0.825472
[24 sec, 117771 itr]: obj = (0.37921), mov = 117771, inf = 98.7%, acc = 1.4%, imp = 870

worst case efficiency: 0.827526
[25 sec, 121367 itr]: obj = (0.381945), mov = 121367, inf = 98.7%, acc = 1.3%, imp = 889

worst case efficiency: 0.829167
[26 sec, 124545 itr]: obj = (0.386071), mov = 124545, inf = 98.7%, acc = 1.3%, imp = 899

worst case efficiency: 0.831643
[27 sec, 128012 itr]: obj = (0.386071), mov = 128012, inf = 98.8%, acc = 1.3%, imp = 899

worst case efficiency: 0.831643
[28 sec, 131708 itr]: obj = (0.386071), mov = 131708, inf = 98.8%, acc = 1.2%, imp = 899

worst case efficiency: 0.831643
[29 sec, 135177 itr]: obj = (0.386071), mov = 135177, inf = 98.8%, acc = 1.2%, imp = 899

worst case efficiency: 0.831643
[30 sec, 138789 itr]: obj = (0.386071), mov = 138789, inf = 98.9%, acc = 1.2%, imp = 899

worst case efficiency: 0.831643
[31 sec, 142439 itr]: obj = (0.386071), mov = 142439, inf = 98.9%, acc = 1.2%, imp = 899

worst case efficiency: 0.831643
[32 sec, 145835 itr]: obj = (0.386071), mov = 145835, inf = 98.9%, acc = 1.1%, imp = 899

worst case efficiency: 0.831643
[33 sec, 149444 itr]: obj = (0.386071), mov = 149444, inf = 98.9%, acc = 1.1%, imp = 899

worst case efficiency: 0.831643
[34 sec, 153213 itr]: obj = (0.386071), mov = 153213, inf = 99%, acc = 1.1%, imp = 899

worst case efficiency: 0.831643
[35 sec, 157149 itr]: obj = (0.386071), mov = 157149, inf = 99%, acc = 1%, imp = 899

worst case efficiency: 0.831643
[36 sec, 161226 itr]: obj = (0.386071), mov = 161226, inf = 99%, acc = 1%, imp = 899

worst case efficiency: 0.831643
[37 sec, 164987 itr]: obj = (0.386071), mov = 164987, inf = 99%, acc = 1%, imp = 899

worst case efficiency: 0.831643
[38 sec, 168182 itr]: obj = (0.386071), mov = 168182, inf = 99.1%, acc = 1%, imp = 899

worst case efficiency: 0.831643
[39 sec, 172870 itr]: obj = (0.386071), mov = 172870, inf = 99.1%, acc = 1%, imp = 899

worst case efficiency: 0.831643
[40 sec, 178425 itr]: obj = (0.386071), mov = 178425, inf = 99.1%, acc = 0.9%, imp = 899

worst case efficiency: 0.831643
[41 sec, 185161 itr]: obj = (0.386071), mov = 185161, inf = 99.2%, acc = 0.9%, imp = 899

worst case efficiency: 0.831643
[42 sec, 194185 itr]: obj = (0.386071), mov = 194185, inf = 99.2%, acc = 0.9%, imp = 899

worst case efficiency: 0.831643
[43 sec, 206678 itr]: obj = (0.386071), mov = 206678, inf = 99.2%, acc = 0.8%, imp = 899

worst case efficiency: 0.831643
[44 sec, 227423 itr]: obj = (0.386071), mov = 227423, inf = 99.3%, acc = 0.7%, imp = 899

worst case efficiency: 0.831643
[45 sec, 239902 itr]: obj = (0.386071), mov = 239902, inf = 99.4%, acc = 0.7%, imp = 899

worst case efficiency: 0.831643
[46 sec, 252530 itr]: obj = (0.386071), mov = 252530, inf = 99.4%, acc = 0.7%, imp = 899

worst case efficiency: 0.831643
[47 sec, 264372 itr]: obj = (0.386071), mov = 264372, inf = 99.4%, acc = 0.6%, imp = 899

worst case efficiency: 0.831643
[48 sec, 276994 itr]: obj = (0.386071), mov = 276994, inf = 99.5%, acc = 0.6%, imp = 899

worst case efficiency: 0.831643
[49 sec, 289368 itr]: obj = (0.386071), mov = 289368, inf = 99.5%, acc = 0.6%, imp = 899

worst case efficiency: 0.831643
[50 sec, 301703 itr]: obj = (0.386071), mov = 301703, inf = 99.5%, acc = 0.6%, imp = 899

worst case efficiency: 0.831643
[51 sec, 313920 itr]: obj = (0.386071), mov = 313920, inf = 99.5%, acc = 0.6%, imp = 899

worst case efficiency: 0.831643
[52 sec, 325989 itr]: obj = (0.386071), mov = 325989, inf = 99.5%, acc = 0.5%, imp = 899

worst case efficiency: 0.831643
[53 sec, 338651 itr]: obj = (0.386071), mov = 338651, inf = 99.6%, acc = 0.5%, imp = 899

worst case efficiency: 0.831643
[54 sec, 351169 itr]: obj = (0.386071), mov = 351169, inf = 99.6%, acc = 0.5%, imp = 899

worst case efficiency: 0.831643
[55 sec, 363426 itr]: obj = (0.386071), mov = 363426, inf = 99.6%, acc = 0.5%, imp = 899

worst case efficiency: 0.831643
[56 sec, 375506 itr]: obj = (0.386071), mov = 375506, inf = 99.6%, acc = 0.5%, imp = 899

worst case efficiency: 0.831643
[57 sec, 388144 itr]: obj = (0.386071), mov = 388144, inf = 99.6%, acc = 0.5%, imp = 899

worst case efficiency: 0.831643
[58 sec, 400758 itr]: obj = (0.386071), mov = 400758, inf = 99.6%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[59 sec, 412972 itr]: obj = (0.386071), mov = 412972, inf = 99.6%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[60 sec, 425260 itr]: obj = (0.386071), mov = 425260, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[61 sec, 437506 itr]: obj = (0.386071), mov = 437506, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[62 sec, 449832 itr]: obj = (0.386071), mov = 449832, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[63 sec, 462110 itr]: obj = (0.386071), mov = 462110, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[64 sec, 473931 itr]: obj = (0.386071), mov = 473931, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[65 sec, 486029 itr]: obj = (0.386071), mov = 486029, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[66 sec, 497995 itr]: obj = (0.386071), mov = 497995, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[67 sec, 510599 itr]: obj = (0.386071), mov = 510599, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[68 sec, 522473 itr]: obj = (0.386071), mov = 522473, inf = 99.7%, acc = 0.4%, imp = 899

worst case efficiency: 0.831643
[69 sec, 534435 itr]: obj = (0.386071), mov = 534435, inf = 99.7%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[70 sec, 546380 itr]: obj = (0.386071), mov = 546380, inf = 99.7%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[71 sec, 557354 itr]: obj = (0.386071), mov = 557354, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[72 sec, 568657 itr]: obj = (0.386071), mov = 568657, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[73 sec, 580138 itr]: obj = (0.386071), mov = 580138, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[74 sec, 591937 itr]: obj = (0.386071), mov = 591937, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[75 sec, 604304 itr]: obj = (0.386071), mov = 604304, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[76 sec, 616671 itr]: obj = (0.386071), mov = 616671, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[77 sec, 629149 itr]: obj = (0.386071), mov = 629149, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[78 sec, 641339 itr]: obj = (0.386071), mov = 641339, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[79 sec, 653584 itr]: obj = (0.386071), mov = 653584, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[80 sec, 666004 itr]: obj = (0.386071), mov = 666004, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[81 sec, 677918 itr]: obj = (0.386071), mov = 677918, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[82 sec, 690156 itr]: obj = (0.386071), mov = 690156, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[83 sec, 702034 itr]: obj = (0.386071), mov = 702034, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[84 sec, 714242 itr]: obj = (0.386071), mov = 714242, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[85 sec, 726229 itr]: obj = (0.386071), mov = 726229, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[86 sec, 738361 itr]: obj = (0.386071), mov = 738361, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[87 sec, 750451 itr]: obj = (0.386071), mov = 750451, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[88 sec, 762456 itr]: obj = (0.386071), mov = 762456, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[89 sec, 774659 itr]: obj = (0.386071), mov = 774659, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[90 sec, 786795 itr]: obj = (0.386071), mov = 786795, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
[90 sec, 786795 itr]: obj = (0.386071), mov = 786795, inf = 99.8%, acc = 0.3%, imp = 899

worst case efficiency: 0.831643
786795 iterations, 786795 moves performed in 90 seconds
Feasible solution: obj = (0.386071)

Run output...
FINAL SOLUTION
Worst case efficiency: 0.831643
Mapping: 0->46, 1->46, 2->23, 3->25, 4->34, 5->5, 6->11, 7->17, 8->46, 9->3, 10->25, 11->41, 12->22, 13->47, 14->37, 15->41, 16->19, 17->33, 18->0, 19->42, 20->4, 21->6, 22->44, 23->16, 24->9, 25->36, 26->28, 27->25, 28->36, 29->25, 30->17, 31->23, 32->23, 33->36, 34->44, 35->19, 36->38, 37->34, 38->30, 39->6, 40->18, 41->25, 42->11, 43->2, 44->30, 45->10, 46->39, 47->4, 48->27, 49->2, 50->44, 51->32, 52->9, 53->47, 54->21, 55->2, 56->9, 57->46, 58->47, 59->22, 60->3, 61->13, 62->10, 63->11, 64->45, 65->5, 66->24, 67->5, 68->15, 69->19, 70->7, 71->28, 72->29, 73->11, 74->35, 75->30, 76->14, 77->9, 78->27, 79->40, 80->43, 81->6, 82->39, 83->31, 84->33, 85->29, 86->41, 87->38, 88->21, 89->43, 90->1, 91->1, 92->31, 93->46, 94->13, 95->40, 96->20, 97->30, 98->12, 99->23, 100->3, 101->12, 102->16, 103->35, 104->40, 105->4, 106->29, 107->0, 108->23, 109->41, 110->2, 111->12, 112->21, 113->32, 114->7, 115->0, 116->26, 117->8, 118->40, 119->11, 120->0, 121->37, 122->34, 123->44, 124->40, 125->4, 126->15, 127->36, 128->24, 129->39, 130->23, 131->7, 132->14, 133->28, 134->25, 135->33, 136->10, 137->8, 138->46, 139->32, 140->31, 141->38, 142->16, 143->16, 144->6, 145->8, 146->1, 147->20, 148->42, 149->13, 150->36, 151->6, 152->3, 153->18, 154->12, 155->36, 156->33, 157->28, 158->20, 159->5, 160->42, 161->10, 162->15, 163->27, 164->45, 165->31, 166->37, 167->34, 168->19, 169->44, 170->6, 171->28, 172->14, 173->43, 174->22, 175->40, 176->7, 177->13, 178->47, 179->27, 180->1, 181->15, 182->30, 183->16, 184->27, 185->0, 186->10, 187->2, 188->17, 189->24, 190->42, 191->15, 192->34, 193->27, 194->35, 195->45, 196->33, 197->14, 198->26, 199->21, 200->19, 201->22, 202->21, 203->32, 204->45, 205->39, 206->31, 207->37, 208->33, 209->7, 210->14, 211->9, 212->15, 213->26, 214->43, 215->20, 216->17, 217->45, 218->4, 219->11, 220->44, 221->38, 222->43, 223->39, 224->41, 225->42, 226->3, 227->1, 228->28, 229->22, 230->32, 231->29, 232->37, 233->8, 234->43, 235->9, 236->13, 237->41, 238->30, 239->39, 240->14, 241->18, 242->24, 243->24, 244->4, 245->42, 246->17, 247->32, 248->31, 249->5, 250->3, 251->19, 252->38, 253->35, 254->7, 255->1, 256->16, 257->26, 258->18, 259->47, 260->22, 261->21, 262->34, 263->37, 264->45, 265->24, 266->5, 267->0, 268->8, 269->26, 270->38, 271->47, 272->12, 273->26, 274->17, 275->13, 276->29, 277->12, 278->18, 279->8, 280->35, 281->10, 282->18, 283->35, 284->20, 285->2, 286->20, 287->29
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.08988	0.873104	0.938051	1.18476	1.19183	1.42756
	2.12059	7.05816	6.40156	7.68801	6.02837	1.20378
	1.84626	7.91102	9.27473	5.83435	9.91548	1.73702
	2.01369	9.57117	7.85014	8.67238	8.31937	1.20498
	1.83235	6.63368	10.8503	10.1486	7.97196	2.49112
	1.75545	12.5849	6.97579	8.38825	5.68706	2.0401
	1.84074	9.11401	7.78449	7.64917	5.74326	2.43403
	1.86407	7.72964	6.18673	8.30148	3.88201	4.75447
Efficiency map:
	0.99722	0.885264	0.916173	0.871762	0.957514	1.00307
	0.856587	0.833245	0.839308	0.881987	0.831804	0.849398
	0.835995	0.881135	0.869541	0.842128	0.842309	0.863448
	0.868847	0.832751	0.879363	0.887025	0.831643	0.946186
	0.833723	0.832703	0.866313	0.895604	0.839354	0.924906
	0.835293	0.902431	0.831774	0.83204	0.852792	0.83839
	0.887159	0.906341	0.858338	0.8557	0.835715	0.840515
	0.831643	0.871733	0.860439	0.864406	0.832978	0.903514
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 17 --global-tlim 90 --seed 401

Starting the Optimization Process
