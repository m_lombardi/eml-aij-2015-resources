==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 14 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 1

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005489
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005202
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.020139
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004777
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.010677
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.019/11.066
- lns time: 1.019535
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009111
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.241/14.316
- lns time: 1.241224
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004925
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009350
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.565/18.895
- lns time: 0.565177
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006746
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009609
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 0.604933
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006914
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008284
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009494
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004791
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004586
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 0.625389
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005045
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008890
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009115
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005199
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.014604
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005523
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006038
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007962
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006490
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.489/53.734
- lns time: 1.489034
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.016044
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.011434
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.725855
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005362
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007204
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.011138
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010158
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004965
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004922
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006239
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005655
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007672
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005913
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.941/81.498
- lns time: 1.941366
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007288
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.010886
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006155
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.011755
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 0.469975
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 24

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 24
number of iterations: 49
total time: 90.004237

Final Solution: 0->2, 1->24, 2->27, 3->43, 4->44, 5->42, 6->45, 7->38, 8->41, 9->18, 10->45, 11->11, 12->18, 13->15, 14->35, 15->46, 16->1, 17->19, 18->35, 19->3, 20->2, 21->28, 22->9, 23->17, 24->17, 25->33, 26->37, 27->8, 28->42, 29->46, 30->40, 31->2, 32->23, 33->27, 34->11, 35->4, 36->32, 37->42, 38->29, 39->0, 40->23, 41->16, 42->3, 43->2, 44->35, 45->28, 46->9, 47->25, 48->1, 49->25, 50->6, 51->8, 52->39, 53->34, 54->14, 55->33, 56->14, 57->32, 58->42, 59->45, 60->6, 61->34, 62->15, 63->5, 64->37, 65->34, 66->9, 67->28, 68->21, 69->44, 70->7, 71->29, 72->4, 73->39, 74->1, 75->29, 76->47, 77->41, 78->30, 79->4, 80->26, 81->0, 82->35, 83->24, 84->19, 85->38, 86->31, 87->18, 88->24, 89->3, 90->27, 91->47, 92->22, 93->46, 94->44, 95->10, 96->0, 97->28, 98->11, 99->9, 100->20, 101->39, 102->31, 103->23, 104->3, 105->4, 106->37, 107->27, 108->47, 109->11, 110->1, 111->36, 112->13, 113->7, 114->38, 115->44, 116->2, 117->32, 118->44, 119->32, 120->37, 121->18, 122->19, 123->24, 124->29, 125->13, 126->1, 127->27, 128->17, 129->8, 130->5, 131->0, 132->31, 133->36, 134->33, 135->13, 136->34, 137->38, 138->6, 139->46, 140->35, 141->37, 142->14, 143->4, 144->15, 145->29, 146->43, 147->19, 148->10, 149->21, 150->12, 151->8, 152->24, 153->11, 154->21, 155->12, 156->5, 157->33, 158->26, 159->46, 160->32, 161->15, 162->13, 163->47, 164->32, 165->37, 166->22, 167->23, 168->16, 169->43, 170->38, 171->36, 172->26, 173->42, 174->16, 175->21, 176->0, 177->10, 178->40, 179->36, 180->22, 181->26, 182->39, 183->25, 184->12, 185->16, 186->17, 187->47, 188->41, 189->0, 190->40, 191->20, 192->33, 193->12, 194->3, 195->30, 196->25, 197->18, 198->18, 199->28, 200->10, 201->31, 202->5, 203->46, 204->7, 205->20, 206->7, 207->5, 208->7, 209->34, 210->30, 211->16, 212->19, 213->19, 214->10, 215->9, 216->45, 217->41, 218->14, 219->12, 220->34, 221->5, 222->20, 223->30, 224->26, 225->29, 226->20, 227->43, 228->36, 229->40, 230->41, 231->20, 232->9, 233->30, 234->36, 235->15, 236->7, 237->16, 238->6, 239->8, 240->33, 241->15, 242->21, 243->6, 244->24, 245->10, 246->2, 247->31, 248->4, 249->23, 250->14, 251->44, 252->13, 253->28, 254->17, 255->12, 256->43, 257->22, 258->23, 259->40, 260->43, 261->38, 262->39, 263->40, 264->27, 265->35, 266->42, 267->25, 268->45, 269->14, 270->6, 271->47, 272->45, 273->17, 274->39, 275->22, 276->31, 277->26, 278->1, 279->30, 280->25, 281->8, 282->13, 283->21, 284->41, 285->3, 286->11, 287->22

Average CPI map:
	2.08,	4.90,	4.42,	2.31,	4.20,	1.80
	3.88,	8.60,	5.30,	3.91,	5.60,	3.57
	2.84,	3.45,	3.43,	3.28,	3.21,	2.03
	2.45,	3.01,	2.97,	2.90,	2.85,	2.83
	2.82,	2.71,	2.69,	0.93,	2.44,	2.07
	4.51,	2.30,	2.25,	3.34,	2.24,	2.24
	2.24,	2.24,	2.24,	2.23,	2.23,	2.95
	2.23,	2.51,	2.22,	2.18,	2.18,	2.17

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
