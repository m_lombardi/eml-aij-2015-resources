==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 6 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 1

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.796/0.796
- lns time: 0.796086
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005369
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005415
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.899/5.706
- lns time: 0.899512
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006006
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005332
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.011558
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.087/12.816
- lns time: 1.086758
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006220
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005939
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006004
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005264
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005147
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009089
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.837/25.690
- lns time: 0.836689
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.012040
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005670
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.992/30.700
- lns time: 0.992443
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004999
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007909
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005731
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008665
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005399
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.015036
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005535
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006177
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005574
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007593
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005603
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005382
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004612
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007026
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005062
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.011133
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.201/64.013
- lns time: 1.201410
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007293
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006037
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005221
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008565
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.004966
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007648
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006213
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005876
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006530
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005984
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006059
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.012760
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 1.908340
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 26

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 26
number of iterations: 48
total time: 90.004879

Final Solution: 0->32, 1->0, 2->30, 3->34, 4->40, 5->35, 6->16, 7->34, 8->6, 9->8, 10->19, 11->26, 12->21, 13->40, 14->20, 15->24, 16->5, 17->47, 18->0, 19->1, 20->17, 21->47, 22->23, 23->40, 24->39, 25->16, 26->42, 27->47, 28->33, 29->14, 30->13, 31->5, 32->2, 33->46, 34->11, 35->5, 36->38, 37->3, 38->23, 39->13, 40->14, 41->36, 42->36, 43->33, 44->35, 45->4, 46->4, 47->41, 48->26, 49->3, 50->29, 51->37, 52->0, 53->41, 54->36, 55->12, 56->2, 57->44, 58->15, 59->34, 60->7, 61->43, 62->35, 63->17, 64->28, 65->13, 66->7, 67->25, 68->45, 69->1, 70->33, 71->22, 72->30, 73->43, 74->7, 75->18, 76->10, 77->8, 78->40, 79->30, 80->7, 81->31, 82->21, 83->29, 84->14, 85->9, 86->38, 87->6, 88->8, 89->8, 90->3, 91->25, 92->37, 93->36, 94->42, 95->0, 96->19, 97->5, 98->11, 99->46, 100->33, 101->38, 102->35, 103->2, 104->46, 105->45, 106->24, 107->37, 108->37, 109->40, 110->27, 111->19, 112->8, 113->36, 114->34, 115->9, 116->0, 117->22, 118->39, 119->4, 120->24, 121->7, 122->44, 123->11, 124->28, 125->8, 126->31, 127->14, 128->39, 129->44, 130->30, 131->18, 132->12, 133->12, 134->11, 135->44, 136->38, 137->10, 138->26, 139->13, 140->42, 141->15, 142->12, 143->25, 144->25, 145->30, 146->20, 147->16, 148->14, 149->38, 150->31, 151->42, 152->29, 153->28, 154->6, 155->21, 156->31, 157->15, 158->20, 159->41, 160->20, 161->13, 162->39, 163->32, 164->26, 165->13, 166->18, 167->14, 168->2, 169->29, 170->45, 171->15, 172->28, 173->29, 174->28, 175->35, 176->9, 177->15, 178->40, 179->41, 180->20, 181->32, 182->19, 183->44, 184->10, 185->12, 186->37, 187->35, 188->25, 189->43, 190->17, 191->10, 192->1, 193->23, 194->22, 195->25, 196->26, 197->19, 198->26, 199->1, 200->45, 201->21, 202->16, 203->28, 204->2, 205->41, 206->3, 207->18, 208->41, 209->1, 210->39, 211->21, 212->27, 213->37, 214->42, 215->31, 216->9, 217->44, 218->18, 219->11, 220->17, 221->9, 222->20, 223->10, 224->47, 225->24, 226->19, 227->33, 228->17, 229->6, 230->9, 231->4, 232->1, 233->5, 234->6, 235->24, 236->12, 237->27, 238->22, 239->30, 240->16, 241->38, 242->46, 243->18, 244->34, 245->32, 246->7, 247->47, 248->22, 249->5, 250->43, 251->3, 252->33, 253->27, 254->45, 255->27, 256->21, 257->29, 258->4, 259->23, 260->31, 261->22, 262->15, 263->23, 264->34, 265->0, 266->43, 267->24, 268->11, 269->43, 270->42, 271->36, 272->3, 273->45, 274->27, 275->17, 276->4, 277->16, 278->46, 279->47, 280->6, 281->10, 282->2, 283->46, 284->39, 285->23, 286->32, 287->32

Average CPI map:
	1.54,	3.26,	2.30,	2.06,	7.46,	0.79
	2.05,	7.12,	6.97,	6.74,	10.18,	6.40
	6.38,	6.22,	6.18,	6.00,	5.89,	5.85
	2.11,	16.25,	14.08,	2.25,	13.45,	5.03
	4.82,	10.43,	5.27,	5.10,	4.96,	2.06
	4.76,	4.70,	4.67,	1.92,	4.49,	4.47
	2.20,	11.34,	3.95,	3.74,	3.54,	3.51
	2.07,	3.41,	10.99,	2.79,	1.70,	2.53

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	1,	1,	0,	0,	1
	1,	1,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
	1,	0,	1,	0,	0,	1
