==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 7 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 101

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.009000
- lns branches: 13488
- lns fails: 6573
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/2.024
- branches (local/global): 29/13517
- fails (local/global): 1/6574
- lns time: 0.023000
- lns branches: 29
- lns fails: 1
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008000
- lns branches: 8410
- lns fails: 4113
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008000
- lns branches: 14409
- lns fails: 7044
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.055/6.103
- branches (local/global): 483/36819
- fails (local/global): 208/17939
- lns time: 0.063000
- lns branches: 483
- lns fails: 208
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 12836
- lns fails: 6248
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.100/8.219
- branches (local/global): 895/50550
- fails (local/global): 418/24605
- lns time: 0.108000
- lns branches: 895
- lns fails: 418
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.993/9.220
- branches (local/global): 13263/63813
- fails (local/global): 6405/31010
- lns time: 1.001000
- lns branches: 13263
- lns fails: 6405
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.089/9.317
- branches (local/global): 1093/64906
- fails (local/global): 502/31512
- lns time: 0.097000
- lns branches: 1093
- lns fails: 502
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 13753
- lns fails: 6661
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 15613
- lns fails: 7602
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 14844
- lns fails: 7250
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.106/15.456
- branches (local/global): 860/109976
- fails (local/global): 407/53432
- lns time: 0.113000
- lns branches: 860
- lns fails: 407
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007000
- lns branches: 15182
- lns fails: 7425
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007000
- lns branches: 10493
- lns fails: 5111
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 27127
- lns fails: 13164
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.010/21.495
- branches (local/global): 32/162810
- fails (local/global): 3/79135
- lns time: 0.021000
- lns branches: 32
- lns fails: 3
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.016000
- lns branches: 15153
- lns fails: 7380
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.066/23.588
- branches (local/global): 235/178198
- fails (local/global): 101/86616
- lns time: 0.087000
- lns branches: 235
- lns fails: 101
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.027000
- lns branches: 16438
- lns fails: 8060
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 15376
- lns fails: 7530
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 10427
- lns fails: 5105
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 29715
- lns fails: 14475
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 19378
- lns fails: 9401
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 16659
- lns fails: 8169
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 5336
- lns fails: 2584
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.817/38.500
- branches (local/global): 7092/298619
- fails (local/global): 3426/145366
- lns time: 0.824000
- lns branches: 7092
- lns fails: 3426
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 23313
- lns fails: 11380
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 11348
- lns fails: 5565
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 12053
- lns fails: 5881
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 10670
- lns fails: 5189
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.111/46.650
- branches (local/global): 1066/357069
- fails (local/global): 511/173892
- lns time: 0.118000
- lns branches: 1066
- lns fails: 511
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 9818
- lns fails: 4797
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 13348
- lns fails: 6527
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 15826
- lns fails: 7744
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.007000
- lns branches: 10123
- lns fails: 4971
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.009000
- lns branches: 10240
- lns fails: 4988
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.071/56.768
- branches (local/global): 1078/417502
- fails (local/global): 503/203422
- lns time: 0.079000
- lns branches: 1078
- lns fails: 503
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 28029
- lns fails: 13667
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 19839
- lns fails: 9663
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 6922
- lns fails: 3394
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 12848
- lns fails: 6316
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 23951
- lns fails: 11643
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 21480
- lns fails: 10526
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 23609
- lns fails: 11573
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.007/70.839
- branches (local/global): 38/554218
- fails (local/global): 6/270210
- lns time: 0.015000
- lns branches: 38
- lns fails: 6
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.009000
- lns branches: 13711
- lns fails: 6657
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.009000
- lns branches: 15974
- lns fails: 7779
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 10786
- lns fails: 5237
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 17624
- lns fails: 8601
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 15098
- lns fails: 7394
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 13008
- lns fails: 6337
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 17187
- lns fails: 8384
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.007000
- lns branches: 10191
- lns fails: 4892
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 19176
- lns fails: 9307
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 1.088000
- lns branches: 8738
- lns fails: 4256

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 18
final number of satisfied efficiency constraints: 29
number of iterations: 56
total time: 90008.000000
total branches: 695711
total fails: 339054

Final Solution: 0->20, 1->17, 2->9, 3->25, 4->31, 5->42, 6->34, 7->34, 8->22, 9->4, 10->44, 11->36, 12->30, 13->15, 14->27, 15->3, 16->31, 17->40, 18->45, 19->9, 20->22, 21->28, 22->37, 23->27, 24->35, 25->40, 26->27, 27->19, 28->8, 29->9, 30->5, 31->42, 32->29, 33->41, 34->33, 35->21, 36->20, 37->0, 38->0, 39->19, 40->13, 41->30, 42->7, 43->47, 44->9, 45->34, 46->31, 47->16, 48->6, 49->35, 50->32, 51->27, 52->40, 53->40, 54->14, 55->23, 56->13, 57->46, 58->7, 59->40, 60->15, 61->18, 62->35, 63->36, 64->46, 65->46, 66->17, 67->0, 68->18, 69->39, 70->18, 71->3, 72->15, 73->29, 74->46, 75->28, 76->45, 77->25, 78->21, 79->15, 80->33, 81->17, 82->21, 83->2, 84->36, 85->22, 86->20, 87->4, 88->19, 89->42, 90->1, 91->13, 92->6, 93->43, 94->5, 95->17, 96->20, 97->4, 98->10, 99->9, 100->47, 101->30, 102->23, 103->42, 104->28, 105->6, 106->4, 107->2, 108->35, 109->5, 110->45, 111->2, 112->29, 113->30, 114->31, 115->44, 116->11, 117->28, 118->8, 119->27, 120->32, 121->37, 122->12, 123->26, 124->26, 125->25, 126->25, 127->19, 128->20, 129->26, 130->19, 131->24, 132->4, 133->1, 134->39, 135->7, 136->8, 137->6, 138->13, 139->37, 140->46, 141->43, 142->26, 143->6, 144->39, 145->3, 146->36, 147->6, 148->32, 149->20, 150->34, 151->17, 152->43, 153->39, 154->28, 155->24, 156->21, 157->0, 158->38, 159->24, 160->8, 161->23, 162->14, 163->29, 164->21, 165->42, 166->41, 167->5, 168->9, 169->30, 170->10, 171->4, 172->45, 173->31, 174->12, 175->47, 176->14, 177->30, 178->15, 179->43, 180->37, 181->33, 182->21, 183->35, 184->8, 185->5, 186->44, 187->42, 188->13, 189->0, 190->38, 191->14, 192->3, 193->10, 194->33, 195->27, 196->34, 197->47, 198->33, 199->11, 200->10, 201->11, 202->14, 203->13, 204->1, 205->41, 206->5, 207->28, 208->24, 209->12, 210->36, 211->1, 212->22, 213->24, 214->22, 215->35, 216->38, 217->44, 218->12, 219->26, 220->16, 221->22, 222->2, 223->11, 224->34, 225->16, 226->19, 227->39, 228->10, 229->47, 230->26, 231->16, 232->37, 233->23, 234->7, 235->23, 236->18, 237->25, 238->41, 239->11, 240->16, 241->44, 242->38, 243->7, 244->14, 245->2, 246->12, 247->10, 248->47, 249->25, 250->3, 251->29, 252->1, 253->15, 254->37, 255->41, 256->33, 257->12, 258->38, 259->32, 260->18, 261->44, 262->32, 263->39, 264->29, 265->43, 266->16, 267->0, 268->36, 269->7, 270->32, 271->2, 272->45, 273->46, 274->31, 275->11, 276->17, 277->38, 278->43, 279->24, 280->1, 281->45, 282->3, 283->18, 284->41, 285->23, 286->8, 287->40

Average CPI map:
	1.41,	1.63,	2.04,	2.08,	2.00,	0.97
	3.87,	3.41,	4.70,	2.77,	2.19,	2.09
	2.06,	3.28,	3.27,	3.04,	1.73,	2.03
	2.54,	2.70,	1.00,	2.68,	2.45,	3.18
	2.01,	1.38,	2.30,	11.25,	2.27,	2.01
	2.29,	1.34,	2.72,	2.20,	1.22,	4.23
	2.49,	2.17,	1.71,	2.17,	7.36,	2.06
	2.19,	2.82,	3.66,	4.95,	7.75,	2.34

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	1,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	1,	1,	1,	1,	1
