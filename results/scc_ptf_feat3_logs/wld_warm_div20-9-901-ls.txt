LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
4.72634, 0.80643, 0.600421, 1.23093, 2.15829, 2.47759, 3.25176, 2.58534, 1.64973, 2.0288, 1.88653, 0.597842, 1.91945, 1.94999, 2.05017, 1.85901, 2.36346, 1.85793, 2.62763, 2.40449, 2.67466, 0.5, 1.80273, 1.99049, 2.14438, 2.14645, 2.18921, 1.95719, 1.9618, 1.60097, 2.16001, 2.59673, 2.04742, 0.800318, 2.34858, 2.04694, 1.76921, 1.80511, 2.51634, 2.22796, 2.43494, 1.24643, 0.85042, 3.01765, 3.33737, 2.13918, 0.686134, 1.96925, 2.62422, 0.758929, 3.07115, 3.19422, 1.85148, 0.5, 3.25117, 0.5, 2.13828, 2.05464, 0.871231, 3.18467, 0.5, 4.79118, 2.3423, 1.0173, 0.921258, 2.42797, 2.31006, 0.5, 0.900488, 2.03999, 1.26812, 4.98134, 1.20626, 1.02555, 2.49961, 3.90973, 0.5, 2.85885, 3.30362, 1.02483, 4.52109, 1.30459, 1.34587, 0.5, 24.6396, 13.1308, 29.8871, 35, 11.4149, 17.9277, 35, 17.8207, 12.66, 17.2125, 20.6874, 28.6195, 1.5002, 2.785, 2.02919, 0.5, 2.24496, 2.94065, 0.5, 2.26738, 2.18001, 3.9793, 2.06294, 1.01036, 2.04474, 3.11418, 2.55831, 0.817484, 2.63524, 0.830046, 2.68802, 2.26018, 1.61054, 2.75781, 1.61053, 1.07294, 9.13066, 8.39748, 33.7156, 22.5257, 35, 23.2305, 3.46287, 1.9553, 0.992387, 0.5, 2.10496, 2.98448, 7.55319, 34.9071, 26.2854, 16.0863, 35, 12.168, 6.05281, 1.9006, 0.5, 1.33709, 0.978716, 1.23078, 1.41619, 2.43974, 0.5, 2.25508, 2.44977, 2.93921, 2.64417, 3.45957, 3.78676, 0.693863, 0.5, 0.915637, 26.8552, 16.3454, 35, 16.1199, 19.8227, 17.8569, 31.4681, 6.23997, 11.1457, 30.1129, 28.0174, 25.016, 23.7705, 22.3787, 15.8532, 24.7516, 24.2344, 21.0116, 1.42575, 3.09505, 2.82666, 0.5, 3.251, 0.901542, 3.97766, 0.5, 2.30717, 3.18453, 0.871821, 1.15882, 0.5, 2.72857, 3.21968, 1.24734, 2.21887, 2.08554, 1.81881, 2.13962, 2.25394, 1.93487, 1.76174, 2.09102, 2.15055, 5.67218, 0.985286, 1.26879, 1.05214, 0.871061, 1.16524, 3.72741, 4.03229, 0.946942, 0.72869, 1.39942, 2.26744, 2.29021, 1.91601, 1.80106, 1.47367, 2.2516, 1.92586, 2.21324, 1.73742, 1.86003, 2.05943, 2.20403, 2.09886, 1.98844, 1.88776, 1.71861, 2.16756, 2.13876, 2.69924, 1.5634, 2.49374, 1.96284, 2.78078, 0.5, 2.3516, 1.66845, 0.5, 1.39727, 2.45327, 3.62942, 2.14902, 1.80381, 1.75116, 2.42663, 0.5, 3.36939, 3.76519, 1.47279, 2.47116, 0.5, 2.89601, 0.894846, 1.82126, 2.34338, 3.42483, 0.888116, 1.28338, 2.23904, 3.35174, 0.527017, 0.989456, 1.61285, 2.28852, 3.23043, 2.36367, 2.37168, 1.78575, 1.28309, 2.05796, 2.13786, 2.33386, 2.13722, 1.05973, 2.2291, 2.35209, 1.888, 1.40055, 2.12991, 2.06404, 2.33329, 1.94813, 2.12407, 1.29112, 2.82369, 1.55113, 1.56719, 2.86181, 1.90506
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54718, operands = 135660
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 901, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 7874 itr]: obj = (27), mov = 7874, inf = 64.1%, acc = 27.5%, imp = 148

number of satisfied efficiency constraints: 27
[2 sec, 41136 itr]: obj = (29), mov = 41136, inf = 70.7%, acc = 16.9%, imp = 150

number of satisfied efficiency constraints: 29
[3 sec, 61013 itr]: obj = (30), mov = 61013, inf = 63.6%, acc = 19.9%, imp = 151

number of satisfied efficiency constraints: 30
[4 sec, 76446 itr]: obj = (30), mov = 76446, inf = 57.2%, acc = 22.6%, imp = 151

number of satisfied efficiency constraints: 30
[5 sec, 93322 itr]: obj = (30), mov = 93322, inf = 51.3%, acc = 24.8%, imp = 151

number of satisfied efficiency constraints: 30
[6 sec, 112268 itr]: obj = (32), mov = 112268, inf = 46%, acc = 26.2%, imp = 153

number of satisfied efficiency constraints: 32
[7 sec, 131111 itr]: obj = (32), mov = 131111, inf = 42%, acc = 27.3%, imp = 183

number of satisfied efficiency constraints: 32
[8 sec, 146792 itr]: obj = (32), mov = 146792, inf = 39.4%, acc = 28.9%, imp = 189

number of satisfied efficiency constraints: 32
[9 sec, 163845 itr]: obj = (32), mov = 163845, inf = 36.8%, acc = 29.5%, imp = 190

number of satisfied efficiency constraints: 32
[10 sec, 181518 itr]: obj = (32), mov = 181518, inf = 34.7%, acc = 29.9%, imp = 190

number of satisfied efficiency constraints: 32
[11 sec, 199225 itr]: obj = (32), mov = 199225, inf = 33%, acc = 30.2%, imp = 190

number of satisfied efficiency constraints: 32
[12 sec, 217357 itr]: obj = (32), mov = 217357, inf = 31.5%, acc = 30.4%, imp = 190

number of satisfied efficiency constraints: 32
[13 sec, 235603 itr]: obj = (32), mov = 235603, inf = 30.2%, acc = 30.5%, imp = 191

number of satisfied efficiency constraints: 32
[14 sec, 255157 itr]: obj = (33), mov = 255157, inf = 29.1%, acc = 30.4%, imp = 192

number of satisfied efficiency constraints: 33
[15 sec, 274244 itr]: obj = (33), mov = 274244, inf = 28.1%, acc = 30.3%, imp = 192

number of satisfied efficiency constraints: 33
[16 sec, 293762 itr]: obj = (33), mov = 293762, inf = 27.2%, acc = 30.3%, imp = 192

number of satisfied efficiency constraints: 33
[17 sec, 313894 itr]: obj = (33), mov = 313894, inf = 26.4%, acc = 30.1%, imp = 192

number of satisfied efficiency constraints: 33
[18 sec, 334185 itr]: obj = (33), mov = 334185, inf = 25.7%, acc = 30%, imp = 192

number of satisfied efficiency constraints: 33
[19 sec, 354301 itr]: obj = (33), mov = 354301, inf = 25.1%, acc = 29.9%, imp = 192

number of satisfied efficiency constraints: 33
[20 sec, 373464 itr]: obj = (33), mov = 373464, inf = 24.6%, acc = 29.9%, imp = 192

number of satisfied efficiency constraints: 33
[21 sec, 392796 itr]: obj = (33), mov = 392796, inf = 24.1%, acc = 29.8%, imp = 192

number of satisfied efficiency constraints: 33
[22 sec, 413169 itr]: obj = (33), mov = 413169, inf = 23.7%, acc = 29.7%, imp = 192

number of satisfied efficiency constraints: 33
[23 sec, 433098 itr]: obj = (33), mov = 433098, inf = 23.3%, acc = 29.7%, imp = 192

number of satisfied efficiency constraints: 33
[24 sec, 453517 itr]: obj = (33), mov = 453517, inf = 22.9%, acc = 29.6%, imp = 192

number of satisfied efficiency constraints: 33
[25 sec, 473573 itr]: obj = (33), mov = 473573, inf = 22.6%, acc = 29.6%, imp = 192

number of satisfied efficiency constraints: 33
[26 sec, 493161 itr]: obj = (33), mov = 493161, inf = 22.3%, acc = 29.6%, imp = 192

number of satisfied efficiency constraints: 33
[27 sec, 511380 itr]: obj = (33), mov = 511380, inf = 22%, acc = 29.7%, imp = 221

number of satisfied efficiency constraints: 33
[28 sec, 529067 itr]: obj = (33), mov = 529067, inf = 21.8%, acc = 29.8%, imp = 222

number of satisfied efficiency constraints: 33
[29 sec, 546940 itr]: obj = (33), mov = 546940, inf = 21.5%, acc = 29.9%, imp = 223

number of satisfied efficiency constraints: 33
[30 sec, 565710 itr]: obj = (33), mov = 565710, inf = 21.3%, acc = 29.9%, imp = 223

number of satisfied efficiency constraints: 33
[31 sec, 583846 itr]: obj = (33), mov = 583846, inf = 21.1%, acc = 29.9%, imp = 223

number of satisfied efficiency constraints: 33
[32 sec, 601618 itr]: obj = (33), mov = 601618, inf = 20.9%, acc = 30%, imp = 224

number of satisfied efficiency constraints: 33
[33 sec, 620480 itr]: obj = (33), mov = 620480, inf = 20.7%, acc = 30%, imp = 224

number of satisfied efficiency constraints: 33
[34 sec, 640163 itr]: obj = (33), mov = 640163, inf = 20.5%, acc = 29.9%, imp = 224

number of satisfied efficiency constraints: 33
[35 sec, 659969 itr]: obj = (33), mov = 659969, inf = 20.3%, acc = 29.8%, imp = 224

number of satisfied efficiency constraints: 33
[36 sec, 679418 itr]: obj = (33), mov = 679418, inf = 20.2%, acc = 29.7%, imp = 224

number of satisfied efficiency constraints: 33
[37 sec, 698805 itr]: obj = (33), mov = 698805, inf = 20%, acc = 29.7%, imp = 224

number of satisfied efficiency constraints: 33
[38 sec, 717279 itr]: obj = (33), mov = 717279, inf = 19.8%, acc = 29.6%, imp = 224

number of satisfied efficiency constraints: 33
[39 sec, 735926 itr]: obj = (33), mov = 735926, inf = 19.7%, acc = 29.6%, imp = 224

number of satisfied efficiency constraints: 33
[40 sec, 755300 itr]: obj = (33), mov = 755300, inf = 19.6%, acc = 29.6%, imp = 224

number of satisfied efficiency constraints: 33
[41 sec, 773674 itr]: obj = (33), mov = 773674, inf = 19.5%, acc = 29.6%, imp = 224

number of satisfied efficiency constraints: 33
[42 sec, 793123 itr]: obj = (33), mov = 793123, inf = 19.3%, acc = 29.5%, imp = 224

number of satisfied efficiency constraints: 33
[43 sec, 812306 itr]: obj = (33), mov = 812306, inf = 19.2%, acc = 29.5%, imp = 224

number of satisfied efficiency constraints: 33
[44 sec, 828381 itr]: obj = (33), mov = 828381, inf = 19.1%, acc = 29.7%, imp = 260

number of satisfied efficiency constraints: 33
[45 sec, 845859 itr]: obj = (33), mov = 845859, inf = 19%, acc = 29.8%, imp = 260

number of satisfied efficiency constraints: 33
[46 sec, 863194 itr]: obj = (33), mov = 863194, inf = 18.9%, acc = 29.9%, imp = 260

number of satisfied efficiency constraints: 33
[47 sec, 880194 itr]: obj = (33), mov = 880194, inf = 18.8%, acc = 30%, imp = 260

number of satisfied efficiency constraints: 33
[48 sec, 897280 itr]: obj = (33), mov = 897280, inf = 18.7%, acc = 30.1%, imp = 260

number of satisfied efficiency constraints: 33
[49 sec, 914546 itr]: obj = (33), mov = 914546, inf = 18.6%, acc = 30.2%, imp = 260

number of satisfied efficiency constraints: 33
[50 sec, 930403 itr]: obj = (33), mov = 930403, inf = 18.5%, acc = 30.3%, imp = 285

number of satisfied efficiency constraints: 33
[51 sec, 945797 itr]: obj = (33), mov = 945797, inf = 18.4%, acc = 30.5%, imp = 288

number of satisfied efficiency constraints: 33
[52 sec, 962701 itr]: obj = (33), mov = 962701, inf = 18.4%, acc = 30.7%, imp = 288

number of satisfied efficiency constraints: 33
[53 sec, 980127 itr]: obj = (33), mov = 980127, inf = 18.3%, acc = 30.8%, imp = 289

number of satisfied efficiency constraints: 33
[54 sec, 997532 itr]: obj = (33), mov = 997532, inf = 18.2%, acc = 30.9%, imp = 289

number of satisfied efficiency constraints: 33
[55 sec, 1014828 itr]: obj = (33), mov = 1014828, inf = 18.1%, acc = 31%, imp = 289

number of satisfied efficiency constraints: 33
[56 sec, 1032911 itr]: obj = (33), mov = 1032911, inf = 18%, acc = 31.1%, imp = 290

number of satisfied efficiency constraints: 33
[57 sec, 1051835 itr]: obj = (33), mov = 1051835, inf = 17.9%, acc = 31.1%, imp = 290

number of satisfied efficiency constraints: 33
[58 sec, 1070643 itr]: obj = (33), mov = 1070643, inf = 17.9%, acc = 31.2%, imp = 290

number of satisfied efficiency constraints: 33
[59 sec, 1089851 itr]: obj = (33), mov = 1089851, inf = 17.8%, acc = 31.2%, imp = 291

number of satisfied efficiency constraints: 33
[60 sec, 1109787 itr]: obj = (33), mov = 1109787, inf = 17.7%, acc = 31.2%, imp = 291

number of satisfied efficiency constraints: 33
[61 sec, 1129279 itr]: obj = (33), mov = 1129279, inf = 17.6%, acc = 31.2%, imp = 291

number of satisfied efficiency constraints: 33
[62 sec, 1150033 itr]: obj = (33), mov = 1150033, inf = 17.5%, acc = 31.1%, imp = 291

number of satisfied efficiency constraints: 33
[63 sec, 1170316 itr]: obj = (34), mov = 1170316, inf = 17.5%, acc = 31.1%, imp = 292

number of satisfied efficiency constraints: 34
[64 sec, 1190816 itr]: obj = (34), mov = 1190816, inf = 17.4%, acc = 31%, imp = 292

number of satisfied efficiency constraints: 34
[65 sec, 1211952 itr]: obj = (34), mov = 1211952, inf = 17.3%, acc = 31%, imp = 292

number of satisfied efficiency constraints: 34
[66 sec, 1232776 itr]: obj = (34), mov = 1232776, inf = 17.2%, acc = 30.9%, imp = 292

number of satisfied efficiency constraints: 34
[67 sec, 1252921 itr]: obj = (34), mov = 1252921, inf = 17.2%, acc = 30.8%, imp = 292

number of satisfied efficiency constraints: 34
[68 sec, 1273174 itr]: obj = (34), mov = 1273174, inf = 17.1%, acc = 30.8%, imp = 292

number of satisfied efficiency constraints: 34
[69 sec, 1295255 itr]: obj = (35), mov = 1295255, inf = 17.1%, acc = 30.7%, imp = 293

number of satisfied efficiency constraints: 35
[70 sec, 1317183 itr]: obj = (35), mov = 1317183, inf = 17%, acc = 30.6%, imp = 293

number of satisfied efficiency constraints: 35
[71 sec, 1334267 itr]: obj = (35), mov = 1334267, inf = 17%, acc = 30.6%, imp = 293

number of satisfied efficiency constraints: 35
[72 sec, 1355679 itr]: obj = (35), mov = 1355679, inf = 16.9%, acc = 30.5%, imp = 293

number of satisfied efficiency constraints: 35
[73 sec, 1377778 itr]: obj = (35), mov = 1377778, inf = 16.9%, acc = 30.4%, imp = 293

number of satisfied efficiency constraints: 35
[74 sec, 1399152 itr]: obj = (35), mov = 1399152, inf = 16.8%, acc = 30.3%, imp = 293

number of satisfied efficiency constraints: 35
[75 sec, 1420934 itr]: obj = (35), mov = 1420934, inf = 16.8%, acc = 30.2%, imp = 293

number of satisfied efficiency constraints: 35
[76 sec, 1443193 itr]: obj = (35), mov = 1443193, inf = 16.7%, acc = 30.1%, imp = 293

number of satisfied efficiency constraints: 35
[77 sec, 1465578 itr]: obj = (35), mov = 1465578, inf = 16.7%, acc = 30.1%, imp = 293

number of satisfied efficiency constraints: 35
[78 sec, 1487914 itr]: obj = (35), mov = 1487914, inf = 16.6%, acc = 30%, imp = 293

number of satisfied efficiency constraints: 35
[79 sec, 1509392 itr]: obj = (35), mov = 1509392, inf = 16.6%, acc = 29.9%, imp = 293

number of satisfied efficiency constraints: 35
[80 sec, 1531430 itr]: obj = (35), mov = 1531430, inf = 16.6%, acc = 29.9%, imp = 293

number of satisfied efficiency constraints: 35
[81 sec, 1552968 itr]: obj = (35), mov = 1552968, inf = 16.5%, acc = 29.8%, imp = 293

number of satisfied efficiency constraints: 35
[82 sec, 1574528 itr]: obj = (35), mov = 1574528, inf = 16.5%, acc = 29.7%, imp = 293

number of satisfied efficiency constraints: 35
[83 sec, 1596516 itr]: obj = (35), mov = 1596516, inf = 16.4%, acc = 29.7%, imp = 293

number of satisfied efficiency constraints: 35
[84 sec, 1618434 itr]: obj = (35), mov = 1618434, inf = 16.4%, acc = 29.6%, imp = 293

number of satisfied efficiency constraints: 35
[85 sec, 1640327 itr]: obj = (35), mov = 1640327, inf = 16.4%, acc = 29.5%, imp = 293

number of satisfied efficiency constraints: 35
[86 sec, 1661971 itr]: obj = (35), mov = 1661971, inf = 16.3%, acc = 29.5%, imp = 293

number of satisfied efficiency constraints: 35
[87 sec, 1683439 itr]: obj = (35), mov = 1683439, inf = 16.3%, acc = 29.4%, imp = 293

number of satisfied efficiency constraints: 35
[88 sec, 1705815 itr]: obj = (35), mov = 1705815, inf = 16.3%, acc = 29.4%, imp = 293

number of satisfied efficiency constraints: 35
[89 sec, 1727964 itr]: obj = (35), mov = 1727964, inf = 16.3%, acc = 29.3%, imp = 293

number of satisfied efficiency constraints: 35
[90 sec, 1750113 itr]: obj = (35), mov = 1750113, inf = 16.2%, acc = 29.3%, imp = 293

number of satisfied efficiency constraints: 35
[90 sec, 1750113 itr]: obj = (35), mov = 1750113, inf = 16.2%, acc = 29.3%, imp = 293

number of satisfied efficiency constraints: 35
1750113 iterations, 1750113 moves performed in 90 seconds
Feasible solution: obj = (35)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 35
Mapping: 0->27, 1->33, 2->44, 3->15, 4->9, 5->24, 6->20, 7->8, 8->23, 9->10, 10->13, 11->31, 12->7, 13->4, 14->19, 15->21, 16->35, 17->23, 18->28, 19->40, 20->25, 21->14, 22->12, 23->44, 24->34, 25->19, 26->2, 27->37, 28->32, 29->13, 30->19, 31->30, 32->4, 33->14, 34->18, 35->13, 36->15, 37->32, 38->0, 39->8, 40->43, 41->47, 42->33, 43->4, 44->40, 45->8, 46->26, 47->5, 48->28, 49->41, 50->29, 51->6, 52->15, 53->35, 54->16, 55->14, 56->21, 57->12, 58->26, 59->8, 60->17, 61->39, 62->11, 63->24, 64->21, 65->3, 66->36, 67->11, 68->3, 69->6, 70->30, 71->33, 72->47, 73->13, 74->30, 75->1, 76->20, 77->43, 78->3, 79->42, 80->44, 81->26, 82->42, 83->31, 84->16, 85->34, 86->39, 87->9, 88->39, 89->11, 90->34, 91->23, 92->17, 93->40, 94->40, 95->41, 96->19, 97->2, 98->21, 99->5, 100->46, 101->22, 102->15, 103->22, 104->23, 105->7, 106->46, 107->23, 108->17, 109->27, 110->22, 111->20, 112->31, 113->41, 114->18, 115->29, 116->4, 117->7, 118->19, 119->6, 120->34, 121->25, 122->29, 123->27, 124->25, 125->47, 126->28, 127->4, 128->5, 129->44, 130->3, 131->18, 132->42, 133->16, 134->12, 135->9, 136->25, 137->39, 138->40, 139->46, 140->32, 141->26, 142->38, 143->26, 144->33, 145->7, 146->17, 147->16, 148->42, 149->10, 150->10, 151->41, 152->43, 153->11, 154->42, 155->29, 156->45, 157->28, 158->45, 159->27, 160->45, 161->28, 162->28, 163->35, 164->1, 165->37, 166->40, 167->39, 168->45, 169->37, 170->0, 171->27, 172->24, 173->37, 174->36, 175->6, 176->45, 177->2, 178->25, 179->20, 180->22, 181->13, 182->2, 183->22, 184->0, 185->1, 186->22, 187->37, 188->2, 189->47, 190->10, 191->15, 192->14, 193->4, 194->6, 195->46, 196->38, 197->5, 198->16, 199->7, 200->24, 201->1, 202->11, 203->41, 204->1, 205->36, 206->9, 207->18, 208->0, 209->33, 210->35, 211->44, 212->11, 213->13, 214->3, 215->38, 216->46, 217->37, 218->32, 219->9, 220->16, 221->45, 222->21, 223->2, 224->32, 225->20, 226->34, 227->32, 228->24, 229->35, 230->38, 231->10, 232->36, 233->12, 234->21, 235->6, 236->26, 237->17, 238->36, 239->34, 240->8, 241->41, 242->0, 243->18, 244->31, 245->3, 246->27, 247->35, 248->44, 249->14, 250->43, 251->36, 252->24, 253->14, 254->30, 255->17, 256->38, 257->43, 258->9, 259->12, 260->33, 261->5, 262->7, 263->12, 264->46, 265->5, 266->20, 267->0, 268->30, 269->29, 270->15, 271->8, 272->42, 273->10, 274->43, 275->38, 276->18, 277->1, 278->31, 279->29, 280->23, 281->30, 282->31, 283->39, 284->19, 285->47, 286->25, 287->47
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	3.83405	3.46303	2.16492	2.26335	2.12009	1.58953
	2.22077	3.17617	2.4039	10.4148	2.33717	4.07199
	5.73337	1.47684	1.07708	1.6285	11.5438	2.99835
	2.13253	1.83641	1.49586	1.8997	2.57142	4.41114
	5.53917	14.5308	0.988303	12.5005	12.3975	7.40562
	2.32855	1.26471	1.67181	1.74054	10.8671	2.40118
	2.26535	13.4004	1.7762	14.3501	12.952	6.05715
	2.32223	2.76128	2.06223	18.4132	2.07215	5.06713
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	1	0	1	1	1
	1	0	0	0	1	1
	1	1	0	1	1	1
	1	1	0	1	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 9 --global-tlim 90 --seed 901

Starting the Optimization Process
