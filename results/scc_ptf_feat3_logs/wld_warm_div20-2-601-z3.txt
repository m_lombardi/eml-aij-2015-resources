==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 2 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 601

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007190
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004430
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.497/5.509
- lns time: 1.497540
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.334/6.843
- lns time: 1.333726
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004639
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.670/9.517
- lns time: 0.670037
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.011429
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006135
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007419
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006531
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.015375
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008382
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005423
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.334/24.912
- lns time: 1.334370
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 0.937671
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005445
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009671
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005608
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008708
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.125297
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010921
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006739
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007240
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 0.614300
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.701/43.345
- lns time: 1.700897
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006183
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009736
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.003956
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008859
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.011777
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006338
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008134
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 1.267723
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007157
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007145
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007789
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.010894
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007256
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006284
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 1.313009
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006941
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 0.318471
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007819
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.010047
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005741
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005913
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005601
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009129
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005568
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 1.608323
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 24

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 24
number of iterations: 50
total time: 90.010916

Final Solution: 0->2, 1->33, 2->25, 3->42, 4->35, 5->13, 6->14, 7->24, 8->3, 9->35, 10->8, 11->24, 12->9, 13->3, 14->9, 15->26, 16->35, 17->2, 18->1, 19->24, 20->7, 21->29, 22->34, 23->42, 24->39, 25->43, 26->44, 27->18, 28->46, 29->31, 30->22, 31->40, 32->41, 33->44, 34->9, 35->28, 36->39, 37->3, 38->6, 39->29, 40->17, 41->43, 42->41, 43->2, 44->20, 45->10, 46->38, 47->5, 48->2, 49->25, 50->41, 51->8, 52->37, 53->13, 54->4, 55->6, 56->7, 57->9, 58->26, 59->1, 60->3, 61->8, 62->16, 63->6, 64->32, 65->27, 66->27, 67->21, 68->28, 69->19, 70->5, 71->21, 72->37, 73->28, 74->36, 75->24, 76->46, 77->23, 78->9, 79->4, 80->6, 81->46, 82->33, 83->11, 84->17, 85->45, 86->26, 87->31, 88->12, 89->20, 90->4, 91->22, 92->11, 93->26, 94->46, 95->4, 96->16, 97->11, 98->22, 99->42, 100->38, 101->5, 102->44, 103->16, 104->45, 105->32, 106->17, 107->15, 108->32, 109->5, 110->26, 111->20, 112->39, 113->39, 114->1, 115->30, 116->41, 117->5, 118->3, 119->15, 120->28, 121->25, 122->1, 123->11, 124->18, 125->8, 126->17, 127->2, 128->30, 129->17, 130->24, 131->13, 132->36, 133->37, 134->46, 135->45, 136->21, 137->47, 138->12, 139->7, 140->44, 141->36, 142->14, 143->34, 144->33, 145->4, 146->30, 147->29, 148->47, 149->0, 150->12, 151->29, 152->34, 153->39, 154->20, 155->27, 156->30, 157->27, 158->37, 159->4, 160->28, 161->20, 162->1, 163->40, 164->12, 165->19, 166->14, 167->35, 168->12, 169->20, 170->35, 171->27, 172->23, 173->30, 174->40, 175->19, 176->12, 177->7, 178->18, 179->42, 180->13, 181->3, 182->18, 183->44, 184->16, 185->23, 186->33, 187->8, 188->0, 189->11, 190->37, 191->27, 192->22, 193->10, 194->46, 195->7, 196->31, 197->19, 198->33, 199->47, 200->10, 201->26, 202->43, 203->36, 204->6, 205->45, 206->34, 207->13, 208->8, 209->36, 210->23, 211->35, 212->24, 213->38, 214->14, 215->10, 216->15, 217->25, 218->32, 219->43, 220->23, 221->15, 222->32, 223->25, 224->47, 225->31, 226->40, 227->41, 228->17, 229->21, 230->10, 231->47, 232->38, 233->34, 234->33, 235->16, 236->13, 237->34, 238->1, 239->0, 240->43, 241->42, 242->40, 243->37, 244->6, 245->29, 246->14, 247->5, 248->0, 249->23, 250->30, 251->15, 252->32, 253->21, 254->14, 255->36, 256->38, 257->15, 258->42, 259->18, 260->11, 261->18, 262->45, 263->38, 264->10, 265->28, 266->22, 267->44, 268->16, 269->22, 270->7, 271->31, 272->40, 273->19, 274->43, 275->25, 276->29, 277->2, 278->0, 279->47, 280->0, 281->45, 282->31, 283->21, 284->39, 285->41, 286->19, 287->9

Average CPI map:
	1.72,	6.63,	6.02,	5.58,	2.96,	0.71
	7.46,	3.90,	2.76,	11.82,	2.55,	1.36
	2.28,	2.26,	2.23,	2.20,	2.18,	2.15
	2.14,	2.14,	2.12,	1.45,	2.11,	2.09
	2.05,	2.04,	8.24,	2.02,	2.02,	2.25
	2.02,	2.03,	2.03,	2.04,	2.04,	3.90
	2.03,	2.03,	2.04,	2.04,	2.03,	2.02
	2.04,	2.16,	2.03,	2.03,	2.03,	2.03

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
