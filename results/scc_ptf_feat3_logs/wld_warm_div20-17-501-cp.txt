==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 17 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 501

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.005/0.005
- branches (local/global): 29/29
- fails (local/global): 2/2
- lns time: 0.012000
- lns branches: 29
- lns fails: 2
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.009/0.021
- branches (local/global): 25/54
- fails (local/global): 2/4
- lns time: 0.018000
- lns branches: 25
- lns fails: 2
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.020/0.050
- branches (local/global): 248/302
- fails (local/global): 101/105
- lns time: 0.028000
- lns branches: 248
- lns fails: 101
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.016/0.074
- branches (local/global): 45/347
- fails (local/global): 8/113
- lns time: 0.024000
- lns branches: 45
- lns fails: 8
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.009/0.091
- branches (local/global): 42/389
- fails (local/global): 9/122
- lns time: 0.017000
- lns branches: 42
- lns fails: 9
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.024/0.123
- branches (local/global): 29/418
- fails (local/global): 2/124
- lns time: 0.033000
- lns branches: 29
- lns fails: 2
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.041/0.173
- branches (local/global): 255/673
- fails (local/global): 105/229
- lns time: 0.049000
- lns branches: 255
- lns fails: 105
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.208/0.389
- branches (local/global): 3794/4467
- fails (local/global): 1796/2025
- lns time: 0.217000
- lns branches: 3794
- lns fails: 1796
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.074/0.472
- branches (local/global): 950/5417
- fails (local/global): 433/2458
- lns time: 0.081000
- lns branches: 950
- lns fails: 433
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.009000
- lns branches: 15043
- lns fails: 7418
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.009/2.497
- branches (local/global): 27/20487
- fails (local/global): 0/9876
- lns time: 0.017000
- lns branches: 27
- lns fails: 0
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.014/2.519
- branches (local/global): 27/20514
- fails (local/global): 1/9877
- lns time: 0.022000
- lns branches: 27
- lns fails: 1
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.270/2.797
- branches (local/global): 3292/23806
- fails (local/global): 1571/11448
- lns time: 0.278000
- lns branches: 3292
- lns fails: 1571
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.031/2.836
- branches (local/global): 61/23867
- fails (local/global): 18/11466
- lns time: 0.039000
- lns branches: 61
- lns fails: 18
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 12786
- lns fails: 6166
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 17936
- lns fails: 8728
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 27875
- lns fails: 13449
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/8.883
- branches (local/global): 79/82543
- fails (local/global): 28/39837
- lns time: 0.023000
- lns branches: 79
- lns fails: 28
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.008000
- lns branches: 10713
- lns fails: 5245
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.014000
- lns branches: 32714
- lns fails: 15841
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 0.939000
- lns branches: 6233
- lns fails: 3091
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.008000
- lns branches: 13242
- lns fails: 6485
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.008000
- lns branches: 14535
- lns fails: 7213
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.057/17.925
- branches (local/global): 308/160288
- fails (local/global): 136/77848
- lns time: 0.066000
- lns branches: 308
- lns fails: 136
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 35
- lower bound 3
- lns time: 0.387000
- lns branches: 2156
- lns fails: 1062
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 35
- lower bound 3
- lns time: 2.008000
- lns branches: 47035
- lns fails: 22758
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 35
- lower bound 3
- lns time: 2.008000
- lns branches: 50676
- lns fails: 24463
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 35
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.148/22.485
- branches (local/global): 1099/261254
- fails (local/global): 520/126651
- lns time: 0.155000
- lns branches: 1099
- lns fails: 520
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
- lns time: 2.008000
- lns branches: 14754
- lns fails: 7257
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
- lns time: 2.007000
- lns branches: 19928
- lns fails: 9670
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.060/26.567
- branches (local/global): 500/296436
- fails (local/global): 218/143796
- lns time: 0.068000
- lns branches: 500
- lns fails: 218
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.009000
- lns branches: 11670
- lns fails: 5711
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 25772
- lns fails: 12685
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.009000
- lns branches: 20085
- lns fails: 9831
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 24735
- lns fails: 12083
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 19873
- lns fails: 9737
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 22845
- lns fails: 11106
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.007000
- lns branches: 26556
- lns fails: 12997
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 15118
- lns fails: 7372
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 16035
- lns fails: 7876
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 16655
- lns fails: 8197
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 36395
- lns fails: 17772
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 22049
- lns fails: 10712
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 0.957000
- lns branches: 6247
- lns fails: 3100
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 21351
- lns fails: 10567
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.386/54.023
- branches (local/global): 4138/585960
- fails (local/global): 1970/285512
- lns time: 0.394000
- lns branches: 4138
- lns fails: 1970
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 27854
- lns fails: 13488
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.646000
- lns branches: 5684
- lns fails: 2821
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 15211
- lns fails: 7460
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.007000
- lns branches: 16306
- lns fails: 8019
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.124000
- lns branches: 729
- lns fails: 363
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 23842
- lns fails: 11726
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 14280
- lns fails: 7011
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 23322
- lns fails: 11401
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 22683
- lns fails: 11082
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 13519
- lns fails: 6652
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 12543
- lns fails: 6213
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 12235
- lns fails: 6009
LNS iteration #58 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.934000
- lns branches: 5918
- lns fails: 2930
LNS iteration #59 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 16428
- lns fails: 8001
LNS iteration #60 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 1.940000
- lns branches: 15114
- lns fails: 7499
LNS iteration #61 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 25532
- lns fails: 12562
LNS iteration #62 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 12418
- lns fails: 6057
LNS iteration #63 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 29521
- lns fails: 14384
LNS iteration #64 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.011000
- lns branches: 11832
- lns fails: 5876
LNS iteration #65 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 19431
- lns fails: 9436
LNS iteration #66 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.196000
- lns branches: 2129
- lns fails: 1047

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 38
number of iterations: 67
total time: 90008.000000
total branches: 912491
total fails: 445549

Final Solution: 0->39, 1->28, 2->18, 3->23, 4->31, 5->33, 6->39, 7->1, 8->29, 9->28, 10->26, 11->46, 12->14, 13->34, 14->33, 15->11, 16->12, 17->11, 18->14, 19->18, 20->4, 21->23, 22->12, 23->13, 24->35, 25->41, 26->29, 27->30, 28->41, 29->1, 30->34, 31->10, 32->6, 33->3, 34->7, 35->22, 36->14, 37->29, 38->33, 39->32, 40->47, 41->37, 42->22, 43->3, 44->13, 45->24, 46->0, 47->4, 48->43, 49->28, 50->15, 51->1, 52->26, 53->47, 54->44, 55->1, 56->7, 57->29, 58->12, 59->6, 60->22, 61->19, 62->42, 63->47, 64->35, 65->12, 66->24, 67->39, 68->20, 69->3, 70->1, 71->28, 72->29, 73->47, 74->34, 75->12, 76->23, 77->6, 78->4, 79->28, 80->42, 81->40, 82->26, 83->5, 84->29, 85->30, 86->17, 87->10, 88->28, 89->41, 90->39, 91->25, 92->25, 93->41, 94->8, 95->44, 96->38, 97->32, 98->40, 99->10, 100->23, 101->0, 102->2, 103->19, 104->46, 105->18, 106->46, 107->25, 108->4, 109->33, 110->17, 111->22, 112->22, 113->43, 114->35, 115->5, 116->24, 117->2, 118->36, 119->4, 120->14, 121->20, 122->44, 123->37, 124->40, 125->5, 126->45, 127->17, 128->3, 129->43, 130->36, 131->15, 132->12, 133->17, 134->31, 135->45, 136->20, 137->8, 138->9, 139->46, 140->2, 141->37, 142->18, 143->42, 144->17, 145->9, 146->6, 147->0, 148->14, 149->47, 150->2, 151->43, 152->30, 153->6, 154->2, 155->5, 156->19, 157->5, 158->24, 159->39, 160->25, 161->10, 162->37, 163->8, 164->27, 165->44, 166->8, 167->8, 168->38, 169->44, 170->20, 171->38, 172->27, 173->40, 174->24, 175->21, 176->41, 177->38, 178->38, 179->9, 180->17, 181->30, 182->33, 183->30, 184->20, 185->10, 186->13, 187->33, 188->42, 189->9, 190->37, 191->43, 192->23, 193->16, 194->24, 195->35, 196->45, 197->36, 198->20, 199->16, 200->21, 201->13, 202->31, 203->9, 204->3, 205->40, 206->37, 207->13, 208->13, 209->19, 210->25, 211->16, 212->21, 213->45, 214->38, 215->19, 216->26, 217->21, 218->34, 219->2, 220->3, 221->6, 222->11, 223->18, 224->32, 225->16, 226->30, 227->26, 228->26, 229->44, 230->8, 231->35, 232->32, 233->10, 234->14, 235->31, 236->0, 237->0, 238->16, 239->40, 240->5, 241->7, 242->23, 243->11, 244->18, 245->36, 246->1, 247->21, 248->35, 249->27, 250->15, 251->34, 252->43, 253->32, 254->31, 255->15, 256->9, 257->16, 258->15, 259->42, 260->41, 261->7, 262->7, 263->19, 264->11, 265->21, 266->39, 267->32, 268->7, 269->42, 270->31, 271->46, 272->4, 273->0, 274->47, 275->27, 276->27, 277->34, 278->22, 279->15, 280->27, 281->11, 282->46, 283->45, 284->45, 285->25, 286->36, 287->36

Average CPI map:
	1.80,	1.49,	4.05,	4.66,	1.25,	1.86
	2.02,	3.05,	15.98,	10.47,	2.03,	2.23
	4.74,	12.00,	1.42,	1.71,	10.03,	2.40
	4.29,	11.09,	10.71,	11.06,	1.93,	2.64
	4.36,	0.78,	1.89,	10.38,	1.62,	2.29
	2.06,	10.19,	1.33,	1.26,	1.39,	2.00
	2.04,	12.32,	20.82,	0.62,	11.09,	4.09
	6.64,	2.82,	11.79,	11.73,	5.08,	2.48

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	1,	1,	1
	1,	1,	0,	0,	1,	1
	1,	1,	1,	1,	0,	1
	1,	0,	0,	1,	0,	1
	1,	1,	0,	0,	0,	1
	1,	1,	1,	0,	1,	1
	1,	1,	1,	1,	1,	1
