==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 18 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 501

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008000
- lns branches: 16120
- lns fails: 7802
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 15544
- lns fails: 7674
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.244/4.261
- branches (local/global): 2792/34456
- fails (local/global): 1316/16792
- lns time: 0.252000
- lns branches: 2792
- lns fails: 1316
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.001/4.270
- branches (local/global): 47/34503
- fails (local/global): 10/16802
- lns time: 0.008000
- lns branches: 47
- lns fails: 10
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 12957
- lns fails: 6333
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 23959
- lns fails: 11533
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007000
- lns branches: 13446
- lns fails: 6518
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 26229
- lns fails: 12676
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007000
- lns branches: 15918
- lns fails: 7775
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 16651
- lns fails: 8083
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 35767
- lns fails: 17330
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 37144
- lns fails: 18169
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 9979
- lns fails: 4886
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 28742
- lns fails: 13792
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 13205
- lns fails: 6429
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007000
- lns branches: 28455
- lns fails: 13914
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 45711
- lns fails: 22033
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.045/30.426
- branches (local/global): 472/343138
- fails (local/global): 206/166479
- lns time: 0.055000
- lns branches: 472
- lns fails: 206
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 14704
- lns fails: 7134
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 44765
- lns fails: 21729
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 12893
- lns fails: 6340
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 12189
- lns fails: 5861
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 16759
- lns fails: 8101
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 16619
- lns fails: 8194
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 16532
- lns fails: 7970
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 19238
- lns fails: 9459
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 16883
- lns fails: 8224
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 14471
- lns fails: 6974
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 19344
- lns fails: 9384
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 34219
- lns fails: 16563
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 15356
- lns fails: 7491
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 20947
- lns fails: 10249
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 11540
- lns fails: 5688
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 40577
- lns fails: 19701
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 19026
- lns fails: 9401
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 13410
- lns fails: 6494
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 23200
- lns fails: 11266
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 14893
- lns fails: 7258
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 20247
- lns fails: 9735
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 13199
- lns fails: 6333
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.219/74.828
- branches (local/global): 2505/776654
- fails (local/global): 1203/377231
- lns time: 0.227000
- lns branches: 2505
- lns fails: 1203
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 17139
- lns fails: 8359
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 15882
- lns fails: 7740
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007000
- lns branches: 14856
- lns fails: 7329
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.006/80.867
- branches (local/global): 54/824585
- fails (local/global): 14/400673
- lns time: 0.015000
- lns branches: 54
- lns fails: 14
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 10497
- lns fails: 5001
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 13402
- lns fails: 6613
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 24394
- lns fails: 11962
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007000
- lns branches: 17300
- lns fails: 8370
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 1.102000
- lns branches: 10139
- lns fails: 4881

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 24
number of iterations: 50
total time: 90009.000000
total branches: 900317
total fails: 437500

Final Solution: 0->2, 1->20, 2->26, 3->2, 4->31, 5->11, 6->23, 7->21, 8->21, 9->20, 10->30, 11->32, 12->0, 13->44, 14->0, 15->47, 16->38, 17->2, 18->15, 19->30, 20->34, 21->9, 22->43, 23->41, 24->16, 25->14, 26->8, 27->32, 28->23, 29->42, 30->47, 31->24, 32->18, 33->5, 34->6, 35->9, 36->5, 37->21, 38->30, 39->25, 40->44, 41->40, 42->25, 43->13, 44->2, 45->29, 46->22, 47->0, 48->12, 49->39, 50->6, 51->46, 52->3, 53->8, 54->41, 55->29, 56->24, 57->28, 58->30, 59->32, 60->37, 61->35, 62->14, 63->37, 64->42, 65->14, 66->27, 67->41, 68->0, 69->8, 70->5, 71->33, 72->8, 73->2, 74->14, 75->12, 76->11, 77->16, 78->37, 79->34, 80->31, 81->43, 82->22, 83->6, 84->44, 85->40, 86->47, 87->13, 88->46, 89->28, 90->24, 91->7, 92->36, 93->36, 94->6, 95->47, 96->17, 97->45, 98->33, 99->3, 100->45, 101->40, 102->33, 103->36, 104->35, 105->12, 106->2, 107->7, 108->11, 109->41, 110->39, 111->1, 112->24, 113->1, 114->1, 115->46, 116->10, 117->6, 118->19, 119->17, 120->34, 121->15, 122->39, 123->27, 124->8, 125->31, 126->38, 127->29, 128->25, 129->44, 130->33, 131->26, 132->19, 133->5, 134->21, 135->45, 136->9, 137->5, 138->20, 139->28, 140->3, 141->34, 142->1, 143->18, 144->7, 145->27, 146->40, 147->39, 148->3, 149->28, 150->23, 151->17, 152->21, 153->39, 154->22, 155->13, 156->39, 157->4, 158->27, 159->36, 160->34, 161->42, 162->36, 163->23, 164->28, 165->31, 166->29, 167->18, 168->30, 169->46, 170->20, 171->1, 172->27, 173->4, 174->31, 175->38, 176->19, 177->9, 178->17, 179->35, 180->45, 181->41, 182->33, 183->25, 184->16, 185->26, 186->6, 187->25, 188->4, 189->17, 190->22, 191->26, 192->47, 193->5, 194->19, 195->33, 196->11, 197->28, 198->12, 199->42, 200->23, 201->1, 202->42, 203->32, 204->32, 205->25, 206->13, 207->17, 208->11, 209->47, 210->43, 211->34, 212->37, 213->44, 214->44, 215->23, 216->45, 217->10, 218->26, 219->13, 220->12, 221->11, 222->18, 223->36, 224->22, 225->22, 226->14, 227->35, 228->24, 229->38, 230->26, 231->10, 232->12, 233->4, 234->15, 235->16, 236->16, 237->46, 238->7, 239->7, 240->10, 241->37, 242->30, 243->16, 244->0, 245->21, 246->45, 247->32, 248->35, 249->10, 250->37, 251->7, 252->14, 253->43, 254->41, 255->4, 256->20, 257->4, 258->29, 259->13, 260->40, 261->8, 262->27, 263->38, 264->20, 265->0, 266->29, 267->10, 268->43, 269->18, 270->40, 271->9, 272->3, 273->43, 274->15, 275->18, 276->42, 277->15, 278->19, 279->38, 280->9, 281->35, 282->24, 283->31, 284->3, 285->19, 286->15, 287->46

Average CPI map:
	1.97,	4.03,	3.78,	4.05,	1.57,	1.59
	2.87,	3.46,	3.42,	0.94,	3.72,	3.14
	3.06,	2.93,	2.90,	1.02,	2.80,	4.75
	2.04,	2.48,	2.48,	4.41,	2.32,	2.91
	2.01,	2.28,	2.27,	4.07,	2.26,	2.29
	2.18,	2.17,	2.17,	2.14,	2.13,	2.12
	2.12,	2.10,	2.10,	2.10,	5.02,	2.10
	2.08,	2.91,	2.09,	2.09,	4.50,	2.07

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	0,	1,	1
