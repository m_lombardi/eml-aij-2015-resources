==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 19 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 801

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.022/0.022
- branches (local/global): 276/276
- fails (local/global): 114/114
- lns time: 0.032000
- lns branches: 276
- lns fails: 114
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.110/0.142
- branches (local/global): 2831/3107
- fails (local/global): 1329/1443
- lns time: 0.118000
- lns branches: 2831
- lns fails: 1329
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.014/0.164
- branches (local/global): 40/3147
- fails (local/global): 7/1450
- lns time: 0.022000
- lns branches: 40
- lns fails: 7
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.044/0.216
- branches (local/global): 1121/4268
- fails (local/global): 511/1961
- lns time: 0.051000
- lns branches: 1121
- lns fails: 511
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.694/0.917
- branches (local/global): 7103/11371
- fails (local/global): 3391/5352
- lns time: 0.702000
- lns branches: 7103
- lns fails: 3391
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.041/0.966
- branches (local/global): 252/11623
- fails (local/global): 108/5460
- lns time: 0.048000
- lns branches: 252
- lns fails: 108
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 39036
- lns fails: 19042
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.146/3.126
- branches (local/global): 2832/53491
- fails (local/global): 1327/25829
- lns time: 0.154000
- lns branches: 2832
- lns fails: 1327
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 27624
- lns fails: 13561
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 17561
- lns fails: 8508
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 17727
- lns fails: 8601
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 16889
- lns fails: 8236
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.163/12.329
- branches (local/global): 11748/145040
- fails (local/global): 5606/70341
- lns time: 1.171000
- lns branches: 11748
- lns fails: 5606
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 13681
- lns fails: 6814
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.040/14.385
- branches (local/global): 382/159103
- fails (local/global): 172/77327
- lns time: 0.049000
- lns branches: 382
- lns fails: 172
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.124/14.518
- branches (local/global): 1345/160448
- fails (local/global): 611/77938
- lns time: 0.132000
- lns branches: 1345
- lns fails: 611
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 22085
- lns fails: 10694
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 42857
- lns fails: 20806
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 18946
- lns fails: 9191
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 20159
- lns fails: 9822
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 20894
- lns fails: 10296
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 0.848000
- lns branches: 6003
- lns fails: 2979
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.151/25.565
- branches (local/global): 1376/292768
- fails (local/global): 633/142359
- lns time: 0.161000
- lns branches: 1376
- lns fails: 633
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.085/25.660
- branches (local/global): 307/293075
- fails (local/global): 134/142493
- lns time: 0.095000
- lns branches: 307
- lns fails: 134
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 27759
- lns fails: 13428
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 20443
- lns fails: 10039
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 22796
- lns fails: 11249
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 32419
- lns fails: 15730
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 11599
- lns fails: 5750
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 10796
- lns fails: 5184
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 16517
- lns fails: 8148
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 18305
- lns fails: 9008
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 38112
- lns fails: 18566
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 23204
- lns fails: 11260
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 16873
- lns fails: 8289
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 16778
- lns fails: 8174
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 35050
- lns fails: 17048
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.016/51.785
- branches (local/global): 397/584123
- fails (local/global): 180/284546
- lns time: 0.024000
- lns branches: 397
- lns fails: 180
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 17135
- lns fails: 8337
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 16482
- lns fails: 8179
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 19357
- lns fails: 9401
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 19060
- lns fails: 9378
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 20514
- lns fails: 10013
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.009000
- lns branches: 13603
- lns fails: 6672
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 51688
- lns fails: 25064
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 16946
- lns fails: 8404
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 16644
- lns fails: 8140
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 20657
- lns fails: 10046
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 26755
- lns fails: 13143
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 36639
- lns fails: 18026
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 18707
- lns fails: 9126
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.009000
- lns branches: 17445
- lns fails: 8563
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.012/79.915
- branches (local/global): 88/895843
- fails (local/global): 32/437070
- lns time: 0.020000
- lns branches: 88
- lns fails: 32
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.008000
- lns branches: 21158
- lns fails: 10318
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.007000
- lns branches: 18988
- lns fails: 9340
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.008000
- lns branches: 20962
- lns fails: 10288
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.008000
- lns branches: 14065
- lns fails: 6917
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 2.008000
- lns branches: 18440
- lns fails: 8994
LNS iteration #58 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
- lns time: 0.046000
- lns branches: 231
- lns fails: 114

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 34
number of iterations: 59
total time: 90008.000000
total branches: 989687
total fails: 483041

Final Solution: 0->7, 1->21, 2->33, 3->23, 4->44, 5->16, 6->0, 7->7, 8->43, 9->35, 10->31, 11->4, 12->22, 13->12, 14->26, 15->32, 16->4, 17->13, 18->17, 19->39, 20->27, 21->23, 22->47, 23->26, 24->10, 25->8, 26->18, 27->10, 28->22, 29->11, 30->21, 31->14, 32->22, 33->1, 34->12, 35->27, 36->13, 37->46, 38->37, 39->38, 40->4, 41->29, 42->20, 43->42, 44->5, 45->2, 46->20, 47->11, 48->18, 49->11, 50->35, 51->14, 52->14, 53->30, 54->35, 55->8, 56->15, 57->41, 58->31, 59->23, 60->12, 61->1, 62->34, 63->37, 64->44, 65->43, 66->40, 67->39, 68->11, 69->15, 70->35, 71->27, 72->32, 73->19, 74->39, 75->25, 76->32, 77->16, 78->23, 79->30, 80->43, 81->28, 82->9, 83->1, 84->29, 85->33, 86->19, 87->25, 88->37, 89->27, 90->28, 91->31, 92->16, 93->10, 94->9, 95->34, 96->27, 97->18, 98->33, 99->3, 100->33, 101->23, 102->2, 103->1, 104->5, 105->15, 106->20, 107->14, 108->14, 109->30, 110->13, 111->12, 112->36, 113->11, 114->43, 115->13, 116->28, 117->14, 118->24, 119->0, 120->20, 121->24, 122->22, 123->45, 124->19, 125->25, 126->46, 127->31, 128->7, 129->2, 130->20, 131->30, 132->27, 133->8, 134->2, 135->7, 136->15, 137->45, 138->45, 139->37, 140->19, 141->39, 142->17, 143->7, 144->42, 145->7, 146->5, 147->46, 148->26, 149->13, 150->40, 151->44, 152->12, 153->34, 154->34, 155->25, 156->41, 157->21, 158->16, 159->35, 160->46, 161->8, 162->44, 163->31, 164->40, 165->16, 166->38, 167->38, 168->4, 169->24, 170->45, 171->28, 172->46, 173->37, 174->1, 175->3, 176->43, 177->36, 178->26, 179->2, 180->21, 181->37, 182->25, 183->9, 184->36, 185->22, 186->9, 187->3, 188->47, 189->41, 190->26, 191->40, 192->21, 193->30, 194->45, 195->13, 196->31, 197->19, 198->10, 199->21, 200->6, 201->30, 202->6, 203->9, 204->3, 205->17, 206->42, 207->18, 208->10, 209->9, 210->3, 211->18, 212->17, 213->47, 214->40, 215->11, 216->15, 217->19, 218->24, 219->44, 220->38, 221->17, 222->17, 223->36, 224->20, 225->6, 226->47, 227->42, 228->6, 229->29, 230->3, 231->26, 232->0, 233->44, 234->15, 235->42, 236->47, 237->47, 238->28, 239->33, 240->45, 241->12, 242->23, 243->38, 244->46, 245->5, 246->22, 247->18, 248->4, 249->8, 250->24, 251->8, 252->5, 253->42, 254->10, 255->36, 256->29, 257->6, 258->32, 259->28, 260->32, 261->25, 262->1, 263->35, 264->41, 265->39, 266->38, 267->2, 268->33, 269->39, 270->16, 271->0, 272->29, 273->0, 274->5, 275->41, 276->34, 277->40, 278->6, 279->36, 280->24, 281->4, 282->34, 283->29, 284->43, 285->41, 286->32, 287->0

Average CPI map:
	1.73,	2.16,	2.09,	2.03,	1.74,	1.50
	2.40,	2.12,	2.74,	10.00,	2.05,	2.04
	5.27,	1.19,	2.05,	1.31,	11.91,	2.25
	2.03,	1.39,	4.16,	1.16,	3.64,	5.43
	3.39,	3.17,	1.46,	1.22,	10.54,	2.39
	5.58,	10.03,	2.32,	1.51,	11.47,	2.04
	2.26,	1.13,	9.58,	2.21,	10.01,	1.69
	2.22,	5.39,	10.03,	5.55,	2.32,	2.11

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	0,	1,	1
	1,	0,	1,	0,	1,	1
	1,	1,	1,	1,	1,	1
