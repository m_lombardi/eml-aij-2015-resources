LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
0.822202, 1.03693, 0.576976, 3.61605, 3.89835, 2.0495, 0.5, 1.27889, 3.11919, 0.751166, 3.15822, 3.19253, 1.85188, 1.784, 1.98891, 4.63716, 0.555712, 1.18234, 0.5, 1.71343, 0.994135, 3.09443, 2.69763, 3.00038, 1.98211, 2.27377, 1.91712, 1.88123, 1.78375, 2.16201, 2.39605, 2.3869, 1.20719, 1.56076, 2.60248, 1.84663, 2.62657, 2.37566, 0.721892, 0.73276, 2.79826, 2.74486, 0.5, 1.84905, 4.00113, 2.00266, 1.86305, 1.7841, 3.3957, 0.5, 3.37115, 1.71734, 2.01574, 1.00006, 2.33235, 0.907894, 1.92003, 2.49172, 1.97915, 2.36887, 0.897262, 3.45424, 2.84769, 2.21824, 2.08257, 0.5, 0.639426, 0.5, 3.79278, 2.74457, 1.31356, 3.00967, 2.02057, 1.82647, 0.823463, 1.45008, 2.80056, 3.07885, 1.79288, 2.02898, 1.89328, 2.17166, 2.45822, 1.65498, 1.8358, 2.39425, 1.43032, 1.78828, 2.37552, 2.17583, 0.5, 0.731085, 0.861213, 2.84868, 3.67892, 3.3801, 3.35293, 1.06079, 2.39516, 2.00835, 1.33842, 1.84435, 0.588556, 2.83028, 3.69538, 1.36061, 3.02517, 0.5, 0.550459, 0.602651, 0.931703, 2.75636, 4.2002, 2.95863, 3.38172, 2.35425, 0.5, 2.04737, 2.57186, 1.1448, 0.5, 2.83378, 2.97744, 2.05238, 2.88634, 0.750057, 4.17159, 1.33515, 0.690573, 2.8313, 0.5, 2.47139, 21.2536, 9.71935, 26.2397, 24.4538, 24.0133, 26.3202, 12.9733, 15.8205, 16.4545, 35, 19.1206, 32.631, 0.5, 2.30977, 1.00696, 1.80931, 1.56134, 4.81262, 1.98746, 2.25654, 1.56531, 1.89278, 2.19437, 2.10355, 4.06932, 2.63585, 2.36012, 0.674881, 0.5, 1.75983, 11.395, 22.7298, 21.6593, 35, 22.4994, 18.7165, 25.1279, 24.7518, 3.96816, 29.1898, 26.6711, 22.2912, 19.041, 19.8969, 14.5628, 30.3099, 20.8656, 27.3238, 0.5, 2.58027, 1.39463, 1.89467, 3.44538, 2.18505, 2.995, 0.779683, 1.10151, 2.0619, 1.94578, 3.11613, 2.22775, 2.20418, 1.64147, 1.65098, 2.42005, 1.85557, 26.2354, 16.8492, 25.7157, 23.4695, 23.8466, 15.8835, 19.4906, 35, 20.7529, 22.1737, 16.3682, 18.2145, 1.42512, 34.7831, 9.95029, 34.7868, 16.0547, 35, 1.85093, 3.66256, 1.23621, 1.03314, 1.64496, 2.57219, 2.51038, 1.97016, 2.43242, 2.53065, 2.05639, 0.5, 1.36001, 2.31327, 1.90728, 2.14544, 2.20865, 2.06535, 1.45245, 2.34827, 2.21373, 2.3091, 1.90416, 1.77229, 1.6424, 2.36952, 2.78981, 1.2392, 1.02584, 2.93322, 1.57511, 3.56747, 0.785944, 4.13503, 0.5, 1.43644, 2.34065, 1.07513, 2.40504, 2.00058, 2.25765, 1.92094, 1.29166, 0.5, 0.871675, 2.43035, 3.92671, 2.9796, 1.26283, 3.57321, 0.705942, 0.5, 5.06186, 0.89616, 2.37925, 2.398, 1.24377, 0.762687, 2.21961, 2.99668, 3.19817, 0.643531, 1.36463, 0.604851, 3.6371, 2.55172, 2.36529, 2.23317, 2.33473, 0.659972, 2.24374, 2.16309
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54812, operands = 135836
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 901, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 5944 itr]: obj = (25), mov = 5944, inf = 57.4%, acc = 34.1%, imp = 143

number of satisfied efficiency constraints: 25
[2 sec, 37311 itr]: obj = (31), mov = 37311, inf = 68.8%, acc = 17.6%, imp = 149

number of satisfied efficiency constraints: 31
[3 sec, 59643 itr]: obj = (34), mov = 59643, inf = 63.7%, acc = 17.4%, imp = 152

number of satisfied efficiency constraints: 34
[4 sec, 82492 itr]: obj = (35), mov = 82492, inf = 54.9%, acc = 17.9%, imp = 153

number of satisfied efficiency constraints: 35
[5 sec, 107201 itr]: obj = (35), mov = 107201, inf = 48.1%, acc = 18.6%, imp = 153

number of satisfied efficiency constraints: 35
[6 sec, 131154 itr]: obj = (35), mov = 131154, inf = 43.7%, acc = 19.2%, imp = 153

number of satisfied efficiency constraints: 35
[7 sec, 155626 itr]: obj = (35), mov = 155626, inf = 40.7%, acc = 19.4%, imp = 153

number of satisfied efficiency constraints: 35
[8 sec, 179679 itr]: obj = (35), mov = 179679, inf = 38.4%, acc = 19.6%, imp = 153

number of satisfied efficiency constraints: 35
[9 sec, 200158 itr]: obj = (35), mov = 200158, inf = 36.8%, acc = 20.5%, imp = 182

number of satisfied efficiency constraints: 35
[10 sec, 219024 itr]: obj = (35), mov = 219024, inf = 35.4%, acc = 21.3%, imp = 186

number of satisfied efficiency constraints: 35
[11 sec, 238421 itr]: obj = (35), mov = 238421, inf = 34.1%, acc = 21.9%, imp = 186

number of satisfied efficiency constraints: 35
[12 sec, 259079 itr]: obj = (35), mov = 259079, inf = 32.9%, acc = 22.1%, imp = 187

number of satisfied efficiency constraints: 35
[13 sec, 279616 itr]: obj = (35), mov = 279616, inf = 31.9%, acc = 22.3%, imp = 187

number of satisfied efficiency constraints: 35
[14 sec, 300230 itr]: obj = (35), mov = 300230, inf = 31%, acc = 22.4%, imp = 187

number of satisfied efficiency constraints: 35
[15 sec, 321469 itr]: obj = (35), mov = 321469, inf = 30.3%, acc = 22.5%, imp = 187

number of satisfied efficiency constraints: 35
[16 sec, 342231 itr]: obj = (35), mov = 342231, inf = 29.6%, acc = 22.6%, imp = 187

number of satisfied efficiency constraints: 35
[17 sec, 362683 itr]: obj = (35), mov = 362683, inf = 29%, acc = 22.8%, imp = 187

number of satisfied efficiency constraints: 35
[18 sec, 377612 itr]: obj = (35), mov = 377612, inf = 28.6%, acc = 23.5%, imp = 226

number of satisfied efficiency constraints: 35
[19 sec, 395549 itr]: obj = (35), mov = 395549, inf = 28%, acc = 23.8%, imp = 228

number of satisfied efficiency constraints: 35
[20 sec, 415304 itr]: obj = (35), mov = 415304, inf = 27.5%, acc = 23.9%, imp = 229

number of satisfied efficiency constraints: 35
[21 sec, 436758 itr]: obj = (35), mov = 436758, inf = 26.9%, acc = 23.8%, imp = 230

number of satisfied efficiency constraints: 35
[22 sec, 458668 itr]: obj = (35), mov = 458668, inf = 26.4%, acc = 23.6%, imp = 230

number of satisfied efficiency constraints: 35
[23 sec, 480345 itr]: obj = (35), mov = 480345, inf = 26%, acc = 23.5%, imp = 230

number of satisfied efficiency constraints: 35
[24 sec, 501208 itr]: obj = (35), mov = 501208, inf = 25.6%, acc = 23.4%, imp = 230

number of satisfied efficiency constraints: 35
[25 sec, 523050 itr]: obj = (35), mov = 523050, inf = 25.2%, acc = 23.3%, imp = 230

number of satisfied efficiency constraints: 35
[26 sec, 545164 itr]: obj = (35), mov = 545164, inf = 24.9%, acc = 23.1%, imp = 230

number of satisfied efficiency constraints: 35
[27 sec, 566772 itr]: obj = (35), mov = 566772, inf = 24.6%, acc = 23.1%, imp = 230

number of satisfied efficiency constraints: 35
[28 sec, 583483 itr]: obj = (35), mov = 583483, inf = 24.3%, acc = 23.3%, imp = 262

number of satisfied efficiency constraints: 35
[29 sec, 599343 itr]: obj = (35), mov = 599343, inf = 24.1%, acc = 23.7%, imp = 267

number of satisfied efficiency constraints: 35
[30 sec, 618201 itr]: obj = (35), mov = 618201, inf = 23.8%, acc = 23.8%, imp = 267

number of satisfied efficiency constraints: 35
[31 sec, 636452 itr]: obj = (35), mov = 636452, inf = 23.6%, acc = 23.9%, imp = 267

number of satisfied efficiency constraints: 35
[32 sec, 656367 itr]: obj = (35), mov = 656367, inf = 23.4%, acc = 24%, imp = 268

number of satisfied efficiency constraints: 35
[33 sec, 675974 itr]: obj = (35), mov = 675974, inf = 23.2%, acc = 24%, imp = 268

number of satisfied efficiency constraints: 35
[34 sec, 696674 itr]: obj = (35), mov = 696674, inf = 22.9%, acc = 23.9%, imp = 269

number of satisfied efficiency constraints: 35
[35 sec, 717557 itr]: obj = (35), mov = 717557, inf = 22.7%, acc = 23.8%, imp = 269

number of satisfied efficiency constraints: 35
[36 sec, 738675 itr]: obj = (35), mov = 738675, inf = 22.5%, acc = 23.7%, imp = 269

number of satisfied efficiency constraints: 35
[37 sec, 760173 itr]: obj = (35), mov = 760173, inf = 22.3%, acc = 23.7%, imp = 269

number of satisfied efficiency constraints: 35
[38 sec, 781193 itr]: obj = (35), mov = 781193, inf = 22.1%, acc = 23.6%, imp = 269

number of satisfied efficiency constraints: 35
[39 sec, 801895 itr]: obj = (35), mov = 801895, inf = 22%, acc = 23.5%, imp = 269

number of satisfied efficiency constraints: 35
[40 sec, 822863 itr]: obj = (35), mov = 822863, inf = 21.8%, acc = 23.5%, imp = 269

number of satisfied efficiency constraints: 35
[41 sec, 843346 itr]: obj = (35), mov = 843346, inf = 21.7%, acc = 23.5%, imp = 269

number of satisfied efficiency constraints: 35
[42 sec, 864345 itr]: obj = (35), mov = 864345, inf = 21.5%, acc = 23.4%, imp = 269

number of satisfied efficiency constraints: 35
[43 sec, 884746 itr]: obj = (35), mov = 884746, inf = 21.4%, acc = 23.4%, imp = 269

number of satisfied efficiency constraints: 35
[44 sec, 905735 itr]: obj = (35), mov = 905735, inf = 21.3%, acc = 23.4%, imp = 269

number of satisfied efficiency constraints: 35
[45 sec, 919737 itr]: obj = (35), mov = 919737, inf = 21.2%, acc = 23.7%, imp = 305

number of satisfied efficiency constraints: 35
[46 sec, 934009 itr]: obj = (35), mov = 934009, inf = 21.1%, acc = 23.9%, imp = 306

number of satisfied efficiency constraints: 35
[47 sec, 948886 itr]: obj = (35), mov = 948886, inf = 21%, acc = 24.2%, imp = 308

number of satisfied efficiency constraints: 35
[48 sec, 966294 itr]: obj = (35), mov = 966294, inf = 20.9%, acc = 24.3%, imp = 310

number of satisfied efficiency constraints: 35
[49 sec, 985719 itr]: obj = (35), mov = 985719, inf = 20.8%, acc = 24.3%, imp = 311

number of satisfied efficiency constraints: 35
[50 sec, 1005263 itr]: obj = (35), mov = 1005263, inf = 20.6%, acc = 24.3%, imp = 311

number of satisfied efficiency constraints: 35
[51 sec, 1024490 itr]: obj = (35), mov = 1024490, inf = 20.5%, acc = 24.3%, imp = 311

number of satisfied efficiency constraints: 35
[52 sec, 1044038 itr]: obj = (35), mov = 1044038, inf = 20.4%, acc = 24.4%, imp = 312

number of satisfied efficiency constraints: 35
[53 sec, 1065607 itr]: obj = (35), mov = 1065607, inf = 20.3%, acc = 24.3%, imp = 312

number of satisfied efficiency constraints: 35
[54 sec, 1086868 itr]: obj = (36), mov = 1086868, inf = 20.2%, acc = 24.2%, imp = 313

number of satisfied efficiency constraints: 36
[55 sec, 1109954 itr]: obj = (36), mov = 1109954, inf = 20.1%, acc = 24.1%, imp = 313

number of satisfied efficiency constraints: 36
[56 sec, 1131747 itr]: obj = (36), mov = 1131747, inf = 20%, acc = 24%, imp = 313

number of satisfied efficiency constraints: 36
[57 sec, 1153826 itr]: obj = (36), mov = 1153826, inf = 19.9%, acc = 23.9%, imp = 313

number of satisfied efficiency constraints: 36
[58 sec, 1175423 itr]: obj = (36), mov = 1175423, inf = 19.8%, acc = 23.8%, imp = 313

number of satisfied efficiency constraints: 36
[59 sec, 1198025 itr]: obj = (36), mov = 1198025, inf = 19.7%, acc = 23.7%, imp = 313

number of satisfied efficiency constraints: 36
[60 sec, 1220646 itr]: obj = (36), mov = 1220646, inf = 19.6%, acc = 23.6%, imp = 313

number of satisfied efficiency constraints: 36
[61 sec, 1238791 itr]: obj = (36), mov = 1238791, inf = 19.6%, acc = 23.6%, imp = 313

number of satisfied efficiency constraints: 36
[62 sec, 1256270 itr]: obj = (36), mov = 1256270, inf = 19.5%, acc = 23.5%, imp = 313

number of satisfied efficiency constraints: 36
[63 sec, 1277862 itr]: obj = (36), mov = 1277862, inf = 19.4%, acc = 23.4%, imp = 313

number of satisfied efficiency constraints: 36
[64 sec, 1299848 itr]: obj = (36), mov = 1299848, inf = 19.4%, acc = 23.3%, imp = 313

number of satisfied efficiency constraints: 36
[65 sec, 1322045 itr]: obj = (36), mov = 1322045, inf = 19.3%, acc = 23.2%, imp = 313

number of satisfied efficiency constraints: 36
[66 sec, 1342578 itr]: obj = (36), mov = 1342578, inf = 19.3%, acc = 23.1%, imp = 313

number of satisfied efficiency constraints: 36
[67 sec, 1364988 itr]: obj = (36), mov = 1364988, inf = 19.2%, acc = 23.1%, imp = 313

number of satisfied efficiency constraints: 36
[68 sec, 1387846 itr]: obj = (36), mov = 1387846, inf = 19.1%, acc = 23%, imp = 313

number of satisfied efficiency constraints: 36
[69 sec, 1410879 itr]: obj = (36), mov = 1410879, inf = 19.1%, acc = 22.9%, imp = 313

number of satisfied efficiency constraints: 36
[70 sec, 1433812 itr]: obj = (36), mov = 1433812, inf = 19%, acc = 22.8%, imp = 313

number of satisfied efficiency constraints: 36
[71 sec, 1457189 itr]: obj = (36), mov = 1457189, inf = 19%, acc = 22.7%, imp = 313

number of satisfied efficiency constraints: 36
[72 sec, 1475892 itr]: obj = (36), mov = 1475892, inf = 18.9%, acc = 22.7%, imp = 313

number of satisfied efficiency constraints: 36
[73 sec, 1492778 itr]: obj = (36), mov = 1492778, inf = 18.9%, acc = 22.7%, imp = 343

number of satisfied efficiency constraints: 36
[74 sec, 1506204 itr]: obj = (36), mov = 1506204, inf = 18.9%, acc = 22.8%, imp = 346

number of satisfied efficiency constraints: 36
[75 sec, 1520049 itr]: obj = (36), mov = 1520049, inf = 18.8%, acc = 22.9%, imp = 347

number of satisfied efficiency constraints: 36
[76 sec, 1534078 itr]: obj = (36), mov = 1534078, inf = 18.8%, acc = 22.9%, imp = 348

number of satisfied efficiency constraints: 36
[77 sec, 1552764 itr]: obj = (36), mov = 1552764, inf = 18.7%, acc = 23%, imp = 348

number of satisfied efficiency constraints: 36
[78 sec, 1571307 itr]: obj = (36), mov = 1571307, inf = 18.7%, acc = 23%, imp = 348

number of satisfied efficiency constraints: 36
[79 sec, 1589327 itr]: obj = (36), mov = 1589327, inf = 18.7%, acc = 23.1%, imp = 348

number of satisfied efficiency constraints: 36
[80 sec, 1606855 itr]: obj = (36), mov = 1606855, inf = 18.6%, acc = 23.2%, imp = 381

number of satisfied efficiency constraints: 36
[81 sec, 1621323 itr]: obj = (36), mov = 1621323, inf = 18.6%, acc = 23.3%, imp = 384

number of satisfied efficiency constraints: 36
[82 sec, 1638390 itr]: obj = (36), mov = 1638390, inf = 18.5%, acc = 23.4%, imp = 386

number of satisfied efficiency constraints: 36
[83 sec, 1656716 itr]: obj = (36), mov = 1656716, inf = 18.5%, acc = 23.4%, imp = 386

number of satisfied efficiency constraints: 36
[84 sec, 1674619 itr]: obj = (36), mov = 1674619, inf = 18.5%, acc = 23.5%, imp = 387

number of satisfied efficiency constraints: 36
[85 sec, 1693801 itr]: obj = (36), mov = 1693801, inf = 18.4%, acc = 23.5%, imp = 387

number of satisfied efficiency constraints: 36
[86 sec, 1712349 itr]: obj = (36), mov = 1712349, inf = 18.4%, acc = 23.5%, imp = 387

number of satisfied efficiency constraints: 36
[87 sec, 1731986 itr]: obj = (36), mov = 1731986, inf = 18.3%, acc = 23.5%, imp = 387

number of satisfied efficiency constraints: 36
[88 sec, 1750200 itr]: obj = (36), mov = 1750200, inf = 18.3%, acc = 23.6%, imp = 387

number of satisfied efficiency constraints: 36
[89 sec, 1769115 itr]: obj = (36), mov = 1769115, inf = 18.2%, acc = 23.6%, imp = 387

number of satisfied efficiency constraints: 36
[90 sec, 1788792 itr]: obj = (36), mov = 1788792, inf = 18.2%, acc = 23.6%, imp = 388

number of satisfied efficiency constraints: 36
[90 sec, 1788792 itr]: obj = (36), mov = 1788792, inf = 18.2%, acc = 23.6%, imp = 388

number of satisfied efficiency constraints: 36
1788792 iterations, 1788792 moves performed in 90 seconds
Feasible solution: obj = (36)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 36
Mapping: 0->15, 1->15, 2->18, 3->8, 4->43, 5->47, 6->23, 7->35, 8->43, 9->11, 10->31, 11->1, 12->14, 13->5, 14->20, 15->17, 16->41, 17->20, 18->1, 19->18, 20->15, 21->36, 22->44, 23->45, 24->41, 25->20, 26->30, 27->4, 28->27, 29->8, 30->40, 31->46, 32->4, 33->33, 34->25, 35->17, 36->28, 37->1, 38->19, 39->22, 40->39, 41->10, 42->2, 43->47, 44->23, 45->42, 46->29, 47->6, 48->4, 49->11, 50->23, 51->23, 52->5, 53->24, 54->15, 55->0, 56->10, 57->25, 58->38, 59->25, 60->32, 61->8, 62->43, 63->8, 64->16, 65->13, 66->20, 67->13, 68->12, 69->7, 70->13, 71->4, 72->10, 73->26, 74->11, 75->3, 76->32, 77->34, 78->1, 79->7, 80->26, 81->32, 82->37, 83->29, 84->3, 85->43, 86->0, 87->36, 88->4, 89->46, 90->24, 91->41, 92->14, 93->1, 94->16, 95->40, 96->1, 97->30, 98->8, 99->25, 100->14, 101->47, 102->20, 103->2, 104->31, 105->27, 106->23, 107->26, 108->47, 109->6, 110->13, 111->36, 112->6, 113->42, 114->5, 115->8, 116->27, 117->7, 118->3, 119->18, 120->22, 121->34, 122->42, 123->26, 124->38, 125->35, 126->0, 127->35, 128->2, 129->38, 130->32, 131->7, 132->16, 133->45, 134->46, 135->47, 136->41, 137->39, 138->37, 139->31, 140->15, 141->3, 142->10, 143->28, 144->26, 145->47, 146->30, 147->13, 148->22, 149->24, 150->22, 151->16, 152->5, 153->22, 154->17, 155->32, 156->7, 157->42, 158->44, 159->3, 160->17, 161->36, 162->0, 163->45, 164->44, 165->37, 166->34, 167->34, 168->39, 169->35, 170->39, 171->34, 172->46, 173->9, 174->30, 175->28, 176->39, 177->9, 178->11, 179->45, 180->29, 181->27, 182->38, 183->32, 184->21, 185->14, 186->24, 187->38, 188->27, 189->40, 190->46, 191->28, 192->25, 193->10, 194->24, 195->12, 196->19, 197->14, 198->21, 199->21, 200->40, 201->37, 202->43, 203->24, 204->31, 205->21, 206->45, 207->21, 208->2, 209->31, 210->0, 211->44, 212->18, 213->44, 214->29, 215->16, 216->9, 217->21, 218->3, 219->23, 220->35, 221->17, 222->28, 223->33, 224->2, 225->37, 226->19, 227->33, 228->18, 229->12, 230->7, 231->38, 232->10, 233->9, 234->20, 235->30, 236->6, 237->34, 238->19, 239->19, 240->33, 241->16, 242->39, 243->0, 244->4, 245->27, 246->5, 247->13, 248->26, 249->44, 250->18, 251->35, 252->6, 253->33, 254->12, 255->12, 256->11, 257->42, 258->11, 259->30, 260->15, 261->36, 262->45, 263->9, 264->6, 265->14, 266->22, 267->41, 268->42, 269->17, 270->33, 271->46, 272->29, 273->2, 274->37, 275->28, 276->43, 277->41, 278->12, 279->5, 280->36, 281->31, 282->9, 283->40, 284->40, 285->29, 286->19, 287->25
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	3.42819	2.34378	3.93069	7.12814	2.14919	1.82112
	2.06736	2.54482	2.69999	10.3104	5.03649	4.41493
	2.25455	1.43701	1.94422	3.75196	11.1069	2.10775
	2.54092	1.85309	1.35424	17.8944	1.23005	2.27466
	4.47211	2.31038	1.25968	1.7099	10.6296	3.66274
	4.31236	10.4885	1.72795	1.52128	13.1046	5.19954
	2.57772	13.1086	2.00275	12.5945	6.35361	4.73763
	2.92623	6.55072	16.737	14.5755	10.3029	5.50949
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	1	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
	1	1	0	1	1	1
	1	1	1	1	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 17 --global-tlim 90 --seed 901

Starting the Optimization Process
