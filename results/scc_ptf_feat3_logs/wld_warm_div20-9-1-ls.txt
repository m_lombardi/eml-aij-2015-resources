LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
4.72634, 0.80643, 0.600421, 1.23093, 2.15829, 2.47759, 3.25176, 2.58534, 1.64973, 2.0288, 1.88653, 0.597842, 1.91945, 1.94999, 2.05017, 1.85901, 2.36346, 1.85793, 2.62763, 2.40449, 2.67466, 0.5, 1.80273, 1.99049, 2.14438, 2.14645, 2.18921, 1.95719, 1.9618, 1.60097, 2.16001, 2.59673, 2.04742, 0.800318, 2.34858, 2.04694, 1.76921, 1.80511, 2.51634, 2.22796, 2.43494, 1.24643, 0.85042, 3.01765, 3.33737, 2.13918, 0.686134, 1.96925, 2.62422, 0.758929, 3.07115, 3.19422, 1.85148, 0.5, 3.25117, 0.5, 2.13828, 2.05464, 0.871231, 3.18467, 0.5, 4.79118, 2.3423, 1.0173, 0.921258, 2.42797, 2.31006, 0.5, 0.900488, 2.03999, 1.26812, 4.98134, 1.20626, 1.02555, 2.49961, 3.90973, 0.5, 2.85885, 3.30362, 1.02483, 4.52109, 1.30459, 1.34587, 0.5, 24.6396, 13.1308, 29.8871, 35, 11.4149, 17.9277, 35, 17.8207, 12.66, 17.2125, 20.6874, 28.6195, 1.5002, 2.785, 2.02919, 0.5, 2.24496, 2.94065, 0.5, 2.26738, 2.18001, 3.9793, 2.06294, 1.01036, 2.04474, 3.11418, 2.55831, 0.817484, 2.63524, 0.830046, 2.68802, 2.26018, 1.61054, 2.75781, 1.61053, 1.07294, 9.13066, 8.39748, 33.7156, 22.5257, 35, 23.2305, 3.46287, 1.9553, 0.992387, 0.5, 2.10496, 2.98448, 7.55319, 34.9071, 26.2854, 16.0863, 35, 12.168, 6.05281, 1.9006, 0.5, 1.33709, 0.978716, 1.23078, 1.41619, 2.43974, 0.5, 2.25508, 2.44977, 2.93921, 2.64417, 3.45957, 3.78676, 0.693863, 0.5, 0.915637, 26.8552, 16.3454, 35, 16.1199, 19.8227, 17.8569, 31.4681, 6.23997, 11.1457, 30.1129, 28.0174, 25.016, 23.7705, 22.3787, 15.8532, 24.7516, 24.2344, 21.0116, 1.42575, 3.09505, 2.82666, 0.5, 3.251, 0.901542, 3.97766, 0.5, 2.30717, 3.18453, 0.871821, 1.15882, 0.5, 2.72857, 3.21968, 1.24734, 2.21887, 2.08554, 1.81881, 2.13962, 2.25394, 1.93487, 1.76174, 2.09102, 2.15055, 5.67218, 0.985286, 1.26879, 1.05214, 0.871061, 1.16524, 3.72741, 4.03229, 0.946942, 0.72869, 1.39942, 2.26744, 2.29021, 1.91601, 1.80106, 1.47367, 2.2516, 1.92586, 2.21324, 1.73742, 1.86003, 2.05943, 2.20403, 2.09886, 1.98844, 1.88776, 1.71861, 2.16756, 2.13876, 2.69924, 1.5634, 2.49374, 1.96284, 2.78078, 0.5, 2.3516, 1.66845, 0.5, 1.39727, 2.45327, 3.62942, 2.14902, 1.80381, 1.75116, 2.42663, 0.5, 3.36939, 3.76519, 1.47279, 2.47116, 0.5, 2.89601, 0.894846, 1.82126, 2.34338, 3.42483, 0.888116, 1.28338, 2.23904, 3.35174, 0.527017, 0.989456, 1.61285, 2.28852, 3.23043, 2.36367, 2.37168, 1.78575, 1.28309, 2.05796, 2.13786, 2.33386, 2.13722, 1.05973, 2.2291, 2.35209, 1.888, 1.40055, 2.12991, 2.06404, 2.33329, 1.94813, 2.12407, 1.29112, 2.82369, 1.55113, 1.56719, 2.86181, 1.90506
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54718, operands = 135660
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 1, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 3189 itr]: obj = (21), mov = 3189, inf = 65.4%, acc = 29.1%, imp = 159

number of satisfied efficiency constraints: 21
[2 sec, 39912 itr]: obj = (30), mov = 39912, inf = 81%, acc = 11.9%, imp = 168

number of satisfied efficiency constraints: 30
[3 sec, 65983 itr]: obj = (30), mov = 65983, inf = 77.5%, acc = 12.6%, imp = 168

number of satisfied efficiency constraints: 30
[4 sec, 83447 itr]: obj = (31), mov = 83447, inf = 73.2%, acc = 14.2%, imp = 169

number of satisfied efficiency constraints: 31
[5 sec, 97614 itr]: obj = (32), mov = 97614, inf = 68.9%, acc = 16.1%, imp = 170

number of satisfied efficiency constraints: 32
[6 sec, 116298 itr]: obj = (34), mov = 116298, inf = 63.1%, acc = 18%, imp = 172

number of satisfied efficiency constraints: 34
[7 sec, 135114 itr]: obj = (35), mov = 135114, inf = 57.8%, acc = 19%, imp = 173

number of satisfied efficiency constraints: 35
[8 sec, 154715 itr]: obj = (35), mov = 154715, inf = 53.5%, acc = 19.8%, imp = 173

number of satisfied efficiency constraints: 35
[9 sec, 173801 itr]: obj = (35), mov = 173801, inf = 50.2%, acc = 20.4%, imp = 173

number of satisfied efficiency constraints: 35
[10 sec, 189486 itr]: obj = (35), mov = 189486, inf = 48.1%, acc = 20.7%, imp = 173

number of satisfied efficiency constraints: 35
[11 sec, 210381 itr]: obj = (35), mov = 210381, inf = 45.6%, acc = 21.1%, imp = 173

number of satisfied efficiency constraints: 35
[12 sec, 232403 itr]: obj = (35), mov = 232403, inf = 43.6%, acc = 21.5%, imp = 173

number of satisfied efficiency constraints: 35
[13 sec, 249785 itr]: obj = (35), mov = 249785, inf = 42.2%, acc = 21.7%, imp = 173

number of satisfied efficiency constraints: 35
[14 sec, 270554 itr]: obj = (35), mov = 270554, inf = 40.7%, acc = 22%, imp = 173

number of satisfied efficiency constraints: 35
[15 sec, 285861 itr]: obj = (35), mov = 285861, inf = 39.8%, acc = 22.2%, imp = 173

number of satisfied efficiency constraints: 35
[16 sec, 299168 itr]: obj = (35), mov = 299168, inf = 39.1%, acc = 22.3%, imp = 173

number of satisfied efficiency constraints: 35
[17 sec, 312993 itr]: obj = (35), mov = 312993, inf = 38.5%, acc = 22.5%, imp = 173

number of satisfied efficiency constraints: 35
[18 sec, 330236 itr]: obj = (35), mov = 330236, inf = 37.7%, acc = 22.6%, imp = 173

number of satisfied efficiency constraints: 35
[19 sec, 347316 itr]: obj = (35), mov = 347316, inf = 37%, acc = 22.8%, imp = 173

number of satisfied efficiency constraints: 35
[20 sec, 360695 itr]: obj = (35), mov = 360695, inf = 36.4%, acc = 23.6%, imp = 209

number of satisfied efficiency constraints: 35
[21 sec, 377234 itr]: obj = (35), mov = 377234, inf = 35.6%, acc = 24.2%, imp = 213

number of satisfied efficiency constraints: 35
[22 sec, 396449 itr]: obj = (35), mov = 396449, inf = 34.5%, acc = 24.5%, imp = 214

number of satisfied efficiency constraints: 35
[23 sec, 411224 itr]: obj = (35), mov = 411224, inf = 33.8%, acc = 24.6%, imp = 214

number of satisfied efficiency constraints: 35
[24 sec, 428652 itr]: obj = (35), mov = 428652, inf = 33%, acc = 24.8%, imp = 214

number of satisfied efficiency constraints: 35
[25 sec, 445564 itr]: obj = (35), mov = 445564, inf = 32.3%, acc = 25.1%, imp = 214

number of satisfied efficiency constraints: 35
[26 sec, 462369 itr]: obj = (35), mov = 462369, inf = 31.6%, acc = 25.3%, imp = 214

number of satisfied efficiency constraints: 35
[27 sec, 476041 itr]: obj = (35), mov = 476041, inf = 31.1%, acc = 25.9%, imp = 244

number of satisfied efficiency constraints: 35
[28 sec, 489336 itr]: obj = (35), mov = 489336, inf = 30.6%, acc = 26.4%, imp = 246

number of satisfied efficiency constraints: 35
[29 sec, 505041 itr]: obj = (35), mov = 505041, inf = 30%, acc = 26.7%, imp = 247

number of satisfied efficiency constraints: 35
[30 sec, 521006 itr]: obj = (35), mov = 521006, inf = 29.5%, acc = 26.9%, imp = 248

number of satisfied efficiency constraints: 35
[31 sec, 537951 itr]: obj = (35), mov = 537951, inf = 28.9%, acc = 27.1%, imp = 248

number of satisfied efficiency constraints: 35
[32 sec, 554210 itr]: obj = (35), mov = 554210, inf = 28.4%, acc = 27.3%, imp = 248

number of satisfied efficiency constraints: 35
[33 sec, 571035 itr]: obj = (35), mov = 571035, inf = 28%, acc = 27.4%, imp = 248

number of satisfied efficiency constraints: 35
[34 sec, 587132 itr]: obj = (35), mov = 587132, inf = 27.5%, acc = 27.6%, imp = 248

number of satisfied efficiency constraints: 35
[35 sec, 603797 itr]: obj = (35), mov = 603797, inf = 27.1%, acc = 27.7%, imp = 248

number of satisfied efficiency constraints: 35
[36 sec, 618475 itr]: obj = (35), mov = 618475, inf = 26.8%, acc = 27.9%, imp = 273

number of satisfied efficiency constraints: 35
[37 sec, 634258 itr]: obj = (35), mov = 634258, inf = 26.4%, acc = 28.3%, imp = 276

number of satisfied efficiency constraints: 35
[38 sec, 649748 itr]: obj = (35), mov = 649748, inf = 26%, acc = 28.5%, imp = 276

number of satisfied efficiency constraints: 35
[39 sec, 665225 itr]: obj = (35), mov = 665225, inf = 25.7%, acc = 28.8%, imp = 276

number of satisfied efficiency constraints: 35
[40 sec, 679358 itr]: obj = (35), mov = 679358, inf = 25.4%, acc = 29%, imp = 276

number of satisfied efficiency constraints: 35
[41 sec, 694220 itr]: obj = (35), mov = 694220, inf = 25.2%, acc = 29.1%, imp = 277

number of satisfied efficiency constraints: 35
[42 sec, 710002 itr]: obj = (35), mov = 710002, inf = 24.9%, acc = 29.2%, imp = 278

number of satisfied efficiency constraints: 35
[43 sec, 725012 itr]: obj = (35), mov = 725012, inf = 24.6%, acc = 29.3%, imp = 278

number of satisfied efficiency constraints: 35
[44 sec, 743772 itr]: obj = (35), mov = 743772, inf = 24.3%, acc = 29.3%, imp = 278

number of satisfied efficiency constraints: 35
[45 sec, 761628 itr]: obj = (35), mov = 761628, inf = 24%, acc = 29.4%, imp = 278

number of satisfied efficiency constraints: 35
[46 sec, 780559 itr]: obj = (35), mov = 780559, inf = 23.7%, acc = 29.4%, imp = 278

number of satisfied efficiency constraints: 35
[47 sec, 800233 itr]: obj = (35), mov = 800233, inf = 23.4%, acc = 29.5%, imp = 278

number of satisfied efficiency constraints: 35
[48 sec, 817604 itr]: obj = (35), mov = 817604, inf = 23.2%, acc = 29.5%, imp = 278

number of satisfied efficiency constraints: 35
[49 sec, 835053 itr]: obj = (35), mov = 835053, inf = 22.9%, acc = 29.6%, imp = 278

number of satisfied efficiency constraints: 35
[50 sec, 849683 itr]: obj = (35), mov = 849683, inf = 22.7%, acc = 29.6%, imp = 278

number of satisfied efficiency constraints: 35
[51 sec, 865368 itr]: obj = (35), mov = 865368, inf = 22.5%, acc = 29.6%, imp = 278

number of satisfied efficiency constraints: 35
[52 sec, 882741 itr]: obj = (35), mov = 882741, inf = 22.3%, acc = 29.7%, imp = 278

number of satisfied efficiency constraints: 35
[53 sec, 899946 itr]: obj = (35), mov = 899946, inf = 22.1%, acc = 29.7%, imp = 278

number of satisfied efficiency constraints: 35
[54 sec, 914838 itr]: obj = (35), mov = 914838, inf = 22%, acc = 29.7%, imp = 278

number of satisfied efficiency constraints: 35
[55 sec, 929057 itr]: obj = (35), mov = 929057, inf = 21.8%, acc = 30%, imp = 304

number of satisfied efficiency constraints: 35
[56 sec, 943079 itr]: obj = (35), mov = 943079, inf = 21.7%, acc = 30.2%, imp = 306

number of satisfied efficiency constraints: 35
[57 sec, 957893 itr]: obj = (35), mov = 957893, inf = 21.5%, acc = 30.4%, imp = 306

number of satisfied efficiency constraints: 35
[58 sec, 974310 itr]: obj = (35), mov = 974310, inf = 21.4%, acc = 30.6%, imp = 306

number of satisfied efficiency constraints: 35
[59 sec, 988345 itr]: obj = (35), mov = 988345, inf = 21.2%, acc = 30.8%, imp = 306

number of satisfied efficiency constraints: 35
[60 sec, 1004524 itr]: obj = (35), mov = 1004524, inf = 21.1%, acc = 30.9%, imp = 307

number of satisfied efficiency constraints: 35
[61 sec, 1019464 itr]: obj = (35), mov = 1019464, inf = 21%, acc = 31%, imp = 307

number of satisfied efficiency constraints: 35
[62 sec, 1035575 itr]: obj = (35), mov = 1035575, inf = 20.8%, acc = 31.1%, imp = 307

number of satisfied efficiency constraints: 35
[63 sec, 1050472 itr]: obj = (35), mov = 1050472, inf = 20.7%, acc = 31.2%, imp = 308

number of satisfied efficiency constraints: 35
[64 sec, 1067182 itr]: obj = (35), mov = 1067182, inf = 20.6%, acc = 31.2%, imp = 309

number of satisfied efficiency constraints: 35
[65 sec, 1084662 itr]: obj = (35), mov = 1084662, inf = 20.4%, acc = 31.2%, imp = 310

number of satisfied efficiency constraints: 35
[66 sec, 1105490 itr]: obj = (35), mov = 1105490, inf = 20.3%, acc = 31.2%, imp = 310

number of satisfied efficiency constraints: 35
[67 sec, 1122102 itr]: obj = (35), mov = 1122102, inf = 20.1%, acc = 31.1%, imp = 310

number of satisfied efficiency constraints: 35
[68 sec, 1138531 itr]: obj = (35), mov = 1138531, inf = 20%, acc = 31.1%, imp = 310

number of satisfied efficiency constraints: 35
[69 sec, 1155531 itr]: obj = (35), mov = 1155531, inf = 19.9%, acc = 31.1%, imp = 310

number of satisfied efficiency constraints: 35
[70 sec, 1171956 itr]: obj = (35), mov = 1171956, inf = 19.8%, acc = 31%, imp = 310

number of satisfied efficiency constraints: 35
[71 sec, 1189917 itr]: obj = (35), mov = 1189917, inf = 19.7%, acc = 31%, imp = 310

number of satisfied efficiency constraints: 35
[72 sec, 1208497 itr]: obj = (35), mov = 1208497, inf = 19.6%, acc = 31%, imp = 310

number of satisfied efficiency constraints: 35
[73 sec, 1227495 itr]: obj = (35), mov = 1227495, inf = 19.5%, acc = 30.9%, imp = 310

number of satisfied efficiency constraints: 35
[74 sec, 1247292 itr]: obj = (35), mov = 1247292, inf = 19.3%, acc = 30.9%, imp = 310

number of satisfied efficiency constraints: 35
[75 sec, 1266059 itr]: obj = (35), mov = 1266059, inf = 19.2%, acc = 30.9%, imp = 310

number of satisfied efficiency constraints: 35
[76 sec, 1287359 itr]: obj = (35), mov = 1287359, inf = 19.1%, acc = 30.8%, imp = 310

number of satisfied efficiency constraints: 35
[77 sec, 1308225 itr]: obj = (35), mov = 1308225, inf = 19%, acc = 30.8%, imp = 310

number of satisfied efficiency constraints: 35
[78 sec, 1328854 itr]: obj = (35), mov = 1328854, inf = 18.9%, acc = 30.8%, imp = 310

number of satisfied efficiency constraints: 35
[79 sec, 1345784 itr]: obj = (35), mov = 1345784, inf = 18.8%, acc = 30.8%, imp = 310

number of satisfied efficiency constraints: 35
[80 sec, 1364921 itr]: obj = (35), mov = 1364921, inf = 18.7%, acc = 30.7%, imp = 310

number of satisfied efficiency constraints: 35
[81 sec, 1385814 itr]: obj = (35), mov = 1385814, inf = 18.6%, acc = 30.7%, imp = 310

number of satisfied efficiency constraints: 35
[82 sec, 1406426 itr]: obj = (35), mov = 1406426, inf = 18.5%, acc = 30.7%, imp = 310

number of satisfied efficiency constraints: 35
[83 sec, 1427103 itr]: obj = (35), mov = 1427103, inf = 18.4%, acc = 30.7%, imp = 310

number of satisfied efficiency constraints: 35
[84 sec, 1447553 itr]: obj = (35), mov = 1447553, inf = 18.3%, acc = 30.6%, imp = 310

number of satisfied efficiency constraints: 35
[85 sec, 1468382 itr]: obj = (35), mov = 1468382, inf = 18.2%, acc = 30.6%, imp = 341

number of satisfied efficiency constraints: 35
[86 sec, 1483856 itr]: obj = (35), mov = 1483856, inf = 18.2%, acc = 30.8%, imp = 349

number of satisfied efficiency constraints: 35
[87 sec, 1502783 itr]: obj = (35), mov = 1502783, inf = 18.1%, acc = 30.9%, imp = 350

number of satisfied efficiency constraints: 35
[88 sec, 1522822 itr]: obj = (35), mov = 1522822, inf = 18%, acc = 30.9%, imp = 351

number of satisfied efficiency constraints: 35
[89 sec, 1542000 itr]: obj = (35), mov = 1542000, inf = 17.9%, acc = 30.9%, imp = 351

number of satisfied efficiency constraints: 35
[90 sec, 1562038 itr]: obj = (35), mov = 1562038, inf = 17.8%, acc = 30.9%, imp = 351

number of satisfied efficiency constraints: 35
[90 sec, 1562038 itr]: obj = (35), mov = 1562038, inf = 17.8%, acc = 30.9%, imp = 351

number of satisfied efficiency constraints: 35
1562038 iterations, 1562038 moves performed in 90 seconds
Feasible solution: obj = (35)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 35
Mapping: 0->17, 1->33, 2->14, 3->27, 4->4, 5->43, 6->29, 7->8, 8->41, 9->28, 10->26, 11->13, 12->40, 13->19, 14->33, 15->36, 16->10, 17->42, 18->42, 19->8, 20->44, 21->14, 22->20, 23->3, 24->28, 25->9, 26->26, 27->3, 28->42, 29->13, 30->10, 31->25, 32->21, 33->41, 34->43, 35->36, 36->29, 37->13, 38->40, 39->35, 40->3, 41->12, 42->14, 43->9, 44->7, 45->10, 46->31, 47->17, 48->22, 49->12, 50->34, 51->29, 52->5, 53->14, 54->38, 55->23, 56->19, 57->35, 58->42, 59->3, 60->5, 61->39, 62->45, 63->28, 64->17, 65->36, 66->40, 67->20, 68->23, 69->7, 70->15, 71->47, 72->32, 73->29, 74->0, 75->40, 76->36, 77->16, 78->36, 79->18, 80->37, 81->30, 82->23, 83->11, 84->25, 85->17, 86->9, 87->41, 88->38, 89->12, 90->44, 91->35, 92->39, 93->23, 94->45, 95->47, 96->27, 97->35, 98->10, 99->21, 100->24, 101->42, 102->35, 103->3, 104->30, 105->43, 106->13, 107->20, 108->11, 109->14, 110->37, 111->32, 112->9, 113->26, 114->33, 115->1, 116->18, 117->11, 118->46, 119->32, 120->22, 121->8, 122->37, 123->44, 124->30, 125->40, 126->43, 127->5, 128->4, 129->47, 130->7, 131->44, 132->39, 133->45, 134->27, 135->5, 136->36, 137->16, 138->18, 139->4, 140->22, 141->33, 142->24, 143->6, 144->24, 145->46, 146->12, 147->0, 148->24, 149->45, 150->28, 151->31, 152->6, 153->21, 154->32, 155->2, 156->37, 157->34, 158->16, 159->45, 160->25, 161->25, 162->46, 163->18, 164->16, 165->38, 166->46, 167->39, 168->34, 169->34, 170->38, 171->5, 172->9, 173->39, 174->27, 175->11, 176->34, 177->20, 178->12, 179->22, 180->6, 181->3, 182->19, 183->38, 184->28, 185->32, 186->41, 187->20, 188->19, 189->19, 190->37, 191->0, 192->40, 193->27, 194->28, 195->10, 196->0, 197->19, 198->16, 199->38, 200->1, 201->2, 202->1, 203->22, 204->17, 205->30, 206->39, 207->13, 208->6, 209->29, 210->7, 211->7, 212->2, 213->24, 214->14, 215->42, 216->12, 217->21, 218->2, 219->0, 220->15, 221->8, 222->4, 223->9, 224->16, 225->11, 226->10, 227->29, 228->41, 229->15, 230->33, 231->26, 232->15, 233->26, 234->37, 235->26, 236->23, 237->1, 238->44, 239->17, 240->31, 241->21, 242->22, 243->31, 244->31, 245->25, 246->25, 247->1, 248->4, 249->47, 250->45, 251->6, 252->31, 253->7, 254->24, 255->30, 256->15, 257->8, 258->43, 259->18, 260->30, 261->47, 262->2, 263->43, 264->46, 265->15, 266->35, 267->13, 268->5, 269->1, 270->46, 271->11, 272->0, 273->8, 274->41, 275->18, 276->6, 277->32, 278->23, 279->33, 280->47, 281->21, 282->2, 283->34, 284->4, 285->20, 286->44, 287->27
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.92029	1.55092	1.56958	2.05578	1.86207	7.86711
	2.00321	2.39723	3.34325	10.6515	2.13238	2.04224
	4.26831	1.38282	1.17312	1.8878	10.8685	4.25705
	2.89053	2.15891	1.35148	1.56373	2.62977	3.75381
	2.05259	12.0084	1.50618	5.74783	1.82674	2.12982
	7.34826	1.84044	1.14757	1.95146	11.8693	4.52901
	7.52292	12.0368	11.5815	12.5107	5.95081	7.1669
	2.08514	3.14175	11.4167	13.3153	11.3722	6.3603
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	1	0	0	0	1
	1	0	0	0	1	1
	1	1	1	1	1	1
	1	1	1	1	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 9 --global-tlim 90 --seed 1

Starting the Optimization Process
