==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 3 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 801

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.828/0.828
- lns time: 0.827989
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.939/1.767
- lns time: 0.938699
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.752/3.519
- lns time: 1.752366
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.119/4.638
- lns time: 1.118807
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007488
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.005489
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008306
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.005629
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008666
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004686
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007222
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 1.363336
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 1.883929
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004548
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.009925
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004724
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004541
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004451
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.005019
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006686
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008131
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008294
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006432
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.012159
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006278
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.009397
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004411
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006661
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007652
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007087
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008301
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007246
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 0.817913
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.010172
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.281/64.174
- lns time: 1.281376
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.005538
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.022488
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.906/69.108
- lns time: 0.905813
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.004560
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.010411
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.609/74.732
- lns time: 1.609520
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.004615
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.007692
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.007785
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.006763
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.005169
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.004859
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.006354
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 1.233972
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 18

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 12
final number of satisfied efficiency constraints: 18
number of iterations: 49
total time: 90.009555

Final Solution: 0->8, 1->1, 2->24, 3->42, 4->40, 5->35, 6->18, 7->18, 8->4, 9->18, 10->25, 11->29, 12->26, 13->8, 14->30, 15->36, 16->36, 17->47, 18->45, 19->2, 20->34, 21->24, 22->42, 23->5, 24->21, 25->33, 26->45, 27->11, 28->13, 29->29, 30->32, 31->15, 32->6, 33->13, 34->43, 35->22, 36->29, 37->37, 38->47, 39->32, 40->20, 41->31, 42->46, 43->5, 44->40, 45->32, 46->31, 47->23, 48->19, 49->16, 50->4, 51->12, 52->27, 53->43, 54->38, 55->13, 56->0, 57->4, 58->39, 59->26, 60->35, 61->22, 62->17, 63->25, 64->15, 65->2, 66->1, 67->41, 68->30, 69->10, 70->31, 71->20, 72->12, 73->10, 74->37, 75->23, 76->12, 77->9, 78->15, 79->14, 80->7, 81->41, 82->9, 83->40, 84->23, 85->26, 86->35, 87->2, 88->8, 89->9, 90->2, 91->6, 92->21, 93->4, 94->24, 95->41, 96->32, 97->42, 98->9, 99->33, 100->11, 101->0, 102->39, 103->5, 104->44, 105->28, 106->24, 107->12, 108->31, 109->39, 110->10, 111->28, 112->43, 113->20, 114->46, 115->11, 116->14, 117->47, 118->11, 119->10, 120->9, 121->9, 122->45, 123->16, 124->21, 125->29, 126->0, 127->6, 128->44, 129->12, 130->30, 131->35, 132->5, 133->28, 134->11, 135->29, 136->24, 137->0, 138->17, 139->8, 140->30, 141->25, 142->23, 143->39, 144->13, 145->20, 146->47, 147->36, 148->14, 149->10, 150->38, 151->14, 152->17, 153->31, 154->24, 155->7, 156->15, 157->27, 158->19, 159->19, 160->27, 161->40, 162->18, 163->16, 164->11, 165->17, 166->6, 167->3, 168->38, 169->35, 170->1, 171->44, 172->37, 173->25, 174->22, 175->33, 176->43, 177->36, 178->37, 179->34, 180->36, 181->32, 182->2, 183->28, 184->4, 185->7, 186->26, 187->19, 188->40, 189->39, 190->38, 191->16, 192->41, 193->32, 194->5, 195->18, 196->20, 197->47, 198->45, 199->20, 200->34, 201->15, 202->16, 203->19, 204->23, 205->40, 206->17, 207->41, 208->14, 209->0, 210->10, 211->21, 212->3, 213->38, 214->34, 215->33, 216->28, 217->16, 218->15, 219->45, 220->2, 221->33, 222->46, 223->22, 224->46, 225->3, 226->46, 227->34, 228->46, 229->29, 230->33, 231->3, 232->23, 233->30, 234->44, 235->3, 236->42, 237->34, 238->8, 239->42, 240->44, 241->43, 242->7, 243->3, 244->5, 245->31, 246->27, 247->1, 248->4, 249->8, 250->18, 251->37, 252->25, 253->7, 254->30, 255->26, 256->13, 257->42, 258->7, 259->25, 260->22, 261->44, 262->39, 263->13, 264->21, 265->0, 266->6, 267->14, 268->35, 269->26, 270->17, 271->1, 272->38, 273->45, 274->36, 275->21, 276->27, 277->22, 278->43, 279->6, 280->12, 281->1, 282->19, 283->28, 284->41, 285->47, 286->27, 287->37

Average CPI map:
	1.18,	1.67,	4.43,	2.07,	3.52,	1.79
	7.25,	2.31,	2.28,	2.20,	2.17,	2.52
	2.07,	2.06,	2.06,	2.05,	2.05,	2.11
	2.15,	2.05,	1.86,	3.23,	2.03,	2.03
	7.08,	2.01,	1.99,	1.99,	1.99,	2.00
	2.21,	0.88,	1.99,	1.99,	1.99,	2.08
	2.01,	1.98,	1.98,	1.98,	1.99,	5.66
	1.96,	1.96,	1.79,	2.40,	2.74,	2.17

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	0,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	0,	0,	0,	0,	1,	1
