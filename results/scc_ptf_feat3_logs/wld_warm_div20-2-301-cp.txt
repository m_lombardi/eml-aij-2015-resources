==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 2 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 301

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007000
- lns branches: 12851
- lns fails: 6254
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 12891
- lns fails: 6342
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.449/4.465
- branches (local/global): 3417/29159
- fails (local/global): 1606/14202
- lns time: 0.456000
- lns branches: 3417
- lns fails: 1606
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 13345
- lns fails: 6428
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 24387
- lns fails: 11770
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009000
- lns branches: 15196
- lns fails: 7376
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 15530
- lns fails: 7518
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 10752
- lns fails: 5201
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 14999
- lns fails: 7268
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009000
- lns branches: 16687
- lns fails: 8105
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 18372
- lns fails: 8926
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 11539
- lns fails: 5601
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 12828
- lns fails: 6266
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009000
- lns branches: 16591
- lns fails: 8085
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 13970
- lns fails: 6844
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.274/28.845
- branches (local/global): 1336/214691
- fails (local/global): 602/104192
- lns time: 0.282000
- lns branches: 1336
- lns fails: 602
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009000
- lns branches: 23057
- lns fails: 11213
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.021/30.883
- branches (local/global): 68/237816
- fails (local/global): 21/115426
- lns time: 0.029000
- lns branches: 68
- lns fails: 21
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.049/30.940
- branches (local/global): 764/238580
- fails (local/global): 354/115780
- lns time: 0.057000
- lns branches: 764
- lns fails: 354
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 12610
- lns fails: 6176
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 25133
- lns fails: 12142
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 11250
- lns fails: 5493
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.017000
- lns branches: 18248
- lns fails: 8927
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.030/39.020
- branches (local/global): 613/306434
- fails (local/global): 283/148801
- lns time: 0.038000
- lns branches: 613
- lns fails: 283
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.723/39.751
- branches (local/global): 8763/315197
- fails (local/global): 4200/153001
- lns time: 0.731000
- lns branches: 8763
- lns fails: 4200
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.011000
- lns branches: 21751
- lns fails: 10697
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 8601
- lns fails: 4117
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 19497
- lns fails: 9552
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 12257
- lns fails: 5861
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 11433
- lns fails: 5615
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007000
- lns branches: 19565
- lns fails: 9565
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 13172
- lns fails: 6326
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.957/54.774
- branches (local/global): 10017/431490
- fails (local/global): 4802/209536
- lns time: 0.964000
- lns branches: 10017
- lns fails: 4802
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.013000
- lns branches: 11576
- lns fails: 5630
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 23202
- lns fails: 11310
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 15381
- lns fails: 7516
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 17848
- lns fails: 8737
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.021/62.840
- branches (local/global): 27/499524
- fails (local/global): 0/242729
- lns time: 0.029000
- lns branches: 27
- lns fails: 0
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 10300
- lns fails: 4987
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.054/64.911
- branches (local/global): 256/510080
- fails (local/global): 108/247824
- lns time: 0.063000
- lns branches: 256
- lns fails: 108
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.009000
- lns branches: 15663
- lns fails: 7673
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 11451
- lns fails: 5565
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 12776
- lns fails: 6255
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 7797
- lns fails: 3801
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 28378
- lns fails: 13845
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 11687
- lns fails: 5648
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 8289
- lns fails: 4015
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.009000
- lns branches: 12754
- lns fails: 6174
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 20390
- lns fails: 10009
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 6942
- lns fails: 3379
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.101/86.103
- branches (local/global): 8331/654538
- fails (local/global): 4004/318192
- lns time: 1.109000
- lns branches: 8331
- lns fails: 4004
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.007000
- lns branches: 15247
- lns fails: 7507
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 1.895000
- lns branches: 11628
- lns fails: 5653

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 28
number of iterations: 53
total time: 90013.000000
total branches: 681413
total fails: 331352

Final Solution: 0->27, 1->33, 2->25, 3->26, 4->42, 5->32, 6->29, 7->30, 8->8, 9->26, 10->47, 11->24, 12->7, 13->3, 14->40, 15->16, 16->9, 17->40, 18->20, 19->24, 20->27, 21->31, 22->23, 23->6, 24->39, 25->7, 26->34, 27->6, 28->18, 29->2, 30->22, 31->44, 32->41, 33->17, 34->24, 35->28, 36->39, 37->26, 38->6, 39->29, 40->21, 41->31, 42->41, 43->26, 44->23, 45->35, 46->38, 47->0, 48->3, 49->25, 50->41, 51->24, 52->37, 53->36, 54->27, 55->27, 56->11, 57->6, 58->30, 59->16, 60->3, 61->10, 62->44, 63->21, 64->44, 65->27, 66->42, 67->13, 68->28, 69->19, 70->17, 71->9, 72->37, 73->28, 74->2, 75->14, 76->18, 77->23, 78->27, 79->35, 80->17, 81->46, 82->33, 83->11, 84->43, 85->45, 86->26, 87->44, 88->12, 89->23, 90->6, 91->22, 92->14, 93->0, 94->47, 95->35, 96->17, 97->34, 98->22, 99->21, 100->38, 101->44, 102->18, 103->32, 104->45, 105->47, 106->10, 107->15, 108->32, 109->30, 110->10, 111->2, 112->39, 113->39, 114->7, 115->10, 116->41, 117->44, 118->3, 119->15, 120->28, 121->25, 122->30, 123->40, 124->43, 125->34, 126->21, 127->1, 128->7, 129->4, 130->24, 131->46, 132->43, 133->37, 134->36, 135->45, 136->9, 137->46, 138->12, 139->35, 140->17, 141->32, 142->13, 143->26, 144->33, 145->6, 146->8, 147->31, 148->18, 149->8, 150->12, 151->5, 152->23, 153->39, 154->20, 155->1, 156->16, 157->16, 158->37, 159->34, 160->28, 161->0, 162->11, 163->47, 164->12, 165->19, 166->29, 167->24, 168->12, 169->1, 170->21, 171->1, 172->20, 173->30, 174->5, 175->19, 176->12, 177->46, 178->43, 179->21, 180->47, 181->8, 182->43, 183->36, 184->32, 185->4, 186->33, 187->4, 188->5, 189->3, 190->37, 191->42, 192->22, 193->11, 194->46, 195->11, 196->31, 197->19, 198->33, 199->18, 200->5, 201->20, 202->35, 203->13, 204->43, 205->45, 206->0, 207->36, 208->0, 209->42, 210->23, 211->34, 212->9, 213->38, 214->30, 215->14, 216->15, 217->25, 218->40, 219->36, 220->42, 221->15, 222->40, 223->25, 224->46, 225->7, 226->16, 227->41, 228->16, 229->29, 230->35, 231->13, 232->38, 233->5, 234->33, 235->4, 236->36, 237->20, 238->11, 239->31, 240->0, 241->2, 242->32, 243->37, 244->5, 245->8, 246->18, 247->4, 248->2, 249->20, 250->14, 251->15, 252->47, 253->8, 254->7, 255->34, 256->38, 257->15, 258->4, 259->13, 260->3, 261->14, 262->45, 263->38, 264->29, 265->28, 266->22, 267->29, 268->14, 269->22, 270->17, 271->10, 272->40, 273->19, 274->10, 275->25, 276->1, 277->42, 278->31, 279->13, 280->1, 281->45, 282->2, 283->9, 284->39, 285->41, 286->19, 287->9

Average CPI map:
	2.02,	2.01,	1.64,	5.19,	2.04,	2.02
	4.60,	4.69,	2.68,	4.02,	2.87,	3.48
	2.28,	0.93,	1.71,	2.20,	11.19,	2.18
	2.32,	2.14,	1.14,	2.12,	2.11,	2.15
	2.07,	2.04,	1.04,	10.11,	2.02,	2.19
	3.94,	2.03,	1.57,	2.04,	1.55,	2.04
	2.05,	2.03,	2.04,	2.04,	10.01,	2.02
	2.05,	3.02,	1.39,	2.03,	2.64,	2.34

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	1,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	0,	1,	1
