==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 4 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 1

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.020000
- lns branches: 8442
- lns fails: 4136
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.088/2.108
- branches (local/global): 473/8915
- fails (local/global): 200/4336
- lns time: 0.099000
- lns branches: 473
- lns fails: 200
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.012/2.131
- branches (local/global): 32/8947
- fails (local/global): 2/4338
- lns time: 0.025000
- lns branches: 32
- lns fails: 2
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.025/2.169
- branches (local/global): 28/8975
- fails (local/global): 0/4338
- lns time: 0.035000
- lns branches: 28
- lns fails: 0
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.807/2.986
- branches (local/global): 13857/22832
- fails (local/global): 6630/10968
- lns time: 0.816000
- lns branches: 13857
- lns fails: 6630
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/3.010
- branches (local/global): 30/22862
- fails (local/global): 2/10970
- lns time: 0.024000
- lns branches: 30
- lns fails: 2
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.199/4.218
- branches (local/global): 20543/43405
- fails (local/global): 9870/20840
- lns time: 1.209000
- lns branches: 20543
- lns fails: 9870
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.009000
- lns branches: 5695
- lns fails: 2768
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.006/6.243
- branches (local/global): 27/49127
- fails (local/global): 0/23608
- lns time: 0.014000
- lns branches: 27
- lns fails: 0
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008000
- lns branches: 14107
- lns fails: 6857
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.009000
- lns branches: 4433
- lns fails: 2127
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.054/10.322
- branches (local/global): 1122/68789
- fails (local/global): 503/33095
- lns time: 0.063000
- lns branches: 1122
- lns fails: 503
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.013000
- lns branches: 7028
- lns fails: 3401
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.008000
- lns branches: 9576
- lns fails: 4672
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.009000
- lns branches: 15421
- lns fails: 7607
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.009000
- lns branches: 12497
- lns fails: 6052
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.013/18.383
- branches (local/global): 253/113564
- fails (local/global): 100/54927
- lns time: 0.021000
- lns branches: 253
- lns fails: 100
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.017/18.408
- branches (local/global): 252/113816
- fails (local/global): 100/55027
- lns time: 0.025000
- lns branches: 252
- lns fails: 100
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.014000
- lns branches: 9481
- lns fails: 4690
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008000
- lns branches: 9679
- lns fails: 4671
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 6038
- lns fails: 2967
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 8015
- lns fails: 3951
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 14904
- lns fails: 7381
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 7096
- lns fails: 3489
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 4929
- lns fails: 2381
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009000
- lns branches: 8065
- lns fails: 3916
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.024/34.516
- branches (local/global): 475/182498
- fails (local/global): 201/88674
- lns time: 0.032000
- lns branches: 475
- lns fails: 201
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 15471
- lns fails: 7661
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009000
- lns branches: 8571
- lns fails: 4186
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/38.556
- branches (local/global): 468/207008
- fails (local/global): 200/100721
- lns time: 0.023000
- lns branches: 468
- lns fails: 200
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.010000
- lns branches: 7434
- lns fails: 3601
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 5407
- lns fails: 2621
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 17420
- lns fails: 8599
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 5158
- lns fails: 2492
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.012000
- lns branches: 6341
- lns fails: 3125
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007000
- lns branches: 7044
- lns fails: 3401
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 10135
- lns fails: 4894
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 8635
- lns fails: 4219
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 6461
- lns fails: 3168
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 6704
- lns fails: 3256
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.010000
- lns branches: 5161
- lns fails: 2525
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.057/60.722
- branches (local/global): 1058/293966
- fails (local/global): 503/143125
- lns time: 0.068000
- lns branches: 1058
- lns fails: 503
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.010000
- lns branches: 10465
- lns fails: 5061
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 4034
- lns fails: 1971
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.011000
- lns branches: 9058
- lns fails: 4401
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 11563
- lns fails: 5703
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.014/68.785
- branches (local/global): 479/329565
- fails (local/global): 204/160465
- lns time: 0.022000
- lns branches: 479
- lns fails: 204
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 4514
- lns fails: 2179
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 29358
- lns fails: 14114
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 8392
- lns fails: 4109
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 4776
- lns fails: 2304
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 5338
- lns fails: 2585
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 12839
- lns fails: 6257
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 6893
- lns fails: 3352
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 7092
- lns fails: 3439
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 7467
- lns fails: 3676
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 23673
- lns fails: 11398
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.126000
- lns branches: 6881
- lns fails: 3301

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 9
final number of satisfied efficiency constraints: 23
number of iterations: 58
total time: 90008.000000
total branches: 446788
total fails: 217179

Final Solution: 0->26, 1->36, 2->40, 3->7, 4->4, 5->44, 6->25, 7->41, 8->5, 9->6, 10->5, 11->24, 12->8, 13->30, 14->45, 15->44, 16->34, 17->32, 18->20, 19->23, 20->18, 21->36, 22->12, 23->20, 24->9, 25->36, 26->13, 27->30, 28->21, 29->30, 30->8, 31->5, 32->15, 33->40, 34->35, 35->23, 36->36, 37->25, 38->0, 39->27, 40->41, 41->30, 42->16, 43->38, 44->30, 45->36, 46->46, 47->47, 48->47, 49->34, 50->40, 51->34, 52->11, 53->2, 54->23, 55->10, 56->39, 57->27, 58->23, 59->25, 60->32, 61->29, 62->41, 63->28, 64->37, 65->28, 66->17, 67->8, 68->31, 69->9, 70->7, 71->28, 72->23, 73->47, 74->12, 75->34, 76->2, 77->9, 78->21, 79->32, 80->21, 81->17, 82->35, 83->41, 84->17, 85->33, 86->6, 87->22, 88->5, 89->24, 90->17, 91->30, 92->4, 93->5, 94->2, 95->34, 96->35, 97->24, 98->1, 99->3, 100->14, 101->34, 102->3, 103->45, 104->25, 105->11, 106->26, 107->28, 108->9, 109->13, 110->4, 111->0, 112->3, 113->12, 114->4, 115->13, 116->15, 117->26, 118->1, 119->36, 120->47, 121->4, 122->4, 123->5, 124->2, 125->6, 126->43, 127->18, 128->44, 129->17, 130->37, 131->26, 132->1, 133->26, 134->31, 135->45, 136->26, 137->19, 138->41, 139->1, 140->25, 141->14, 142->16, 143->40, 144->19, 145->39, 146->40, 147->22, 148->15, 149->6, 150->43, 151->39, 152->46, 153->31, 154->38, 155->25, 156->27, 157->6, 158->19, 159->43, 160->33, 161->16, 162->9, 163->39, 164->35, 165->11, 166->11, 167->37, 168->20, 169->41, 170->40, 171->29, 172->24, 173->35, 174->35, 175->13, 176->29, 177->38, 178->16, 179->19, 180->22, 181->2, 182->14, 183->19, 184->37, 185->46, 186->45, 187->27, 188->44, 189->21, 190->15, 191->20, 192->7, 193->17, 194->0, 195->0, 196->39, 197->37, 198->8, 199->44, 200->21, 201->24, 202->18, 203->46, 204->1, 205->16, 206->2, 207->3, 208->22, 209->1, 210->47, 211->27, 212->33, 213->10, 214->19, 215->38, 216->8, 217->37, 218->28, 219->42, 220->12, 221->42, 222->43, 223->14, 224->21, 225->7, 226->45, 227->29, 228->32, 229->44, 230->16, 231->7, 232->39, 233->31, 234->0, 235->0, 236->46, 237->18, 238->14, 239->11, 240->18, 241->42, 242->14, 243->33, 244->20, 245->24, 246->29, 247->20, 248->10, 249->10, 250->47, 251->27, 252->15, 253->6, 254->46, 255->10, 256->3, 257->11, 258->38, 259->13, 260->45, 261->32, 262->7, 263->13, 264->42, 265->32, 266->42, 267->33, 268->29, 269->12, 270->3, 271->8, 272->31, 273->43, 274->15, 275->18, 276->9, 277->43, 278->31, 279->10, 280->22, 281->23, 282->33, 283->28, 284->22, 285->12, 286->38, 287->42

Average CPI map:
	1.66,	2.67,	2.62,	2.09,	2.19,	1.18
	2.03,	2.07,	1.44,	1.36,	2.65,	2.33
	2.00,	1.68,	2.00,	1.99,	1.98,	2.06
	2.14,	1.95,	1.95,	1.95,	1.94,	2.39
	2.06,	0.67,	1.39,	1.62,	2.15,	2.18
	2.09,	2.55,	1.94,	1.94,	1.40,	2.35
	2.02,	1.93,	1.93,	1.93,	1.16,	2.78
	2.09,	2.86,	1.92,	1.92,	2.57,	2.25

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	1,	1
