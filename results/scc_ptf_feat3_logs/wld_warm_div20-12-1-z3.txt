==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 12 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 1

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006040
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004655
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005523
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006593
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004956
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004409
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005261
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007341
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006307
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006229
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.011833
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009568
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.543/24.621
- lns time: 0.542681
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005630
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.003842
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004399
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.713/31.348
- lns time: 0.713192
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005875
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007919
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004406
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007261
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007178
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004851
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004922
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009136
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006177
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.020838
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006508
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004848
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.381/56.819
- lns time: 1.380899
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004864
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007558
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006693
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004406
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.014953
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006095
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008815
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010313
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008603
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.011852
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005019
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009277
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008943
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006585
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006149
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.012694
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.052282
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 23

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 23
number of iterations: 47
total time: 90.004378

Final Solution: 0->4, 1->14, 2->33, 3->34, 4->22, 5->42, 6->17, 7->36, 8->15, 9->41, 10->24, 11->22, 12->9, 13->44, 14->28, 15->33, 16->47, 17->28, 18->21, 19->24, 20->26, 21->21, 22->37, 23->27, 24->11, 25->14, 26->11, 27->34, 28->38, 29->32, 30->31, 31->41, 32->20, 33->29, 34->33, 35->40, 36->19, 37->32, 38->37, 39->40, 40->30, 41->4, 42->39, 43->47, 44->28, 45->2, 46->12, 47->25, 48->27, 49->1, 50->39, 51->24, 52->6, 53->35, 54->43, 55->2, 56->18, 57->46, 58->45, 59->11, 60->39, 61->30, 62->18, 63->35, 64->36, 65->9, 66->35, 67->3, 68->42, 69->41, 70->10, 71->47, 72->45, 73->8, 74->7, 75->13, 76->17, 77->18, 78->28, 79->23, 80->14, 81->5, 82->18, 83->16, 84->35, 85->39, 86->29, 87->44, 88->38, 89->27, 90->35, 91->7, 92->8, 93->41, 94->40, 95->34, 96->22, 97->20, 98->38, 99->25, 100->2, 101->31, 102->23, 103->46, 104->0, 105->33, 106->43, 107->37, 108->31, 109->44, 110->19, 111->6, 112->46, 113->3, 114->37, 115->7, 116->23, 117->17, 118->20, 119->45, 120->3, 121->10, 122->1, 123->47, 124->1, 125->45, 126->8, 127->43, 128->16, 129->23, 130->39, 131->29, 132->31, 133->38, 134->17, 135->47, 136->16, 137->37, 138->42, 139->46, 140->39, 141->30, 142->40, 143->28, 144->8, 145->12, 146->33, 147->12, 148->23, 149->42, 150->45, 151->41, 152->31, 153->21, 154->42, 155->0, 156->22, 157->38, 158->43, 159->36, 160->14, 161->15, 162->7, 163->5, 164->3, 165->26, 166->6, 167->17, 168->43, 169->13, 170->21, 171->26, 172->27, 173->4, 174->12, 175->0, 176->11, 177->18, 178->46, 179->47, 180->44, 181->45, 182->5, 183->5, 184->30, 185->26, 186->1, 187->6, 188->29, 189->22, 190->4, 191->19, 192->16, 193->8, 194->21, 195->16, 196->20, 197->6, 198->25, 199->36, 200->29, 201->27, 202->4, 203->25, 204->2, 205->19, 206->10, 207->9, 208->10, 209->15, 210->40, 211->10, 212->0, 213->19, 214->26, 215->8, 216->27, 217->25, 218->0, 219->18, 220->28, 221->13, 222->1, 223->12, 224->38, 225->41, 226->19, 227->9, 228->10, 229->6, 230->32, 231->31, 232->30, 233->36, 234->24, 235->0, 236->3, 237->42, 238->29, 239->46, 240->26, 241->24, 242->43, 243->9, 244->33, 245->5, 246->34, 247->15, 248->44, 249->20, 250->23, 251->21, 252->3, 253->34, 254->34, 255->13, 256->24, 257->35, 258->14, 259->40, 260->2, 261->4, 262->2, 263->15, 264->32, 265->1, 266->30, 267->12, 268->25, 269->22, 270->36, 271->44, 272->13, 273->20, 274->11, 275->32, 276->7, 277->13, 278->17, 279->9, 280->14, 281->32, 282->7, 283->5, 284->16, 285->37, 286->11, 287->15

Average CPI map:
	0.90,	7.30,	7.30,	2.19,	2.04,	1.55
	6.19,	6.16,	6.03,	12.20,	7.69,	1.43
	5.19,	4.95,	4.67,	4.55,	11.45,	3.84
	2.16,	3.56,	3.02,	3.02,	2.93,	2.46
	2.40,	6.09,	2.28,	2.28,	2.25,	2.23
	2.21,	2.18,	8.91,	2.16,	2.17,	2.16
	2.15,	2.12,	2.12,	2.12,	2.12,	2.12
	2.12,	2.58,	2.12,	2.13,	2.10,	2.10

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	1,	0,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
