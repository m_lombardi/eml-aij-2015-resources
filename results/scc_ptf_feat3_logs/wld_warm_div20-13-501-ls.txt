LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
1.6502, 2.00333, 1.9065, 2.11556, 2.03108, 2.29334, 1.64123, 2.18834, 2.01249, 2.11594, 2.07649, 1.96551, 1.87434, 1.94937, 1.78433, 2.04827, 2.08405, 2.25964, 3.83077, 0.5, 1.34283, 1.78054, 1.86379, 2.68208, 0.5, 0.632387, 2.72557, 1.51653, 3.92585, 2.69967, 0.854641, 2.00383, 1.64434, 2.4628, 2.56279, 2.47159, 2.2666, 2.78582, 1.04079, 2.35568, 1.89932, 1.65177, 1.69302, 0.5, 1.33046, 2.11318, 1.29651, 5.06683, 0.5, 0.674983, 2.83931, 4.16687, 1.92577, 1.89306, 1.85307, 2.29043, 1.97125, 2.28525, 1.74132, 1.85868, 2.1023, 1.69577, 0.5, 4.78543, 1.17135, 1.74515, 1.49476, 2.15464, 1.68504, 2.9912, 0.5, 3.17436, 3.25083, 1.08147, 4.28784, 0.766191, 1.41715, 1.19651, 3.16454, 1.22029, 2.6492, 0.5, 3.17416, 1.29181, 3.4715, 2.55399, 1.29515, 0.598873, 2.42992, 1.65057, 0.5, 2.19827, 1.40764, 5.2324, 0.530361, 2.13134, 0.552511, 1.63958, 1.58991, 2.25201, 2.78607, 3.17992, 2.68404, 1.50343, 1.73646, 1.41908, 2.0783, 2.57868, 1.23405, 0.5, 3.04146, 0.519661, 4.36761, 2.33723, 2.55193, 1.2158, 1.77266, 2.66243, 1.81623, 1.98095, 4.08448, 1.49901, 0.5, 1.13612, 2.26147, 2.51892, 2.01921, 2.31561, 2.40868, 0.835971, 2.32472, 2.0958, 8.98374, 5.98729, 12.3374, 10.3917, 11.0733, 11.2266, 1.82936, 2.02405, 2.01205, 2.00186, 2.04013, 2.09254, 0.5, 2.00094, 2.607, 2.37311, 3.10599, 1.41296, 2.56858, 3.26128, 2.67461, 2.48799, 0.5, 0.507533, 1.00187, 3.477, 3.25426, 0.5, 0.60843, 3.15844, 2.41498, 0.5, 1.17888, 3.0427, 2.66682, 2.19662, 0.5, 3.5692, 2.12025, 1.02808, 4.10788, 0.674602, 0.710945, 3.87408, 0.5, 3.94802, 2.41269, 0.554262, 3.60707, 1.17239, 1.93735, 3.0093, 1.26611, 1.00778, 0.5, 0.679767, 2.59624, 0.922478, 4.00551, 3.296, 2.32441, 1.9184, 1.74826, 2.05557, 1.95495, 1.99841, 1.89109, 2.17257, 1.11112, 2.36627, 2.16235, 2.2966, 2.95795, 2.21251, 1.45507, 3.84861, 1.02586, 0.5, 4.88615, 2.812, 1.24048, 0.890107, 1.24751, 0.923747, 3.27567, 1.25894, 2.99415, 0.51144, 1.76212, 2.19768, 2.19369, 2.05351, 1.97408, 0.96577, 2.39755, 2.4154, 3.9962, 2.97263, 1.52039, 0.5, 2.02982, 0.980961, 0.5, 2.79523, 1.75675, 3.19911, 0.979511, 2.76939, 2.44806, 1.02196, 0.5, 3.54448, 3.12757, 1.35794, 3.98877, 0.5, 1.78633, 2.72841, 2.17012, 0.826369, 3.50534, 0.968886, 3.36137, 0.646376, 3.01803, 0.5, 1.81909, 2.06064, 1.95243, 2.11472, 1.86814, 2.18497, 1.70376, 2.80852, 1.5081, 0.710288, 2.90055, 2.36879, 3.06217, 1.79808, 1.54725, 0.661954, 3.50832, 1.42223, 3.10937, 3.01357, 1.88328, 1.46832, 2.02546, 0.5, 3.14065, 2.85111, 0.700269, 2.8563, 0.5, 1.95167
cpi min/max: 0.5/12.3374
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55157, operands = 136540
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 501, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 19149 itr]: obj = (21), mov = 19149, inf = 70.9%, acc = 19.3%, imp = 158

number of satisfied efficiency constraints: 21
[2 sec, 45430 itr]: obj = (21), mov = 45430, inf = 69.7%, acc = 19.2%, imp = 158

number of satisfied efficiency constraints: 21
[3 sec, 62498 itr]: obj = (21), mov = 62498, inf = 63.1%, acc = 23%, imp = 158

number of satisfied efficiency constraints: 21
[4 sec, 77647 itr]: obj = (23), mov = 77647, inf = 57%, acc = 26.2%, imp = 160

number of satisfied efficiency constraints: 23
[5 sec, 94675 itr]: obj = (23), mov = 94675, inf = 51%, acc = 28.9%, imp = 160

number of satisfied efficiency constraints: 23
[6 sec, 112952 itr]: obj = (23), mov = 112952, inf = 46.2%, acc = 30.9%, imp = 160

number of satisfied efficiency constraints: 23
[7 sec, 131457 itr]: obj = (23), mov = 131457, inf = 42.7%, acc = 32.3%, imp = 160

number of satisfied efficiency constraints: 23
[8 sec, 147024 itr]: obj = (23), mov = 147024, inf = 40.4%, acc = 34.1%, imp = 188

number of satisfied efficiency constraints: 23
[9 sec, 162172 itr]: obj = (23), mov = 162172, inf = 38.3%, acc = 35.5%, imp = 188

number of satisfied efficiency constraints: 23
[10 sec, 176882 itr]: obj = (23), mov = 176882, inf = 36.6%, acc = 36.4%, imp = 188

number of satisfied efficiency constraints: 23
[11 sec, 192717 itr]: obj = (23), mov = 192717, inf = 35.1%, acc = 37.1%, imp = 189

number of satisfied efficiency constraints: 23
[12 sec, 208301 itr]: obj = (23), mov = 208301, inf = 33.7%, acc = 37.6%, imp = 190

number of satisfied efficiency constraints: 23
[13 sec, 224896 itr]: obj = (23), mov = 224896, inf = 32.4%, acc = 37.8%, imp = 190

number of satisfied efficiency constraints: 23
[14 sec, 241656 itr]: obj = (23), mov = 241656, inf = 31.4%, acc = 38%, imp = 190

number of satisfied efficiency constraints: 23
[15 sec, 257863 itr]: obj = (23), mov = 257863, inf = 30.5%, acc = 38.1%, imp = 190

number of satisfied efficiency constraints: 23
[16 sec, 273835 itr]: obj = (23), mov = 273835, inf = 29.7%, acc = 38.2%, imp = 190

number of satisfied efficiency constraints: 23
[17 sec, 290725 itr]: obj = (23), mov = 290725, inf = 29%, acc = 38.2%, imp = 190

number of satisfied efficiency constraints: 23
[18 sec, 307330 itr]: obj = (23), mov = 307330, inf = 28.3%, acc = 38.3%, imp = 190

number of satisfied efficiency constraints: 23
[19 sec, 323658 itr]: obj = (23), mov = 323658, inf = 27.8%, acc = 38.4%, imp = 190

number of satisfied efficiency constraints: 23
[20 sec, 340319 itr]: obj = (23), mov = 340319, inf = 27.3%, acc = 38.5%, imp = 190

number of satisfied efficiency constraints: 23
[21 sec, 357621 itr]: obj = (23), mov = 357621, inf = 26.8%, acc = 38.5%, imp = 190

number of satisfied efficiency constraints: 23
[22 sec, 373423 itr]: obj = (23), mov = 373423, inf = 26.4%, acc = 38.6%, imp = 228

number of satisfied efficiency constraints: 23
[23 sec, 386529 itr]: obj = (23), mov = 386529, inf = 26.1%, acc = 39.1%, imp = 229

number of satisfied efficiency constraints: 23
[24 sec, 400697 itr]: obj = (23), mov = 400697, inf = 25.7%, acc = 39.4%, imp = 229

number of satisfied efficiency constraints: 23
[25 sec, 414656 itr]: obj = (23), mov = 414656, inf = 25.4%, acc = 39.7%, imp = 229

number of satisfied efficiency constraints: 23
[26 sec, 429960 itr]: obj = (23), mov = 429960, inf = 25.1%, acc = 39.9%, imp = 230

number of satisfied efficiency constraints: 23
[27 sec, 445320 itr]: obj = (23), mov = 445320, inf = 24.8%, acc = 39.9%, imp = 230

number of satisfied efficiency constraints: 23
[28 sec, 460496 itr]: obj = (23), mov = 460496, inf = 24.5%, acc = 40.1%, imp = 230

number of satisfied efficiency constraints: 23
[29 sec, 476034 itr]: obj = (23), mov = 476034, inf = 24.3%, acc = 40.1%, imp = 231

number of satisfied efficiency constraints: 23
[30 sec, 493222 itr]: obj = (24), mov = 493222, inf = 24%, acc = 39.9%, imp = 232

number of satisfied efficiency constraints: 24
[31 sec, 510341 itr]: obj = (24), mov = 510341, inf = 23.7%, acc = 39.8%, imp = 232

number of satisfied efficiency constraints: 24
[32 sec, 527559 itr]: obj = (24), mov = 527559, inf = 23.5%, acc = 39.6%, imp = 232

number of satisfied efficiency constraints: 24
[33 sec, 545234 itr]: obj = (24), mov = 545234, inf = 23.2%, acc = 39.5%, imp = 232

number of satisfied efficiency constraints: 24
[34 sec, 562556 itr]: obj = (24), mov = 562556, inf = 23%, acc = 39.3%, imp = 232

number of satisfied efficiency constraints: 24
[35 sec, 579670 itr]: obj = (24), mov = 579670, inf = 22.8%, acc = 39.2%, imp = 232

number of satisfied efficiency constraints: 24
[36 sec, 598234 itr]: obj = (24), mov = 598234, inf = 22.6%, acc = 39%, imp = 232

number of satisfied efficiency constraints: 24
[37 sec, 616183 itr]: obj = (24), mov = 616183, inf = 22.4%, acc = 38.8%, imp = 232

number of satisfied efficiency constraints: 24
[38 sec, 633859 itr]: obj = (24), mov = 633859, inf = 22.2%, acc = 38.7%, imp = 232

number of satisfied efficiency constraints: 24
[39 sec, 651152 itr]: obj = (24), mov = 651152, inf = 22.1%, acc = 38.6%, imp = 232

number of satisfied efficiency constraints: 24
[40 sec, 668357 itr]: obj = (24), mov = 668357, inf = 21.9%, acc = 38.5%, imp = 232

number of satisfied efficiency constraints: 24
[41 sec, 685861 itr]: obj = (24), mov = 685861, inf = 21.7%, acc = 38.4%, imp = 232

number of satisfied efficiency constraints: 24
[42 sec, 703008 itr]: obj = (24), mov = 703008, inf = 21.6%, acc = 38.3%, imp = 232

number of satisfied efficiency constraints: 24
[43 sec, 720656 itr]: obj = (24), mov = 720656, inf = 21.5%, acc = 38.2%, imp = 232

number of satisfied efficiency constraints: 24
[44 sec, 733410 itr]: obj = (24), mov = 733410, inf = 21.4%, acc = 38.5%, imp = 274

number of satisfied efficiency constraints: 24
[45 sec, 748554 itr]: obj = (24), mov = 748554, inf = 21.2%, acc = 38.5%, imp = 276

number of satisfied efficiency constraints: 24
[46 sec, 765143 itr]: obj = (24), mov = 765143, inf = 21.1%, acc = 38.5%, imp = 276

number of satisfied efficiency constraints: 24
[47 sec, 781285 itr]: obj = (24), mov = 781285, inf = 21%, acc = 38.5%, imp = 276

number of satisfied efficiency constraints: 24
[48 sec, 797420 itr]: obj = (24), mov = 797420, inf = 20.9%, acc = 38.5%, imp = 276

number of satisfied efficiency constraints: 24
[49 sec, 813309 itr]: obj = (24), mov = 813309, inf = 20.8%, acc = 38.5%, imp = 276

number of satisfied efficiency constraints: 24
[50 sec, 829467 itr]: obj = (24), mov = 829467, inf = 20.7%, acc = 38.5%, imp = 276

number of satisfied efficiency constraints: 24
[51 sec, 844035 itr]: obj = (24), mov = 844035, inf = 20.7%, acc = 38.6%, imp = 321

number of satisfied efficiency constraints: 24
[52 sec, 859924 itr]: obj = (24), mov = 859924, inf = 20.6%, acc = 38.7%, imp = 322

number of satisfied efficiency constraints: 24
[53 sec, 876169 itr]: obj = (24), mov = 876169, inf = 20.5%, acc = 38.6%, imp = 322

number of satisfied efficiency constraints: 24
[54 sec, 892991 itr]: obj = (24), mov = 892991, inf = 20.4%, acc = 38.6%, imp = 322

number of satisfied efficiency constraints: 24
[55 sec, 909359 itr]: obj = (24), mov = 909359, inf = 20.4%, acc = 38.6%, imp = 322

number of satisfied efficiency constraints: 24
[56 sec, 925819 itr]: obj = (24), mov = 925819, inf = 20.3%, acc = 38.6%, imp = 322

number of satisfied efficiency constraints: 24
[57 sec, 942118 itr]: obj = (24), mov = 942118, inf = 20.2%, acc = 38.6%, imp = 322

number of satisfied efficiency constraints: 24
[58 sec, 956997 itr]: obj = (24), mov = 956997, inf = 20.1%, acc = 38.6%, imp = 361

number of satisfied efficiency constraints: 24
[59 sec, 972144 itr]: obj = (24), mov = 972144, inf = 20.1%, acc = 38.7%, imp = 363

number of satisfied efficiency constraints: 24
[60 sec, 987740 itr]: obj = (24), mov = 987740, inf = 20%, acc = 38.7%, imp = 363

number of satisfied efficiency constraints: 24
[61 sec, 1003487 itr]: obj = (24), mov = 1003487, inf = 19.9%, acc = 38.7%, imp = 363

number of satisfied efficiency constraints: 24
[62 sec, 1019845 itr]: obj = (24), mov = 1019845, inf = 19.9%, acc = 38.7%, imp = 363

number of satisfied efficiency constraints: 24
[63 sec, 1036418 itr]: obj = (24), mov = 1036418, inf = 19.8%, acc = 38.7%, imp = 363

number of satisfied efficiency constraints: 24
[64 sec, 1052969 itr]: obj = (24), mov = 1052969, inf = 19.8%, acc = 38.6%, imp = 363

number of satisfied efficiency constraints: 24
[65 sec, 1068553 itr]: obj = (24), mov = 1068553, inf = 19.7%, acc = 38.6%, imp = 400

number of satisfied efficiency constraints: 24
[66 sec, 1082406 itr]: obj = (24), mov = 1082406, inf = 19.7%, acc = 38.7%, imp = 402

number of satisfied efficiency constraints: 24
[67 sec, 1097720 itr]: obj = (24), mov = 1097720, inf = 19.6%, acc = 38.8%, imp = 402

number of satisfied efficiency constraints: 24
[68 sec, 1113445 itr]: obj = (24), mov = 1113445, inf = 19.6%, acc = 38.8%, imp = 403

number of satisfied efficiency constraints: 24
[69 sec, 1129599 itr]: obj = (24), mov = 1129599, inf = 19.5%, acc = 38.8%, imp = 403

number of satisfied efficiency constraints: 24
[70 sec, 1145313 itr]: obj = (24), mov = 1145313, inf = 19.5%, acc = 38.7%, imp = 403

number of satisfied efficiency constraints: 24
[71 sec, 1161343 itr]: obj = (24), mov = 1161343, inf = 19.4%, acc = 38.7%, imp = 403

number of satisfied efficiency constraints: 24
[72 sec, 1177092 itr]: obj = (24), mov = 1177092, inf = 19.4%, acc = 38.7%, imp = 403

number of satisfied efficiency constraints: 24
[73 sec, 1193387 itr]: obj = (24), mov = 1193387, inf = 19.4%, acc = 38.7%, imp = 403

number of satisfied efficiency constraints: 24
[74 sec, 1207643 itr]: obj = (24), mov = 1207643, inf = 19.3%, acc = 38.7%, imp = 435

number of satisfied efficiency constraints: 24
[75 sec, 1222975 itr]: obj = (24), mov = 1222975, inf = 19.3%, acc = 38.7%, imp = 437

number of satisfied efficiency constraints: 24
[76 sec, 1239245 itr]: obj = (24), mov = 1239245, inf = 19.2%, acc = 38.7%, imp = 437

number of satisfied efficiency constraints: 24
[77 sec, 1255788 itr]: obj = (24), mov = 1255788, inf = 19.2%, acc = 38.7%, imp = 438

number of satisfied efficiency constraints: 24
[78 sec, 1273356 itr]: obj = (24), mov = 1273356, inf = 19.1%, acc = 38.6%, imp = 438

number of satisfied efficiency constraints: 24
[79 sec, 1290339 itr]: obj = (24), mov = 1290339, inf = 19.1%, acc = 38.5%, imp = 438

number of satisfied efficiency constraints: 24
[80 sec, 1307982 itr]: obj = (24), mov = 1307982, inf = 19%, acc = 38.4%, imp = 438

number of satisfied efficiency constraints: 24
[81 sec, 1325360 itr]: obj = (24), mov = 1325360, inf = 19%, acc = 38.4%, imp = 438

number of satisfied efficiency constraints: 24
[82 sec, 1342697 itr]: obj = (24), mov = 1342697, inf = 18.9%, acc = 38.3%, imp = 438

number of satisfied efficiency constraints: 24
[83 sec, 1358855 itr]: obj = (24), mov = 1358855, inf = 18.9%, acc = 38.3%, imp = 481

number of satisfied efficiency constraints: 24
[84 sec, 1373325 itr]: obj = (24), mov = 1373325, inf = 18.8%, acc = 38.3%, imp = 482

number of satisfied efficiency constraints: 24
[85 sec, 1388156 itr]: obj = (24), mov = 1388156, inf = 18.8%, acc = 38.4%, imp = 483

number of satisfied efficiency constraints: 24
[86 sec, 1404464 itr]: obj = (24), mov = 1404464, inf = 18.8%, acc = 38.4%, imp = 483

number of satisfied efficiency constraints: 24
[87 sec, 1420085 itr]: obj = (24), mov = 1420085, inf = 18.7%, acc = 38.4%, imp = 483

number of satisfied efficiency constraints: 24
[88 sec, 1436392 itr]: obj = (24), mov = 1436392, inf = 18.7%, acc = 38.4%, imp = 483

number of satisfied efficiency constraints: 24
[89 sec, 1452190 itr]: obj = (24), mov = 1452190, inf = 18.7%, acc = 38.3%, imp = 483

number of satisfied efficiency constraints: 24
[90 sec, 1467635 itr]: obj = (24), mov = 1467635, inf = 18.6%, acc = 38.3%, imp = 483

number of satisfied efficiency constraints: 24
[90 sec, 1467635 itr]: obj = (24), mov = 1467635, inf = 18.6%, acc = 38.3%, imp = 483

number of satisfied efficiency constraints: 24
1467635 iterations, 1467635 moves performed in 90 seconds
Feasible solution: obj = (24)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 24
Mapping: 0->24, 1->29, 2->17, 3->42, 4->28, 5->12, 6->32, 7->24, 8->14, 9->34, 10->35, 11->26, 12->47, 13->17, 14->28, 15->15, 16->34, 17->38, 18->3, 19->9, 20->26, 21->7, 22->20, 23->40, 24->32, 25->27, 26->4, 27->44, 28->45, 29->16, 30->21, 31->15, 32->28, 33->10, 34->3, 35->2, 36->32, 37->39, 38->31, 39->47, 40->39, 41->27, 42->18, 43->34, 44->32, 45->17, 46->41, 47->46, 48->37, 49->19, 50->2, 51->30, 52->19, 53->2, 54->22, 55->46, 56->42, 57->17, 58->30, 59->27, 60->20, 61->7, 62->41, 63->35, 64->27, 65->38, 66->9, 67->47, 68->18, 69->47, 70->12, 71->18, 72->14, 73->45, 74->11, 75->13, 76->21, 77->1, 78->43, 79->16, 80->12, 81->8, 82->43, 83->3, 84->23, 85->0, 86->27, 87->33, 88->32, 89->24, 90->45, 91->43, 92->2, 93->12, 94->44, 95->23, 96->27, 97->5, 98->11, 99->29, 100->44, 101->10, 102->13, 103->23, 104->13, 105->30, 106->14, 107->21, 108->22, 109->35, 110->3, 111->4, 112->43, 113->16, 114->20, 115->5, 116->34, 117->12, 118->20, 119->1, 120->42, 121->37, 122->7, 123->45, 124->42, 125->44, 126->5, 127->32, 128->43, 129->19, 130->25, 131->11, 132->25, 133->28, 134->36, 135->18, 136->1, 137->6, 138->47, 139->15, 140->21, 141->35, 142->8, 143->39, 144->6, 145->2, 146->3, 147->22, 148->40, 149->37, 150->36, 151->24, 152->46, 153->4, 154->1, 155->22, 156->0, 157->40, 158->40, 159->0, 160->14, 161->30, 162->38, 163->39, 164->25, 165->40, 166->37, 167->30, 168->6, 169->8, 170->7, 171->15, 172->0, 173->1, 174->8, 175->10, 176->19, 177->4, 178->17, 179->20, 180->19, 181->6, 182->29, 183->41, 184->4, 185->42, 186->39, 187->19, 188->24, 189->18, 190->47, 191->18, 192->31, 193->33, 194->9, 195->37, 196->34, 197->13, 198->21, 199->5, 200->34, 201->5, 202->33, 203->14, 204->38, 205->16, 206->25, 207->16, 208->9, 209->16, 210->38, 211->4, 212->9, 213->6, 214->33, 215->7, 216->8, 217->15, 218->23, 219->36, 220->44, 221->26, 222->11, 223->5, 224->0, 225->24, 226->35, 227->45, 228->9, 229->41, 230->31, 231->45, 232->13, 233->36, 234->7, 235->26, 236->37, 237->46, 238->42, 239->40, 240->33, 241->6, 242->25, 243->36, 244->43, 245->11, 246->46, 247->39, 248->44, 249->10, 250->15, 251->26, 252->41, 253->25, 254->10, 255->3, 256->38, 257->22, 258->26, 259->1, 260->31, 261->31, 262->13, 263->28, 264->33, 265->2, 266->14, 267->22, 268->29, 269->23, 270->21, 271->41, 272->36, 273->31, 274->29, 275->17, 276->46, 277->10, 278->28, 279->11, 280->35, 281->20, 282->0, 283->29, 284->30, 285->23, 286->8, 287->12
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	2.21308	2.91433	2.23684	2.33003	2.29323	1.91116
	2.55184	1.25338	1.76599	1.66759	3.10336	2.16558
	2.54817	1.84718	1.95913	1.75555	2.13639	2.01487
	3.52709	1.37059	1.56475	1.9693	1.19634	2.55425
	2.05207	2.56855	1.82445	1.19364	2.58588	2.57544
	2.23044	1.60245	1.7473	1.67983	1.58979	2.2978
	3.58169	1.64852	2.88032	1.37961	3.05524	2.18031
	2.07001	3.07347	1.81672	1.59314	3.38819	2.53512
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 13 --global-tlim 90 --seed 501

Starting the Optimization Process
