LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
3.1185, 0.5, 0.607702, 3.19726, 2.97682, 1.59972, 1.2667, 0.5, 1.84807, 2.67677, 4.31718, 1.39128, 1.38609, 0.740994, 2.87739, 1.35274, 0.5, 5.14279, 2.03451, 1.58177, 1.99084, 2.24611, 2.47489, 1.67187, 2.29591, 1.95594, 2.15617, 2.23197, 2.12958, 1.23042, 1.90417, 0.952039, 2.77433, 3.27355, 2.59592, 0.5, 2.30129, 2.03152, 1.79238, 2.58658, 2.56517, 0.723053, 1.15921, 2.5037, 3.06571, 0.5, 4.01994, 0.751436, 2.07061, 2.85755, 2.15705, 2.13, 0.5, 2.28479, 0.5, 1.89212, 1.03681, 4.97361, 1.26166, 2.3358, 2.69817, 2.25878, 2.13461, 1.27949, 2.41042, 1.21854, 0.5, 2.91184, 1.75848, 0.602156, 2.03868, 4.18885, 3.17262, 0.836678, 1.13818, 2.52222, 3.8303, 0.5, 2.28779, 1.48897, 1.85023, 2.37186, 2.18497, 1.81617, 3.47157, 0.5, 1.6529, 3.37429, 1.13977, 1.86148, 0.659983, 3.33868, 3.06632, 0.5, 1.68691, 2.7481, 3.04099, 1.37412, 0.5, 1.30755, 2.75621, 3.02113, 2.33381, 1.36575, 1.99757, 2.25412, 2.02993, 2.01883, 23.3234, 11.3647, 20.8728, 35, 31.9616, 9.47742, 17.4741, 30.6184, 1.65074, 35, 12.6022, 34.6546, 22.2602, 32.7888, 24.5756, 28.6538, 20.0375, 3.68398, 1.50353, 1.90833, 2.17959, 2.2546, 2.10717, 2.04678, 1.21772, 0.5, 3.54066, 2.46042, 0.735172, 3.54603, 0.553124, 0.5, 2.27941, 1.90978, 3.50422, 3.25347, 22.0891, 18.8224, 24.2305, 25.6473, 27.3808, 13.83, 3.17025, 3.30664, 2.79629, 0.593973, 1.61461, 0.51823, 18.4125, 26.3241, 23.8043, 11.2281, 24.5957, 27.6354, 1.65389, 0.636179, 3.22803, 3.47817, 2.50373, 0.5, 2.31526, 2.39553, 2.21396, 0.5, 2.08559, 2.48966, 3.2208, 0.708539, 1.15484, 2.55069, 2.18064, 2.18449, 5.55644, 18.2864, 34.1855, 29.3457, 16.0332, 28.5927, 35, 16.9505, 11.0546, 10.5481, 25.3906, 33.0562, 14.4612, 22.5954, 35, 19.9472, 21.931, 18.0652, 2.45174, 1.45843, 2.87827, 3.2761, 0.5, 1.43546, 3.29898, 2.95099, 1.35285, 1.76898, 1.68086, 0.947335, 0.73712, 4.07143, 3.22793, 2.51851, 0.813986, 0.631026, 3.64731, 2.31815, 2.46721, 1.19628, 0.5, 1.87105, 3.39384, 2.72169, 0.935464, 0.5, 2.88208, 1.56692, 3.40934, 1.73601, 2.65717, 2.75577, 0.5, 0.941706, 2.62079, 0.5, 2.58771, 2.08297, 1.81875, 2.38979, 2.53707, 2.16432, 2.21632, 1.37434, 1.3439, 2.36405, 3.73156, 1.77974, 0.728544, 0.5, 2.90481, 2.35535, 0.5, 1.41785, 6.76133, 0.581924, 2.05896, 0.679942, 1.02313, 0.99413, 2.11886, 3.93538, 3.4285, 0.5, 2.09843, 1.83453, 2.19878, 2.24731, 1.88714, 1.7338, 2.19896, 0.845662, 3.11492, 0.865433, 2.44446, 2.53056, 1.83412, 3.42751, 0.87598, 2.87187, 1.11936, 1.87116, 3.03655, 0.5, 2.99289, 1.82857, 2.1193, 1.5227
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54798, operands = 135836
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 27806 itr]: obj = (29), mov = 27806, inf = 70.6%, acc = 18.6%, imp = 162

number of satisfied efficiency constraints: 29
[2 sec, 55067 itr]: obj = (31), mov = 55067, inf = 68.1%, acc = 18.1%, imp = 164

number of satisfied efficiency constraints: 31
[3 sec, 75064 itr]: obj = (32), mov = 75064, inf = 61.2%, acc = 20.7%, imp = 165

number of satisfied efficiency constraints: 32
[4 sec, 96231 itr]: obj = (33), mov = 96231, inf = 53.6%, acc = 22.9%, imp = 166

number of satisfied efficiency constraints: 33
[5 sec, 118893 itr]: obj = (34), mov = 118893, inf = 47.5%, acc = 24.8%, imp = 167

number of satisfied efficiency constraints: 34
[6 sec, 143354 itr]: obj = (35), mov = 143354, inf = 42.8%, acc = 25.7%, imp = 168

number of satisfied efficiency constraints: 35
[7 sec, 168573 itr]: obj = (35), mov = 168573, inf = 39.5%, acc = 26.2%, imp = 168

number of satisfied efficiency constraints: 35
[8 sec, 193961 itr]: obj = (35), mov = 193961, inf = 37%, acc = 26.6%, imp = 168

number of satisfied efficiency constraints: 35
[9 sec, 219147 itr]: obj = (35), mov = 219147, inf = 35%, acc = 27%, imp = 168

number of satisfied efficiency constraints: 35
[10 sec, 244476 itr]: obj = (35), mov = 244476, inf = 33.4%, acc = 27.2%, imp = 168

number of satisfied efficiency constraints: 35
[11 sec, 270212 itr]: obj = (36), mov = 270212, inf = 32.1%, acc = 27.2%, imp = 169

number of satisfied efficiency constraints: 36
[12 sec, 296797 itr]: obj = (36), mov = 296797, inf = 31%, acc = 27.1%, imp = 169

number of satisfied efficiency constraints: 36
[13 sec, 322582 itr]: obj = (36), mov = 322582, inf = 30.1%, acc = 27.1%, imp = 169

number of satisfied efficiency constraints: 36
[14 sec, 348606 itr]: obj = (37), mov = 348606, inf = 29.3%, acc = 27%, imp = 170

number of satisfied efficiency constraints: 37
[15 sec, 375374 itr]: obj = (37), mov = 375374, inf = 28.5%, acc = 26.7%, imp = 170

number of satisfied efficiency constraints: 37
[16 sec, 402730 itr]: obj = (37), mov = 402730, inf = 27.9%, acc = 26.6%, imp = 170

number of satisfied efficiency constraints: 37
[17 sec, 429952 itr]: obj = (37), mov = 429952, inf = 27.3%, acc = 26.5%, imp = 170

number of satisfied efficiency constraints: 37
[18 sec, 457523 itr]: obj = (37), mov = 457523, inf = 26.8%, acc = 26.3%, imp = 170

number of satisfied efficiency constraints: 37
[19 sec, 485104 itr]: obj = (37), mov = 485104, inf = 26.3%, acc = 26.2%, imp = 170

number of satisfied efficiency constraints: 37
[20 sec, 512679 itr]: obj = (37), mov = 512679, inf = 25.9%, acc = 26.1%, imp = 170

number of satisfied efficiency constraints: 37
[21 sec, 540288 itr]: obj = (37), mov = 540288, inf = 25.5%, acc = 26%, imp = 170

number of satisfied efficiency constraints: 37
[22 sec, 567556 itr]: obj = (37), mov = 567556, inf = 25.2%, acc = 25.9%, imp = 170

number of satisfied efficiency constraints: 37
[23 sec, 591525 itr]: obj = (37), mov = 591525, inf = 25%, acc = 25.9%, imp = 170

number of satisfied efficiency constraints: 37
[24 sec, 616557 itr]: obj = (37), mov = 616557, inf = 24.7%, acc = 25.8%, imp = 170

number of satisfied efficiency constraints: 37
[25 sec, 644264 itr]: obj = (37), mov = 644264, inf = 24.5%, acc = 25.8%, imp = 170

number of satisfied efficiency constraints: 37
[26 sec, 671488 itr]: obj = (37), mov = 671488, inf = 24.2%, acc = 25.7%, imp = 170

number of satisfied efficiency constraints: 37
[27 sec, 699087 itr]: obj = (37), mov = 699087, inf = 24%, acc = 25.7%, imp = 170

number of satisfied efficiency constraints: 37
[28 sec, 726265 itr]: obj = (37), mov = 726265, inf = 23.8%, acc = 25.7%, imp = 170

number of satisfied efficiency constraints: 37
[29 sec, 753459 itr]: obj = (37), mov = 753459, inf = 23.6%, acc = 25.6%, imp = 170

number of satisfied efficiency constraints: 37
[30 sec, 780857 itr]: obj = (37), mov = 780857, inf = 23.4%, acc = 25.6%, imp = 170

number of satisfied efficiency constraints: 37
[31 sec, 808009 itr]: obj = (37), mov = 808009, inf = 23.3%, acc = 25.6%, imp = 170

number of satisfied efficiency constraints: 37
[32 sec, 835286 itr]: obj = (37), mov = 835286, inf = 23.1%, acc = 25.5%, imp = 170

number of satisfied efficiency constraints: 37
[33 sec, 861433 itr]: obj = (37), mov = 861433, inf = 23%, acc = 25.5%, imp = 170

number of satisfied efficiency constraints: 37
[34 sec, 888254 itr]: obj = (37), mov = 888254, inf = 22.8%, acc = 25.5%, imp = 170

number of satisfied efficiency constraints: 37
[35 sec, 915855 itr]: obj = (37), mov = 915855, inf = 22.7%, acc = 25.5%, imp = 170

number of satisfied efficiency constraints: 37
[36 sec, 942912 itr]: obj = (37), mov = 942912, inf = 22.6%, acc = 25.4%, imp = 170

number of satisfied efficiency constraints: 37
[37 sec, 970626 itr]: obj = (37), mov = 970626, inf = 22.4%, acc = 25.4%, imp = 170

number of satisfied efficiency constraints: 37
[38 sec, 997565 itr]: obj = (38), mov = 997565, inf = 22.3%, acc = 25.3%, imp = 171

number of satisfied efficiency constraints: 38
[39 sec, 1026638 itr]: obj = (38), mov = 1026638, inf = 22.2%, acc = 25.2%, imp = 171

number of satisfied efficiency constraints: 38
[40 sec, 1055097 itr]: obj = (38), mov = 1055097, inf = 22.1%, acc = 25.2%, imp = 171

number of satisfied efficiency constraints: 38
[41 sec, 1083615 itr]: obj = (38), mov = 1083615, inf = 22%, acc = 25.1%, imp = 171

number of satisfied efficiency constraints: 38
[42 sec, 1112492 itr]: obj = (38), mov = 1112492, inf = 21.9%, acc = 25%, imp = 171

number of satisfied efficiency constraints: 38
[43 sec, 1141466 itr]: obj = (38), mov = 1141466, inf = 21.8%, acc = 24.9%, imp = 171

number of satisfied efficiency constraints: 38
[44 sec, 1169719 itr]: obj = (38), mov = 1169719, inf = 21.7%, acc = 24.9%, imp = 171

number of satisfied efficiency constraints: 38
[45 sec, 1197396 itr]: obj = (38), mov = 1197396, inf = 21.6%, acc = 24.8%, imp = 171

number of satisfied efficiency constraints: 38
[46 sec, 1225787 itr]: obj = (38), mov = 1225787, inf = 21.6%, acc = 24.7%, imp = 171

number of satisfied efficiency constraints: 38
[47 sec, 1254521 itr]: obj = (38), mov = 1254521, inf = 21.5%, acc = 24.7%, imp = 171

number of satisfied efficiency constraints: 38
[48 sec, 1282779 itr]: obj = (38), mov = 1282779, inf = 21.4%, acc = 24.6%, imp = 171

number of satisfied efficiency constraints: 38
[49 sec, 1310627 itr]: obj = (38), mov = 1310627, inf = 21.4%, acc = 24.6%, imp = 171

number of satisfied efficiency constraints: 38
[50 sec, 1339105 itr]: obj = (38), mov = 1339105, inf = 21.3%, acc = 24.5%, imp = 171

number of satisfied efficiency constraints: 38
[51 sec, 1368101 itr]: obj = (38), mov = 1368101, inf = 21.2%, acc = 24.5%, imp = 171

number of satisfied efficiency constraints: 38
[52 sec, 1396500 itr]: obj = (38), mov = 1396500, inf = 21.2%, acc = 24.5%, imp = 171

number of satisfied efficiency constraints: 38
[53 sec, 1425002 itr]: obj = (38), mov = 1425002, inf = 21.1%, acc = 24.4%, imp = 171

number of satisfied efficiency constraints: 38
[54 sec, 1453090 itr]: obj = (38), mov = 1453090, inf = 21.1%, acc = 24.4%, imp = 171

number of satisfied efficiency constraints: 38
[55 sec, 1481874 itr]: obj = (38), mov = 1481874, inf = 21%, acc = 24.3%, imp = 171

number of satisfied efficiency constraints: 38
[56 sec, 1510372 itr]: obj = (38), mov = 1510372, inf = 21%, acc = 24.3%, imp = 171

number of satisfied efficiency constraints: 38
[57 sec, 1540165 itr]: obj = (38), mov = 1540165, inf = 20.9%, acc = 24.2%, imp = 171

number of satisfied efficiency constraints: 38
[58 sec, 1568949 itr]: obj = (38), mov = 1568949, inf = 20.9%, acc = 24.2%, imp = 171

number of satisfied efficiency constraints: 38
[59 sec, 1597747 itr]: obj = (38), mov = 1597747, inf = 20.8%, acc = 24.2%, imp = 171

number of satisfied efficiency constraints: 38
[60 sec, 1626869 itr]: obj = (38), mov = 1626869, inf = 20.8%, acc = 24.2%, imp = 171

number of satisfied efficiency constraints: 38
[61 sec, 1656410 itr]: obj = (38), mov = 1656410, inf = 20.7%, acc = 24.1%, imp = 171

number of satisfied efficiency constraints: 38
[62 sec, 1685616 itr]: obj = (38), mov = 1685616, inf = 20.7%, acc = 24.1%, imp = 171

number of satisfied efficiency constraints: 38
[63 sec, 1714708 itr]: obj = (38), mov = 1714708, inf = 20.7%, acc = 24.1%, imp = 171

number of satisfied efficiency constraints: 38
[64 sec, 1744013 itr]: obj = (38), mov = 1744013, inf = 20.6%, acc = 24%, imp = 171

number of satisfied efficiency constraints: 38
[65 sec, 1773674 itr]: obj = (38), mov = 1773674, inf = 20.6%, acc = 24%, imp = 171

number of satisfied efficiency constraints: 38
[66 sec, 1802888 itr]: obj = (38), mov = 1802888, inf = 20.5%, acc = 24%, imp = 171

number of satisfied efficiency constraints: 38
[67 sec, 1832541 itr]: obj = (38), mov = 1832541, inf = 20.5%, acc = 24%, imp = 171

number of satisfied efficiency constraints: 38
[68 sec, 1862038 itr]: obj = (38), mov = 1862038, inf = 20.5%, acc = 23.9%, imp = 171

number of satisfied efficiency constraints: 38
[69 sec, 1891851 itr]: obj = (38), mov = 1891851, inf = 20.4%, acc = 23.9%, imp = 171

number of satisfied efficiency constraints: 38
[70 sec, 1921988 itr]: obj = (38), mov = 1921988, inf = 20.4%, acc = 23.9%, imp = 171

number of satisfied efficiency constraints: 38
[71 sec, 1951542 itr]: obj = (38), mov = 1951542, inf = 20.4%, acc = 23.8%, imp = 171

number of satisfied efficiency constraints: 38
[72 sec, 1980596 itr]: obj = (38), mov = 1980596, inf = 20.3%, acc = 23.8%, imp = 171

number of satisfied efficiency constraints: 38
[73 sec, 2009573 itr]: obj = (38), mov = 2009573, inf = 20.3%, acc = 23.8%, imp = 171

number of satisfied efficiency constraints: 38
[74 sec, 2038596 itr]: obj = (38), mov = 2038596, inf = 20.3%, acc = 23.8%, imp = 171

number of satisfied efficiency constraints: 38
[75 sec, 2068176 itr]: obj = (38), mov = 2068176, inf = 20.2%, acc = 23.7%, imp = 171

number of satisfied efficiency constraints: 38
[76 sec, 2097474 itr]: obj = (38), mov = 2097474, inf = 20.2%, acc = 23.7%, imp = 171

number of satisfied efficiency constraints: 38
[77 sec, 2125532 itr]: obj = (38), mov = 2125532, inf = 20.2%, acc = 23.7%, imp = 171

number of satisfied efficiency constraints: 38
[78 sec, 2155729 itr]: obj = (38), mov = 2155729, inf = 20.2%, acc = 23.6%, imp = 171

number of satisfied efficiency constraints: 38
[79 sec, 2185332 itr]: obj = (38), mov = 2185332, inf = 20.1%, acc = 23.6%, imp = 171

number of satisfied efficiency constraints: 38
[80 sec, 2214897 itr]: obj = (38), mov = 2214897, inf = 20.1%, acc = 23.6%, imp = 171

number of satisfied efficiency constraints: 38
[81 sec, 2243979 itr]: obj = (38), mov = 2243979, inf = 20.1%, acc = 23.6%, imp = 171

number of satisfied efficiency constraints: 38
[82 sec, 2272406 itr]: obj = (38), mov = 2272406, inf = 20.1%, acc = 23.6%, imp = 171

number of satisfied efficiency constraints: 38
[83 sec, 2301553 itr]: obj = (38), mov = 2301553, inf = 20%, acc = 23.5%, imp = 171

number of satisfied efficiency constraints: 38
[84 sec, 2330754 itr]: obj = (38), mov = 2330754, inf = 20%, acc = 23.5%, imp = 171

number of satisfied efficiency constraints: 38
[85 sec, 2359059 itr]: obj = (38), mov = 2359059, inf = 20%, acc = 23.5%, imp = 171

number of satisfied efficiency constraints: 38
[86 sec, 2388552 itr]: obj = (38), mov = 2388552, inf = 20%, acc = 23.5%, imp = 171

number of satisfied efficiency constraints: 38
[87 sec, 2415524 itr]: obj = (38), mov = 2415524, inf = 20%, acc = 23.5%, imp = 171

number of satisfied efficiency constraints: 38
[88 sec, 2444192 itr]: obj = (38), mov = 2444192, inf = 19.9%, acc = 23.5%, imp = 171

number of satisfied efficiency constraints: 38
[89 sec, 2473112 itr]: obj = (38), mov = 2473112, inf = 19.9%, acc = 23.4%, imp = 171

number of satisfied efficiency constraints: 38
[90 sec, 2502146 itr]: obj = (38), mov = 2502146, inf = 19.9%, acc = 23.4%, imp = 171

number of satisfied efficiency constraints: 38
[90 sec, 2502146 itr]: obj = (38), mov = 2502146, inf = 19.9%, acc = 23.4%, imp = 171

number of satisfied efficiency constraints: 38
2502146 iterations, 2502146 moves performed in 90 seconds
Feasible solution: obj = (38)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 38
Mapping: 0->4, 1->29, 2->29, 3->42, 4->16, 5->25, 6->19, 7->22, 8->33, 9->43, 10->22, 11->17, 12->15, 13->21, 14->7, 15->2, 16->36, 17->31, 18->9, 19->24, 20->3, 21->37, 22->45, 23->33, 24->44, 25->40, 26->35, 27->23, 28->5, 29->47, 30->4, 31->2, 32->28, 33->38, 34->43, 35->17, 36->0, 37->16, 38->7, 39->8, 40->7, 41->36, 42->11, 43->6, 44->13, 45->42, 46->38, 47->18, 48->6, 49->43, 50->1, 51->16, 52->0, 53->24, 54->23, 55->26, 56->19, 57->38, 58->2, 59->32, 60->10, 61->34, 62->4, 63->36, 64->9, 65->27, 66->47, 67->31, 68->20, 69->25, 70->7, 71->38, 72->12, 73->1, 74->26, 75->40, 76->12, 77->25, 78->31, 79->20, 80->10, 81->44, 82->12, 83->12, 84->3, 85->0, 86->5, 87->30, 88->25, 89->22, 90->18, 91->46, 92->42, 93->33, 94->0, 95->32, 96->28, 97->5, 98->47, 99->25, 100->34, 101->0, 102->26, 103->26, 104->15, 105->10, 106->10, 107->9, 108->1, 109->18, 110->13, 111->31, 112->40, 113->29, 114->42, 115->16, 116->19, 117->37, 118->14, 119->28, 120->13, 121->39, 122->38, 123->23, 124->45, 125->35, 126->15, 127->6, 128->21, 129->22, 130->19, 131->24, 132->4, 133->23, 134->24, 135->44, 136->12, 137->28, 138->1, 139->20, 140->2, 141->35, 142->14, 143->32, 144->41, 145->46, 146->14, 147->39, 148->14, 149->45, 150->7, 151->39, 152->8, 153->22, 154->2, 155->36, 156->9, 157->11, 158->28, 159->17, 160->1, 161->47, 162->21, 163->25, 164->14, 165->1, 166->43, 167->29, 168->36, 169->37, 170->47, 171->35, 172->27, 173->43, 174->40, 175->35, 176->17, 177->8, 178->18, 179->23, 180->6, 181->37, 182->41, 183->44, 184->39, 185->45, 186->9, 187->13, 188->35, 189->30, 190->44, 191->45, 192->31, 193->36, 194->34, 195->34, 196->16, 197->11, 198->10, 199->41, 200->10, 201->13, 202->42, 203->5, 204->40, 205->39, 206->33, 207->20, 208->41, 209->20, 210->3, 211->38, 212->45, 213->43, 214->18, 215->27, 216->2, 217->18, 218->21, 219->15, 220->11, 221->46, 222->8, 223->32, 224->32, 225->41, 226->30, 227->29, 228->34, 229->26, 230->39, 231->0, 232->20, 233->21, 234->28, 235->30, 236->23, 237->30, 238->46, 239->9, 240->27, 241->34, 242->3, 243->41, 244->42, 245->31, 246->13, 247->32, 248->47, 249->24, 250->44, 251->22, 252->26, 253->11, 254->11, 255->6, 256->4, 257->4, 258->12, 259->6, 260->3, 261->5, 262->37, 263->15, 264->5, 265->29, 266->16, 267->46, 268->3, 269->33, 270->19, 271->27, 272->46, 273->27, 274->24, 275->37, 276->33, 277->14, 278->15, 279->8, 280->17, 281->40, 282->7, 283->17, 284->8, 285->30, 286->19, 287->21
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.79418	9.15736	1.85129	2.07031	1.85232	2.10431
	2.26919	2.58007	2.86536	10.3777	2.36041	9.03794
	2.12706	11.6928	12.3956	1.24324	10.3144	2.64893
	3.01482	1.72995	1.16063	1.58435	1.98043	6.10966
	2.06641	0.964229	1.49431	1.36389	11.7402	2.41443
	3.536	10.3613	2.29571	1.49012	10.9227	3.33551
	4.65524	10.6478	7.51716	13.8974	7.47178	10.2147
	4.34692	2.60702	10.7949	16.8699	5.20218	5.46805
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	1	1	0	1	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
	1	1	1	1	1	1
	1	1	1	1	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 6 --global-tlim 90 --seed 101

Starting the Optimization Process
