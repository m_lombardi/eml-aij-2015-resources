LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
0.5, 0.793797, 3.01448, 2.89409, 2.97166, 1.82598, 1.46422, 1.02987, 4.33222, 3.15438, 0.5, 1.51931, 2.09408, 1.95425, 1.74965, 2.13165, 2.02029, 2.05007, 2.40715, 0.751014, 2.93856, 1.5686, 1.81978, 2.51489, 0.810358, 3.37159, 2.01811, 0.5, 3.8527, 1.44724, 1.43318, 2.33024, 1.63724, 1.9352, 2.17271, 2.49144, 2.62915, 2.43595, 1.70695, 1.6611, 2.1134, 1.45345, 2.13604, 2.94479, 1.10006, 2.21292, 2.90612, 0.700066, 2.24627, 2.07499, 0.712951, 3.80092, 1.44741, 1.71746, 1.98396, 2.25489, 2.0251, 1.72142, 1.96386, 2.05077, 0.724777, 5.76477, 1.8992, 2.38764, 0.513167, 0.710449, 31.2107, 27.2561, 18.7677, 35, 11.0826, 8.68286, 1.51957, 3.17254, 0.5, 1.02075, 1.81243, 3.9747, 1.72133, 0.667203, 2.55119, 2.14292, 2.30777, 2.60958, 0.631906, 2.19898, 4.19696, 1.52562, 0.851563, 2.59497, 3.36286, 3.496, 0.975857, 1.57842, 2.08687, 0.5, 28.3048, 15.5874, 35, 25.0792, 12.9663, 15.0623, 1.94871, 2.30428, 2.28502, 1.8023, 1.39475, 2.26493, 3.11261, 1.48502, 2.7242, 1.50319, 2.67498, 0.5, 0.94498, 1.14016, 2.84732, 3.11272, 2.697, 1.25782, 1.49547, 2.60328, 2.37214, 1.91276, 1.0462, 2.57016, 0.556693, 2.96248, 3.93222, 0.5, 1.54412, 2.50448, 20.8662, 35, 16.4223, 16.63, 24.1447, 18.937, 35, 18.8404, 13.9087, 21.4826, 32.0696, 10.6987, 2.03447, 3.84498, 3.47505, 0.840083, 1.07748, 0.727933, 3.5496, 3.73055, 1.03076, 0.5, 2.33398, 0.855113, 1.93585, 0.915647, 0.553338, 1.61452, 1.35227, 5.62837, 3.13578, 3.41479, 0.603717, 1.40872, 0.5, 2.93699, 1.64791, 2.04324, 0.971774, 0.682902, 1.77925, 4.87493, 0.679022, 1.01475, 0.78205, 2.11252, 0.933492, 6.47816, 1.34254, 1.35093, 0.5, 3.04181, 3.12938, 2.63533, 0.5, 1.80789, 4.25635, 2.83402, 0.859458, 1.74228, 2.29634, 1.85749, 1.87562, 2.42096, 1.42836, 2.12123, 3.7915, 2.21285, 0.575129, 0.780089, 4.14043, 0.5, 0.5, 0.855756, 1.6246, 2.73087, 3.7941, 2.49466, 3.98145, 0.5, 1.82333, 1.23156, 2.84167, 1.62198, 4.5138, 2.24199, 1.19583, 2.07482, 0.739494, 1.23407, 1.85496, 0.557577, 2.51568, 1.41475, 4.12757, 1.52947, 1.92157, 2.25752, 1.90951, 2.10541, 1.70878, 2.09721, 0.957288, 0.997952, 2.95043, 3.22083, 3.20122, 0.672285, 3.6616, 3.14231, 2.4321, 1.26004, 0.5, 1.00395, 0.996237, 3.61131, 0.889852, 2.02482, 2.91463, 1.56314, 0.5, 2.30284, 5.09568, 1.76483, 0.697285, 1.63936, 2.35738, 1.92499, 1.23851, 2.49156, 2.46238, 1.52517, 2.20721, 2.20715, 1.75975, 2.13094, 1.83949, 1.85546, 2.53771, 1.97268, 2.60817, 0.5, 2.55362, 1.82783, 0.623567, 3.18571, 0.742433, 2.03774, 0.739147, 4.6714, 1.30779, 2.98554, 3.00575, 2.50208, 1.69884, 0.5
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54812, operands = 135836
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 401, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 15110 itr]: obj = (26), mov = 15110, inf = 70.7%, acc = 18.8%, imp = 160

number of satisfied efficiency constraints: 26
[2 sec, 46303 itr]: obj = (27), mov = 46303, inf = 72.9%, acc = 14.3%, imp = 161

number of satisfied efficiency constraints: 27
[3 sec, 66608 itr]: obj = (28), mov = 66608, inf = 67.4%, acc = 15.6%, imp = 162

number of satisfied efficiency constraints: 28
[4 sec, 83660 itr]: obj = (28), mov = 83660, inf = 59.1%, acc = 18.4%, imp = 162

number of satisfied efficiency constraints: 28
[5 sec, 101381 itr]: obj = (28), mov = 101381, inf = 51.4%, acc = 21.2%, imp = 162

number of satisfied efficiency constraints: 28
[6 sec, 119368 itr]: obj = (28), mov = 119368, inf = 46.1%, acc = 22.9%, imp = 162

number of satisfied efficiency constraints: 28
[7 sec, 136482 itr]: obj = (29), mov = 136482, inf = 42.2%, acc = 24.6%, imp = 200

number of satisfied efficiency constraints: 29
[8 sec, 151549 itr]: obj = (29), mov = 151549, inf = 39.5%, acc = 26.4%, imp = 202

number of satisfied efficiency constraints: 29
[9 sec, 167535 itr]: obj = (29), mov = 167535, inf = 37.2%, acc = 27.6%, imp = 202

number of satisfied efficiency constraints: 29
[10 sec, 183312 itr]: obj = (29), mov = 183312, inf = 35.4%, acc = 28.6%, imp = 203

number of satisfied efficiency constraints: 29
[11 sec, 200341 itr]: obj = (29), mov = 200341, inf = 33.7%, acc = 29.2%, imp = 204

number of satisfied efficiency constraints: 29
[12 sec, 218187 itr]: obj = (29), mov = 218187, inf = 32.1%, acc = 29.4%, imp = 204

number of satisfied efficiency constraints: 29
[13 sec, 236348 itr]: obj = (29), mov = 236348, inf = 30.9%, acc = 29.6%, imp = 204

number of satisfied efficiency constraints: 29
[14 sec, 254642 itr]: obj = (29), mov = 254642, inf = 29.7%, acc = 29.7%, imp = 204

number of satisfied efficiency constraints: 29
[15 sec, 272218 itr]: obj = (29), mov = 272218, inf = 28.8%, acc = 29.9%, imp = 204

number of satisfied efficiency constraints: 29
[16 sec, 289600 itr]: obj = (29), mov = 289600, inf = 28%, acc = 30.1%, imp = 204

number of satisfied efficiency constraints: 29
[17 sec, 308291 itr]: obj = (29), mov = 308291, inf = 27.2%, acc = 30.1%, imp = 205

number of satisfied efficiency constraints: 29
[18 sec, 327721 itr]: obj = (30), mov = 327721, inf = 26.5%, acc = 30%, imp = 206

number of satisfied efficiency constraints: 30
[19 sec, 349485 itr]: obj = (30), mov = 349485, inf = 25.9%, acc = 29.5%, imp = 206

number of satisfied efficiency constraints: 30
[20 sec, 370743 itr]: obj = (30), mov = 370743, inf = 25.3%, acc = 29.2%, imp = 206

number of satisfied efficiency constraints: 30
[21 sec, 391693 itr]: obj = (30), mov = 391693, inf = 24.8%, acc = 28.9%, imp = 206

number of satisfied efficiency constraints: 30
[22 sec, 412788 itr]: obj = (30), mov = 412788, inf = 24.4%, acc = 28.6%, imp = 206

number of satisfied efficiency constraints: 30
[23 sec, 433958 itr]: obj = (30), mov = 433958, inf = 24%, acc = 28.3%, imp = 206

number of satisfied efficiency constraints: 30
[24 sec, 454729 itr]: obj = (30), mov = 454729, inf = 23.6%, acc = 28.2%, imp = 206

number of satisfied efficiency constraints: 30
[25 sec, 475595 itr]: obj = (30), mov = 475595, inf = 23.2%, acc = 28%, imp = 206

number of satisfied efficiency constraints: 30
[26 sec, 496661 itr]: obj = (30), mov = 496661, inf = 22.9%, acc = 27.8%, imp = 206

number of satisfied efficiency constraints: 30
[27 sec, 516737 itr]: obj = (30), mov = 516737, inf = 22.6%, acc = 27.7%, imp = 206

number of satisfied efficiency constraints: 30
[28 sec, 537015 itr]: obj = (30), mov = 537015, inf = 22.4%, acc = 27.6%, imp = 206

number of satisfied efficiency constraints: 30
[29 sec, 557599 itr]: obj = (30), mov = 557599, inf = 22.1%, acc = 27.5%, imp = 206

number of satisfied efficiency constraints: 30
[30 sec, 578978 itr]: obj = (30), mov = 578978, inf = 21.9%, acc = 27.3%, imp = 206

number of satisfied efficiency constraints: 30
[31 sec, 600759 itr]: obj = (30), mov = 600759, inf = 21.7%, acc = 27.2%, imp = 206

number of satisfied efficiency constraints: 30
[32 sec, 621334 itr]: obj = (30), mov = 621334, inf = 21.5%, acc = 27.1%, imp = 206

number of satisfied efficiency constraints: 30
[33 sec, 643148 itr]: obj = (30), mov = 643148, inf = 21.3%, acc = 26.9%, imp = 206

number of satisfied efficiency constraints: 30
[34 sec, 664319 itr]: obj = (30), mov = 664319, inf = 21.1%, acc = 26.9%, imp = 206

number of satisfied efficiency constraints: 30
[35 sec, 685578 itr]: obj = (30), mov = 685578, inf = 20.9%, acc = 26.8%, imp = 206

number of satisfied efficiency constraints: 30
[36 sec, 705592 itr]: obj = (30), mov = 705592, inf = 20.7%, acc = 26.8%, imp = 206

number of satisfied efficiency constraints: 30
[37 sec, 726172 itr]: obj = (30), mov = 726172, inf = 20.6%, acc = 26.7%, imp = 206

number of satisfied efficiency constraints: 30
[38 sec, 746695 itr]: obj = (30), mov = 746695, inf = 20.5%, acc = 26.7%, imp = 206

number of satisfied efficiency constraints: 30
[39 sec, 768670 itr]: obj = (30), mov = 768670, inf = 20.3%, acc = 26.6%, imp = 206

number of satisfied efficiency constraints: 30
[40 sec, 786769 itr]: obj = (30), mov = 786769, inf = 20.2%, acc = 26.7%, imp = 238

number of satisfied efficiency constraints: 30
[41 sec, 803140 itr]: obj = (30), mov = 803140, inf = 20.2%, acc = 26.9%, imp = 239

number of satisfied efficiency constraints: 30
[42 sec, 820510 itr]: obj = (30), mov = 820510, inf = 20.1%, acc = 27%, imp = 239

number of satisfied efficiency constraints: 30
[43 sec, 839038 itr]: obj = (30), mov = 839038, inf = 19.9%, acc = 27.1%, imp = 240

number of satisfied efficiency constraints: 30
[44 sec, 859131 itr]: obj = (30), mov = 859131, inf = 19.8%, acc = 27.1%, imp = 241

number of satisfied efficiency constraints: 30
[45 sec, 879734 itr]: obj = (31), mov = 879734, inf = 19.7%, acc = 27%, imp = 242

number of satisfied efficiency constraints: 31
[46 sec, 902435 itr]: obj = (31), mov = 902435, inf = 19.6%, acc = 26.9%, imp = 242

number of satisfied efficiency constraints: 31
[47 sec, 924679 itr]: obj = (31), mov = 924679, inf = 19.6%, acc = 26.7%, imp = 242

number of satisfied efficiency constraints: 31
[48 sec, 947325 itr]: obj = (31), mov = 947325, inf = 19.5%, acc = 26.6%, imp = 242

number of satisfied efficiency constraints: 31
[49 sec, 969677 itr]: obj = (31), mov = 969677, inf = 19.4%, acc = 26.5%, imp = 242

number of satisfied efficiency constraints: 31
[50 sec, 991829 itr]: obj = (31), mov = 991829, inf = 19.4%, acc = 26.4%, imp = 242

number of satisfied efficiency constraints: 31
[51 sec, 1013971 itr]: obj = (31), mov = 1013971, inf = 19.3%, acc = 26.3%, imp = 242

number of satisfied efficiency constraints: 31
[52 sec, 1036097 itr]: obj = (31), mov = 1036097, inf = 19.2%, acc = 26.2%, imp = 242

number of satisfied efficiency constraints: 31
[53 sec, 1059409 itr]: obj = (31), mov = 1059409, inf = 19.1%, acc = 26%, imp = 242

number of satisfied efficiency constraints: 31
[54 sec, 1081827 itr]: obj = (31), mov = 1081827, inf = 19%, acc = 25.9%, imp = 242

number of satisfied efficiency constraints: 31
[55 sec, 1098542 itr]: obj = (31), mov = 1098542, inf = 19%, acc = 26%, imp = 281

number of satisfied efficiency constraints: 31
[56 sec, 1114116 itr]: obj = (31), mov = 1114116, inf = 18.9%, acc = 26.2%, imp = 282

number of satisfied efficiency constraints: 31
[57 sec, 1131394 itr]: obj = (31), mov = 1131394, inf = 18.9%, acc = 26.3%, imp = 283

number of satisfied efficiency constraints: 31
[58 sec, 1151664 itr]: obj = (31), mov = 1151664, inf = 18.9%, acc = 26.3%, imp = 285

number of satisfied efficiency constraints: 31
[59 sec, 1172442 itr]: obj = (31), mov = 1172442, inf = 18.8%, acc = 26.3%, imp = 285

number of satisfied efficiency constraints: 31
[60 sec, 1193267 itr]: obj = (31), mov = 1193267, inf = 18.8%, acc = 26.3%, imp = 285

number of satisfied efficiency constraints: 31
[61 sec, 1213848 itr]: obj = (31), mov = 1213848, inf = 18.7%, acc = 26.2%, imp = 285

number of satisfied efficiency constraints: 31
[62 sec, 1234687 itr]: obj = (31), mov = 1234687, inf = 18.7%, acc = 26.2%, imp = 285

number of satisfied efficiency constraints: 31
[63 sec, 1251647 itr]: obj = (31), mov = 1251647, inf = 18.7%, acc = 26.3%, imp = 311

number of satisfied efficiency constraints: 31
[64 sec, 1266369 itr]: obj = (31), mov = 1266369, inf = 18.6%, acc = 26.5%, imp = 313

number of satisfied efficiency constraints: 31
[65 sec, 1281921 itr]: obj = (31), mov = 1281921, inf = 18.6%, acc = 26.7%, imp = 313

number of satisfied efficiency constraints: 31
[66 sec, 1298368 itr]: obj = (31), mov = 1298368, inf = 18.6%, acc = 26.8%, imp = 315

number of satisfied efficiency constraints: 31
[67 sec, 1316253 itr]: obj = (31), mov = 1316253, inf = 18.5%, acc = 26.9%, imp = 315

number of satisfied efficiency constraints: 31
[68 sec, 1334053 itr]: obj = (31), mov = 1334053, inf = 18.5%, acc = 26.9%, imp = 315

number of satisfied efficiency constraints: 31
[69 sec, 1353234 itr]: obj = (31), mov = 1353234, inf = 18.5%, acc = 26.9%, imp = 316

number of satisfied efficiency constraints: 31
[70 sec, 1372562 itr]: obj = (31), mov = 1372562, inf = 18.4%, acc = 26.9%, imp = 317

number of satisfied efficiency constraints: 31
[71 sec, 1393101 itr]: obj = (31), mov = 1393101, inf = 18.4%, acc = 26.9%, imp = 317

number of satisfied efficiency constraints: 31
[72 sec, 1412620 itr]: obj = (31), mov = 1412620, inf = 18.4%, acc = 26.9%, imp = 317

number of satisfied efficiency constraints: 31
[73 sec, 1433022 itr]: obj = (31), mov = 1433022, inf = 18.3%, acc = 26.9%, imp = 317

number of satisfied efficiency constraints: 31
[74 sec, 1453271 itr]: obj = (31), mov = 1453271, inf = 18.3%, acc = 26.8%, imp = 317

number of satisfied efficiency constraints: 31
[75 sec, 1473940 itr]: obj = (31), mov = 1473940, inf = 18.3%, acc = 26.8%, imp = 317

number of satisfied efficiency constraints: 31
[76 sec, 1495085 itr]: obj = (31), mov = 1495085, inf = 18.2%, acc = 26.7%, imp = 317

number of satisfied efficiency constraints: 31
[77 sec, 1516376 itr]: obj = (31), mov = 1516376, inf = 18.2%, acc = 26.7%, imp = 317

number of satisfied efficiency constraints: 31
[78 sec, 1536447 itr]: obj = (31), mov = 1536447, inf = 18.2%, acc = 26.7%, imp = 317

number of satisfied efficiency constraints: 31
[79 sec, 1558247 itr]: obj = (31), mov = 1558247, inf = 18.2%, acc = 26.6%, imp = 317

number of satisfied efficiency constraints: 31
[80 sec, 1579301 itr]: obj = (31), mov = 1579301, inf = 18.1%, acc = 26.6%, imp = 317

number of satisfied efficiency constraints: 31
[81 sec, 1600071 itr]: obj = (31), mov = 1600071, inf = 18.1%, acc = 26.5%, imp = 317

number of satisfied efficiency constraints: 31
[82 sec, 1620191 itr]: obj = (31), mov = 1620191, inf = 18.1%, acc = 26.5%, imp = 317

number of satisfied efficiency constraints: 31
[83 sec, 1641459 itr]: obj = (31), mov = 1641459, inf = 18.1%, acc = 26.5%, imp = 317

number of satisfied efficiency constraints: 31
[84 sec, 1662207 itr]: obj = (31), mov = 1662207, inf = 18%, acc = 26.4%, imp = 355

number of satisfied efficiency constraints: 31
[85 sec, 1672124 itr]: obj = (31), mov = 1672124, inf = 18%, acc = 26.6%, imp = 357

number of satisfied efficiency constraints: 31
[86 sec, 1684533 itr]: obj = (31), mov = 1684533, inf = 18%, acc = 26.7%, imp = 359

number of satisfied efficiency constraints: 31
[87 sec, 1699670 itr]: obj = (31), mov = 1699670, inf = 18%, acc = 26.8%, imp = 360

number of satisfied efficiency constraints: 31
[88 sec, 1716095 itr]: obj = (31), mov = 1716095, inf = 18%, acc = 26.9%, imp = 361

number of satisfied efficiency constraints: 31
[89 sec, 1734096 itr]: obj = (31), mov = 1734096, inf = 17.9%, acc = 26.9%, imp = 361

number of satisfied efficiency constraints: 31
[90 sec, 1751628 itr]: obj = (31), mov = 1751628, inf = 17.9%, acc = 27%, imp = 361

number of satisfied efficiency constraints: 31
[90 sec, 1751628 itr]: obj = (31), mov = 1751628, inf = 17.9%, acc = 27%, imp = 361

number of satisfied efficiency constraints: 31
1751628 iterations, 1751628 moves performed in 90 seconds
Feasible solution: obj = (31)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 31
Mapping: 0->30, 1->1, 2->26, 3->9, 4->18, 5->15, 6->3, 7->5, 8->31, 9->18, 10->24, 11->14, 12->22, 13->10, 14->11, 15->7, 16->13, 17->36, 18->12, 19->26, 20->47, 21->13, 22->22, 23->28, 24->2, 25->40, 26->17, 27->17, 28->47, 29->5, 30->13, 31->34, 32->35, 33->47, 34->20, 35->39, 36->34, 37->39, 38->3, 39->29, 40->7, 41->29, 42->19, 43->16, 44->4, 45->43, 46->9, 47->4, 48->27, 49->39, 50->44, 51->37, 52->28, 53->0, 54->13, 55->37, 56->0, 57->30, 58->12, 59->32, 60->18, 61->6, 62->5, 63->31, 64->5, 65->25, 66->16, 67->16, 68->33, 69->9, 70->2, 71->34, 72->27, 73->42, 74->32, 75->27, 76->1, 77->43, 78->6, 79->15, 80->8, 81->15, 82->44, 83->38, 84->0, 85->31, 86->29, 87->6, 88->35, 89->17, 90->9, 91->3, 92->14, 93->38, 94->4, 95->36, 96->30, 97->45, 98->45, 99->34, 100->37, 101->42, 102->25, 103->46, 104->17, 105->38, 106->15, 107->22, 108->25, 109->27, 110->43, 111->32, 112->7, 113->21, 114->22, 115->19, 116->40, 117->5, 118->6, 119->14, 120->4, 121->7, 122->43, 123->27, 124->19, 125->37, 126->36, 127->0, 128->17, 129->21, 130->5, 131->20, 132->24, 133->37, 134->9, 135->35, 136->45, 137->23, 138->36, 139->34, 140->1, 141->12, 142->25, 143->2, 144->41, 145->3, 146->40, 147->33, 148->35, 149->2, 150->28, 151->10, 152->26, 153->1, 154->8, 155->23, 156->11, 157->39, 158->6, 159->39, 160->39, 161->2, 162->10, 163->47, 164->23, 165->35, 166->36, 167->42, 168->0, 169->4, 170->42, 171->33, 172->23, 173->21, 174->21, 175->14, 176->38, 177->41, 178->23, 179->46, 180->22, 181->24, 182->47, 183->35, 184->38, 185->40, 186->38, 187->25, 188->29, 189->46, 190->42, 191->44, 192->26, 193->1, 194->7, 195->30, 196->36, 197->15, 198->8, 199->43, 200->2, 201->13, 202->18, 203->12, 204->20, 205->24, 206->41, 207->46, 208->47, 209->12, 210->37, 211->31, 212->18, 213->25, 214->8, 215->21, 216->44, 217->45, 218->18, 219->11, 220->44, 221->0, 222->19, 223->20, 224->11, 225->28, 226->40, 227->29, 228->10, 229->19, 230->14, 231->41, 232->33, 233->14, 234->12, 235->6, 236->10, 237->46, 238->23, 239->24, 240->26, 241->43, 242->31, 243->11, 244->32, 245->20, 246->4, 247->40, 248->20, 249->9, 250->29, 251->15, 252->32, 253->41, 254->45, 255->7, 256->44, 257->19, 258->16, 259->16, 260->24, 261->10, 262->45, 263->28, 264->30, 265->8, 266->33, 267->8, 268->30, 269->27, 270->16, 271->26, 272->21, 273->33, 274->22, 275->11, 276->3, 277->32, 278->13, 279->42, 280->28, 281->41, 282->17, 283->3, 284->46, 285->34, 286->1, 287->31
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.70315	3.42854	4.92052	2.35354	1.40366	1.59105
	2.21	2.19396	2.64274	10.435	2.69736	1.89398
	4.9676	1.42143	1.46241	1.61921	11.3719	2.10635
	2.33507	1.67904	1.27143	1.79735	1.83666	4.38496
	4.24727	6.81346	2.12115	1.67331	1.86516	2.66866
	6.16565	2.05849	1.37328	4.04321	10.0107	4.10779
	6.67252	10.0956	1.73362	1.81414	3.3447	2.47521
	4.17346	2.77319	1.7856	14.0887	3.42898	2.73923
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
	1	1	0	1	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 15 --global-tlim 90 --seed 401

Starting the Optimization Process
