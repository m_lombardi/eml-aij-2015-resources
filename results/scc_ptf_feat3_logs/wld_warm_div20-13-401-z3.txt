==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 13 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 401

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.006760
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007094
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007477
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.701/6.723
- lns time: 0.701622
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005578
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 1.065198
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005530
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.007098
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.823/14.630
- lns time: 0.823558
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 2.005446
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.519/18.154
- lns time: 1.518805
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 0.827276
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.430/20.412
- lns time: 1.430299
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.008743
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.006400
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005911
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005928
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.006360
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 1.930790
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005814
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.007725
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005627
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 1.477460
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 0.749761
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005863
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.526/44.154
- lns time: 1.526292
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006330
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.013052
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.009813
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 1.285649
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.005916
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.011351
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006673
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.005968
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007415
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006795
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006388
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006860
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004643
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.014330
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006917
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007704
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007906
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.855/78.424
- lns time: 0.855612
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.007163
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 0.941419
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.005568
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.006104
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.007112
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 0.714702
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 1.901629
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 17

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 11
final number of satisfied efficiency constraints: 17
number of iterations: 51
total time: 90.007433

Final Solution: 0->17, 1->46, 2->31, 3->27, 4->41, 5->6, 6->47, 7->18, 8->46, 9->26, 10->2, 11->39, 12->26, 13->28, 14->24, 15->39, 16->33, 17->24, 18->22, 19->18, 20->29, 21->15, 22->4, 23->22, 24->5, 25->33, 26->19, 27->1, 28->19, 29->20, 30->0, 31->47, 32->18, 33->37, 34->31, 35->10, 36->9, 37->16, 38->18, 39->16, 40->24, 41->3, 42->5, 43->30, 44->17, 45->2, 46->27, 47->7, 48->3, 49->37, 50->17, 51->12, 52->33, 53->24, 54->22, 55->7, 56->40, 57->8, 58->9, 59->23, 60->10, 61->6, 62->5, 63->9, 64->14, 65->45, 66->38, 67->23, 68->2, 69->35, 70->5, 71->38, 72->10, 73->4, 74->11, 75->42, 76->33, 77->16, 78->40, 79->19, 80->26, 81->6, 82->39, 83->25, 84->10, 85->32, 86->26, 87->31, 88->39, 89->2, 90->7, 91->14, 92->31, 93->6, 94->0, 95->30, 96->28, 97->46, 98->45, 99->12, 100->15, 101->37, 102->21, 103->40, 104->8, 105->34, 106->34, 107->17, 108->20, 109->8, 110->29, 111->27, 112->11, 113->5, 114->33, 115->11, 116->14, 117->25, 118->19, 119->29, 120->14, 121->39, 122->9, 123->13, 124->45, 125->34, 126->30, 127->28, 128->43, 129->44, 130->2, 131->31, 132->36, 133->35, 134->30, 135->3, 136->0, 137->42, 138->21, 139->44, 140->47, 141->24, 142->40, 143->32, 144->18, 145->44, 146->27, 147->24, 148->45, 149->32, 150->1, 151->33, 152->23, 153->35, 154->45, 155->25, 156->11, 157->36, 158->34, 159->12, 160->32, 161->41, 162->41, 163->13, 164->42, 165->47, 166->45, 167->36, 168->14, 169->4, 170->25, 171->9, 172->13, 173->42, 174->41, 175->20, 176->15, 177->16, 178->29, 179->28, 180->23, 181->15, 182->34, 183->3, 184->30, 185->6, 186->30, 187->38, 188->35, 189->47, 190->15, 191->31, 192->3, 193->32, 194->11, 195->37, 196->38, 197->43, 198->28, 199->20, 200->12, 201->47, 202->22, 203->17, 204->7, 205->13, 206->1, 207->21, 208->8, 209->11, 210->8, 211->12, 212->21, 213->46, 214->22, 215->0, 216->32, 217->23, 218->35, 219->26, 220->13, 221->15, 222->36, 223->38, 224->41, 225->5, 226->44, 227->40, 228->17, 229->6, 230->43, 231->18, 232->1, 233->4, 234->19, 235->14, 236->12, 237->10, 238->3, 239->16, 240->38, 241->7, 242->20, 243->25, 244->43, 245->0, 246->16, 247->21, 248->42, 249->1, 250->21, 251->43, 252->27, 253->1, 254->4, 255->34, 256->35, 257->22, 258->20, 259->10, 260->37, 261->28, 262->25, 263->19, 264->7, 265->13, 266->41, 267->40, 268->8, 269->46, 270->46, 271->4, 272->44, 273->42, 274->26, 275->2, 276->44, 277->29, 278->27, 279->37, 280->43, 281->23, 282->29, 283->36, 284->39, 285->9, 286->0, 287->36

Average CPI map:
	2.54,	1.88,	1.88,	3.14,	2.11,	1.08
	2.28,	2.26,	2.22,	2.20,	2.76,	2.19
	2.10,	2.09,	2.09,	2.07,	2.84,	2.45
	1.06,	2.06,	2.05,	2.05,	2.05,	2.01
	2.04,	2.00,	1.99,	1.99,	1.56,	2.49
	3.13,	1.98,	1.98,	1.98,	1.98,	3.35
	3.61,	1.97,	1.97,	1.97,	1.97,	1.97
	2.72,	1.98,	1.99,	1.98,	2.00,	2.00

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	0,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	0
