==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 17 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 901

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.999/1.999
- lns time: 1.998871
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007434
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.163/5.170
- lns time: 1.163446
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.975/6.145
- lns time: 0.974924
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007576
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005889
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.738/11.896
- lns time: 1.738431
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009342
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006341
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009727
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006495
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.013717
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008196
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005902
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005591
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.012328
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.056219
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009476
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005613
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008942
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008339
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006005
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005888
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.012384
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.692/46.779
- lns time: 0.692424
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005571
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.010580
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006466
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.013315
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005771
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004490
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.011197
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.832/61.669
- lns time: 0.831980
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008202
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006132
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005668
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007356
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005850
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006897
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005838
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007046
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006407
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008283
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007662
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.010390
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005640
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007510
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 0.236978
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 26

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 26
number of iterations: 48
total time: 90.004730

Final Solution: 0->0, 1->5, 2->20, 3->14, 4->3, 5->1, 6->25, 7->14, 8->27, 9->37, 10->26, 11->25, 12->5, 13->42, 14->47, 15->22, 16->19, 17->3, 18->36, 19->5, 20->46, 21->29, 22->4, 23->33, 24->20, 25->41, 26->14, 27->24, 28->41, 29->10, 30->0, 31->18, 32->9, 33->28, 34->7, 35->2, 36->47, 37->27, 38->38, 39->32, 40->7, 41->2, 42->2, 43->24, 44->3, 45->8, 46->11, 47->43, 48->19, 49->37, 50->25, 51->1, 52->26, 53->6, 54->24, 55->44, 56->15, 57->14, 58->19, 59->1, 60->43, 61->17, 62->42, 63->7, 64->35, 65->4, 66->24, 67->5, 68->9, 69->5, 70->47, 71->32, 72->27, 73->11, 74->39, 75->26, 76->46, 77->38, 78->45, 79->28, 80->1, 81->40, 82->16, 83->36, 84->18, 85->5, 86->24, 87->44, 88->28, 89->41, 90->0, 91->31, 92->40, 93->41, 94->30, 95->21, 96->23, 97->4, 98->23, 99->18, 100->11, 101->1, 102->33, 103->45, 104->10, 105->20, 106->31, 107->6, 108->0, 109->30, 110->45, 111->12, 112->21, 113->12, 114->20, 115->10, 116->37, 117->29, 118->7, 119->6, 120->9, 121->43, 122->3, 123->31, 124->40, 125->12, 126->2, 127->17, 128->28, 129->44, 130->36, 131->15, 132->26, 133->45, 134->21, 135->17, 136->22, 137->21, 138->42, 139->40, 140->34, 141->8, 142->31, 143->22, 144->33, 145->39, 146->30, 147->46, 148->29, 149->16, 150->10, 151->43, 152->0, 153->9, 154->43, 155->38, 156->4, 157->37, 158->32, 159->27, 160->33, 161->39, 162->43, 163->10, 164->22, 165->2, 166->22, 167->33, 168->15, 169->16, 170->25, 171->21, 172->36, 173->23, 174->32, 175->29, 176->41, 177->16, 178->27, 179->9, 180->13, 181->8, 182->11, 183->12, 184->22, 185->42, 186->35, 187->0, 188->6, 189->7, 190->17, 191->28, 192->46, 193->44, 194->32, 195->35, 196->19, 197->18, 198->13, 199->35, 200->14, 201->20, 202->19, 203->39, 204->34, 205->38, 206->28, 207->34, 208->24, 209->34, 210->23, 211->8, 212->44, 213->4, 214->38, 215->16, 216->4, 217->21, 218->36, 219->2, 220->30, 221->9, 222->13, 223->18, 224->17, 225->47, 226->32, 227->14, 228->19, 229->36, 230->13, 231->30, 232->45, 233->37, 234->27, 235->34, 236->46, 237->40, 238->10, 239->40, 240->7, 241->29, 242->34, 243->6, 244->1, 245->39, 246->31, 247->16, 248->3, 249->8, 250->15, 251->25, 252->35, 253->3, 254->20, 255->23, 256->42, 257->11, 258->15, 259->47, 260->41, 261->18, 262->47, 263->38, 264->13, 265->15, 266->29, 267->17, 268->46, 269->42, 270->26, 271->6, 272->33, 273->35, 274->11, 275->30, 276->8, 277->25, 278->12, 279->23, 280->13, 281->12, 282->31, 283->45, 284->37, 285->26, 286->44, 287->39

Average CPI map:
	1.10,	1.82,	7.55,	2.32,	7.49,	1.71
	1.23,	2.32,	13.62,	6.21,	5.81,	1.76
	2.05,	6.01,	5.92,	5.81,	16.82,	5.69
	2.08,	5.59,	5.53,	15.50,	18.15,	5.34
	4.08,	2.19,	5.15,	5.08,	5.09,	4.95
	2.01,	4.81,	4.81,	4.09,	13.58,	4.45
	5.48,	1.46,	9.99,	4.31,	4.30,	4.09
	3.82,	3.56,	3.32,	3.29,	2.52,	2.15

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	0,	1,	1,	0,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
