==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 8 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 501

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006479
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005596
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008839
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.730/6.750
- lns time: 0.729635
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007149
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006462
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006528
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006592
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004456
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009323
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.864/19.655
- lns time: 0.863978
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.748/20.403
- lns time: 0.747881
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007956
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005587
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006401
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007437
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006479
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006902
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008317
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006738
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006982
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008268
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005747
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005206
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010852
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008285
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008314
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007390
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004726
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005253
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005609
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.011034
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009637
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.608/64.164
- lns time: 1.608276
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006169
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005769
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.011803
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005305
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005246
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007639
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007015
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 1.077349
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009663
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.005454
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007064
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009990
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.011249
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 0.669250
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 24

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 24
number of iterations: 48
total time: 90.003278

Final Solution: 0->10, 1->5, 2->46, 3->41, 4->13, 5->36, 6->32, 7->27, 8->15, 9->43, 10->22, 11->26, 12->2, 13->0, 14->41, 15->35, 16->14, 17->30, 18->38, 19->40, 20->24, 21->21, 22->25, 23->47, 24->33, 25->15, 26->2, 27->26, 28->24, 29->3, 30->35, 31->4, 32->17, 33->19, 34->10, 35->12, 36->28, 37->11, 38->32, 39->29, 40->45, 41->22, 42->20, 43->18, 44->7, 45->29, 46->16, 47->36, 48->33, 49->3, 50->43, 51->5, 52->4, 53->19, 54->7, 55->21, 56->28, 57->26, 58->42, 59->31, 60->37, 61->35, 62->47, 63->40, 64->16, 65->47, 66->0, 67->22, 68->24, 69->27, 70->23, 71->26, 72->9, 73->9, 74->36, 75->40, 76->35, 77->37, 78->6, 79->43, 80->18, 81->8, 82->20, 83->7, 84->34, 85->28, 86->6, 87->8, 88->47, 89->25, 90->17, 91->17, 92->46, 93->25, 94->30, 95->30, 96->33, 97->40, 98->3, 99->46, 100->20, 101->0, 102->46, 103->40, 104->42, 105->8, 106->45, 107->21, 108->4, 109->31, 110->34, 111->23, 112->41, 113->12, 114->25, 115->15, 116->34, 117->36, 118->0, 119->10, 120->29, 121->4, 122->39, 123->27, 124->39, 125->41, 126->44, 127->44, 128->2, 129->28, 130->1, 131->42, 132->47, 133->46, 134->31, 135->27, 136->10, 137->42, 138->21, 139->39, 140->47, 141->2, 142->31, 143->9, 144->38, 145->19, 146->29, 147->37, 148->0, 149->25, 150->11, 151->38, 152->16, 153->17, 154->6, 155->21, 156->2, 157->28, 158->39, 159->6, 160->7, 161->35, 162->13, 163->23, 164->8, 165->4, 166->20, 167->3, 168->30, 169->0, 170->32, 171->12, 172->19, 173->21, 174->43, 175->43, 176->35, 177->46, 178->1, 179->23, 180->38, 181->5, 182->34, 183->12, 184->24, 185->27, 186->43, 187->30, 188->18, 189->2, 190->33, 191->44, 192->34, 193->1, 194->4, 195->18, 196->13, 197->11, 198->32, 199->13, 200->37, 201->22, 202->7, 203->12, 204->17, 205->11, 206->7, 207->20, 208->45, 209->45, 210->29, 211->20, 212->8, 213->32, 214->24, 215->3, 216->16, 217->32, 218->14, 219->9, 220->26, 221->25, 222->18, 223->22, 224->12, 225->38, 226->10, 227->19, 228->8, 229->23, 230->44, 231->40, 232->23, 233->33, 234->14, 235->15, 236->11, 237->1, 238->45, 239->24, 240->14, 241->44, 242->14, 243->16, 244->6, 245->41, 246->45, 247->5, 248->44, 249->34, 250->15, 251->15, 252->38, 253->9, 254->36, 255->5, 256->18, 257->27, 258->3, 259->10, 260->41, 261->36, 262->39, 263->31, 264->11, 265->16, 266->42, 267->31, 268->39, 269->14, 270->19, 271->33, 272->13, 273->17, 274->5, 275->22, 276->42, 277->1, 278->30, 279->29, 280->6, 281->9, 282->26, 283->13, 284->37, 285->28, 286->37, 287->1

Average CPI map:
	1.12,	5.37,	2.31,	7.06,	6.96,	1.77
	6.42,	2.12,	5.76,	2.23,	7.23,	13.19
	4.77,	4.76,	4.62,	4.53,	4.29,	3.71
	3.60,	3.44,	3.42,	3.40,	5.14,	3.06
	4.41,	2.45,	2.26,	2.21,	2.18,	2.18
	2.00,	2.16,	2.14,	2.14,	2.14,	2.12
	2.12,	2.12,	11.71,	2.13,	2.13,	2.12
	2.07,	2.69,	2.13,	3.93,	2.11,	2.11

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	1,	0,	0,	1
	1,	1,	0,	0,	0,	1
