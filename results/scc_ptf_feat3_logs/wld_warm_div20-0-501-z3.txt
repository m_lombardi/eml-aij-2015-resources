==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 501

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006294
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 0.974259
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.012559
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.652/5.645
- lns time: 0.651645
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.014287
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005247
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004823
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.732/12.401
- lns time: 0.731991
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005942
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009854
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.023/17.440
- lns time: 1.022931
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.464/18.904
- lns time: 1.464478
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008874
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005042
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 0.724716
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.838736
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006139
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.310883
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007153
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005056
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006623
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004729
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004620
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004601
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005346
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005159
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008607
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005516
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.970153
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006466
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006053
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006510
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005723
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009512
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004447
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004684
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007703
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007599
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006350
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007376
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005584
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006520
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006773
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005313
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005246
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006789
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006591
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008521
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.056554
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 23

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 23
number of iterations: 49
total time: 90.006576

Final Solution: 0->25, 1->26, 2->36, 3->37, 4->23, 5->23, 6->46, 7->37, 8->27, 9->26, 10->23, 11->21, 12->47, 13->30, 14->14, 15->17, 16->0, 17->21, 18->4, 19->45, 20->25, 21->32, 22->38, 23->14, 24->37, 25->15, 26->14, 27->36, 28->5, 29->10, 30->8, 31->13, 32->11, 33->35, 34->34, 35->4, 36->29, 37->24, 38->35, 39->22, 40->17, 41->25, 42->1, 43->18, 44->42, 45->7, 46->44, 47->20, 48->44, 49->2, 50->41, 51->15, 52->38, 53->27, 54->43, 55->9, 56->2, 57->12, 58->46, 59->39, 60->1, 61->7, 62->13, 63->5, 64->17, 65->42, 66->24, 67->47, 68->3, 69->16, 70->20, 71->16, 72->19, 73->40, 74->3, 75->4, 76->14, 77->23, 78->44, 79->7, 80->13, 81->4, 82->35, 83->31, 84->17, 85->47, 86->11, 87->20, 88->10, 89->33, 90->46, 91->37, 92->10, 93->29, 94->22, 95->39, 96->42, 97->32, 98->45, 99->2, 100->18, 101->19, 102->20, 103->42, 104->22, 105->3, 106->2, 107->14, 108->9, 109->21, 110->44, 111->30, 112->27, 113->33, 114->4, 115->27, 116->28, 117->15, 118->5, 119->30, 120->8, 121->10, 122->45, 123->24, 124->24, 125->46, 126->18, 127->26, 128->6, 129->35, 130->11, 131->43, 132->26, 133->33, 134->0, 135->34, 136->31, 137->38, 138->2, 139->25, 140->32, 141->45, 142->1, 143->40, 144->33, 145->38, 146->8, 147->23, 148->17, 149->3, 150->9, 151->14, 152->45, 153->29, 154->22, 155->11, 156->27, 157->44, 158->31, 159->46, 160->30, 161->26, 162->39, 163->41, 164->12, 165->6, 166->43, 167->35, 168->12, 169->41, 170->42, 171->8, 172->0, 173->7, 174->28, 175->19, 176->31, 177->5, 178->9, 179->31, 180->27, 181->38, 182->32, 183->12, 184->20, 185->43, 186->16, 187->25, 188->8, 189->21, 190->15, 191->19, 192->41, 193->45, 194->40, 195->47, 196->5, 197->0, 198->36, 199->35, 200->9, 201->40, 202->21, 203->12, 204->11, 205->23, 206->31, 207->10, 208->13, 209->6, 210->29, 211->1, 212->39, 213->15, 214->39, 215->34, 216->29, 217->2, 218->4, 219->3, 220->1, 221->26, 222->1, 223->47, 224->18, 225->6, 226->28, 227->24, 228->19, 229->30, 230->34, 231->8, 232->42, 233->12, 234->47, 235->9, 236->25, 237->43, 238->21, 239->34, 240->7, 241->17, 242->28, 243->32, 244->36, 245->0, 246->11, 247->5, 248->38, 249->18, 250->16, 251->6, 252->0, 253->10, 254->36, 255->24, 256->28, 257->19, 258->6, 259->41, 260->43, 261->40, 262->46, 263->33, 264->41, 265->13, 266->36, 267->37, 268->3, 269->44, 270->28, 271->16, 272->33, 273->16, 274->37, 275->15, 276->40, 277->29, 278->20, 279->13, 280->22, 281->30, 282->22, 283->7, 284->34, 285->18, 286->32, 287->39

Average CPI map:
	1.09,	4.46,	5.22,	4.15,	4.10,	1.30
	3.81,	2.79,	6.56,	3.58,	2.40,	3.45
	3.42,	1.13,	3.39,	3.37,	3.32,	3.27
	3.25,	3.21,	3.20,	3.08,	2.74,	3.08
	4.40,	2.69,	2.63,	2.52,	2.49,	2.49
	2.45,	2.46,	2.46,	2.43,	2.38,	2.03
	2.42,	2.31,	4.99,	2.28,	2.27,	2.25
	2.24,	4.17,	2.23,	3.67,	2.18,	2.18

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
