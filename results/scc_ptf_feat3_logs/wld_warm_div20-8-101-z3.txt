==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 8 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 101

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006170
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008428
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006573
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009687
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.822/9.853
- lns time: 1.821811
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006473
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006426
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.010043
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005943
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008034
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 0.291530
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.535/20.716
- lns time: 0.535372
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005427
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007423
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008891
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006981
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.016589
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005662
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.010350
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006513
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.010914
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008143
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 0.971766
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005567
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007646
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006697
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006399
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009438
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008687
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007759
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005648
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005643
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009105
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.010436
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006172
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.011410
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.975/69.850
- lns time: 1.974708
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009458
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005428
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007513
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.014302
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009173
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009126
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009248
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006825
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005931
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005942
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 0.072393
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 23

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 23
number of iterations: 48
total time: 90.005802

Final Solution: 0->2, 1->11, 2->46, 3->41, 4->13, 5->36, 6->32, 7->5, 8->15, 9->22, 10->26, 11->0, 12->2, 13->10, 14->41, 15->35, 16->14, 17->38, 18->38, 19->40, 20->26, 21->4, 22->25, 23->47, 24->33, 25->15, 26->24, 27->34, 28->38, 29->0, 30->35, 31->44, 32->22, 33->19, 34->24, 35->12, 36->28, 37->10, 38->32, 39->29, 40->7, 41->43, 42->20, 43->18, 44->5, 45->29, 46->16, 47->36, 48->33, 49->34, 50->44, 51->30, 52->4, 53->19, 54->17, 55->4, 56->28, 57->34, 58->9, 59->31, 60->37, 61->35, 62->47, 63->40, 64->16, 65->47, 66->45, 67->43, 68->0, 69->5, 70->23, 71->3, 72->30, 73->42, 74->36, 75->40, 76->35, 77->37, 78->6, 79->22, 80->18, 81->8, 82->20, 83->45, 84->3, 85->28, 86->21, 87->8, 88->47, 89->25, 90->45, 91->7, 92->46, 93->25, 94->1, 95->42, 96->33, 97->40, 98->3, 99->46, 100->20, 101->7, 102->46, 103->40, 104->38, 105->8, 106->45, 107->11, 108->21, 109->31, 110->3, 111->23, 112->41, 113->12, 114->25, 115->15, 116->3, 117->36, 118->22, 119->11, 120->29, 121->21, 122->39, 123->10, 124->39, 125->41, 126->0, 127->10, 128->9, 129->28, 130->21, 131->38, 132->47, 133->46, 134->31, 135->1, 136->11, 137->30, 138->21, 139->39, 140->47, 141->0, 142->31, 143->42, 144->27, 145->19, 146->29, 147->37, 148->7, 149->25, 150->1, 151->9, 152->16, 153->17, 154->44, 155->11, 156->27, 157->28, 158->39, 159->21, 160->17, 161->35, 162->13, 163->23, 164->8, 165->44, 166->20, 167->10, 168->42, 169->4, 170->32, 171->12, 172->19, 173->44, 174->44, 175->2, 176->35, 177->46, 178->17, 179->23, 180->42, 181->24, 182->2, 183->12, 184->6, 185->1, 186->27, 187->38, 188->18, 189->24, 190->33, 191->5, 192->26, 193->5, 194->6, 195->18, 196->13, 197->34, 198->32, 199->13, 200->37, 201->5, 202->7, 203->12, 204->43, 205->43, 206->43, 207->20, 208->45, 209->17, 210->29, 211->20, 212->8, 213->32, 214->26, 215->0, 216->16, 217->32, 218->14, 219->9, 220->3, 221->25, 222->18, 223->4, 224->12, 225->27, 226->34, 227->19, 228->8, 229->23, 230->27, 231->40, 232->23, 233->33, 234->14, 235->15, 236->34, 237->7, 238->22, 239->27, 240->14, 241->6, 242->14, 243->16, 244->4, 245->41, 246->45, 247->24, 248->24, 249->26, 250->15, 251->15, 252->30, 253->1, 254->36, 255->42, 256->18, 257->10, 258->2, 259->1, 260->41, 261->36, 262->39, 263->31, 264->43, 265->16, 266->30, 267->31, 268->39, 269->14, 270->19, 271->33, 272->13, 273->22, 274->6, 275->11, 276->9, 277->17, 278->30, 279->29, 280->6, 281->9, 282->26, 283->13, 284->37, 285->28, 286->37, 287->2

Average CPI map:
	0.78,	7.91,	1.93,	2.11,	4.95,	1.74
	2.07,	6.01,	5.76,	5.48,	6.78,	3.65
	4.77,	4.76,	4.62,	4.53,	4.29,	4.03
	3.60,	3.44,	3.42,	1.32,	3.21,	3.06
	2.50,	2.45,	1.36,	11.28,	2.18,	2.18
	2.16,	2.16,	2.14,	2.14,	13.78,	2.12
	2.12,	2.12,	2.12,	2.13,	2.13,	2.12
	2.12,	2.12,	12.02,	2.11,	2.11,	2.11

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	1,	0,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	1,	0,	0,	1
