==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 4 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 901

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 0.501750
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.005155
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.006927
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 0.791753
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.007145
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 1.204836
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.005476
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.005400
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.004466
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.263/15.796
- lns time: 1.263305
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
- lns time: 2.004401
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
- lns time: 2.004836
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
- lns time: 2.005639
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.334/23.145
- lns time: 1.334311
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.010379
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007151
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005804
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007958
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.174/32.350
- lns time: 1.173816
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.004855
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005466
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007709
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005836
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.010190
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.006435
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.991/46.382
- lns time: 1.991376
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 1.296003
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005366
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.008517
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.008022
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.006827
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 1.010506
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.020755
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005306
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005072
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005081
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.006438
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005855
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005170
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005313
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.007804
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005397
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005418
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.004756
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.011823
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.006966
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.008582
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.007217
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 1.169663
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 12

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 9
final number of satisfied efficiency constraints: 12
number of iterations: 49
total time: 90.004232

Final Solution: 0->29, 1->43, 2->34, 3->7, 4->1, 5->44, 6->39, 7->31, 8->35, 9->30, 10->47, 11->23, 12->34, 13->36, 14->45, 15->44, 16->35, 17->32, 18->20, 19->5, 20->23, 21->23, 22->3, 23->20, 24->9, 25->46, 26->0, 27->1, 28->21, 29->35, 30->36, 31->10, 32->15, 33->2, 34->42, 35->34, 36->4, 37->0, 38->11, 39->23, 40->27, 41->30, 42->16, 43->38, 44->11, 45->4, 46->29, 47->2, 48->28, 49->18, 50->25, 51->5, 52->17, 53->18, 54->10, 55->11, 56->24, 57->1, 58->40, 59->6, 60->32, 61->29, 62->28, 63->40, 64->37, 65->0, 66->4, 67->5, 68->2, 69->25, 70->7, 71->46, 72->31, 73->41, 74->10, 75->0, 76->8, 77->9, 78->21, 79->32, 80->21, 81->9, 82->24, 83->23, 84->1, 85->33, 86->31, 87->22, 88->6, 89->35, 90->13, 91->8, 92->12, 93->12, 94->9, 95->36, 96->42, 97->28, 98->12, 99->40, 100->14, 101->10, 102->25, 103->45, 104->11, 105->47, 106->29, 107->41, 108->25, 109->13, 110->35, 111->39, 112->46, 113->36, 114->31, 115->28, 116->15, 117->26, 118->39, 119->13, 120->41, 121->4, 122->8, 123->18, 124->30, 125->27, 126->24, 127->24, 128->44, 129->13, 130->37, 131->1, 132->3, 133->26, 134->0, 135->45, 136->26, 137->19, 138->27, 139->9, 140->5, 141->14, 142->16, 143->2, 144->19, 145->12, 146->3, 147->22, 148->15, 149->26, 150->25, 151->24, 152->8, 153->42, 154->38, 155->2, 156->29, 157->1, 158->19, 159->41, 160->33, 161->16, 162->43, 163->46, 164->31, 165->18, 166->17, 167->37, 168->20, 169->9, 170->17, 171->11, 172->8, 173->24, 174->17, 175->47, 176->29, 177->38, 178->16, 179->19, 180->22, 181->18, 182->14, 183->19, 184->37, 185->30, 186->45, 187->26, 188->44, 189->21, 190->15, 191->20, 192->7, 193->35, 194->43, 195->13, 196->5, 197->37, 198->36, 199->44, 200->21, 201->18, 202->27, 203->34, 204->4, 205->16, 206->30, 207->40, 208->22, 209->12, 210->36, 211->39, 212->33, 213->46, 214->19, 215->38, 216->30, 217->37, 218->28, 219->42, 220->5, 221->17, 222->43, 223->14, 224->21, 225->7, 226->45, 227->6, 228->32, 229->44, 230->16, 231->7, 232->42, 233->42, 234->47, 235->3, 236->26, 237->6, 238->14, 239->10, 240->28, 241->34, 242->14, 243->33, 244->20, 245->43, 246->43, 247->20, 248->17, 249->40, 250->41, 251->47, 252->15, 253->27, 254->6, 255->12, 256->31, 257->11, 258->38, 259->39, 260->45, 261->32, 262->7, 263->23, 264->39, 265->32, 266->6, 267->33, 268->41, 269->0, 270->25, 271->8, 272->3, 273->13, 274->15, 275->10, 276->34, 277->47, 278->4, 279->2, 280->22, 281->27, 282->33, 283->46, 284->22, 285->40, 286->38, 287->3

Average CPI map:
	1.40,	1.36,	2.28,	1.67,	2.16,	0.98
	2.12,	2.07,	2.06,	2.06,	2.06,	2.06
	2.14,	2.03,	2.00,	1.99,	1.98,	1.97
	2.64,	1.95,	1.95,	1.95,	1.94,	1.95
	2.43,	1.94,	1.94,	1.94,	1.94,	1.94
	1.93,	2.79,	1.94,	1.94,	1.93,	2.47
	1.93,	1.93,	1.93,	1.90,	1.93,	1.93
	2.23,	1.92,	1.92,	1.92,	2.46,	2.13

Efficiency Satisfaction Map:
	1,	0,	1,	1,	1,	1
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	0
	0,	0,	0,	0,	0,	1
	0,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	1
