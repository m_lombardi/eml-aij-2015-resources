LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
3.12567, 0.752061, 3.77408, 1.62275, 1.54955, 1.17589, 1.72939, 3.02359, 2.35293, 0.5, 3.11872, 1.27538, 17.84, 24.8309, 22.3046, 23.9911, 15.5276, 27.5057, 0.5, 1.8254, 3.13266, 3.48523, 1.94102, 1.1157, 2.11213, 2.06647, 1.99774, 1.74421, 2.0075, 2.07194, 0.522909, 1.49971, 1.5007, 3.20029, 3.94412, 1.33228, 2.47984, 1.60996, 1.63519, 2.5718, 1.81176, 1.89145, 3.25335, 0.5, 0.507717, 3.35651, 3.32506, 1.05735, 1.59151, 2.60202, 0.868737, 2.60868, 1.45236, 2.8767, 12.5817, 35, 12.3399, 20.615, 17.7576, 33.7058, 0.5, 2.2086, 4.49219, 0.521533, 0.703125, 3.57454, 1.32375, 0.5, 3.53538, 4.27056, 1.22948, 1.14083, 2.1446, 2.20424, 1.96104, 2.3198, 1.54687, 1.82346, 3.22698, 0.778605, 1.7414, 3.17747, 1.91045, 1.16509, 2.33292, 2.04677, 0.599128, 0.63446, 5.14332, 1.24341, 0.954258, 4.07886, 1.13576, 2.37027, 2.04029, 1.42056, 2.83431, 1.10327, 2.71016, 2.72216, 2.1301, 0.5, 1.53209, 2.281, 2.40503, 2.18578, 1.86167, 1.73443, 1.37888, 0.5, 3.68517, 2.2678, 3.31085, 0.85731, 1.65819, 3.48785, 2.43323, 0.5, 1.0792, 2.84153, 0.604221, 1.83231, 0.5, 1.61493, 4.32856, 3.11998, 2.26413, 1.06299, 1.88884, 2.27832, 2.21424, 2.29148, 3.33042, 0.786218, 1.01043, 3.19589, 0.5, 3.17704, 2.29831, 2.17468, 2.40911, 2.53069, 1.17653, 1.41068, 1.39781, 1.5314, 2.1906, 1.35996, 2.01672, 3.50351, 1.17117, 1.34719, 3.3889, 1.49879, 2.75224, 1.8417, 2.56654, 2.58568, 1.96223, 0.5, 2.58026, 1.8053, 0.5, 2.44487, 2.88778, 2.77599, 2.85053, 0.540841, 0.5, 4.13547, 4.13324, 0.602114, 1.27328, 1.3559, 2.10053, 2.27077, 1.69581, 2.41086, 2.27242, 1.24961, 1.70121, 3.12426, 2.78517, 0.96334, 1.21139, 2.21463, 2.55195, 2.33888, 1.12488, 1.28664, 2.49165, 2.206, 1.2706, 2.2993, 2.38943, 1.68816, 2.55896, 1.79355, 3.41952, 2.01037, 2.19211, 1.06076, 2.53684, 0.780401, 3.0517, 0.966597, 2.17906, 0.5, 3.13944, 2.16321, 4.00701, 1.14437, 1.66825, 1.46278, 0.5, 3.21759, 0.5, 2.20956, 3.44682, 1.98752, 2.67762, 1.17849, 1.89847, 1.28251, 2.3825, 2.37247, 1.96994, 2.09411, 2.33515, 1.65527, 1.94325, 1.54898, 1.9655, 2.55185, 2.18274, 0.5, 4.98818, 0.755168, 2.90778, 0.666132, 1.5655, 1.11219, 0.864467, 3.32768, 2.80025, 2.32992, 4.80928, 0.5, 0.877944, 0.527548, 0.644329, 4.6409, 2.55565, 2.59951, 2.2867, 1.42287, 0.851681, 2.28359, 4.41532, 0.5, 1.83263, 1.2317, 1.53574, 2.48461, 2.30571, 1.85041, 2.23376, 2.05481, 1.74023, 1.81507, 1.67615, 3.04315, 3.28507, 0.5, 2.93744, 0.558177, 2.41297, 2.36075, 2.18932, 1.05135, 1.98417, 2.00144, 1.52199, 2.30636, 1.96989, 1.97137, 1.243, 2.98739
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55078, operands = 136364
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 1, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 15587 itr]: obj = (23), mov = 15587, inf = 71.1%, acc = 19.6%, imp = 149

number of satisfied efficiency constraints: 23
[2 sec, 51833 itr]: obj = (25), mov = 51833, inf = 72.1%, acc = 16.7%, imp = 151

number of satisfied efficiency constraints: 25
[3 sec, 78941 itr]: obj = (25), mov = 78941, inf = 65.8%, acc = 19.7%, imp = 151

number of satisfied efficiency constraints: 25
[4 sec, 100980 itr]: obj = (26), mov = 100980, inf = 58.6%, acc = 23.3%, imp = 152

number of satisfied efficiency constraints: 26
[5 sec, 122930 itr]: obj = (26), mov = 122930, inf = 53.2%, acc = 25.6%, imp = 152

number of satisfied efficiency constraints: 26
[6 sec, 141731 itr]: obj = (26), mov = 141731, inf = 49.6%, acc = 28%, imp = 182

number of satisfied efficiency constraints: 26
[7 sec, 158927 itr]: obj = (26), mov = 158927, inf = 46.2%, acc = 30.2%, imp = 185

number of satisfied efficiency constraints: 26
[8 sec, 175885 itr]: obj = (26), mov = 175885, inf = 43.2%, acc = 31.8%, imp = 185

number of satisfied efficiency constraints: 26
[9 sec, 193556 itr]: obj = (26), mov = 193556, inf = 40.8%, acc = 33%, imp = 185

number of satisfied efficiency constraints: 26
[10 sec, 212810 itr]: obj = (27), mov = 212810, inf = 38.7%, acc = 33.8%, imp = 187

number of satisfied efficiency constraints: 27
[11 sec, 232638 itr]: obj = (27), mov = 232638, inf = 36.7%, acc = 34.2%, imp = 187

number of satisfied efficiency constraints: 27
[12 sec, 252525 itr]: obj = (27), mov = 252525, inf = 35.2%, acc = 34.5%, imp = 187

number of satisfied efficiency constraints: 27
[13 sec, 272801 itr]: obj = (27), mov = 272801, inf = 33.8%, acc = 34.7%, imp = 187

number of satisfied efficiency constraints: 27
[14 sec, 292855 itr]: obj = (27), mov = 292855, inf = 32.6%, acc = 34.8%, imp = 187

number of satisfied efficiency constraints: 27
[15 sec, 311516 itr]: obj = (27), mov = 311516, inf = 31.6%, acc = 35.1%, imp = 187

number of satisfied efficiency constraints: 27
[16 sec, 331072 itr]: obj = (27), mov = 331072, inf = 30.7%, acc = 35.3%, imp = 187

number of satisfied efficiency constraints: 27
[17 sec, 351336 itr]: obj = (27), mov = 351336, inf = 29.9%, acc = 35.4%, imp = 187

number of satisfied efficiency constraints: 27
[18 sec, 371162 itr]: obj = (27), mov = 371162, inf = 29.2%, acc = 35.6%, imp = 187

number of satisfied efficiency constraints: 27
[19 sec, 391050 itr]: obj = (27), mov = 391050, inf = 28.5%, acc = 35.8%, imp = 217

number of satisfied efficiency constraints: 27
[20 sec, 408373 itr]: obj = (27), mov = 408373, inf = 27.9%, acc = 36.2%, imp = 226

number of satisfied efficiency constraints: 27
[21 sec, 427950 itr]: obj = (27), mov = 427950, inf = 27.3%, acc = 36.4%, imp = 226

number of satisfied efficiency constraints: 27
[22 sec, 446450 itr]: obj = (27), mov = 446450, inf = 26.7%, acc = 36.5%, imp = 226

number of satisfied efficiency constraints: 27
[23 sec, 466140 itr]: obj = (27), mov = 466140, inf = 26.2%, acc = 36.6%, imp = 226

number of satisfied efficiency constraints: 27
[24 sec, 485792 itr]: obj = (27), mov = 485792, inf = 25.8%, acc = 36.7%, imp = 226

number of satisfied efficiency constraints: 27
[25 sec, 504473 itr]: obj = (27), mov = 504473, inf = 25.4%, acc = 36.8%, imp = 259

number of satisfied efficiency constraints: 27
[26 sec, 520815 itr]: obj = (27), mov = 520815, inf = 25%, acc = 37.3%, imp = 263

number of satisfied efficiency constraints: 27
[27 sec, 538945 itr]: obj = (27), mov = 538945, inf = 24.6%, acc = 37.5%, imp = 263

number of satisfied efficiency constraints: 27
[28 sec, 557158 itr]: obj = (27), mov = 557158, inf = 24.3%, acc = 37.7%, imp = 263

number of satisfied efficiency constraints: 27
[29 sec, 575756 itr]: obj = (27), mov = 575756, inf = 23.9%, acc = 37.8%, imp = 263

number of satisfied efficiency constraints: 27
[30 sec, 593891 itr]: obj = (27), mov = 593891, inf = 23.6%, acc = 38%, imp = 263

number of satisfied efficiency constraints: 27
[31 sec, 612149 itr]: obj = (27), mov = 612149, inf = 23.3%, acc = 38.1%, imp = 263

number of satisfied efficiency constraints: 27
[32 sec, 627998 itr]: obj = (27), mov = 627998, inf = 23.1%, acc = 38.5%, imp = 297

number of satisfied efficiency constraints: 27
[33 sec, 644925 itr]: obj = (27), mov = 644925, inf = 22.8%, acc = 38.7%, imp = 297

number of satisfied efficiency constraints: 27
[34 sec, 661465 itr]: obj = (27), mov = 661465, inf = 22.6%, acc = 38.9%, imp = 297

number of satisfied efficiency constraints: 27
[35 sec, 678460 itr]: obj = (27), mov = 678460, inf = 22.3%, acc = 39.1%, imp = 297

number of satisfied efficiency constraints: 27
[36 sec, 694964 itr]: obj = (27), mov = 694964, inf = 22.1%, acc = 39.3%, imp = 297

number of satisfied efficiency constraints: 27
[37 sec, 712222 itr]: obj = (27), mov = 712222, inf = 21.9%, acc = 39.4%, imp = 298

number of satisfied efficiency constraints: 27
[38 sec, 729489 itr]: obj = (27), mov = 729489, inf = 21.7%, acc = 39.5%, imp = 298

number of satisfied efficiency constraints: 27
[39 sec, 747240 itr]: obj = (27), mov = 747240, inf = 21.5%, acc = 39.6%, imp = 298

number of satisfied efficiency constraints: 27
[40 sec, 765204 itr]: obj = (27), mov = 765204, inf = 21.3%, acc = 39.7%, imp = 298

number of satisfied efficiency constraints: 27
[41 sec, 782493 itr]: obj = (27), mov = 782493, inf = 21.1%, acc = 39.8%, imp = 298

number of satisfied efficiency constraints: 27
[42 sec, 801406 itr]: obj = (27), mov = 801406, inf = 20.9%, acc = 39.8%, imp = 299

number of satisfied efficiency constraints: 27
[43 sec, 819743 itr]: obj = (27), mov = 819743, inf = 20.8%, acc = 39.7%, imp = 299

number of satisfied efficiency constraints: 27
[44 sec, 838632 itr]: obj = (27), mov = 838632, inf = 20.6%, acc = 39.7%, imp = 299

number of satisfied efficiency constraints: 27
[45 sec, 857313 itr]: obj = (27), mov = 857313, inf = 20.4%, acc = 39.7%, imp = 299

number of satisfied efficiency constraints: 27
[46 sec, 876383 itr]: obj = (27), mov = 876383, inf = 20.3%, acc = 39.7%, imp = 299

number of satisfied efficiency constraints: 27
[47 sec, 894749 itr]: obj = (27), mov = 894749, inf = 20.2%, acc = 39.7%, imp = 299

number of satisfied efficiency constraints: 27
[48 sec, 913426 itr]: obj = (27), mov = 913426, inf = 20%, acc = 39.6%, imp = 299

number of satisfied efficiency constraints: 27
[49 sec, 932267 itr]: obj = (27), mov = 932267, inf = 19.9%, acc = 39.6%, imp = 299

number of satisfied efficiency constraints: 27
[50 sec, 951534 itr]: obj = (27), mov = 951534, inf = 19.7%, acc = 39.6%, imp = 299

number of satisfied efficiency constraints: 27
[51 sec, 970219 itr]: obj = (27), mov = 970219, inf = 19.6%, acc = 39.6%, imp = 299

number of satisfied efficiency constraints: 27
[52 sec, 989173 itr]: obj = (27), mov = 989173, inf = 19.5%, acc = 39.6%, imp = 299

number of satisfied efficiency constraints: 27
[53 sec, 1008430 itr]: obj = (27), mov = 1008430, inf = 19.4%, acc = 39.6%, imp = 299

number of satisfied efficiency constraints: 27
[54 sec, 1027633 itr]: obj = (27), mov = 1027633, inf = 19.3%, acc = 39.5%, imp = 299

number of satisfied efficiency constraints: 27
[55 sec, 1046221 itr]: obj = (27), mov = 1046221, inf = 19.2%, acc = 39.5%, imp = 299

number of satisfied efficiency constraints: 27
[56 sec, 1065449 itr]: obj = (27), mov = 1065449, inf = 19.1%, acc = 39.5%, imp = 299

number of satisfied efficiency constraints: 27
[57 sec, 1084454 itr]: obj = (27), mov = 1084454, inf = 19%, acc = 39.5%, imp = 299

number of satisfied efficiency constraints: 27
[58 sec, 1103097 itr]: obj = (27), mov = 1103097, inf = 18.9%, acc = 39.5%, imp = 299

number of satisfied efficiency constraints: 27
[59 sec, 1121798 itr]: obj = (27), mov = 1121798, inf = 18.8%, acc = 39.5%, imp = 299

number of satisfied efficiency constraints: 27
[60 sec, 1141167 itr]: obj = (27), mov = 1141167, inf = 18.7%, acc = 39.4%, imp = 299

number of satisfied efficiency constraints: 27
[61 sec, 1160989 itr]: obj = (27), mov = 1160989, inf = 18.6%, acc = 39.4%, imp = 299

number of satisfied efficiency constraints: 27
[62 sec, 1179734 itr]: obj = (28), mov = 1179734, inf = 18.5%, acc = 39.4%, imp = 300

number of satisfied efficiency constraints: 28
[63 sec, 1200161 itr]: obj = (28), mov = 1200161, inf = 18.4%, acc = 39.3%, imp = 300

number of satisfied efficiency constraints: 28
[64 sec, 1221446 itr]: obj = (28), mov = 1221446, inf = 18.4%, acc = 39.2%, imp = 300

number of satisfied efficiency constraints: 28
[65 sec, 1241813 itr]: obj = (28), mov = 1241813, inf = 18.3%, acc = 39.1%, imp = 300

number of satisfied efficiency constraints: 28
[66 sec, 1262332 itr]: obj = (28), mov = 1262332, inf = 18.2%, acc = 39%, imp = 300

number of satisfied efficiency constraints: 28
[67 sec, 1282681 itr]: obj = (28), mov = 1282681, inf = 18.1%, acc = 38.9%, imp = 300

number of satisfied efficiency constraints: 28
[68 sec, 1303143 itr]: obj = (28), mov = 1303143, inf = 18.1%, acc = 38.9%, imp = 300

number of satisfied efficiency constraints: 28
[69 sec, 1322652 itr]: obj = (28), mov = 1322652, inf = 18%, acc = 38.8%, imp = 300

number of satisfied efficiency constraints: 28
[70 sec, 1343106 itr]: obj = (28), mov = 1343106, inf = 17.9%, acc = 38.7%, imp = 300

number of satisfied efficiency constraints: 28
[71 sec, 1363821 itr]: obj = (28), mov = 1363821, inf = 17.9%, acc = 38.7%, imp = 300

number of satisfied efficiency constraints: 28
[72 sec, 1384204 itr]: obj = (28), mov = 1384204, inf = 17.8%, acc = 38.6%, imp = 300

number of satisfied efficiency constraints: 28
[73 sec, 1405035 itr]: obj = (28), mov = 1405035, inf = 17.8%, acc = 38.5%, imp = 300

number of satisfied efficiency constraints: 28
[74 sec, 1425249 itr]: obj = (28), mov = 1425249, inf = 17.7%, acc = 38.5%, imp = 300

number of satisfied efficiency constraints: 28
[75 sec, 1445691 itr]: obj = (28), mov = 1445691, inf = 17.6%, acc = 38.4%, imp = 300

number of satisfied efficiency constraints: 28
[76 sec, 1466444 itr]: obj = (28), mov = 1466444, inf = 17.6%, acc = 38.4%, imp = 300

number of satisfied efficiency constraints: 28
[77 sec, 1487230 itr]: obj = (28), mov = 1487230, inf = 17.5%, acc = 38.3%, imp = 300

number of satisfied efficiency constraints: 28
[78 sec, 1507381 itr]: obj = (28), mov = 1507381, inf = 17.5%, acc = 38.3%, imp = 300

number of satisfied efficiency constraints: 28
[79 sec, 1528189 itr]: obj = (28), mov = 1528189, inf = 17.4%, acc = 38.2%, imp = 300

number of satisfied efficiency constraints: 28
[80 sec, 1547516 itr]: obj = (28), mov = 1547516, inf = 17.4%, acc = 38.2%, imp = 300

number of satisfied efficiency constraints: 28
[81 sec, 1566911 itr]: obj = (28), mov = 1566911, inf = 17.3%, acc = 38.1%, imp = 300

number of satisfied efficiency constraints: 28
[82 sec, 1587294 itr]: obj = (28), mov = 1587294, inf = 17.3%, acc = 38.1%, imp = 300

number of satisfied efficiency constraints: 28
[83 sec, 1608151 itr]: obj = (28), mov = 1608151, inf = 17.2%, acc = 38%, imp = 300

number of satisfied efficiency constraints: 28
[84 sec, 1628513 itr]: obj = (28), mov = 1628513, inf = 17.2%, acc = 38%, imp = 300

number of satisfied efficiency constraints: 28
[85 sec, 1649166 itr]: obj = (28), mov = 1649166, inf = 17.1%, acc = 37.9%, imp = 300

number of satisfied efficiency constraints: 28
[86 sec, 1670519 itr]: obj = (28), mov = 1670519, inf = 17.1%, acc = 37.9%, imp = 300

number of satisfied efficiency constraints: 28
[87 sec, 1691217 itr]: obj = (29), mov = 1691217, inf = 17%, acc = 37.8%, imp = 301

number of satisfied efficiency constraints: 29
[88 sec, 1712692 itr]: obj = (29), mov = 1712692, inf = 17%, acc = 37.7%, imp = 301

number of satisfied efficiency constraints: 29
[89 sec, 1734164 itr]: obj = (29), mov = 1734164, inf = 17%, acc = 37.7%, imp = 301

number of satisfied efficiency constraints: 29
[90 sec, 1756937 itr]: obj = (29), mov = 1756937, inf = 16.9%, acc = 37.6%, imp = 301

number of satisfied efficiency constraints: 29
[90 sec, 1756937 itr]: obj = (29), mov = 1756937, inf = 16.9%, acc = 37.6%, imp = 301

number of satisfied efficiency constraints: 29
1756937 iterations, 1756937 moves performed in 90 seconds
Feasible solution: obj = (29)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 29
Mapping: 0->32, 1->21, 2->47, 3->14, 4->47, 5->31, 6->28, 7->33, 8->46, 9->12, 10->42, 11->32, 12->16, 13->9, 14->12, 15->41, 16->35, 17->9, 18->29, 19->17, 20->36, 21->46, 22->9, 23->19, 24->5, 25->10, 26->25, 27->40, 28->21, 29->22, 30->14, 31->2, 32->38, 33->22, 34->30, 35->30, 36->8, 37->17, 38->6, 39->43, 40->3, 41->29, 42->13, 43->38, 44->45, 45->45, 46->8, 47->28, 48->39, 49->2, 50->0, 51->35, 52->19, 53->10, 54->11, 55->40, 56->9, 57->9, 58->40, 59->16, 60->26, 61->43, 62->36, 63->31, 64->19, 65->43, 66->4, 67->28, 68->18, 69->44, 70->35, 71->27, 72->1, 73->3, 74->5, 75->13, 76->45, 77->40, 78->44, 79->32, 80->22, 81->46, 82->29, 83->23, 84->25, 85->30, 86->11, 87->39, 88->44, 89->32, 90->2, 91->5, 92->29, 93->27, 94->22, 95->47, 96->29, 97->45, 98->37, 99->40, 100->7, 101->39, 102->2, 103->17, 104->6, 105->41, 106->10, 107->31, 108->15, 109->11, 110->21, 111->43, 112->32, 113->15, 114->18, 115->24, 116->41, 117->37, 118->41, 119->16, 120->27, 121->7, 122->38, 123->11, 124->18, 125->37, 126->20, 127->20, 128->19, 129->35, 130->13, 131->6, 132->7, 133->5, 134->33, 135->44, 136->34, 137->47, 138->11, 139->0, 140->20, 141->36, 142->14, 143->5, 144->36, 145->4, 146->24, 147->4, 148->15, 149->26, 150->26, 151->13, 152->42, 153->25, 154->33, 155->13, 156->23, 157->42, 158->0, 159->28, 160->42, 161->6, 162->38, 163->22, 164->17, 165->20, 166->22, 167->4, 168->0, 169->26, 170->44, 171->27, 172->26, 173->34, 174->1, 175->7, 176->1, 177->0, 178->37, 179->33, 180->41, 181->23, 182->24, 183->30, 184->12, 185->10, 186->8, 187->8, 188->28, 189->12, 190->1, 191->19, 192->37, 193->14, 194->47, 195->7, 196->25, 197->38, 198->43, 199->42, 200->45, 201->12, 202->3, 203->37, 204->33, 205->4, 206->39, 207->39, 208->36, 209->3, 210->44, 211->45, 212->17, 213->24, 214->35, 215->24, 216->31, 217->8, 218->28, 219->34, 220->46, 221->1, 222->17, 223->0, 224->26, 225->1, 226->40, 227->2, 228->36, 229->42, 230->25, 231->31, 232->6, 233->23, 234->23, 235->15, 236->29, 237->34, 238->30, 239->23, 240->12, 241->27, 242->18, 243->30, 244->46, 245->6, 246->16, 247->20, 248->5, 249->11, 250->20, 251->41, 252->16, 253->39, 254->43, 255->24, 256->18, 257->15, 258->18, 259->25, 260->35, 261->38, 262->13, 263->4, 264->21, 265->19, 266->27, 267->21, 268->2, 269->47, 270->3, 271->9, 272->46, 273->34, 274->10, 275->33, 276->8, 277->10, 278->31, 279->34, 280->21, 281->7, 282->3, 283->32, 284->16, 285->14, 286->15, 287->14
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.53317	1.99726	1.73707	1.9857	1.36786	1.87115
	2.07207	2.20887	2.55304	15.0459	2.38628	3.02027
	4.65482	2.08534	1.76337	1.37992	10.6204	2.02848
	2.60893	1.53607	1.60942	2.13157	2.39155	2.04277
	2.42781	1.80528	2.16099	1.3439	1.39307	2.21002
	2.42033	1.27836	2.00671	1.94096	1.02499	3.99613
	2.83799	1.77559	1.00433	1.33409	10.1696	6.00524
	2.55653	2.72149	3.99617	1.64181	2.96309	2.35429
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	1	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 2 --global-tlim 90 --seed 1

Starting the Optimization Process
