==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 6 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 101

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.864/0.864
- lns time: 0.864356
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.014706
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005197
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 2.019/6.904
- lns time: 2.019563
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004738
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.101/10.010
- lns time: 1.101570
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004693
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008021
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008167
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005412
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.011551
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004953
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007444
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004961
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004988
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004521
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004638
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008639
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.833/34.921
- lns time: 0.833361
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006612
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.622/37.550
- lns time: 0.622021
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005343
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008089
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007011
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005835
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005085
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005687
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005341
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006056
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004923
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004061
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006162
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004540
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007367
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004946
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006099
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.013028
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004867
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006564
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007151
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006892
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004545
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008892
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.010338
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005485
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.009126
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004726
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 0.285081
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 25

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 25
number of iterations: 48
total time: 90.003352

Final Solution: 0->0, 1->17, 2->27, 3->29, 4->40, 5->35, 6->16, 7->1, 8->6, 9->8, 10->1, 11->26, 12->25, 13->40, 14->20, 15->20, 16->2, 17->47, 18->22, 19->5, 20->27, 21->47, 22->9, 23->40, 24->39, 25->16, 26->36, 27->47, 28->33, 29->14, 30->13, 31->2, 32->2, 33->31, 34->11, 35->32, 36->38, 37->21, 38->3, 39->13, 40->14, 41->36, 42->0, 43->21, 44->35, 45->15, 46->31, 47->41, 48->26, 49->23, 50->37, 51->20, 52->5, 53->41, 54->6, 55->12, 56->5, 57->15, 58->24, 59->10, 60->7, 61->43, 62->35, 63->11, 64->28, 65->13, 66->7, 67->42, 68->45, 69->29, 70->9, 71->2, 72->10, 73->43, 74->7, 75->18, 76->6, 77->8, 78->40, 79->27, 80->7, 81->23, 82->42, 83->1, 84->14, 85->15, 86->38, 87->19, 88->8, 89->8, 90->33, 91->20, 92->5, 93->32, 94->42, 95->20, 96->36, 97->0, 98->30, 99->18, 100->15, 101->38, 102->35, 103->22, 104->18, 105->45, 106->46, 107->19, 108->22, 109->40, 110->3, 111->46, 112->8, 113->44, 114->34, 115->31, 116->37, 117->2, 118->39, 119->9, 120->4, 121->7, 122->19, 123->34, 124->28, 125->8, 126->17, 127->14, 128->39, 129->44, 130->11, 131->4, 132->12, 133->12, 134->27, 135->25, 136->38, 137->9, 138->26, 139->13, 140->42, 141->10, 142->12, 143->25, 144->25, 145->30, 146->34, 147->16, 148->14, 149->38, 150->0, 151->21, 152->1, 153->28, 154->36, 155->25, 156->4, 157->31, 158->21, 159->41, 160->18, 161->13, 162->39, 163->23, 164->26, 165->13, 166->10, 167->14, 168->37, 169->29, 170->45, 171->15, 172->28, 173->22, 174->28, 175->35, 176->4, 177->10, 178->40, 179->41, 180->10, 181->9, 182->46, 183->32, 184->36, 185->12, 186->1, 187->35, 188->42, 189->43, 190->17, 191->6, 192->37, 193->32, 194->20, 195->29, 196->26, 197->33, 198->26, 199->29, 200->45, 201->9, 202->16, 203->28, 204->22, 205->41, 206->21, 207->11, 208->41, 209->1, 210->39, 211->4, 212->31, 213->19, 214->42, 215->3, 216->24, 217->36, 218->3, 219->17, 220->27, 221->46, 222->18, 223->6, 224->47, 225->18, 226->44, 227->33, 228->34, 229->44, 230->31, 231->24, 232->19, 233->0, 234->23, 235->5, 236->12, 237->27, 238->2, 239->30, 240->16, 241->38, 242->30, 243->11, 244->19, 245->23, 246->7, 247->47, 248->37, 249->21, 250->43, 251->33, 252->22, 253->3, 254->45, 255->17, 256->25, 257->5, 258->24, 259->3, 260->24, 261->34, 262->15, 263->32, 264->29, 265->46, 266->43, 267->17, 268->30, 269->43, 270->44, 271->44, 272->33, 273->45, 274->11, 275->46, 276->24, 277->16, 278->30, 279->47, 280->6, 281->23, 282->37, 283->4, 284->39, 285->0, 286->34, 287->32

Average CPI map:
	1.93,	7.56,	7.54,	4.70,	8.07,	1.23
	7.18,	7.12,	6.97,	10.71,	3.00,	1.93
	6.38,	6.22,	6.18,	2.11,	5.89,	5.24
	5.72,	5.72,	7.91,	5.58,	5.50,	2.12
	2.11,	5.29,	5.27,	1.70,	4.96,	4.95
	4.45,	11.69,	9.16,	4.65,	13.30,	4.47
	4.31,	4.06,	3.95,	3.74,	3.54,	3.51
	3.49,	3.41,	3.23,	2.79,	12.91,	2.53

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
