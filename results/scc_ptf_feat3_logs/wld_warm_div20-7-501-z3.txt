==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 7 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 501

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.392/1.392
- lns time: 1.392413
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008398
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005911
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004385
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 1.996339
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009879
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007553
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006551
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006968
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008175
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008978
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007127
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007572
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007287
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007481
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007782
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005843
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007360
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.404/35.910
- lns time: 0.403651
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009321
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.010249
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009017
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009080
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005689
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.105/47.058
- lns time: 1.104950
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007591
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 1.128639
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008902
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007432
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005960
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007439
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008119
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005701
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008609
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.011900
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005712
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.014574
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005152
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006275
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009986
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007400
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005363
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007683
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 1.707502
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007170
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004475
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 1.966582
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 21

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 18
final number of satisfied efficiency constraints: 21
number of iterations: 47
total time: 90.006125

Final Solution: 0->2, 1->18, 2->10, 3->25, 4->41, 5->30, 6->15, 7->29, 8->22, 9->0, 10->43, 11->27, 12->5, 13->1, 14->15, 15->0, 16->31, 17->23, 18->7, 19->10, 20->22, 21->28, 22->37, 23->18, 24->2, 25->2, 26->3, 27->19, 28->21, 29->9, 30->47, 31->9, 32->31, 33->2, 34->33, 35->46, 36->47, 37->8, 38->34, 39->19, 40->13, 41->32, 42->16, 43->8, 44->11, 45->1, 46->45, 47->4, 48->6, 49->36, 50->24, 51->8, 52->23, 53->42, 54->14, 55->11, 56->13, 57->17, 58->16, 59->12, 60->17, 61->36, 62->35, 63->20, 64->9, 65->27, 66->21, 67->29, 68->23, 69->39, 70->43, 71->45, 72->27, 73->0, 74->46, 75->28, 76->36, 77->5, 78->46, 79->21, 80->33, 81->1, 82->17, 83->23, 84->21, 85->22, 86->32, 87->34, 88->19, 89->10, 90->24, 91->13, 92->6, 93->41, 94->2, 95->47, 96->41, 97->34, 98->42, 99->3, 100->32, 101->24, 102->44, 103->30, 104->28, 105->6, 106->29, 107->41, 108->42, 109->17, 110->21, 111->41, 112->4, 113->32, 114->45, 115->11, 116->38, 117->28, 118->15, 119->3, 120->8, 121->37, 122->12, 123->26, 124->26, 125->43, 126->43, 127->19, 128->5, 129->26, 130->19, 131->9, 132->15, 133->21, 134->39, 135->45, 136->31, 137->6, 138->13, 139->37, 140->43, 141->7, 142->26, 143->6, 144->39, 145->31, 146->7, 147->6, 148->32, 149->23, 150->0, 151->40, 152->24, 153->39, 154->28, 155->42, 156->27, 157->29, 158->42, 159->30, 160->18, 161->38, 162->14, 163->23, 164->29, 165->9, 166->20, 167->24, 168->10, 169->47, 170->12, 171->34, 172->45, 173->5, 174->11, 175->25, 176->14, 177->8, 178->1, 179->18, 180->37, 181->33, 182->35, 183->12, 184->27, 185->47, 186->12, 187->38, 188->13, 189->0, 190->38, 191->14, 192->4, 193->44, 194->33, 195->15, 196->4, 197->25, 198->33, 199->35, 200->30, 201->35, 202->14, 203->13, 204->4, 205->2, 206->20, 207->28, 208->36, 209->11, 210->30, 211->8, 212->22, 213->5, 214->22, 215->46, 216->12, 217->25, 218->44, 219->26, 220->16, 221->22, 222->17, 223->46, 224->3, 225->29, 226->19, 227->39, 228->30, 229->27, 230->26, 231->7, 232->37, 233->25, 234->40, 235->41, 236->40, 237->43, 238->20, 239->35, 240->16, 241->38, 242->11, 243->7, 244->14, 245->40, 246->38, 247->1, 248->45, 249->5, 250->31, 251->34, 252->47, 253->15, 254->37, 255->20, 256->33, 257->44, 258->42, 259->1, 260->40, 261->44, 262->18, 263->39, 264->3, 265->16, 266->0, 267->4, 268->16, 269->10, 270->24, 271->36, 272->7, 273->35, 274->34, 275->46, 276->3, 277->44, 278->18, 279->32, 280->17, 281->9, 282->10, 283->36, 284->40, 285->25, 286->31, 287->20

Average CPI map:
	1.11,	1.67,	4.53,	4.34,	2.03,	1.40
	3.87,	3.75,	4.67,	3.49,	3.42,	3.37
	3.34,	3.28,	3.27,	2.41,	3.00,	3.95
	5.82,	2.70,	2.61,	3.79,	2.45,	2.45
	2.43,	2.32,	2.30,	6.21,	2.27,	2.01
	2.23,	2.23,	2.20,	2.20,	2.19,	2.19
	4.96,	2.17,	2.17,	2.17,	2.16,	2.15
	2.15,	2.15,	2.14,	2.14,	2.13,	2.03

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	1,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
