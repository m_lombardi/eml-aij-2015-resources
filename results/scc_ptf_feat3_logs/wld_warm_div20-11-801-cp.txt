==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 801

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.009000
- lns branches: 6662
- lns fails: 3239
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.018000
- lns branches: 8041
- lns fails: 3889
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.007000
- lns branches: 13843
- lns fails: 6731
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.009/6.043
- branches (local/global): 31/28577
- fails (local/global): 2/13861
- lns time: 0.016000
- lns branches: 31
- lns fails: 2
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
- lns time: 2.012000
- lns branches: 9205
- lns fails: 4466
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.013/8.075
- branches (local/global): 253/38035
- fails (local/global): 101/18428
- lns time: 0.020000
- lns branches: 253
- lns fails: 101
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/8.097
- branches (local/global): 43/38078
- fails (local/global): 8/18436
- lns time: 0.023000
- lns branches: 43
- lns fails: 8
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007000
- lns branches: 7673
- lns fails: 3742
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007000
- lns branches: 16145
- lns fails: 7883
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.065/12.184
- branches (local/global): 363/62259
- fails (local/global): 166/30227
- lns time: 0.073000
- lns branches: 363
- lns fails: 166
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.008000
- lns branches: 8132
- lns fails: 3949
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.007/14.207
- branches (local/global): 27/70418
- fails (local/global): 0/34176
- lns time: 0.014000
- lns branches: 27
- lns fails: 0
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.006/14.220
- branches (local/global): 273/70691
- fails (local/global): 111/34287
- lns time: 0.012000
- lns branches: 273
- lns fails: 111
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.009000
- lns branches: 6587
- lns fails: 3205
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.003/16.238
- branches (local/global): 151/77429
- fails (local/global): 62/37554
- lns time: 0.010000
- lns branches: 151
- lns fails: 62
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.007000
- lns branches: 9515
- lns fails: 4604
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.007000
- lns branches: 8470
- lns fails: 4112
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.008000
- lns branches: 9453
- lns fails: 4595
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.027/22.294
- branches (local/global): 485/105352
- fails (local/global): 208/51073
- lns time: 0.035000
- lns branches: 485
- lns fails: 208
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008000
- lns branches: 9273
- lns fails: 4501
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.112/24.422
- branches (local/global): 1752/116377
- fails (local/global): 805/56379
- lns time: 0.119000
- lns branches: 1752
- lns fails: 805
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.008000
- lns branches: 9116
- lns fails: 4451
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.008000
- lns branches: 6869
- lns fails: 3343
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.437/28.882
- branches (local/global): 10626/142988
- fails (local/global): 5024/69197
- lns time: 0.446000
- lns branches: 10626
- lns fails: 5024
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.009000
- lns branches: 8809
- lns fails: 4296
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.008000
- lns branches: 5947
- lns fails: 2926
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.396/33.304
- branches (local/global): 7822/165566
- fails (local/global): 3742/80161
- lns time: 0.404000
- lns branches: 7822
- lns fails: 3742
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.664/33.976
- branches (local/global): 9670/175236
- fails (local/global): 4635/84796
- lns time: 0.672000
- lns branches: 9670
- lns fails: 4635
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 7836
- lns fails: 3868
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 7586
- lns fails: 3732
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 13105
- lns fails: 6458
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009000
- lns branches: 4352
- lns fails: 2130
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.813/43.830
- branches (local/global): 10437/218552
- fails (local/global): 5109/106093
- lns time: 1.822000
- lns branches: 10437
- lns fails: 5109
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.243/44.082
- branches (local/global): 10431/228983
- fails (local/global): 4905/110998
- lns time: 0.251000
- lns branches: 10431
- lns fails: 4905
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 12236
- lns fails: 5938
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 11459
- lns fails: 5637
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.247/48.352
- branches (local/global): 6647/259325
- fails (local/global): 3212/125785
- lns time: 0.256000
- lns branches: 6647
- lns fails: 3212
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.016/48.377
- branches (local/global): 454/259779
- fails (local/global): 202/125987
- lns time: 0.023000
- lns branches: 454
- lns fails: 202
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 16247
- lns fails: 7950
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.090/50.482
- branches (local/global): 899/276925
- fails (local/global): 403/134340
- lns time: 0.098000
- lns branches: 899
- lns fails: 403
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 8611
- lns fails: 4181
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 15261
- lns fails: 7356
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 7183
- lns fails: 3496
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 15492
- lns fails: 7651
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 8665
- lns fails: 4218
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 8432
- lns fails: 4099
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 11991
- lns fails: 5789
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 9667
- lns fails: 4713
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.009000
- lns branches: 6149
- lns fails: 3024
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 12871
- lns fails: 6381
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 6062
- lns fails: 2948
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 6050
- lns fails: 2936
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 9375
- lns fails: 4538
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 7336
- lns fails: 3602
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 6827
- lns fails: 3350
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 5962
- lns fails: 2925
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 9289
- lns fails: 4547
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 7327
- lns fails: 3573
LNS iteration #58 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 24006
- lns fails: 11825
LNS iteration #59 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 1.370000
- lns branches: 5356
- lns fails: 2609

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 9
final number of satisfied efficiency constraints: 25
number of iterations: 60
total time: 90007.000000
total branches: 468837
total fails: 228101

Final Solution: 0->38, 1->6, 2->36, 3->7, 4->35, 5->16, 6->44, 7->34, 8->20, 9->10, 10->28, 11->11, 12->26, 13->46, 14->30, 15->30, 16->2, 17->35, 18->44, 19->2, 20->21, 21->14, 22->41, 23->5, 24->47, 25->16, 26->35, 27->4, 28->35, 29->41, 30->38, 31->44, 32->10, 33->0, 34->0, 35->46, 36->15, 37->11, 38->9, 39->38, 40->32, 41->13, 42->34, 43->11, 44->12, 45->12, 46->41, 47->30, 48->25, 49->17, 50->1, 51->18, 52->33, 53->18, 54->4, 55->23, 56->22, 57->25, 58->29, 59->21, 60->39, 61->26, 62->32, 63->33, 64->18, 65->43, 66->4, 67->31, 68->1, 69->37, 70->35, 71->47, 72->27, 73->13, 74->24, 75->4, 76->21, 77->5, 78->26, 79->19, 80->8, 81->5, 82->43, 83->3, 84->9, 85->47, 86->47, 87->4, 88->47, 89->18, 90->4, 91->38, 92->10, 93->8, 94->27, 95->7, 96->37, 97->43, 98->29, 99->27, 100->19, 101->17, 102->19, 103->22, 104->24, 105->37, 106->15, 107->6, 108->16, 109->30, 110->43, 111->20, 112->3, 113->13, 114->14, 115->31, 116->24, 117->23, 118->17, 119->7, 120->45, 121->31, 122->34, 123->25, 124->12, 125->2, 126->12, 127->15, 128->28, 129->24, 130->18, 131->11, 132->7, 133->40, 134->45, 135->25, 136->9, 137->0, 138->47, 139->6, 140->6, 141->28, 142->19, 143->9, 144->21, 145->41, 146->42, 147->40, 148->11, 149->20, 150->45, 151->27, 152->8, 153->8, 154->44, 155->3, 156->24, 157->38, 158->12, 159->0, 160->7, 161->17, 162->25, 163->7, 164->10, 165->1, 166->45, 167->16, 168->31, 169->15, 170->25, 171->30, 172->1, 173->14, 174->10, 175->46, 176->19, 177->36, 178->3, 179->39, 180->23, 181->32, 182->33, 183->32, 184->20, 185->3, 186->41, 187->33, 188->19, 189->44, 190->43, 191->2, 192->1, 193->40, 194->36, 195->0, 196->13, 197->9, 198->44, 199->17, 200->16, 201->41, 202->22, 203->45, 204->22, 205->22, 206->29, 207->26, 208->5, 209->13, 210->14, 211->17, 212->37, 213->0, 214->8, 215->37, 216->15, 217->11, 218->29, 219->40, 220->6, 221->42, 222->2, 223->33, 224->20, 225->21, 226->27, 227->23, 228->8, 229->39, 230->10, 231->23, 232->1, 233->31, 234->39, 235->22, 236->14, 237->27, 238->35, 239->21, 240->31, 241->28, 242->46, 243->29, 244->14, 245->3, 246->18, 247->23, 248->38, 249->28, 250->15, 251->5, 252->34, 253->13, 254->32, 255->42, 256->32, 257->33, 258->40, 259->12, 260->24, 261->39, 262->5, 263->36, 264->30, 265->28, 266->34, 267->37, 268->45, 269->36, 270->2, 271->40, 272->26, 273->42, 274->29, 275->42, 276->20, 277->46, 278->42, 279->16, 280->9, 281->43, 282->39, 283->26, 284->46, 285->6, 286->36, 287->34

Average CPI map:
	1.10,	2.03,	1.62,	2.15,	2.18,	1.32
	2.20,	2.10,	1.34,	1.36,	2.74,	2.14
	2.07,	2.03,	1.03,	1.99,	0.92,	2.04
	2.08,	1.23,	1.96,	1.88,	1.82,	2.03
	2.00,	1.95,	1.94,	1.95,	1.19,	2.04
	2.44,	2.09,	1.50,	1.93,	1.93,	2.76
	2.13,	4.21,	1.03,	1.91,	3.14,	2.01
	2.04,	1.91,	1.97,	3.70,	2.80,	2.05

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	1,	1
	1,	0,	0,	1,	1,	1
