==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 3 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 501

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005308
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.011421
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.810/4.827
- lns time: 0.810571
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.810/5.637
- lns time: 0.809662
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.005662
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.006929
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.004724
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.005947
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.005689
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.004922
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.006462
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.007831
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.004970
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 1.402322
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.004678
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.004464
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.004629
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.006161
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.005167
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.017563
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.580/37.716
- lns time: 0.580617
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005698
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 1.993380
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005679
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005050
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.006170
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005354
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.006499
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.008006
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 1.567410
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.006026
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.004895
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 0.931216
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.004830
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.906/64.172
- lns time: 1.906280
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006739
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.005556
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006259
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004764
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006703
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006923
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.974/77.183
- lns time: 0.974475
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.706/78.889
- lns time: 1.705832
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 1.161960
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.005370
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.003847
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.010015
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.006554
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 1.927283
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 18

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 12
final number of satisfied efficiency constraints: 18
number of iterations: 49
total time: 90.004472

Final Solution: 0->8, 1->41, 2->11, 3->42, 4->40, 5->45, 6->20, 7->6, 8->42, 9->18, 10->25, 11->12, 12->3, 13->8, 14->17, 15->1, 16->2, 17->0, 18->17, 19->44, 20->3, 21->12, 22->5, 23->44, 24->4, 25->33, 26->43, 27->30, 28->13, 29->12, 30->32, 31->15, 32->11, 33->13, 34->0, 35->22, 36->29, 37->37, 38->47, 39->32, 40->18, 41->2, 42->11, 43->41, 44->40, 45->32, 46->34, 47->30, 48->19, 49->16, 50->4, 51->47, 52->27, 53->0, 54->38, 55->13, 56->23, 57->29, 58->39, 59->0, 60->35, 61->22, 62->4, 63->25, 64->15, 65->42, 66->23, 67->31, 68->29, 69->10, 70->6, 71->29, 72->21, 73->10, 74->37, 75->30, 76->47, 77->9, 78->15, 79->14, 80->7, 81->5, 82->9, 83->40, 84->19, 85->1, 86->45, 87->2, 88->8, 89->9, 90->24, 91->19, 92->42, 93->23, 94->11, 95->2, 96->32, 97->6, 98->9, 99->33, 100->36, 101->47, 102->39, 103->45, 104->20, 105->28, 106->30, 107->42, 108->24, 109->39, 110->10, 111->28, 112->36, 113->36, 114->2, 115->36, 116->14, 117->11, 118->41, 119->10, 120->9, 121->9, 122->45, 123->16, 124->6, 125->43, 126->1, 127->26, 128->34, 129->12, 130->45, 131->24, 132->43, 133->28, 134->43, 135->12, 136->21, 137->34, 138->35, 139->8, 140->46, 141->25, 142->43, 143->39, 144->13, 145->24, 146->19, 147->46, 148->14, 149->10, 150->38, 151->14, 152->5, 153->31, 154->17, 155->7, 156->15, 157->27, 158->36, 159->30, 160->27, 161->40, 162->35, 163->16, 164->34, 165->20, 166->41, 167->31, 168->38, 169->45, 170->18, 171->29, 172->37, 173->25, 174->22, 175->33, 176->0, 177->34, 178->37, 179->3, 180->17, 181->32, 182->2, 183->28, 184->23, 185->7, 186->1, 187->3, 188->40, 189->39, 190->38, 191->16, 192->6, 193->32, 194->0, 195->18, 196->18, 197->21, 198->26, 199->20, 200->36, 201->15, 202->16, 203->23, 204->30, 205->40, 206->35, 207->20, 208->14, 209->41, 210->10, 211->21, 212->31, 213->38, 214->35, 215->33, 216->28, 217->16, 218->15, 219->19, 220->44, 221->33, 222->43, 223->22, 224->46, 225->26, 226->4, 227->24, 228->26, 229->12, 230->33, 231->29, 232->19, 233->46, 234->34, 235->31, 236->4, 237->35, 238->8, 239->4, 240->44, 241->1, 242->7, 243->3, 244->46, 245->31, 246->27, 247->20, 248->5, 249->8, 250->6, 251->37, 252->25, 253->7, 254->3, 255->1, 256->13, 257->17, 258->7, 259->25, 260->22, 261->46, 262->39, 263->13, 264->21, 265->23, 266->17, 267->14, 268->26, 269->11, 270->18, 271->5, 272->38, 273->47, 274->44, 275->21, 276->27, 277->22, 278->24, 279->26, 280->42, 281->44, 282->41, 283->28, 284->5, 285->47, 286->27, 287->37

Average CPI map:
	1.79,	1.71,	4.77,	2.02,	1.79,	1.90
	2.22,	2.31,	2.28,	2.20,	2.17,	6.55
	2.07,	2.06,	2.06,	2.05,	2.05,	2.50
	2.05,	7.81,	2.04,	2.04,	2.03,	2.72
	4.09,	2.01,	2.43,	1.99,	1.99,	1.78
	2.06,	1.99,	1.99,	1.99,	1.39,	2.67
	2.27,	1.98,	1.98,	1.98,	1.99,	2.13
	3.08,	3.11,	1.98,	1.98,	1.99,	1.99

Efficiency Satisfaction Map:
	1,	1,	1,	1,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	0
