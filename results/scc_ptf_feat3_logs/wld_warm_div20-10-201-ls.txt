LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
3.11484, 1.31985, 3.06196, 2.24868, 1.56505, 0.689625, 2.94788, 4.22163, 0.5, 1.0955, 1.12584, 2.10916, 2.16532, 2.8622, 0.5, 3.9949, 1.10726, 1.37032, 0.840907, 1.02263, 5.44189, 3.42573, 0.5, 0.76885, 2.69363, 3.3641, 1.18983, 2.6822, 0.772127, 1.29811, 1.52187, 0.86324, 2.80671, 3.00534, 0.5, 3.30284, 2.12224, 1.93669, 2.21617, 1.98871, 1.86562, 1.87058, 2.39572, 0.65529, 0.894178, 2.74223, 2.47322, 2.83936, 3.04446, 0.671981, 2.00403, 0.71065, 2.52319, 3.04569, 3.15744, 1.00197, 0.51089, 0.5, 1.93431, 4.89539, 1.42669, 2.93932, 0.5, 1.77978, 3.33895, 2.01526, 2.53642, 1.64555, 1.64284, 1.76686, 2.51944, 1.88888, 2.09478, 1.99304, 2.10934, 2.07176, 2.10164, 1.62943, 4.71945, 0.944256, 1.22478, 0.907209, 2.84864, 1.35566, 1.29569, 1.0915, 2.45619, 1.42612, 2.69527, 3.03521, 2.44238, 2.14992, 2.14672, 1.82336, 1.14075, 2.29686, 1.07415, 1.0764, 3.20396, 2.56241, 3.42599, 0.657095, 2.17662, 0.5, 2.14023, 2.97797, 2.45487, 1.75032, 2.22379, 0.5, 0.873372, 1.46214, 5.50263, 1.43807, 2.3066, 0.5, 3.14549, 2.3214, 3.13423, 0.59228, 1.51608, 2.54255, 2.34912, 1.89862, 1.75884, 1.93478, 1.45848, 2.87459, 2.5519, 2.89483, 0.5, 1.7202, 22.9088, 20.9588, 19.8077, 17.8279, 23.9168, 26.58, 12.0166, 35, 17.52, 24.9915, 26.7989, 15.673, 1.76953, 3.54949, 0.937844, 3.29155, 1.95158, 0.5, 3.18083, 1.83118, 1.74062, 1.38675, 1.93425, 1.92638, 2.35013, 0.5, 2.474, 1.75192, 0.560141, 4.3638, 1.19019, 1.0697, 2.03575, 3.31605, 3.35632, 1.03198, 28.768, 13.8994, 0.611224, 25.0785, 29.237, 34.4058, 6.05207, 13.3041, 15.2356, 10.9548, 0.5, 13.9534, 2.33803, 1.06575, 2.09857, 2.33305, 2.16404, 2.00055, 2.61542, 1.54177, 0.5, 1.79488, 3.20907, 2.33885, 4.12849, 0.927456, 2.24663, 0.5, 0.680962, 3.51646, 0.608593, 2.05161, 1.06879, 1.42931, 0.669232, 6.17247, 12.5992, 21.4084, 28.5877, 25.309, 23.2977, 20.798, 31.1414, 35, 2.78184, 9.42673, 29.7011, 23.9489, 1.98877, 2.11658, 1.56475, 2.40997, 0.638422, 3.28151, 2.06456, 1.54087, 0.824402, 2.76175, 4.30842, 0.5, 1.85735, 1.85229, 1.81619, 1.95836, 2.23531, 2.2805, 2.91727, 2.90424, 0.5, 1.93073, 1.76373, 1.98403, 0.656259, 2.57474, 2.62819, 0.5, 2.86465, 2.77616, 4.14294, 0.657899, 0.5, 2.95018, 1.77226, 1.97673, 2.34938, 2.38527, 1.19575, 1.9998, 2.17922, 1.89058, 2.14808, 1.89555, 2.14984, 1.6986, 2.00209, 2.10584, 2.28654, 0.759182, 3.18437, 0.5, 3.47319, 1.79672, 3.13449, 0.5, 2.0656, 2.79866, 2.72589, 0.775361, 2.64321, 0.571303, 4.27622, 2.0708, 1.93847, 0.5, 3.6644, 1.06602, 0.5, 2.50283, 1.40451, 2.86225
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54982, operands = 136188
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 201, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 13245 itr]: obj = (26), mov = 13245, inf = 73.3%, acc = 18.9%, imp = 177

number of satisfied efficiency constraints: 26
[2 sec, 46669 itr]: obj = (27), mov = 46669, inf = 80.4%, acc = 12.1%, imp = 178

number of satisfied efficiency constraints: 27
[3 sec, 70668 itr]: obj = (28), mov = 70668, inf = 78.6%, acc = 12.5%, imp = 179

number of satisfied efficiency constraints: 28
[4 sec, 88040 itr]: obj = (28), mov = 88040, inf = 74.8%, acc = 14.1%, imp = 179

number of satisfied efficiency constraints: 28
[5 sec, 104584 itr]: obj = (28), mov = 104584, inf = 71.3%, acc = 15.6%, imp = 179

number of satisfied efficiency constraints: 28
[6 sec, 123848 itr]: obj = (28), mov = 123848, inf = 67.2%, acc = 17.7%, imp = 179

number of satisfied efficiency constraints: 28
[7 sec, 145117 itr]: obj = (28), mov = 145117, inf = 63.3%, acc = 20.3%, imp = 211

number of satisfied efficiency constraints: 28
[8 sec, 163495 itr]: obj = (28), mov = 163495, inf = 59.2%, acc = 22.7%, imp = 214

number of satisfied efficiency constraints: 28
[9 sec, 184511 itr]: obj = (30), mov = 184511, inf = 55.2%, acc = 23.7%, imp = 216

number of satisfied efficiency constraints: 30
[10 sec, 206567 itr]: obj = (30), mov = 206567, inf = 52%, acc = 24.4%, imp = 216

number of satisfied efficiency constraints: 30
[11 sec, 229188 itr]: obj = (31), mov = 229188, inf = 49.2%, acc = 24.9%, imp = 217

number of satisfied efficiency constraints: 31
[12 sec, 252056 itr]: obj = (31), mov = 252056, inf = 47%, acc = 25.2%, imp = 217

number of satisfied efficiency constraints: 31
[13 sec, 274521 itr]: obj = (31), mov = 274521, inf = 45.2%, acc = 25.5%, imp = 217

number of satisfied efficiency constraints: 31
[14 sec, 296652 itr]: obj = (31), mov = 296652, inf = 43.6%, acc = 25.8%, imp = 217

number of satisfied efficiency constraints: 31
[15 sec, 320186 itr]: obj = (31), mov = 320186, inf = 42.2%, acc = 26%, imp = 217

number of satisfied efficiency constraints: 31
[16 sec, 343080 itr]: obj = (31), mov = 343080, inf = 41%, acc = 26.1%, imp = 217

number of satisfied efficiency constraints: 31
[17 sec, 365315 itr]: obj = (31), mov = 365315, inf = 40%, acc = 26.3%, imp = 217

number of satisfied efficiency constraints: 31
[18 sec, 383989 itr]: obj = (31), mov = 383989, inf = 39.2%, acc = 26.9%, imp = 246

number of satisfied efficiency constraints: 31
[19 sec, 402067 itr]: obj = (31), mov = 402067, inf = 38.3%, acc = 27.5%, imp = 248

number of satisfied efficiency constraints: 31
[20 sec, 420670 itr]: obj = (31), mov = 420670, inf = 37.5%, acc = 27.9%, imp = 249

number of satisfied efficiency constraints: 31
[21 sec, 440185 itr]: obj = (31), mov = 440185, inf = 36.6%, acc = 28.3%, imp = 250

number of satisfied efficiency constraints: 31
[22 sec, 461344 itr]: obj = (31), mov = 461344, inf = 35.9%, acc = 28.4%, imp = 251

number of satisfied efficiency constraints: 31
[23 sec, 483242 itr]: obj = (32), mov = 483242, inf = 35.1%, acc = 28.5%, imp = 252

number of satisfied efficiency constraints: 32
[24 sec, 507224 itr]: obj = (32), mov = 507224, inf = 34.4%, acc = 28.4%, imp = 252

number of satisfied efficiency constraints: 32
[25 sec, 530818 itr]: obj = (32), mov = 530818, inf = 33.7%, acc = 28.3%, imp = 252

number of satisfied efficiency constraints: 32
[26 sec, 553989 itr]: obj = (32), mov = 553989, inf = 33.1%, acc = 28.3%, imp = 252

number of satisfied efficiency constraints: 32
[27 sec, 576294 itr]: obj = (32), mov = 576294, inf = 32.7%, acc = 28.3%, imp = 252

number of satisfied efficiency constraints: 32
[28 sec, 599581 itr]: obj = (32), mov = 599581, inf = 32.1%, acc = 28.2%, imp = 252

number of satisfied efficiency constraints: 32
[29 sec, 623226 itr]: obj = (32), mov = 623226, inf = 31.7%, acc = 28.2%, imp = 252

number of satisfied efficiency constraints: 32
[30 sec, 646493 itr]: obj = (32), mov = 646493, inf = 31.3%, acc = 28.2%, imp = 252

number of satisfied efficiency constraints: 32
[31 sec, 670711 itr]: obj = (32), mov = 670711, inf = 30.9%, acc = 28.1%, imp = 252

number of satisfied efficiency constraints: 32
[32 sec, 694425 itr]: obj = (32), mov = 694425, inf = 30.6%, acc = 28%, imp = 252

number of satisfied efficiency constraints: 32
[33 sec, 717397 itr]: obj = (32), mov = 717397, inf = 30.3%, acc = 28%, imp = 252

number of satisfied efficiency constraints: 32
[34 sec, 737006 itr]: obj = (32), mov = 737006, inf = 30%, acc = 28.3%, imp = 286

number of satisfied efficiency constraints: 32
[35 sec, 754055 itr]: obj = (32), mov = 754055, inf = 29.8%, acc = 28.6%, imp = 288

number of satisfied efficiency constraints: 32
[36 sec, 772459 itr]: obj = (32), mov = 772459, inf = 29.5%, acc = 28.8%, imp = 289

number of satisfied efficiency constraints: 32
[37 sec, 790562 itr]: obj = (32), mov = 790562, inf = 29.2%, acc = 29%, imp = 289

number of satisfied efficiency constraints: 32
[38 sec, 808730 itr]: obj = (32), mov = 808730, inf = 28.9%, acc = 29.2%, imp = 289

number of satisfied efficiency constraints: 32
[39 sec, 826394 itr]: obj = (32), mov = 826394, inf = 28.7%, acc = 29.4%, imp = 289

number of satisfied efficiency constraints: 32
[40 sec, 844158 itr]: obj = (32), mov = 844158, inf = 28.5%, acc = 29.6%, imp = 314

number of satisfied efficiency constraints: 32
[41 sec, 859383 itr]: obj = (32), mov = 859383, inf = 28.3%, acc = 29.9%, imp = 321

number of satisfied efficiency constraints: 32
[42 sec, 875436 itr]: obj = (32), mov = 875436, inf = 28.1%, acc = 30.2%, imp = 321

number of satisfied efficiency constraints: 32
[43 sec, 891809 itr]: obj = (32), mov = 891809, inf = 27.9%, acc = 30.4%, imp = 322

number of satisfied efficiency constraints: 32
[44 sec, 909666 itr]: obj = (32), mov = 909666, inf = 27.7%, acc = 30.6%, imp = 322

number of satisfied efficiency constraints: 32
[45 sec, 926424 itr]: obj = (32), mov = 926424, inf = 27.4%, acc = 30.8%, imp = 322

number of satisfied efficiency constraints: 32
[46 sec, 935748 itr]: obj = (32), mov = 935748, inf = 27.3%, acc = 30.9%, imp = 323

number of satisfied efficiency constraints: 32
[47 sec, 951559 itr]: obj = (32), mov = 951559, inf = 27.1%, acc = 31%, imp = 323

number of satisfied efficiency constraints: 32
[48 sec, 969570 itr]: obj = (32), mov = 969570, inf = 27%, acc = 31.1%, imp = 324

number of satisfied efficiency constraints: 32
[49 sec, 988101 itr]: obj = (32), mov = 988101, inf = 26.8%, acc = 31.1%, imp = 324

number of satisfied efficiency constraints: 32
[50 sec, 1005927 itr]: obj = (32), mov = 1005927, inf = 26.6%, acc = 31.2%, imp = 324

number of satisfied efficiency constraints: 32
[51 sec, 1025115 itr]: obj = (32), mov = 1025115, inf = 26.4%, acc = 31.2%, imp = 324

number of satisfied efficiency constraints: 32
[52 sec, 1044458 itr]: obj = (32), mov = 1044458, inf = 26.2%, acc = 31.3%, imp = 324

number of satisfied efficiency constraints: 32
[53 sec, 1065033 itr]: obj = (32), mov = 1065033, inf = 26%, acc = 31.3%, imp = 325

number of satisfied efficiency constraints: 32
[54 sec, 1085053 itr]: obj = (32), mov = 1085053, inf = 25.9%, acc = 31.3%, imp = 325

number of satisfied efficiency constraints: 32
[55 sec, 1106063 itr]: obj = (32), mov = 1106063, inf = 25.7%, acc = 31.3%, imp = 325

number of satisfied efficiency constraints: 32
[56 sec, 1125113 itr]: obj = (32), mov = 1125113, inf = 25.6%, acc = 31.3%, imp = 325

number of satisfied efficiency constraints: 32
[57 sec, 1142448 itr]: obj = (32), mov = 1142448, inf = 25.4%, acc = 31.3%, imp = 325

number of satisfied efficiency constraints: 32
[58 sec, 1151575 itr]: obj = (32), mov = 1151575, inf = 25.4%, acc = 31.2%, imp = 326

number of satisfied efficiency constraints: 32
[59 sec, 1170262 itr]: obj = (32), mov = 1170262, inf = 25.3%, acc = 31.1%, imp = 326

number of satisfied efficiency constraints: 32
[60 sec, 1185782 itr]: obj = (32), mov = 1185782, inf = 25.2%, acc = 31.1%, imp = 326

number of satisfied efficiency constraints: 32
[61 sec, 1207361 itr]: obj = (32), mov = 1207361, inf = 25%, acc = 31%, imp = 326

number of satisfied efficiency constraints: 32
[62 sec, 1229840 itr]: obj = (32), mov = 1229840, inf = 24.9%, acc = 30.9%, imp = 326

number of satisfied efficiency constraints: 32
[63 sec, 1251656 itr]: obj = (32), mov = 1251656, inf = 24.7%, acc = 30.9%, imp = 326

number of satisfied efficiency constraints: 32
[64 sec, 1274082 itr]: obj = (32), mov = 1274082, inf = 24.6%, acc = 30.8%, imp = 326

number of satisfied efficiency constraints: 32
[65 sec, 1295297 itr]: obj = (32), mov = 1295297, inf = 24.5%, acc = 30.7%, imp = 326

number of satisfied efficiency constraints: 32
[66 sec, 1316756 itr]: obj = (32), mov = 1316756, inf = 24.4%, acc = 30.7%, imp = 326

number of satisfied efficiency constraints: 32
[67 sec, 1333117 itr]: obj = (32), mov = 1333117, inf = 24.3%, acc = 30.7%, imp = 326

number of satisfied efficiency constraints: 32
[68 sec, 1353755 itr]: obj = (32), mov = 1353755, inf = 24.2%, acc = 30.6%, imp = 326

number of satisfied efficiency constraints: 32
[69 sec, 1376532 itr]: obj = (32), mov = 1376532, inf = 24%, acc = 30.6%, imp = 326

number of satisfied efficiency constraints: 32
[70 sec, 1398354 itr]: obj = (32), mov = 1398354, inf = 23.9%, acc = 30.5%, imp = 326

number of satisfied efficiency constraints: 32
[71 sec, 1420642 itr]: obj = (32), mov = 1420642, inf = 23.8%, acc = 30.4%, imp = 326

number of satisfied efficiency constraints: 32
[72 sec, 1443667 itr]: obj = (32), mov = 1443667, inf = 23.7%, acc = 30.4%, imp = 326

number of satisfied efficiency constraints: 32
[73 sec, 1466262 itr]: obj = (32), mov = 1466262, inf = 23.6%, acc = 30.3%, imp = 326

number of satisfied efficiency constraints: 32
[74 sec, 1488367 itr]: obj = (32), mov = 1488367, inf = 23.5%, acc = 30.3%, imp = 326

number of satisfied efficiency constraints: 32
[75 sec, 1510980 itr]: obj = (32), mov = 1510980, inf = 23.4%, acc = 30.2%, imp = 326

number of satisfied efficiency constraints: 32
[76 sec, 1532120 itr]: obj = (32), mov = 1532120, inf = 23.3%, acc = 30.2%, imp = 326

number of satisfied efficiency constraints: 32
[77 sec, 1554112 itr]: obj = (32), mov = 1554112, inf = 23.2%, acc = 30.1%, imp = 326

number of satisfied efficiency constraints: 32
[78 sec, 1576692 itr]: obj = (32), mov = 1576692, inf = 23.1%, acc = 30%, imp = 326

number of satisfied efficiency constraints: 32
[79 sec, 1599418 itr]: obj = (32), mov = 1599418, inf = 23.1%, acc = 30%, imp = 326

number of satisfied efficiency constraints: 32
[80 sec, 1621482 itr]: obj = (32), mov = 1621482, inf = 23%, acc = 30%, imp = 326

number of satisfied efficiency constraints: 32
[81 sec, 1643706 itr]: obj = (32), mov = 1643706, inf = 22.9%, acc = 29.9%, imp = 326

number of satisfied efficiency constraints: 32
[82 sec, 1665438 itr]: obj = (32), mov = 1665438, inf = 22.8%, acc = 29.9%, imp = 326

number of satisfied efficiency constraints: 32
[83 sec, 1688671 itr]: obj = (32), mov = 1688671, inf = 22.7%, acc = 29.8%, imp = 326

number of satisfied efficiency constraints: 32
[84 sec, 1711932 itr]: obj = (32), mov = 1711932, inf = 22.7%, acc = 29.8%, imp = 326

number of satisfied efficiency constraints: 32
[85 sec, 1735037 itr]: obj = (32), mov = 1735037, inf = 22.6%, acc = 29.7%, imp = 326

number of satisfied efficiency constraints: 32
[86 sec, 1757739 itr]: obj = (32), mov = 1757739, inf = 22.5%, acc = 29.7%, imp = 326

number of satisfied efficiency constraints: 32
[87 sec, 1778520 itr]: obj = (32), mov = 1778520, inf = 22.5%, acc = 29.7%, imp = 326

number of satisfied efficiency constraints: 32
[88 sec, 1798822 itr]: obj = (32), mov = 1798822, inf = 22.4%, acc = 29.6%, imp = 326

number of satisfied efficiency constraints: 32
[89 sec, 1820709 itr]: obj = (32), mov = 1820709, inf = 22.4%, acc = 29.6%, imp = 326

number of satisfied efficiency constraints: 32
[90 sec, 1844200 itr]: obj = (32), mov = 1844200, inf = 22.3%, acc = 29.5%, imp = 326

number of satisfied efficiency constraints: 32
[90 sec, 1844200 itr]: obj = (32), mov = 1844200, inf = 22.3%, acc = 29.5%, imp = 326

number of satisfied efficiency constraints: 32
1844200 iterations, 1844200 moves performed in 90 seconds
Feasible solution: obj = (32)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 32
Mapping: 0->30, 1->42, 2->23, 3->6, 4->35, 5->11, 6->35, 7->31, 8->33, 9->41, 10->0, 11->33, 12->0, 13->43, 14->21, 15->14, 16->28, 17->27, 18->29, 19->28, 20->39, 21->5, 22->5, 23->44, 24->6, 25->37, 26->18, 27->32, 28->25, 29->3, 30->45, 31->29, 32->9, 33->6, 34->22, 35->39, 36->42, 37->3, 38->16, 39->15, 40->24, 41->46, 42->40, 43->28, 44->44, 45->43, 46->2, 47->23, 48->36, 49->12, 50->9, 51->3, 52->41, 53->36, 54->8, 55->25, 56->19, 57->12, 58->35, 59->47, 60->24, 61->36, 62->38, 63->19, 64->23, 65->27, 66->5, 67->33, 68->24, 69->15, 70->36, 71->10, 72->17, 73->36, 74->34, 75->44, 76->13, 77->13, 78->18, 79->17, 80->33, 81->44, 82->16, 83->24, 84->47, 85->19, 86->37, 87->11, 88->31, 89->30, 90->21, 91->7, 92->37, 93->45, 94->11, 95->42, 96->2, 97->35, 98->2, 99->15, 100->9, 101->15, 102->23, 103->45, 104->4, 105->13, 106->47, 107->18, 108->43, 109->26, 110->25, 111->21, 112->29, 113->0, 114->38, 115->19, 116->10, 117->27, 118->27, 119->20, 120->23, 121->40, 122->30, 123->32, 124->26, 125->20, 126->14, 127->5, 128->10, 129->27, 130->32, 131->46, 132->20, 133->31, 134->8, 135->40, 136->16, 137->11, 138->39, 139->9, 140->39, 141->31, 142->26, 143->32, 144->10, 145->18, 146->13, 147->17, 148->46, 149->19, 150->3, 151->20, 152->14, 153->32, 154->41, 155->6, 156->45, 157->36, 158->43, 159->38, 160->14, 161->12, 162->5, 163->18, 164->2, 165->11, 166->30, 167->47, 168->34, 169->28, 170->25, 171->46, 172->37, 173->46, 174->6, 175->4, 176->15, 177->39, 178->26, 179->42, 180->26, 181->20, 182->46, 183->12, 184->27, 185->37, 186->14, 187->45, 188->32, 189->7, 190->17, 191->8, 192->31, 193->23, 194->8, 195->21, 196->42, 197->6, 198->22, 199->17, 200->1, 201->1, 202->18, 203->35, 204->40, 205->40, 206->39, 207->24, 208->34, 209->4, 210->16, 211->37, 212->8, 213->7, 214->9, 215->34, 216->20, 217->17, 218->30, 219->22, 220->28, 221->19, 222->2, 223->22, 224->38, 225->43, 226->29, 227->0, 228->2, 229->13, 230->10, 231->16, 232->11, 233->12, 234->43, 235->13, 236->38, 237->47, 238->15, 239->41, 240->33, 241->34, 242->42, 243->0, 244->3, 245->29, 246->3, 247->45, 248->22, 249->26, 250->14, 251->25, 252->41, 253->4, 254->1, 255->33, 256->16, 257->1, 258->30, 259->7, 260->44, 261->25, 262->21, 263->5, 264->12, 265->47, 266->41, 267->28, 268->31, 269->1, 270->34, 271->4, 272->21, 273->22, 274->7, 275->29, 276->8, 277->38, 278->4, 279->24, 280->10, 281->0, 282->40, 283->1, 284->44, 285->7, 286->35, 287->9
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.03821	1.40786	2.11817	2.35564	7.23398	2.10546
	3.24043	3.41597	5.49596	12.6333	2.18507	5.89797
	2.07265	2.06724	2.02364	3.99574	10.7101	2.28464
	2.158	1.27728	5.0536	1.49537	1.39302	2.31007
	5.61177	1.15567	5.80767	2.31668	2.97051	2.51112
	2.59472	10.0781	3.77344	1.35592	13.9722	2.51677
	2.34033	12.3674	1.07571	12.9706	10.073	2.17845
	3.83359	2.66354	1.21531	1.39917	11.1875	2.06131
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	1	0	0	1	1
	1	1	0	1	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 10 --global-tlim 90 --seed 201

Starting the Optimization Process
