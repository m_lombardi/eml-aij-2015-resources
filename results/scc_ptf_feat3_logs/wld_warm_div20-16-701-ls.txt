LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
1.94365, 1.72274, 3.0003, 2.67473, 0.708817, 1.94976, 2.19814, 1.98059, 1.65253, 2.4329, 1.55026, 2.18558, 1.48288, 2.34618, 2.63831, 0.569931, 2.33748, 2.62522, 0.804851, 1.52861, 1.80011, 2.60212, 2.33104, 2.93327, 1.94532, 4.65093, 0.878058, 0.5, 1.06048, 2.96521, 2.23336, 2.27321, 1.83764, 2.40798, 2.09804, 1.14978, 2.18893, 1.90383, 1.76951, 2.21741, 1.98643, 1.93389, 1.55668, 2.88121, 2.46075, 0.911159, 3.03305, 1.15715, 0.591041, 1.89129, 1.07596, 6.14979, 0.665151, 1.62677, 0.767756, 4.62481, 4.39743, 1.19803, 0.511976, 0.5, 1.96853, 2.16397, 1.90805, 2.06591, 1.81344, 2.0801, 3.14364, 2.68947, 3.07273, 0.992259, 1.12898, 0.972916, 1.28574, 3.11346, 2.32277, 3.49911, 0.5, 1.27892, 2.49159, 1.16019, 1.80184, 1.58221, 2.66877, 2.29539, 3.32212, 0.892106, 1.63738, 3.35417, 0.746758, 2.04747, 3.13617, 0.50463, 0.5, 2.24314, 3.80159, 1.81447, 3.23507, 1.39096, 1.87515, 0.5, 4.3616, 0.637222, 1.13916, 0.822893, 4.33047, 0.600166, 4.60731, 0.5, 1.53706, 1.61894, 2.89586, 3.09672, 1.62184, 1.22957, 2.25583, 2.5967, 1.38999, 1.90329, 1.28025, 2.57393, 5.29231, 1.79515, 1.08895, 2.52152, 0.5, 0.802071, 1.39208, 4.15828, 1.19659, 3.60276, 0.832847, 0.817443, 1.42375, 3.38068, 3.5672, 1.98301, 0.5, 1.14536, 2.08175, 1.67462, 1.4119, 2.48904, 2.36101, 1.98167, 0.831055, 2.3706, 2.51188, 1.69479, 2.39317, 2.1985, 2.14689, 1.49416, 2.03144, 2.24304, 1.86527, 2.21921, 0.5, 3.26911, 3.04269, 1.6405, 0.513251, 3.03445, 2.40604, 1.29186, 1.22159, 4.02385, 2.55665, 0.5, 0.5, 1.05656, 1.97776, 3.13619, 0.881539, 4.44794, 3.45001, 1.15696, 1.77274, 2.26232, 0.5, 2.85797, 2.69944, 2.17428, 2.35897, 0.949505, 1.29318, 2.52463, 2.31628, 1.77659, 2.19046, 1.3835, 2.0286, 2.30457, 2.11015, 1.89318, 1.85647, 1.92611, 2.07169, 2.1424, 13.0536, 21.2619, 23.5566, 26.8842, 25.3488, 21.8948, 1.7748, 0.834071, 3.4189, 0.867087, 3.53284, 1.57231, 3.08433, 0.5, 0.925509, 1.9174, 3.21161, 2.36115, 3.31126, 0.804972, 5.2321, 0.809171, 1.18812, 0.654377, 3.01691, 2.54381, 1.23675, 1.63031, 2.58394, 0.988287, 0.619142, 2.15659, 3.61914, 1.34939, 3.50477, 0.750974, 1.35392, 1.40387, 2.36883, 1.9597, 2.43715, 2.47654, 1.52286, 2.39294, 2.11697, 2.54111, 2.11235, 1.31377, 2.14366, 1.8421, 2.27972, 2.50482, 1.00258, 2.22712, 4.00905, 0.693261, 2.39196, 3.76351, 0.5, 0.642218, 2.10021, 2.42915, 1.13832, 3.29873, 0.550335, 2.48325, 1.22267, 2.23415, 4.01466, 1.01903, 3.00948, 0.5, 3.52561, 1.02266, 1.11455, 2.19579, 2.65783, 1.48356, 1.83991, 1.85062, 2.77056, 0.5, 2.90158, 2.13732, 1.16738, 1.15033, 2.96474, 3.88986, 1.97515, 0.852553
cpi min/max: 0.5/26.8842
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55175, operands = 136540
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 701, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 2014 itr]: infeas = (5, 5, 6), mov = 2014, inf = 70.8%, acc = 25.8%, imp = 134

number of satisfied efficiency constraints: 10
[2 sec, 23535 itr]: obj = (21), mov = 23535, inf = 68%, acc = 20.9%, imp = 147

number of satisfied efficiency constraints: 21
[3 sec, 45682 itr]: obj = (22), mov = 45682, inf = 67.5%, acc = 19.3%, imp = 148

number of satisfied efficiency constraints: 22
[4 sec, 62598 itr]: obj = (22), mov = 62598, inf = 61.5%, acc = 21.6%, imp = 148

number of satisfied efficiency constraints: 22
[5 sec, 77686 itr]: obj = (22), mov = 77686, inf = 56.6%, acc = 23.7%, imp = 148

number of satisfied efficiency constraints: 22
[6 sec, 93511 itr]: obj = (22), mov = 93511, inf = 52.3%, acc = 25.7%, imp = 148

number of satisfied efficiency constraints: 22
[7 sec, 109654 itr]: obj = (22), mov = 109654, inf = 48.6%, acc = 27.3%, imp = 148

number of satisfied efficiency constraints: 22
[8 sec, 126510 itr]: obj = (23), mov = 126510, inf = 45.6%, acc = 28.6%, imp = 149

number of satisfied efficiency constraints: 23
[9 sec, 142137 itr]: obj = (23), mov = 142137, inf = 43.3%, acc = 29.6%, imp = 185

number of satisfied efficiency constraints: 23
[10 sec, 157946 itr]: obj = (23), mov = 157946, inf = 40.8%, acc = 30.5%, imp = 186

number of satisfied efficiency constraints: 23
[11 sec, 173759 itr]: obj = (23), mov = 173759, inf = 38.8%, acc = 31%, imp = 186

number of satisfied efficiency constraints: 23
[12 sec, 189179 itr]: obj = (24), mov = 189179, inf = 37.1%, acc = 31.4%, imp = 187

number of satisfied efficiency constraints: 24
[13 sec, 206279 itr]: obj = (24), mov = 206279, inf = 35.5%, acc = 31.5%, imp = 187

number of satisfied efficiency constraints: 24
[14 sec, 222323 itr]: obj = (24), mov = 222323, inf = 34.3%, acc = 31.5%, imp = 187

number of satisfied efficiency constraints: 24
[15 sec, 238547 itr]: obj = (24), mov = 238547, inf = 33.1%, acc = 31.6%, imp = 187

number of satisfied efficiency constraints: 24
[16 sec, 255669 itr]: obj = (24), mov = 255669, inf = 32.1%, acc = 31.7%, imp = 187

number of satisfied efficiency constraints: 24
[17 sec, 272807 itr]: obj = (24), mov = 272807, inf = 31.2%, acc = 31.8%, imp = 187

number of satisfied efficiency constraints: 24
[18 sec, 289648 itr]: obj = (24), mov = 289648, inf = 30.4%, acc = 31.8%, imp = 187

number of satisfied efficiency constraints: 24
[19 sec, 306093 itr]: obj = (24), mov = 306093, inf = 29.8%, acc = 31.9%, imp = 187

number of satisfied efficiency constraints: 24
[20 sec, 319204 itr]: obj = (24), mov = 319204, inf = 29.2%, acc = 32.5%, imp = 220

number of satisfied efficiency constraints: 24
[21 sec, 334580 itr]: obj = (24), mov = 334580, inf = 28.7%, acc = 32.8%, imp = 221

number of satisfied efficiency constraints: 24
[22 sec, 350063 itr]: obj = (24), mov = 350063, inf = 28.1%, acc = 33%, imp = 221

number of satisfied efficiency constraints: 24
[23 sec, 365785 itr]: obj = (24), mov = 365785, inf = 27.6%, acc = 33.2%, imp = 221

number of satisfied efficiency constraints: 24
[24 sec, 381365 itr]: obj = (24), mov = 381365, inf = 27.1%, acc = 33.3%, imp = 221

number of satisfied efficiency constraints: 24
[25 sec, 397305 itr]: obj = (24), mov = 397305, inf = 26.7%, acc = 33.4%, imp = 222

number of satisfied efficiency constraints: 24
[26 sec, 414438 itr]: obj = (24), mov = 414438, inf = 26.3%, acc = 33.4%, imp = 222

number of satisfied efficiency constraints: 24
[27 sec, 431780 itr]: obj = (24), mov = 431780, inf = 25.8%, acc = 33.3%, imp = 222

number of satisfied efficiency constraints: 24
[28 sec, 448925 itr]: obj = (24), mov = 448925, inf = 25.4%, acc = 33.3%, imp = 222

number of satisfied efficiency constraints: 24
[29 sec, 465943 itr]: obj = (24), mov = 465943, inf = 25%, acc = 33.2%, imp = 222

number of satisfied efficiency constraints: 24
[30 sec, 482961 itr]: obj = (24), mov = 482961, inf = 24.7%, acc = 33.2%, imp = 222

number of satisfied efficiency constraints: 24
[31 sec, 501160 itr]: obj = (24), mov = 501160, inf = 24.4%, acc = 33.1%, imp = 222

number of satisfied efficiency constraints: 24
[32 sec, 518891 itr]: obj = (24), mov = 518891, inf = 24.1%, acc = 33%, imp = 222

number of satisfied efficiency constraints: 24
[33 sec, 537189 itr]: obj = (24), mov = 537189, inf = 23.9%, acc = 32.9%, imp = 222

number of satisfied efficiency constraints: 24
[34 sec, 554200 itr]: obj = (24), mov = 554200, inf = 23.7%, acc = 32.8%, imp = 222

number of satisfied efficiency constraints: 24
[35 sec, 572087 itr]: obj = (24), mov = 572087, inf = 23.4%, acc = 32.7%, imp = 222

number of satisfied efficiency constraints: 24
[36 sec, 589906 itr]: obj = (24), mov = 589906, inf = 23.2%, acc = 32.6%, imp = 222

number of satisfied efficiency constraints: 24
[37 sec, 608586 itr]: obj = (25), mov = 608586, inf = 23%, acc = 32.5%, imp = 223

number of satisfied efficiency constraints: 25
[38 sec, 627625 itr]: obj = (25), mov = 627625, inf = 22.8%, acc = 32.3%, imp = 223

number of satisfied efficiency constraints: 25
[39 sec, 647033 itr]: obj = (25), mov = 647033, inf = 22.6%, acc = 32.1%, imp = 223

number of satisfied efficiency constraints: 25
[40 sec, 666294 itr]: obj = (25), mov = 666294, inf = 22.4%, acc = 31.9%, imp = 223

number of satisfied efficiency constraints: 25
[41 sec, 685122 itr]: obj = (25), mov = 685122, inf = 22.2%, acc = 31.8%, imp = 223

number of satisfied efficiency constraints: 25
[42 sec, 704023 itr]: obj = (25), mov = 704023, inf = 22.1%, acc = 31.6%, imp = 223

number of satisfied efficiency constraints: 25
[43 sec, 723061 itr]: obj = (25), mov = 723061, inf = 21.9%, acc = 31.5%, imp = 223

number of satisfied efficiency constraints: 25
[44 sec, 741860 itr]: obj = (25), mov = 741860, inf = 21.7%, acc = 31.4%, imp = 223

number of satisfied efficiency constraints: 25
[45 sec, 760600 itr]: obj = (25), mov = 760600, inf = 21.6%, acc = 31.3%, imp = 223

number of satisfied efficiency constraints: 25
[46 sec, 779656 itr]: obj = (25), mov = 779656, inf = 21.5%, acc = 31.2%, imp = 223

number of satisfied efficiency constraints: 25
[47 sec, 798847 itr]: obj = (25), mov = 798847, inf = 21.4%, acc = 31.1%, imp = 223

number of satisfied efficiency constraints: 25
[48 sec, 813130 itr]: obj = (25), mov = 813130, inf = 21.3%, acc = 31%, imp = 223

number of satisfied efficiency constraints: 25
[49 sec, 828379 itr]: obj = (25), mov = 828379, inf = 21.2%, acc = 31%, imp = 223

number of satisfied efficiency constraints: 25
[50 sec, 846448 itr]: obj = (25), mov = 846448, inf = 21.1%, acc = 30.9%, imp = 223

number of satisfied efficiency constraints: 25
[51 sec, 865942 itr]: obj = (25), mov = 865942, inf = 21%, acc = 30.7%, imp = 223

number of satisfied efficiency constraints: 25
[52 sec, 884439 itr]: obj = (25), mov = 884439, inf = 20.9%, acc = 30.7%, imp = 223

number of satisfied efficiency constraints: 25
[53 sec, 904020 itr]: obj = (25), mov = 904020, inf = 20.8%, acc = 30.6%, imp = 223

number of satisfied efficiency constraints: 25
[54 sec, 922933 itr]: obj = (25), mov = 922933, inf = 20.7%, acc = 30.5%, imp = 223

number of satisfied efficiency constraints: 25
[55 sec, 942259 itr]: obj = (25), mov = 942259, inf = 20.6%, acc = 30.4%, imp = 223

number of satisfied efficiency constraints: 25
[56 sec, 961887 itr]: obj = (25), mov = 961887, inf = 20.5%, acc = 30.3%, imp = 223

number of satisfied efficiency constraints: 25
[57 sec, 981197 itr]: obj = (25), mov = 981197, inf = 20.4%, acc = 30.3%, imp = 223

number of satisfied efficiency constraints: 25
[58 sec, 1000391 itr]: obj = (25), mov = 1000391, inf = 20.3%, acc = 30.2%, imp = 223

number of satisfied efficiency constraints: 25
[59 sec, 1019395 itr]: obj = (25), mov = 1019395, inf = 20.3%, acc = 30.1%, imp = 223

number of satisfied efficiency constraints: 25
[60 sec, 1038020 itr]: obj = (25), mov = 1038020, inf = 20.2%, acc = 30.1%, imp = 223

number of satisfied efficiency constraints: 25
[61 sec, 1057479 itr]: obj = (25), mov = 1057479, inf = 20.1%, acc = 30%, imp = 223

number of satisfied efficiency constraints: 25
[62 sec, 1076329 itr]: obj = (25), mov = 1076329, inf = 20%, acc = 30%, imp = 223

number of satisfied efficiency constraints: 25
[63 sec, 1095345 itr]: obj = (25), mov = 1095345, inf = 20%, acc = 29.9%, imp = 223

number of satisfied efficiency constraints: 25
[64 sec, 1114708 itr]: obj = (25), mov = 1114708, inf = 19.9%, acc = 29.8%, imp = 223

number of satisfied efficiency constraints: 25
[65 sec, 1134340 itr]: obj = (25), mov = 1134340, inf = 19.9%, acc = 29.8%, imp = 223

number of satisfied efficiency constraints: 25
[66 sec, 1153382 itr]: obj = (25), mov = 1153382, inf = 19.8%, acc = 29.7%, imp = 223

number of satisfied efficiency constraints: 25
[67 sec, 1172013 itr]: obj = (25), mov = 1172013, inf = 19.7%, acc = 29.7%, imp = 223

number of satisfied efficiency constraints: 25
[68 sec, 1191064 itr]: obj = (25), mov = 1191064, inf = 19.7%, acc = 29.7%, imp = 223

number of satisfied efficiency constraints: 25
[69 sec, 1210257 itr]: obj = (25), mov = 1210257, inf = 19.6%, acc = 29.6%, imp = 223

number of satisfied efficiency constraints: 25
[70 sec, 1229974 itr]: obj = (25), mov = 1229974, inf = 19.6%, acc = 29.6%, imp = 223

number of satisfied efficiency constraints: 25
[71 sec, 1250193 itr]: obj = (25), mov = 1250193, inf = 19.5%, acc = 29.5%, imp = 223

number of satisfied efficiency constraints: 25
[72 sec, 1267901 itr]: obj = (25), mov = 1267901, inf = 19.5%, acc = 29.5%, imp = 271

number of satisfied efficiency constraints: 25
[73 sec, 1282217 itr]: obj = (25), mov = 1282217, inf = 19.5%, acc = 29.7%, imp = 271

number of satisfied efficiency constraints: 25
[74 sec, 1296340 itr]: obj = (25), mov = 1296340, inf = 19.4%, acc = 29.8%, imp = 272

number of satisfied efficiency constraints: 25
[75 sec, 1311772 itr]: obj = (25), mov = 1311772, inf = 19.4%, acc = 29.9%, imp = 272

number of satisfied efficiency constraints: 25
[76 sec, 1327828 itr]: obj = (25), mov = 1327828, inf = 19.3%, acc = 29.9%, imp = 272

number of satisfied efficiency constraints: 25
[77 sec, 1344007 itr]: obj = (25), mov = 1344007, inf = 19.3%, acc = 30%, imp = 272

number of satisfied efficiency constraints: 25
[78 sec, 1359854 itr]: obj = (25), mov = 1359854, inf = 19.2%, acc = 30.1%, imp = 272

number of satisfied efficiency constraints: 25
[79 sec, 1375199 itr]: obj = (25), mov = 1375199, inf = 19.2%, acc = 30.2%, imp = 272

number of satisfied efficiency constraints: 25
[80 sec, 1388127 itr]: obj = (25), mov = 1388127, inf = 19.1%, acc = 30.4%, imp = 308

number of satisfied efficiency constraints: 25
[81 sec, 1401837 itr]: obj = (25), mov = 1401837, inf = 19.1%, acc = 30.5%, imp = 309

number of satisfied efficiency constraints: 25
[82 sec, 1416459 itr]: obj = (25), mov = 1416459, inf = 19.1%, acc = 30.6%, imp = 309

number of satisfied efficiency constraints: 25
[83 sec, 1431205 itr]: obj = (25), mov = 1431205, inf = 19%, acc = 30.7%, imp = 309

number of satisfied efficiency constraints: 25
[84 sec, 1445757 itr]: obj = (25), mov = 1445757, inf = 19%, acc = 30.8%, imp = 309

number of satisfied efficiency constraints: 25
[85 sec, 1460518 itr]: obj = (25), mov = 1460518, inf = 19%, acc = 31%, imp = 309

number of satisfied efficiency constraints: 25
[86 sec, 1475255 itr]: obj = (25), mov = 1475255, inf = 18.9%, acc = 31.1%, imp = 309

number of satisfied efficiency constraints: 25
[87 sec, 1489440 itr]: obj = (25), mov = 1489440, inf = 18.9%, acc = 31.2%, imp = 309

number of satisfied efficiency constraints: 25
[88 sec, 1504666 itr]: obj = (25), mov = 1504666, inf = 18.8%, acc = 31.3%, imp = 310

number of satisfied efficiency constraints: 25
[89 sec, 1520829 itr]: obj = (25), mov = 1520829, inf = 18.8%, acc = 31.3%, imp = 310

number of satisfied efficiency constraints: 25
[90 sec, 1536390 itr]: obj = (25), mov = 1536390, inf = 18.8%, acc = 31.4%, imp = 310

number of satisfied efficiency constraints: 25
[90 sec, 1536390 itr]: obj = (25), mov = 1536390, inf = 18.8%, acc = 31.4%, imp = 310

number of satisfied efficiency constraints: 25
1536390 iterations, 1536390 moves performed in 90 seconds
Feasible solution: obj = (25)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 25
Mapping: 0->42, 1->13, 2->3, 3->15, 4->38, 5->4, 6->26, 7->13, 8->21, 9->17, 10->35, 11->23, 12->21, 13->28, 14->40, 15->16, 16->43, 17->26, 18->2, 19->32, 20->16, 21->22, 22->47, 23->30, 24->29, 25->10, 26->35, 27->45, 28->1, 29->40, 30->7, 31->21, 32->21, 33->42, 34->23, 35->39, 36->30, 37->41, 38->12, 39->38, 40->30, 41->1, 42->24, 43->5, 44->46, 45->13, 46->1, 47->14, 48->20, 49->28, 50->15, 51->18, 52->5, 53->1, 54->33, 55->36, 56->11, 57->29, 58->38, 59->26, 60->29, 61->28, 62->23, 63->14, 64->26, 65->29, 66->10, 67->6, 68->33, 69->2, 70->27, 71->11, 72->17, 73->6, 74->37, 75->12, 76->0, 77->45, 78->36, 79->8, 80->19, 81->22, 82->25, 83->8, 84->43, 85->7, 86->27, 87->34, 88->36, 89->27, 90->11, 91->22, 92->2, 93->16, 94->40, 95->36, 96->40, 97->15, 98->31, 99->37, 100->46, 101->18, 102->25, 103->0, 104->47, 105->39, 106->3, 107->27, 108->14, 109->9, 110->42, 111->46, 112->4, 113->25, 114->2, 115->46, 116->41, 117->28, 118->32, 119->44, 120->33, 121->31, 122->12, 123->24, 124->36, 125->18, 126->31, 127->37, 128->31, 129->20, 130->3, 131->1, 132->34, 133->39, 134->45, 135->7, 136->32, 137->13, 138->1, 139->24, 140->47, 141->17, 142->35, 143->5, 144->18, 145->18, 146->45, 147->8, 148->16, 149->27, 150->30, 151->26, 152->33, 153->43, 154->32, 155->18, 156->47, 157->35, 158->13, 159->42, 160->14, 161->37, 162->14, 163->6, 164->20, 165->6, 166->35, 167->38, 168->20, 169->45, 170->31, 171->43, 172->33, 173->44, 174->34, 175->7, 176->14, 177->9, 178->20, 179->34, 180->40, 181->17, 182->46, 183->39, 184->23, 185->36, 186->34, 187->41, 188->7, 189->22, 190->3, 191->44, 192->37, 193->15, 194->15, 195->12, 196->9, 197->42, 198->29, 199->39, 200->30, 201->44, 202->44, 203->2, 204->22, 205->19, 206->41, 207->47, 208->23, 209->4, 210->24, 211->25, 212->25, 213->22, 214->32, 215->19, 216->8, 217->9, 218->44, 219->33, 220->19, 221->37, 222->17, 223->28, 224->0, 225->28, 226->24, 227->19, 228->29, 229->11, 230->10, 231->0, 232->10, 233->11, 234->13, 235->41, 236->9, 237->11, 238->32, 239->7, 240->19, 241->8, 242->17, 243->16, 244->41, 245->20, 246->34, 247->26, 248->45, 249->38, 250->4, 251->8, 252->31, 253->6, 254->12, 255->47, 256->9, 257->38, 258->5, 259->46, 260->21, 261->43, 262->25, 263->0, 264->30, 265->5, 266->43, 267->4, 268->10, 269->21, 270->40, 271->42, 272->23, 273->2, 274->24, 275->12, 276->6, 277->27, 278->5, 279->0, 280->35, 281->39, 282->3, 283->3, 284->16, 285->10, 286->15, 287->4
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.14871	1.7589	4.77392	2.13113	1.33635	2.10549
	2.2753	1.82207	2.18028	1.60446	3.6363	2.22896
	2.02653	1.69274	1.57536	1.81107	2.08537	2.25264
	2.16832	1.44939	1.28819	1.48076	1.62744	2.02204
	2.34649	1.16889	1.74551	1.56049	2.07981	3.47746
	5.67246	2.04096	1.80381	2.14249	2.59097	2.25278
	2.11704	2.13001	1.18087	4.91323	3.14421	2.00092
	2.00884	3.0587	11.1319	1.86571	2.88398	2.20067
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	1	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 16 --global-tlim 90 --seed 701

Starting the Optimization Process
