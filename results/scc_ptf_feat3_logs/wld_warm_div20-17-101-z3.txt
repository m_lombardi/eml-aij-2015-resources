==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 17 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 101

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.011396
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006124
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007744
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005946
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006930
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005688
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007204
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005808
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009953
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.924/19.991
- lns time: 1.924340
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008697
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007226
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.623/24.630
- lns time: 0.622956
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.913/25.543
- lns time: 0.912868
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009340
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007587
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007037
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007570
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008188
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.016010
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005773
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.752/40.356
- lns time: 0.752090
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006600
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.006593
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009514
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.010760
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.617/50.007
- lns time: 1.617394
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005335
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008481
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004466
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006270
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.009523
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.871/61.913
- lns time: 1.871488
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009158
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008891
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007436
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.006431
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009240
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.005960
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.455/75.415
- lns time: 1.455301
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.005871
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.011719
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.006294
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.005413
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.007334
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008616
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.005693
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 0.538733
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 27

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 27
number of iterations: 48
total time: 90.004991

Final Solution: 0->38, 1->12, 2->5, 3->14, 4->8, 5->30, 6->29, 7->14, 8->27, 9->1, 10->47, 11->19, 12->5, 13->11, 14->22, 15->32, 16->12, 17->8, 18->32, 19->37, 20->46, 21->39, 22->4, 23->33, 24->32, 25->41, 26->14, 27->8, 28->41, 29->38, 30->22, 31->10, 32->0, 33->28, 34->7, 35->1, 36->6, 37->27, 38->30, 39->47, 40->45, 41->2, 42->29, 43->12, 44->26, 45->9, 46->7, 47->47, 48->19, 49->3, 50->22, 51->38, 52->1, 53->26, 54->37, 55->44, 56->15, 57->14, 58->43, 59->30, 60->42, 61->17, 62->19, 63->47, 64->35, 65->4, 66->9, 67->5, 68->39, 69->19, 70->1, 71->20, 72->27, 73->5, 74->29, 75->5, 76->46, 77->30, 78->31, 79->28, 80->3, 81->40, 82->2, 83->36, 84->24, 85->16, 86->34, 87->44, 88->28, 89->41, 90->6, 91->31, 92->40, 93->41, 94->2, 95->21, 96->23, 97->4, 98->23, 99->11, 100->18, 101->9, 102->21, 103->20, 104->3, 105->0, 106->25, 107->7, 108->18, 109->22, 110->31, 111->25, 112->20, 113->38, 114->32, 115->33, 116->8, 117->0, 118->3, 119->7, 120->9, 121->10, 122->37, 123->31, 124->40, 125->33, 126->16, 127->17, 128->28, 129->44, 130->12, 131->15, 132->25, 133->25, 134->26, 135->17, 136->18, 137->45, 138->12, 139->40, 140->36, 141->19, 142->20, 143->6, 144->34, 145->0, 146->24, 147->46, 148->29, 149->25, 150->21, 151->11, 152->30, 153->9, 154->11, 155->36, 156->4, 157->10, 158->32, 159->27, 160->12, 161->0, 162->11, 163->21, 164->19, 165->16, 166->22, 167->33, 168->15, 169->16, 170->6, 171->8, 172->31, 173->23, 174->20, 175->39, 176->41, 177->7, 178->27, 179->39, 180->13, 181->8, 182->22, 183->42, 184->18, 185->3, 186->35, 187->36, 188->6, 189->33, 190->17, 191->28, 192->46, 193->44, 194->9, 195->35, 196->45, 197->6, 198->13, 199->35, 200->14, 201->25, 202->45, 203->39, 204->30, 205->47, 206->28, 207->2, 208->37, 209->42, 210->23, 211->20, 212->44, 213->4, 214->38, 215->29, 216->4, 217->45, 218->3, 219->34, 220->1, 221->24, 222->13, 223->18, 224->17, 225->43, 226->32, 227->14, 228->43, 229->38, 230->13, 231->37, 232->10, 233->42, 234->27, 235->2, 236->46, 237->40, 238->11, 239->40, 240->33, 241->24, 242->45, 243->29, 244->26, 245->39, 246->31, 247->34, 248->37, 249->43, 250->15, 251->47, 252->35, 253->1, 254->24, 255->23, 256->43, 257->34, 258->15, 259->2, 260->41, 261->18, 262->7, 263->36, 264->13, 265->15, 266->0, 267->17, 268->46, 269->42, 270->26, 271->21, 272->42, 273->35, 274->26, 275->16, 276->16, 277->43, 278->21, 279->23, 280->13, 281->34, 282->10, 283->10, 284->36, 285->5, 286->44, 287->24

Average CPI map:
	1.57,	1.44,	5.65,	2.01,	7.49,	1.14
	7.11,	6.72,	6.54,	1.42,	2.44,	3.59
	2.90,	6.01,	5.92,	5.81,	12.09,	5.69
	5.62,	11.47,	13.83,	5.41,	5.38,	5.34
	2.06,	10.84,	6.14,	5.08,	5.09,	6.60
	4.88,	5.63,	2.49,	4.75,	1.83,	4.45
	4.38,	4.39,	4.34,	12.15,	4.30,	4.09
	4.20,	2.15,	3.32,	10.31,	2.52,	7.39

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	1,	1
	1,	1,	1,	0,	0,	1
	1,	1,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	1,	0,	1
	1,	0,	0,	1,	0,	1
