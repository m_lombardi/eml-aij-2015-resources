==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 7 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 601

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.028/0.028
- branches (local/global): 465/465
- fails (local/global): 205/205
- lns time: 0.036000
- lns branches: 465
- lns fails: 205
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.019/0.055
- branches (local/global): 28/493
- fails (local/global): 2/207
- lns time: 0.029000
- lns branches: 28
- lns fails: 2
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 14545
- lns fails: 7111
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.063/2.136
- branches (local/global): 868/15906
- fails (local/global): 401/7719
- lns time: 0.071000
- lns branches: 868
- lns fails: 401
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 14786
- lns fails: 7228
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.120/4.272
- branches (local/global): 1307/31999
- fails (local/global): 600/15547
- lns time: 0.129000
- lns branches: 1307
- lns fails: 600
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 8366
- lns fails: 4063
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 16007
- lns fails: 7800
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 11185
- lns fails: 5475
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 13749
- lns fails: 6715
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 12593
- lns fails: 6129
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007000
- lns branches: 25152
- lns fails: 12252
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.477/17.803
- branches (local/global): 17350/136401
- fails (local/global): 8423/66404
- lns time: 1.485000
- lns branches: 17350
- lns fails: 8423
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 14139
- lns fails: 6866
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 9589
- lns fails: 4655
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 11086
- lns fails: 5390
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.103/23.940
- branches (local/global): 874/172089
- fails (local/global): 400/83715
- lns time: 0.111000
- lns branches: 874
- lns fails: 400
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 14061
- lns fails: 6881
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 15087
- lns fails: 7276
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 8772
- lns fails: 4294
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 11382
- lns fails: 5611
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 8809
- lns fails: 4327
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.020/34.011
- branches (local/global): 250/230450
- fails (local/global): 101/112205
- lns time: 0.027000
- lns branches: 250
- lns fails: 101
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 18562
- lns fails: 9077
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 11727
- lns fails: 5690
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 32051
- lns fails: 15634
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.428/40.470
- branches (local/global): 3417/296207
- fails (local/global): 1635/144241
- lns time: 0.436000
- lns branches: 3417
- lns fails: 1635
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 12661
- lns fails: 6198
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 12719
- lns fails: 6194
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 20086
- lns fails: 9871
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 19316
- lns fails: 9401
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 14620
- lns fails: 7122
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 17060
- lns fails: 8401
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 13710
- lns fails: 6681
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.053/54.588
- branches (local/global): 1087/407466
- fails (local/global): 500/198609
- lns time: 0.061000
- lns branches: 1087
- lns fails: 500
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 23790
- lns fails: 11532
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.009000
- lns branches: 16760
- lns fails: 8160
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.041/58.654
- branches (local/global): 1432/449448
- fails (local/global): 648/218949
- lns time: 0.048000
- lns branches: 1432
- lns fails: 648
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.012000
- lns branches: 23623
- lns fails: 11496
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.009000
- lns branches: 12180
- lns fails: 5969
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.007000
- lns branches: 19296
- lns fails: 9452
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 16791
- lns fails: 8201
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.009000
- lns branches: 11745
- lns fails: 5732
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.009000
- lns branches: 13504
- lns fails: 6595
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.007000
- lns branches: 18232
- lns fails: 8961
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 23438
- lns fails: 11373
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.009000
- lns branches: 11507
- lns fails: 5614
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.007000
- lns branches: 19106
- lns fails: 9316
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.007000
- lns branches: 16619
- lns fails: 8201
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.224/80.977
- branches (local/global): 2545/638034
- fails (local/global): 1204/311063
- lns time: 0.233000
- lns branches: 2545
- lns fails: 1204
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 18289
- lns fails: 8979
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.007000
- lns branches: 19025
- lns fails: 9286
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.009000
- lns branches: 5822
- lns fails: 2860
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.009000
- lns branches: 19777
- lns fails: 9662
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 0.989000
- lns branches: 12162
- lns fails: 5934

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 18
final number of satisfied efficiency constraints: 29
number of iterations: 55
total time: 90008.000000
total branches: 713109
total fails: 347784

Final Solution: 0->2, 1->45, 2->39, 3->25, 4->4, 5->30, 6->47, 7->22, 8->46, 9->27, 10->32, 11->18, 12->31, 13->15, 14->27, 15->5, 16->47, 17->23, 18->17, 19->36, 20->40, 21->28, 22->37, 23->32, 24->2, 25->2, 26->6, 27->19, 28->32, 29->9, 30->0, 31->9, 32->36, 33->2, 34->33, 35->22, 36->5, 37->42, 38->4, 39->19, 40->4, 41->6, 42->16, 43->47, 44->18, 45->27, 46->0, 47->13, 48->35, 49->34, 50->1, 51->32, 52->36, 53->34, 54->14, 55->0, 56->45, 57->43, 58->16, 59->44, 60->15, 61->11, 62->32, 63->20, 64->9, 65->47, 66->29, 67->42, 68->36, 69->18, 70->5, 71->7, 72->15, 73->29, 74->47, 75->28, 76->23, 77->4, 78->22, 79->15, 80->33, 81->29, 82->43, 83->31, 84->13, 85->40, 86->31, 87->34, 88->19, 89->24, 90->12, 91->34, 92->17, 93->40, 94->2, 95->43, 96->5, 97->7, 98->47, 99->5, 100->5, 101->12, 102->24, 103->30, 104->44, 105->35, 106->22, 107->41, 108->1, 109->10, 110->43, 111->22, 112->44, 113->35, 114->45, 115->34, 116->38, 117->36, 118->29, 119->6, 120->27, 121->37, 122->28, 123->26, 124->26, 125->23, 126->35, 127->19, 128->1, 129->26, 130->19, 131->9, 132->27, 133->42, 134->18, 135->7, 136->12, 137->6, 138->10, 139->37, 140->10, 141->3, 142->26, 143->3, 144->39, 145->42, 146->39, 147->3, 148->3, 149->18, 150->23, 151->4, 152->12, 153->1, 154->12, 155->12, 156->11, 157->45, 158->42, 159->30, 160->11, 161->38, 162->14, 163->23, 164->21, 165->9, 166->20, 167->28, 168->31, 169->11, 170->28, 171->45, 172->34, 173->21, 174->11, 175->25, 176->14, 177->13, 178->15, 179->24, 180->37, 181->33, 182->6, 183->28, 184->29, 185->11, 186->44, 187->38, 188->35, 189->29, 190->38, 191->14, 192->41, 193->8, 194->33, 195->44, 196->22, 197->25, 198->33, 199->6, 200->30, 201->18, 202->14, 203->46, 204->0, 205->2, 206->20, 207->8, 208->23, 209->7, 210->30, 211->40, 212->1, 213->24, 214->41, 215->1, 216->31, 217->25, 218->8, 219->26, 220->16, 221->4, 222->21, 223->13, 224->3, 225->13, 226->19, 227->39, 228->30, 229->43, 230->26, 231->3, 232->37, 233->25, 234->41, 235->41, 236->41, 237->17, 238->20, 239->17, 240->16, 241->38, 242->21, 243->21, 244->14, 245->40, 246->38, 247->27, 248->7, 249->17, 250->7, 251->0, 252->39, 253->15, 254->37, 255->20, 256->33, 257->8, 258->36, 259->0, 260->35, 261->44, 262->31, 263->32, 264->46, 265->16, 266->8, 267->42, 268->16, 269->10, 270->24, 271->21, 272->40, 273->46, 274->45, 275->10, 276->17, 277->24, 278->43, 279->46, 280->13, 281->9, 282->10, 283->39, 284->46, 285->25, 286->8, 287->20

Average CPI map:
	3.59,	2.09,	4.53,	2.27,	1.60,	1.53
	4.17,	2.24,	2.39,	3.49,	2.31,	3.17
	2.27,	1.73,	3.27,	3.04,	3.00,	3.60
	2.19,	2.70,	2.61,	2.17,	1.32,	2.02
	2.04,	2.32,	2.30,	2.28,	1.32,	2.27
	2.23,	1.14,	10.55,	2.20,	5.40,	3.50
	3.96,	2.17,	2.17,	1.06,	3.64,	1.86
	2.12,	3.48,	4.81,	2.77,	2.41,	4.70

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	1,	0,	1,	1
	1,	0,	0,	0,	1,	1
	1,	1,	1,	0,	1,	1
