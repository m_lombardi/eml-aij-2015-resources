==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 13 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 1

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.009000
- lns branches: 25539
- lns fails: 12447
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.345/2.354
- branches (local/global): 3369/28908
- fails (local/global): 1600/14047
- lns time: 0.358000
- lns branches: 3369
- lns fails: 1600
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.005/2.372
- branches (local/global): 482/29390
- fails (local/global): 204/14251
- lns time: 0.012000
- lns branches: 482
- lns fails: 204
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.038/2.417
- branches (local/global): 479/29869
- fails (local/global): 204/14455
- lns time: 0.047000
- lns branches: 479
- lns fails: 204
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.007/2.433
- branches (local/global): 87/29956
- fails (local/global): 30/14485
- lns time: 0.015000
- lns branches: 87
- lns fails: 30
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.007000
- lns branches: 30790
- lns fails: 15078
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.009000
- lns branches: 7970
- lns fails: 3872
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.015000
- lns branches: 14422
- lns fails: 7085
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.006/8.478
- branches (local/global): 24/83162
- fails (local/global): 0/40520
- lns time: 0.014000
- lns branches: 24
- lns fails: 0
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007000
- lns branches: 21077
- lns fails: 10396
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.010000
- lns branches: 18963
- lns fails: 9220
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.023/12.526
- branches (local/global): 186/123388
- fails (local/global): 79/60215
- lns time: 0.031000
- lns branches: 186
- lns fails: 79
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.014000
- lns branches: 7794
- lns fails: 3801
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.010000
- lns branches: 10551
- lns fails: 5099
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.009000
- lns branches: 48854
- lns fails: 23816
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.021/18.588
- branches (local/global): 428/191015
- fails (local/global): 192/93123
- lns time: 0.030000
- lns branches: 428
- lns fails: 192
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.043/18.640
- branches (local/global): 3038/194053
- fails (local/global): 1406/94529
- lns time: 0.053000
- lns branches: 3038
- lns fails: 1406
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.467/19.117
- branches (local/global): 5272/199325
- fails (local/global): 2519/97048
- lns time: 0.475000
- lns branches: 5272
- lns fails: 2519
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.011000
- lns branches: 24247
- lns fails: 11739
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.002/21.138
- branches (local/global): 73/223645
- fails (local/global): 24/108811
- lns time: 0.013000
- lns branches: 73
- lns fails: 24
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 14901
- lns fails: 7382
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 10265
- lns fails: 4968
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.013/25.178
- branches (local/global): 244/249055
- fails (local/global): 101/121262
- lns time: 0.021000
- lns branches: 244
- lns fails: 101
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 18022
- lns fails: 8829
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009000
- lns branches: 8913
- lns fails: 4340
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 8385
- lns fails: 4118
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.131/31.342
- branches (local/global): 1095/285470
- fails (local/global): 525/139074
- lns time: 0.138000
- lns branches: 1095
- lns fails: 525
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 15780
- lns fails: 7786
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 8962
- lns fails: 4338
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 8159
- lns fails: 3947
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.013000
- lns branches: 11568
- lns fails: 5657
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 14526
- lns fails: 7124
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 12246
- lns fails: 6005
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.014000
- lns branches: 13371
- lns fails: 6524
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.229/46.645
- branches (local/global): 10377/380459
- fails (local/global): 5019/185474
- lns time: 1.236000
- lns branches: 10377
- lns fails: 5019
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 14071
- lns fails: 6872
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 17686
- lns fails: 8689
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 6943
- lns fails: 3401
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 16537
- lns fails: 8202
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.014000
- lns branches: 14818
- lns fails: 7275
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.240/56.938
- branches (local/global): 1077/451591
- fails (local/global): 511/220424
- lns time: 0.253000
- lns branches: 1077
- lns fails: 511
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 7533
- lns fails: 3730
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010000
- lns branches: 12069
- lns fails: 5770
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010000
- lns branches: 10286
- lns fails: 5043
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.140/63.119
- branches (local/global): 858/482337
- fails (local/global): 405/235372
- lns time: 0.151000
- lns branches: 858
- lns fails: 405
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 18585
- lns fails: 9148
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.012000
- lns branches: 9399
- lns fails: 4601
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.014000
- lns branches: 7386
- lns fails: 3631
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/69.180
- branches (local/global): 902/518609
- fails (local/global): 411/253163
- lns time: 0.024000
- lns branches: 902
- lns fails: 411
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.018000
- lns branches: 10914
- lns fails: 5364
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.011000
- lns branches: 6529
- lns fails: 3199
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.012000
- lns branches: 7602
- lns fails: 3744
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 14966
- lns fails: 7399
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/77.253
- branches (local/global): 365/558985
- fails (local/global): 158/273027
- lns time: 0.023000
- lns branches: 365
- lns fails: 158
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 5836
- lns fails: 2835
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 16380
- lns fails: 8059
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.014000
- lns branches: 9098
- lns fails: 4478
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.011000
- lns branches: 8651
- lns fails: 4249
LNS iteration #58 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.012000
- lns branches: 6679
- lns fails: 3277
LNS iteration #59 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.011000
- lns branches: 8170
- lns fails: 4001
LNS iteration #60 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 0.688000
- lns branches: 3117
- lns fails: 1512

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 11
final number of satisfied efficiency constraints: 26
number of iterations: 61
total time: 90011.000000
total branches: 616916
total fails: 301438

Final Solution: 0->5, 1->41, 2->4, 3->15, 4->3, 5->46, 6->16, 7->26, 8->14, 9->27, 10->41, 11->39, 12->12, 13->23, 14->37, 15->39, 16->33, 17->46, 18->47, 19->6, 20->18, 21->15, 22->24, 23->46, 24->36, 25->33, 26->5, 27->4, 28->45, 29->20, 30->19, 31->47, 32->27, 33->5, 34->24, 35->22, 36->9, 37->10, 38->27, 39->43, 40->8, 41->0, 42->18, 43->8, 44->28, 45->23, 46->30, 47->7, 48->5, 49->37, 50->35, 51->45, 52->33, 53->19, 54->47, 55->7, 56->22, 57->23, 58->9, 59->35, 60->15, 61->4, 62->37, 63->9, 64->12, 65->41, 66->11, 67->18, 68->37, 69->38, 70->5, 71->11, 72->13, 73->17, 74->46, 75->4, 76->33, 77->36, 78->11, 79->14, 80->40, 81->27, 82->39, 83->25, 84->18, 85->32, 86->1, 87->6, 88->39, 89->26, 90->7, 91->14, 92->31, 93->6, 94->28, 95->24, 96->18, 97->27, 98->36, 99->6, 100->46, 101->40, 102->21, 103->47, 104->22, 105->34, 106->34, 107->23, 108->20, 109->47, 110->43, 111->3, 112->14, 113->47, 114->33, 115->31, 116->12, 117->25, 118->35, 119->4, 120->3, 121->39, 122->9, 123->38, 124->12, 125->34, 126->35, 127->37, 128->43, 129->6, 130->8, 131->31, 132->45, 133->36, 134->1, 135->12, 136->30, 137->31, 138->21, 139->31, 140->30, 141->17, 142->17, 143->32, 144->41, 145->15, 146->38, 147->37, 148->45, 149->32, 150->8, 151->33, 152->2, 153->10, 154->1, 155->25, 156->1, 157->28, 158->34, 159->2, 160->32, 161->17, 162->17, 163->1, 164->14, 165->13, 166->40, 167->13, 168->14, 169->24, 170->25, 171->9, 172->44, 173->23, 174->12, 175->20, 176->19, 177->44, 178->42, 179->22, 180->2, 181->4, 182->34, 183->10, 184->24, 185->42, 186->16, 187->24, 188->28, 189->16, 190->44, 191->43, 192->43, 193->32, 194->29, 195->0, 196->11, 197->27, 198->28, 199->20, 200->2, 201->42, 202->19, 203->3, 204->7, 205->3, 206->36, 207->21, 208->8, 209->5, 210->2, 211->40, 212->21, 213->16, 214->29, 215->0, 216->32, 217->29, 218->46, 219->26, 220->13, 221->43, 222->17, 223->11, 224->16, 225->26, 226->38, 227->23, 228->44, 229->6, 230->16, 231->31, 232->42, 233->3, 234->19, 235->18, 236->35, 237->41, 238->19, 239->29, 240->38, 241->7, 242->20, 243->25, 244->40, 245->8, 246->44, 247->21, 248->36, 249->42, 250->21, 251->30, 252->10, 253->26, 254->45, 255->34, 256->10, 257->22, 258->20, 259->0, 260->30, 261->28, 262->25, 263->35, 264->7, 265->29, 266->11, 267->22, 268->40, 269->29, 270->41, 271->0, 272->15, 273->1, 274->45, 275->13, 276->44, 277->26, 278->0, 279->15, 280->30, 281->2, 282->42, 283->10, 284->39, 285->9, 286->38, 287->13

Average CPI map:
	1.73,	2.72,	2.21,	2.02,	1.51,	1.39
	2.07,	2.26,	1.61,	2.20,	2.94,	2.23
	3.03,	2.27,	1.91,	1.84,	1.24,	2.15
	2.00,	1.15,	2.05,	2.05,	1.32,	2.00
	2.01,	2.00,	1.55,	1.49,	1.99,	2.03
	3.20,	3.08,	1.98,	1.98,	1.98,	2.08
	2.09,	1.56,	2.01,	1.97,	2.89,	2.10
	2.28,	2.60,	3.86,	4.51,	2.88,	2.00

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	1,	1,	1,	1,	1
