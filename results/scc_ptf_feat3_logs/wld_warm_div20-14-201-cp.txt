==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 14 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 201

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.010/0.010
- branches (local/global): 34/34
- fails (local/global): 3/3
- lns time: 0.018000
- lns branches: 34
- lns fails: 3
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.030/0.048
- branches (local/global): 258/292
- fails (local/global): 107/110
- lns time: 0.038000
- lns branches: 258
- lns fails: 107
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 18386
- lns fails: 8915
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.010/2.074
- branches (local/global): 38/18716
- fails (local/global): 5/9030
- lns time: 0.017000
- lns branches: 38
- lns fails: 5
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.352/2.433
- branches (local/global): 3488/22204
- fails (local/global): 1659/10689
- lns time: 0.361000
- lns branches: 3488
- lns fails: 1659
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 30084
- lns fails: 14479
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.016/4.466
- branches (local/global): 33/52321
- fails (local/global): 3/25171
- lns time: 0.024000
- lns branches: 33
- lns fails: 3
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007000
- lns branches: 18346
- lns fails: 8868
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.422/6.903
- branches (local/global): 3383/74050
- fails (local/global): 1607/35646
- lns time: 0.430000
- lns branches: 3383
- lns fails: 1607
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.164/7.075
- branches (local/global): 1094/75144
- fails (local/global): 511/36157
- lns time: 0.173000
- lns branches: 1094
- lns fails: 511
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 32246
- lns fails: 15671
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 41940
- lns fails: 20417
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 23318
- lns fails: 11361
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 44729
- lns fails: 21613
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.271/15.387
- branches (local/global): 3817/221194
- fails (local/global): 1811/107030
- lns time: 0.279000
- lns branches: 3817
- lns fails: 1811
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.007000
- lns branches: 25807
- lns fails: 12545
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 26647
- lns fails: 13035
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.057/20.467
- branches (local/global): 16651/290299
- fails (local/global): 8012/140622
- lns time: 1.065000
- lns branches: 16651
- lns fails: 8012
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 0.900000
- lns branches: 5704
- lns fails: 2825
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.007000
- lns branches: 23004
- lns fails: 11165
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 23587
- lns fails: 11469
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.485/25.875
- branches (local/global): 7089/349683
- fails (local/global): 3402/169483
- lns time: 0.495000
- lns branches: 7089
- lns fails: 3402
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 51684
- lns fails: 25039
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 21150
- lns fails: 10349
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.050/29.951
- branches (local/global): 572/423089
- fails (local/global): 253/205124
- lns time: 0.059000
- lns branches: 572
- lns fails: 253
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 13550
- lns fails: 6719
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 45648
- lns fails: 22371
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 18268
- lns fails: 9046
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 47145
- lns fails: 22898
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007000
- lns branches: 26868
- lns fails: 13066
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 21739
- lns fails: 10715
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 27189
- lns fails: 13198
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 21372
- lns fails: 10399
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 26341
- lns fails: 12802
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007000
- lns branches: 37345
- lns fails: 18301
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 14635
- lns fails: 7158
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 27713
- lns fails: 13502
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007000
- lns branches: 26686
- lns fails: 12871
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 12103
- lns fails: 5955
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 65037
- lns fails: 31451
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 13958
- lns fails: 6827
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 44542
- lns fails: 21655
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 17718
- lns fails: 8638
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007000
- lns branches: 17492
- lns fails: 8601
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 16233
- lns fails: 7934
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 19683
- lns fails: 9630
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 39610
- lns fails: 19273
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 17323
- lns fails: 8459
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 13790
- lns fails: 6684
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 7933
- lns fails: 3880
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 15131
- lns fails: 7453
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 12390
- lns fails: 6039
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 13158
- lns fails: 6401
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 22849
- lns fails: 11175
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 1.815000
- lns branches: 9695
- lns fails: 4748

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 30
number of iterations: 55
total time: 90008.000000
total branches: 1136233
total fails: 552973

Final Solution: 0->43, 1->2, 2->27, 3->24, 4->14, 5->42, 6->45, 7->2, 8->11, 9->34, 10->45, 11->18, 12->14, 13->15, 14->35, 15->11, 16->19, 17->20, 18->35, 19->18, 20->26, 21->28, 22->9, 23->41, 24->24, 25->29, 26->12, 27->41, 28->42, 29->29, 30->10, 31->26, 32->12, 33->16, 34->38, 35->4, 36->7, 37->42, 38->37, 39->27, 40->11, 41->23, 42->30, 43->44, 44->35, 45->28, 46->33, 47->25, 48->33, 49->25, 50->6, 51->41, 52->38, 53->36, 54->0, 55->14, 56->44, 57->7, 58->42, 59->45, 60->2, 61->36, 62->15, 63->14, 64->37, 65->11, 66->39, 67->28, 68->21, 69->34, 70->11, 71->43, 72->29, 73->5, 74->20, 75->16, 76->47, 77->10, 78->1, 79->10, 80->36, 81->1, 82->35, 83->30, 84->4, 85->40, 86->31, 87->13, 88->17, 89->9, 90->41, 91->47, 92->22, 93->29, 94->0, 95->16, 96->27, 97->28, 98->36, 99->41, 100->29, 101->17, 102->31, 103->37, 104->38, 105->43, 106->37, 107->37, 108->47, 109->6, 110->39, 111->10, 112->43, 113->39, 114->2, 115->44, 116->36, 117->18, 118->0, 119->7, 120->37, 121->7, 122->4, 123->24, 124->12, 125->4, 126->19, 127->12, 128->8, 129->41, 130->38, 131->8, 132->31, 133->44, 134->14, 135->19, 136->26, 137->40, 138->32, 139->17, 140->35, 141->24, 142->34, 143->19, 144->15, 145->0, 146->3, 147->4, 148->23, 149->21, 150->3, 151->32, 152->30, 153->2, 154->21, 155->20, 156->40, 157->18, 158->26, 159->3, 160->14, 161->15, 162->19, 163->47, 164->7, 165->10, 166->22, 167->46, 168->46, 169->27, 170->6, 171->26, 172->26, 173->42, 174->12, 175->21, 176->20, 177->44, 178->1, 179->36, 180->22, 181->11, 182->5, 183->25, 184->18, 185->16, 186->43, 187->47, 188->5, 189->9, 190->38, 191->3, 192->18, 193->46, 194->34, 195->39, 196->25, 197->24, 198->30, 199->28, 200->46, 201->31, 202->17, 203->20, 204->3, 205->8, 206->32, 207->39, 208->33, 209->33, 210->8, 211->23, 212->1, 213->19, 214->16, 215->9, 216->45, 217->24, 218->34, 219->20, 220->6, 221->10, 222->43, 223->46, 224->39, 225->16, 226->3, 227->34, 228->1, 229->1, 230->23, 231->46, 232->30, 233->9, 234->17, 235->15, 236->7, 237->23, 238->6, 239->8, 240->29, 241->15, 242->21, 243->33, 244->30, 245->40, 246->32, 247->31, 248->13, 249->6, 250->32, 251->0, 252->13, 253->28, 254->17, 255->12, 256->4, 257->22, 258->23, 259->5, 260->9, 261->38, 262->5, 263->8, 264->27, 265->35, 266->42, 267->25, 268->45, 269->0, 270->2, 271->47, 272->45, 273->40, 274->5, 275->22, 276->31, 277->44, 278->13, 279->27, 280->25, 281->13, 282->13, 283->21, 284->33, 285->40, 286->32, 287->22

Average CPI map:
	1.63,	2.81,	1.65,	2.10,	2.03,	2.10
	2.28,	2.61,	5.58,	3.74,	2.63,	1.93
	2.03,	11.93,	1.66,	3.28,	3.24,	2.04
	2.31,	1.00,	1.30,	2.90,	2.85,	3.05
	2.02,	2.71,	1.16,	2.57,	2.44,	3.71
	2.48,	2.30,	10.35,	10.56,	1.42,	2.24
	2.10,	2.02,	1.43,	1.64,	5.76,	2.23
	2.23,	2.69,	4.33,	2.18,	2.58,	2.17

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	1,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	1,	1,	0,	1
	1,	0,	0,	0,	1,	1
	1,	1,	1,	0,	1,	1
