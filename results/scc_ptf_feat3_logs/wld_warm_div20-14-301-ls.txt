LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
2.55359, 2.36117, 0.573532, 2.29872, 2.64543, 1.56757, 1.64625, 1.52824, 1.43803, 0.5, 3.64814, 3.23935, 2.51107, 2.40504, 2.72906, 2.62251, 0.5, 1.23232, 2.20879, 1.20027, 0.5, 5.53224, 0.994675, 1.56401, 1.61236, 0.5, 4.21228, 2.68281, 0.831039, 2.16151, 4.10149, 1.69636, 1.32855, 1.90991, 2.46369, 0.5, 2.06286, 2.17432, 2.44801, 2.31855, 1.94914, 1.04712, 0.5, 3.40758, 0.692599, 0.581298, 3.95491, 2.86361, 3.40809, 0.55863, 0.5, 4.06533, 0.728905, 2.73904, 1.09989, 0.5, 3.11603, 4.30389, 2.12124, 0.858948, 1.79955, 2.07464, 1.89003, 1.92703, 2.08784, 2.2209, 0.91615, 2.83479, 2.3763, 0.840966, 2.02627, 3.00552, 0.930088, 4.13549, 0.907767, 3.06438, 0.894031, 2.06824, 0.5, 3.39243, 2.32285, 0.984231, 4.27939, 0.521107, 1.92992, 2.6927, 0.606388, 3.40636, 2.86463, 0.5, 0.769231, 2.15511, 2.37213, 2.15005, 2.14917, 2.40431, 2.008, 2.01354, 1.8307, 2.11466, 1.93379, 2.09931, 2.78209, 2.86996, 0.624814, 2.52425, 2.69888, 0.5, 2.60002, 0.5, 2.55397, 2.20386, 2.44118, 1.70097, 2.19957, 3.88385, 0.909231, 0.61331, 1.61421, 2.77983, 1.52734, 2.40316, 2.38423, 1.9802, 1.84871, 1.85637, 1.69252, 0.5, 2.40423, 2.1761, 1.91143, 3.31573, 1.42531, 4.22734, 0.962699, 0.5, 0.680057, 4.20459, 3.36067, 3.62207, 2.07557, 0.698655, 0.5, 1.74304, 1.10969, 0.896667, 2.3006, 2.96707, 1.81721, 2.90877, 2.8252, 3.51572, 1.32983, 1.039, 1.93841, 1.35184, 3.35032, 2.99665, 2.84917, 0.87015, 1.43371, 0.5, 1.04984, 2.15255, 2.28473, 2.20354, 1.94725, 2.36208, 3.17786, 2.84797, 0.703695, 0.695322, 0.564924, 4.01022, 1.0335, 1.27125, 0.589603, 4.29137, 2.09966, 2.71461, 1.30416, 1.33529, 2.18506, 1.33513, 5.34037, 0.5, 2.64882, 3.53865, 0.5, 3.29267, 0.756623, 1.26323, 0.5, 2.27668, 1.685, 1.8148, 2.35731, 3.36622, 2.52334, 2.31358, 2.47449, 2.05636, 0.951763, 1.68047, 4.83694, 8.8023, 13.8034, 0.83421, 27.4394, 4.28373, 13.8001, 12.8991, 9.04167, 0.5, 10.4205, 13.3387, 2.16182, 2.17339, 2.40504, 2.0239, 1.43849, 1.79737, 2.93771, 2.80105, 2.00619, 1.16265, 0.5, 2.59241, 2.07594, 2.18482, 1.40789, 2.3826, 2.13165, 1.8171, 1.50139, 3.08218, 1.8065, 0.603463, 2.5178, 2.48866, 14.6043, 10.687, 0.5, 14.1735, 7.84635, 12.1889, 17.4451, 4.65895, 16.1089, 7.99628, 11.6184, 2.17231, 3.15289, 1.3518, 1.20014, 3.24318, 2.55199, 0.5, 0.5, 1.5547, 2.47587, 2.08944, 2.69116, 2.68883, 1.34601, 1.47435, 2.67901, 1.99912, 2.6342, 1.86732, 0.953892, 1.68086, 2.14936, 2.77665, 1.54645, 2.89279, 2.29255, 7.04134, 20.331, 6.35332, 7.11756, 16.8643, 11.703, 8.39222, 10.1196, 9.35711, 12.3621, 8.06594
cpi min/max: 0.5/27.4394
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55157, operands = 136540
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 301, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 16702 itr]: obj = (23), mov = 16702, inf = 69.1%, acc = 22.9%, imp = 156

number of satisfied efficiency constraints: 23
[2 sec, 48432 itr]: obj = (26), mov = 48432, inf = 75.9%, acc = 16.3%, imp = 159

number of satisfied efficiency constraints: 26
[3 sec, 78807 itr]: obj = (26), mov = 78807, inf = 75.1%, acc = 15.9%, imp = 159

number of satisfied efficiency constraints: 26
[4 sec, 102247 itr]: obj = (26), mov = 102247, inf = 72.2%, acc = 17.1%, imp = 159

number of satisfied efficiency constraints: 26
[5 sec, 120317 itr]: obj = (27), mov = 120317, inf = 68.9%, acc = 18.7%, imp = 160

number of satisfied efficiency constraints: 27
[6 sec, 136083 itr]: obj = (28), mov = 136083, inf = 65.5%, acc = 20.7%, imp = 179

number of satisfied efficiency constraints: 28
[7 sec, 151016 itr]: obj = (28), mov = 151016, inf = 61.8%, acc = 23.5%, imp = 179

number of satisfied efficiency constraints: 28
[8 sec, 167571 itr]: obj = (28), mov = 167571, inf = 58.1%, acc = 25.6%, imp = 181

number of satisfied efficiency constraints: 28
[9 sec, 185757 itr]: obj = (28), mov = 185757, inf = 54.5%, acc = 27.2%, imp = 182

number of satisfied efficiency constraints: 28
[10 sec, 204595 itr]: obj = (28), mov = 204595, inf = 51.6%, acc = 28.3%, imp = 182

number of satisfied efficiency constraints: 28
[11 sec, 223683 itr]: obj = (28), mov = 223683, inf = 49.1%, acc = 29.2%, imp = 182

number of satisfied efficiency constraints: 28
[12 sec, 241729 itr]: obj = (28), mov = 241729, inf = 47.1%, acc = 30%, imp = 182

number of satisfied efficiency constraints: 28
[13 sec, 260887 itr]: obj = (28), mov = 260887, inf = 45.3%, acc = 30.6%, imp = 183

number of satisfied efficiency constraints: 28
[14 sec, 281046 itr]: obj = (28), mov = 281046, inf = 43.7%, acc = 31%, imp = 183

number of satisfied efficiency constraints: 28
[15 sec, 301387 itr]: obj = (28), mov = 301387, inf = 42.3%, acc = 31.3%, imp = 183

number of satisfied efficiency constraints: 28
[16 sec, 321304 itr]: obj = (28), mov = 321304, inf = 41.1%, acc = 31.6%, imp = 183

number of satisfied efficiency constraints: 28
[17 sec, 341456 itr]: obj = (28), mov = 341456, inf = 40%, acc = 31.8%, imp = 183

number of satisfied efficiency constraints: 28
[18 sec, 361579 itr]: obj = (28), mov = 361579, inf = 39%, acc = 32%, imp = 183

number of satisfied efficiency constraints: 28
[19 sec, 381452 itr]: obj = (28), mov = 381452, inf = 38.2%, acc = 32.3%, imp = 183

number of satisfied efficiency constraints: 28
[20 sec, 401048 itr]: obj = (28), mov = 401048, inf = 37.4%, acc = 32.5%, imp = 183

number of satisfied efficiency constraints: 28
[21 sec, 421104 itr]: obj = (28), mov = 421104, inf = 36.7%, acc = 32.7%, imp = 183

number of satisfied efficiency constraints: 28
[22 sec, 441086 itr]: obj = (28), mov = 441086, inf = 36.1%, acc = 32.8%, imp = 183

number of satisfied efficiency constraints: 28
[23 sec, 460252 itr]: obj = (28), mov = 460252, inf = 35.5%, acc = 33%, imp = 183

number of satisfied efficiency constraints: 28
[24 sec, 480190 itr]: obj = (28), mov = 480190, inf = 35%, acc = 33.1%, imp = 183

number of satisfied efficiency constraints: 28
[25 sec, 500559 itr]: obj = (28), mov = 500559, inf = 34.5%, acc = 33.2%, imp = 183

number of satisfied efficiency constraints: 28
[26 sec, 519499 itr]: obj = (28), mov = 519499, inf = 34%, acc = 33.3%, imp = 183

number of satisfied efficiency constraints: 28
[27 sec, 538575 itr]: obj = (28), mov = 538575, inf = 33.6%, acc = 33.4%, imp = 183

number of satisfied efficiency constraints: 28
[28 sec, 557923 itr]: obj = (28), mov = 557923, inf = 33.3%, acc = 33.6%, imp = 219

number of satisfied efficiency constraints: 28
[29 sec, 574630 itr]: obj = (28), mov = 574630, inf = 32.9%, acc = 33.8%, imp = 225

number of satisfied efficiency constraints: 28
[30 sec, 591835 itr]: obj = (28), mov = 591835, inf = 32.4%, acc = 34%, imp = 225

number of satisfied efficiency constraints: 28
[31 sec, 609421 itr]: obj = (28), mov = 609421, inf = 32%, acc = 34.2%, imp = 225

number of satisfied efficiency constraints: 28
[32 sec, 626704 itr]: obj = (28), mov = 626704, inf = 31.6%, acc = 34.4%, imp = 225

number of satisfied efficiency constraints: 28
[33 sec, 644106 itr]: obj = (28), mov = 644106, inf = 31.3%, acc = 34.6%, imp = 225

number of satisfied efficiency constraints: 28
[34 sec, 661473 itr]: obj = (28), mov = 661473, inf = 30.9%, acc = 34.8%, imp = 225

number of satisfied efficiency constraints: 28
[35 sec, 677021 itr]: obj = (28), mov = 677021, inf = 30.6%, acc = 35.1%, imp = 251

number of satisfied efficiency constraints: 28
[36 sec, 691479 itr]: obj = (28), mov = 691479, inf = 30.3%, acc = 35.4%, imp = 254

number of satisfied efficiency constraints: 28
[37 sec, 707518 itr]: obj = (28), mov = 707518, inf = 30%, acc = 35.6%, imp = 254

number of satisfied efficiency constraints: 28
[38 sec, 723697 itr]: obj = (28), mov = 723697, inf = 29.7%, acc = 35.8%, imp = 254

number of satisfied efficiency constraints: 28
[39 sec, 740093 itr]: obj = (28), mov = 740093, inf = 29.5%, acc = 36%, imp = 254

number of satisfied efficiency constraints: 28
[40 sec, 756526 itr]: obj = (28), mov = 756526, inf = 29.2%, acc = 36.2%, imp = 255

number of satisfied efficiency constraints: 28
[41 sec, 773768 itr]: obj = (28), mov = 773768, inf = 28.9%, acc = 36.3%, imp = 255

number of satisfied efficiency constraints: 28
[42 sec, 791362 itr]: obj = (28), mov = 791362, inf = 28.6%, acc = 36.4%, imp = 256

number of satisfied efficiency constraints: 28
[43 sec, 810219 itr]: obj = (28), mov = 810219, inf = 28.4%, acc = 36.4%, imp = 256

number of satisfied efficiency constraints: 28
[44 sec, 829238 itr]: obj = (28), mov = 829238, inf = 28.1%, acc = 36.4%, imp = 256

number of satisfied efficiency constraints: 28
[45 sec, 847699 itr]: obj = (28), mov = 847699, inf = 27.9%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[46 sec, 866467 itr]: obj = (28), mov = 866467, inf = 27.7%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[47 sec, 884268 itr]: obj = (28), mov = 884268, inf = 27.5%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[48 sec, 902813 itr]: obj = (28), mov = 902813, inf = 27.2%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[49 sec, 921504 itr]: obj = (28), mov = 921504, inf = 27%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[50 sec, 940180 itr]: obj = (28), mov = 940180, inf = 26.8%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[51 sec, 958719 itr]: obj = (28), mov = 958719, inf = 26.6%, acc = 36.6%, imp = 256

number of satisfied efficiency constraints: 28
[52 sec, 977937 itr]: obj = (28), mov = 977937, inf = 26.5%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[53 sec, 996857 itr]: obj = (28), mov = 996857, inf = 26.3%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[54 sec, 1015448 itr]: obj = (28), mov = 1015448, inf = 26.1%, acc = 36.5%, imp = 256

number of satisfied efficiency constraints: 28
[55 sec, 1033387 itr]: obj = (28), mov = 1033387, inf = 26%, acc = 36.6%, imp = 280

number of satisfied efficiency constraints: 28
[56 sec, 1047309 itr]: obj = (28), mov = 1047309, inf = 25.8%, acc = 36.8%, imp = 283

number of satisfied efficiency constraints: 28
[57 sec, 1062597 itr]: obj = (28), mov = 1062597, inf = 25.7%, acc = 37%, imp = 284

number of satisfied efficiency constraints: 28
[58 sec, 1079033 itr]: obj = (28), mov = 1079033, inf = 25.5%, acc = 37.1%, imp = 285

number of satisfied efficiency constraints: 28
[59 sec, 1095940 itr]: obj = (28), mov = 1095940, inf = 25.4%, acc = 37.2%, imp = 285

number of satisfied efficiency constraints: 28
[60 sec, 1112312 itr]: obj = (28), mov = 1112312, inf = 25.2%, acc = 37.3%, imp = 285

number of satisfied efficiency constraints: 28
[61 sec, 1129300 itr]: obj = (28), mov = 1129300, inf = 25.1%, acc = 37.3%, imp = 285

number of satisfied efficiency constraints: 28
[62 sec, 1145862 itr]: obj = (28), mov = 1145862, inf = 25%, acc = 37.4%, imp = 285

number of satisfied efficiency constraints: 28
[63 sec, 1162068 itr]: obj = (28), mov = 1162068, inf = 24.8%, acc = 37.5%, imp = 297

number of satisfied efficiency constraints: 28
[64 sec, 1176132 itr]: obj = (28), mov = 1176132, inf = 24.7%, acc = 37.7%, imp = 312

number of satisfied efficiency constraints: 28
[65 sec, 1190831 itr]: obj = (28), mov = 1190831, inf = 24.6%, acc = 37.9%, imp = 312

number of satisfied efficiency constraints: 28
[66 sec, 1205201 itr]: obj = (28), mov = 1205201, inf = 24.5%, acc = 38%, imp = 312

number of satisfied efficiency constraints: 28
[67 sec, 1220178 itr]: obj = (28), mov = 1220178, inf = 24.4%, acc = 38.1%, imp = 312

number of satisfied efficiency constraints: 28
[68 sec, 1235421 itr]: obj = (28), mov = 1235421, inf = 24.3%, acc = 38.3%, imp = 312

number of satisfied efficiency constraints: 28
[69 sec, 1250224 itr]: obj = (28), mov = 1250224, inf = 24.2%, acc = 38.4%, imp = 312

number of satisfied efficiency constraints: 28
[70 sec, 1264744 itr]: obj = (28), mov = 1264744, inf = 24.1%, acc = 38.5%, imp = 313

number of satisfied efficiency constraints: 28
[71 sec, 1281025 itr]: obj = (28), mov = 1281025, inf = 24%, acc = 38.6%, imp = 313

number of satisfied efficiency constraints: 28
[72 sec, 1297250 itr]: obj = (28), mov = 1297250, inf = 23.9%, acc = 38.7%, imp = 313

number of satisfied efficiency constraints: 28
[73 sec, 1313036 itr]: obj = (28), mov = 1313036, inf = 23.8%, acc = 38.8%, imp = 313

number of satisfied efficiency constraints: 28
[74 sec, 1329769 itr]: obj = (28), mov = 1329769, inf = 23.8%, acc = 38.8%, imp = 313

number of satisfied efficiency constraints: 28
[75 sec, 1345823 itr]: obj = (28), mov = 1345823, inf = 23.7%, acc = 38.9%, imp = 314

number of satisfied efficiency constraints: 28
[76 sec, 1362670 itr]: obj = (28), mov = 1362670, inf = 23.6%, acc = 38.9%, imp = 314

number of satisfied efficiency constraints: 28
[77 sec, 1379509 itr]: obj = (28), mov = 1379509, inf = 23.5%, acc = 38.9%, imp = 314

number of satisfied efficiency constraints: 28
[78 sec, 1396125 itr]: obj = (28), mov = 1396125, inf = 23.4%, acc = 38.9%, imp = 314

number of satisfied efficiency constraints: 28
[79 sec, 1413118 itr]: obj = (28), mov = 1413118, inf = 23.3%, acc = 39%, imp = 314

number of satisfied efficiency constraints: 28
[80 sec, 1430598 itr]: obj = (28), mov = 1430598, inf = 23.2%, acc = 39%, imp = 314

number of satisfied efficiency constraints: 28
[81 sec, 1448169 itr]: obj = (28), mov = 1448169, inf = 23.1%, acc = 39%, imp = 314

number of satisfied efficiency constraints: 28
[82 sec, 1465574 itr]: obj = (28), mov = 1465574, inf = 23.1%, acc = 39%, imp = 314

number of satisfied efficiency constraints: 28
[83 sec, 1482337 itr]: obj = (28), mov = 1482337, inf = 23%, acc = 39.1%, imp = 314

number of satisfied efficiency constraints: 28
[84 sec, 1499511 itr]: obj = (28), mov = 1499511, inf = 22.9%, acc = 39.1%, imp = 314

number of satisfied efficiency constraints: 28
[85 sec, 1516618 itr]: obj = (28), mov = 1516618, inf = 22.8%, acc = 39.1%, imp = 314

number of satisfied efficiency constraints: 28
[86 sec, 1533799 itr]: obj = (28), mov = 1533799, inf = 22.8%, acc = 39.1%, imp = 314

number of satisfied efficiency constraints: 28
[87 sec, 1550840 itr]: obj = (28), mov = 1550840, inf = 22.7%, acc = 39.2%, imp = 314

number of satisfied efficiency constraints: 28
[88 sec, 1567988 itr]: obj = (28), mov = 1567988, inf = 22.6%, acc = 39.2%, imp = 314

number of satisfied efficiency constraints: 28
[89 sec, 1584169 itr]: obj = (28), mov = 1584169, inf = 22.5%, acc = 39.2%, imp = 314

number of satisfied efficiency constraints: 28
[90 sec, 1601277 itr]: obj = (28), mov = 1601277, inf = 22.5%, acc = 39.3%, imp = 314

number of satisfied efficiency constraints: 28
[90 sec, 1601277 itr]: obj = (28), mov = 1601277, inf = 22.5%, acc = 39.3%, imp = 314

number of satisfied efficiency constraints: 28
1601277 iterations, 1601277 moves performed in 90 seconds
Feasible solution: obj = (28)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 28
Mapping: 0->13, 1->45, 2->30, 3->17, 4->40, 5->44, 6->41, 7->21, 8->16, 9->4, 10->34, 11->40, 12->36, 13->41, 14->8, 15->24, 16->0, 17->13, 18->24, 19->24, 20->2, 21->19, 22->19, 23->37, 24->33, 25->38, 26->4, 27->31, 28->28, 29->23, 30->8, 31->20, 32->18, 33->3, 34->8, 35->25, 36->1, 37->21, 38->41, 39->15, 40->29, 41->35, 42->38, 43->11, 44->3, 45->20, 46->42, 47->18, 48->32, 49->14, 50->44, 51->34, 52->22, 53->25, 54->37, 55->37, 56->47, 57->24, 58->15, 59->17, 60->21, 61->17, 62->32, 63->0, 64->35, 65->26, 66->23, 67->21, 68->33, 69->18, 70->29, 71->7, 72->47, 73->12, 74->35, 75->20, 76->23, 77->22, 78->0, 79->43, 80->32, 81->6, 82->18, 83->6, 84->17, 85->35, 86->28, 87->43, 88->0, 89->27, 90->31, 91->19, 92->46, 93->42, 94->10, 95->31, 96->14, 97->16, 98->28, 99->42, 100->9, 101->31, 102->15, 103->46, 104->1, 105->12, 106->36, 107->30, 108->25, 109->26, 110->16, 111->22, 112->44, 113->39, 114->10, 115->10, 116->14, 117->45, 118->29, 119->19, 120->13, 121->43, 122->47, 123->18, 124->26, 125->33, 126->22, 127->27, 128->3, 129->6, 130->38, 131->3, 132->1, 133->36, 134->13, 135->47, 136->26, 137->0, 138->34, 139->47, 140->11, 141->16, 142->25, 143->26, 144->5, 145->5, 146->6, 147->33, 148->5, 149->36, 150->12, 151->34, 152->31, 153->32, 154->10, 155->22, 156->32, 157->9, 158->33, 159->2, 160->39, 161->20, 162->42, 163->24, 164->8, 165->8, 166->25, 167->13, 168->5, 169->2, 170->5, 171->45, 172->23, 173->14, 174->15, 175->4, 176->11, 177->20, 178->5, 179->3, 180->39, 181->4, 182->31, 183->45, 184->46, 185->20, 186->0, 187->30, 188->39, 189->4, 190->42, 191->27, 192->23, 193->46, 194->13, 195->12, 196->41, 197->37, 198->46, 199->22, 200->30, 201->37, 202->21, 203->7, 204->40, 205->7, 206->9, 207->38, 208->17, 209->34, 210->6, 211->1, 212->9, 213->14, 214->29, 215->2, 216->2, 217->10, 218->45, 219->6, 220->39, 221->1, 222->30, 223->3, 224->26, 225->11, 226->32, 227->28, 228->19, 229->15, 230->15, 231->11, 232->29, 233->4, 234->19, 235->43, 236->44, 237->41, 238->28, 239->27, 240->44, 241->11, 242->27, 243->7, 244->16, 245->43, 246->9, 247->41, 248->24, 249->35, 250->7, 251->36, 252->40, 253->36, 254->25, 255->38, 256->17, 257->21, 258->38, 259->47, 260->46, 261->1, 262->18, 263->16, 264->12, 265->27, 266->45, 267->28, 268->40, 269->14, 270->2, 271->44, 272->35, 273->8, 274->29, 275->30, 276->37, 277->23, 278->42, 279->7, 280->34, 281->9, 282->43, 283->33, 284->12, 285->40, 286->10, 287->39
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	2.10751	3.48315	3.44542	2.30635	2.07143	1.63413
	3.63434	7.60558	2.75986	10.3475	4.11775	3.38416
	3.79423	1.7205	1.64223	1.97468	2.87323	6.19227
	2.33064	2.50653	1.77223	1.63144	1.72649	2.01299
	4.76616	1.58108	1.49982	1.12104	1.72958	3.28137
	2.15286	1.91176	2.08505	3.34225	4.33186	2.81351
	2.64503	1.81317	1.24814	2.40721	4.31099	2.35317
	5.05951	6.02933	3.76674	1.6815	2.97639	2.01785
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	0	0	0	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 14 --global-tlim 90 --seed 301

Starting the Optimization Process
