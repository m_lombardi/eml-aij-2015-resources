==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 3 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 401

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.008000
- lns branches: 21986
- lns fails: 10628
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.040/2.048
- branches (local/global): 260/22246
- fails (local/global): 104/10732
- lns time: 0.049000
- lns branches: 260
- lns fails: 104
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 2.008000
- lns branches: 19076
- lns fails: 9232
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.016/4.081
- branches (local/global): 49/41371
- fails (local/global): 12/19976
- lns time: 0.024000
- lns branches: 49
- lns fails: 12
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.055/4.144
- branches (local/global): 236/41607
- fails (local/global): 100/20076
- lns time: 0.064000
- lns branches: 236
- lns fails: 100
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.016/4.169
- branches (local/global): 33/41640
- fails (local/global): 4/20080
- lns time: 0.024000
- lns branches: 33
- lns fails: 4
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008000
- lns branches: 17825
- lns fails: 8619
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.009000
- lns branches: 26558
- lns fails: 12815
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.008000
- lns branches: 7247
- lns fails: 3521
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.027/10.229
- branches (local/global): 260/93530
- fails (local/global): 107/45142
- lns time: 0.034000
- lns branches: 260
- lns fails: 107
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.056/10.292
- branches (local/global): 1119/94649
- fails (local/global): 500/45642
- lns time: 0.064000
- lns branches: 1119
- lns fails: 500
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.009000
- lns branches: 24186
- lns fails: 11701
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.433/12.742
- branches (local/global): 7160/125995
- fails (local/global): 3408/60751
- lns time: 0.442000
- lns branches: 7160
- lns fails: 3408
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.008000
- lns branches: 15069
- lns fails: 7434
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.008000
- lns branches: 17568
- lns fails: 8573
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.009000
- lns branches: 14302
- lns fails: 6919
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.452/19.228
- branches (local/global): 7721/180655
- fails (local/global): 3721/87398
- lns time: 0.460000
- lns branches: 7721
- lns fails: 3721
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008000
- lns branches: 79989
- lns fails: 38977
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.011/21.255
- branches (local/global): 277/260921
- fails (local/global): 116/126491
- lns time: 0.019000
- lns branches: 277
- lns fails: 116
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.009000
- lns branches: 18242
- lns fails: 8968
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007000
- lns branches: 16496
- lns fails: 8170
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008000
- lns branches: 9800
- lns fails: 4821
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.061/27.348
- branches (local/global): 6983/312442
- fails (local/global): 3302/151752
- lns time: 0.069000
- lns branches: 6983
- lns fails: 3302
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009000
- lns branches: 5618
- lns fails: 2768
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.011000
- lns branches: 24632
- lns fails: 12041
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.495/31.871
- branches (local/global): 9167/351859
- fails (local/global): 4411/170972
- lns time: 0.506000
- lns branches: 9167
- lns fails: 4411
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 15048
- lns fails: 7442
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 16622
- lns fails: 8066
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 16473
- lns fails: 8021
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007000
- lns branches: 31044
- lns fails: 15110
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 14554
- lns fails: 7115
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.013000
- lns branches: 7193
- lns fails: 3525
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 9251
- lns fails: 4486
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 38372
- lns fails: 18604
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 13208
- lns fails: 6394
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.008/49.969
- branches (local/global): 33/513657
- fails (local/global): 2/249737
- lns time: 0.016000
- lns branches: 33
- lns fails: 2
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 17931
- lns fails: 8723
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 18481
- lns fails: 8980
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 30469
- lns fails: 14969
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007000
- lns branches: 22431
- lns fails: 11035
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 21945
- lns fails: 10637
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 25260
- lns fails: 12204
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 23859
- lns fails: 11697
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.009000
- lns branches: 12411
- lns fails: 6112
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.041/66.084
- branches (local/global): 555/686999
- fails (local/global): 252/334346
- lns time: 0.049000
- lns branches: 555
- lns fails: 252
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.009000
- lns branches: 16701
- lns fails: 8101
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 13447
- lns fails: 6637
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.009000
- lns branches: 6346
- lns fails: 3114
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 15361
- lns fails: 7527
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 7961
- lns fails: 3860
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 15928
- lns fails: 7747
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 13199
- lns fails: 6536
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.007000
- lns branches: 25345
- lns fails: 12367
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 15289
- lns fails: 7468
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 21585
- lns fails: 10428
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.097/86.268
- branches (local/global): 1766/839927
- fails (local/global): 804/408935
- lns time: 0.106000
- lns branches: 1766
- lns fails: 804
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.010000
- lns branches: 7241
- lns fails: 3496
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 1.721000
- lns branches: 6662
- lns fails: 3273

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 12
final number of satisfied efficiency constraints: 26
number of iterations: 58
total time: 90008.000000
total branches: 853830
total fails: 415704

Final Solution: 0->8, 1->16, 2->15, 3->22, 4->43, 5->31, 6->20, 7->3, 8->7, 9->41, 10->25, 11->42, 12->26, 13->8, 14->34, 15->11, 16->24, 17->0, 18->11, 19->44, 20->2, 21->27, 22->22, 23->44, 24->15, 25->33, 26->36, 27->30, 28->13, 29->18, 30->32, 31->18, 32->0, 33->13, 34->16, 35->22, 36->6, 37->35, 38->47, 39->32, 40->45, 41->42, 42->11, 43->11, 44->43, 45->32, 46->15, 47->23, 48->19, 49->4, 50->3, 51->47, 52->40, 53->3, 54->38, 55->13, 56->16, 57->5, 58->41, 59->26, 60->24, 61->22, 62->34, 63->25, 64->7, 65->39, 66->34, 67->46, 68->45, 69->12, 70->35, 71->15, 72->21, 73->12, 74->7, 75->23, 76->47, 77->16, 78->11, 79->14, 80->30, 81->0, 82->45, 83->27, 84->23, 85->26, 86->31, 87->24, 88->8, 89->37, 90->7, 91->7, 92->2, 93->30, 94->9, 95->9, 96->32, 97->3, 98->30, 99->33, 100->29, 101->47, 102->39, 103->22, 104->20, 105->28, 106->1, 107->27, 108->39, 109->18, 110->15, 111->28, 112->46, 113->37, 114->10, 115->42, 116->14, 117->29, 118->6, 119->31, 120->40, 121->37, 122->39, 123->2, 124->27, 125->45, 126->31, 127->18, 128->16, 129->40, 130->43, 131->35, 132->37, 133->28, 134->46, 135->1, 136->21, 137->15, 138->7, 139->8, 140->22, 141->25, 142->23, 143->18, 144->13, 145->30, 146->40, 147->0, 148->14, 149->29, 150->38, 151->14, 152->4, 153->0, 154->5, 155->42, 156->2, 157->27, 158->19, 159->19, 160->12, 161->12, 162->40, 163->42, 164->3, 165->20, 166->41, 167->4, 168->38, 169->31, 170->17, 171->31, 172->1, 173->25, 174->30, 175->33, 176->45, 177->42, 178->37, 179->24, 180->45, 181->32, 182->24, 183->28, 184->1, 185->10, 186->26, 187->19, 188->27, 189->17, 190->38, 191->29, 192->1, 193->32, 194->4, 195->39, 196->39, 197->21, 198->6, 199->20, 200->5, 201->41, 202->16, 203->19, 204->23, 205->9, 206->35, 207->20, 208->14, 209->6, 210->10, 211->21, 212->4, 213->38, 214->34, 215->33, 216->28, 217->2, 218->2, 219->35, 220->44, 221->33, 222->10, 223->1, 224->46, 225->17, 226->40, 227->5, 228->35, 229->6, 230->33, 231->10, 232->23, 233->0, 234->34, 235->46, 236->9, 237->36, 238->8, 239->9, 240->44, 241->5, 242->29, 243->17, 244->9, 245->11, 246->43, 247->20, 248->6, 249->8, 250->36, 251->36, 252->25, 253->29, 254->18, 255->26, 256->13, 257->41, 258->4, 259->25, 260->3, 261->46, 262->41, 263->13, 264->21, 265->37, 266->17, 267->14, 268->24, 269->26, 270->17, 271->34, 272->38, 273->47, 274->44, 275->21, 276->12, 277->43, 278->5, 279->36, 280->43, 281->44, 282->19, 283->28, 284->10, 285->47, 286->12, 287->36

Average CPI map:
	1.10,	2.14,	3.35,	2.10,	1.62,	1.82
	2.13,	10.33,	2.28,	10.60,	3.24,	2.02
	2.00,	2.06,	2.06,	0.68,	1.40,	2.08
	2.18,	2.05,	2.04,	2.04,	1.77,	2.03
	2.00,	2.01,	1.99,	1.06,	1.99,	2.03
	2.84,	1.09,	1.99,	1.99,	2.07,	2.27
	2.20,	1.39,	1.98,	1.57,	3.24,	2.03
	2.02,	2.49,	1.98,	3.85,	2.81,	1.99

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	0,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	1,	1,	0
