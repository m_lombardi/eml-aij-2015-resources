LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
0.5, 0.793797, 3.01448, 2.89409, 2.97166, 1.82598, 1.46422, 1.02987, 4.33222, 3.15438, 0.5, 1.51931, 2.09408, 1.95425, 1.74965, 2.13165, 2.02029, 2.05007, 2.40715, 0.751014, 2.93856, 1.5686, 1.81978, 2.51489, 0.810358, 3.37159, 2.01811, 0.5, 3.8527, 1.44724, 1.43318, 2.33024, 1.63724, 1.9352, 2.17271, 2.49144, 2.62915, 2.43595, 1.70695, 1.6611, 2.1134, 1.45345, 2.13604, 2.94479, 1.10006, 2.21292, 2.90612, 0.700066, 2.24627, 2.07499, 0.712951, 3.80092, 1.44741, 1.71746, 1.98396, 2.25489, 2.0251, 1.72142, 1.96386, 2.05077, 0.724777, 5.76477, 1.8992, 2.38764, 0.513167, 0.710449, 31.2107, 27.2561, 18.7677, 35, 11.0826, 8.68286, 1.51957, 3.17254, 0.5, 1.02075, 1.81243, 3.9747, 1.72133, 0.667203, 2.55119, 2.14292, 2.30777, 2.60958, 0.631906, 2.19898, 4.19696, 1.52562, 0.851563, 2.59497, 3.36286, 3.496, 0.975857, 1.57842, 2.08687, 0.5, 28.3048, 15.5874, 35, 25.0792, 12.9663, 15.0623, 1.94871, 2.30428, 2.28502, 1.8023, 1.39475, 2.26493, 3.11261, 1.48502, 2.7242, 1.50319, 2.67498, 0.5, 0.94498, 1.14016, 2.84732, 3.11272, 2.697, 1.25782, 1.49547, 2.60328, 2.37214, 1.91276, 1.0462, 2.57016, 0.556693, 2.96248, 3.93222, 0.5, 1.54412, 2.50448, 20.8662, 35, 16.4223, 16.63, 24.1447, 18.937, 35, 18.8404, 13.9087, 21.4826, 32.0696, 10.6987, 2.03447, 3.84498, 3.47505, 0.840083, 1.07748, 0.727933, 3.5496, 3.73055, 1.03076, 0.5, 2.33398, 0.855113, 1.93585, 0.915647, 0.553338, 1.61452, 1.35227, 5.62837, 3.13578, 3.41479, 0.603717, 1.40872, 0.5, 2.93699, 1.64791, 2.04324, 0.971774, 0.682902, 1.77925, 4.87493, 0.679022, 1.01475, 0.78205, 2.11252, 0.933492, 6.47816, 1.34254, 1.35093, 0.5, 3.04181, 3.12938, 2.63533, 0.5, 1.80789, 4.25635, 2.83402, 0.859458, 1.74228, 2.29634, 1.85749, 1.87562, 2.42096, 1.42836, 2.12123, 3.7915, 2.21285, 0.575129, 0.780089, 4.14043, 0.5, 0.5, 0.855756, 1.6246, 2.73087, 3.7941, 2.49466, 3.98145, 0.5, 1.82333, 1.23156, 2.84167, 1.62198, 4.5138, 2.24199, 1.19583, 2.07482, 0.739494, 1.23407, 1.85496, 0.557577, 2.51568, 1.41475, 4.12757, 1.52947, 1.92157, 2.25752, 1.90951, 2.10541, 1.70878, 2.09721, 0.957288, 0.997952, 2.95043, 3.22083, 3.20122, 0.672285, 3.6616, 3.14231, 2.4321, 1.26004, 0.5, 1.00395, 0.996237, 3.61131, 0.889852, 2.02482, 2.91463, 1.56314, 0.5, 2.30284, 5.09568, 1.76483, 0.697285, 1.63936, 2.35738, 1.92499, 1.23851, 2.49156, 2.46238, 1.52517, 2.20721, 2.20715, 1.75975, 2.13094, 1.83949, 1.85546, 2.53771, 1.97268, 2.60817, 0.5, 2.55362, 1.82783, 0.623567, 3.18571, 0.742433, 2.03774, 0.739147, 4.6714, 1.30779, 2.98554, 3.00575, 2.50208, 1.69884, 0.5
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54812, operands = 135836
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 901, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 11343 itr]: obj = (22), mov = 11343, inf = 65.6%, acc = 26.2%, imp = 140

number of satisfied efficiency constraints: 22
[2 sec, 43423 itr]: obj = (26), mov = 43423, inf = 71.8%, acc = 18.1%, imp = 144

number of satisfied efficiency constraints: 26
[3 sec, 63007 itr]: obj = (27), mov = 63007, inf = 66.1%, acc = 20.1%, imp = 145

number of satisfied efficiency constraints: 27
[4 sec, 80211 itr]: obj = (27), mov = 80211, inf = 59.9%, acc = 22.3%, imp = 145

number of satisfied efficiency constraints: 27
[5 sec, 100107 itr]: obj = (29), mov = 100107, inf = 53%, acc = 24.7%, imp = 147

number of satisfied efficiency constraints: 29
[6 sec, 119308 itr]: obj = (29), mov = 119308, inf = 47.3%, acc = 26.5%, imp = 147

number of satisfied efficiency constraints: 29
[7 sec, 136047 itr]: obj = (29), mov = 136047, inf = 43.6%, acc = 28.6%, imp = 175

number of satisfied efficiency constraints: 29
[8 sec, 153645 itr]: obj = (29), mov = 153645, inf = 40.2%, acc = 29.5%, imp = 177

number of satisfied efficiency constraints: 29
[9 sec, 171729 itr]: obj = (29), mov = 171729, inf = 37.5%, acc = 29.8%, imp = 177

number of satisfied efficiency constraints: 29
[10 sec, 190812 itr]: obj = (29), mov = 190812, inf = 35.2%, acc = 30%, imp = 177

number of satisfied efficiency constraints: 29
[11 sec, 209132 itr]: obj = (29), mov = 209132, inf = 33.4%, acc = 30.3%, imp = 177

number of satisfied efficiency constraints: 29
[12 sec, 227742 itr]: obj = (29), mov = 227742, inf = 31.8%, acc = 30.4%, imp = 177

number of satisfied efficiency constraints: 29
[13 sec, 242985 itr]: obj = (29), mov = 242985, inf = 30.7%, acc = 31.2%, imp = 213

number of satisfied efficiency constraints: 29
[14 sec, 257785 itr]: obj = (29), mov = 257785, inf = 29.7%, acc = 32%, imp = 214

number of satisfied efficiency constraints: 29
[15 sec, 273811 itr]: obj = (29), mov = 273811, inf = 28.8%, acc = 32.4%, imp = 216

number of satisfied efficiency constraints: 29
[16 sec, 290865 itr]: obj = (29), mov = 290865, inf = 27.9%, acc = 32.6%, imp = 216

number of satisfied efficiency constraints: 29
[17 sec, 309068 itr]: obj = (29), mov = 309068, inf = 27.1%, acc = 32.5%, imp = 217

number of satisfied efficiency constraints: 29
[18 sec, 327835 itr]: obj = (29), mov = 327835, inf = 26.3%, acc = 32.3%, imp = 217

number of satisfied efficiency constraints: 29
[19 sec, 346657 itr]: obj = (29), mov = 346657, inf = 25.6%, acc = 32.2%, imp = 217

number of satisfied efficiency constraints: 29
[20 sec, 365141 itr]: obj = (29), mov = 365141, inf = 25%, acc = 32%, imp = 217

number of satisfied efficiency constraints: 29
[21 sec, 384208 itr]: obj = (30), mov = 384208, inf = 24.5%, acc = 31.9%, imp = 218

number of satisfied efficiency constraints: 30
[22 sec, 403426 itr]: obj = (30), mov = 403426, inf = 24%, acc = 31.7%, imp = 218

number of satisfied efficiency constraints: 30
[23 sec, 423008 itr]: obj = (30), mov = 423008, inf = 23.5%, acc = 31.5%, imp = 218

number of satisfied efficiency constraints: 30
[24 sec, 442936 itr]: obj = (30), mov = 442936, inf = 23%, acc = 31.2%, imp = 218

number of satisfied efficiency constraints: 30
[25 sec, 462125 itr]: obj = (30), mov = 462125, inf = 22.6%, acc = 31.1%, imp = 218

number of satisfied efficiency constraints: 30
[26 sec, 482218 itr]: obj = (30), mov = 482218, inf = 22.3%, acc = 30.9%, imp = 218

number of satisfied efficiency constraints: 30
[27 sec, 501915 itr]: obj = (30), mov = 501915, inf = 22%, acc = 30.8%, imp = 218

number of satisfied efficiency constraints: 30
[28 sec, 521819 itr]: obj = (30), mov = 521819, inf = 21.7%, acc = 30.6%, imp = 218

number of satisfied efficiency constraints: 30
[29 sec, 541986 itr]: obj = (30), mov = 541986, inf = 21.4%, acc = 30.4%, imp = 218

number of satisfied efficiency constraints: 30
[30 sec, 561875 itr]: obj = (30), mov = 561875, inf = 21.1%, acc = 30.3%, imp = 218

number of satisfied efficiency constraints: 30
[31 sec, 582269 itr]: obj = (30), mov = 582269, inf = 20.8%, acc = 30.2%, imp = 218

number of satisfied efficiency constraints: 30
[32 sec, 602089 itr]: obj = (30), mov = 602089, inf = 20.6%, acc = 30.1%, imp = 218

number of satisfied efficiency constraints: 30
[33 sec, 622061 itr]: obj = (30), mov = 622061, inf = 20.4%, acc = 30%, imp = 218

number of satisfied efficiency constraints: 30
[34 sec, 641804 itr]: obj = (30), mov = 641804, inf = 20.1%, acc = 29.9%, imp = 218

number of satisfied efficiency constraints: 30
[35 sec, 661500 itr]: obj = (30), mov = 661500, inf = 20%, acc = 29.8%, imp = 218

number of satisfied efficiency constraints: 30
[36 sec, 680973 itr]: obj = (30), mov = 680973, inf = 19.8%, acc = 29.8%, imp = 218

number of satisfied efficiency constraints: 30
[37 sec, 698819 itr]: obj = (30), mov = 698819, inf = 19.6%, acc = 29.8%, imp = 249

number of satisfied efficiency constraints: 30
[38 sec, 713166 itr]: obj = (30), mov = 713166, inf = 19.5%, acc = 30.1%, imp = 250

number of satisfied efficiency constraints: 30
[39 sec, 728393 itr]: obj = (30), mov = 728393, inf = 19.4%, acc = 30.4%, imp = 250

number of satisfied efficiency constraints: 30
[40 sec, 744056 itr]: obj = (30), mov = 744056, inf = 19.3%, acc = 30.5%, imp = 251

number of satisfied efficiency constraints: 30
[41 sec, 760029 itr]: obj = (30), mov = 760029, inf = 19.1%, acc = 30.6%, imp = 253

number of satisfied efficiency constraints: 30
[42 sec, 777724 itr]: obj = (30), mov = 777724, inf = 19%, acc = 30.6%, imp = 253

number of satisfied efficiency constraints: 30
[43 sec, 795708 itr]: obj = (30), mov = 795708, inf = 18.9%, acc = 30.6%, imp = 253

number of satisfied efficiency constraints: 30
[44 sec, 813737 itr]: obj = (30), mov = 813737, inf = 18.8%, acc = 30.7%, imp = 253

number of satisfied efficiency constraints: 30
[45 sec, 831377 itr]: obj = (30), mov = 831377, inf = 18.7%, acc = 30.7%, imp = 253

number of satisfied efficiency constraints: 30
[46 sec, 849333 itr]: obj = (30), mov = 849333, inf = 18.6%, acc = 30.7%, imp = 253

number of satisfied efficiency constraints: 30
[47 sec, 866753 itr]: obj = (30), mov = 866753, inf = 18.5%, acc = 30.7%, imp = 253

number of satisfied efficiency constraints: 30
[48 sec, 884662 itr]: obj = (30), mov = 884662, inf = 18.4%, acc = 30.7%, imp = 253

number of satisfied efficiency constraints: 30
[49 sec, 903041 itr]: obj = (30), mov = 903041, inf = 18.2%, acc = 30.7%, imp = 253

number of satisfied efficiency constraints: 30
[50 sec, 918382 itr]: obj = (30), mov = 918382, inf = 18.2%, acc = 30.8%, imp = 292

number of satisfied efficiency constraints: 30
[51 sec, 936178 itr]: obj = (30), mov = 936178, inf = 18.1%, acc = 30.8%, imp = 293

number of satisfied efficiency constraints: 30
[52 sec, 953556 itr]: obj = (30), mov = 953556, inf = 18%, acc = 30.8%, imp = 293

number of satisfied efficiency constraints: 30
[53 sec, 971206 itr]: obj = (30), mov = 971206, inf = 17.9%, acc = 30.8%, imp = 293

number of satisfied efficiency constraints: 30
[54 sec, 988924 itr]: obj = (30), mov = 988924, inf = 17.8%, acc = 30.9%, imp = 293

number of satisfied efficiency constraints: 30
[55 sec, 1007340 itr]: obj = (30), mov = 1007340, inf = 17.7%, acc = 30.8%, imp = 294

number of satisfied efficiency constraints: 30
[56 sec, 1026236 itr]: obj = (30), mov = 1026236, inf = 17.7%, acc = 30.8%, imp = 294

number of satisfied efficiency constraints: 30
[57 sec, 1045392 itr]: obj = (30), mov = 1045392, inf = 17.6%, acc = 30.7%, imp = 294

number of satisfied efficiency constraints: 30
[58 sec, 1064293 itr]: obj = (30), mov = 1064293, inf = 17.5%, acc = 30.7%, imp = 294

number of satisfied efficiency constraints: 30
[59 sec, 1083416 itr]: obj = (30), mov = 1083416, inf = 17.4%, acc = 30.6%, imp = 294

number of satisfied efficiency constraints: 30
[60 sec, 1102070 itr]: obj = (30), mov = 1102070, inf = 17.4%, acc = 30.6%, imp = 294

number of satisfied efficiency constraints: 30
[61 sec, 1120978 itr]: obj = (30), mov = 1120978, inf = 17.3%, acc = 30.5%, imp = 294

number of satisfied efficiency constraints: 30
[62 sec, 1139871 itr]: obj = (30), mov = 1139871, inf = 17.3%, acc = 30.5%, imp = 294

number of satisfied efficiency constraints: 30
[63 sec, 1158916 itr]: obj = (30), mov = 1158916, inf = 17.2%, acc = 30.4%, imp = 294

number of satisfied efficiency constraints: 30
[64 sec, 1177753 itr]: obj = (30), mov = 1177753, inf = 17.1%, acc = 30.4%, imp = 294

number of satisfied efficiency constraints: 30
[65 sec, 1197668 itr]: obj = (30), mov = 1197668, inf = 17.1%, acc = 30.3%, imp = 294

number of satisfied efficiency constraints: 30
[66 sec, 1217087 itr]: obj = (30), mov = 1217087, inf = 17%, acc = 30.2%, imp = 294

number of satisfied efficiency constraints: 30
[67 sec, 1231934 itr]: obj = (30), mov = 1231934, inf = 17%, acc = 30.4%, imp = 327

number of satisfied efficiency constraints: 30
[68 sec, 1248450 itr]: obj = (30), mov = 1248450, inf = 16.9%, acc = 30.4%, imp = 327

number of satisfied efficiency constraints: 30
[69 sec, 1264638 itr]: obj = (30), mov = 1264638, inf = 16.9%, acc = 30.5%, imp = 327

number of satisfied efficiency constraints: 30
[70 sec, 1281494 itr]: obj = (30), mov = 1281494, inf = 16.8%, acc = 30.5%, imp = 327

number of satisfied efficiency constraints: 30
[71 sec, 1298003 itr]: obj = (30), mov = 1298003, inf = 16.8%, acc = 30.6%, imp = 327

number of satisfied efficiency constraints: 30
[72 sec, 1314583 itr]: obj = (30), mov = 1314583, inf = 16.7%, acc = 30.6%, imp = 327

number of satisfied efficiency constraints: 30
[73 sec, 1330937 itr]: obj = (30), mov = 1330937, inf = 16.7%, acc = 30.7%, imp = 353

number of satisfied efficiency constraints: 30
[74 sec, 1344472 itr]: obj = (30), mov = 1344472, inf = 16.6%, acc = 30.9%, imp = 359

number of satisfied efficiency constraints: 30
[75 sec, 1358562 itr]: obj = (30), mov = 1358562, inf = 16.6%, acc = 30.9%, imp = 360

number of satisfied efficiency constraints: 30
[76 sec, 1371112 itr]: obj = (30), mov = 1371112, inf = 16.5%, acc = 31%, imp = 360

number of satisfied efficiency constraints: 30
[77 sec, 1386284 itr]: obj = (30), mov = 1386284, inf = 16.5%, acc = 31.1%, imp = 360

number of satisfied efficiency constraints: 30
[78 sec, 1400551 itr]: obj = (30), mov = 1400551, inf = 16.5%, acc = 31.1%, imp = 361

number of satisfied efficiency constraints: 30
[79 sec, 1415751 itr]: obj = (30), mov = 1415751, inf = 16.4%, acc = 31.2%, imp = 361

number of satisfied efficiency constraints: 30
[80 sec, 1431548 itr]: obj = (30), mov = 1431548, inf = 16.4%, acc = 31.2%, imp = 361

number of satisfied efficiency constraints: 30
[81 sec, 1447580 itr]: obj = (30), mov = 1447580, inf = 16.3%, acc = 31.2%, imp = 361

number of satisfied efficiency constraints: 30
[82 sec, 1463490 itr]: obj = (30), mov = 1463490, inf = 16.3%, acc = 31.3%, imp = 361

number of satisfied efficiency constraints: 30
[83 sec, 1478438 itr]: obj = (30), mov = 1478438, inf = 16.3%, acc = 31.3%, imp = 361

number of satisfied efficiency constraints: 30
[84 sec, 1495473 itr]: obj = (30), mov = 1495473, inf = 16.2%, acc = 31.3%, imp = 362

number of satisfied efficiency constraints: 30
[85 sec, 1513457 itr]: obj = (30), mov = 1513457, inf = 16.2%, acc = 31.3%, imp = 362

number of satisfied efficiency constraints: 30
[86 sec, 1529480 itr]: obj = (30), mov = 1529480, inf = 16.2%, acc = 31.3%, imp = 362

number of satisfied efficiency constraints: 30
[87 sec, 1545082 itr]: obj = (30), mov = 1545082, inf = 16.1%, acc = 31.2%, imp = 362

number of satisfied efficiency constraints: 30
[88 sec, 1560069 itr]: obj = (30), mov = 1560069, inf = 16.1%, acc = 31.2%, imp = 362

number of satisfied efficiency constraints: 30
[89 sec, 1576589 itr]: obj = (30), mov = 1576589, inf = 16.1%, acc = 31.2%, imp = 362

number of satisfied efficiency constraints: 30
[90 sec, 1588643 itr]: obj = (30), mov = 1588643, inf = 16%, acc = 31.2%, imp = 362

number of satisfied efficiency constraints: 30
[90 sec, 1588643 itr]: obj = (30), mov = 1588643, inf = 16%, acc = 31.2%, imp = 362

number of satisfied efficiency constraints: 30
1588643 iterations, 1588643 moves performed in 90 seconds
Feasible solution: obj = (30)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 30
Mapping: 0->35, 1->31, 2->0, 3->45, 4->8, 5->46, 6->25, 7->14, 8->8, 9->36, 10->39, 11->32, 12->42, 13->35, 14->26, 15->26, 16->33, 17->45, 18->10, 19->4, 20->41, 21->39, 22->35, 23->27, 24->17, 25->8, 26->38, 27->27, 28->24, 29->38, 30->44, 31->47, 32->32, 33->7, 34->7, 35->8, 36->21, 37->26, 38->12, 39->23, 40->47, 41->0, 42->11, 43->16, 44->29, 45->38, 46->23, 47->6, 48->4, 49->30, 50->3, 51->43, 52->32, 53->22, 54->18, 55->44, 56->19, 57->4, 58->16, 59->47, 60->38, 61->47, 62->10, 63->4, 64->45, 65->44, 66->15, 67->40, 68->40, 69->32, 70->28, 71->12, 72->1, 73->20, 74->15, 75->0, 76->38, 77->8, 78->40, 79->14, 80->34, 81->11, 82->30, 83->40, 84->34, 85->39, 86->47, 87->13, 88->22, 89->28, 90->31, 91->19, 92->0, 93->3, 94->34, 95->36, 96->20, 97->36, 98->20, 99->9, 100->19, 101->18, 102->40, 103->24, 104->24, 105->13, 106->27, 107->11, 108->31, 109->35, 110->2, 111->42, 112->9, 113->25, 114->19, 115->6, 116->29, 117->28, 118->43, 119->5, 120->32, 121->28, 122->42, 123->24, 124->13, 125->10, 126->18, 127->1, 128->41, 129->37, 130->23, 131->5, 132->29, 133->16, 134->7, 135->40, 136->9, 137->46, 138->28, 139->16, 140->22, 141->28, 142->46, 143->10, 144->27, 145->44, 146->35, 147->3, 148->41, 149->29, 150->35, 151->26, 152->5, 153->32, 154->43, 155->27, 156->12, 157->5, 158->14, 159->30, 160->39, 161->3, 162->9, 163->8, 164->34, 165->24, 166->12, 167->21, 168->29, 169->33, 170->44, 171->22, 172->10, 173->16, 174->45, 175->34, 176->0, 177->7, 178->5, 179->17, 180->2, 181->25, 182->41, 183->37, 184->14, 185->42, 186->24, 187->13, 188->6, 189->9, 190->13, 191->2, 192->45, 193->21, 194->42, 195->43, 196->11, 197->41, 198->11, 199->37, 200->22, 201->26, 202->12, 203->25, 204->2, 205->15, 206->38, 207->43, 208->20, 209->46, 210->9, 211->37, 212->41, 213->21, 214->31, 215->19, 216->6, 217->15, 218->47, 219->7, 220->36, 221->18, 222->33, 223->25, 224->16, 225->21, 226->36, 227->15, 228->23, 229->4, 230->1, 231->23, 232->4, 233->1, 234->21, 235->3, 236->31, 237->17, 238->45, 239->11, 240->6, 241->30, 242->15, 243->36, 244->18, 245->31, 246->17, 247->20, 248->19, 249->23, 250->43, 251->14, 252->17, 253->26, 254->2, 255->10, 256->18, 257->1, 258->33, 259->33, 260->3, 261->0, 262->6, 263->39, 264->5, 265->29, 266->13, 267->46, 268->37, 269->1, 270->25, 271->42, 272->14, 273->22, 274->46, 275->39, 276->37, 277->20, 278->44, 279->30, 280->34, 281->30, 282->2, 283->12, 284->17, 285->7, 286->33, 287->27
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.62302	1.99727	2.11875	1.83272	1.84544	1.4749
	2.78906	4.5366	3.42607	10.3084	3.51989	2.07267
	3.32527	1.46687	1.59185	6.46167	11.0233	2.50189
	3.33905	3.65737	12.8448	1.83787	3.03928	2.02719
	2.04391	1.15174	2.18845	1.29987	12.646	4.89942
	2.64146	2.34422	6.93324	1.98328	1.27126	2.13062
	4.22814	1.45295	1.64001	1.49547	11.4889	2.06547
	2.07551	2.81639	1.65962	1.93898	10.002	2.942
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	1	0	0	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 15 --global-tlim 90 --seed 901

Starting the Optimization Process
