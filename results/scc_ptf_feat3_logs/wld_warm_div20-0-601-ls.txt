LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
2.32528, 2.69661, 2.33489, 2.61968, 1.39219, 0.631348, 2.14457, 2.09786, 2.0156, 1.40994, 1.98032, 2.35171, 2.14681, 5.51269, 0.508952, 0.535891, 0.5, 2.79566, 3.22107, 2.76887, 0.692256, 1.46167, 0.850599, 3.00553, 2.23892, 1.87871, 1.2304, 2.62408, 1.53786, 2.49003, 2.48813, 1.34152, 1.20766, 2.56005, 2.63035, 1.77229, 2.66158, 1.90349, 1.20222, 0.5, 2.9669, 2.76581, 1.98381, 1.59411, 2.21138, 1.7083, 2.1388, 2.3636, 2.19001, 0.5, 4.03973, 1.23369, 3.32551, 0.711065, 3.24643, 1.1781, 4.52644, 1.21575, 0.964015, 0.869259, 0.938447, 3.922, 0.843693, 1.07256, 1.25476, 3.96854, 1.39503, 3.56897, 0.5, 2.38786, 2.90278, 1.24536, 2.93462, 0.896481, 1.06807, 2.45856, 1.87207, 2.7702, 1.69628, 2.78384, 1.12446, 0.5, 4.4353, 1.46012, 2.38702, 1.72511, 2.41288, 0.580633, 2.60767, 2.28668, 3.58483, 1.62044, 2.45795, 1.4516, 1.25484, 1.63034, 0.91103, 2.63715, 3.19185, 2.24391, 1.0621, 1.95395, 1.32327, 1.66125, 1.74398, 2.48313, 2.38041, 2.40796, 2.4193, 1.97648, 3.89831, 1.45677, 1.43626, 0.812887, 1.06923, 2.69108, 2.69001, 2.40738, 0.5, 2.6423, 2.42072, 2.2702, 2.33269, 1.11317, 2.14266, 1.72055, 1.81811, 2.32174, 0.5, 1.62056, 3.11018, 2.62942, 0.707084, 5.29154, 0.5, 1.5014, 0.779448, 3.22052, 0.5, 1.39923, 2.29096, 2.96028, 0.653012, 4.19651, 2.63646, 4.68393, 16.8611, 9.38735, 10.584, 15.8471, 12.4193, 11.3196, 8.5879, 5.72528, 10.3222, 11.6257, 2.31026, 0.923478, 2.2913, 2.17043, 2.2971, 2.00743, 2.10152, 2.2143, 1.84459, 1.79931, 2.20512, 1.83515, 2.41186, 0.899128, 2.56038, 3.06383, 0.544909, 2.51989, 0.713665, 2.36931, 5.49921, 0.839979, 0.5, 2.07783, 5.95985, 14.7485, 5.49833, 11.4358, 10.0437, 12.3138, 10.8716, 6.94639, 11.4038, 9.4133, 11.2042, 10.1607, 2.11606, 2.1784, 1.63394, 2.49925, 1.77286, 1.79949, 2.09519, 0.5, 3.13735, 2.57248, 0.589622, 3.10535, 0.5, 2.33476, 2.6411, 2.10101, 1.2168, 3.20633, 0.714263, 1.016, 4.23816, 2.98103, 2.23276, 0.817779, 2.06887, 21.1907, 15.5591, 1.76883, 12.7602, 6.65228, 9.39986, 0.989694, 12.6479, 13.8169, 5.73979, 17.4058, 0.553682, 0.732482, 4.94519, 3.15069, 2.11795, 0.5, 2.1616, 1.82805, 1.98405, 2.41493, 1.33159, 2.27978, 1.97777, 1.89826, 2.04107, 2.08565, 1.86146, 2.1358, 1.84052, 2.09338, 3.1304, 0.617474, 1.88191, 2.43632, 1.05405, 2.49331, 2.40819, 2.42397, 2.30645, 1.31403, 1.10261, 2.56829, 2.2386, 2.10825, 2.51723, 1.46503, 1.65952, 0.5, 3.22461, 0.849226, 3.22171, 2.54493, 1.44435, 2.97665, 2.08928, 0.529936, 4.44981, 0.509979, 2.21915, 2.3045, 1.9667, 1.74361, 1.68983, 2.07621, 0.917113, 3.85127, 2.09248, 1.7479, 0.810541, 2.58069
cpi min/max: 0.5/21.1907
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55179, operands = 136540
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 601, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 1882 itr]: infeas = (12, 12, 18), mov = 1882, inf = 66.4%, acc = 29.6%, imp = 131

number of satisfied efficiency constraints: 16
[2 sec, 26832 itr]: obj = (25), mov = 26832, inf = 75.8%, acc = 17.3%, imp = 144

number of satisfied efficiency constraints: 25
[3 sec, 48134 itr]: obj = (25), mov = 48134, inf = 73.3%, acc = 18.2%, imp = 144

number of satisfied efficiency constraints: 25
[4 sec, 64112 itr]: obj = (26), mov = 64112, inf = 67.6%, acc = 21.4%, imp = 145

number of satisfied efficiency constraints: 26
[5 sec, 80129 itr]: obj = (26), mov = 80129, inf = 62.6%, acc = 24.1%, imp = 145

number of satisfied efficiency constraints: 26
[6 sec, 97466 itr]: obj = (26), mov = 97466, inf = 57.9%, acc = 26.6%, imp = 145

number of satisfied efficiency constraints: 26
[7 sec, 114464 itr]: obj = (26), mov = 114464, inf = 54.1%, acc = 29%, imp = 145

number of satisfied efficiency constraints: 26
[8 sec, 131670 itr]: obj = (26), mov = 131670, inf = 51.2%, acc = 30.6%, imp = 145

number of satisfied efficiency constraints: 26
[9 sec, 146342 itr]: obj = (26), mov = 146342, inf = 49.1%, acc = 32.5%, imp = 169

number of satisfied efficiency constraints: 26
[10 sec, 159834 itr]: obj = (26), mov = 159834, inf = 46.9%, acc = 34%, imp = 169

number of satisfied efficiency constraints: 26
[11 sec, 174003 itr]: obj = (26), mov = 174003, inf = 44.7%, acc = 35.2%, imp = 170

number of satisfied efficiency constraints: 26
[12 sec, 188646 itr]: obj = (26), mov = 188646, inf = 42.6%, acc = 36.2%, imp = 170

number of satisfied efficiency constraints: 26
[13 sec, 204854 itr]: obj = (27), mov = 204854, inf = 40.8%, acc = 36.8%, imp = 171

number of satisfied efficiency constraints: 27
[14 sec, 220521 itr]: obj = (27), mov = 220521, inf = 39.3%, acc = 37.3%, imp = 171

number of satisfied efficiency constraints: 27
[15 sec, 236539 itr]: obj = (27), mov = 236539, inf = 37.9%, acc = 37.8%, imp = 171

number of satisfied efficiency constraints: 27
[16 sec, 250716 itr]: obj = (27), mov = 250716, inf = 36.9%, acc = 38.1%, imp = 171

number of satisfied efficiency constraints: 27
[17 sec, 265202 itr]: obj = (27), mov = 265202, inf = 36%, acc = 38.5%, imp = 171

number of satisfied efficiency constraints: 27
[18 sec, 281048 itr]: obj = (27), mov = 281048, inf = 35.1%, acc = 38.8%, imp = 171

number of satisfied efficiency constraints: 27
[19 sec, 296951 itr]: obj = (27), mov = 296951, inf = 34.2%, acc = 39%, imp = 171

number of satisfied efficiency constraints: 27
[20 sec, 313005 itr]: obj = (28), mov = 313005, inf = 33.4%, acc = 39.1%, imp = 172

number of satisfied efficiency constraints: 28
[21 sec, 329675 itr]: obj = (28), mov = 329675, inf = 32.8%, acc = 39.1%, imp = 172

number of satisfied efficiency constraints: 28
[22 sec, 346260 itr]: obj = (28), mov = 346260, inf = 32.1%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[23 sec, 363568 itr]: obj = (28), mov = 363568, inf = 31.4%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[24 sec, 381056 itr]: obj = (28), mov = 381056, inf = 30.8%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[25 sec, 398274 itr]: obj = (28), mov = 398274, inf = 30.4%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[26 sec, 416071 itr]: obj = (28), mov = 416071, inf = 29.9%, acc = 39.1%, imp = 172

number of satisfied efficiency constraints: 28
[27 sec, 432645 itr]: obj = (28), mov = 432645, inf = 29.5%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[28 sec, 449401 itr]: obj = (28), mov = 449401, inf = 29.1%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[29 sec, 467057 itr]: obj = (28), mov = 467057, inf = 28.7%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[30 sec, 483529 itr]: obj = (28), mov = 483529, inf = 28.4%, acc = 39.3%, imp = 172

number of satisfied efficiency constraints: 28
[31 sec, 500889 itr]: obj = (28), mov = 500889, inf = 28.1%, acc = 39.3%, imp = 172

number of satisfied efficiency constraints: 28
[32 sec, 518773 itr]: obj = (28), mov = 518773, inf = 27.8%, acc = 39.3%, imp = 172

number of satisfied efficiency constraints: 28
[33 sec, 536048 itr]: obj = (28), mov = 536048, inf = 27.5%, acc = 39.3%, imp = 172

number of satisfied efficiency constraints: 28
[34 sec, 553638 itr]: obj = (28), mov = 553638, inf = 27.2%, acc = 39.3%, imp = 172

number of satisfied efficiency constraints: 28
[35 sec, 571206 itr]: obj = (28), mov = 571206, inf = 27%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[36 sec, 588694 itr]: obj = (28), mov = 588694, inf = 26.7%, acc = 39.3%, imp = 172

number of satisfied efficiency constraints: 28
[37 sec, 606294 itr]: obj = (28), mov = 606294, inf = 26.5%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[38 sec, 624182 itr]: obj = (28), mov = 624182, inf = 26.3%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[39 sec, 641457 itr]: obj = (28), mov = 641457, inf = 26.1%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[40 sec, 658821 itr]: obj = (28), mov = 658821, inf = 25.9%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[41 sec, 676694 itr]: obj = (28), mov = 676694, inf = 25.7%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[42 sec, 694333 itr]: obj = (28), mov = 694333, inf = 25.6%, acc = 39.2%, imp = 172

number of satisfied efficiency constraints: 28
[43 sec, 711470 itr]: obj = (28), mov = 711470, inf = 25.4%, acc = 39.2%, imp = 177

number of satisfied efficiency constraints: 28
[44 sec, 726050 itr]: obj = (28), mov = 726050, inf = 25.2%, acc = 39.4%, imp = 207

number of satisfied efficiency constraints: 28
[45 sec, 740604 itr]: obj = (28), mov = 740604, inf = 25.1%, acc = 39.6%, imp = 207

number of satisfied efficiency constraints: 28
[46 sec, 755114 itr]: obj = (28), mov = 755114, inf = 24.9%, acc = 39.8%, imp = 207

number of satisfied efficiency constraints: 28
[47 sec, 769637 itr]: obj = (28), mov = 769637, inf = 24.7%, acc = 40%, imp = 207

number of satisfied efficiency constraints: 28
[48 sec, 782794 itr]: obj = (28), mov = 782794, inf = 24.6%, acc = 40.1%, imp = 207

number of satisfied efficiency constraints: 28
[49 sec, 796105 itr]: obj = (28), mov = 796105, inf = 24.4%, acc = 40.2%, imp = 207

number of satisfied efficiency constraints: 28
[50 sec, 809555 itr]: obj = (28), mov = 809555, inf = 24.3%, acc = 40.4%, imp = 207

number of satisfied efficiency constraints: 28
[51 sec, 823899 itr]: obj = (28), mov = 823899, inf = 24.2%, acc = 40.5%, imp = 207

number of satisfied efficiency constraints: 28
[52 sec, 836708 itr]: obj = (28), mov = 836708, inf = 24%, acc = 40.7%, imp = 232

number of satisfied efficiency constraints: 28
[53 sec, 849874 itr]: obj = (28), mov = 849874, inf = 23.9%, acc = 40.9%, imp = 232

number of satisfied efficiency constraints: 28
[54 sec, 863228 itr]: obj = (28), mov = 863228, inf = 23.8%, acc = 41.1%, imp = 232

number of satisfied efficiency constraints: 28
[55 sec, 876201 itr]: obj = (28), mov = 876201, inf = 23.6%, acc = 41.3%, imp = 232

number of satisfied efficiency constraints: 28
[56 sec, 889522 itr]: obj = (28), mov = 889522, inf = 23.5%, acc = 41.5%, imp = 232

number of satisfied efficiency constraints: 28
[57 sec, 901328 itr]: obj = (28), mov = 901328, inf = 23.4%, acc = 41.7%, imp = 232

number of satisfied efficiency constraints: 28
[58 sec, 914319 itr]: obj = (28), mov = 914319, inf = 23.3%, acc = 41.8%, imp = 232

number of satisfied efficiency constraints: 28
[59 sec, 927715 itr]: obj = (28), mov = 927715, inf = 23.2%, acc = 42%, imp = 232

number of satisfied efficiency constraints: 28
[60 sec, 939907 itr]: obj = (28), mov = 939907, inf = 23.1%, acc = 42.2%, imp = 256

number of satisfied efficiency constraints: 28
[61 sec, 953075 itr]: obj = (28), mov = 953075, inf = 23%, acc = 42.3%, imp = 258

number of satisfied efficiency constraints: 28
[62 sec, 966975 itr]: obj = (28), mov = 966975, inf = 22.9%, acc = 42.5%, imp = 259

number of satisfied efficiency constraints: 28
[63 sec, 981490 itr]: obj = (28), mov = 981490, inf = 22.7%, acc = 42.6%, imp = 259

number of satisfied efficiency constraints: 28
[64 sec, 995530 itr]: obj = (28), mov = 995530, inf = 22.6%, acc = 42.7%, imp = 259

number of satisfied efficiency constraints: 28
[65 sec, 1009345 itr]: obj = (28), mov = 1009345, inf = 22.5%, acc = 42.8%, imp = 259

number of satisfied efficiency constraints: 28
[66 sec, 1023242 itr]: obj = (28), mov = 1023242, inf = 22.4%, acc = 42.8%, imp = 259

number of satisfied efficiency constraints: 28
[67 sec, 1037124 itr]: obj = (28), mov = 1037124, inf = 22.3%, acc = 42.9%, imp = 259

number of satisfied efficiency constraints: 28
[68 sec, 1050923 itr]: obj = (28), mov = 1050923, inf = 22.2%, acc = 43%, imp = 259

number of satisfied efficiency constraints: 28
[69 sec, 1063680 itr]: obj = (28), mov = 1063680, inf = 22.2%, acc = 43.2%, imp = 277

number of satisfied efficiency constraints: 28
[70 sec, 1075872 itr]: obj = (28), mov = 1075872, inf = 22.1%, acc = 43.3%, imp = 277

number of satisfied efficiency constraints: 28
[71 sec, 1085543 itr]: obj = (28), mov = 1085543, inf = 22%, acc = 43.4%, imp = 277

number of satisfied efficiency constraints: 28
[72 sec, 1095699 itr]: obj = (28), mov = 1095699, inf = 21.9%, acc = 43.5%, imp = 277

number of satisfied efficiency constraints: 28
[73 sec, 1107477 itr]: obj = (28), mov = 1107477, inf = 21.9%, acc = 43.5%, imp = 277

number of satisfied efficiency constraints: 28
[74 sec, 1121186 itr]: obj = (28), mov = 1121186, inf = 21.8%, acc = 43.6%, imp = 277

number of satisfied efficiency constraints: 28
[75 sec, 1134320 itr]: obj = (28), mov = 1134320, inf = 21.7%, acc = 43.8%, imp = 277

number of satisfied efficiency constraints: 28
[76 sec, 1147742 itr]: obj = (28), mov = 1147742, inf = 21.6%, acc = 43.9%, imp = 277

number of satisfied efficiency constraints: 28
[77 sec, 1158717 itr]: obj = (28), mov = 1158717, inf = 21.5%, acc = 44%, imp = 277

number of satisfied efficiency constraints: 28
[78 sec, 1170966 itr]: obj = (28), mov = 1170966, inf = 21.5%, acc = 44.1%, imp = 298

number of satisfied efficiency constraints: 28
[79 sec, 1184275 itr]: obj = (28), mov = 1184275, inf = 21.4%, acc = 44.2%, imp = 299

number of satisfied efficiency constraints: 28
[80 sec, 1197905 itr]: obj = (28), mov = 1197905, inf = 21.3%, acc = 44.3%, imp = 300

number of satisfied efficiency constraints: 28
[81 sec, 1212192 itr]: obj = (28), mov = 1212192, inf = 21.3%, acc = 44.3%, imp = 300

number of satisfied efficiency constraints: 28
[82 sec, 1226818 itr]: obj = (28), mov = 1226818, inf = 21.2%, acc = 44.3%, imp = 300

number of satisfied efficiency constraints: 28
[83 sec, 1241039 itr]: obj = (28), mov = 1241039, inf = 21.1%, acc = 44.4%, imp = 300

number of satisfied efficiency constraints: 28
[84 sec, 1255618 itr]: obj = (28), mov = 1255618, inf = 21%, acc = 44.4%, imp = 300

number of satisfied efficiency constraints: 28
[85 sec, 1270080 itr]: obj = (28), mov = 1270080, inf = 21%, acc = 44.5%, imp = 300

number of satisfied efficiency constraints: 28
[86 sec, 1283623 itr]: obj = (28), mov = 1283623, inf = 20.9%, acc = 44.5%, imp = 335

number of satisfied efficiency constraints: 28
[87 sec, 1297446 itr]: obj = (28), mov = 1297446, inf = 20.9%, acc = 44.6%, imp = 336

number of satisfied efficiency constraints: 28
[88 sec, 1310345 itr]: obj = (28), mov = 1310345, inf = 20.8%, acc = 44.7%, imp = 336

number of satisfied efficiency constraints: 28
[89 sec, 1323560 itr]: obj = (28), mov = 1323560, inf = 20.7%, acc = 44.8%, imp = 336

number of satisfied efficiency constraints: 28
[90 sec, 1336713 itr]: obj = (28), mov = 1336713, inf = 20.7%, acc = 44.8%, imp = 336

number of satisfied efficiency constraints: 28
[90 sec, 1336713 itr]: obj = (28), mov = 1336713, inf = 20.7%, acc = 44.8%, imp = 336

number of satisfied efficiency constraints: 28
1336713 iterations, 1336713 moves performed in 90 seconds
Feasible solution: obj = (28)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 28
Mapping: 0->37, 1->42, 2->40, 3->22, 4->5, 5->11, 6->7, 7->10, 8->5, 9->16, 10->47, 11->18, 12->22, 13->29, 14->19, 15->39, 16->39, 17->46, 18->17, 19->46, 20->29, 21->29, 22->2, 23->23, 24->14, 25->36, 26->45, 27->23, 28->6, 29->21, 30->12, 31->30, 32->15, 33->37, 34->22, 35->13, 36->24, 37->33, 38->28, 39->11, 40->36, 41->44, 42->0, 43->32, 44->13, 45->26, 46->8, 47->35, 48->39, 49->3, 50->34, 51->38, 52->25, 53->15, 54->10, 55->28, 56->10, 57->25, 58->21, 59->19, 60->12, 61->30, 62->21, 63->16, 64->1, 65->18, 66->17, 67->47, 68->45, 69->14, 70->41, 71->26, 72->26, 73->21, 74->23, 75->6, 76->20, 77->45, 78->47, 79->23, 80->44, 81->25, 82->17, 83->45, 84->24, 85->41, 86->38, 87->14, 88->6, 89->16, 90->28, 91->36, 92->43, 93->21, 94->18, 95->18, 96->12, 97->0, 98->9, 99->8, 100->36, 101->25, 102->2, 103->37, 104->1, 105->24, 106->28, 107->6, 108->1, 109->37, 110->2, 111->33, 112->42, 113->14, 114->47, 115->10, 116->31, 117->46, 118->32, 119->36, 120->30, 121->14, 122->4, 123->19, 124->25, 125->29, 126->10, 127->44, 128->16, 129->4, 130->29, 131->46, 132->1, 133->8, 134->30, 135->1, 136->41, 137->33, 138->19, 139->20, 140->32, 141->23, 142->16, 143->3, 144->8, 145->39, 146->9, 147->9, 148->10, 149->9, 150->26, 151->15, 152->12, 153->34, 154->34, 155->34, 156->32, 157->17, 158->12, 159->27, 160->43, 161->24, 162->13, 163->32, 164->35, 165->35, 166->25, 167->41, 168->33, 169->13, 170->4, 171->37, 172->45, 173->30, 174->5, 175->12, 176->6, 177->26, 178->33, 179->20, 180->2, 181->27, 182->35, 183->40, 184->43, 185->43, 186->19, 187->27, 188->26, 189->31, 190->40, 191->42, 192->7, 193->21, 194->7, 195->4, 196->15, 197->44, 198->7, 199->38, 200->31, 201->1, 202->24, 203->3, 204->37, 205->28, 206->8, 207->5, 208->42, 209->34, 210->22, 211->38, 212->40, 213->4, 214->22, 215->0, 216->18, 217->43, 218->40, 219->4, 220->9, 221->2, 222->7, 223->42, 224->34, 225->19, 226->41, 227->40, 228->0, 229->18, 230->11, 231->35, 232->8, 233->16, 234->44, 235->29, 236->30, 237->35, 238->3, 239->27, 240->13, 241->5, 242->15, 243->9, 244->32, 245->41, 246->11, 247->47, 248->20, 249->28, 250->44, 251->22, 252->42, 253->11, 254->0, 255->11, 256->2, 257->17, 258->20, 259->27, 260->43, 261->39, 262->20, 263->13, 264->31, 265->0, 266->31, 267->39, 268->3, 269->6, 270->17, 271->46, 272->24, 273->5, 274->7, 275->38, 276->15, 277->46, 278->36, 279->31, 280->3, 281->27, 282->38, 283->47, 284->33, 285->45, 286->23, 287->14
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.48343	1.69984	3.49846	2.34083	2.29379	1.44178
	2.8427	3.63991	2.84496	10.0222	4.16066	2.13906
	2.93102	1.73785	1.81187	3.2119	1.07036	2.12221
	2.00113	4.61332	2.01656	1.4707	2.13003	2.20872
	2.03634	1.8905	5.09188	5.13161	1.88296	2.38757
	2.1147	3.64473	1.79518	1.93085	7.92787	2.84524
	2.02286	2.01448	1.09828	1.81122	10.363	2.51968
	2.92568	8.42364	2.00917	1.37559	2.64708	2.37658
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	0	0	0	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 0 --global-tlim 90 --seed 601

Starting the Optimization Process
