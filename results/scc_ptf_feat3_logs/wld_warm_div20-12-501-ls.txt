LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
2.90519, 3.00187, 2.59408, 0.764911, 0.602349, 2.1316, 0.945185, 2.18256, 2.29122, 2.06357, 2.25563, 2.26183, 1.9514, 2.07615, 1.46262, 2.1898, 2.08454, 2.23549, 0.578052, 2.78771, 2.71554, 1.39733, 2.5547, 1.96666, 1.77915, 2.29415, 2.28815, 1.52202, 2.04631, 2.07021, 0.705144, 3.52595, 0.577794, 4.35465, 2.00297, 0.8335, 2.27118, 1.69393, 2.16483, 2.14014, 1.47659, 2.25333, 2.1552, 2.09153, 4.46658, 0.968409, 1.81829, 0.5, 0.671436, 0.5, 3.58689, 5.41061, 1.06563, 0.765436, 2.44583, 0.5, 2.00023, 2.50143, 3.50006, 1.05246, 0.830313, 0.692026, 1.66722, 2.58658, 3.77569, 2.44817, 1.53474, 2.44819, 2.51045, 2.51047, 0.5, 2.49615, 2.07622, 1.75409, 2.42361, 1.82052, 1.43757, 2.48798, 0.674821, 1.93223, 1.22573, 2.59825, 3.23437, 2.3346, 2.02062, 1.56531, 0.685947, 2.50408, 2.53617, 2.68787, 2.18405, 1.74593, 2.38926, 1.61039, 2.05674, 2.01363, 2.79678, 2.81147, 0.814836, 0.5, 2.45418, 2.62273, 0.605606, 3.33616, 0.5, 3.92751, 2.85574, 0.774989, 4.03938, 1.63878, 2.82253, 0.5, 1.6534, 1.3459, 1.54685, 0.5, 2.79143, 2.7573, 1.90179, 2.50264, 3.17346, 1.34045, 1.67046, 0.944843, 3.21848, 1.65231, 0.5, 2.50508, 3.06415, 1.40992, 2.53316, 1.98769, 2.20824, 1.55341, 0.511615, 3.33439, 2.34796, 2.04439, 2.065, 2.08406, 2.0553, 2.22587, 1.59248, 1.97728, 1.10884, 2.33107, 0.734037, 1.20233, 5.75006, 0.873654, 2.11678, 2.13263, 1.99938, 2.2624, 1.61199, 1.87683, 1.92695, 2.1557, 2.19942, 2.03818, 1.83717, 1.84259, 1.06903, 1.63587, 2.1269, 2.24311, 3.16967, 1.75542, 2.27664, 2.31265, 2.80435, 1.44689, 1.45856, 1.7009, 3.03613, 0.892094, 1.26678, 3.03981, 2.10236, 1.66282, 0.894979, 0.940761, 1.88205, 0.974263, 2.63175, 4.67619, 2.46838, 1.70812, 2.64584, 1.40925, 1.87971, 1.88871, 35, 27.3053, 9.19818, 23.0218, 9.20599, 28.2687, 3.11393, 1.54415, 2.22701, 2.23798, 2.37693, 0.5, 35, 12.4549, 16.6137, 25.8873, 23.624, 18.4201, 3.5718, 2.28371, 0.5, 0.566681, 1.96172, 3.11608, 4.6502, 0.651365, 0.5, 0.509514, 2.67246, 3.01647, 0.959727, 0.5, 3.60859, 0.857319, 1.36745, 4.70692, 1.79929, 2.43769, 1.7069, 1.48299, 1.99055, 2.58259, 1.43298, 1.11253, 3.52307, 3.52528, 1.46405, 0.942092, 0.664891, 1.93508, 3.20626, 3.19041, 1.50792, 1.49543, 2.18655, 1.25744, 2.12228, 2.26964, 2.25767, 1.90641, 0.52902, 2.58977, 3.92245, 0.5, 0.607089, 3.85167, 0.5, 2.51582, 3.21189, 1.11241, 1.68973, 2.97016, 30.6793, 35, 4.2256, 22.2669, 31.2751, 8.55314, 0.766831, 3.50378, 1.21244, 1.36904, 1.13738, 4.01053, 28.0884, 20.8486, 15.6235, 35, 19.1319, 13.3075, 3.14793, 0.715389, 2.92986, 3.64648, 1.06034, 0.5
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54816, operands = 135836
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 501, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 20487 itr]: obj = (25), mov = 20487, inf = 75.6%, acc = 17.7%, imp = 181

number of satisfied efficiency constraints: 25
[2 sec, 52804 itr]: obj = (26), mov = 52804, inf = 77.9%, acc = 13.7%, imp = 182

number of satisfied efficiency constraints: 26
[3 sec, 72723 itr]: obj = (28), mov = 72723, inf = 72.8%, acc = 15.3%, imp = 184

number of satisfied efficiency constraints: 28
[4 sec, 88413 itr]: obj = (28), mov = 88413, inf = 66.8%, acc = 17.5%, imp = 184

number of satisfied efficiency constraints: 28
[5 sec, 102717 itr]: obj = (29), mov = 102717, inf = 61.9%, acc = 19.2%, imp = 185

number of satisfied efficiency constraints: 29
[6 sec, 119092 itr]: obj = (29), mov = 119092, inf = 57%, acc = 20.5%, imp = 185

number of satisfied efficiency constraints: 29
[7 sec, 138018 itr]: obj = (30), mov = 138018, inf = 52.1%, acc = 21.9%, imp = 216

number of satisfied efficiency constraints: 30
[8 sec, 155823 itr]: obj = (30), mov = 155823, inf = 48%, acc = 23.5%, imp = 219

number of satisfied efficiency constraints: 30
[9 sec, 174942 itr]: obj = (30), mov = 174942, inf = 44.6%, acc = 24.2%, imp = 219

number of satisfied efficiency constraints: 30
[10 sec, 194841 itr]: obj = (30), mov = 194841, inf = 41.8%, acc = 24.6%, imp = 220

number of satisfied efficiency constraints: 30
[11 sec, 213865 itr]: obj = (30), mov = 213865, inf = 39.4%, acc = 24.9%, imp = 220

number of satisfied efficiency constraints: 30
[12 sec, 234709 itr]: obj = (30), mov = 234709, inf = 37.3%, acc = 24.9%, imp = 220

number of satisfied efficiency constraints: 30
[13 sec, 254656 itr]: obj = (30), mov = 254656, inf = 35.6%, acc = 25.1%, imp = 220

number of satisfied efficiency constraints: 30
[14 sec, 273118 itr]: obj = (30), mov = 273118, inf = 34.2%, acc = 25.4%, imp = 252

number of satisfied efficiency constraints: 30
[15 sec, 287524 itr]: obj = (30), mov = 287524, inf = 33.3%, acc = 26.4%, imp = 256

number of satisfied efficiency constraints: 30
[16 sec, 303288 itr]: obj = (30), mov = 303288, inf = 32.3%, acc = 27.1%, imp = 257

number of satisfied efficiency constraints: 30
[17 sec, 319311 itr]: obj = (30), mov = 319311, inf = 31.5%, acc = 27.6%, imp = 258

number of satisfied efficiency constraints: 30
[18 sec, 336293 itr]: obj = (30), mov = 336293, inf = 30.7%, acc = 27.9%, imp = 258

number of satisfied efficiency constraints: 30
[19 sec, 354402 itr]: obj = (30), mov = 354402, inf = 29.9%, acc = 28.1%, imp = 259

number of satisfied efficiency constraints: 30
[20 sec, 372206 itr]: obj = (30), mov = 372206, inf = 29.3%, acc = 28.2%, imp = 259

number of satisfied efficiency constraints: 30
[21 sec, 390938 itr]: obj = (30), mov = 390938, inf = 28.6%, acc = 28.2%, imp = 260

number of satisfied efficiency constraints: 30
[22 sec, 411578 itr]: obj = (30), mov = 411578, inf = 28%, acc = 28%, imp = 260

number of satisfied efficiency constraints: 30
[23 sec, 432124 itr]: obj = (30), mov = 432124, inf = 27.4%, acc = 27.8%, imp = 260

number of satisfied efficiency constraints: 30
[24 sec, 451760 itr]: obj = (30), mov = 451760, inf = 26.9%, acc = 27.7%, imp = 260

number of satisfied efficiency constraints: 30
[25 sec, 471339 itr]: obj = (30), mov = 471339, inf = 26.4%, acc = 27.7%, imp = 260

number of satisfied efficiency constraints: 30
[26 sec, 491091 itr]: obj = (30), mov = 491091, inf = 26%, acc = 27.6%, imp = 260

number of satisfied efficiency constraints: 30
[27 sec, 511227 itr]: obj = (30), mov = 511227, inf = 25.6%, acc = 27.6%, imp = 260

number of satisfied efficiency constraints: 30
[28 sec, 531584 itr]: obj = (30), mov = 531584, inf = 25.2%, acc = 27.5%, imp = 260

number of satisfied efficiency constraints: 30
[29 sec, 551553 itr]: obj = (30), mov = 551553, inf = 24.9%, acc = 27.4%, imp = 260

number of satisfied efficiency constraints: 30
[30 sec, 572530 itr]: obj = (30), mov = 572530, inf = 24.5%, acc = 27.3%, imp = 260

number of satisfied efficiency constraints: 30
[31 sec, 593452 itr]: obj = (30), mov = 593452, inf = 24.2%, acc = 27.2%, imp = 260

number of satisfied efficiency constraints: 30
[32 sec, 615315 itr]: obj = (31), mov = 615315, inf = 24%, acc = 27.1%, imp = 261

number of satisfied efficiency constraints: 31
[33 sec, 636669 itr]: obj = (31), mov = 636669, inf = 23.7%, acc = 26.9%, imp = 261

number of satisfied efficiency constraints: 31
[34 sec, 658913 itr]: obj = (31), mov = 658913, inf = 23.4%, acc = 26.7%, imp = 261

number of satisfied efficiency constraints: 31
[35 sec, 680836 itr]: obj = (31), mov = 680836, inf = 23.2%, acc = 26.6%, imp = 261

number of satisfied efficiency constraints: 31
[36 sec, 702743 itr]: obj = (31), mov = 702743, inf = 22.9%, acc = 26.4%, imp = 261

number of satisfied efficiency constraints: 31
[37 sec, 723928 itr]: obj = (31), mov = 723928, inf = 22.7%, acc = 26.3%, imp = 261

number of satisfied efficiency constraints: 31
[38 sec, 745949 itr]: obj = (31), mov = 745949, inf = 22.5%, acc = 26.2%, imp = 261

number of satisfied efficiency constraints: 31
[39 sec, 767579 itr]: obj = (31), mov = 767579, inf = 22.4%, acc = 26.1%, imp = 261

number of satisfied efficiency constraints: 31
[40 sec, 789909 itr]: obj = (31), mov = 789909, inf = 22.2%, acc = 26%, imp = 261

number of satisfied efficiency constraints: 31
[41 sec, 811319 itr]: obj = (31), mov = 811319, inf = 22%, acc = 25.9%, imp = 261

number of satisfied efficiency constraints: 31
[42 sec, 832706 itr]: obj = (31), mov = 832706, inf = 21.9%, acc = 25.9%, imp = 261

number of satisfied efficiency constraints: 31
[43 sec, 854217 itr]: obj = (31), mov = 854217, inf = 21.7%, acc = 25.8%, imp = 261

number of satisfied efficiency constraints: 31
[44 sec, 875845 itr]: obj = (31), mov = 875845, inf = 21.6%, acc = 25.7%, imp = 261

number of satisfied efficiency constraints: 31
[45 sec, 898126 itr]: obj = (31), mov = 898126, inf = 21.5%, acc = 25.6%, imp = 261

number of satisfied efficiency constraints: 31
[46 sec, 919584 itr]: obj = (31), mov = 919584, inf = 21.3%, acc = 25.5%, imp = 261

number of satisfied efficiency constraints: 31
[47 sec, 941018 itr]: obj = (31), mov = 941018, inf = 21.2%, acc = 25.5%, imp = 261

number of satisfied efficiency constraints: 31
[48 sec, 956770 itr]: obj = (31), mov = 956770, inf = 21.1%, acc = 25.4%, imp = 261

number of satisfied efficiency constraints: 31
[49 sec, 974267 itr]: obj = (31), mov = 974267, inf = 21%, acc = 25.4%, imp = 261

number of satisfied efficiency constraints: 31
[50 sec, 993975 itr]: obj = (31), mov = 993975, inf = 20.9%, acc = 25.3%, imp = 261

number of satisfied efficiency constraints: 31
[51 sec, 1015077 itr]: obj = (31), mov = 1015077, inf = 20.8%, acc = 25.3%, imp = 261

number of satisfied efficiency constraints: 31
[52 sec, 1037487 itr]: obj = (31), mov = 1037487, inf = 20.7%, acc = 25.2%, imp = 261

number of satisfied efficiency constraints: 31
[53 sec, 1058503 itr]: obj = (31), mov = 1058503, inf = 20.6%, acc = 25.2%, imp = 261

number of satisfied efficiency constraints: 31
[54 sec, 1081136 itr]: obj = (31), mov = 1081136, inf = 20.5%, acc = 25.1%, imp = 261

number of satisfied efficiency constraints: 31
[55 sec, 1102016 itr]: obj = (31), mov = 1102016, inf = 20.5%, acc = 25%, imp = 261

number of satisfied efficiency constraints: 31
[56 sec, 1123274 itr]: obj = (31), mov = 1123274, inf = 20.4%, acc = 25%, imp = 261

number of satisfied efficiency constraints: 31
[57 sec, 1143950 itr]: obj = (31), mov = 1143950, inf = 20.3%, acc = 25%, imp = 261

number of satisfied efficiency constraints: 31
[58 sec, 1163875 itr]: obj = (31), mov = 1163875, inf = 20.2%, acc = 25%, imp = 261

number of satisfied efficiency constraints: 31
[59 sec, 1184195 itr]: obj = (31), mov = 1184195, inf = 20.1%, acc = 25%, imp = 261

number of satisfied efficiency constraints: 31
[60 sec, 1205477 itr]: obj = (31), mov = 1205477, inf = 20%, acc = 24.9%, imp = 261

number of satisfied efficiency constraints: 31
[61 sec, 1226690 itr]: obj = (31), mov = 1226690, inf = 20%, acc = 24.9%, imp = 261

number of satisfied efficiency constraints: 31
[62 sec, 1248201 itr]: obj = (31), mov = 1248201, inf = 19.9%, acc = 24.8%, imp = 261

number of satisfied efficiency constraints: 31
[63 sec, 1269244 itr]: obj = (31), mov = 1269244, inf = 19.8%, acc = 24.8%, imp = 261

number of satisfied efficiency constraints: 31
[64 sec, 1290164 itr]: obj = (31), mov = 1290164, inf = 19.8%, acc = 24.8%, imp = 261

number of satisfied efficiency constraints: 31
[65 sec, 1312202 itr]: obj = (31), mov = 1312202, inf = 19.7%, acc = 24.7%, imp = 261

number of satisfied efficiency constraints: 31
[66 sec, 1331782 itr]: obj = (31), mov = 1331782, inf = 19.6%, acc = 24.7%, imp = 261

number of satisfied efficiency constraints: 31
[67 sec, 1352715 itr]: obj = (31), mov = 1352715, inf = 19.6%, acc = 24.7%, imp = 261

number of satisfied efficiency constraints: 31
[68 sec, 1367725 itr]: obj = (31), mov = 1367725, inf = 19.5%, acc = 24.9%, imp = 294

number of satisfied efficiency constraints: 31
[69 sec, 1381452 itr]: obj = (31), mov = 1381452, inf = 19.5%, acc = 25.1%, imp = 296

number of satisfied efficiency constraints: 31
[70 sec, 1398828 itr]: obj = (31), mov = 1398828, inf = 19.4%, acc = 25.2%, imp = 298

number of satisfied efficiency constraints: 31
[71 sec, 1416314 itr]: obj = (31), mov = 1416314, inf = 19.4%, acc = 25.3%, imp = 298

number of satisfied efficiency constraints: 31
[72 sec, 1435309 itr]: obj = (31), mov = 1435309, inf = 19.3%, acc = 25.3%, imp = 300

number of satisfied efficiency constraints: 31
[73 sec, 1454999 itr]: obj = (31), mov = 1454999, inf = 19.2%, acc = 25.3%, imp = 300

number of satisfied efficiency constraints: 31
[74 sec, 1474073 itr]: obj = (31), mov = 1474073, inf = 19.2%, acc = 25.4%, imp = 300

number of satisfied efficiency constraints: 31
[75 sec, 1494336 itr]: obj = (31), mov = 1494336, inf = 19.1%, acc = 25.4%, imp = 300

number of satisfied efficiency constraints: 31
[76 sec, 1513691 itr]: obj = (31), mov = 1513691, inf = 19.1%, acc = 25.4%, imp = 300

number of satisfied efficiency constraints: 31
[77 sec, 1533589 itr]: obj = (31), mov = 1533589, inf = 19%, acc = 25.4%, imp = 300

number of satisfied efficiency constraints: 31
[78 sec, 1553090 itr]: obj = (31), mov = 1553090, inf = 19%, acc = 25.4%, imp = 300

number of satisfied efficiency constraints: 31
[79 sec, 1573131 itr]: obj = (31), mov = 1573131, inf = 18.9%, acc = 25.4%, imp = 300

number of satisfied efficiency constraints: 31
[80 sec, 1585337 itr]: obj = (31), mov = 1585337, inf = 18.9%, acc = 25.4%, imp = 333

number of satisfied efficiency constraints: 31
[81 sec, 1599849 itr]: obj = (31), mov = 1599849, inf = 18.9%, acc = 25.6%, imp = 335

number of satisfied efficiency constraints: 31
[82 sec, 1614590 itr]: obj = (31), mov = 1614590, inf = 18.8%, acc = 25.7%, imp = 336

number of satisfied efficiency constraints: 31
[83 sec, 1631549 itr]: obj = (31), mov = 1631549, inf = 18.8%, acc = 25.8%, imp = 337

number of satisfied efficiency constraints: 31
[84 sec, 1648269 itr]: obj = (31), mov = 1648269, inf = 18.8%, acc = 25.9%, imp = 338

number of satisfied efficiency constraints: 31
[85 sec, 1666525 itr]: obj = (31), mov = 1666525, inf = 18.7%, acc = 25.9%, imp = 338

number of satisfied efficiency constraints: 31
[86 sec, 1685540 itr]: obj = (31), mov = 1685540, inf = 18.7%, acc = 25.9%, imp = 338

number of satisfied efficiency constraints: 31
[87 sec, 1704998 itr]: obj = (31), mov = 1704998, inf = 18.6%, acc = 26%, imp = 338

number of satisfied efficiency constraints: 31
[88 sec, 1724436 itr]: obj = (31), mov = 1724436, inf = 18.6%, acc = 26%, imp = 338

number of satisfied efficiency constraints: 31
[89 sec, 1742782 itr]: obj = (31), mov = 1742782, inf = 18.5%, acc = 26%, imp = 338

number of satisfied efficiency constraints: 31
[90 sec, 1759815 itr]: obj = (31), mov = 1759815, inf = 18.5%, acc = 26.1%, imp = 338

number of satisfied efficiency constraints: 31
[90 sec, 1759815 itr]: obj = (31), mov = 1759815, inf = 18.5%, acc = 26.1%, imp = 338

number of satisfied efficiency constraints: 31
1759815 iterations, 1759815 moves performed in 90 seconds
Feasible solution: obj = (31)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 31
Mapping: 0->13, 1->44, 2->18, 3->26, 4->15, 5->41, 6->26, 7->24, 8->11, 9->13, 10->2, 11->22, 12->26, 13->38, 14->27, 15->25, 16->47, 17->44, 18->40, 19->29, 20->14, 21->39, 22->45, 23->37, 24->40, 25->8, 26->44, 27->27, 28->16, 29->11, 30->24, 31->28, 32->41, 33->30, 34->21, 35->38, 36->25, 37->30, 38->10, 39->1, 40->47, 41->12, 42->35, 43->23, 44->47, 45->22, 46->7, 47->35, 48->37, 49->0, 50->1, 51->9, 52->25, 53->41, 54->46, 55->19, 56->10, 57->46, 58->41, 59->37, 60->40, 61->6, 62->38, 63->44, 64->17, 65->39, 66->5, 67->9, 68->3, 69->1, 70->5, 71->22, 72->10, 73->7, 74->43, 75->18, 76->15, 77->16, 78->5, 79->26, 80->12, 81->6, 82->0, 83->43, 84->22, 85->20, 86->22, 87->14, 88->28, 89->36, 90->19, 91->37, 92->35, 93->11, 94->32, 95->33, 96->40, 97->12, 98->36, 99->20, 100->23, 101->21, 102->47, 103->31, 104->19, 105->4, 106->38, 107->4, 108->46, 109->2, 110->28, 111->20, 112->7, 113->2, 114->21, 115->30, 116->45, 117->17, 118->33, 119->23, 120->31, 121->3, 122->42, 123->5, 124->6, 125->23, 126->19, 127->34, 128->2, 129->37, 130->46, 131->7, 132->43, 133->36, 134->1, 135->8, 136->34, 137->25, 138->0, 139->18, 140->32, 141->42, 142->38, 143->3, 144->30, 145->3, 146->25, 147->20, 148->46, 149->25, 150->7, 151->5, 152->6, 153->0, 154->13, 155->15, 156->33, 157->40, 158->27, 159->14, 160->14, 161->41, 162->4, 163->42, 164->18, 165->17, 166->27, 167->29, 168->36, 169->8, 170->32, 171->15, 172->3, 173->40, 174->29, 175->2, 176->21, 177->5, 178->3, 179->32, 180->26, 181->19, 182->18, 183->13, 184->43, 185->35, 186->43, 187->15, 188->20, 189->35, 190->12, 191->4, 192->45, 193->45, 194->33, 195->23, 196->10, 197->28, 198->28, 199->39, 200->21, 201->36, 202->32, 203->15, 204->28, 205->34, 206->24, 207->16, 208->44, 209->42, 210->41, 211->21, 212->6, 213->47, 214->24, 215->1, 216->11, 217->4, 218->39, 219->26, 220->9, 221->0, 222->27, 223->17, 224->16, 225->20, 226->39, 227->47, 228->11, 229->42, 230->23, 231->14, 232->22, 233->14, 234->0, 235->4, 236->9, 237->12, 238->38, 239->33, 240->11, 241->10, 242->6, 243->31, 244->2, 245->1, 246->27, 247->13, 248->16, 249->42, 250->43, 251->30, 252->37, 253->8, 254->17, 255->17, 256->29, 257->45, 258->39, 259->8, 260->12, 261->24, 262->18, 263->35, 264->34, 265->34, 266->13, 267->29, 268->16, 269->8, 270->19, 271->32, 272->31, 273->24, 274->31, 275->34, 276->44, 277->9, 278->30, 279->9, 280->7, 281->10, 282->36, 283->33, 284->45, 285->46, 286->29, 287->31
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	2.0852	2.22677	1.78408	1.95336	1.57069	1.47114
	2.03573	4.74369	3.59999	11.6505	5.11498	2.18103
	2.48457	2.17301	2.19343	1.26196	11.2379	2.28309
	2.03289	0.898607	1.2118	1.99168	1.73725	5.57156
	3.99076	1.52978	1.16637	1.91667	12.5446	5.25226
	4.19789	2.09164	2.40999	2.783	14.4996	2.35001
	2.11978	1.22924	1.74819	1.29285	1.64015	2.06488
	4.7766	2.38738	10.3041	12.4055	3.48606	2.31782
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	1	1	1	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 12 --global-tlim 90 --seed 501

Starting the Optimization Process
