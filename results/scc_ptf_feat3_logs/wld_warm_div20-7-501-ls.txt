LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
0.972575, 2.39708, 1.10988, 0.590115, 2.58177, 4.34858, 0.5, 1.03667, 2.36382, 0.612766, 3.57534, 3.91141, 1.53204, 1.85105, 4.67066, 0.5, 0.6393, 2.80695, 13.572, 11.5707, 5.78405, 1.9676, 3.78843, 23.3172, 3.32744, 18.1633, 17.1306, 7.28002, 12.9949, 1.10378, 1.90526, 2.45448, 1.46299, 2.49448, 1.50742, 2.17537, 2.51368, 2.46304, 2.01304, 1.88034, 1.14183, 1.98806, 2.41115, 2.15852, 1.81141, 1.37689, 2.11544, 2.12659, 14.241, 15.1094, 5.59434, 15.7059, 5.7725, 3.57682, 10.6036, 11.2391, 10.6929, 7.39745, 9.05378, 11.0131, 9.26518, 8.19654, 3.98137, 6.80209, 11.9629, 19.7919, 2.29059, 1.7813, 1.95388, 1.60408, 2.19396, 2.17619, 3.01605, 1.42464, 2.53107, 0.629612, 3.18414, 1.21449, 0.858703, 2.41671, 2.0058, 1.96992, 2.39201, 2.35686, 0.906531, 2.81277, 0.641091, 4.01414, 0.512801, 3.11266, 1.29375, 3.07795, 1.05474, 3.58323, 0.5, 2.49033, 0.813519, 2.64727, 2.57569, 0.5, 2.6701, 2.79341, 0.838942, 0.639214, 4.65179, 3.15641, 0.5, 2.21365, 0.822736, 1.76103, 3.18847, 1.62129, 3.12014, 1.48633, 0.856542, 3.11118, 0.734933, 1.39642, 2.67217, 3.22875, 2.33159, 2.2639, 2.43214, 0.610603, 2.72188, 1.63988, 0.838625, 2.88699, 0.5, 4.7624, 1.22551, 1.78647, 2.69998, 0.968234, 2.62558, 2.53529, 2.67092, 0.5, 1.83021, 0.705851, 2.55879, 2.46593, 1.96498, 2.47424, 0.757559, 4.31369, 0.5, 1.76552, 4.10794, 0.555287, 0.948601, 2.08102, 2.35583, 2.07851, 2.32456, 2.21146, 3.39997, 0.5, 2.10716, 1.45376, 2.94558, 1.59353, 1.84764, 1.28335, 6.56344, 0.5, 1.24181, 0.563758, 0.5, 3.44047, 1.82506, 1.52724, 3.49939, 1.20783, 0.5, 2.74182, 2.42425, 2.52648, 1.167, 2.64046, 1.58248, 4.07144, 0.672312, 0.5, 4.6308, 0.542965, 3.10362, 2.06947, 0.5, 0.636549, 2.62714, 3.06321, 1.7173, 2.11514, 0.653563, 3.4178, 1.74094, 2.35526, 2.27769, 1.52815, 1.97292, 2.64313, 1.15193, 2.42618, 2.36577, 1.7214, 0.530125, 2.67858, 2.26666, 2.43746, 2.29013, 2.83215, 1.9097, 2.04107, 1.27906, 1.64788, 1.13966, 4.91036, 2.18005, 1.36552, 1.85803, 0.546382, 1.86867, 2.12365, 1.72341, 1.64876, 2.39067, 2.24484, 2.67214, 2.48739, 2.3493, 1.05876, 2.06872, 1.36369, 2.23285, 2.10667, 1.6163, 2.10851, 1.89827, 2.0374, 1.19725, 3.74648, 1.12694, 1.76902, 0.5, 3.6603, 2.25271, 1.96708, 1.64466, 1.87493, 1.98718, 2.27345, 1.27521, 0.5, 2.63387, 2.36776, 2.66633, 2.55683, 1.63328, 1.66022, 0.770267, 3.53181, 0.677605, 3.72681, 2.49276, 2.95295, 2.51765, 1.08754, 0.5, 2.4491, 1.9564, 0.5, 3.14584, 2.26871, 0.660473, 3.46857, 0.975609, 1.64126, 2.93782, 2.28308, 1.04524, 3.117, 1.79099, 0.5, 2.6142, 1.96334, 2.28364, 2.84784
cpi min/max: 0.5/23.3172
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55169, operands = 136540
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 501, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 17620 itr]: obj = (23), mov = 17620, inf = 72.9%, acc = 19.6%, imp = 159

number of satisfied efficiency constraints: 23
[2 sec, 49504 itr]: obj = (25), mov = 49504, inf = 76.2%, acc = 16.8%, imp = 161

number of satisfied efficiency constraints: 25
[3 sec, 70412 itr]: obj = (25), mov = 70412, inf = 71.7%, acc = 19.4%, imp = 161

number of satisfied efficiency constraints: 25
[4 sec, 85642 itr]: obj = (25), mov = 85642, inf = 66.7%, acc = 22.4%, imp = 161

number of satisfied efficiency constraints: 25
[5 sec, 99768 itr]: obj = (25), mov = 99768, inf = 62.4%, acc = 25.1%, imp = 161

number of satisfied efficiency constraints: 25
[6 sec, 116413 itr]: obj = (25), mov = 116413, inf = 57.2%, acc = 28.5%, imp = 161

number of satisfied efficiency constraints: 25
[7 sec, 134520 itr]: obj = (25), mov = 134520, inf = 52.4%, acc = 31.7%, imp = 161

number of satisfied efficiency constraints: 25
[8 sec, 151854 itr]: obj = (26), mov = 151854, inf = 48.6%, acc = 34.2%, imp = 187

number of satisfied efficiency constraints: 26
[9 sec, 170140 itr]: obj = (26), mov = 170140, inf = 45.2%, acc = 35.7%, imp = 187

number of satisfied efficiency constraints: 26
[10 sec, 187603 itr]: obj = (26), mov = 187603, inf = 42.4%, acc = 37%, imp = 187

number of satisfied efficiency constraints: 26
[11 sec, 205314 itr]: obj = (26), mov = 205314, inf = 40.1%, acc = 38%, imp = 187

number of satisfied efficiency constraints: 26
[12 sec, 223002 itr]: obj = (26), mov = 223002, inf = 38.2%, acc = 39%, imp = 187

number of satisfied efficiency constraints: 26
[13 sec, 240675 itr]: obj = (26), mov = 240675, inf = 36.5%, acc = 39.8%, imp = 187

number of satisfied efficiency constraints: 26
[14 sec, 256708 itr]: obj = (26), mov = 256708, inf = 35.2%, acc = 40.8%, imp = 213

number of satisfied efficiency constraints: 26
[15 sec, 272158 itr]: obj = (26), mov = 272158, inf = 34%, acc = 41.7%, imp = 214

number of satisfied efficiency constraints: 26
[16 sec, 288166 itr]: obj = (26), mov = 288166, inf = 32.8%, acc = 42.4%, imp = 215

number of satisfied efficiency constraints: 26
[17 sec, 305619 itr]: obj = (26), mov = 305619, inf = 31.8%, acc = 42.8%, imp = 215

number of satisfied efficiency constraints: 26
[18 sec, 323086 itr]: obj = (26), mov = 323086, inf = 30.9%, acc = 43.2%, imp = 215

number of satisfied efficiency constraints: 26
[19 sec, 340480 itr]: obj = (26), mov = 340480, inf = 30%, acc = 43.5%, imp = 215

number of satisfied efficiency constraints: 26
[20 sec, 357477 itr]: obj = (26), mov = 357477, inf = 29.3%, acc = 43.9%, imp = 215

number of satisfied efficiency constraints: 26
[21 sec, 373041 itr]: obj = (26), mov = 373041, inf = 28.6%, acc = 44.4%, imp = 244

number of satisfied efficiency constraints: 26
[22 sec, 388747 itr]: obj = (26), mov = 388747, inf = 28%, acc = 44.8%, imp = 244

number of satisfied efficiency constraints: 26
[23 sec, 404451 itr]: obj = (26), mov = 404451, inf = 27.4%, acc = 45.2%, imp = 244

number of satisfied efficiency constraints: 26
[24 sec, 420223 itr]: obj = (26), mov = 420223, inf = 26.8%, acc = 45.5%, imp = 244

number of satisfied efficiency constraints: 26
[25 sec, 436350 itr]: obj = (26), mov = 436350, inf = 26.3%, acc = 45.8%, imp = 244

number of satisfied efficiency constraints: 26
[26 sec, 452956 itr]: obj = (26), mov = 452956, inf = 25.8%, acc = 46%, imp = 245

number of satisfied efficiency constraints: 26
[27 sec, 470509 itr]: obj = (26), mov = 470509, inf = 25.4%, acc = 46.1%, imp = 245

number of satisfied efficiency constraints: 26
[28 sec, 487066 itr]: obj = (26), mov = 487066, inf = 25%, acc = 46.3%, imp = 245

number of satisfied efficiency constraints: 26
[29 sec, 503666 itr]: obj = (26), mov = 503666, inf = 24.6%, acc = 46.4%, imp = 245

number of satisfied efficiency constraints: 26
[30 sec, 520505 itr]: obj = (26), mov = 520505, inf = 24.3%, acc = 46.6%, imp = 245

number of satisfied efficiency constraints: 26
[31 sec, 537112 itr]: obj = (26), mov = 537112, inf = 23.9%, acc = 46.6%, imp = 245

number of satisfied efficiency constraints: 26
[32 sec, 554012 itr]: obj = (27), mov = 554012, inf = 23.6%, acc = 46.7%, imp = 246

number of satisfied efficiency constraints: 27
[33 sec, 572274 itr]: obj = (27), mov = 572274, inf = 23.3%, acc = 46.7%, imp = 246

number of satisfied efficiency constraints: 27
[34 sec, 590631 itr]: obj = (27), mov = 590631, inf = 23%, acc = 46.6%, imp = 246

number of satisfied efficiency constraints: 27
[35 sec, 609138 itr]: obj = (27), mov = 609138, inf = 22.8%, acc = 46.5%, imp = 246

number of satisfied efficiency constraints: 27
[36 sec, 627110 itr]: obj = (27), mov = 627110, inf = 22.6%, acc = 46.5%, imp = 246

number of satisfied efficiency constraints: 27
[37 sec, 644733 itr]: obj = (27), mov = 644733, inf = 22.3%, acc = 46.5%, imp = 246

number of satisfied efficiency constraints: 27
[38 sec, 663243 itr]: obj = (27), mov = 663243, inf = 22.1%, acc = 46.4%, imp = 246

number of satisfied efficiency constraints: 27
[39 sec, 681273 itr]: obj = (27), mov = 681273, inf = 21.9%, acc = 46.4%, imp = 246

number of satisfied efficiency constraints: 27
[40 sec, 699604 itr]: obj = (27), mov = 699604, inf = 21.6%, acc = 46.3%, imp = 246

number of satisfied efficiency constraints: 27
[41 sec, 717920 itr]: obj = (27), mov = 717920, inf = 21.5%, acc = 46.3%, imp = 246

number of satisfied efficiency constraints: 27
[42 sec, 736311 itr]: obj = (27), mov = 736311, inf = 21.2%, acc = 46.2%, imp = 246

number of satisfied efficiency constraints: 27
[43 sec, 754995 itr]: obj = (27), mov = 754995, inf = 21.1%, acc = 46.1%, imp = 246

number of satisfied efficiency constraints: 27
[44 sec, 773193 itr]: obj = (27), mov = 773193, inf = 20.9%, acc = 46.1%, imp = 246

number of satisfied efficiency constraints: 27
[45 sec, 791080 itr]: obj = (27), mov = 791080, inf = 20.7%, acc = 46.1%, imp = 246

number of satisfied efficiency constraints: 27
[46 sec, 808876 itr]: obj = (27), mov = 808876, inf = 20.6%, acc = 46.1%, imp = 246

number of satisfied efficiency constraints: 27
[47 sec, 827423 itr]: obj = (27), mov = 827423, inf = 20.4%, acc = 46.1%, imp = 246

number of satisfied efficiency constraints: 27
[48 sec, 846056 itr]: obj = (27), mov = 846056, inf = 20.3%, acc = 46%, imp = 246

number of satisfied efficiency constraints: 27
[49 sec, 864044 itr]: obj = (27), mov = 864044, inf = 20.1%, acc = 46%, imp = 246

number of satisfied efficiency constraints: 27
[50 sec, 882601 itr]: obj = (27), mov = 882601, inf = 20%, acc = 45.9%, imp = 246

number of satisfied efficiency constraints: 27
[51 sec, 900745 itr]: obj = (27), mov = 900745, inf = 19.9%, acc = 45.9%, imp = 246

number of satisfied efficiency constraints: 27
[52 sec, 918582 itr]: obj = (27), mov = 918582, inf = 19.8%, acc = 45.9%, imp = 246

number of satisfied efficiency constraints: 27
[53 sec, 936690 itr]: obj = (27), mov = 936690, inf = 19.7%, acc = 45.9%, imp = 246

number of satisfied efficiency constraints: 27
[54 sec, 954514 itr]: obj = (27), mov = 954514, inf = 19.6%, acc = 45.9%, imp = 246

number of satisfied efficiency constraints: 27
[55 sec, 973155 itr]: obj = (27), mov = 973155, inf = 19.5%, acc = 45.8%, imp = 246

number of satisfied efficiency constraints: 27
[56 sec, 988078 itr]: obj = (27), mov = 988078, inf = 19.4%, acc = 46%, imp = 276

number of satisfied efficiency constraints: 27
[57 sec, 1003441 itr]: obj = (27), mov = 1003441, inf = 19.3%, acc = 46.1%, imp = 276

number of satisfied efficiency constraints: 27
[58 sec, 1019629 itr]: obj = (27), mov = 1019629, inf = 19.2%, acc = 46.2%, imp = 276

number of satisfied efficiency constraints: 27
[59 sec, 1035327 itr]: obj = (27), mov = 1035327, inf = 19.1%, acc = 46.3%, imp = 276

number of satisfied efficiency constraints: 27
[60 sec, 1050899 itr]: obj = (27), mov = 1050899, inf = 19%, acc = 46.4%, imp = 276

number of satisfied efficiency constraints: 27
[61 sec, 1066678 itr]: obj = (27), mov = 1066678, inf = 18.9%, acc = 46.6%, imp = 276

number of satisfied efficiency constraints: 27
[62 sec, 1082037 itr]: obj = (27), mov = 1082037, inf = 18.9%, acc = 46.7%, imp = 276

number of satisfied efficiency constraints: 27
[63 sec, 1096482 itr]: obj = (27), mov = 1096482, inf = 18.8%, acc = 46.8%, imp = 306

number of satisfied efficiency constraints: 27
[64 sec, 1112219 itr]: obj = (27), mov = 1112219, inf = 18.7%, acc = 46.9%, imp = 307

number of satisfied efficiency constraints: 27
[65 sec, 1128404 itr]: obj = (27), mov = 1128404, inf = 18.6%, acc = 47%, imp = 307

number of satisfied efficiency constraints: 27
[66 sec, 1144079 itr]: obj = (27), mov = 1144079, inf = 18.6%, acc = 47%, imp = 308

number of satisfied efficiency constraints: 27
[67 sec, 1161225 itr]: obj = (27), mov = 1161225, inf = 18.5%, acc = 47%, imp = 309

number of satisfied efficiency constraints: 27
[68 sec, 1179137 itr]: obj = (27), mov = 1179137, inf = 18.4%, acc = 47%, imp = 309

number of satisfied efficiency constraints: 27
[69 sec, 1196714 itr]: obj = (27), mov = 1196714, inf = 18.3%, acc = 47%, imp = 309

number of satisfied efficiency constraints: 27
[70 sec, 1214269 itr]: obj = (27), mov = 1214269, inf = 18.2%, acc = 46.9%, imp = 309

number of satisfied efficiency constraints: 27
[71 sec, 1232265 itr]: obj = (27), mov = 1232265, inf = 18.2%, acc = 46.9%, imp = 309

number of satisfied efficiency constraints: 27
[72 sec, 1250081 itr]: obj = (27), mov = 1250081, inf = 18.1%, acc = 46.9%, imp = 309

number of satisfied efficiency constraints: 27
[73 sec, 1267870 itr]: obj = (27), mov = 1267870, inf = 18%, acc = 46.8%, imp = 309

number of satisfied efficiency constraints: 27
[74 sec, 1285698 itr]: obj = (27), mov = 1285698, inf = 18%, acc = 46.8%, imp = 309

number of satisfied efficiency constraints: 27
[75 sec, 1303780 itr]: obj = (27), mov = 1303780, inf = 17.9%, acc = 46.7%, imp = 309

number of satisfied efficiency constraints: 27
[76 sec, 1320520 itr]: obj = (27), mov = 1320520, inf = 17.8%, acc = 46.8%, imp = 341

number of satisfied efficiency constraints: 27
[77 sec, 1337037 itr]: obj = (27), mov = 1337037, inf = 17.8%, acc = 46.8%, imp = 342

number of satisfied efficiency constraints: 27
[78 sec, 1353400 itr]: obj = (27), mov = 1353400, inf = 17.7%, acc = 46.8%, imp = 342

number of satisfied efficiency constraints: 27
[79 sec, 1369580 itr]: obj = (27), mov = 1369580, inf = 17.7%, acc = 46.8%, imp = 342

number of satisfied efficiency constraints: 27
[80 sec, 1386382 itr]: obj = (27), mov = 1386382, inf = 17.6%, acc = 46.9%, imp = 342

number of satisfied efficiency constraints: 27
[81 sec, 1403201 itr]: obj = (27), mov = 1403201, inf = 17.6%, acc = 46.9%, imp = 342

number of satisfied efficiency constraints: 27
[82 sec, 1419560 itr]: obj = (27), mov = 1419560, inf = 17.5%, acc = 46.9%, imp = 342

number of satisfied efficiency constraints: 27
[83 sec, 1434902 itr]: obj = (27), mov = 1434902, inf = 17.5%, acc = 47%, imp = 373

number of satisfied efficiency constraints: 27
[84 sec, 1449988 itr]: obj = (27), mov = 1449988, inf = 17.4%, acc = 47.1%, imp = 373

number of satisfied efficiency constraints: 27
[85 sec, 1465252 itr]: obj = (27), mov = 1465252, inf = 17.4%, acc = 47.1%, imp = 373

number of satisfied efficiency constraints: 27
[86 sec, 1480585 itr]: obj = (27), mov = 1480585, inf = 17.3%, acc = 47.2%, imp = 373

number of satisfied efficiency constraints: 27
[87 sec, 1495708 itr]: obj = (27), mov = 1495708, inf = 17.3%, acc = 47.2%, imp = 373

number of satisfied efficiency constraints: 27
[88 sec, 1510895 itr]: obj = (27), mov = 1510895, inf = 17.2%, acc = 47.3%, imp = 373

number of satisfied efficiency constraints: 27
[89 sec, 1526469 itr]: obj = (27), mov = 1526469, inf = 17.2%, acc = 47.4%, imp = 374

number of satisfied efficiency constraints: 27
[90 sec, 1542753 itr]: obj = (27), mov = 1542753, inf = 17.1%, acc = 47.4%, imp = 374

number of satisfied efficiency constraints: 27
[90 sec, 1542753 itr]: obj = (27), mov = 1542753, inf = 17.1%, acc = 47.4%, imp = 374

number of satisfied efficiency constraints: 27
1542753 iterations, 1542753 moves performed in 90 seconds
Feasible solution: obj = (27)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 27
Mapping: 0->45, 1->1, 2->36, 3->35, 4->41, 5->11, 6->32, 7->38, 8->23, 9->38, 10->46, 11->0, 12->47, 13->5, 14->8, 15->37, 16->9, 17->1, 18->47, 19->41, 20->26, 21->39, 22->36, 23->16, 24->19, 25->18, 26->8, 27->30, 28->40, 29->0, 30->45, 31->7, 32->28, 33->1, 34->25, 35->23, 36->34, 37->28, 38->27, 39->0, 40->33, 41->42, 42->13, 43->37, 44->10, 45->15, 46->12, 47->22, 48->35, 49->16, 50->24, 51->16, 52->46, 53->40, 54->3, 55->6, 56->7, 57->29, 58->9, 59->1, 60->14, 61->4, 62->28, 63->23, 64->7, 65->20, 66->23, 67->24, 68->10, 69->15, 70->25, 71->5, 72->16, 73->12, 74->19, 75->5, 76->16, 77->30, 78->19, 79->9, 80->45, 81->3, 82->39, 83->44, 84->29, 85->3, 86->5, 87->40, 88->13, 89->21, 90->33, 91->43, 92->38, 93->10, 94->14, 95->46, 96->14, 97->40, 98->18, 99->14, 100->36, 101->22, 102->39, 103->20, 104->11, 105->11, 106->37, 107->26, 108->15, 109->37, 110->41, 111->34, 112->42, 113->34, 114->44, 115->9, 116->33, 117->14, 118->7, 119->20, 120->18, 121->33, 122->35, 123->27, 124->24, 125->32, 126->32, 127->43, 128->21, 129->10, 130->15, 131->15, 132->21, 133->11, 134->37, 135->38, 136->43, 137->22, 138->31, 139->2, 140->42, 141->46, 142->27, 143->36, 144->3, 145->46, 146->11, 147->26, 148->13, 149->31, 150->27, 151->36, 152->30, 153->47, 154->2, 155->3, 156->19, 157->4, 158->25, 159->36, 160->4, 161->14, 162->30, 163->9, 164->22, 165->13, 166->12, 167->47, 168->45, 169->8, 170->20, 171->26, 172->40, 173->18, 174->18, 175->2, 176->2, 177->12, 178->0, 179->18, 180->34, 181->17, 182->6, 183->29, 184->2, 185->39, 186->37, 187->27, 188->31, 189->13, 190->43, 191->40, 192->17, 193->32, 194->28, 195->12, 196->5, 197->24, 198->38, 199->25, 200->11, 201->4, 202->3, 203->10, 204->17, 205->20, 206->13, 207->9, 208->46, 209->22, 210->23, 211->44, 212->35, 213->25, 214->23, 215->17, 216->6, 217->41, 218->8, 219->31, 220->29, 221->41, 222->47, 223->8, 224->42, 225->7, 226->28, 227->17, 228->6, 229->32, 230->10, 231->1, 232->17, 233->1, 234->29, 235->15, 236->29, 237->34, 238->28, 239->26, 240->35, 241->45, 242->33, 243->19, 244->34, 245->31, 246->21, 247->26, 248->41, 249->6, 250->7, 251->44, 252->21, 253->32, 254->43, 255->47, 256->4, 257->42, 258->4, 259->35, 260->30, 261->43, 262->30, 263->25, 264->5, 265->0, 266->12, 267->33, 268->6, 269->8, 270->21, 271->20, 272->39, 273->0, 274->39, 275->22, 276->44, 277->19, 278->42, 279->16, 280->44, 281->38, 282->27, 283->24, 284->24, 285->31, 286->2, 287->45
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	2.21403	3.52235	2.51849	3.25121	3.09748	1.58861
	3.01635	5.23641	5.33242	3.19715	2.8144	2.59966
	2.2073	1.44976	2.34477	1.48706	10.436	2.35266
	4.56981	2.25458	4.61772	1.96616	2.98158	2.86685
	2.5945	2.18409	2.54916	1.56628	2.14165	2.41852
	2.35764	1.64578	1.34684	1.27481	1.63538	3.67174
	2.26291	1.77479	1.77236	1.59131	4.96595	4.07372
	2.48084	2.90478	1.72331	1.99633	3.48074	3.66379
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	0	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 7 --global-tlim 90 --seed 501

Starting the Optimization Process
