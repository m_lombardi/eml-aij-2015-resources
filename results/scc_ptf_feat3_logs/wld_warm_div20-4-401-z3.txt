==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 4 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 401

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 1.837854
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
- lns time: 2.004500
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 9
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.404/4.247
- lns time: 0.404372
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.083/5.330
- lns time: 1.083333
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
- lns time: 2.005145
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 10
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.324/8.659
- lns time: 1.324030
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.013177
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.004523
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.009293
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.007455
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.006098
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 1.228538
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.004939
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005029
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005124
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005676
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005247
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 0.869505
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.008802
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.013182
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005385
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.014412
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.006281
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.004837
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
- lns time: 2.005322
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 11
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.857/46.739
- lns time: 1.856861
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 1.181898
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.005469
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.004839
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 1.073987
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.008588
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.006333
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.682/57.702
- lns time: 0.682507
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 2.006565
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 2.009965
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 2.008888
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 1.106453
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 1.979700
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 1.951742
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.644/69.410
- lns time: 0.644594
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.004645
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.007865
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 0.940878
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005019
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.005037
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.009205
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
- lns time: 2.004798
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.644/83.032
- lns time: 0.644418
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.002/84.035
- lns time: 1.002432
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.760/84.795
- lns time: 0.760513
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.004933
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.004652
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 0.987222
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 0.212459
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 18

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 9
final number of satisfied efficiency constraints: 18
number of iterations: 54
total time: 90.004524

Final Solution: 0->12, 1->43, 2->34, 3->7, 4->47, 5->6, 6->44, 7->27, 8->11, 9->0, 10->47, 11->45, 12->34, 13->19, 14->3, 15->18, 16->6, 17->32, 18->20, 19->29, 20->23, 21->17, 22->46, 23->20, 24->9, 25->44, 26->19, 27->5, 28->21, 29->35, 30->4, 31->44, 32->15, 33->16, 34->12, 35->34, 36->4, 37->44, 38->16, 39->44, 40->35, 41->0, 42->1, 43->38, 44->2, 45->19, 46->11, 47->31, 48->28, 49->44, 50->25, 51->6, 52->31, 53->3, 54->12, 55->22, 56->39, 57->5, 58->40, 59->31, 60->32, 61->11, 62->28, 63->40, 64->37, 65->5, 66->42, 67->10, 68->42, 69->25, 70->7, 71->17, 72->18, 73->41, 74->10, 75->45, 76->8, 77->9, 78->21, 79->32, 80->21, 81->9, 82->1, 83->3, 84->23, 85->33, 86->27, 87->22, 88->47, 89->42, 90->13, 91->8, 92->0, 93->29, 94->9, 95->42, 96->17, 97->28, 98->31, 99->40, 100->14, 101->5, 102->25, 103->24, 104->16, 105->30, 106->0, 107->41, 108->25, 109->13, 110->17, 111->10, 112->46, 113->4, 114->36, 115->28, 116->15, 117->26, 118->45, 119->13, 120->41, 121->4, 122->8, 123->6, 124->23, 125->35, 126->45, 127->5, 128->24, 129->13, 130->37, 131->31, 132->35, 133->26, 134->17, 135->17, 136->26, 137->36, 138->24, 139->9, 140->18, 141->14, 142->18, 143->22, 144->1, 145->39, 146->46, 147->23, 148->15, 149->26, 150->25, 151->39, 152->8, 153->1, 154->38, 155->0, 156->12, 157->0, 158->4, 159->41, 160->33, 161->16, 162->43, 163->39, 164->11, 165->12, 166->1, 167->37, 168->20, 169->9, 170->31, 171->30, 172->8, 173->2, 174->18, 175->18, 176->29, 177->38, 178->30, 179->36, 180->23, 181->27, 182->14, 183->19, 184->37, 185->27, 186->45, 187->26, 188->22, 189->21, 190->15, 191->20, 192->7, 193->2, 194->43, 195->13, 196->39, 197->37, 198->4, 199->30, 200->21, 201->11, 202->12, 203->34, 204->42, 205->3, 206->16, 207->40, 208->22, 209->47, 210->36, 211->10, 212->33, 213->46, 214->19, 215->38, 216->3, 217->37, 218->28, 219->29, 220->46, 221->2, 222->43, 223->14, 224->21, 225->7, 226->45, 227->27, 228->32, 229->30, 230->16, 231->7, 232->39, 233->29, 234->1, 235->2, 236->26, 237->35, 238->14, 239->10, 240->28, 241->34, 242->14, 243->33, 244->20, 245->43, 246->43, 247->20, 248->11, 249->40, 250->41, 251->24, 252->15, 253->42, 254->35, 255->19, 256->2, 257->3, 258->38, 259->23, 260->24, 261->32, 262->7, 263->36, 264->24, 265->32, 266->47, 267->33, 268->41, 269->46, 270->25, 271->8, 272->30, 273->13, 274->15, 275->10, 276->34, 277->47, 278->36, 279->29, 280->6, 281->5, 282->33, 283->27, 284->22, 285->40, 286->38, 287->6

Average CPI map:
	0.77,	2.05,	1.54,	2.33,	2.36,	0.83
	2.30,	2.07,	2.06,	2.06,	2.06,	2.06
	2.53,	2.03,	2.00,	1.99,	1.44,	2.26
	2.23,	1.83,	1.95,	1.95,	1.70,	2.04
	2.05,	1.94,	1.94,	3.37,	1.94,	2.21
	2.68,	1.76,	1.94,	1.94,	1.93,	2.09
	2.50,	1.93,	1.93,	1.93,	1.93,	1.93
	2.50,	1.92,	0.82,	2.54,	1.92,	1.92

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	0
