==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 301

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005843
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004506
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004966
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004882
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004789
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005683
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007053
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 0.882342
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009009
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 0.829840
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.010053
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004544
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005311
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.360/25.138
- lns time: 1.359647
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005216
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.774/27.918
- lns time: 0.774132
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.014018
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.660/30.592
- lns time: 0.660340
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007517
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004997
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004736
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 1.017183
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005976
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008978
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004799
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006836
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005065
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004960
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005332
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006160
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004571
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.130/56.810
- lns time: 1.130511
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.003630
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005338
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.683316
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009232
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.011306
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005931
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008648
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004894
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010103
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005596
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005261
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005283
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006326
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004659
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005340
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005287
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.416455
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 23

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 23
number of iterations: 49
total time: 90.006402

Final Solution: 0->25, 1->26, 2->0, 3->37, 4->23, 5->23, 6->46, 7->37, 8->27, 9->26, 10->23, 11->21, 12->36, 13->30, 14->14, 15->11, 16->0, 17->21, 18->16, 19->24, 20->25, 21->32, 22->1, 23->14, 24->37, 25->15, 26->14, 27->22, 28->35, 29->43, 30->2, 31->0, 32->11, 33->1, 34->34, 35->5, 36->29, 37->18, 38->1, 39->41, 40->17, 41->25, 42->24, 43->36, 44->42, 45->45, 46->36, 47->20, 48->36, 49->2, 50->44, 51->15, 52->38, 53->27, 54->43, 55->9, 56->38, 57->12, 58->46, 59->39, 60->45, 61->10, 62->22, 63->16, 64->11, 65->42, 66->24, 67->16, 68->3, 69->5, 70->20, 71->11, 72->19, 73->40, 74->3, 75->17, 76->14, 77->23, 78->47, 79->0, 80->8, 81->4, 82->7, 83->31, 84->5, 85->22, 86->4, 87->20, 88->38, 89->33, 90->46, 91->37, 92->44, 93->29, 94->18, 95->39, 96->42, 97->32, 98->7, 99->47, 100->2, 101->19, 102->20, 103->42, 104->7, 105->3, 106->18, 107->14, 108->9, 109->21, 110->10, 111->30, 112->27, 113->33, 114->17, 115->27, 116->28, 117->15, 118->44, 119->30, 120->8, 121->35, 122->24, 123->1, 124->45, 125->46, 126->8, 127->26, 128->6, 129->7, 130->5, 131->35, 132->26, 133->33, 134->41, 135->34, 136->31, 137->43, 138->8, 139->25, 140->32, 141->18, 142->24, 143->40, 144->33, 145->35, 146->2, 147->23, 148->17, 149->3, 150->9, 151->14, 152->24, 153->29, 154->18, 155->4, 156->27, 157->47, 158->31, 159->46, 160->30, 161->26, 162->39, 163->43, 164->12, 165->6, 166->4, 167->10, 168->12, 169->17, 170->42, 171->13, 172->18, 173->45, 174->28, 175->19, 176->31, 177->35, 178->9, 179->31, 180->27, 181->10, 182->32, 183->12, 184->20, 185->10, 186->11, 187->25, 188->13, 189->21, 190->15, 191->19, 192->16, 193->45, 194->40, 195->47, 196->44, 197->1, 198->36, 199->44, 200->9, 201->40, 202->21, 203->12, 204->4, 205->23, 206->31, 207->41, 208->13, 209->6, 210->29, 211->22, 212->39, 213->15, 214->39, 215->34, 216->29, 217->38, 218->16, 219->3, 220->41, 221->26, 222->7, 223->0, 224->8, 225->6, 226->28, 227->38, 228->19, 229->30, 230->34, 231->8, 232->42, 233->12, 234->47, 235->9, 236->25, 237->43, 238->21, 239->34, 240->36, 241->11, 242->28, 243->32, 244->13, 245->41, 246->5, 247->35, 248->10, 249->22, 250->16, 251->6, 252->1, 253->47, 254->13, 255->41, 256->28, 257->19, 258->6, 259->7, 260->43, 261->40, 262->46, 263->33, 264->38, 265->13, 266->2, 267->37, 268->3, 269->22, 270->28, 271->4, 272->33, 273->5, 274->37, 275->15, 276->40, 277->29, 278->20, 279->0, 280->44, 281->30, 282->17, 283->45, 284->34, 285->2, 286->32, 287->39

Average CPI map:
	1.62,	1.43,	4.31,	4.15,	3.37,	2.00
	3.81,	3.83,	3.61,	3.58,	6.64,	2.84
	3.42,	3.41,	3.39,	3.37,	4.57,	3.15
	3.23,	3.21,	3.20,	3.08,	1.56,	3.08
	2.95,	2.69,	2.63,	2.52,	2.49,	2.49
	2.45,	2.46,	2.46,	2.43,	2.38,	2.34
	2.02,	2.31,	8.45,	2.28,	2.27,	3.40
	2.24,	2.64,	1.83,	2.22,	2.18,	2.00

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
