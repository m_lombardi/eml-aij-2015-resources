==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 6 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 701

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008381
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007648
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005075
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.035/7.056
- lns time: 1.035158
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.997/9.054
- lns time: 1.997508
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004541
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005414
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.927/13.990
- lns time: 0.926633
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010827
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004940
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008660
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004589
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.852/23.871
- lns time: 1.852095
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.817/24.689
- lns time: 0.817507
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005301
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008117
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.004710
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.006657
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.011932
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.714/36.440
- lns time: 1.713945
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008782
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.978/39.426
- lns time: 0.978221
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.994/41.421
- lns time: 1.994127
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.005896
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.672/44.099
- lns time: 0.672415
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.887/44.986
- lns time: 0.886727
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.014628
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.014239
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.010070
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.004991
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.004691
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.005177
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007086
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007879
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.005775
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.004691
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.006556
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.005443
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009341
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.004735
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.004219
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007586
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.006629
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.006607
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.005561
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.005155
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007784
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 1.466421
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 1.405733
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 30

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 30
number of iterations: 49
total time: 90.006803

Final Solution: 0->19, 1->0, 2->16, 3->22, 4->40, 5->34, 6->0, 7->1, 8->28, 9->8, 10->1, 11->26, 12->25, 13->40, 14->46, 15->35, 16->32, 17->47, 18->20, 19->42, 20->20, 21->47, 22->23, 23->40, 24->39, 25->35, 26->36, 27->47, 28->33, 29->11, 30->7, 31->28, 32->30, 33->24, 34->11, 35->3, 36->38, 37->2, 38->42, 39->4, 40->45, 41->36, 42->10, 43->8, 44->43, 45->0, 46->13, 47->41, 48->26, 49->4, 50->20, 51->34, 52->5, 53->2, 54->5, 55->12, 56->5, 57->13, 58->15, 59->42, 60->9, 61->43, 62->14, 63->23, 64->28, 65->34, 66->41, 67->13, 68->2, 69->16, 70->17, 71->19, 72->30, 73->41, 74->35, 75->18, 76->21, 77->5, 78->40, 79->32, 80->4, 81->31, 82->2, 83->1, 84->37, 85->41, 86->38, 87->19, 88->6, 89->6, 90->33, 91->9, 92->29, 93->10, 94->4, 95->14, 96->36, 97->20, 98->42, 99->18, 100->3, 101->38, 102->2, 103->17, 104->18, 105->43, 106->7, 107->30, 108->21, 109->40, 110->27, 111->3, 112->45, 113->44, 114->42, 115->41, 116->17, 117->32, 118->39, 119->13, 120->24, 121->19, 122->21, 123->13, 124->6, 125->28, 126->31, 127->29, 128->39, 129->44, 130->32, 131->24, 132->12, 133->12, 134->21, 135->25, 136->38, 137->10, 138->26, 139->0, 140->9, 141->15, 142->12, 143->25, 144->25, 145->8, 146->9, 147->9, 148->22, 149->38, 150->31, 151->22, 152->1, 153->6, 154->36, 155->25, 156->31, 157->15, 158->45, 159->37, 160->18, 161->19, 162->39, 163->30, 164->26, 165->32, 166->7, 167->41, 168->23, 169->29, 170->43, 171->15, 172->6, 173->37, 174->28, 175->16, 176->14, 177->15, 178->40, 179->11, 180->46, 181->37, 182->35, 183->10, 184->36, 185->12, 186->1, 187->22, 188->45, 189->22, 190->45, 191->28, 192->29, 193->17, 194->37, 195->45, 196->26, 197->33, 198->26, 199->16, 200->43, 201->23, 202->35, 203->6, 204->23, 205->43, 206->34, 207->46, 208->11, 209->1, 210->39, 211->3, 212->27, 213->30, 214->34, 215->31, 216->21, 217->36, 218->24, 219->11, 220->17, 221->11, 222->18, 223->8, 224->47, 225->18, 226->44, 227->33, 228->14, 229->44, 230->9, 231->13, 232->5, 233->4, 234->10, 235->35, 236->12, 237->27, 238->7, 239->0, 240->7, 241->38, 242->46, 243->24, 244->30, 245->32, 246->19, 247->47, 248->20, 249->16, 250->29, 251->33, 252->20, 253->27, 254->21, 255->27, 256->25, 257->0, 258->4, 259->3, 260->31, 261->8, 262->15, 263->17, 264->16, 265->14, 266->8, 267->46, 268->34, 269->42, 270->44, 271->44, 272->33, 273->14, 274->27, 275->37, 276->7, 277->22, 278->46, 279->47, 280->5, 281->10, 282->23, 283->24, 284->39, 285->3, 286->29, 287->2

Average CPI map:
	0.97,	7.56,	2.02,	7.53,	1.82,	0.69
	4.53,	2.10,	5.48,	10.14,	6.51,	1.79
	6.38,	12.99,	2.02,	6.00,	1.00,	4.78
	5.72,	12.47,	1.46,	10.95,	10.80,	2.61
	5.32,	5.29,	5.27,	5.10,	7.53,	4.48
	2.08,	4.70,	7.49,	4.65,	1.50,	6.61
	4.31,	12.17,	3.95,	3.74,	3.54,	5.62
	4.24,	2.60,	3.23,	19.12,	2.59,	2.53

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	1,	0,	1
	1,	1,	0,	0,	0,	1
	1,	1,	0,	1,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
	1,	1,	0,	1,	0,	1
