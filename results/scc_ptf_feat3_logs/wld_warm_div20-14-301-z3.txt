==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 14 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 301

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.010933
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 1.016663
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005931
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006765
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009197
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007997
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005852
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 1.023060
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006953
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.010378
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006542
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 0.540798
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.013081
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006733
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 1.838761
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006611
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007131
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.007933
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008362
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.009174
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006289
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.010696
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.750/42.316
- lns time: 1.749885
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007518
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005570
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 1.120125
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005653
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004659
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008983
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.011278
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005799
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.822/58.307
- lns time: 0.822254
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006323
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.641/60.955
- lns time: 0.641579
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006368
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006969
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007874
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.007936
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.872/70.857
- lns time: 1.872182
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009585
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008570
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005735
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005974
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.014544
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007770
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006180
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005720
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006496
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.077773
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 23

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 23
number of iterations: 49
total time: 90.005141

Final Solution: 0->2, 1->24, 2->27, 3->17, 4->29, 5->47, 6->45, 7->42, 8->33, 9->42, 10->45, 11->11, 12->38, 13->15, 14->35, 15->46, 16->31, 17->19, 18->35, 19->18, 20->2, 21->28, 22->9, 23->41, 24->43, 25->31, 26->37, 27->41, 28->42, 29->46, 30->40, 31->2, 32->23, 33->16, 34->11, 35->4, 36->30, 37->0, 38->12, 39->27, 40->23, 41->12, 42->5, 43->2, 44->35, 45->28, 46->43, 47->5, 48->17, 49->32, 50->29, 51->41, 52->39, 53->34, 54->14, 55->42, 56->14, 57->25, 58->0, 59->45, 60->36, 61->34, 62->15, 63->18, 64->37, 65->34, 66->5, 67->28, 68->21, 69->6, 70->17, 71->3, 72->4, 73->39, 74->1, 75->16, 76->47, 77->33, 78->8, 79->4, 80->29, 81->8, 82->35, 83->24, 84->19, 85->47, 86->3, 87->31, 88->24, 89->9, 90->41, 91->7, 92->22, 93->46, 94->36, 95->16, 96->27, 97->28, 98->11, 99->41, 100->20, 101->39, 102->31, 103->23, 104->33, 105->4, 106->37, 107->10, 108->30, 109->11, 110->3, 111->6, 112->13, 113->1, 114->7, 115->44, 116->2, 117->32, 118->36, 119->7, 120->37, 121->18, 122->19, 123->24, 124->12, 125->13, 126->1, 127->12, 128->7, 129->41, 130->30, 131->8, 132->3, 133->44, 134->47, 135->13, 136->34, 137->38, 138->44, 139->46, 140->35, 141->37, 142->14, 143->4, 144->15, 145->38, 146->29, 147->19, 148->10, 149->21, 150->6, 151->38, 152->24, 153->11, 154->21, 155->26, 156->47, 157->18, 158->36, 159->46, 160->5, 161->15, 162->13, 163->0, 164->32, 165->37, 166->22, 167->23, 168->12, 169->27, 170->0, 171->26, 172->26, 173->42, 174->10, 175->21, 176->26, 177->33, 178->40, 179->29, 180->22, 181->26, 182->39, 183->30, 184->44, 185->16, 186->43, 187->47, 188->31, 189->9, 190->40, 191->20, 192->18, 193->33, 194->32, 195->8, 196->30, 197->32, 198->7, 199->28, 200->10, 201->3, 202->5, 203->46, 204->3, 205->20, 206->42, 207->43, 208->25, 209->34, 210->8, 211->10, 212->19, 213->19, 214->16, 215->9, 216->45, 217->43, 218->14, 219->6, 220->34, 221->7, 222->20, 223->5, 224->29, 225->16, 226->20, 227->38, 228->6, 229->40, 230->1, 231->20, 232->43, 233->9, 234->36, 235->15, 236->0, 237->31, 238->6, 239->8, 240->25, 241->15, 242->21, 243->44, 244->24, 245->12, 246->2, 247->25, 248->4, 249->23, 250->14, 251->36, 252->13, 253->28, 254->1, 255->10, 256->25, 257->22, 258->23, 259->40, 260->9, 261->38, 262->39, 263->40, 264->27, 265->35, 266->0, 267->30, 268->45, 269->14, 270->26, 271->32, 272->45, 273->33, 274->39, 275->22, 276->17, 277->44, 278->17, 279->27, 280->25, 281->1, 282->13, 283->21, 284->17, 285->18, 286->11, 287->22

Average CPI map:
	1.94,	3.96,	4.42,	2.41,	4.20,	1.58
	2.08,	2.31,	3.82,	3.74,	3.66,	3.57
	3.54,	3.45,	3.43,	3.28,	3.24,	6.75
	3.06,	3.01,	2.97,	2.90,	2.85,	2.83
	2.82,	10.11,	0.92,	2.57,	2.44,	2.08
	2.04,	1.38,	1.70,	2.25,	2.24,	2.24
	2.01,	2.24,	2.63,	2.23,	2.23,	2.23
	3.53,	2.23,	6.34,	2.18,	2.18,	2.17

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	1,	0,	0,	1
