==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 12 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 401

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/0.015
- branches (local/global): 85/85
- fails (local/global): 30/30
- lns time: 0.024000
- lns branches: 85
- lns fails: 30
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 25806
- lns fails: 12548
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 25083
- lns fails: 12196
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008000
- lns branches: 29906
- lns fails: 14361
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.017/6.065
- branches (local/global): 36/80916
- fails (local/global): 5/39140
- lns time: 0.025000
- lns branches: 36
- lns fails: 5
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.032/6.105
- branches (local/global): 139/81055
- fails (local/global): 56/39196
- lns time: 0.040000
- lns branches: 139
- lns fails: 56
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.034/6.147
- branches (local/global): 163/81218
- fails (local/global): 68/39264
- lns time: 0.042000
- lns branches: 163
- lns fails: 68
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.031/6.186
- branches (local/global): 89/81307
- fails (local/global): 32/39296
- lns time: 0.041000
- lns branches: 89
- lns fails: 32
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 19014
- lns fails: 9201
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.043/8.247
- branches (local/global): 2595/102916
- fails (local/global): 1215/49712
- lns time: 0.050000
- lns branches: 2595
- lns fails: 1215
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.768/9.022
- branches (local/global): 7198/110114
- fails (local/global): 3446/53158
- lns time: 0.774000
- lns branches: 7198
- lns fails: 3446
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 0.399000
- lns branches: 2294
- lns fails: 1137
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.009000
- lns branches: 16924
- lns fails: 8322
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.060/11.496
- branches (local/global): 265/129597
- fails (local/global): 110/62727
- lns time: 0.070000
- lns branches: 265
- lns fails: 110
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 17830
- lns fails: 8796
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.096/13.610
- branches (local/global): 887/148314
- fails (local/global): 404/71927
- lns time: 0.104000
- lns branches: 887
- lns fails: 404
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 25030
- lns fails: 12124
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.174/15.800
- branches (local/global): 3419/176763
- fails (local/global): 1606/85657
- lns time: 0.182000
- lns branches: 3419
- lns fails: 1606
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 34246
- lns fails: 16701
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 17769
- lns fails: 8691
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 23406
- lns fails: 11401
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007000
- lns branches: 15933
- lns fails: 7854
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 27026
- lns fails: 13093
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 43025
- lns fails: 20860
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.078/27.935
- branches (local/global): 933/339101
- fails (local/global): 427/164684
- lns time: 0.086000
- lns branches: 933
- lns fails: 427
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.029/27.972
- branches (local/global): 265/339366
- fails (local/global): 117/164801
- lns time: 0.037000
- lns branches: 265
- lns fails: 117
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 17143
- lns fails: 8427
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 17705
- lns fails: 8719
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 22292
- lns fails: 10857
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 17969
- lns fails: 8860
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 25088
- lns fails: 12257
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 23211
- lns fails: 11288
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 18663
- lns fails: 9098
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.384/42.420
- branches (local/global): 11086/492523
- fails (local/global): 5287/239594
- lns time: 0.392000
- lns branches: 11086
- lns fails: 5287
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 17399
- lns fails: 8543
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 14688
- lns fails: 7268
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 46656
- lns fails: 22670
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.009000
- lns branches: 18005
- lns fails: 8693
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 13195
- lns fails: 6401
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 19942
- lns fails: 9718
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 13802
- lns fails: 6749
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 49965
- lns fails: 24176
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 42338
- lns fails: 20699
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 20879
- lns fails: 10140
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 57881
- lns fails: 28114
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 12579
- lns fails: 6178
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 25879
- lns fails: 12723
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 27802
- lns fails: 13397
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 16705
- lns fails: 8140
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.011000
- lns branches: 22801
- lns fails: 11136
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 13434
- lns fails: 6537
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.009000
- lns branches: 18983
- lns fails: 9208
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 22430
- lns fails: 11038
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 15326
- lns fails: 7585
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 0.432000
- lns branches: 2230
- lns fails: 1111
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.007000
- lns branches: 37337
- lns fails: 18189
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.009000
- lns branches: 8967
- lns fails: 4396
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 2.008000
- lns branches: 16462
- lns fails: 8109
LNS iteration #58 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
- lns time: 0.961000
- lns branches: 6781
- lns fails: 3335

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 33
number of iterations: 59
total time: 90007.000000
total branches: 1054989
total fails: 513847

Final Solution: 0->33, 1->7, 2->17, 3->20, 4->2, 5->42, 6->15, 7->36, 8->26, 9->41, 10->24, 11->2, 12->11, 13->44, 14->23, 15->4, 16->6, 17->27, 18->21, 19->24, 20->12, 21->21, 22->11, 23->12, 24->12, 25->7, 26->43, 27->20, 28->38, 29->6, 30->31, 31->41, 32->6, 33->22, 34->16, 35->27, 36->19, 37->9, 38->47, 39->27, 40->30, 41->11, 42->39, 43->47, 44->28, 45->32, 46->1, 47->15, 48->12, 49->18, 50->39, 51->24, 52->18, 53->44, 54->8, 55->32, 56->11, 57->46, 58->35, 59->29, 60->39, 61->30, 62->27, 63->45, 64->36, 65->1, 66->35, 67->37, 68->42, 69->41, 70->9, 71->14, 72->35, 73->4, 74->8, 75->9, 76->20, 77->15, 78->23, 79->27, 80->20, 81->35, 82->40, 83->7, 84->11, 85->39, 86->29, 87->43, 88->38, 89->5, 90->45, 91->33, 92->8, 93->41, 94->27, 95->44, 96->13, 97->34, 98->38, 99->5, 100->35, 101->31, 102->23, 103->46, 104->29, 105->8, 106->1, 107->3, 108->31, 109->2, 110->19, 111->18, 112->46, 113->18, 114->6, 115->29, 116->28, 117->37, 118->26, 119->43, 120->7, 121->17, 122->18, 123->6, 124->25, 125->32, 126->33, 127->14, 128->40, 129->1, 130->39, 131->0, 132->31, 133->38, 134->47, 135->22, 136->0, 137->37, 138->42, 139->46, 140->39, 141->30, 142->23, 143->23, 144->4, 145->12, 146->4, 147->26, 148->8, 149->42, 150->35, 151->41, 152->31, 153->21, 154->42, 155->9, 156->44, 157->38, 158->43, 159->36, 160->7, 161->15, 162->29, 163->3, 164->47, 165->8, 166->25, 167->3, 168->10, 169->34, 170->21, 171->0, 172->26, 173->33, 174->40, 175->3, 176->11, 177->43, 178->46, 179->47, 180->2, 181->32, 182->9, 183->9, 184->30, 185->28, 186->37, 187->26, 188->12, 189->44, 190->10, 191->19, 192->3, 193->17, 194->21, 195->16, 196->18, 197->37, 198->13, 199->36, 200->0, 201->26, 202->34, 203->3, 204->37, 205->19, 206->34, 207->13, 208->28, 209->10, 210->40, 211->10, 212->0, 213->19, 214->17, 215->16, 216->40, 217->20, 218->14, 219->14, 220->40, 221->13, 222->1, 223->5, 224->38, 225->41, 226->19, 227->25, 228->7, 229->5, 230->4, 231->31, 232->30, 233->36, 234->24, 235->15, 236->47, 237->42, 238->0, 239->46, 240->5, 241->24, 242->22, 243->22, 244->17, 245->32, 246->16, 247->5, 248->2, 249->34, 250->10, 251->21, 252->20, 253->43, 254->25, 255->17, 256->24, 257->45, 258->14, 259->28, 260->25, 261->33, 262->44, 263->13, 264->29, 265->34, 266->30, 267->28, 268->13, 269->23, 270->36, 271->22, 272->4, 273->1, 274->14, 275->45, 276->16, 277->2, 278->16, 279->45, 280->45, 281->6, 282->22, 283->32, 284->10, 285->25, 286->33, 287->15

Average CPI map:
	1.66,	1.81,	4.73,	6.76,	1.45,	1.34
	3.42,	2.41,	3.20,	1.46,	5.01,	2.01
	2.02,	11.51,	1.27,	1.23,	12.34,	5.87
	2.38,	3.56,	1.02,	3.02,	3.46,	2.48
	2.40,	3.65,	1.80,	1.81,	10.06,	5.75
	2.21,	2.18,	1.05,	1.50,	10.23,	2.38
	2.15,	12.16,	2.12,	2.12,	3.37,	2.12
	2.12,	2.52,	1.65,	11.13,	2.10,	2.01

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	1,	0,	0,	1,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	0,	1,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	0,	1,	1
	1,	1,	0,	1,	0,	1
