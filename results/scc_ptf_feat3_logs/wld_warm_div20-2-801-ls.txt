LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
3.12567, 0.752061, 3.77408, 1.62275, 1.54955, 1.17589, 1.72939, 3.02359, 2.35293, 0.5, 3.11872, 1.27538, 17.84, 24.8309, 22.3046, 23.9911, 15.5276, 27.5057, 0.5, 1.8254, 3.13266, 3.48523, 1.94102, 1.1157, 2.11213, 2.06647, 1.99774, 1.74421, 2.0075, 2.07194, 0.522909, 1.49971, 1.5007, 3.20029, 3.94412, 1.33228, 2.47984, 1.60996, 1.63519, 2.5718, 1.81176, 1.89145, 3.25335, 0.5, 0.507717, 3.35651, 3.32506, 1.05735, 1.59151, 2.60202, 0.868737, 2.60868, 1.45236, 2.8767, 12.5817, 35, 12.3399, 20.615, 17.7576, 33.7058, 0.5, 2.2086, 4.49219, 0.521533, 0.703125, 3.57454, 1.32375, 0.5, 3.53538, 4.27056, 1.22948, 1.14083, 2.1446, 2.20424, 1.96104, 2.3198, 1.54687, 1.82346, 3.22698, 0.778605, 1.7414, 3.17747, 1.91045, 1.16509, 2.33292, 2.04677, 0.599128, 0.63446, 5.14332, 1.24341, 0.954258, 4.07886, 1.13576, 2.37027, 2.04029, 1.42056, 2.83431, 1.10327, 2.71016, 2.72216, 2.1301, 0.5, 1.53209, 2.281, 2.40503, 2.18578, 1.86167, 1.73443, 1.37888, 0.5, 3.68517, 2.2678, 3.31085, 0.85731, 1.65819, 3.48785, 2.43323, 0.5, 1.0792, 2.84153, 0.604221, 1.83231, 0.5, 1.61493, 4.32856, 3.11998, 2.26413, 1.06299, 1.88884, 2.27832, 2.21424, 2.29148, 3.33042, 0.786218, 1.01043, 3.19589, 0.5, 3.17704, 2.29831, 2.17468, 2.40911, 2.53069, 1.17653, 1.41068, 1.39781, 1.5314, 2.1906, 1.35996, 2.01672, 3.50351, 1.17117, 1.34719, 3.3889, 1.49879, 2.75224, 1.8417, 2.56654, 2.58568, 1.96223, 0.5, 2.58026, 1.8053, 0.5, 2.44487, 2.88778, 2.77599, 2.85053, 0.540841, 0.5, 4.13547, 4.13324, 0.602114, 1.27328, 1.3559, 2.10053, 2.27077, 1.69581, 2.41086, 2.27242, 1.24961, 1.70121, 3.12426, 2.78517, 0.96334, 1.21139, 2.21463, 2.55195, 2.33888, 1.12488, 1.28664, 2.49165, 2.206, 1.2706, 2.2993, 2.38943, 1.68816, 2.55896, 1.79355, 3.41952, 2.01037, 2.19211, 1.06076, 2.53684, 0.780401, 3.0517, 0.966597, 2.17906, 0.5, 3.13944, 2.16321, 4.00701, 1.14437, 1.66825, 1.46278, 0.5, 3.21759, 0.5, 2.20956, 3.44682, 1.98752, 2.67762, 1.17849, 1.89847, 1.28251, 2.3825, 2.37247, 1.96994, 2.09411, 2.33515, 1.65527, 1.94325, 1.54898, 1.9655, 2.55185, 2.18274, 0.5, 4.98818, 0.755168, 2.90778, 0.666132, 1.5655, 1.11219, 0.864467, 3.32768, 2.80025, 2.32992, 4.80928, 0.5, 0.877944, 0.527548, 0.644329, 4.6409, 2.55565, 2.59951, 2.2867, 1.42287, 0.851681, 2.28359, 4.41532, 0.5, 1.83263, 1.2317, 1.53574, 2.48461, 2.30571, 1.85041, 2.23376, 2.05481, 1.74023, 1.81507, 1.67615, 3.04315, 3.28507, 0.5, 2.93744, 0.558177, 2.41297, 2.36075, 2.18932, 1.05135, 1.98417, 2.00144, 1.52199, 2.30636, 1.96989, 1.97137, 1.243, 2.98739
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55078, operands = 136364
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 801, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 25222 itr]: obj = (24), mov = 25222, inf = 69.4%, acc = 18.3%, imp = 139

number of satisfied efficiency constraints: 24
[2 sec, 51393 itr]: obj = (25), mov = 51393, inf = 66.5%, acc = 19%, imp = 140

number of satisfied efficiency constraints: 25
[3 sec, 70437 itr]: obj = (26), mov = 70437, inf = 59.6%, acc = 22.1%, imp = 141

number of satisfied efficiency constraints: 26
[4 sec, 88587 itr]: obj = (26), mov = 88587, inf = 53.6%, acc = 24.4%, imp = 141

number of satisfied efficiency constraints: 26
[5 sec, 107670 itr]: obj = (27), mov = 107670, inf = 48.2%, acc = 26.1%, imp = 142

number of satisfied efficiency constraints: 27
[6 sec, 127493 itr]: obj = (27), mov = 127493, inf = 44.1%, acc = 27.1%, imp = 142

number of satisfied efficiency constraints: 27
[7 sec, 145135 itr]: obj = (27), mov = 145135, inf = 41.4%, acc = 28.6%, imp = 175

number of satisfied efficiency constraints: 27
[8 sec, 161651 itr]: obj = (27), mov = 161651, inf = 38.9%, acc = 30%, imp = 176

number of satisfied efficiency constraints: 27
[9 sec, 178672 itr]: obj = (27), mov = 178672, inf = 36.8%, acc = 30.9%, imp = 177

number of satisfied efficiency constraints: 27
[10 sec, 196435 itr]: obj = (27), mov = 196435, inf = 35.1%, acc = 31.5%, imp = 177

number of satisfied efficiency constraints: 27
[11 sec, 214032 itr]: obj = (27), mov = 214032, inf = 33.6%, acc = 32%, imp = 177

number of satisfied efficiency constraints: 27
[12 sec, 231352 itr]: obj = (27), mov = 231352, inf = 32.4%, acc = 32.6%, imp = 177

number of satisfied efficiency constraints: 27
[13 sec, 249045 itr]: obj = (27), mov = 249045, inf = 31.3%, acc = 32.9%, imp = 177

number of satisfied efficiency constraints: 27
[14 sec, 260033 itr]: obj = (27), mov = 260033, inf = 30.7%, acc = 33.4%, imp = 208

number of satisfied efficiency constraints: 27
[15 sec, 273838 itr]: obj = (27), mov = 273838, inf = 30%, acc = 33.9%, imp = 211

number of satisfied efficiency constraints: 27
[16 sec, 290011 itr]: obj = (27), mov = 290011, inf = 29.2%, acc = 34.3%, imp = 211

number of satisfied efficiency constraints: 27
[17 sec, 306824 itr]: obj = (27), mov = 306824, inf = 28.4%, acc = 34.5%, imp = 212

number of satisfied efficiency constraints: 27
[18 sec, 324462 itr]: obj = (27), mov = 324462, inf = 27.8%, acc = 34.6%, imp = 212

number of satisfied efficiency constraints: 27
[19 sec, 341442 itr]: obj = (27), mov = 341442, inf = 27.2%, acc = 34.8%, imp = 212

number of satisfied efficiency constraints: 27
[20 sec, 357937 itr]: obj = (27), mov = 357937, inf = 26.7%, acc = 34.9%, imp = 212

number of satisfied efficiency constraints: 27
[21 sec, 374282 itr]: obj = (27), mov = 374282, inf = 26.2%, acc = 35%, imp = 212

number of satisfied efficiency constraints: 27
[22 sec, 390649 itr]: obj = (27), mov = 390649, inf = 25.8%, acc = 35.3%, imp = 248

number of satisfied efficiency constraints: 27
[23 sec, 405563 itr]: obj = (27), mov = 405563, inf = 25.4%, acc = 35.6%, imp = 249

number of satisfied efficiency constraints: 27
[24 sec, 420009 itr]: obj = (27), mov = 420009, inf = 25%, acc = 36%, imp = 249

number of satisfied efficiency constraints: 27
[25 sec, 434437 itr]: obj = (27), mov = 434437, inf = 24.8%, acc = 36.4%, imp = 249

number of satisfied efficiency constraints: 27
[26 sec, 449865 itr]: obj = (27), mov = 449865, inf = 24.4%, acc = 36.6%, imp = 250

number of satisfied efficiency constraints: 27
[27 sec, 466449 itr]: obj = (27), mov = 466449, inf = 24.1%, acc = 36.7%, imp = 250

number of satisfied efficiency constraints: 27
[28 sec, 482793 itr]: obj = (27), mov = 482793, inf = 23.8%, acc = 36.8%, imp = 250

number of satisfied efficiency constraints: 27
[29 sec, 498569 itr]: obj = (27), mov = 498569, inf = 23.5%, acc = 37%, imp = 250

number of satisfied efficiency constraints: 27
[30 sec, 513905 itr]: obj = (27), mov = 513905, inf = 23.3%, acc = 37.2%, imp = 250

number of satisfied efficiency constraints: 27
[31 sec, 529559 itr]: obj = (27), mov = 529559, inf = 23%, acc = 37.3%, imp = 250

number of satisfied efficiency constraints: 27
[32 sec, 546669 itr]: obj = (27), mov = 546669, inf = 22.8%, acc = 37.3%, imp = 251

number of satisfied efficiency constraints: 27
[33 sec, 564145 itr]: obj = (27), mov = 564145, inf = 22.5%, acc = 37.3%, imp = 251

number of satisfied efficiency constraints: 27
[34 sec, 581529 itr]: obj = (27), mov = 581529, inf = 22.4%, acc = 37.3%, imp = 251

number of satisfied efficiency constraints: 27
[35 sec, 599152 itr]: obj = (28), mov = 599152, inf = 22.1%, acc = 37.2%, imp = 253

number of satisfied efficiency constraints: 28
[36 sec, 619754 itr]: obj = (28), mov = 619754, inf = 21.9%, acc = 36.9%, imp = 253

number of satisfied efficiency constraints: 28
[37 sec, 639189 itr]: obj = (28), mov = 639189, inf = 21.7%, acc = 36.7%, imp = 253

number of satisfied efficiency constraints: 28
[38 sec, 659412 itr]: obj = (28), mov = 659412, inf = 21.6%, acc = 36.5%, imp = 253

number of satisfied efficiency constraints: 28
[39 sec, 680100 itr]: obj = (28), mov = 680100, inf = 21.4%, acc = 36.2%, imp = 253

number of satisfied efficiency constraints: 28
[40 sec, 699887 itr]: obj = (28), mov = 699887, inf = 21.3%, acc = 36%, imp = 253

number of satisfied efficiency constraints: 28
[41 sec, 719707 itr]: obj = (28), mov = 719707, inf = 21.1%, acc = 35.8%, imp = 253

number of satisfied efficiency constraints: 28
[42 sec, 741065 itr]: obj = (29), mov = 741065, inf = 21%, acc = 35.6%, imp = 254

number of satisfied efficiency constraints: 29
[43 sec, 763413 itr]: obj = (29), mov = 763413, inf = 20.9%, acc = 35.3%, imp = 254

number of satisfied efficiency constraints: 29
[44 sec, 785039 itr]: obj = (29), mov = 785039, inf = 20.7%, acc = 35%, imp = 254

number of satisfied efficiency constraints: 29
[45 sec, 806788 itr]: obj = (29), mov = 806788, inf = 20.6%, acc = 34.7%, imp = 254

number of satisfied efficiency constraints: 29
[46 sec, 828953 itr]: obj = (29), mov = 828953, inf = 20.5%, acc = 34.4%, imp = 254

number of satisfied efficiency constraints: 29
[47 sec, 850061 itr]: obj = (29), mov = 850061, inf = 20.4%, acc = 34.2%, imp = 254

number of satisfied efficiency constraints: 29
[48 sec, 872000 itr]: obj = (29), mov = 872000, inf = 20.2%, acc = 34%, imp = 254

number of satisfied efficiency constraints: 29
[49 sec, 893552 itr]: obj = (29), mov = 893552, inf = 20.2%, acc = 33.8%, imp = 254

number of satisfied efficiency constraints: 29
[50 sec, 915357 itr]: obj = (29), mov = 915357, inf = 20.1%, acc = 33.6%, imp = 254

number of satisfied efficiency constraints: 29
[51 sec, 937019 itr]: obj = (29), mov = 937019, inf = 20%, acc = 33.3%, imp = 254

number of satisfied efficiency constraints: 29
[52 sec, 958300 itr]: obj = (29), mov = 958300, inf = 19.9%, acc = 33.2%, imp = 254

number of satisfied efficiency constraints: 29
[53 sec, 979856 itr]: obj = (29), mov = 979856, inf = 19.8%, acc = 33%, imp = 254

number of satisfied efficiency constraints: 29
[54 sec, 1002483 itr]: obj = (29), mov = 1002483, inf = 19.7%, acc = 32.8%, imp = 254

number of satisfied efficiency constraints: 29
[55 sec, 1024872 itr]: obj = (29), mov = 1024872, inf = 19.6%, acc = 32.6%, imp = 254

number of satisfied efficiency constraints: 29
[56 sec, 1046885 itr]: obj = (29), mov = 1046885, inf = 19.5%, acc = 32.5%, imp = 254

number of satisfied efficiency constraints: 29
[57 sec, 1069390 itr]: obj = (29), mov = 1069390, inf = 19.4%, acc = 32.3%, imp = 254

number of satisfied efficiency constraints: 29
[58 sec, 1091118 itr]: obj = (29), mov = 1091118, inf = 19.4%, acc = 32.1%, imp = 254

number of satisfied efficiency constraints: 29
[59 sec, 1112828 itr]: obj = (29), mov = 1112828, inf = 19.3%, acc = 32%, imp = 254

number of satisfied efficiency constraints: 29
[60 sec, 1134582 itr]: obj = (29), mov = 1134582, inf = 19.2%, acc = 31.9%, imp = 254

number of satisfied efficiency constraints: 29
[61 sec, 1155772 itr]: obj = (29), mov = 1155772, inf = 19.1%, acc = 31.8%, imp = 254

number of satisfied efficiency constraints: 29
[62 sec, 1177941 itr]: obj = (29), mov = 1177941, inf = 19.1%, acc = 31.6%, imp = 254

number of satisfied efficiency constraints: 29
[63 sec, 1199774 itr]: obj = (29), mov = 1199774, inf = 19%, acc = 31.5%, imp = 254

number of satisfied efficiency constraints: 29
[64 sec, 1221445 itr]: obj = (29), mov = 1221445, inf = 19%, acc = 31.4%, imp = 254

number of satisfied efficiency constraints: 29
[65 sec, 1242846 itr]: obj = (29), mov = 1242846, inf = 18.9%, acc = 31.3%, imp = 254

number of satisfied efficiency constraints: 29
[66 sec, 1265082 itr]: obj = (29), mov = 1265082, inf = 18.9%, acc = 31.1%, imp = 254

number of satisfied efficiency constraints: 29
[67 sec, 1286603 itr]: obj = (29), mov = 1286603, inf = 18.8%, acc = 31%, imp = 254

number of satisfied efficiency constraints: 29
[68 sec, 1309057 itr]: obj = (29), mov = 1309057, inf = 18.7%, acc = 30.9%, imp = 254

number of satisfied efficiency constraints: 29
[69 sec, 1331489 itr]: obj = (29), mov = 1331489, inf = 18.7%, acc = 30.8%, imp = 254

number of satisfied efficiency constraints: 29
[70 sec, 1353156 itr]: obj = (29), mov = 1353156, inf = 18.7%, acc = 30.7%, imp = 254

number of satisfied efficiency constraints: 29
[71 sec, 1375241 itr]: obj = (29), mov = 1375241, inf = 18.6%, acc = 30.6%, imp = 254

number of satisfied efficiency constraints: 29
[72 sec, 1397204 itr]: obj = (29), mov = 1397204, inf = 18.6%, acc = 30.5%, imp = 254

number of satisfied efficiency constraints: 29
[73 sec, 1420108 itr]: obj = (29), mov = 1420108, inf = 18.6%, acc = 30.4%, imp = 254

number of satisfied efficiency constraints: 29
[74 sec, 1441726 itr]: obj = (29), mov = 1441726, inf = 18.6%, acc = 30.4%, imp = 254

number of satisfied efficiency constraints: 29
[75 sec, 1462354 itr]: obj = (29), mov = 1462354, inf = 18.5%, acc = 30.3%, imp = 254

number of satisfied efficiency constraints: 29
[76 sec, 1483996 itr]: obj = (29), mov = 1483996, inf = 18.5%, acc = 30.2%, imp = 254

number of satisfied efficiency constraints: 29
[77 sec, 1505840 itr]: obj = (29), mov = 1505840, inf = 18.4%, acc = 30.2%, imp = 254

number of satisfied efficiency constraints: 29
[78 sec, 1527706 itr]: obj = (29), mov = 1527706, inf = 18.4%, acc = 30.1%, imp = 254

number of satisfied efficiency constraints: 29
[79 sec, 1549669 itr]: obj = (29), mov = 1549669, inf = 18.4%, acc = 30%, imp = 254

number of satisfied efficiency constraints: 29
[80 sec, 1565962 itr]: obj = (29), mov = 1565962, inf = 18.3%, acc = 30.1%, imp = 288

number of satisfied efficiency constraints: 29
[81 sec, 1582275 itr]: obj = (29), mov = 1582275, inf = 18.3%, acc = 30.2%, imp = 289

number of satisfied efficiency constraints: 29
[82 sec, 1599837 itr]: obj = (29), mov = 1599837, inf = 18.3%, acc = 30.3%, imp = 289

number of satisfied efficiency constraints: 29
[83 sec, 1617475 itr]: obj = (29), mov = 1617475, inf = 18.2%, acc = 30.3%, imp = 289

number of satisfied efficiency constraints: 29
[84 sec, 1633953 itr]: obj = (29), mov = 1633953, inf = 18.2%, acc = 30.4%, imp = 289

number of satisfied efficiency constraints: 29
[85 sec, 1651267 itr]: obj = (29), mov = 1651267, inf = 18.2%, acc = 30.5%, imp = 289

number of satisfied efficiency constraints: 29
[86 sec, 1667361 itr]: obj = (29), mov = 1667361, inf = 18.1%, acc = 30.5%, imp = 327

number of satisfied efficiency constraints: 29
[87 sec, 1683334 itr]: obj = (29), mov = 1683334, inf = 18.1%, acc = 30.6%, imp = 331

number of satisfied efficiency constraints: 29
[88 sec, 1699290 itr]: obj = (29), mov = 1699290, inf = 18.1%, acc = 30.7%, imp = 331

number of satisfied efficiency constraints: 29
[89 sec, 1715559 itr]: obj = (29), mov = 1715559, inf = 18%, acc = 30.8%, imp = 331

number of satisfied efficiency constraints: 29
[90 sec, 1731341 itr]: obj = (29), mov = 1731341, inf = 18%, acc = 30.9%, imp = 331

number of satisfied efficiency constraints: 29
[90 sec, 1731341 itr]: obj = (29), mov = 1731341, inf = 18%, acc = 30.9%, imp = 331

number of satisfied efficiency constraints: 29
1731341 iterations, 1731341 moves performed in 90 seconds
Feasible solution: obj = (29)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 29
Mapping: 0->44, 1->19, 2->39, 3->3, 4->4, 5->30, 6->3, 7->6, 8->39, 9->0, 10->13, 11->23, 12->16, 13->20, 14->37, 15->34, 16->18, 17->37, 18->5, 19->35, 20->43, 21->29, 22->17, 23->21, 24->41, 25->23, 26->16, 27->32, 28->23, 29->29, 30->4, 31->13, 32->25, 33->40, 34->12, 35->14, 36->24, 37->29, 38->28, 39->11, 40->33, 41->6, 42->3, 43->13, 44->9, 45->9, 46->6, 47->45, 48->41, 49->47, 50->4, 51->27, 52->17, 53->7, 54->36, 55->16, 56->40, 57->5, 58->30, 59->34, 60->19, 61->1, 62->46, 63->36, 64->18, 65->34, 66->47, 67->0, 68->29, 69->7, 70->17, 71->20, 72->10, 73->11, 74->24, 75->37, 76->33, 77->32, 78->41, 79->19, 80->31, 81->40, 82->33, 83->15, 84->24, 85->21, 86->47, 87->18, 88->10, 89->28, 90->41, 91->42, 92->33, 93->37, 94->10, 95->44, 96->8, 97->14, 98->37, 99->21, 100->19, 101->1, 102->31, 103->4, 104->11, 105->28, 106->1, 107->29, 108->18, 109->15, 110->38, 111->2, 112->2, 113->30, 114->28, 115->37, 116->22, 117->12, 118->45, 119->2, 120->28, 121->20, 122->44, 123->32, 124->1, 125->42, 126->26, 127->14, 128->47, 129->15, 130->43, 131->8, 132->14, 133->36, 134->30, 135->42, 136->31, 137->29, 138->46, 139->47, 140->11, 141->0, 142->2, 143->0, 144->38, 145->33, 146->35, 147->33, 148->7, 149->47, 150->20, 151->27, 152->16, 153->2, 154->13, 155->17, 156->8, 157->12, 158->11, 159->25, 160->22, 161->12, 162->28, 163->27, 164->40, 165->43, 166->20, 167->5, 168->19, 169->35, 170->34, 171->11, 172->14, 173->9, 174->45, 175->12, 176->27, 177->10, 178->26, 179->14, 180->23, 181->15, 182->44, 183->4, 184->22, 185->43, 186->30, 187->3, 188->0, 189->38, 190->35, 191->35, 192->25, 193->26, 194->24, 195->24, 196->7, 197->42, 198->34, 199->20, 200->19, 201->22, 202->34, 203->21, 204->6, 205->1, 206->26, 207->21, 208->40, 209->8, 210->25, 211->13, 212->3, 213->38, 214->38, 215->2, 216->27, 217->16, 218->16, 219->38, 220->1, 221->26, 222->36, 223->18, 224->43, 225->36, 226->7, 227->32, 228->39, 229->18, 230->10, 231->4, 232->42, 233->46, 234->15, 235->30, 236->39, 237->22, 238->41, 239->9, 240->35, 241->13, 242->0, 243->40, 244->7, 245->9, 246->12, 247->36, 248->45, 249->31, 250->5, 251->17, 252->26, 253->46, 254->8, 255->6, 256->31, 257->23, 258->23, 259->6, 260->27, 261->25, 262->25, 263->15, 264->43, 265->5, 266->41, 267->44, 268->39, 269->9, 270->42, 271->8, 272->46, 273->21, 274->5, 275->22, 276->32, 277->24, 278->17, 279->32, 280->10, 281->45, 282->31, 283->46, 284->39, 285->44, 286->3, 287->45
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.15512	2.09051	2.38551	1.97594	1.28909	4.51468
	2.20244	2.74885	2.5309	1.67188	2.61108	2.02575
	2.65252	1.68787	1.55864	1.95584	10.6472	2.2158
	3.53031	1.14215	5.63935	1.2775	1.43316	2.29158
	2.20202	1.67429	2.12484	1.7382	1.30447	2.60233
	3.97552	1.11245	1.79017	1.54937	11.8935	2.40244
	3.11007	10.1164	1.71999	2.86008	4.67875	2.17107
	2.63832	2.50429	1.97626	1.68398	2.92222	2.01532
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	0	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 2 --global-tlim 90 --seed 801

Starting the Optimization Process
