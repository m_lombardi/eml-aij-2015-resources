LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
5.43083, 0.846831, 0.5, 0.652849, 2.47094, 2.09855, 1.78214, 1.89325, 2.37566, 2.25137, 1.8154, 1.88219, 2.61464, 2.87522, 2.33856, 0.979855, 1.54307, 1.64866, 2.36799, 3.12893, 1.92914, 1.66545, 1.4476, 1.46089, 0.904489, 1.92516, 3.43279, 1.28338, 2.25966, 2.19452, 2.55022, 2.78861, 0.5, 2.81406, 0.664355, 2.68275, 0.514187, 3.39168, 3.11245, 0.532515, 3.94917, 0.5, 2.28941, 0.577209, 3.34893, 3.48031, 0.5, 1.80413, 1.12712, 2.78344, 0.5, 2.03939, 3.57351, 1.97653, 1.94836, 4.10893, 0.5, 3.03288, 0.592334, 1.81749, 2.21826, 1.79282, 2.25651, 2.20399, 2.25821, 1.2702, 1.10852, 3.4975, 3.52883, 2.84283, 0.5, 0.522315, 2.23127, 2.29447, 1.94743, 1.15556, 2.06765, 2.30363, 3.99612, 1.00188, 0.5, 0.909134, 4.97378, 0.619083, 3.81571, 2.20031, 1.47596, 2.37954, 0.5, 1.62847, 18.2709, 34.7112, 10.9156, 12.7765, 35, 20.3258, 1.90049, 2.42843, 0.5, 1.27537, 4.38409, 1.51162, 2.15977, 2.01288, 2.71314, 2.19569, 1.92999, 0.988539, 0.622872, 3.35123, 0.5, 3.56069, 2.42638, 1.53884, 3.05416, 1.28964, 2.8098, 0.5, 3.41914, 0.927258, 2.86078, 0.919532, 3.11728, 1.05394, 0.5, 3.54847, 0.806652, 1.96282, 1.81311, 2.82054, 2.42039, 2.17649, 3.10721, 1.85215, 2.83333, 1.25211, 1.78501, 1.17019, 2.39235, 0.917416, 2.03617, 2.62819, 2.65586, 1.37001, 0.5, 0.81376, 4.18687, 0.761575, 4.10168, 1.63612, 1.31548, 0.5, 1.75947, 1.2712, 1.58142, 5.57242, 0.5, 1.213, 1.77748, 3.94291, 2.61327, 1.95335, 3.03731, 0.5, 1.93913, 1.1377, 3.19529, 2.19058, 3.38841, 0.754301, 1.1115, 0.5, 2.50944, 3.73634, 1.15008, 3.45956, 3.13002, 2.39488, 0.586407, 1.27904, 3.95646, 2.18818, 0.818208, 1.24856, 0.876001, 2.91259, 1.18745, 2.7227, 1.40181, 2.47111, 2.47244, 1.74449, 5.793, 1.27361, 1.52309, 1.76074, 0.5, 1.14957, 2.17614, 3.84929, 2.18361, 1.03366, 2.2573, 0.5, 2.21916, 2.13373, 2.37615, 0.5, 2.25941, 2.51155, 4.81962, 0.5, 0.522854, 2.16593, 3.45629, 0.535304, 0.507733, 3.97852, 1.72979, 2.52493, 0.716319, 2.54271, 3.10751, 0.5, 2.41706, 2.45753, 2.96668, 0.551223, 2.63326, 2.5958, 2.18761, 2.57435, 0.5, 1.50899, 0.5, 2.55441, 2.12355, 2.54257, 2.31356, 1.96591, 2.11884, 1.45463, 2.32145, 2.13165, 2.07741, 1.89603, 2.19979, 2.23706, 1.57536, 1.62265, 2.19076, 2.17437, 1.17379, 1.62116, 1.44191, 3.61089, 0.996352, 3.1559, 0.91361, 0.5, 3.84622, 3.11552, 1.95067, 1.67398, 3.84838, 0.572116, 1.58161, 1.71341, 3.78449, 0.5, 2.74025, 2.74401, 0.587667, 0.792352, 2.42444, 2.71128, 1.82558, 2.22267, 2.12212, 1.556, 2.27243, 2.0012, 2.25107, 2.59722, 2.95243, 2.40883, 0.5, 1.29045
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55062, operands = 136364
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 1, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 3702 itr]: obj = (19), mov = 3702, inf = 61.9%, acc = 31.2%, imp = 144

number of satisfied efficiency constraints: 19
[2 sec, 42911 itr]: obj = (21), mov = 42911, inf = 74.8%, acc = 15.9%, imp = 146

number of satisfied efficiency constraints: 21
[3 sec, 69312 itr]: obj = (22), mov = 69312, inf = 69.1%, acc = 18.5%, imp = 147

number of satisfied efficiency constraints: 22
[4 sec, 89385 itr]: obj = (22), mov = 89385, inf = 61.2%, acc = 22.3%, imp = 147

number of satisfied efficiency constraints: 22
[5 sec, 108588 itr]: obj = (22), mov = 108588, inf = 55.5%, acc = 25.3%, imp = 147

number of satisfied efficiency constraints: 22
[6 sec, 128819 itr]: obj = (23), mov = 128819, inf = 51.3%, acc = 27.2%, imp = 148

number of satisfied efficiency constraints: 23
[7 sec, 146460 itr]: obj = (23), mov = 146460, inf = 48.2%, acc = 29.1%, imp = 187

number of satisfied efficiency constraints: 23
[8 sec, 162360 itr]: obj = (23), mov = 162360, inf = 45.2%, acc = 30.5%, imp = 188

number of satisfied efficiency constraints: 23
[9 sec, 178408 itr]: obj = (23), mov = 178408, inf = 42.6%, acc = 31.7%, imp = 188

number of satisfied efficiency constraints: 23
[10 sec, 193812 itr]: obj = (23), mov = 193812, inf = 40.5%, acc = 32.7%, imp = 188

number of satisfied efficiency constraints: 23
[11 sec, 209482 itr]: obj = (23), mov = 209482, inf = 38.7%, acc = 33.6%, imp = 188

number of satisfied efficiency constraints: 23
[12 sec, 225164 itr]: obj = (23), mov = 225164, inf = 37.3%, acc = 34.3%, imp = 188

number of satisfied efficiency constraints: 23
[13 sec, 240763 itr]: obj = (23), mov = 240763, inf = 36%, acc = 34.8%, imp = 188

number of satisfied efficiency constraints: 23
[14 sec, 257493 itr]: obj = (23), mov = 257493, inf = 34.7%, acc = 35.2%, imp = 189

number of satisfied efficiency constraints: 23
[15 sec, 273381 itr]: obj = (23), mov = 273381, inf = 33.6%, acc = 35.4%, imp = 189

number of satisfied efficiency constraints: 23
[16 sec, 289620 itr]: obj = (23), mov = 289620, inf = 32.7%, acc = 35.6%, imp = 189

number of satisfied efficiency constraints: 23
[17 sec, 307102 itr]: obj = (24), mov = 307102, inf = 31.7%, acc = 35.7%, imp = 190

number of satisfied efficiency constraints: 24
[18 sec, 326187 itr]: obj = (24), mov = 326187, inf = 30.9%, acc = 35.5%, imp = 190

number of satisfied efficiency constraints: 24
[19 sec, 345318 itr]: obj = (24), mov = 345318, inf = 30.2%, acc = 35.4%, imp = 190

number of satisfied efficiency constraints: 24
[20 sec, 363734 itr]: obj = (24), mov = 363734, inf = 29.5%, acc = 35.2%, imp = 190

number of satisfied efficiency constraints: 24
[21 sec, 382526 itr]: obj = (24), mov = 382526, inf = 28.8%, acc = 35.1%, imp = 190

number of satisfied efficiency constraints: 24
[22 sec, 400034 itr]: obj = (24), mov = 400034, inf = 28.3%, acc = 35.1%, imp = 190

number of satisfied efficiency constraints: 24
[23 sec, 417879 itr]: obj = (24), mov = 417879, inf = 27.8%, acc = 35.1%, imp = 190

number of satisfied efficiency constraints: 24
[24 sec, 437358 itr]: obj = (25), mov = 437358, inf = 27.4%, acc = 34.9%, imp = 191

number of satisfied efficiency constraints: 25
[25 sec, 457216 itr]: obj = (25), mov = 457216, inf = 27%, acc = 34.6%, imp = 191

number of satisfied efficiency constraints: 25
[26 sec, 477007 itr]: obj = (25), mov = 477007, inf = 26.5%, acc = 34.4%, imp = 191

number of satisfied efficiency constraints: 25
[27 sec, 489858 itr]: obj = (25), mov = 489858, inf = 26.3%, acc = 34.3%, imp = 191

number of satisfied efficiency constraints: 25
[28 sec, 506376 itr]: obj = (25), mov = 506376, inf = 26%, acc = 34.1%, imp = 191

number of satisfied efficiency constraints: 25
[29 sec, 526699 itr]: obj = (25), mov = 526699, inf = 25.7%, acc = 33.9%, imp = 191

number of satisfied efficiency constraints: 25
[30 sec, 546496 itr]: obj = (25), mov = 546496, inf = 25.4%, acc = 33.8%, imp = 191

number of satisfied efficiency constraints: 25
[31 sec, 566578 itr]: obj = (25), mov = 566578, inf = 25.1%, acc = 33.6%, imp = 191

number of satisfied efficiency constraints: 25
[32 sec, 586810 itr]: obj = (25), mov = 586810, inf = 24.9%, acc = 33.4%, imp = 191

number of satisfied efficiency constraints: 25
[33 sec, 607293 itr]: obj = (25), mov = 607293, inf = 24.6%, acc = 33.2%, imp = 191

number of satisfied efficiency constraints: 25
[34 sec, 627951 itr]: obj = (25), mov = 627951, inf = 24.4%, acc = 33%, imp = 191

number of satisfied efficiency constraints: 25
[35 sec, 648606 itr]: obj = (25), mov = 648606, inf = 24.2%, acc = 32.8%, imp = 191

number of satisfied efficiency constraints: 25
[36 sec, 669478 itr]: obj = (25), mov = 669478, inf = 23.9%, acc = 32.7%, imp = 191

number of satisfied efficiency constraints: 25
[37 sec, 689822 itr]: obj = (25), mov = 689822, inf = 23.7%, acc = 32.5%, imp = 191

number of satisfied efficiency constraints: 25
[38 sec, 710537 itr]: obj = (25), mov = 710537, inf = 23.5%, acc = 32.3%, imp = 191

number of satisfied efficiency constraints: 25
[39 sec, 729621 itr]: obj = (25), mov = 729621, inf = 23.4%, acc = 32.3%, imp = 191

number of satisfied efficiency constraints: 25
[40 sec, 750812 itr]: obj = (25), mov = 750812, inf = 23.2%, acc = 32.1%, imp = 191

number of satisfied efficiency constraints: 25
[41 sec, 771692 itr]: obj = (25), mov = 771692, inf = 23.1%, acc = 32%, imp = 191

number of satisfied efficiency constraints: 25
[42 sec, 788963 itr]: obj = (25), mov = 788963, inf = 23%, acc = 31.9%, imp = 191

number of satisfied efficiency constraints: 25
[43 sec, 806516 itr]: obj = (25), mov = 806516, inf = 22.9%, acc = 31.8%, imp = 191

number of satisfied efficiency constraints: 25
[44 sec, 824209 itr]: obj = (25), mov = 824209, inf = 22.8%, acc = 31.7%, imp = 191

number of satisfied efficiency constraints: 25
[45 sec, 844994 itr]: obj = (25), mov = 844994, inf = 22.7%, acc = 31.6%, imp = 191

number of satisfied efficiency constraints: 25
[46 sec, 864762 itr]: obj = (25), mov = 864762, inf = 22.6%, acc = 31.6%, imp = 191

number of satisfied efficiency constraints: 25
[47 sec, 884061 itr]: obj = (25), mov = 884061, inf = 22.5%, acc = 31.5%, imp = 191

number of satisfied efficiency constraints: 25
[48 sec, 902322 itr]: obj = (25), mov = 902322, inf = 22.4%, acc = 31.5%, imp = 191

number of satisfied efficiency constraints: 25
[49 sec, 921599 itr]: obj = (25), mov = 921599, inf = 22.3%, acc = 31.5%, imp = 191

number of satisfied efficiency constraints: 25
[50 sec, 941620 itr]: obj = (25), mov = 941620, inf = 22.2%, acc = 31.4%, imp = 191

number of satisfied efficiency constraints: 25
[51 sec, 961318 itr]: obj = (25), mov = 961318, inf = 22.1%, acc = 31.4%, imp = 191

number of satisfied efficiency constraints: 25
[52 sec, 978457 itr]: obj = (25), mov = 978457, inf = 22%, acc = 31.4%, imp = 191

number of satisfied efficiency constraints: 25
[53 sec, 992672 itr]: obj = (25), mov = 992672, inf = 21.9%, acc = 31.4%, imp = 191

number of satisfied efficiency constraints: 25
[54 sec, 1001839 itr]: obj = (25), mov = 1001839, inf = 21.9%, acc = 31.3%, imp = 191

number of satisfied efficiency constraints: 25
[55 sec, 1016427 itr]: obj = (25), mov = 1016427, inf = 21.8%, acc = 31.3%, imp = 191

number of satisfied efficiency constraints: 25
[56 sec, 1030133 itr]: obj = (25), mov = 1030133, inf = 21.8%, acc = 31.3%, imp = 191

number of satisfied efficiency constraints: 25
[57 sec, 1049353 itr]: obj = (25), mov = 1049353, inf = 21.7%, acc = 31.2%, imp = 191

number of satisfied efficiency constraints: 25
[58 sec, 1068625 itr]: obj = (25), mov = 1068625, inf = 21.6%, acc = 31.2%, imp = 191

number of satisfied efficiency constraints: 25
[59 sec, 1087707 itr]: obj = (25), mov = 1087707, inf = 21.6%, acc = 31.2%, imp = 191

number of satisfied efficiency constraints: 25
[60 sec, 1105693 itr]: obj = (25), mov = 1105693, inf = 21.5%, acc = 31.2%, imp = 225

number of satisfied efficiency constraints: 25
[61 sec, 1118633 itr]: obj = (25), mov = 1118633, inf = 21.5%, acc = 31.4%, imp = 228

number of satisfied efficiency constraints: 25
[62 sec, 1134032 itr]: obj = (25), mov = 1134032, inf = 21.4%, acc = 31.4%, imp = 228

number of satisfied efficiency constraints: 25
[63 sec, 1150418 itr]: obj = (25), mov = 1150418, inf = 21.4%, acc = 31.5%, imp = 228

number of satisfied efficiency constraints: 25
[64 sec, 1166281 itr]: obj = (25), mov = 1166281, inf = 21.3%, acc = 31.6%, imp = 228

number of satisfied efficiency constraints: 25
[65 sec, 1182559 itr]: obj = (25), mov = 1182559, inf = 21.3%, acc = 31.7%, imp = 228

number of satisfied efficiency constraints: 25
[66 sec, 1197681 itr]: obj = (25), mov = 1197681, inf = 21.2%, acc = 31.8%, imp = 228

number of satisfied efficiency constraints: 25
[67 sec, 1211236 itr]: obj = (25), mov = 1211236, inf = 21.1%, acc = 31.8%, imp = 228

number of satisfied efficiency constraints: 25
[68 sec, 1223651 itr]: obj = (25), mov = 1223651, inf = 21.1%, acc = 31.9%, imp = 268

number of satisfied efficiency constraints: 25
[69 sec, 1235399 itr]: obj = (25), mov = 1235399, inf = 21%, acc = 32%, imp = 268

number of satisfied efficiency constraints: 25
[70 sec, 1248393 itr]: obj = (25), mov = 1248393, inf = 21%, acc = 32.1%, imp = 268

number of satisfied efficiency constraints: 25
[71 sec, 1262702 itr]: obj = (25), mov = 1262702, inf = 20.9%, acc = 32.2%, imp = 270

number of satisfied efficiency constraints: 25
[72 sec, 1278089 itr]: obj = (25), mov = 1278089, inf = 20.9%, acc = 32.2%, imp = 270

number of satisfied efficiency constraints: 25
[73 sec, 1294755 itr]: obj = (25), mov = 1294755, inf = 20.8%, acc = 32.2%, imp = 270

number of satisfied efficiency constraints: 25
[74 sec, 1310771 itr]: obj = (25), mov = 1310771, inf = 20.7%, acc = 32.3%, imp = 270

number of satisfied efficiency constraints: 25
[75 sec, 1325331 itr]: obj = (25), mov = 1325331, inf = 20.7%, acc = 32.3%, imp = 270

number of satisfied efficiency constraints: 25
[76 sec, 1342377 itr]: obj = (25), mov = 1342377, inf = 20.6%, acc = 32.3%, imp = 270

number of satisfied efficiency constraints: 25
[77 sec, 1358865 itr]: obj = (25), mov = 1358865, inf = 20.6%, acc = 32.3%, imp = 287

number of satisfied efficiency constraints: 25
[78 sec, 1369614 itr]: obj = (25), mov = 1369614, inf = 20.5%, acc = 32.4%, imp = 307

number of satisfied efficiency constraints: 25
[79 sec, 1382751 itr]: obj = (25), mov = 1382751, inf = 20.5%, acc = 32.4%, imp = 308

number of satisfied efficiency constraints: 25
[80 sec, 1395642 itr]: obj = (25), mov = 1395642, inf = 20.5%, acc = 32.5%, imp = 308

number of satisfied efficiency constraints: 25
[81 sec, 1408404 itr]: obj = (25), mov = 1408404, inf = 20.4%, acc = 32.5%, imp = 308

number of satisfied efficiency constraints: 25
[82 sec, 1423349 itr]: obj = (25), mov = 1423349, inf = 20.4%, acc = 32.6%, imp = 308

number of satisfied efficiency constraints: 25
[83 sec, 1437583 itr]: obj = (25), mov = 1437583, inf = 20.3%, acc = 32.7%, imp = 309

number of satisfied efficiency constraints: 25
[84 sec, 1454209 itr]: obj = (25), mov = 1454209, inf = 20.3%, acc = 32.6%, imp = 309

number of satisfied efficiency constraints: 25
[85 sec, 1469379 itr]: obj = (25), mov = 1469379, inf = 20.2%, acc = 32.6%, imp = 310

number of satisfied efficiency constraints: 25
[86 sec, 1487622 itr]: obj = (25), mov = 1487622, inf = 20.2%, acc = 32.6%, imp = 310

number of satisfied efficiency constraints: 25
[87 sec, 1506106 itr]: obj = (25), mov = 1506106, inf = 20.1%, acc = 32.5%, imp = 310

number of satisfied efficiency constraints: 25
[88 sec, 1523551 itr]: obj = (25), mov = 1523551, inf = 20%, acc = 32.5%, imp = 310

number of satisfied efficiency constraints: 25
[89 sec, 1541209 itr]: obj = (25), mov = 1541209, inf = 20%, acc = 32.5%, imp = 310

number of satisfied efficiency constraints: 25
[90 sec, 1560779 itr]: obj = (25), mov = 1560779, inf = 19.9%, acc = 32.4%, imp = 310

number of satisfied efficiency constraints: 25
[90 sec, 1560779 itr]: obj = (25), mov = 1560779, inf = 19.9%, acc = 32.4%, imp = 310

number of satisfied efficiency constraints: 25
1560779 iterations, 1560779 moves performed in 90 seconds
Feasible solution: obj = (25)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 25
Mapping: 0->11, 1->2, 2->27, 3->28, 4->37, 5->15, 6->38, 7->44, 8->18, 9->16, 10->4, 11->26, 12->23, 13->21, 14->38, 15->34, 16->19, 17->13, 18->12, 19->20, 20->17, 21->11, 22->26, 23->19, 24->34, 25->15, 26->10, 27->23, 28->4, 29->11, 30->33, 31->10, 32->6, 33->10, 34->7, 35->35, 36->7, 37->36, 38->25, 39->42, 40->46, 41->21, 42->8, 43->21, 44->8, 45->25, 46->39, 47->35, 48->32, 49->9, 50->39, 51->13, 52->25, 53->22, 54->15, 55->24, 56->5, 57->5, 58->18, 59->41, 60->13, 61->33, 62->29, 63->0, 64->9, 65->9, 66->47, 67->42, 68->42, 69->1, 70->3, 71->32, 72->43, 73->45, 74->24, 75->19, 76->16, 77->3, 78->17, 79->31, 80->32, 81->31, 82->47, 83->5, 84->17, 85->47, 86->1, 87->43, 88->35, 89->36, 90->25, 91->25, 92->3, 93->43, 94->45, 95->18, 96->45, 97->42, 98->0, 99->14, 100->23, 101->33, 102->14, 103->21, 104->40, 105->12, 106->44, 107->7, 108->7, 109->36, 110->11, 111->43, 112->7, 113->41, 114->29, 115->8, 116->46, 117->3, 118->15, 119->27, 120->43, 121->17, 122->43, 123->1, 124->15, 125->13, 126->20, 127->41, 128->5, 129->12, 130->31, 131->30, 132->0, 133->9, 134->2, 135->26, 136->37, 137->21, 138->0, 139->18, 140->22, 141->41, 142->40, 143->20, 144->16, 145->44, 146->40, 147->8, 148->25, 149->4, 150->38, 151->33, 152->0, 153->41, 154->29, 155->27, 156->32, 157->17, 158->14, 159->12, 160->9, 161->44, 162->39, 163->42, 164->20, 165->35, 166->35, 167->19, 168->2, 169->38, 170->45, 171->32, 172->6, 173->26, 174->20, 175->29, 176->6, 177->1, 178->47, 179->1, 180->23, 181->29, 182->11, 183->4, 184->32, 185->46, 186->23, 187->40, 188->39, 189->36, 190->22, 191->34, 192->30, 193->27, 194->41, 195->33, 196->34, 197->24, 198->1, 199->31, 200->34, 201->12, 202->47, 203->39, 204->38, 205->17, 206->44, 207->5, 208->2, 209->2, 210->37, 211->14, 212->38, 213->30, 214->19, 215->14, 216->19, 217->46, 218->39, 219->33, 220->18, 221->3, 222->40, 223->27, 224->16, 225->28, 226->10, 227->27, 228->42, 229->28, 230->4, 231->16, 232->28, 233->28, 234->30, 235->21, 236->9, 237->10, 238->28, 239->12, 240->37, 241->26, 242->15, 243->20, 244->30, 245->31, 246->24, 247->22, 248->6, 249->8, 250->36, 251->0, 252->45, 253->13, 254->8, 255->4, 256->3, 257->6, 258->29, 259->44, 260->24, 261->11, 262->14, 263->36, 264->35, 265->18, 266->5, 267->31, 268->26, 269->7, 270->47, 271->40, 272->22, 273->16, 274->6, 275->46, 276->2, 277->30, 278->23, 279->34, 280->24, 281->45, 282->37, 283->46, 284->10, 285->22, 286->37, 287->13
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	2.0229	1.87047	2.27752	2.95971	2.12637	1.34111
	2.21586	0.952721	1.79235	2.15014	2.91619	2.28742
	2.38778	2.06107	1.36643	2.03544	1.76713	2.33454
	4.24994	1.71902	1.75441	1.61498	1.95312	2.59136
	2.58739	11.2083	2.25956	1.55409	1.67145	2.24224
	2.48925	1.96502	0.670907	1.77339	1.31141	2.19471
	2.45121	2.32425	1.48875	1.27815	3.02168	1.79027
	2.18676	4.48768	1.57775	7.24691	3.15976	2.3111
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	1	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 3 --global-tlim 90 --seed 1

Starting the Optimization Process
