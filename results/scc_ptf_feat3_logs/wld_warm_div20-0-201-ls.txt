LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
2.32528, 2.69661, 2.33489, 2.61968, 1.39219, 0.631348, 2.14457, 2.09786, 2.0156, 1.40994, 1.98032, 2.35171, 2.14681, 5.51269, 0.508952, 0.535891, 0.5, 2.79566, 3.22107, 2.76887, 0.692256, 1.46167, 0.850599, 3.00553, 2.23892, 1.87871, 1.2304, 2.62408, 1.53786, 2.49003, 2.48813, 1.34152, 1.20766, 2.56005, 2.63035, 1.77229, 2.66158, 1.90349, 1.20222, 0.5, 2.9669, 2.76581, 1.98381, 1.59411, 2.21138, 1.7083, 2.1388, 2.3636, 2.19001, 0.5, 4.03973, 1.23369, 3.32551, 0.711065, 3.24643, 1.1781, 4.52644, 1.21575, 0.964015, 0.869259, 0.938447, 3.922, 0.843693, 1.07256, 1.25476, 3.96854, 1.39503, 3.56897, 0.5, 2.38786, 2.90278, 1.24536, 2.93462, 0.896481, 1.06807, 2.45856, 1.87207, 2.7702, 1.69628, 2.78384, 1.12446, 0.5, 4.4353, 1.46012, 2.38702, 1.72511, 2.41288, 0.580633, 2.60767, 2.28668, 3.58483, 1.62044, 2.45795, 1.4516, 1.25484, 1.63034, 0.91103, 2.63715, 3.19185, 2.24391, 1.0621, 1.95395, 1.32327, 1.66125, 1.74398, 2.48313, 2.38041, 2.40796, 2.4193, 1.97648, 3.89831, 1.45677, 1.43626, 0.812887, 1.06923, 2.69108, 2.69001, 2.40738, 0.5, 2.6423, 2.42072, 2.2702, 2.33269, 1.11317, 2.14266, 1.72055, 1.81811, 2.32174, 0.5, 1.62056, 3.11018, 2.62942, 0.707084, 5.29154, 0.5, 1.5014, 0.779448, 3.22052, 0.5, 1.39923, 2.29096, 2.96028, 0.653012, 4.19651, 2.63646, 4.68393, 16.8611, 9.38735, 10.584, 15.8471, 12.4193, 11.3196, 8.5879, 5.72528, 10.3222, 11.6257, 2.31026, 0.923478, 2.2913, 2.17043, 2.2971, 2.00743, 2.10152, 2.2143, 1.84459, 1.79931, 2.20512, 1.83515, 2.41186, 0.899128, 2.56038, 3.06383, 0.544909, 2.51989, 0.713665, 2.36931, 5.49921, 0.839979, 0.5, 2.07783, 5.95985, 14.7485, 5.49833, 11.4358, 10.0437, 12.3138, 10.8716, 6.94639, 11.4038, 9.4133, 11.2042, 10.1607, 2.11606, 2.1784, 1.63394, 2.49925, 1.77286, 1.79949, 2.09519, 0.5, 3.13735, 2.57248, 0.589622, 3.10535, 0.5, 2.33476, 2.6411, 2.10101, 1.2168, 3.20633, 0.714263, 1.016, 4.23816, 2.98103, 2.23276, 0.817779, 2.06887, 21.1907, 15.5591, 1.76883, 12.7602, 6.65228, 9.39986, 0.989694, 12.6479, 13.8169, 5.73979, 17.4058, 0.553682, 0.732482, 4.94519, 3.15069, 2.11795, 0.5, 2.1616, 1.82805, 1.98405, 2.41493, 1.33159, 2.27978, 1.97777, 1.89826, 2.04107, 2.08565, 1.86146, 2.1358, 1.84052, 2.09338, 3.1304, 0.617474, 1.88191, 2.43632, 1.05405, 2.49331, 2.40819, 2.42397, 2.30645, 1.31403, 1.10261, 2.56829, 2.2386, 2.10825, 2.51723, 1.46503, 1.65952, 0.5, 3.22461, 0.849226, 3.22171, 2.54493, 1.44435, 2.97665, 2.08928, 0.529936, 4.44981, 0.509979, 2.21915, 2.3045, 1.9667, 1.74361, 1.68983, 2.07621, 0.917113, 3.85127, 2.09248, 1.7479, 0.810541, 2.58069
cpi min/max: 0.5/21.1907
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 55179, operands = 136540
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 201, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 3380 itr]: obj = (20), mov = 3380, inf = 60.9%, acc = 31%, imp = 176

number of satisfied efficiency constraints: 20
[2 sec, 36122 itr]: obj = (25), mov = 36122, inf = 82.2%, acc = 12.4%, imp = 181

number of satisfied efficiency constraints: 25
[3 sec, 61006 itr]: obj = (25), mov = 61006, inf = 82.3%, acc = 11.9%, imp = 181

number of satisfied efficiency constraints: 25
[4 sec, 76984 itr]: obj = (25), mov = 76984, inf = 80.7%, acc = 12.7%, imp = 181

number of satisfied efficiency constraints: 25
[5 sec, 91265 itr]: obj = (26), mov = 91265, inf = 77.6%, acc = 14.4%, imp = 182

number of satisfied efficiency constraints: 26
[6 sec, 106743 itr]: obj = (26), mov = 106743, inf = 72.7%, acc = 16.8%, imp = 182

number of satisfied efficiency constraints: 26
[7 sec, 122224 itr]: obj = (26), mov = 122224, inf = 68%, acc = 19.3%, imp = 182

number of satisfied efficiency constraints: 26
[8 sec, 138566 itr]: obj = (26), mov = 138566, inf = 64%, acc = 21.4%, imp = 182

number of satisfied efficiency constraints: 26
[9 sec, 151979 itr]: obj = (26), mov = 151979, inf = 61.1%, acc = 23.3%, imp = 210

number of satisfied efficiency constraints: 26
[10 sec, 164514 itr]: obj = (26), mov = 164514, inf = 58.6%, acc = 25.1%, imp = 210

number of satisfied efficiency constraints: 26
[11 sec, 175196 itr]: obj = (26), mov = 175196, inf = 56.6%, acc = 26.7%, imp = 210

number of satisfied efficiency constraints: 26
[12 sec, 185960 itr]: obj = (26), mov = 185960, inf = 54.6%, acc = 28.1%, imp = 210

number of satisfied efficiency constraints: 26
[13 sec, 196065 itr]: obj = (26), mov = 196065, inf = 52.7%, acc = 29.3%, imp = 210

number of satisfied efficiency constraints: 26
[14 sec, 206580 itr]: obj = (26), mov = 206580, inf = 51%, acc = 30.4%, imp = 210

number of satisfied efficiency constraints: 26
[15 sec, 217998 itr]: obj = (26), mov = 217998, inf = 49.4%, acc = 31.3%, imp = 211

number of satisfied efficiency constraints: 26
[16 sec, 231519 itr]: obj = (26), mov = 231519, inf = 47.6%, acc = 32.2%, imp = 211

number of satisfied efficiency constraints: 26
[17 sec, 245895 itr]: obj = (26), mov = 245895, inf = 45.9%, acc = 33.1%, imp = 211

number of satisfied efficiency constraints: 26
[18 sec, 258015 itr]: obj = (26), mov = 258015, inf = 44.6%, acc = 33.7%, imp = 211

number of satisfied efficiency constraints: 26
[19 sec, 271677 itr]: obj = (26), mov = 271677, inf = 43.4%, acc = 34.3%, imp = 211

number of satisfied efficiency constraints: 26
[20 sec, 285081 itr]: obj = (26), mov = 285081, inf = 42.2%, acc = 34.9%, imp = 211

number of satisfied efficiency constraints: 26
[21 sec, 296635 itr]: obj = (26), mov = 296635, inf = 41.3%, acc = 35.4%, imp = 211

number of satisfied efficiency constraints: 26
[22 sec, 307996 itr]: obj = (26), mov = 307996, inf = 40.4%, acc = 35.8%, imp = 211

number of satisfied efficiency constraints: 26
[23 sec, 318788 itr]: obj = (26), mov = 318788, inf = 39.7%, acc = 36.2%, imp = 211

number of satisfied efficiency constraints: 26
[24 sec, 330818 itr]: obj = (27), mov = 330818, inf = 38.9%, acc = 36.4%, imp = 212

number of satisfied efficiency constraints: 27
[25 sec, 345061 itr]: obj = (27), mov = 345061, inf = 38.1%, acc = 36.7%, imp = 212

number of satisfied efficiency constraints: 27
[26 sec, 359708 itr]: obj = (27), mov = 359708, inf = 37.2%, acc = 36.9%, imp = 212

number of satisfied efficiency constraints: 27
[27 sec, 372636 itr]: obj = (27), mov = 372636, inf = 36.6%, acc = 37%, imp = 212

number of satisfied efficiency constraints: 27
[28 sec, 385374 itr]: obj = (27), mov = 385374, inf = 35.9%, acc = 37.2%, imp = 212

number of satisfied efficiency constraints: 27
[29 sec, 396630 itr]: obj = (27), mov = 396630, inf = 35.5%, acc = 37.4%, imp = 212

number of satisfied efficiency constraints: 27
[30 sec, 407706 itr]: obj = (27), mov = 407706, inf = 35%, acc = 37.5%, imp = 212

number of satisfied efficiency constraints: 27
[31 sec, 420405 itr]: obj = (27), mov = 420405, inf = 34.6%, acc = 37.7%, imp = 212

number of satisfied efficiency constraints: 27
[32 sec, 432808 itr]: obj = (27), mov = 432808, inf = 34.1%, acc = 37.7%, imp = 212

number of satisfied efficiency constraints: 27
[33 sec, 447744 itr]: obj = (27), mov = 447744, inf = 33.6%, acc = 37.8%, imp = 212

number of satisfied efficiency constraints: 27
[34 sec, 462572 itr]: obj = (27), mov = 462572, inf = 33.1%, acc = 38%, imp = 212

number of satisfied efficiency constraints: 27
[35 sec, 473218 itr]: obj = (27), mov = 473218, inf = 32.8%, acc = 38.1%, imp = 212

number of satisfied efficiency constraints: 27
[36 sec, 481986 itr]: obj = (27), mov = 481986, inf = 32.5%, acc = 38.2%, imp = 212

number of satisfied efficiency constraints: 27
[37 sec, 492990 itr]: obj = (27), mov = 492990, inf = 32.2%, acc = 38.3%, imp = 212

number of satisfied efficiency constraints: 27
[38 sec, 505220 itr]: obj = (27), mov = 505220, inf = 31.9%, acc = 38.4%, imp = 212

number of satisfied efficiency constraints: 27
[39 sec, 516526 itr]: obj = (27), mov = 516526, inf = 31.6%, acc = 38.5%, imp = 212

number of satisfied efficiency constraints: 27
[40 sec, 529305 itr]: obj = (27), mov = 529305, inf = 31.3%, acc = 38.6%, imp = 212

number of satisfied efficiency constraints: 27
[41 sec, 544203 itr]: obj = (27), mov = 544203, inf = 30.9%, acc = 38.7%, imp = 212

number of satisfied efficiency constraints: 27
[42 sec, 559184 itr]: obj = (28), mov = 559184, inf = 30.6%, acc = 38.8%, imp = 213

number of satisfied efficiency constraints: 28
[43 sec, 575099 itr]: obj = (28), mov = 575099, inf = 30.3%, acc = 38.8%, imp = 213

number of satisfied efficiency constraints: 28
[44 sec, 591543 itr]: obj = (28), mov = 591543, inf = 30%, acc = 38.8%, imp = 213

number of satisfied efficiency constraints: 28
[45 sec, 608875 itr]: obj = (28), mov = 608875, inf = 29.6%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[46 sec, 623603 itr]: obj = (28), mov = 623603, inf = 29.4%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[47 sec, 637389 itr]: obj = (28), mov = 637389, inf = 29.1%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[48 sec, 651919 itr]: obj = (28), mov = 651919, inf = 28.9%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[49 sec, 665285 itr]: obj = (28), mov = 665285, inf = 28.7%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[50 sec, 678059 itr]: obj = (28), mov = 678059, inf = 28.5%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[51 sec, 690514 itr]: obj = (28), mov = 690514, inf = 28.3%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[52 sec, 703290 itr]: obj = (28), mov = 703290, inf = 28.1%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[53 sec, 718968 itr]: obj = (28), mov = 718968, inf = 27.9%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[54 sec, 733078 itr]: obj = (28), mov = 733078, inf = 27.7%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[55 sec, 748599 itr]: obj = (28), mov = 748599, inf = 27.5%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[56 sec, 764098 itr]: obj = (28), mov = 764098, inf = 27.4%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[57 sec, 779763 itr]: obj = (28), mov = 779763, inf = 27.2%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[58 sec, 795853 itr]: obj = (28), mov = 795853, inf = 27%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[59 sec, 811958 itr]: obj = (28), mov = 811958, inf = 26.8%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[60 sec, 828947 itr]: obj = (28), mov = 828947, inf = 26.6%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[61 sec, 845344 itr]: obj = (28), mov = 845344, inf = 26.5%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[62 sec, 861897 itr]: obj = (28), mov = 861897, inf = 26.3%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[63 sec, 877902 itr]: obj = (28), mov = 877902, inf = 26.2%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[64 sec, 893976 itr]: obj = (28), mov = 893976, inf = 26%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[65 sec, 909797 itr]: obj = (28), mov = 909797, inf = 25.9%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[66 sec, 926312 itr]: obj = (28), mov = 926312, inf = 25.7%, acc = 38.7%, imp = 213

number of satisfied efficiency constraints: 28
[67 sec, 942728 itr]: obj = (28), mov = 942728, inf = 25.6%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[68 sec, 959423 itr]: obj = (28), mov = 959423, inf = 25.5%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[69 sec, 975903 itr]: obj = (28), mov = 975903, inf = 25.4%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[70 sec, 992694 itr]: obj = (28), mov = 992694, inf = 25.2%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[71 sec, 1009167 itr]: obj = (28), mov = 1009167, inf = 25.1%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[72 sec, 1025416 itr]: obj = (28), mov = 1025416, inf = 25%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[73 sec, 1041865 itr]: obj = (28), mov = 1041865, inf = 24.9%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[74 sec, 1057556 itr]: obj = (28), mov = 1057556, inf = 24.8%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[75 sec, 1074372 itr]: obj = (28), mov = 1074372, inf = 24.7%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[76 sec, 1091245 itr]: obj = (28), mov = 1091245, inf = 24.6%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[77 sec, 1107652 itr]: obj = (28), mov = 1107652, inf = 24.5%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[78 sec, 1124458 itr]: obj = (28), mov = 1124458, inf = 24.4%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[79 sec, 1141472 itr]: obj = (28), mov = 1141472, inf = 24.3%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[80 sec, 1158526 itr]: obj = (28), mov = 1158526, inf = 24.2%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[81 sec, 1174768 itr]: obj = (28), mov = 1174768, inf = 24.2%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[82 sec, 1191473 itr]: obj = (28), mov = 1191473, inf = 24.1%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[83 sec, 1209085 itr]: obj = (28), mov = 1209085, inf = 24%, acc = 38.6%, imp = 213

number of satisfied efficiency constraints: 28
[84 sec, 1225646 itr]: obj = (28), mov = 1225646, inf = 23.9%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
[85 sec, 1242159 itr]: obj = (28), mov = 1242159, inf = 23.9%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
[86 sec, 1258890 itr]: obj = (28), mov = 1258890, inf = 23.8%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
[87 sec, 1276036 itr]: obj = (28), mov = 1276036, inf = 23.7%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
[88 sec, 1292826 itr]: obj = (28), mov = 1292826, inf = 23.6%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
[89 sec, 1309388 itr]: obj = (28), mov = 1309388, inf = 23.6%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
[90 sec, 1325850 itr]: obj = (28), mov = 1325850, inf = 23.5%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
[90 sec, 1325850 itr]: obj = (28), mov = 1325850, inf = 23.5%, acc = 38.5%, imp = 213

number of satisfied efficiency constraints: 28
1325850 iterations, 1325850 moves performed in 90 seconds
Feasible solution: obj = (28)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 28
Mapping: 0->27, 1->1, 2->42, 3->6, 4->17, 5->5, 6->29, 7->6, 8->20, 9->23, 10->18, 11->16, 12->17, 13->34, 14->36, 15->5, 16->24, 17->8, 18->43, 19->32, 20->27, 21->31, 22->1, 23->40, 24->21, 25->0, 26->27, 27->24, 28->11, 29->16, 30->10, 31->15, 32->44, 33->23, 34->9, 35->18, 36->31, 37->44, 38->35, 39->37, 40->47, 41->18, 42->15, 43->36, 44->0, 45->26, 46->33, 47->0, 48->25, 49->20, 50->40, 51->29, 52->7, 53->25, 54->18, 55->13, 56->26, 57->3, 58->14, 59->17, 60->31, 61->23, 62->17, 63->24, 64->16, 65->22, 66->20, 67->15, 68->19, 69->8, 70->26, 71->28, 72->23, 73->47, 74->6, 75->39, 76->7, 77->9, 78->3, 79->41, 80->44, 81->14, 82->39, 83->23, 84->8, 85->42, 86->20, 87->21, 88->39, 89->10, 90->34, 91->30, 92->29, 93->1, 94->12, 95->12, 96->28, 97->30, 98->40, 99->20, 100->22, 101->38, 102->11, 103->41, 104->23, 105->35, 106->25, 107->12, 108->5, 109->39, 110->33, 111->19, 112->36, 113->33, 114->4, 115->46, 116->3, 117->11, 118->44, 119->14, 120->37, 121->43, 122->43, 123->13, 124->19, 125->19, 126->41, 127->30, 128->14, 129->37, 130->43, 131->2, 132->0, 133->8, 134->5, 135->32, 136->16, 137->34, 138->28, 139->30, 140->9, 141->2, 142->11, 143->10, 144->40, 145->47, 146->12, 147->35, 148->46, 149->34, 150->25, 151->9, 152->6, 153->26, 154->40, 155->21, 156->33, 157->29, 158->47, 159->8, 160->39, 161->31, 162->7, 163->14, 164->9, 165->32, 166->27, 167->18, 168->12, 169->44, 170->3, 171->40, 172->24, 173->42, 174->1, 175->7, 176->41, 177->13, 178->16, 179->15, 180->36, 181->41, 182->34, 183->45, 184->45, 185->46, 186->29, 187->46, 188->25, 189->45, 190->17, 191->24, 192->1, 193->45, 194->6, 195->22, 196->7, 197->3, 198->36, 199->33, 200->43, 201->10, 202->13, 203->22, 204->19, 205->38, 206->14, 207->36, 208->27, 209->39, 210->47, 211->21, 212->17, 213->6, 214->22, 215->5, 216->2, 217->4, 218->43, 219->4, 220->30, 221->1, 222->5, 223->4, 224->45, 225->4, 226->32, 227->45, 228->32, 229->31, 230->11, 231->46, 232->2, 233->31, 234->33, 235->42, 236->37, 237->46, 238->16, 239->38, 240->13, 241->25, 242->4, 243->28, 244->21, 245->8, 246->10, 247->38, 248->32, 249->12, 250->3, 251->42, 252->24, 253->42, 254->47, 255->26, 256->18, 257->15, 258->35, 259->35, 260->0, 261->13, 262->7, 263->44, 264->35, 265->27, 266->30, 267->15, 268->10, 269->21, 270->41, 271->38, 272->2, 273->29, 274->34, 275->26, 276->20, 277->2, 278->11, 279->28, 280->37, 281->38, 282->9, 283->22, 284->28, 285->37, 286->19, 287->0
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.99668	2.41347	2.36172	1.97397	6.81274	2.38403
	3.16475	2.32642	2.86138	3.6288	2.76767	2.1389
	4.19726	1.30115	1.57695	1.8559	1.45126	3.44906
	2.31774	1.18842	1.79776	3.31128	2.78654	2.33845
	2.65938	5.16713	2.96613	1.36164	1.42969	3.02688
	3.99389	1.3836	2.58224	1.97031	6.35222	3.06719
	2.28256	1.66051	2.28579	2.83024	4.37661	4.65922
	2.22293	4.93844	1.18329	10.5208	6.35015	2.32684
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	0	0	0	1	1
	1	1	0	1	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 0 --global-tlim 90 --seed 201

Starting the Optimization Process
