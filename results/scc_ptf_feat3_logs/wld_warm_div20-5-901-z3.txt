==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 5 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 901

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.069/1.069
- lns time: 1.069171
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006344
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004636
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 0.939699
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006386
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.012841
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 1.096961
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004933
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005124
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004669
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007599
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006328
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.010615
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.008537
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004653
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.445/28.634
- lns time: 1.445319
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004942
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 0.888674
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006741
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.012063
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.744/37.290
- lns time: 1.744029
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004540
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005270
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006952
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004510
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.864/47.175
- lns time: 1.863851
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006601
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.013786
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005273
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006696
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010404
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.006126
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007437
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005781
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004878
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.109037
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005686
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005879
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005369
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.007572
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008087
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004826
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004773
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004629
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.004962
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009153
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.005575
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 1.589270
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 23

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 23
number of iterations: 48
total time: 90.007187

Final Solution: 0->19, 1->12, 2->11, 3->27, 4->2, 5->16, 6->36, 7->42, 8->14, 9->22, 10->44, 11->10, 12->26, 13->45, 14->41, 15->24, 16->31, 17->36, 18->4, 19->32, 20->34, 21->1, 22->31, 23->22, 24->10, 25->3, 26->27, 27->5, 28->8, 29->4, 30->17, 31->25, 32->3, 33->22, 34->11, 35->22, 36->15, 37->14, 38->18, 39->19, 40->17, 41->16, 42->23, 43->12, 44->26, 45->40, 46->46, 47->21, 48->40, 49->8, 50->15, 51->9, 52->5, 53->29, 54->20, 55->2, 56->25, 57->38, 58->4, 59->29, 60->36, 61->26, 62->29, 63->39, 64->0, 65->30, 66->30, 67->15, 68->45, 69->11, 70->40, 71->6, 72->8, 73->28, 74->36, 75->31, 76->24, 77->30, 78->18, 79->3, 80->22, 81->24, 82->20, 83->34, 84->39, 85->37, 86->23, 87->28, 88->32, 89->11, 90->5, 91->28, 92->15, 93->1, 94->39, 95->37, 96->6, 97->22, 98->43, 99->30, 100->5, 101->11, 102->35, 103->5, 104->43, 105->42, 106->8, 107->46, 108->9, 109->47, 110->32, 111->6, 112->7, 113->13, 114->20, 115->38, 116->7, 117->1, 118->25, 119->40, 120->1, 121->2, 122->7, 123->43, 124->41, 125->10, 126->0, 127->14, 128->33, 129->8, 130->26, 131->29, 132->2, 133->32, 134->43, 135->45, 136->41, 137->9, 138->36, 139->32, 140->38, 141->41, 142->37, 143->4, 144->42, 145->20, 146->10, 147->16, 148->21, 149->10, 150->43, 151->33, 152->20, 153->0, 154->32, 155->3, 156->17, 157->28, 158->35, 159->45, 160->44, 161->47, 162->33, 163->14, 164->12, 165->42, 166->7, 167->4, 168->46, 169->23, 170->12, 171->19, 172->17, 173->33, 174->20, 175->47, 176->7, 177->21, 178->21, 179->8, 180->27, 181->16, 182->6, 183->34, 184->3, 185->35, 186->13, 187->35, 188->13, 189->44, 190->21, 191->44, 192->47, 193->14, 194->23, 195->15, 196->40, 197->31, 198->27, 199->1, 200->24, 201->18, 202->43, 203->38, 204->44, 205->42, 206->31, 207->23, 208->24, 209->14, 210->9, 211->9, 212->28, 213->0, 214->39, 215->24, 216->5, 217->41, 218->30, 219->19, 220->26, 221->26, 222->34, 223->9, 224->16, 225->15, 226->21, 227->35, 228->16, 229->4, 230->34, 231->38, 232->42, 233->12, 234->37, 235->37, 236->6, 237->17, 238->46, 239->13, 240->18, 241->17, 242->41, 243->23, 244->46, 245->29, 246->11, 247->2, 248->13, 249->34, 250->45, 251->2, 252->18, 253->39, 254->19, 255->45, 256->29, 257->28, 258->33, 259->31, 260->0, 261->46, 262->36, 263->30, 264->33, 265->3, 266->39, 267->38, 268->25, 269->10, 270->47, 271->44, 272->27, 273->6, 274->1, 275->25, 276->12, 277->19, 278->35, 279->40, 280->7, 281->18, 282->47, 283->27, 284->25, 285->0, 286->13, 287->37

Average CPI map:
	1.12,	5.74,	2.05,	4.45,	1.38,	1.22
	3.91,	2.01,	3.78,	3.61,	2.64,	1.82
	3.41,	3.37,	3.34,	3.24,	3.12,	3.10
	3.09,	3.08,	5.64,	2.75,	2.75,	2.68
	2.68,	2.42,	2.40,	2.35,	2.33,	2.30
	2.27,	2.26,	2.24,	2.22,	4.80,	2.11
	6.47,	2.20,	4.20,	1.70,	5.82,	2.19
	2.19,	2.19,	2.19,	2.18,	4.87,	4.15

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	0,	0,	1,	1
	1,	0,	0,	0,	0,	0
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	1,	1
