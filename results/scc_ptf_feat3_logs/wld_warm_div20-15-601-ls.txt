LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tmp.lsp...
Run input...
Workload Information:
0.5, 0.793797, 3.01448, 2.89409, 2.97166, 1.82598, 1.46422, 1.02987, 4.33222, 3.15438, 0.5, 1.51931, 2.09408, 1.95425, 1.74965, 2.13165, 2.02029, 2.05007, 2.40715, 0.751014, 2.93856, 1.5686, 1.81978, 2.51489, 0.810358, 3.37159, 2.01811, 0.5, 3.8527, 1.44724, 1.43318, 2.33024, 1.63724, 1.9352, 2.17271, 2.49144, 2.62915, 2.43595, 1.70695, 1.6611, 2.1134, 1.45345, 2.13604, 2.94479, 1.10006, 2.21292, 2.90612, 0.700066, 2.24627, 2.07499, 0.712951, 3.80092, 1.44741, 1.71746, 1.98396, 2.25489, 2.0251, 1.72142, 1.96386, 2.05077, 0.724777, 5.76477, 1.8992, 2.38764, 0.513167, 0.710449, 31.2107, 27.2561, 18.7677, 35, 11.0826, 8.68286, 1.51957, 3.17254, 0.5, 1.02075, 1.81243, 3.9747, 1.72133, 0.667203, 2.55119, 2.14292, 2.30777, 2.60958, 0.631906, 2.19898, 4.19696, 1.52562, 0.851563, 2.59497, 3.36286, 3.496, 0.975857, 1.57842, 2.08687, 0.5, 28.3048, 15.5874, 35, 25.0792, 12.9663, 15.0623, 1.94871, 2.30428, 2.28502, 1.8023, 1.39475, 2.26493, 3.11261, 1.48502, 2.7242, 1.50319, 2.67498, 0.5, 0.94498, 1.14016, 2.84732, 3.11272, 2.697, 1.25782, 1.49547, 2.60328, 2.37214, 1.91276, 1.0462, 2.57016, 0.556693, 2.96248, 3.93222, 0.5, 1.54412, 2.50448, 20.8662, 35, 16.4223, 16.63, 24.1447, 18.937, 35, 18.8404, 13.9087, 21.4826, 32.0696, 10.6987, 2.03447, 3.84498, 3.47505, 0.840083, 1.07748, 0.727933, 3.5496, 3.73055, 1.03076, 0.5, 2.33398, 0.855113, 1.93585, 0.915647, 0.553338, 1.61452, 1.35227, 5.62837, 3.13578, 3.41479, 0.603717, 1.40872, 0.5, 2.93699, 1.64791, 2.04324, 0.971774, 0.682902, 1.77925, 4.87493, 0.679022, 1.01475, 0.78205, 2.11252, 0.933492, 6.47816, 1.34254, 1.35093, 0.5, 3.04181, 3.12938, 2.63533, 0.5, 1.80789, 4.25635, 2.83402, 0.859458, 1.74228, 2.29634, 1.85749, 1.87562, 2.42096, 1.42836, 2.12123, 3.7915, 2.21285, 0.575129, 0.780089, 4.14043, 0.5, 0.5, 0.855756, 1.6246, 2.73087, 3.7941, 2.49466, 3.98145, 0.5, 1.82333, 1.23156, 2.84167, 1.62198, 4.5138, 2.24199, 1.19583, 2.07482, 0.739494, 1.23407, 1.85496, 0.557577, 2.51568, 1.41475, 4.12757, 1.52947, 1.92157, 2.25752, 1.90951, 2.10541, 1.70878, 2.09721, 0.957288, 0.997952, 2.95043, 3.22083, 3.20122, 0.672285, 3.6616, 3.14231, 2.4321, 1.26004, 0.5, 1.00395, 0.996237, 3.61131, 0.889852, 2.02482, 2.91463, 1.56314, 0.5, 2.30284, 5.09568, 1.76483, 0.697285, 1.63936, 2.35738, 1.92499, 1.23851, 2.49156, 2.46238, 1.52517, 2.20721, 2.20715, 1.75975, 2.13094, 1.83949, 1.85546, 2.53771, 1.97268, 2.60817, 0.5, 2.55362, 1.82783, 0.623567, 3.18571, 0.742433, 2.03774, 0.739147, 4.6714, 1.30779, 2.98554, 3.00575, 2.50208, 1.69884, 0.5
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 54812, operands = 135836
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 601, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 48


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

number of satisfied efficiency constraints: 34
[1 sec, 19933 itr]: obj = (26), mov = 19933, inf = 71.4%, acc = 18%, imp = 170

number of satisfied efficiency constraints: 26
[2 sec, 52078 itr]: obj = (28), mov = 52078, inf = 72.3%, acc = 15.8%, imp = 172

number of satisfied efficiency constraints: 28
[3 sec, 76247 itr]: obj = (30), mov = 76247, inf = 64.9%, acc = 17.8%, imp = 174

number of satisfied efficiency constraints: 30
[4 sec, 99869 itr]: obj = (30), mov = 99869, inf = 56.6%, acc = 20.4%, imp = 174

number of satisfied efficiency constraints: 30
[5 sec, 123515 itr]: obj = (30), mov = 123515, inf = 51%, acc = 22.2%, imp = 174

number of satisfied efficiency constraints: 30
[6 sec, 141861 itr]: obj = (30), mov = 141861, inf = 47.4%, acc = 25%, imp = 202

number of satisfied efficiency constraints: 30
[7 sec, 158507 itr]: obj = (30), mov = 158507, inf = 44.2%, acc = 27.4%, imp = 203

number of satisfied efficiency constraints: 30
[8 sec, 175684 itr]: obj = (30), mov = 175684, inf = 41.5%, acc = 28.8%, imp = 205

number of satisfied efficiency constraints: 30
[9 sec, 195370 itr]: obj = (30), mov = 195370, inf = 39.1%, acc = 29.5%, imp = 206

number of satisfied efficiency constraints: 30
[10 sec, 215978 itr]: obj = (30), mov = 215978, inf = 37.1%, acc = 29.9%, imp = 206

number of satisfied efficiency constraints: 30
[11 sec, 237028 itr]: obj = (30), mov = 237028, inf = 35.4%, acc = 30%, imp = 207

number of satisfied efficiency constraints: 30
[12 sec, 258307 itr]: obj = (30), mov = 258307, inf = 33.9%, acc = 30.1%, imp = 207

number of satisfied efficiency constraints: 30
[13 sec, 279987 itr]: obj = (31), mov = 279987, inf = 32.7%, acc = 30.1%, imp = 208

number of satisfied efficiency constraints: 31
[14 sec, 303253 itr]: obj = (31), mov = 303253, inf = 31.5%, acc = 29.9%, imp = 208

number of satisfied efficiency constraints: 31
[15 sec, 326520 itr]: obj = (31), mov = 326520, inf = 30.6%, acc = 29.7%, imp = 208

number of satisfied efficiency constraints: 31
[16 sec, 348761 itr]: obj = (31), mov = 348761, inf = 29.7%, acc = 29.6%, imp = 208

number of satisfied efficiency constraints: 31
[17 sec, 371540 itr]: obj = (31), mov = 371540, inf = 28.9%, acc = 29.5%, imp = 208

number of satisfied efficiency constraints: 31
[18 sec, 394559 itr]: obj = (31), mov = 394559, inf = 28.2%, acc = 29.5%, imp = 208

number of satisfied efficiency constraints: 31
[19 sec, 417164 itr]: obj = (31), mov = 417164, inf = 27.6%, acc = 29.4%, imp = 208

number of satisfied efficiency constraints: 31
[20 sec, 439852 itr]: obj = (31), mov = 439852, inf = 27.1%, acc = 29.3%, imp = 208

number of satisfied efficiency constraints: 31
[21 sec, 463089 itr]: obj = (31), mov = 463089, inf = 26.6%, acc = 29.2%, imp = 208

number of satisfied efficiency constraints: 31
[22 sec, 486110 itr]: obj = (31), mov = 486110, inf = 26.2%, acc = 29.1%, imp = 208

number of satisfied efficiency constraints: 31
[23 sec, 509163 itr]: obj = (31), mov = 509163, inf = 25.7%, acc = 29.1%, imp = 208

number of satisfied efficiency constraints: 31
[24 sec, 531604 itr]: obj = (31), mov = 531604, inf = 25.4%, acc = 29%, imp = 208

number of satisfied efficiency constraints: 31
[25 sec, 554681 itr]: obj = (31), mov = 554681, inf = 25%, acc = 29%, imp = 208

number of satisfied efficiency constraints: 31
[26 sec, 577328 itr]: obj = (31), mov = 577328, inf = 24.7%, acc = 28.9%, imp = 208

number of satisfied efficiency constraints: 31
[27 sec, 598135 itr]: obj = (31), mov = 598135, inf = 24.4%, acc = 28.9%, imp = 208

number of satisfied efficiency constraints: 31
[28 sec, 614452 itr]: obj = (31), mov = 614452, inf = 24.3%, acc = 28.9%, imp = 208

number of satisfied efficiency constraints: 31
[29 sec, 629648 itr]: obj = (31), mov = 629648, inf = 24%, acc = 29.4%, imp = 238

number of satisfied efficiency constraints: 31
[30 sec, 647122 itr]: obj = (31), mov = 647122, inf = 23.8%, acc = 29.7%, imp = 240

number of satisfied efficiency constraints: 31
[31 sec, 665507 itr]: obj = (31), mov = 665507, inf = 23.6%, acc = 29.9%, imp = 241

number of satisfied efficiency constraints: 31
[32 sec, 684238 itr]: obj = (31), mov = 684238, inf = 23.3%, acc = 30.1%, imp = 241

number of satisfied efficiency constraints: 31
[33 sec, 703920 itr]: obj = (31), mov = 703920, inf = 23.1%, acc = 30.2%, imp = 241

number of satisfied efficiency constraints: 31
[34 sec, 723037 itr]: obj = (31), mov = 723037, inf = 22.9%, acc = 30.3%, imp = 241

number of satisfied efficiency constraints: 31
[35 sec, 743082 itr]: obj = (31), mov = 743082, inf = 22.7%, acc = 30.4%, imp = 241

number of satisfied efficiency constraints: 31
[36 sec, 762888 itr]: obj = (31), mov = 762888, inf = 22.5%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[37 sec, 783237 itr]: obj = (31), mov = 783237, inf = 22.3%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[38 sec, 801763 itr]: obj = (31), mov = 801763, inf = 22.2%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[39 sec, 819737 itr]: obj = (31), mov = 819737, inf = 22%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[40 sec, 839906 itr]: obj = (31), mov = 839906, inf = 21.9%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[41 sec, 860373 itr]: obj = (31), mov = 860373, inf = 21.7%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[42 sec, 880073 itr]: obj = (31), mov = 880073, inf = 21.6%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[43 sec, 896248 itr]: obj = (31), mov = 896248, inf = 21.5%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[44 sec, 915729 itr]: obj = (31), mov = 915729, inf = 21.3%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[45 sec, 936514 itr]: obj = (31), mov = 936514, inf = 21.2%, acc = 30.5%, imp = 242

number of satisfied efficiency constraints: 31
[46 sec, 957956 itr]: obj = (31), mov = 957956, inf = 21.1%, acc = 30.4%, imp = 242

number of satisfied efficiency constraints: 31
[47 sec, 979250 itr]: obj = (31), mov = 979250, inf = 21%, acc = 30.4%, imp = 242

number of satisfied efficiency constraints: 31
[48 sec, 1000026 itr]: obj = (31), mov = 1000026, inf = 20.8%, acc = 30.4%, imp = 242

number of satisfied efficiency constraints: 31
[49 sec, 1019790 itr]: obj = (31), mov = 1019790, inf = 20.7%, acc = 30.4%, imp = 242

number of satisfied efficiency constraints: 31
[50 sec, 1039853 itr]: obj = (31), mov = 1039853, inf = 20.6%, acc = 30.4%, imp = 242

number of satisfied efficiency constraints: 31
[51 sec, 1056377 itr]: obj = (31), mov = 1056377, inf = 20.5%, acc = 30.6%, imp = 270

number of satisfied efficiency constraints: 31
[52 sec, 1072446 itr]: obj = (31), mov = 1072446, inf = 20.4%, acc = 30.9%, imp = 270

number of satisfied efficiency constraints: 31
[53 sec, 1089247 itr]: obj = (31), mov = 1089247, inf = 20.3%, acc = 31.1%, imp = 271

number of satisfied efficiency constraints: 31
[54 sec, 1107469 itr]: obj = (31), mov = 1107469, inf = 20.2%, acc = 31.2%, imp = 272

number of satisfied efficiency constraints: 31
[55 sec, 1126432 itr]: obj = (31), mov = 1126432, inf = 20.1%, acc = 31.3%, imp = 273

number of satisfied efficiency constraints: 31
[56 sec, 1147219 itr]: obj = (31), mov = 1147219, inf = 20%, acc = 31.3%, imp = 274

number of satisfied efficiency constraints: 31
[57 sec, 1168385 itr]: obj = (31), mov = 1168385, inf = 19.9%, acc = 31.3%, imp = 275

number of satisfied efficiency constraints: 31
[58 sec, 1190105 itr]: obj = (31), mov = 1190105, inf = 19.8%, acc = 31.2%, imp = 275

number of satisfied efficiency constraints: 31
[59 sec, 1212054 itr]: obj = (31), mov = 1212054, inf = 19.7%, acc = 31.2%, imp = 275

number of satisfied efficiency constraints: 31
[60 sec, 1234197 itr]: obj = (31), mov = 1234197, inf = 19.6%, acc = 31.1%, imp = 275

number of satisfied efficiency constraints: 31
[61 sec, 1256472 itr]: obj = (31), mov = 1256472, inf = 19.5%, acc = 31%, imp = 275

number of satisfied efficiency constraints: 31
[62 sec, 1278930 itr]: obj = (31), mov = 1278930, inf = 19.4%, acc = 31%, imp = 275

number of satisfied efficiency constraints: 31
[63 sec, 1301580 itr]: obj = (31), mov = 1301580, inf = 19.3%, acc = 30.9%, imp = 275

number of satisfied efficiency constraints: 31
[64 sec, 1323600 itr]: obj = (31), mov = 1323600, inf = 19.2%, acc = 30.8%, imp = 275

number of satisfied efficiency constraints: 31
[65 sec, 1345208 itr]: obj = (31), mov = 1345208, inf = 19.2%, acc = 30.8%, imp = 275

number of satisfied efficiency constraints: 31
[66 sec, 1367850 itr]: obj = (31), mov = 1367850, inf = 19.1%, acc = 30.7%, imp = 275

number of satisfied efficiency constraints: 31
[67 sec, 1390163 itr]: obj = (31), mov = 1390163, inf = 19%, acc = 30.7%, imp = 275

number of satisfied efficiency constraints: 31
[68 sec, 1412094 itr]: obj = (31), mov = 1412094, inf = 18.9%, acc = 30.7%, imp = 275

number of satisfied efficiency constraints: 31
[69 sec, 1434220 itr]: obj = (31), mov = 1434220, inf = 18.9%, acc = 30.6%, imp = 294

number of satisfied efficiency constraints: 31
[70 sec, 1449836 itr]: obj = (31), mov = 1449836, inf = 18.8%, acc = 30.8%, imp = 303

number of satisfied efficiency constraints: 31
[71 sec, 1467952 itr]: obj = (31), mov = 1467952, inf = 18.8%, acc = 30.9%, imp = 303

number of satisfied efficiency constraints: 31
[72 sec, 1485703 itr]: obj = (31), mov = 1485703, inf = 18.7%, acc = 31%, imp = 303

number of satisfied efficiency constraints: 31
[73 sec, 1503517 itr]: obj = (31), mov = 1503517, inf = 18.6%, acc = 31.1%, imp = 303

number of satisfied efficiency constraints: 31
[74 sec, 1522790 itr]: obj = (31), mov = 1522790, inf = 18.6%, acc = 31.2%, imp = 304

number of satisfied efficiency constraints: 31
[75 sec, 1542476 itr]: obj = (31), mov = 1542476, inf = 18.5%, acc = 31.2%, imp = 304

number of satisfied efficiency constraints: 31
[76 sec, 1561179 itr]: obj = (31), mov = 1561179, inf = 18.5%, acc = 31.3%, imp = 304

number of satisfied efficiency constraints: 31
[77 sec, 1580931 itr]: obj = (31), mov = 1580931, inf = 18.4%, acc = 31.3%, imp = 305

number of satisfied efficiency constraints: 31
[78 sec, 1601807 itr]: obj = (31), mov = 1601807, inf = 18.4%, acc = 31.3%, imp = 305

number of satisfied efficiency constraints: 31
[79 sec, 1622515 itr]: obj = (31), mov = 1622515, inf = 18.3%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[80 sec, 1643127 itr]: obj = (31), mov = 1643127, inf = 18.3%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[81 sec, 1663575 itr]: obj = (31), mov = 1663575, inf = 18.2%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[82 sec, 1684384 itr]: obj = (31), mov = 1684384, inf = 18.2%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[83 sec, 1705436 itr]: obj = (31), mov = 1705436, inf = 18.1%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[84 sec, 1726153 itr]: obj = (31), mov = 1726153, inf = 18.1%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[85 sec, 1746905 itr]: obj = (31), mov = 1746905, inf = 18.1%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[86 sec, 1767005 itr]: obj = (31), mov = 1767005, inf = 18%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[87 sec, 1787798 itr]: obj = (31), mov = 1787798, inf = 18%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[88 sec, 1808808 itr]: obj = (31), mov = 1808808, inf = 17.9%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[89 sec, 1828857 itr]: obj = (31), mov = 1828857, inf = 17.9%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[90 sec, 1849666 itr]: obj = (31), mov = 1849666, inf = 17.9%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
[90 sec, 1849666 itr]: obj = (31), mov = 1849666, inf = 17.9%, acc = 31.4%, imp = 305

number of satisfied efficiency constraints: 31
1849666 iterations, 1849666 moves performed in 90 seconds
Feasible solution: obj = (31)

Run output...
FINAL SOLUTION
satisfied efficiency constraints: 31
Mapping: 0->21, 1->15, 2->26, 3->34, 4->44, 5->14, 6->23, 7->35, 8->47, 9->10, 10->15, 11->39, 12->31, 13->28, 14->18, 15->10, 16->32, 17->20, 18->41, 19->27, 20->4, 21->18, 22->36, 23->1, 24->29, 25->35, 26->24, 27->33, 28->24, 29->33, 30->4, 31->2, 32->24, 33->29, 34->34, 35->41, 36->44, 37->46, 38->33, 39->20, 40->2, 41->22, 42->0, 43->22, 44->47, 45->8, 46->3, 47->13, 48->29, 49->20, 50->38, 51->38, 52->22, 53->25, 54->35, 55->8, 56->29, 57->6, 58->26, 59->5, 60->26, 61->13, 62->42, 63->36, 64->6, 65->28, 66->37, 67->16, 68->37, 69->23, 70->8, 71->29, 72->15, 73->7, 74->36, 75->14, 76->12, 77->24, 78->18, 79->4, 80->3, 81->18, 82->3, 83->13, 84->19, 85->45, 86->14, 87->21, 88->27, 89->9, 90->37, 91->6, 92->45, 93->27, 94->0, 95->42, 96->34, 97->18, 98->40, 99->16, 100->42, 101->43, 102->35, 103->43, 104->7, 105->29, 106->17, 107->36, 108->9, 109->21, 110->40, 111->11, 112->47, 113->11, 114->45, 115->24, 116->39, 117->22, 118->40, 119->39, 120->31, 121->33, 122->11, 123->16, 124->5, 125->46, 126->15, 127->6, 128->13, 129->42, 130->23, 131->1, 132->34, 133->9, 134->40, 135->11, 136->2, 137->44, 138->17, 139->9, 140->36, 141->1, 142->44, 143->36, 144->22, 145->18, 146->9, 147->30, 148->28, 149->17, 150->46, 151->28, 152->27, 153->23, 154->22, 155->20, 156->32, 157->25, 158->45, 159->40, 160->39, 161->6, 162->43, 163->43, 164->28, 165->35, 166->17, 167->14, 168->32, 169->16, 170->32, 171->25, 172->1, 173->12, 174->38, 175->4, 176->20, 177->16, 178->0, 179->8, 180->38, 181->17, 182->19, 183->12, 184->34, 185->7, 186->25, 187->5, 188->44, 189->46, 190->26, 191->32, 192->28, 193->30, 194->0, 195->43, 196->0, 197->38, 198->8, 199->3, 200->31, 201->4, 202->34, 203->21, 204->39, 205->1, 206->15, 207->33, 208->25, 209->46, 210->14, 211->12, 212->23, 213->20, 214->44, 215->19, 216->47, 217->41, 218->21, 219->1, 220->30, 221->14, 222->30, 223->25, 224->40, 225->47, 226->42, 227->5, 228->2, 229->46, 230->6, 231->10, 232->27, 233->16, 234->5, 235->31, 236->30, 237->3, 238->37, 239->17, 240->45, 241->10, 242->3, 243->45, 244->2, 245->31, 246->19, 247->31, 248->5, 249->10, 250->11, 251->12, 252->32, 253->27, 254->42, 255->7, 256->26, 257->4, 258->10, 259->19, 260->19, 261->43, 262->35, 263->0, 264->23, 265->7, 266->41, 267->37, 268->41, 269->13, 270->8, 271->9, 272->41, 273->2, 274->37, 275->21, 276->33, 277->39, 278->12, 279->11, 280->47, 281->30, 282->15, 283->26, 284->7, 285->13, 286->24, 287->38
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.66426	5.20197	5.25165	2.60514	1.41219	1.38025
	2.70516	2.51177	4.7263	10.8326	2.48599	4.32628
	2.08912	2.89403	2.5327	1.05041	10.0835	6.60765
	4.43581	1.15227	1.44248	1.17238	2.22114	7.08981
	2.38696	1.36128	1.70757	1.37056	1.7288	2.91701
	2.15231	1.62965	1.46968	1.60198	10.2513	2.03421
	5.26329	10.2045	1.52611	1.77707	10.1623	2.22466
	4.18146	4.80494	10.6176	1.59913	2.69032	2.46249
Efficiency constraint satisfaction map:
	1	1	1	1	1	1
	1	1	1	1	1	1
	1	0	0	0	1	1
	1	0	0	0	0	1
	1	0	0	0	0	1
	1	0	0	0	1	1
	1	1	0	0	1	1
	1	1	1	0	1	1
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 15 --global-tlim 90 --seed 601

Starting the Optimization Process
