==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 5 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 801

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.008861
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005110
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.156/5.170
- lns time: 1.156170
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.015363
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.004653
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.006407
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005396
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005041
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.011256
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005515
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.005296
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.011924
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.675/24.916
- lns time: 1.674848
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004794
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007071
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005617
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.004893
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005067
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.008042
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.010043
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.013137
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006791
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.011600
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006499
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007862
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.006581
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005177
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007170
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.007217
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.009988
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.005009
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.386/62.434
- lns time: 1.386187
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.004026
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.009238
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008083
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.006858
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005883
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.013777
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008377
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.005904
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.011616
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.696/81.204
- lns time: 0.695801
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.209/82.414
- lns time: 1.209598
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.341/83.755
- lns time: 1.341112
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008310
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.005865
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.009405
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 0.227040
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 25

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 19
final number of satisfied efficiency constraints: 25
number of iterations: 48
total time: 90.005478

Final Solution: 0->19, 1->16, 2->10, 3->27, 4->38, 5->16, 6->0, 7->42, 8->14, 9->22, 10->16, 11->36, 12->26, 13->45, 14->1, 15->39, 16->31, 17->36, 18->2, 19->2, 20->46, 21->0, 22->31, 23->22, 24->2, 25->3, 26->27, 27->32, 28->8, 29->46, 30->17, 31->25, 32->3, 33->22, 34->10, 35->22, 36->15, 37->14, 38->18, 39->19, 40->9, 41->16, 42->23, 43->34, 44->26, 45->9, 46->17, 47->7, 48->9, 49->8, 50->15, 51->7, 52->6, 53->29, 54->47, 55->4, 56->25, 57->35, 58->46, 59->29, 60->36, 61->26, 62->29, 63->40, 64->32, 65->44, 66->38, 67->15, 68->45, 69->4, 70->39, 71->6, 72->8, 73->28, 74->9, 75->31, 76->40, 77->2, 78->18, 79->3, 80->22, 81->24, 82->20, 83->34, 84->24, 85->37, 86->23, 87->28, 88->1, 89->1, 90->21, 91->28, 92->15, 93->41, 94->5, 95->37, 96->7, 97->22, 98->21, 99->30, 100->47, 101->39, 102->41, 103->32, 104->4, 105->42, 106->8, 107->16, 108->4, 109->34, 110->2, 111->6, 112->38, 113->13, 114->43, 115->10, 116->11, 117->43, 118->25, 119->39, 120->0, 121->20, 122->21, 123->9, 124->1, 125->36, 126->38, 127->14, 128->33, 129->8, 130->26, 131->29, 132->5, 133->30, 134->43, 135->45, 136->17, 137->6, 138->10, 139->30, 140->10, 141->44, 142->37, 143->11, 144->42, 145->4, 146->44, 147->34, 148->32, 149->43, 150->21, 151->33, 152->20, 153->5, 154->32, 155->3, 156->1, 157->28, 158->35, 159->45, 160->41, 161->20, 162->33, 163->14, 164->1, 165->42, 166->47, 167->39, 168->36, 169->23, 170->16, 171->19, 172->17, 173->33, 174->32, 175->24, 176->24, 177->11, 178->11, 179->8, 180->27, 181->41, 182->21, 183->5, 184->3, 185->36, 186->13, 187->44, 188->13, 189->0, 190->2, 191->44, 192->20, 193->14, 194->23, 195->15, 196->40, 197->31, 198->27, 199->12, 200->4, 201->18, 202->21, 203->35, 204->41, 205->42, 206->31, 207->23, 208->40, 209->14, 210->7, 211->6, 212->28, 213->46, 214->40, 215->39, 216->47, 217->41, 218->30, 219->19, 220->26, 221->26, 222->46, 223->34, 224->0, 225->15, 226->11, 227->35, 228->0, 229->35, 230->30, 231->10, 232->42, 233->12, 234->37, 235->37, 236->6, 237->12, 238->7, 239->13, 240->18, 241->12, 242->38, 243->23, 244->24, 245->29, 246->46, 247->43, 248->13, 249->17, 250->45, 251->47, 252->18, 253->24, 254->19, 255->45, 256->29, 257->28, 258->33, 259->31, 260->12, 261->12, 262->35, 263->5, 264->33, 265->3, 266->40, 267->38, 268->25, 269->30, 270->47, 271->9, 272->27, 273->34, 274->44, 275->25, 276->43, 277->19, 278->5, 279->11, 280->7, 281->18, 282->20, 283->27, 284->25, 285->17, 286->13, 287->37

Average CPI map:
	0.80,	2.06,	1.85,	4.45,	3.37,	1.68
	3.08,	3.39,	3.78,	11.79,	3.48,	2.27
	2.05,	3.37,	3.34,	3.24,	2.92,	3.97
	3.09,	3.08,	2.96,	1.53,	2.75,	2.68
	2.67,	2.42,	2.40,	2.35,	2.33,	2.30
	2.03,	2.26,	1.15,	2.22,	9.49,	2.22
	2.21,	2.20,	1.45,	2.22,	2.21,	2.11
	2.19,	8.06,	4.01,	2.18,	2.18,	2.17

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	0,	1,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	1,	1,	0,	0,	1
