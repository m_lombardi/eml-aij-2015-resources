==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 15 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 1

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.021/0.021
- branches (local/global): 35/35
- fails (local/global): 4/4
- lns time: 0.031000
- lns branches: 35
- lns fails: 4
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.038/0.069
- branches (local/global): 888/923
- fails (local/global): 400/404
- lns time: 0.048000
- lns branches: 888
- lns fails: 400
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.019/0.098
- branches (local/global): 41/964
- fails (local/global): 7/411
- lns time: 0.028000
- lns branches: 41
- lns fails: 7
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.034/0.141
- branches (local/global): 247/1211
- fails (local/global): 104/515
- lns time: 0.043000
- lns branches: 247
- lns fails: 104
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.101/1.251
- branches (local/global): 10384/11595
- fails (local/global): 4959/5474
- lns time: 1.110000
- lns branches: 10384
- lns fails: 4959
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.083/1.343
- branches (local/global): 477/12072
- fails (local/global): 205/5679
- lns time: 0.093000
- lns branches: 477
- lns fails: 205
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.031/1.384
- branches (local/global): 101/12173
- fails (local/global): 38/5717
- lns time: 0.040000
- lns branches: 101
- lns fails: 38
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 32476
- lns fails: 15714
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.020/3.421
- branches (local/global): 297/44946
- fails (local/global): 124/21555
- lns time: 0.028000
- lns branches: 297
- lns fails: 124
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 22490
- lns fails: 10923
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.223/5.660
- branches (local/global): 1309/68745
- fails (local/global): 604/33082
- lns time: 0.233000
- lns branches: 1309
- lns fails: 604
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 30772
- lns fails: 14894
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.009000
- lns branches: 13802
- lns fails: 6745
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.007000
- lns branches: 18597
- lns fails: 9097
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 10828
- lns fails: 5351
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.302/14.004
- branches (local/global): 5543/148287
- fails (local/global): 2631/71800
- lns time: 0.310000
- lns branches: 5543
- lns fails: 2631
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 23540
- lns fails: 11388
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.013000
- lns branches: 13833
- lns fails: 6812
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007000
- lns branches: 10194
- lns fails: 4929
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 35443
- lns fails: 17187
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.007000
- lns branches: 48130
- lns fails: 23380
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.008000
- lns branches: 23173
- lns fails: 11260
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.015000
- lns branches: 14239
- lns fails: 6993
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
- lns time: 2.009000
- lns branches: 12947
- lns fails: 6376
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.026/30.113
- branches (local/global): 260/330046
- fails (local/global): 106/160231
- lns time: 0.035000
- lns branches: 260
- lns fails: 106
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.009000
- lns branches: 14386
- lns fails: 6954
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.008000
- lns branches: 20099
- lns fails: 9756
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.008000
- lns branches: 28497
- lns fails: 13802
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.009000
- lns branches: 14265
- lns fails: 6907
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.008000
- lns branches: 11198
- lns fails: 5407
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.007000
- lns branches: 22756
- lns fails: 11127
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.013000
- lns branches: 14863
- lns fails: 7238
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.009000
- lns branches: 20182
- lns fails: 9790
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.009000
- lns branches: 13726
- lns fails: 6724
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.008000
- lns branches: 21306
- lns fails: 10461
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.013000
- lns branches: 19885
- lns fails: 9595
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
- lns time: 2.009000
- lns branches: 20135
- lns fails: 9774
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.409/55.641
- branches (local/global): 18373/569717
- fails (local/global): 8938/276704
- lns time: 1.419000
- lns branches: 18373
- lns fails: 8938
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 11772
- lns fails: 5749
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.012000
- lns branches: 15097
- lns fails: 7363
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 16030
- lns fails: 7916
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 15788
- lns fails: 7780
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.013000
- lns branches: 10592
- lns fails: 5201
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.012000
- lns branches: 18952
- lns fails: 9223
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 15411
- lns fails: 7565
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 17701
- lns fails: 8633
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 33379
- lns fails: 16147
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 14779
- lns fails: 7160
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 20222
- lns fails: 9951
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 25177
- lns fails: 12198
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 15890
- lns fails: 7805
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 20995
- lns fails: 10268
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.009000
- lns branches: 17686
- lns fails: 8602
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.011000
- lns branches: 13417
- lns fails: 6601
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.016000
- lns branches: 10455
- lns fails: 5028
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 0.199000
- lns branches: 2180
- lns fails: 1052

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 32
number of iterations: 56
total time: 90014.000000
total branches: 865240
total fails: 420946

Final Solution: 0->34, 1->42, 2->16, 3->27, 4->31, 5->3, 6->1, 7->45, 8->17, 9->6, 10->4, 11->41, 12->15, 13->25, 14->16, 15->5, 16->5, 17->44, 18->43, 19->15, 20->23, 21->2, 22->23, 23->17, 24->2, 25->21, 26->44, 27->4, 28->39, 29->34, 30->3, 31->8, 32->32, 33->25, 34->29, 35->3, 36->36, 37->5, 38->21, 39->2, 40->35, 41->36, 42->7, 43->13, 44->17, 45->24, 46->26, 47->19, 48->9, 49->41, 50->3, 51->41, 52->35, 53->12, 54->2, 55->46, 56->47, 57->18, 58->41, 59->18, 60->4, 61->3, 62->18, 63->33, 64->14, 65->32, 66->12, 67->46, 68->10, 69->31, 70->37, 71->23, 72->42, 73->39, 74->6, 75->1, 76->1, 77->0, 78->12, 79->26, 80->39, 81->30, 82->43, 83->0, 84->33, 85->28, 86->7, 87->44, 88->32, 89->8, 90->9, 91->11, 92->19, 93->4, 94->1, 95->4, 96->9, 97->17, 98->46, 99->39, 100->20, 101->39, 102->18, 103->40, 104->43, 105->20, 106->29, 107->37, 108->14, 109->38, 110->11, 111->25, 112->8, 113->32, 114->1, 115->14, 116->28, 117->8, 118->6, 119->23, 120->18, 121->43, 122->37, 123->34, 124->15, 125->41, 126->20, 127->20, 128->38, 129->25, 130->25, 131->47, 132->37, 133->40, 134->16, 135->39, 136->9, 137->13, 138->40, 139->13, 140->31, 141->37, 142->13, 143->4, 144->47, 145->31, 146->7, 147->44, 148->16, 149->35, 150->47, 151->44, 152->32, 153->19, 154->1, 155->5, 156->38, 157->2, 158->19, 159->12, 160->28, 161->26, 162->18, 163->22, 164->23, 165->30, 166->12, 167->24, 168->19, 169->11, 170->15, 171->29, 172->46, 173->28, 174->28, 175->5, 176->41, 177->36, 178->3, 179->24, 180->26, 181->27, 182->33, 183->43, 184->8, 185->35, 186->21, 187->46, 188->13, 189->30, 190->2, 191->40, 192->7, 193->30, 194->10, 195->10, 196->32, 197->34, 198->43, 199->33, 200->0, 201->25, 202->22, 203->14, 204->12, 205->47, 206->0, 207->31, 208->42, 209->34, 210->36, 211->45, 212->24, 213->20, 214->29, 215->33, 216->30, 217->23, 218->19, 219->42, 220->34, 221->21, 222->28, 223->21, 224->45, 225->17, 226->35, 227->5, 228->35, 229->20, 230->6, 231->7, 232->14, 233->38, 234->11, 235->15, 236->31, 237->13, 238->22, 239->27, 240->11, 241->22, 242->9, 243->24, 244->14, 245->21, 246->45, 247->22, 248->45, 249->37, 250->10, 251->47, 252->15, 253->16, 254->27, 255->40, 256->30, 257->40, 258->9, 259->36, 260->0, 261->0, 262->10, 263->45, 264->26, 265->27, 266->7, 267->6, 268->27, 269->29, 270->44, 271->42, 272->38, 273->16, 274->42, 275->26, 276->24, 277->22, 278->38, 279->10, 280->36, 281->29, 282->33, 283->6, 284->46, 285->8, 286->11, 287->17

Average CPI map:
	2.09,	1.61,	1.30,	2.19,	2.42,	1.66
	2.23,	2.66,	2.72,	10.47,	5.08,	2.43
	6.21,	13.38,	1.25,	1.06,	4.18,	4.24
	2.04,	0.93,	3.63,	1.40,	3.45,	2.92
	2.56,	1.37,	2.43,	2.34,	2.30,	2.27
	2.24,	10.23,	1.03,	1.44,	1.54,	2.16
	2.14,	10.02,	2.13,	11.06,	12.91,	2.12
	2.12,	2.74,	2.12,	1.24,	11.85,	2.09

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	1,	1,	1
	1,	1,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	1,	0,	0,	0,	1
	1,	1,	0,	1,	1,	1
	1,	1,	0,	0,	1,	1
