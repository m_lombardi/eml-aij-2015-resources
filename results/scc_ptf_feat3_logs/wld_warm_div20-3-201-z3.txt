==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 3 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --seed 201

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.007382
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
- lns time: 2.014276
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 12
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.556/4.578
- lns time: 0.556276
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 2.006257
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
- lns time: 2.010445
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 13
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.742/9.336
- lns time: 0.741693
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.007761
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.005320
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.006885
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.006651
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 2.005344
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
- lns time: 1.876349
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 14
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.639/22.884
- lns time: 1.639539
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 15
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.843/24.727
- lns time: 1.843349
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.007819
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004671
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.005616
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006335
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 1.151915
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.006354
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
- lns time: 2.004010
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 16
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.019/38.933
- lns time: 1.019273
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 2.005517
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
- lns time: 1.005928
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 17
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.923/42.868
- lns time: 0.922711
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.008521
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.003785
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.006534
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.008008
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.005893
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.004910
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 1.067967
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 1.817135
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.007954
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.006138
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.005341
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 2.006167
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.959/66.775
- lns time: 0.958917
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 1.770758
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.011269
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.005266
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 0.762850
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004972
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004689
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.004669
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006320
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 1.364574
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006789
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
- lns time: 2.006037
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 1.265/87.988
- lns time: 1.265438
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 0.670684
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
- lns time: 1.345681
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 18

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 12
final number of satisfied efficiency constraints: 18
number of iterations: 52
total time: 90.004941

Final Solution: 0->8, 1->5, 2->29, 3->42, 4->40, 5->45, 6->20, 7->30, 8->10, 9->18, 10->1, 11->35, 12->26, 13->8, 14->23, 15->36, 16->2, 17->29, 18->23, 19->44, 20->34, 21->41, 22->42, 23->44, 24->1, 25->33, 26->2, 27->41, 28->13, 29->35, 30->32, 31->15, 32->25, 33->13, 34->25, 35->25, 36->0, 37->28, 38->35, 39->32, 40->18, 41->5, 42->23, 43->29, 44->40, 45->32, 46->6, 47->11, 48->19, 49->16, 50->4, 51->22, 52->27, 53->37, 54->38, 55->13, 56->4, 57->4, 58->39, 59->26, 60->24, 61->41, 62->17, 63->47, 64->15, 65->30, 66->17, 67->31, 68->30, 69->10, 70->25, 71->30, 72->21, 73->47, 74->11, 75->17, 76->12, 77->9, 78->15, 79->14, 80->0, 81->14, 82->9, 83->40, 84->28, 85->26, 86->45, 87->14, 88->8, 89->9, 90->6, 91->29, 92->36, 93->4, 94->22, 95->17, 96->32, 97->42, 98->9, 99->33, 100->3, 101->22, 102->39, 103->45, 104->20, 105->28, 106->35, 107->2, 108->3, 109->39, 110->0, 111->28, 112->2, 113->47, 114->17, 115->5, 116->14, 117->3, 118->2, 119->25, 120->9, 121->9, 122->45, 123->16, 124->6, 125->2, 126->5, 127->36, 128->24, 129->41, 130->45, 131->6, 132->43, 133->23, 134->3, 135->0, 136->21, 137->24, 138->36, 139->8, 140->46, 141->10, 142->43, 143->39, 144->13, 145->37, 146->29, 147->46, 148->1, 149->47, 150->38, 151->0, 152->17, 153->31, 154->22, 155->12, 156->15, 157->27, 158->19, 159->19, 160->27, 161->40, 162->37, 163->16, 164->1, 165->20, 166->11, 167->31, 168->38, 169->45, 170->18, 171->5, 172->3, 173->10, 174->22, 175->33, 176->1, 177->43, 178->7, 179->34, 180->28, 181->32, 182->36, 183->23, 184->4, 185->41, 186->26, 187->19, 188->40, 189->39, 190->38, 191->16, 192->10, 193->32, 194->29, 195->18, 196->18, 197->21, 198->7, 199->20, 200->34, 201->15, 202->16, 203->19, 204->3, 205->40, 206->11, 207->20, 208->23, 209->14, 210->10, 211->21, 212->31, 213->38, 214->34, 215->33, 216->36, 217->16, 218->15, 219->7, 220->44, 221->33, 222->14, 223->6, 224->46, 225->43, 226->47, 227->34, 228->24, 229->35, 230->33, 231->30, 232->6, 233->46, 234->24, 235->31, 236->42, 237->34, 238->8, 239->42, 240->44, 241->37, 242->12, 243->37, 244->46, 245->31, 246->27, 247->20, 248->4, 249->8, 250->30, 251->11, 252->25, 253->41, 254->12, 255->26, 256->13, 257->42, 258->7, 259->0, 260->43, 261->46, 262->39, 263->13, 264->21, 265->12, 266->7, 267->47, 268->24, 269->26, 270->18, 271->28, 272->38, 273->5, 274->44, 275->21, 276->27, 277->22, 278->35, 279->11, 280->12, 281->44, 282->19, 283->43, 284->37, 285->1, 286->27, 287->7

Average CPI map:
	0.63,	2.38,	2.56,	2.18,	3.21,	0.79
	3.74,	1.51,	2.28,	2.20,	3.70,	2.18
	2.37,	2.06,	2.12,	2.05,	2.05,	4.94
	2.05,	2.05,	2.04,	2.04,	7.25,	2.06
	2.02,	1.07,	1.99,	1.99,	3.28,	7.19
	2.00,	1.99,	1.99,	1.99,	1.99,	2.31
	2.93,	2.06,	1.98,	1.98,	1.99,	2.02
	1.96,	2.84,	1.98,	1.98,	1.99,	2.06

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	0,	0,	0,	0,	1
	0,	0,	0,	0,	0,	1
	0,	0,	0,	0,	0,	1
	0,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	0,	1,	0,	0,	0,	1
