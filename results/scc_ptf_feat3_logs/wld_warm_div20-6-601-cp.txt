==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 6 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 601

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.033/0.033
- branches (local/global): 113/113
- fails (local/global): 44/44
- lns time: 0.044000
- lns branches: 113
- lns fails: 44
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.028/0.072
- branches (local/global): 31/144
- fails (local/global): 3/47
- lns time: 0.045000
- lns branches: 31
- lns fails: 3
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.026/0.115
- branches (local/global): 66/210
- fails (local/global): 22/69
- lns time: 0.034000
- lns branches: 66
- lns fails: 22
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.018/0.141
- branches (local/global): 178/388
- fails (local/global): 75/144
- lns time: 0.026000
- lns branches: 178
- lns fails: 75
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.024/0.173
- branches (local/global): 301/689
- fails (local/global): 127/271
- lns time: 0.032000
- lns branches: 301
- lns fails: 127
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.821/1.002
- branches (local/global): 7584/8273
- fails (local/global): 3605/3876
- lns time: 0.829000
- lns branches: 7584
- lns fails: 3605
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.021/1.031
- branches (local/global): 88/8361
- fails (local/global): 31/3907
- lns time: 0.029000
- lns branches: 88
- lns fails: 31
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.019/1.058
- branches (local/global): 40/8401
- fails (local/global): 7/3914
- lns time: 0.027000
- lns branches: 40
- lns fails: 7
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
- lns time: 2.008000
- lns branches: 38310
- lns fails: 18734
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 28
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.117/3.191
- branches (local/global): 804/47515
- fails (local/global): 366/23014
- lns time: 0.125000
- lns branches: 804
- lns fails: 366
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
- lns time: 2.008000
- lns branches: 15205
- lns fails: 7424
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 29
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/5.222
- branches (local/global): 50/62770
- fails (local/global): 15/30453
- lns time: 0.023000
- lns branches: 50
- lns fails: 15
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 30
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.382/5.612
- branches (local/global): 7143/69913
- fails (local/global): 3402/33855
- lns time: 0.392000
- lns branches: 7143
- lns fails: 3402
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 31
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.023/5.645
- branches (local/global): 37/69950
- fails (local/global): 5/33860
- lns time: 0.033000
- lns branches: 37
- lns fails: 5
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.007000
- lns branches: 36395
- lns fails: 17648
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
- lns time: 2.008000
- lns branches: 15466
- lns fails: 7504
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 32
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.030/9.700
- branches (local/global): 265/122076
- fails (local/global): 113/59125
- lns time: 0.038000
- lns branches: 265
- lns fails: 113
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 33
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.480/10.188
- branches (local/global): 7141/129217
- fails (local/global): 3410/62535
- lns time: 0.488000
- lns branches: 7141
- lns fails: 3410
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 34
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.305/10.501
- branches (local/global): 8238/137455
- fails (local/global): 3944/66479
- lns time: 0.313000
- lns branches: 8238
- lns fails: 3944
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 35
- lower bound 3
- lns time: 2.008000
- lns branches: 15184
- lns fails: 7475
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 35
- lower bound 3
- lns time: 1.020000
- lns branches: 5723
- lns fails: 2845
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 35
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.016/13.553
- branches (local/global): 35/158397
- fails (local/global): 4/76803
- lns time: 0.024000
- lns branches: 35
- lns fails: 4
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
- lns time: 2.008000
- lns branches: 8080
- lns fails: 3958
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
- lns time: 2.008000
- lns branches: 25956
- lns fails: 12595
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
- lns time: 2.008000
- lns branches: 10511
- lns fails: 5201
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
- lns time: 2.008000
- lns branches: 13628
- lns fails: 6694
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 36
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.029/21.622
- branches (local/global): 120/216692
- fails (local/global): 51/105302
- lns time: 0.037000
- lns branches: 120
- lns fails: 51
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 37718
- lns fails: 18358
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.007000
- lns branches: 14831
- lns fails: 7337
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 14514
- lns fails: 7207
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 33820
- lns fails: 16548
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.007000
- lns branches: 22706
- lns fails: 11184
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 12694
- lns fails: 6241
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.010000
- lns branches: 11014
- lns fails: 5362
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 19039
- lns fails: 9334
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 24040
- lns fails: 11645
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.010000
- lns branches: 17009
- lns fails: 8370
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.007000
- lns branches: 19391
- lns fails: 9518
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 32190
- lns fails: 15711
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 11522
- lns fails: 5717
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.009000
- lns branches: 11356
- lns fails: 5573
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
- lns time: 2.008000
- lns branches: 39515
- lns fails: 19201
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 37
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.166/51.918
- branches (local/global): 875/538926
- fails (local/global): 412/263020
- lns time: 0.176000
- lns branches: 875
- lns fails: 412
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.983000
- lns branches: 6301
- lns fails: 3137
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.007000
- lns branches: 15593
- lns fails: 7668
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 13492
- lns fails: 6613
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 17959
- lns fails: 8831
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 12941
- lns fails: 6365
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 14620
- lns fails: 7219
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 19376
- lns fails: 9541
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.007000
- lns branches: 14250
- lns fails: 7055
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 14838
- lns fails: 7303
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 15031
- lns fails: 7375
LNS iteration #53 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.007000
- lns branches: 21190
- lns fails: 10438
LNS iteration #54 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 19222
- lns fails: 9476
LNS iteration #55 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 15430
- lns fails: 7590
LNS iteration #56 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.015000
- lns branches: 15385
- lns fails: 7546
LNS iteration #57 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.395000
- lns branches: 2102
- lns fails: 1048
LNS iteration #58 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 18484
- lns fails: 9073
LNS iteration #59 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.007000
- lns branches: 29599
- lns fails: 14362
LNS iteration #60 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 33593
- lns fails: 16435
LNS iteration #61 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.008000
- lns branches: 18280
- lns fails: 8978
LNS iteration #62 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 2.009000
- lns branches: 13262
- lns fails: 6441
LNS iteration #63 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.344000
- lns branches: 2205
- lns fails: 1100
LNS iteration #64 ========================================
- number of satisfied efficiency constraints: 38
- lower bound 3
- lns time: 0.209000
- lns branches: 999
- lns fails: 501

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 20
final number of satisfied efficiency constraints: 38
number of iterations: 65
total time: 90008.000000
total branches: 873078
total fails: 427115

Final Solution: 0->30, 1->36, 2->38, 3->39, 4->19, 5->13, 6->20, 7->18, 8->23, 9->25, 10->42, 11->26, 12->41, 13->6, 14->38, 15->15, 16->5, 17->31, 18->42, 19->20, 20->32, 21->47, 22->37, 23->5, 24->1, 25->42, 26->15, 27->36, 28->33, 29->14, 30->35, 31->36, 32->11, 33->27, 34->0, 35->13, 36->1, 37->37, 38->15, 39->35, 40->14, 41->38, 42->47, 43->32, 44->27, 45->32, 46->29, 47->41, 48->26, 49->24, 50->37, 51->10, 52->22, 53->23, 54->22, 55->10, 56->22, 57->0, 58->12, 59->10, 60->24, 61->43, 62->8, 63->17, 64->0, 65->35, 66->41, 67->5, 68->3, 69->20, 70->37, 71->34, 72->27, 73->31, 74->41, 75->16, 76->19, 77->17, 78->46, 79->32, 80->0, 81->43, 82->42, 83->40, 84->14, 85->3, 86->29, 87->9, 88->3, 89->13, 90->11, 91->16, 92->10, 93->47, 94->40, 95->19, 96->10, 97->1, 98->47, 99->20, 100->19, 101->32, 102->35, 103->47, 104->10, 105->45, 106->7, 107->22, 108->39, 109->46, 110->45, 111->37, 112->17, 113->2, 114->18, 115->19, 116->6, 117->47, 118->3, 119->44, 120->27, 121->19, 122->9, 123->21, 124->43, 125->8, 126->31, 127->14, 128->42, 129->33, 130->36, 131->7, 132->15, 133->18, 134->25, 135->29, 136->29, 137->44, 138->26, 139->13, 140->40, 141->12, 142->34, 143->27, 144->23, 145->25, 146->43, 147->45, 148->14, 149->1, 150->34, 151->44, 152->34, 153->24, 154->36, 155->1, 156->21, 157->39, 158->34, 159->28, 160->40, 161->27, 162->22, 163->4, 164->26, 165->35, 166->12, 167->14, 168->8, 169->43, 170->45, 171->15, 172->12, 173->29, 174->0, 175->41, 176->13, 177->12, 178->22, 179->7, 180->24, 181->25, 182->9, 183->21, 184->36, 185->28, 186->40, 187->41, 188->46, 189->28, 190->25, 191->37, 192->6, 193->34, 194->16, 195->16, 196->26, 197->44, 198->26, 199->18, 200->45, 201->8, 202->18, 203->38, 204->39, 205->7, 206->3, 207->7, 208->7, 209->18, 210->23, 211->21, 212->46, 213->5, 214->42, 215->24, 216->46, 217->46, 218->16, 219->24, 220->17, 221->6, 222->39, 223->8, 224->29, 225->15, 226->44, 227->33, 228->21, 229->33, 230->2, 231->4, 232->2, 233->0, 234->30, 235->20, 236->12, 237->33, 238->6, 239->4, 240->16, 241->31, 242->4, 243->38, 244->2, 245->30, 246->21, 247->4, 248->6, 249->3, 250->28, 251->9, 252->23, 253->13, 254->45, 255->2, 256->40, 257->20, 258->4, 259->17, 260->31, 261->9, 262->28, 263->2, 264->35, 265->5, 266->43, 267->11, 268->33, 269->31, 270->11, 271->5, 272->11, 273->32, 274->9, 275->25, 276->30, 277->28, 278->1, 279->44, 280->23, 281->11, 282->8, 283->38, 284->39, 285->17, 286->30, 287->30

Average CPI map:
	2.67,	3.53,	2.51,	2.98,	1.80,	1.71
	3.55,	2.11,	2.86,	11.81,	2.41,	2.14
	2.15,	1.17,	6.18,	1.25,	10.97,	6.18
	3.56,	12.62,	0.99,	14.60,	1.32,	4.76
	2.26,	11.87,	5.27,	10.44,	10.02,	2.05
	2.26,	2.25,	1.73,	1.94,	10.01,	2.27
	3.91,	12.79,	1.25,	10.42,	11.24,	3.57
	2.25,	8.92,	10.89,	10.10,	5.65,	6.80

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	1,	0,	1
	1,	1,	0,	1,	1,	1
	1,	0,	0,	0,	1,	1
	1,	1,	0,	1,	1,	1
	1,	1,	1,	1,	1,	1
