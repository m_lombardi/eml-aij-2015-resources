==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 7 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 2 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 701

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- number of satisfied efficiency constraints: 18
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.066/0.066
- branches (local/global): 1130/1130
- fails (local/global): 505/505
- lns time: 0.076000
- lns branches: 1130
- lns fails: 505
LNS iteration #1 ========================================
- number of satisfied efficiency constraints: 19
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.121/0.197
- branches (local/global): 1309/2439
- fails (local/global): 608/1113
- lns time: 0.129000
- lns branches: 1309
- lns fails: 608
LNS iteration #2 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
- lns time: 2.007000
- lns branches: 16034
- lns fails: 7838
LNS iteration #3 ========================================
- number of satisfied efficiency constraints: 20
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.015/2.227
- branches (local/global): 27/18500
- fails (local/global): 0/8951
- lns time: 0.023000
- lns branches: 27
- lns fails: 0
LNS iteration #4 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
- lns time: 2.014000
- lns branches: 15531
- lns fails: 7631
LNS iteration #5 ========================================
- number of satisfied efficiency constraints: 21
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.030/4.279
- branches (local/global): 442/34473
- fails (local/global): 200/16782
- lns time: 0.038000
- lns branches: 442
- lns fails: 200
LNS iteration #6 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
- lns time: 2.008000
- lns branches: 17755
- lns fails: 8685
LNS iteration #7 ========================================
- number of satisfied efficiency constraints: 22
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.602/6.897
- branches (local/global): 3230/55458
- fails (local/global): 1534/27001
- lns time: 0.610000
- lns branches: 3230
- lns fails: 1534
LNS iteration #8 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 16274
- lns fails: 7987
LNS iteration #9 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.010000
- lns branches: 11650
- lns fails: 5591
LNS iteration #10 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.008000
- lns branches: 11707
- lns fails: 5725
LNS iteration #11 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 15209
- lns fails: 7417
LNS iteration #12 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
- lns time: 2.009000
- lns branches: 11551
- lns fails: 5663
LNS iteration #13 ========================================
- number of satisfied efficiency constraints: 23
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.004/16.953
- branches (local/global): 28/121877
- fails (local/global): 0/59384
- lns time: 0.011000
- lns branches: 28
- lns fails: 0
LNS iteration #14 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.010000
- lns branches: 12349
- lns fails: 5975
LNS iteration #15 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 19123
- lns fails: 9352
LNS iteration #16 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.007000
- lns branches: 27451
- lns fails: 13439
LNS iteration #17 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 10814
- lns fails: 5253
LNS iteration #18 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 17267
- lns fails: 8372
LNS iteration #19 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 18816
- lns fails: 9162
LNS iteration #20 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 13187
- lns fails: 6434
LNS iteration #21 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 10326
- lns fails: 5001
LNS iteration #22 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 11654
- lns fails: 5647
LNS iteration #23 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
- lns time: 2.008000
- lns branches: 14652
- lns fails: 7132
LNS iteration #24 ========================================
- number of satisfied efficiency constraints: 24
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.651/37.692
- branches (local/global): 5067/282583
- fails (local/global): 2404/137555
- lns time: 0.659000
- lns branches: 5067
- lns fails: 2404
LNS iteration #25 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
- lns time: 2.008000
- lns branches: 24761
- lns fails: 12081
LNS iteration #26 ========================================
- number of satisfied efficiency constraints: 25
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.028/39.736
- branches (local/global): 890/308234
- fails (local/global): 400/150036
- lns time: 0.035000
- lns branches: 890
- lns fails: 400
LNS iteration #27 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 24989
- lns fails: 12080
LNS iteration #28 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 13672
- lns fails: 6714
LNS iteration #29 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 15997
- lns fails: 7843
LNS iteration #30 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 13763
- lns fails: 6740
LNS iteration #31 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 18170
- lns fails: 8883
LNS iteration #32 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 13395
- lns fails: 6536
LNS iteration #33 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 21953
- lns fails: 10657
LNS iteration #34 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 20724
- lns fails: 9994
LNS iteration #35 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 17582
- lns fails: 8566
LNS iteration #36 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 19256
- lns fails: 9401
LNS iteration #37 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 7945
- lns fails: 3833
LNS iteration #38 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 18993
- lns fails: 9301
LNS iteration #39 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 22676
- lns fails: 11024
LNS iteration #40 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 12815
- lns fails: 6189
LNS iteration #41 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 12567
- lns fails: 6109
LNS iteration #42 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.011000
- lns branches: 20461
- lns fails: 10001
LNS iteration #43 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.007000
- lns branches: 18004
- lns fails: 8779
LNS iteration #44 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 12728
- lns fails: 6206
LNS iteration #45 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 11922
- lns fails: 5818
LNS iteration #46 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.009000
- lns branches: 14669
- lns fails: 7181
LNS iteration #47 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
- lns time: 2.008000
- lns branches: 13138
- lns fails: 6462
LNS iteration #48 ========================================
- number of satisfied efficiency constraints: 26
- lower bound 3
--- checking solution
--- solution ok!
- solution found, obj var is: 4
- time (local/global): 0.580/82.496
- branches (local/global): 3323/656976
- fails (local/global): 1605/319958
- lns time: 0.589000
- lns branches: 3323
- lns fails: 1605
LNS iteration #49 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.008000
- lns branches: 16870
- lns fails: 8264
LNS iteration #50 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.007000
- lns branches: 25443
- lns fails: 12401
LNS iteration #51 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 2.009000
- lns branches: 14639
- lns fails: 7200
LNS iteration #52 ========================================
- number of satisfied efficiency constraints: 27
- lower bound 3
- lns time: 1.479000
- lns branches: 8505
- lns fails: 4141

LNS optimization is over========================================
initial number of satisfied efficiency constraints: 18
final number of satisfied efficiency constraints: 27
number of iterations: 53
total time: 90008.000000
total branches: 722433
total fails: 351964

Final Solution: 0->27, 1->17, 2->39, 3->25, 4->41, 5->30, 6->14, 7->12, 8->22, 9->36, 10->43, 11->12, 12->4, 13->15, 14->36, 15->1, 16->31, 17->20, 18->12, 19->45, 20->22, 21->28, 22->37, 23->24, 24->40, 25->2, 26->20, 27->19, 28->47, 29->9, 30->21, 31->9, 32->31, 33->8, 34->4, 35->6, 36->39, 37->24, 38->34, 39->19, 40->13, 41->32, 42->5, 43->14, 44->10, 45->47, 46->6, 47->7, 48->20, 49->6, 50->40, 51->4, 52->11, 53->5, 54->42, 55->1, 56->13, 57->18, 58->17, 59->12, 60->15, 61->16, 62->35, 63->44, 64->9, 65->20, 66->29, 67->2, 68->23, 69->1, 70->43, 71->45, 72->15, 73->29, 74->45, 75->28, 76->44, 77->18, 78->45, 79->15, 80->23, 81->29, 82->18, 83->33, 84->27, 85->22, 86->32, 87->34, 88->19, 89->23, 90->24, 91->13, 92->11, 93->41, 94->36, 95->42, 96->41, 97->34, 98->17, 99->33, 100->32, 101->8, 102->1, 103->30, 104->28, 105->20, 106->14, 107->41, 108->42, 109->42, 110->7, 111->41, 112->40, 113->32, 114->11, 115->33, 116->38, 117->28, 118->29, 119->44, 120->8, 121->37, 122->24, 123->26, 124->26, 125->43, 126->43, 127->19, 128->1, 129->26, 130->19, 131->9, 132->40, 133->4, 134->1, 135->39, 136->31, 137->42, 138->13, 139->37, 140->43, 141->14, 142->26, 143->46, 144->24, 145->31, 146->47, 147->39, 148->32, 149->45, 150->2, 151->25, 152->8, 153->27, 154->28, 155->42, 156->46, 157->2, 158->5, 159->30, 160->5, 161->38, 162->0, 163->23, 164->21, 165->9, 166->3, 167->36, 168->33, 169->0, 170->12, 171->34, 172->44, 173->16, 174->6, 175->40, 176->3, 177->27, 178->15, 179->4, 180->37, 181->23, 182->35, 183->4, 184->29, 185->21, 186->27, 187->38, 188->13, 189->29, 190->38, 191->14, 192->39, 193->0, 194->23, 195->5, 196->5, 197->8, 198->10, 199->35, 200->30, 201->35, 202->2, 203->13, 204->21, 205->47, 206->39, 207->28, 208->3, 209->10, 210->30, 211->21, 212->22, 213->3, 214->22, 215->6, 216->3, 217->36, 218->17, 219->26, 220->0, 221->22, 222->0, 223->45, 224->11, 225->7, 226->19, 227->46, 228->30, 229->46, 230->26, 231->47, 232->37, 233->36, 234->8, 235->41, 236->25, 237->43, 238->6, 239->35, 240->0, 241->38, 242->33, 243->14, 244->27, 245->40, 246->38, 247->47, 248->11, 249->18, 250->31, 251->34, 252->21, 253->15, 254->37, 255->46, 256->10, 257->17, 258->17, 259->7, 260->25, 261->44, 262->12, 263->20, 264->10, 265->16, 266->7, 267->2, 268->16, 269->10, 270->25, 271->16, 272->7, 273->35, 274->34, 275->44, 276->33, 277->3, 278->18, 279->32, 280->16, 281->9, 282->11, 283->18, 284->24, 285->25, 286->31, 287->46

Average CPI map:
	2.05,	2.88,	3.94,	1.79,	3.81,	2.70
	3.91,	2.38,	2.43,	3.49,	2.36,	2.14
	5.34,	3.28,	1.74,	3.04,	2.40,	3.40
	2.72,	2.70,	10.14,	2.58,	2.45,	2.18
	5.48,	1.50,	2.30,	1.68,	2.27,	2.27
	2.23,	2.23,	2.20,	1.43,	2.19,	2.19
	2.10,	2.17,	2.17,	1.70,	3.52,	2.15
	3.06,	2.15,	3.95,	3.30,	2.64,	3.27

Efficiency Satisfaction Map:
	1,	1,	1,	1,	1,	1
	1,	1,	1,	0,	1,	1
	1,	0,	0,	0,	0,	1
	1,	0,	1,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	0,	1
	1,	0,	0,	0,	1,	1
	1,	0,	1,	0,	1,	1
