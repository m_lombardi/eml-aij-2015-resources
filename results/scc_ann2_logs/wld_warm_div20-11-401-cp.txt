==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 3 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 401

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- worst case average CPI: 1.908
- worst case efficiency: 0.627
- efficiency to beat: 0.627
- normalized lower bound 538
--- checking solution
--- solution ok! Maximum rounding error: 0.000084
- solution found, obj var is: 653
- de-normalized obj var: 0.639180
- time (local/global): 0.000/0.000
- branches (local/global): 22/22
- fails (local/global): 0/0
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #1 ========================================
- worst case average CPI: 1.504
- worst case efficiency: 0.628
- efficiency to beat: 0.628
- normalized lower bound 547
--- checking solution
--- solution ok! Maximum rounding error: 0.000025
- solution found, obj var is: 617
- de-normalized obj var: 0.637020
- time (local/global): 0.001/0.006
- branches (local/global): 22/44
- fails (local/global): 0/0
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #2 ========================================
- worst case average CPI: 1.401
- worst case efficiency: 0.629
- efficiency to beat: 0.629
- normalized lower bound 562
--- checking solution
--- solution ok! Maximum rounding error: 0.000015
- solution found, obj var is: 597
- de-normalized obj var: 0.635820
- time (local/global): 0.000/0.010
- branches (local/global): 16/60
- fails (local/global): 0/0
- lns time: 0.004000
- lns branches: 16
- lns fails: 0
LNS iteration #3 ========================================
- worst case average CPI: 1.397
- worst case efficiency: 0.636
- efficiency to beat: 0.636
- normalized lower bound 681
--- checking solution
--- solution ok! Maximum rounding error: 0.000092
- solution found, obj var is: 738
- de-normalized obj var: 0.644280
- time (local/global): 0.006/0.020
- branches (local/global): 448/508
- fails (local/global): 200/200
- lns time: 0.010000
- lns branches: 448
- lns fails: 200
LNS iteration #4 ========================================
- worst case average CPI: 1.397
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 703
--- checking solution
--- solution ok! Maximum rounding error: 0.000138
- solution found, obj var is: 706
- de-normalized obj var: 0.642360
- time (local/global): 0.000/0.024
- branches (local/global): 21/529
- fails (local/global): 4/204
- lns time: 0.004000
- lns branches: 21
- lns fails: 4
LNS iteration #5 ========================================
- worst case average CPI: 1.095
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 704
--- checking solution
--- solution ok! Maximum rounding error: 0.000051
- solution found, obj var is: 711
- de-normalized obj var: 0.642660
- time (local/global): 0.001/0.029
- branches (local/global): 22/551
- fails (local/global): 0/204
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #6 ========================================
- worst case average CPI: 1.095
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 738
--- checking solution
--- solution ok! Maximum rounding error: 0.000066
- solution found, obj var is: 740
- de-normalized obj var: 0.644400
- time (local/global): 0.001/0.034
- branches (local/global): 22/573
- fails (local/global): 0/204
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #7 ========================================
- worst case average CPI: 1.095
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 740
--- checking solution
--- solution ok! Maximum rounding error: 0.000084
- solution found, obj var is: 752
- de-normalized obj var: 0.645120
- time (local/global): 0.004/0.042
- branches (local/global): 305/878
- fails (local/global): 134/338
- lns time: 0.009000
- lns branches: 305
- lns fails: 134
LNS iteration #8 ========================================
- worst case average CPI: 0.824
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 742
--- checking solution
--- solution ok! Maximum rounding error: 0.000037
- solution found, obj var is: 788
- de-normalized obj var: 0.647280
- time (local/global): 0.005/0.052
- branches (local/global): 455/1333
- fails (local/global): 200/538
- lns time: 0.009000
- lns branches: 455
- lns fails: 200
LNS iteration #9 ========================================
- worst case average CPI: 0.824
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 755
--- checking solution
--- solution ok! Maximum rounding error: 0.000062
- solution found, obj var is: 790
- de-normalized obj var: 0.647400
- time (local/global): 0.001/0.057
- branches (local/global): 25/1358
- fails (local/global): 2/540
- lns time: 0.005000
- lns branches: 25
- lns fails: 2
LNS iteration #10 ========================================
- worst case average CPI: 0.824
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 765
--- checking solution
--- solution ok! Maximum rounding error: 0.000049
- solution found, obj var is: 767
- de-normalized obj var: 0.646020
- time (local/global): 0.007/0.068
- branches (local/global): 532/1890
- fails (local/global): 241/781
- lns time: 0.011000
- lns branches: 532
- lns fails: 241
LNS iteration #11 ========================================
- worst case average CPI: 1.008
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 773
--- checking solution
--- solution ok! Maximum rounding error: 0.000058
- solution found, obj var is: 823
- de-normalized obj var: 0.649380
- time (local/global): 0.011/0.083
- branches (local/global): 891/2781
- fails (local/global): 408/1189
- lns time: 0.015000
- lns branches: 891
- lns fails: 408
LNS iteration #12 ========================================
- worst case average CPI: 1.008
- worst case efficiency: 0.643
- efficiency to beat: 0.643
- normalized lower bound 802
--- checking solution
--- solution ok! Maximum rounding error: 0.000072
- solution found, obj var is: 855
- de-normalized obj var: 0.651300
- time (local/global): 0.001/0.088
- branches (local/global): 24/2805
- fails (local/global): 1/1190
- lns time: 0.005000
- lns branches: 24
- lns fails: 1
LNS iteration #13 ========================================
- worst case average CPI: 1.008
- worst case efficiency: 0.644
- efficiency to beat: 0.644
- normalized lower bound 825
--- checking solution
--- solution ok! Maximum rounding error: 0.000008
- solution found, obj var is: 827
- de-normalized obj var: 0.649620
- time (local/global): 0.067/0.159
- branches (local/global): 5150/7955
- fails (local/global): 2450/3640
- lns time: 0.072000
- lns branches: 5150
- lns fails: 2450
LNS iteration #14 ========================================
- worst case average CPI: 1.008
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 832
--- checking solution
--- solution ok! Maximum rounding error: 0.000071
- solution found, obj var is: 833
- de-normalized obj var: 0.649980
- time (local/global): 0.001/0.165
- branches (local/global): 23/7978
- fails (local/global): 1/3641
- lns time: 0.005000
- lns branches: 23
- lns fails: 1
LNS iteration #15 ========================================
- worst case average CPI: 1.008
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 832
--- checking solution
--- solution ok! Maximum rounding error: 0.000098
- solution found, obj var is: 835
- de-normalized obj var: 0.650100
- time (local/global): 0.001/0.170
- branches (local/global): 21/7999
- fails (local/global): 0/3641
- lns time: 0.005000
- lns branches: 21
- lns fails: 0
LNS iteration #16 ========================================
- worst case average CPI: 1.120
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 836
--- checking solution
--- solution ok! Maximum rounding error: 0.000045
- solution found, obj var is: 842
- de-normalized obj var: 0.650520
- time (local/global): 0.001/0.175
- branches (local/global): 136/8135
- fails (local/global): 61/3702
- lns time: 0.005000
- lns branches: 136
- lns fails: 61
LNS iteration #17 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 241108
- lns fails: 117844
LNS iteration #18 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 226111
- lns fails: 110678
LNS iteration #19 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 247877
- lns fails: 121223
LNS iteration #20 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 210840
- lns fails: 103310
LNS iteration #21 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 212599
- lns fails: 104212
LNS iteration #22 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 277169
- lns fails: 135345
LNS iteration #23 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 219687
- lns fails: 107514
LNS iteration #24 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 192645
- lns fails: 94447
LNS iteration #25 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 227407
- lns fails: 111393
LNS iteration #26 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 239805
- lns fails: 117393
LNS iteration #27 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 194545
- lns fails: 95192
LNS iteration #28 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.006000
- lns branches: 273312
- lns fails: 134631
LNS iteration #29 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 237367
- lns fails: 116034
LNS iteration #30 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 243685
- lns fails: 119220
LNS iteration #31 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 185197
- lns fails: 90730
LNS iteration #32 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 177908
- lns fails: 87121
LNS iteration #33 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 172989
- lns fails: 84564
LNS iteration #34 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 232794
- lns fails: 113839
LNS iteration #35 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 256721
- lns fails: 125459
LNS iteration #36 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 209727
- lns fails: 102868
LNS iteration #37 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 213906
- lns fails: 104704
LNS iteration #38 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 217734
- lns fails: 106662
LNS iteration #39 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 170108
- lns fails: 83154
LNS iteration #40 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 207784
- lns fails: 102042
LNS iteration #41 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 233910
- lns fails: 114374
LNS iteration #42 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 226945
- lns fails: 111001
LNS iteration #43 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 266443
- lns fails: 130197
LNS iteration #44 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 222202
- lns fails: 108765
LNS iteration #45 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 199765
- lns fails: 98005
LNS iteration #46 ========================================
- worst case average CPI: 1.074
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 2.707000
- lns branches: 175703
- lns fails: 85984

LNS optimization is over========================================
initial worst case average CPI: 1.908
final worst case average CPI: 1.074
initial worst case efficiency: 0.627
final worst case efficiency: 0.645
number of iterations: 47
total time: 90004.000000
total branches: 6622128
total fails: 3241607

Final Solution: 0->0, 1->2, 2->28, 3->7, 4->18, 5->1, 6->18, 7->24, 8->19, 9->35, 10->5, 11->9, 12->3, 13->26, 14->28, 15->19, 16->28, 17->28, 18->44, 19->40, 20->47, 21->24, 22->19, 23->28, 24->26, 25->31, 26->40, 27->25, 28->32, 29->41, 30->9, 31->28, 32->18, 33->39, 34->1, 35->44, 36->15, 37->0, 38->5, 39->4, 40->2, 41->13, 42->14, 43->34, 44->14, 45->14, 46->33, 47->0, 48->36, 49->42, 50->20, 51->3, 52->33, 53->46, 54->25, 55->34, 56->16, 57->36, 58->21, 59->22, 60->24, 61->21, 62->32, 63->22, 64->46, 65->38, 66->25, 67->31, 68->19, 69->39, 70->43, 71->47, 72->4, 73->13, 74->38, 75->36, 76->12, 77->32, 78->35, 79->30, 80->24, 81->32, 82->43, 83->0, 84->12, 85->41, 86->11, 87->26, 88->33, 89->29, 90->36, 91->11, 92->33, 93->21, 94->17, 95->7, 96->9, 97->5, 98->37, 99->20, 100->14, 101->44, 102->27, 103->30, 104->45, 105->22, 106->15, 107->43, 108->6, 109->8, 110->5, 111->19, 112->32, 113->13, 114->37, 115->21, 116->22, 117->23, 118->42, 119->7, 120->18, 121->11, 122->33, 123->16, 124->8, 125->40, 126->17, 127->15, 128->43, 129->10, 130->46, 131->21, 132->7, 133->23, 134->22, 135->36, 136->9, 137->11, 138->20, 139->10, 140->6, 141->11, 142->20, 143->5, 144->3, 145->3, 146->42, 147->21, 148->37, 149->19, 150->8, 151->10, 152->38, 153->17, 154->9, 155->41, 156->32, 157->40, 158->1, 159->3, 160->7, 161->42, 162->34, 163->7, 164->20, 165->12, 166->37, 167->37, 168->34, 169->15, 170->26, 171->25, 172->0, 173->46, 174->29, 175->20, 176->1, 177->3, 178->47, 179->22, 180->23, 181->34, 182->16, 183->29, 184->17, 185->23, 186->6, 187->12, 188->39, 189->44, 190->43, 191->9, 192->12, 193->47, 194->31, 195->23, 196->13, 197->24, 198->42, 199->17, 200->29, 201->18, 202->16, 203->45, 204->25, 205->30, 206->45, 207->30, 208->5, 209->13, 210->24, 211->44, 212->6, 213->6, 214->8, 215->26, 216->15, 217->29, 218->23, 219->1, 220->41, 221->42, 222->38, 223->2, 224->25, 225->47, 226->35, 227->38, 228->45, 229->27, 230->46, 231->1, 232->4, 233->39, 234->27, 235->35, 236->11, 237->4, 238->14, 239->4, 240->2, 241->2, 242->46, 243->45, 244->31, 245->27, 246->14, 247->47, 248->10, 249->45, 250->15, 251->40, 252->27, 253->13, 254->10, 255->6, 256->0, 257->4, 258->40, 259->26, 260->8, 261->34, 262->18, 263->36, 264->35, 265->2, 266->39, 267->16, 268->31, 269->31, 270->29, 271->33, 272->30, 273->44, 274->41, 275->8, 276->17, 277->37, 278->16, 279->12, 280->10, 281->38, 282->39, 283->30, 284->27, 285->43, 286->41, 287->35

Average CPI map:
	2.31,	1.73,	1.34,	1.52,	1.07,	1.24
	2.14,	2.10,	2.08,	2.06,	1.96,	1.32
	2.04,	2.03,	2.34,	1.99,	2.35,	1.93
	1.97,	2.31,	2.49,	2.23,	2.40,	1.94
	1.57,	2.19,	3.05,	2.94,	2.48,	1.95
	1.40,	2.20,	1.94,	2.73,	2.15,	2.12
	1.51,	1.93,	2.30,	2.08,	1.91,	1.12
	1.92,	1.97,	1.91,	1.91,	1.91,	1.91

Efficiency map:
	0.997,	0.936,	0.880,	0.903,	0.919,	0.996
	0.839,	0.661,	0.651,	0.655,	0.654,	0.812
	0.830,	0.647,	0.652,	0.662,	0.650,	0.821
	0.819,	0.648,	0.648,	0.650,	0.668,	0.821
	0.804,	0.651,	0.650,	0.651,	0.650,	0.818
	0.771,	0.648,	0.645,	0.649,	0.651,	0.829
	0.872,	0.660,	0.650,	0.650,	0.656,	0.762
	0.840,	0.681,	0.664,	0.663,	0.677,	0.839
