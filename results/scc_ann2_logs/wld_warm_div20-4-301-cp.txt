==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 4 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 3 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 301

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- worst case average CPI: 1.916
- worst case efficiency: 0.627
- efficiency to beat: 0.627
- normalized lower bound 530
--- checking solution
--- solution ok! Maximum rounding error: 0.000097
- solution found, obj var is: 626
- de-normalized obj var: 0.637560
- time (local/global): 0.000/0.000
- branches (local/global): 22/22
- fails (local/global): 0/0
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #1 ========================================
- worst case average CPI: 1.395
- worst case efficiency: 0.628
- efficiency to beat: 0.628
- normalized lower bound 551
--- checking solution
--- solution ok! Maximum rounding error: 0.000066
- solution found, obj var is: 573
- de-normalized obj var: 0.634380
- time (local/global): 0.108/0.113
- branches (local/global): 6661/6683
- fails (local/global): 3201/3201
- lns time: 0.113000
- lns branches: 6661
- lns fails: 3201
LNS iteration #2 ========================================
- worst case average CPI: 1.378
- worst case efficiency: 0.634
- efficiency to beat: 0.634
- normalized lower bound 657
--- checking solution
--- solution ok! Maximum rounding error: 0.000025
- solution found, obj var is: 658
- de-normalized obj var: 0.639480
- time (local/global): 0.053/0.171
- branches (local/global): 3824/10507
- fails (local/global): 1803/5004
- lns time: 0.059000
- lns branches: 3824
- lns fails: 1803
LNS iteration #3 ========================================
- worst case average CPI: 1.395
- worst case efficiency: 0.635
- efficiency to beat: 0.635
- normalized lower bound 661
--- checking solution
--- solution ok! Maximum rounding error: 0.000078
- solution found, obj var is: 675
- de-normalized obj var: 0.640500
- time (local/global): 0.000/0.177
- branches (local/global): 27/10534
- fails (local/global): 4/5008
- lns time: 0.005000
- lns branches: 27
- lns fails: 4
LNS iteration #4 ========================================
- worst case average CPI: 1.395
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 701
--- checking solution
--- solution ok! Maximum rounding error: 0.000077
- solution found, obj var is: 781
- de-normalized obj var: 0.646860
- time (local/global): 0.001/0.183
- branches (local/global): 22/10556
- fails (local/global): 0/5008
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #5 ========================================
- worst case average CPI: 1.395
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 703
--- checking solution
--- solution ok! Maximum rounding error: 0.000032
- solution found, obj var is: 705
- de-normalized obj var: 0.642300
- time (local/global): 0.048/0.235
- branches (local/global): 4585/15141
- fails (local/global): 2170/7178
- lns time: 0.053000
- lns branches: 4585
- lns fails: 2170
LNS iteration #6 ========================================
- worst case average CPI: 1.395
- worst case efficiency: 0.638
- efficiency to beat: 0.638
- normalized lower bound 712
--- checking solution
--- solution ok! Maximum rounding error: 0.000072
- solution found, obj var is: 717
- de-normalized obj var: 0.643020
- time (local/global): 0.048/0.288
- branches (local/global): 2770/17911
- fails (local/global): 1313/8491
- lns time: 0.052000
- lns branches: 2770
- lns fails: 1313
LNS iteration #7 ========================================
- worst case average CPI: 1.395
- worst case efficiency: 0.638
- efficiency to beat: 0.638
- normalized lower bound 716
--- checking solution
--- solution ok! Maximum rounding error: 0.000000
- solution found, obj var is: 719
- de-normalized obj var: 0.643140
- time (local/global): 0.020/0.312
- branches (local/global): 1420/19331
- fails (local/global): 680/9171
- lns time: 0.026000
- lns branches: 1420
- lns fails: 680
LNS iteration #8 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 727
--- checking solution
--- solution ok! Maximum rounding error: 0.000040
- solution found, obj var is: 749
- de-normalized obj var: 0.644940
- time (local/global): 0.003/0.321
- branches (local/global): 237/19568
- fails (local/global): 100/9271
- lns time: 0.008000
- lns branches: 237
- lns fails: 100
LNS iteration #9 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 736
--- checking solution
--- solution ok! Maximum rounding error: 0.000041
- solution found, obj var is: 779
- de-normalized obj var: 0.646740
- time (local/global): 0.001/0.327
- branches (local/global): 24/19592
- fails (local/global): 1/9272
- lns time: 0.005000
- lns branches: 24
- lns fails: 1
LNS iteration #10 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 742
--- checking solution
--- solution ok! Maximum rounding error: 0.000235
- solution found, obj var is: 803
- de-normalized obj var: 0.648180
- time (local/global): 0.001/0.332
- branches (local/global): 24/19616
- fails (local/global): 1/9273
- lns time: 0.005000
- lns branches: 24
- lns fails: 1
LNS iteration #11 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 752
--- checking solution
--- solution ok! Maximum rounding error: 0.000013
- solution found, obj var is: 758
- de-normalized obj var: 0.645480
- time (local/global): 0.001/0.337
- branches (local/global): 27/19643
- fails (local/global): 2/9275
- lns time: 0.005000
- lns branches: 27
- lns fails: 2
LNS iteration #12 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 755
- lns time: 3.004000
- lns branches: 191383
- lns fails: 93713
LNS iteration #13 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 755
--- checking solution
--- solution ok! Maximum rounding error: 0.000074
- solution found, obj var is: 769
- de-normalized obj var: 0.646140
- time (local/global): 0.005/3.350
- branches (local/global): 400/211426
- fails (local/global): 181/103169
- lns time: 0.010000
- lns branches: 400
- lns fails: 181
LNS iteration #14 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 769
--- checking solution
--- solution ok! Maximum rounding error: 0.000100
- solution found, obj var is: 771
- de-normalized obj var: 0.646260
- time (local/global): 0.000/3.355
- branches (local/global): 22/211448
- fails (local/global): 0/103169
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #15 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.643
- efficiency to beat: 0.643
- normalized lower bound 798
- lns time: 3.004000
- lns branches: 207498
- lns fails: 101768
LNS iteration #16 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.643
- efficiency to beat: 0.643
- normalized lower bound 798
--- checking solution
--- solution ok! Maximum rounding error: 0.000050
- solution found, obj var is: 929
- de-normalized obj var: 0.655740
- time (local/global): 0.001/6.365
- branches (local/global): 22/418968
- fails (local/global): 0/204937
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #17 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.643
- efficiency to beat: 0.643
- normalized lower bound 803
--- checking solution
--- solution ok! Maximum rounding error: 0.000068
- solution found, obj var is: 803
- de-normalized obj var: 0.648180
- time (local/global): 0.082/6.451
- branches (local/global): 6671/425639
- fails (local/global): 3200/208137
- lns time: 0.086000
- lns branches: 6671
- lns fails: 3200
LNS iteration #18 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.644
- efficiency to beat: 0.644
- normalized lower bound 822
- lns time: 3.004000
- lns branches: 274309
- lns fails: 133988
LNS iteration #19 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.644
- efficiency to beat: 0.644
- normalized lower bound 822
- lns time: 3.004000
- lns branches: 210631
- lns fails: 103281
LNS iteration #20 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.644
- efficiency to beat: 0.644
- normalized lower bound 822
--- checking solution
--- solution ok! Maximum rounding error: 0.000126
- solution found, obj var is: 837
- de-normalized obj var: 0.650220
- time (local/global): 0.006/12.469
- branches (local/global): 457/911036
- fails (local/global): 200/445606
- lns time: 0.010000
- lns branches: 457
- lns fails: 200
LNS iteration #21 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 831
--- checking solution
--- solution ok! Maximum rounding error: 0.000071
- solution found, obj var is: 913
- de-normalized obj var: 0.654780
- time (local/global): 0.001/12.474
- branches (local/global): 22/911058
- fails (local/global): 0/445606
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #22 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.007000
- lns branches: 205402
- lns fails: 100653
LNS iteration #23 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 202438
- lns fails: 100087
LNS iteration #24 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 186758
- lns fails: 91472
LNS iteration #25 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 210623
- lns fails: 103260
LNS iteration #26 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 212632
- lns fails: 104254
LNS iteration #27 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 247504
- lns fails: 121111
LNS iteration #28 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 202795
- lns fails: 99568
LNS iteration #29 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 200554
- lns fails: 98421
LNS iteration #30 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 226965
- lns fails: 111235
LNS iteration #31 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 202016
- lns fails: 98990
LNS iteration #32 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 207622
- lns fails: 101879
LNS iteration #33 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 197655
- lns fails: 96737
LNS iteration #34 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 227629
- lns fails: 111474
LNS iteration #35 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 171072
- lns fails: 83554
LNS iteration #36 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 247775
- lns fails: 122369
LNS iteration #37 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 183809
- lns fails: 89918
LNS iteration #38 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 282670
- lns fails: 139305
LNS iteration #39 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 243866
- lns fails: 120490
LNS iteration #40 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 182386
- lns fails: 89127
LNS iteration #41 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.005000
- lns branches: 208524
- lns fails: 102182
LNS iteration #42 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 211207
- lns fails: 103618
LNS iteration #43 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.005000
- lns branches: 178776
- lns fails: 87441
LNS iteration #44 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 196557
- lns fails: 96258
LNS iteration #45 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 232480
- lns fails: 113837
LNS iteration #46 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 3.004000
- lns branches: 182043
- lns fails: 89077
LNS iteration #47 ========================================
- worst case average CPI: 1.122
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
- lns time: 2.421000
- lns branches: 164774
- lns fails: 80255

LNS optimization is over========================================
initial worst case average CPI: 1.916
final worst case average CPI: 1.122
initial worst case efficiency: 0.627
final worst case efficiency: 0.645
number of iterations: 48
total time: 90004.000000
total branches: 6327590
total fails: 3102178

Final Solution: 0->26, 1->7, 2->17, 3->43, 4->10, 5->2, 6->0, 7->6, 8->31, 9->18, 10->12, 11->27, 12->17, 13->35, 14->18, 15->2, 16->3, 17->33, 18->29, 19->36, 20->38, 21->23, 22->46, 23->26, 24->9, 25->41, 26->45, 27->23, 28->10, 29->29, 30->33, 31->7, 32->30, 33->35, 34->31, 35->28, 36->3, 37->16, 38->26, 39->24, 40->18, 41->44, 42->16, 43->20, 44->26, 45->45, 46->30, 47->20, 48->28, 49->5, 50->12, 51->5, 52->33, 53->34, 54->25, 55->11, 56->39, 57->6, 58->34, 59->0, 60->32, 61->44, 62->46, 63->38, 64->39, 65->32, 66->4, 67->23, 68->33, 69->23, 70->43, 71->4, 72->41, 73->19, 74->28, 75->34, 76->8, 77->9, 78->16, 79->19, 80->36, 81->9, 82->23, 83->38, 84->34, 85->16, 86->6, 87->32, 88->47, 89->38, 90->13, 91->8, 92->31, 93->31, 94->9, 95->47, 96->42, 97->28, 98->27, 99->21, 100->14, 101->25, 102->15, 103->42, 104->37, 105->0, 106->30, 107->32, 108->38, 109->13, 110->44, 111->15, 112->44, 113->11, 114->42, 115->28, 116->19, 117->34, 118->19, 119->13, 120->12, 121->4, 122->8, 123->44, 124->29, 125->18, 126->34, 127->6, 128->20, 129->13, 130->37, 131->32, 132->32, 133->12, 134->42, 135->42, 136->16, 137->15, 138->27, 139->9, 140->4, 141->14, 142->16, 143->20, 144->19, 145->11, 146->10, 147->20, 148->15, 149->35, 150->27, 151->1, 152->8, 153->10, 154->47, 155->2, 156->26, 157->15, 158->46, 159->22, 160->44, 161->4, 162->39, 163->11, 164->5, 165->5, 166->30, 167->39, 168->2, 169->9, 170->10, 171->1, 172->8, 173->40, 174->33, 175->24, 176->29, 177->46, 178->45, 179->26, 180->22, 181->5, 182->14, 183->19, 184->37, 185->41, 186->12, 187->40, 188->3, 189->39, 190->25, 191->17, 192->37, 193->0, 194->37, 195->13, 196->1, 197->7, 198->3, 199->24, 200->30, 201->24, 202->23, 203->29, 204->45, 205->45, 206->17, 207->40, 208->35, 209->2, 210->22, 211->12, 212->1, 213->42, 214->29, 215->47, 216->17, 217->7, 218->1, 219->31, 220->46, 221->28, 222->39, 223->14, 224->3, 225->43, 226->41, 227->0, 228->30, 229->36, 230->3, 231->7, 232->1, 233->31, 234->18, 235->40, 236->7, 237->6, 238->14, 239->21, 240->43, 241->11, 242->14, 243->24, 244->46, 245->37, 246->43, 247->40, 248->25, 249->21, 250->22, 251->6, 252->15, 253->38, 254->0, 255->5, 256->40, 257->11, 258->47, 259->27, 260->36, 261->2, 262->43, 263->27, 264->21, 265->35, 266->41, 267->36, 268->35, 269->10, 270->25, 271->8, 272->4, 273->13, 274->25, 275->21, 276->17, 277->33, 278->45, 279->20, 280->22, 281->18, 282->47, 283->21, 284->22, 285->24, 286->41, 287->36

Average CPI map:
	2.45,	1.76,	1.67,	2.45,	1.56,	2.11
	2.11,	2.22,	2.06,	2.06,	2.02,	1.87
	1.49,	2.03,	2.00,	1.27,	2.19,	1.40
	1.12,	2.26,	2.44,	2.11,	2.10,	1.60
	1.47,	2.28,	3.08,	2.78,	2.38,	2.00
	1.95,	1.94,	1.73,	2.63,	2.60,	2.08
	1.65,	1.51,	2.31,	2.44,	1.65,	2.68
	2.17,	1.72,	1.77,	1.76,	1.41,	1.67

Efficiency map:
	0.997,	0.937,	0.909,	0.950,	0.928,	0.996
	0.837,	0.665,	0.650,	0.655,	0.658,	0.837
	0.813,	0.647,	0.646,	0.662,	0.648,	0.806
	0.794,	0.646,	0.646,	0.646,	0.649,	0.810
	0.799,	0.654,	0.650,	0.648,	0.647,	0.822
	0.805,	0.646,	0.645,	0.646,	0.668,	0.829
	0.872,	0.656,	0.650,	0.672,	0.650,	0.859
	0.849,	0.667,	0.655,	0.652,	0.665,	0.829
