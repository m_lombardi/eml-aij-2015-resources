==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 3 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 601

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- worst case average CPI: 1.908
- worst case efficiency: 0.627
- efficiency to beat: 0.627
- normalized lower bound 538
--- checking solution
--- solution ok! Maximum rounding error: 0.000019
- solution found, obj var is: 656
- de-normalized obj var: 0.639360
- time (local/global): 0.001/0.001
- branches (local/global): 16/16
- fails (local/global): 0/0
- lns time: 0.005000
- lns branches: 16
- lns fails: 0
LNS iteration #1 ========================================
- worst case average CPI: 1.673
- worst case efficiency: 0.628
- efficiency to beat: 0.628
- normalized lower bound 547
--- checking solution
--- solution ok! Maximum rounding error: 0.000000
- solution found, obj var is: 724
- de-normalized obj var: 0.643440
- time (local/global): 0.000/0.005
- branches (local/global): 20/36
- fails (local/global): 2/2
- lns time: 0.004000
- lns branches: 20
- lns fails: 2
LNS iteration #2 ========================================
- worst case average CPI: 1.466
- worst case efficiency: 0.629
- efficiency to beat: 0.629
- normalized lower bound 562
--- checking solution
--- solution ok! Maximum rounding error: 0.000053
- solution found, obj var is: 591
- de-normalized obj var: 0.635460
- time (local/global): 0.001/0.010
- branches (local/global): 20/56
- fails (local/global): 0/2
- lns time: 0.005000
- lns branches: 20
- lns fails: 0
LNS iteration #3 ========================================
- worst case average CPI: 1.440
- worst case efficiency: 0.635
- efficiency to beat: 0.635
- normalized lower bound 675
--- checking solution
--- solution ok! Maximum rounding error: 0.000053
- solution found, obj var is: 755
- de-normalized obj var: 0.645300
- time (local/global): 0.000/0.014
- branches (local/global): 22/78
- fails (local/global): 0/2
- lns time: 0.004000
- lns branches: 22
- lns fails: 0
LNS iteration #4 ========================================
- worst case average CPI: 1.440
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 700
--- checking solution
--- solution ok! Maximum rounding error: 0.000079
- solution found, obj var is: 700
- de-normalized obj var: 0.642000
- time (local/global): 0.002/0.020
- branches (local/global): 88/166
- fails (local/global): 35/37
- lns time: 0.006000
- lns branches: 88
- lns fails: 35
LNS iteration #5 ========================================
- worst case average CPI: 1.440
- worst case efficiency: 0.638
- efficiency to beat: 0.638
- normalized lower bound 718
--- checking solution
--- solution ok! Maximum rounding error: 0.000057
- solution found, obj var is: 726
- de-normalized obj var: 0.643560
- time (local/global): 0.000/0.024
- branches (local/global): 20/186
- fails (local/global): 0/37
- lns time: 0.005000
- lns branches: 20
- lns fails: 0
LNS iteration #6 ========================================
- worst case average CPI: 1.440
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 738
--- checking solution
--- solution ok! Maximum rounding error: 0.000044
- solution found, obj var is: 932
- de-normalized obj var: 0.655920
- time (local/global): 0.000/0.029
- branches (local/global): 21/207
- fails (local/global): 0/37
- lns time: 0.004000
- lns branches: 21
- lns fails: 0
LNS iteration #7 ========================================
- worst case average CPI: 1.440
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 740
--- checking solution
--- solution ok! Maximum rounding error: 0.000000
- solution found, obj var is: 749
- de-normalized obj var: 0.644940
- time (local/global): 0.001/0.034
- branches (local/global): 57/264
- fails (local/global): 19/56
- lns time: 0.005000
- lns branches: 57
- lns fails: 19
LNS iteration #8 ========================================
- worst case average CPI: 1.414
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 744
--- checking solution
--- solution ok! Maximum rounding error: 0.000064
- solution found, obj var is: 787
- de-normalized obj var: 0.647220
- time (local/global): 0.003/0.041
- branches (local/global): 236/500
- fails (local/global): 100/156
- lns time: 0.007000
- lns branches: 236
- lns fails: 100
LNS iteration #9 ========================================
- worst case average CPI: 1.414
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 747
--- checking solution
--- solution ok! Maximum rounding error: 0.000167
- solution found, obj var is: 795
- de-normalized obj var: 0.647700
- time (local/global): 0.003/0.048
- branches (local/global): 240/740
- fails (local/global): 100/256
- lns time: 0.007000
- lns branches: 240
- lns fails: 100
LNS iteration #10 ========================================
- worst case average CPI: 1.414
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 760
--- checking solution
--- solution ok! Maximum rounding error: 0.000026
- solution found, obj var is: 769
- de-normalized obj var: 0.646140
- time (local/global): 0.019/0.071
- branches (local/global): 1303/2043
- fails (local/global): 601/857
- lns time: 0.023000
- lns branches: 1303
- lns fails: 601
LNS iteration #11 ========================================
- worst case average CPI: 0.697
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 772
--- checking solution
--- solution ok! Maximum rounding error: 0.000124
- solution found, obj var is: 805
- de-normalized obj var: 0.648300
- time (local/global): 0.003/0.078
- branches (local/global): 240/2283
- fails (local/global): 101/958
- lns time: 0.007000
- lns branches: 240
- lns fails: 101
LNS iteration #12 ========================================
- worst case average CPI: 0.697
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 772
--- checking solution
--- solution ok! Maximum rounding error: 0.000058
- solution found, obj var is: 993
- de-normalized obj var: 0.659580
- time (local/global): 0.002/0.084
- branches (local/global): 231/2514
- fails (local/global): 101/1059
- lns time: 0.006000
- lns branches: 231
- lns fails: 101
LNS iteration #13 ========================================
- worst case average CPI: 0.697
- worst case efficiency: 0.642
- efficiency to beat: 0.642
- normalized lower bound 782
--- checking solution
--- solution ok! Maximum rounding error: 0.000050
- solution found, obj var is: 807
- de-normalized obj var: 0.648420
- time (local/global): 0.002/0.090
- branches (local/global): 89/2603
- fails (local/global): 35/1094
- lns time: 0.006000
- lns branches: 89
- lns fails: 35
LNS iteration #14 ========================================
- worst case average CPI: 0.697
- worst case efficiency: 0.643
- efficiency to beat: 0.643
- normalized lower bound 798
--- checking solution
--- solution ok! Maximum rounding error: 0.000073
- solution found, obj var is: 852
- de-normalized obj var: 0.651120
- time (local/global): 0.005/0.099
- branches (local/global): 475/3078
- fails (local/global): 211/1305
- lns time: 0.009000
- lns branches: 475
- lns fails: 211
LNS iteration #15 ========================================
- worst case average CPI: 0.697
- worst case efficiency: 0.644
- efficiency to beat: 0.644
- normalized lower bound 811
--- checking solution
--- solution ok! Maximum rounding error: 0.000070
- solution found, obj var is: 816
- de-normalized obj var: 0.648960
- time (local/global): 0.316/0.419
- branches (local/global): 28652/31730
- fails (local/global): 13834/15139
- lns time: 0.320000
- lns branches: 28652
- lns fails: 13834
LNS iteration #16 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 833
--- checking solution
--- solution ok! Maximum rounding error: 0.000061
- solution found, obj var is: 881
- de-normalized obj var: 0.652860
- time (local/global): 0.045/0.468
- branches (local/global): 4326/36056
- fails (local/global): 2040/17179
- lns time: 0.049000
- lns branches: 4326
- lns fails: 2040
LNS iteration #17 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 197503
- lns fails: 96806
LNS iteration #18 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 214565
- lns fails: 105151
LNS iteration #19 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 235887
- lns fails: 115372
LNS iteration #20 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 200038
- lns fails: 98034
LNS iteration #21 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 243471
- lns fails: 119109
LNS iteration #22 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 230448
- lns fails: 112843
LNS iteration #23 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 192628
- lns fails: 94220
LNS iteration #24 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 212696
- lns fails: 104230
LNS iteration #25 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 212103
- lns fails: 103891
LNS iteration #26 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 213658
- lns fails: 104826
LNS iteration #27 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 278174
- lns fails: 137301
LNS iteration #28 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 235430
- lns fails: 115223
LNS iteration #29 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 200902
- lns fails: 98516
LNS iteration #30 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 243188
- lns fails: 118880
LNS iteration #31 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 191704
- lns fails: 93835
LNS iteration #32 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 270466
- lns fails: 132123
LNS iteration #33 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 202951
- lns fails: 99488
LNS iteration #34 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 208958
- lns fails: 102338
LNS iteration #35 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 224530
- lns fails: 110014
LNS iteration #36 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 258807
- lns fails: 126465
LNS iteration #37 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 210910
- lns fails: 103296
LNS iteration #38 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 213317
- lns fails: 104611
LNS iteration #39 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 236669
- lns fails: 115679
LNS iteration #40 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 210847
- lns fails: 103458
LNS iteration #41 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 208529
- lns fails: 102240
LNS iteration #42 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 335984
- lns fails: 165602
LNS iteration #43 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 185720
- lns fails: 90817
LNS iteration #44 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 194244
- lns fails: 95025
LNS iteration #45 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 192039
- lns fails: 93968
LNS iteration #46 ========================================
- worst case average CPI: 0.962
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 2.416000
- lns branches: 189050
- lns fails: 92474

LNS optimization is over========================================
initial worst case average CPI: 1.908
final worst case average CPI: 0.962
initial worst case efficiency: 0.627
final worst case efficiency: 0.645
number of iterations: 47
total time: 90004.000000
total branches: 6681472
total fails: 3273014

Final Solution: 0->0, 1->44, 2->11, 3->7, 4->30, 5->9, 6->30, 7->34, 8->11, 9->10, 10->36, 11->45, 12->35, 13->45, 14->17, 15->19, 16->17, 17->17, 18->9, 19->1, 20->47, 21->19, 22->38, 23->29, 24->38, 25->16, 26->40, 27->4, 28->32, 29->41, 30->29, 31->23, 32->30, 33->3, 34->46, 35->38, 36->15, 37->0, 38->31, 39->37, 40->31, 41->13, 42->11, 43->16, 44->26, 45->36, 46->20, 47->0, 48->23, 49->42, 50->24, 51->44, 52->8, 53->1, 54->4, 55->35, 56->6, 57->25, 58->24, 59->3, 60->39, 61->26, 62->32, 63->8, 64->39, 65->43, 66->4, 67->44, 68->34, 69->20, 70->14, 71->47, 72->14, 73->13, 74->44, 75->4, 76->25, 77->32, 78->31, 79->18, 80->30, 81->32, 82->43, 83->0, 84->6, 85->3, 86->3, 87->4, 88->31, 89->29, 90->4, 91->24, 92->7, 93->41, 94->40, 95->34, 96->23, 97->43, 98->2, 99->27, 100->17, 101->29, 102->12, 103->18, 104->18, 105->3, 106->15, 107->12, 108->46, 109->8, 110->43, 111->24, 112->32, 113->13, 114->21, 115->33, 116->14, 117->37, 118->42, 119->24, 120->30, 121->33, 122->34, 123->37, 124->37, 125->29, 126->31, 127->15, 128->8, 129->19, 130->23, 131->12, 132->7, 133->21, 134->25, 135->25, 136->12, 137->23, 138->17, 139->12, 140->28, 141->10, 142->5, 143->34, 144->34, 145->41, 146->42, 147->41, 148->21, 149->7, 150->19, 151->14, 152->10, 153->6, 154->27, 155->6, 156->32, 157->33, 158->10, 159->33, 160->7, 161->42, 162->22, 163->7, 164->17, 165->29, 166->21, 167->6, 168->24, 169->15, 170->9, 171->44, 172->0, 173->39, 174->9, 175->20, 176->26, 177->41, 178->47, 179->39, 180->27, 181->37, 182->33, 183->6, 184->9, 185->9, 186->39, 187->23, 188->19, 189->25, 190->43, 191->45, 192->28, 193->47, 194->20, 195->14, 196->13, 197->11, 198->42, 199->28, 200->36, 201->5, 202->12, 203->30, 204->38, 205->45, 206->27, 207->19, 208->2, 209->13, 210->5, 211->25, 212->39, 213->2, 214->40, 215->21, 216->15, 217->33, 218->36, 219->36, 220->31, 221->42, 222->38, 223->36, 224->38, 225->47, 226->1, 227->28, 228->18, 229->1, 230->46, 231->3, 232->35, 233->18, 234->1, 235->2, 236->16, 237->1, 238->27, 239->37, 240->44, 241->2, 242->46, 243->5, 244->11, 245->26, 246->14, 247->47, 248->20, 249->18, 250->15, 251->40, 252->11, 253->13, 254->22, 255->2, 256->0, 257->8, 258->40, 259->10, 260->45, 261->46, 262->45, 263->27, 264->5, 265->5, 266->22, 267->16, 268->20, 269->22, 270->40, 271->26, 272->35, 273->22, 274->41, 275->8, 276->26, 277->21, 278->16, 279->16, 280->22, 281->43, 282->46, 283->35, 284->28, 285->35, 286->10, 287->28

Average CPI map:
	2.31,	1.98,	1.16,	2.19,	2.18,	0.96
	1.29,	2.05,	2.31,	1.81,	2.05,	1.92
	1.54,	2.03,	2.14,	1.99,	1.98,	2.13
	1.32,	2.53,	2.49,	2.52,	2.08,	2.24
	1.96,	2.37,	2.86,	2.84,	2.61,	1.25
	2.24,	2.11,	1.94,	2.76,	2.11,	1.44
	1.73,	1.27,	2.22,	2.18,	1.67,	1.93
	1.92,	1.91,	1.87,	1.97,	1.72,	1.91

Efficiency map:
	0.997,	0.937,	0.862,	0.939,	0.935,	0.996
	0.799,	0.659,	0.664,	0.649,	0.659,	0.839
	0.814,	0.647,	0.648,	0.662,	0.646,	0.827
	0.801,	0.651,	0.648,	0.660,	0.648,	0.830
	0.820,	0.659,	0.646,	0.649,	0.653,	0.743
	0.823,	0.648,	0.645,	0.650,	0.649,	0.800
	0.872,	0.654,	0.648,	0.656,	0.650,	0.816
	0.839,	0.677,	0.661,	0.667,	0.672,	0.839
