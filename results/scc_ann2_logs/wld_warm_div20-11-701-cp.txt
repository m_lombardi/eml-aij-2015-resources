==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 3 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 701

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- worst case average CPI: 1.908
- worst case efficiency: 0.627
- efficiency to beat: 0.627
- normalized lower bound 538
--- checking solution
--- solution ok! Maximum rounding error: 0.000060
- solution found, obj var is: 587
- de-normalized obj var: 0.635220
- time (local/global): 0.000/0.000
- branches (local/global): 22/22
- fails (local/global): 0/0
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #1 ========================================
- worst case average CPI: 1.599
- worst case efficiency: 0.628
- efficiency to beat: 0.628
- normalized lower bound 548
--- checking solution
--- solution ok! Maximum rounding error: 0.000026
- solution found, obj var is: 548
- de-normalized obj var: 0.632880
- time (local/global): 0.042/0.047
- branches (local/global): 3382/3404
- fails (local/global): 1602/1602
- lns time: 0.046000
- lns branches: 3382
- lns fails: 1602
LNS iteration #2 ========================================
- worst case average CPI: 1.582
- worst case efficiency: 0.633
- efficiency to beat: 0.633
- normalized lower bound 632
--- checking solution
--- solution ok! Maximum rounding error: 0.000037
- solution found, obj var is: 642
- de-normalized obj var: 0.638520
- time (local/global): 0.001/0.052
- branches (local/global): 36/3440
- fails (local/global): 7/1609
- lns time: 0.006000
- lns branches: 36
- lns fails: 7
LNS iteration #3 ========================================
- worst case average CPI: 1.582
- worst case efficiency: 0.634
- efficiency to beat: 0.634
- normalized lower bound 652
--- checking solution
--- solution ok! Maximum rounding error: 0.000048
- solution found, obj var is: 667
- de-normalized obj var: 0.640020
- time (local/global): 0.010/0.067
- branches (local/global): 873/4313
- fails (local/global): 401/2010
- lns time: 0.014000
- lns branches: 873
- lns fails: 401
LNS iteration #4 ========================================
- worst case average CPI: 1.453
- worst case efficiency: 0.635
- efficiency to beat: 0.635
- normalized lower bound 670
--- checking solution
--- solution ok! Maximum rounding error: 0.000047
- solution found, obj var is: 673
- de-normalized obj var: 0.640380
- time (local/global): 0.006/0.077
- branches (local/global): 456/4769
- fails (local/global): 203/2213
- lns time: 0.011000
- lns branches: 456
- lns fails: 203
LNS iteration #5 ========================================
- worst case average CPI: 1.453
- worst case efficiency: 0.635
- efficiency to beat: 0.635
- normalized lower bound 672
--- checking solution
--- solution ok! Maximum rounding error: 0.000004
- solution found, obj var is: 794
- de-normalized obj var: 0.647640
- time (local/global): 0.001/0.083
- branches (local/global): 22/4791
- fails (local/global): 0/2213
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #6 ========================================
- worst case average CPI: 1.453
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 702
--- checking solution
--- solution ok! Maximum rounding error: 0.000044
- solution found, obj var is: 848
- de-normalized obj var: 0.650880
- time (local/global): 0.001/0.088
- branches (local/global): 20/4811
- fails (local/global): 0/2213
- lns time: 0.005000
- lns branches: 20
- lns fails: 0
LNS iteration #7 ========================================
- worst case average CPI: 1.453
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 731
--- checking solution
--- solution ok! Maximum rounding error: 0.000153
- solution found, obj var is: 806
- de-normalized obj var: 0.648360
- time (local/global): 0.001/0.093
- branches (local/global): 23/4834
- fails (local/global): 1/2214
- lns time: 0.006000
- lns branches: 23
- lns fails: 1
LNS iteration #8 ========================================
- worst case average CPI: 1.453
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 732
--- checking solution
--- solution ok! Maximum rounding error: 0.000011
- solution found, obj var is: 737
- de-normalized obj var: 0.644220
- time (local/global): 0.010/0.108
- branches (local/global): 1038/5872
- fails (local/global): 494/2708
- lns time: 0.014000
- lns branches: 1038
- lns fails: 494
LNS iteration #9 ========================================
- worst case average CPI: 1.311
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 746
--- checking solution
--- solution ok! Maximum rounding error: 0.000000
- solution found, obj var is: 756
- de-normalized obj var: 0.645360
- time (local/global): 0.007/0.119
- branches (local/global): 294/6166
- fails (local/global): 129/2837
- lns time: 0.011000
- lns branches: 294
- lns fails: 129
LNS iteration #10 ========================================
- worst case average CPI: 1.311
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 752
--- checking solution
--- solution ok! Maximum rounding error: 0.000040
- solution found, obj var is: 753
- de-normalized obj var: 0.645180
- time (local/global): 0.005/0.128
- branches (local/global): 255/6421
- fails (local/global): 112/2949
- lns time: 0.009000
- lns branches: 255
- lns fails: 112
LNS iteration #11 ========================================
- worst case average CPI: 1.311
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 758
--- checking solution
--- solution ok! Maximum rounding error: 0.000066
- solution found, obj var is: 768
- de-normalized obj var: 0.646080
- time (local/global): 0.005/0.137
- branches (local/global): 457/6878
- fails (local/global): 200/3149
- lns time: 0.009000
- lns branches: 457
- lns fails: 200
LNS iteration #12 ========================================
- worst case average CPI: 1.311
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 773
--- checking solution
--- solution ok! Maximum rounding error: 0.000053
- solution found, obj var is: 777
- de-normalized obj var: 0.646620
- time (local/global): 0.003/0.144
- branches (local/global): 241/7119
- fails (local/global): 101/3250
- lns time: 0.007000
- lns branches: 241
- lns fails: 101
LNS iteration #13 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.642
- efficiency to beat: 0.642
- normalized lower bound 782
--- checking solution
--- solution ok! Maximum rounding error: 0.000104
- solution found, obj var is: 782
- de-normalized obj var: 0.646920
- time (local/global): 0.035/0.183
- branches (local/global): 3120/10239
- fails (local/global): 1475/4725
- lns time: 0.039000
- lns branches: 3120
- lns fails: 1475
LNS iteration #14 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 826
--- checking solution
--- solution ok! Maximum rounding error: 0.000000
- solution found, obj var is: 834
- de-normalized obj var: 0.650040
- time (local/global): 0.001/0.188
- branches (local/global): 22/10261
- fails (local/global): 0/4725
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #15 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 832
--- checking solution
--- solution ok! Maximum rounding error: 0.000169
- solution found, obj var is: 849
- de-normalized obj var: 0.650940
- time (local/global): 0.001/0.193
- branches (local/global): 22/10283
- fails (local/global): 0/4725
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #16 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 836
--- checking solution
--- solution ok! Maximum rounding error: 0.000077
- solution found, obj var is: 845
- de-normalized obj var: 0.650700
- time (local/global): 0.001/0.198
- branches (local/global): 22/10305
- fails (local/global): 1/4726
- lns time: 0.005000
- lns branches: 22
- lns fails: 1
LNS iteration #17 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 842
--- checking solution
--- solution ok! Maximum rounding error: 0.000054
- solution found, obj var is: 843
- de-normalized obj var: 0.650580
- time (local/global): 0.012/0.214
- branches (local/global): 1085/11390
- fails (local/global): 500/5226
- lns time: 0.016000
- lns branches: 1085
- lns fails: 500
LNS iteration #18 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 215463
- lns fails: 105526
LNS iteration #19 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 237009
- lns fails: 116017
LNS iteration #20 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 238331
- lns fails: 116508
LNS iteration #21 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 238452
- lns fails: 116729
LNS iteration #22 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 211580
- lns fails: 103689
LNS iteration #23 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 245568
- lns fails: 120114
LNS iteration #24 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 258125
- lns fails: 126135
LNS iteration #25 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 196601
- lns fails: 96277
LNS iteration #26 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 222147
- lns fails: 108680
LNS iteration #27 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 286799
- lns fails: 141363
LNS iteration #28 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 216230
- lns fails: 106086
LNS iteration #29 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 221090
- lns fails: 108177
LNS iteration #30 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 216139
- lns fails: 105980
LNS iteration #31 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 230227
- lns fails: 112757
LNS iteration #32 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 192874
- lns fails: 94426
LNS iteration #33 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 218427
- lns fails: 107069
LNS iteration #34 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 261417
- lns fails: 129058
LNS iteration #35 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 207190
- lns fails: 101715
LNS iteration #36 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 216516
- lns fails: 106127
LNS iteration #37 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 218784
- lns fails: 107301
LNS iteration #38 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 195715
- lns fails: 95984
LNS iteration #39 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 191739
- lns fails: 93942
LNS iteration #40 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 216014
- lns fails: 105732
LNS iteration #41 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 283573
- lns fails: 139740
LNS iteration #42 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 246436
- lns fails: 120502
LNS iteration #43 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 188440
- lns fails: 92132
LNS iteration #44 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 214266
- lns fails: 105004
LNS iteration #45 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 205477
- lns fails: 100852
LNS iteration #46 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 3.004000
- lns branches: 213583
- lns fails: 104551
LNS iteration #47 ========================================
- worst case average CPI: 0.856
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 845
- lns time: 2.670000
- lns branches: 188976
- lns fails: 92492

LNS optimization is over========================================
initial worst case average CPI: 1.908
final worst case average CPI: 0.856
initial worst case efficiency: 0.627
final worst case efficiency: 0.646
number of iterations: 48
total time: 90004.000000
total branches: 6704578
total fails: 3285891

Final Solution: 0->0, 1->25, 2->2, 3->9, 4->18, 5->21, 6->33, 7->23, 8->6, 9->10, 10->1, 11->8, 12->31, 13->47, 14->15, 15->46, 16->44, 17->1, 18->8, 19->40, 20->30, 21->14, 22->45, 23->33, 24->12, 25->32, 26->40, 27->4, 28->6, 29->41, 30->42, 31->30, 32->33, 33->3, 34->23, 35->19, 36->37, 37->0, 38->35, 39->21, 40->44, 41->15, 42->43, 43->2, 44->17, 45->17, 46->46, 47->0, 48->20, 49->34, 50->12, 51->14, 52->7, 53->46, 54->24, 55->31, 56->16, 57->24, 58->25, 59->3, 60->47, 61->22, 62->7, 63->22, 64->19, 65->21, 66->20, 67->17, 68->11, 69->33, 70->4, 71->37, 72->39, 73->39, 74->28, 75->42, 76->13, 77->32, 78->20, 79->28, 80->30, 81->5, 82->5, 83->0, 84->12, 85->3, 86->3, 87->34, 88->42, 89->29, 90->30, 91->17, 92->33, 93->41, 94->7, 95->26, 96->33, 97->43, 98->38, 99->20, 100->45, 101->24, 102->13, 103->28, 104->17, 105->3, 106->37, 107->22, 108->5, 109->19, 110->5, 111->7, 112->7, 113->22, 114->42, 115->30, 116->35, 117->24, 118->38, 119->2, 120->19, 121->21, 122->37, 123->36, 124->24, 125->40, 126->35, 127->27, 128->28, 129->28, 130->19, 131->1, 132->36, 133->26, 134->31, 135->34, 136->42, 137->43, 138->39, 139->13, 140->21, 141->10, 142->35, 143->8, 144->11, 145->34, 146->9, 147->26, 148->38, 149->2, 150->8, 151->39, 152->10, 153->13, 154->31, 155->13, 156->17, 157->40, 158->10, 159->5, 160->20, 161->34, 162->25, 163->36, 164->35, 165->12, 166->37, 167->37, 168->26, 169->4, 170->26, 171->8, 172->0, 173->46, 174->29, 175->44, 176->25, 177->23, 178->9, 179->47, 180->2, 181->1, 182->41, 183->29, 184->32, 185->23, 186->6, 187->30, 188->14, 189->46, 190->43, 191->9, 192->12, 193->31, 194->11, 195->34, 196->43, 197->18, 198->31, 199->16, 200->29, 201->46, 202->22, 203->39, 204->36, 205->41, 206->2, 207->27, 208->42, 209->16, 210->14, 211->44, 212->6, 213->43, 214->8, 215->21, 216->4, 217->29, 218->45, 219->14, 220->44, 221->38, 222->27, 223->7, 224->32, 225->1, 226->27, 227->16, 228->23, 229->36, 230->19, 231->3, 232->9, 233->1, 234->26, 235->15, 236->16, 237->4, 238->28, 239->15, 240->9, 241->47, 242->18, 243->45, 244->11, 245->27, 246->14, 247->27, 248->4, 249->13, 250->20, 251->40, 252->38, 253->22, 254->15, 255->5, 256->0, 257->47, 258->40, 259->10, 260->24, 261->47, 262->18, 263->15, 264->25, 265->25, 266->45, 267->32, 268->11, 269->11, 270->29, 271->12, 272->36, 273->18, 274->23, 275->44, 276->6, 277->38, 278->32, 279->6, 280->18, 281->16, 282->41, 283->41, 284->39, 285->35, 286->10, 287->45

Average CPI map:
	2.31,	1.87,	2.11,	2.19,	1.32,	1.80
	1.96,	2.36,	2.14,	1.77,	2.05,	2.04
	2.04,	2.27,	2.03,	1.68,	2.07,	1.47
	1.81,	2.47,	2.47,	2.21,	2.13,	1.23
	1.68,	2.10,	3.07,	2.68,	2.55,	1.95
	2.08,	2.05,	2.29,	2.81,	2.15,	1.93
	1.64,	1.93,	2.26,	2.09,	1.91,	2.15
	1.37,	0.86,	1.76,	1.82,	1.77,	1.31

Efficiency map:
	0.997,	0.938,	0.936,	0.939,	0.924,	0.996
	0.832,	0.671,	0.655,	0.648,	0.659,	0.844
	0.831,	0.648,	0.646,	0.662,	0.646,	0.808
	0.815,	0.651,	0.647,	0.649,	0.651,	0.800
	0.809,	0.647,	0.650,	0.647,	0.651,	0.817
	0.814,	0.647,	0.646,	0.651,	0.651,	0.822
	0.872,	0.660,	0.649,	0.651,	0.656,	0.830
	0.818,	0.658,	0.654,	0.657,	0.674,	0.812
