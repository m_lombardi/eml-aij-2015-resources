==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 13 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 3 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 201

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- worst case average CPI: 1.966
- worst case efficiency: 0.629
- efficiency to beat: 0.629
- normalized lower bound 568
--- checking solution
--- solution ok! Maximum rounding error: 0.000075
- solution found, obj var is: 719
- de-normalized obj var: 0.643140
- time (local/global): 0.000/0.000
- branches (local/global): 22/22
- fails (local/global): 0/0
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #1 ========================================
- worst case average CPI: 1.620
- worst case efficiency: 0.630
- efficiency to beat: 0.630
- normalized lower bound 577
--- checking solution
--- solution ok! Maximum rounding error: 0.000078
- solution found, obj var is: 586
- de-normalized obj var: 0.635160
- time (local/global): 0.001/0.006
- branches (local/global): 21/43
- fails (local/global): 0/0
- lns time: 0.005000
- lns branches: 21
- lns fails: 0
LNS iteration #2 ========================================
- worst case average CPI: 1.620
- worst case efficiency: 0.631
- efficiency to beat: 0.631
- normalized lower bound 596
--- checking solution
--- solution ok! Maximum rounding error: 0.000042
- solution found, obj var is: 596
- de-normalized obj var: 0.635760
- time (local/global): 0.001/0.011
- branches (local/global): 52/95
- fails (local/global): 16/16
- lns time: 0.005000
- lns branches: 52
- lns fails: 16
LNS iteration #3 ========================================
- worst case average CPI: 1.420
- worst case efficiency: 0.635
- efficiency to beat: 0.635
- normalized lower bound 671
--- checking solution
--- solution ok! Maximum rounding error: 0.000086
- solution found, obj var is: 697
- de-normalized obj var: 0.641820
- time (local/global): 0.001/0.016
- branches (local/global): 25/120
- fails (local/global): 1/17
- lns time: 0.005000
- lns branches: 25
- lns fails: 1
LNS iteration #4 ========================================
- worst case average CPI: 1.420
- worst case efficiency: 0.636
- efficiency to beat: 0.636
- normalized lower bound 680
--- checking solution
--- solution ok! Maximum rounding error: 0.000119
- solution found, obj var is: 716
- de-normalized obj var: 0.642960
- time (local/global): 0.001/0.021
- branches (local/global): 24/144
- fails (local/global): 1/18
- lns time: 0.005000
- lns branches: 24
- lns fails: 1
LNS iteration #5 ========================================
- worst case average CPI: 1.620
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 708
--- checking solution
--- solution ok! Maximum rounding error: 0.000148
- solution found, obj var is: 727
- de-normalized obj var: 0.643620
- time (local/global): 0.001/0.026
- branches (local/global): 22/166
- fails (local/global): 0/18
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #6 ========================================
- worst case average CPI: 1.587
- worst case efficiency: 0.638
- efficiency to beat: 0.638
- normalized lower bound 722
--- checking solution
--- solution ok! Maximum rounding error: 0.000090
- solution found, obj var is: 750
- de-normalized obj var: 0.645000
- time (local/global): 0.016/0.046
- branches (local/global): 1089/1255
- fails (local/global): 501/519
- lns time: 0.020000
- lns branches: 1089
- lns fails: 501
LNS iteration #7 ========================================
- worst case average CPI: 1.169
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 726
--- checking solution
--- solution ok! Maximum rounding error: 0.000091
- solution found, obj var is: 736
- de-normalized obj var: 0.644160
- time (local/global): 0.074/0.124
- branches (local/global): 5103/6358
- fails (local/global): 2432/2951
- lns time: 0.078000
- lns branches: 5103
- lns fails: 2432
LNS iteration #8 ========================================
- worst case average CPI: 1.169
- worst case efficiency: 0.642
- efficiency to beat: 0.642
- normalized lower bound 782
--- checking solution
--- solution ok! Maximum rounding error: 0.000056
- solution found, obj var is: 782
- de-normalized obj var: 0.646920
- time (local/global): 0.030/0.158
- branches (local/global): 3070/9428
- fails (local/global): 1484/4435
- lns time: 0.034000
- lns branches: 3070
- lns fails: 1484
LNS iteration #9 ========================================
- worst case average CPI: 1.169
- worst case efficiency: 0.643
- efficiency to beat: 0.643
- normalized lower bound 801
--- checking solution
--- solution ok! Maximum rounding error: 0.000053
- solution found, obj var is: 807
- de-normalized obj var: 0.648420
- time (local/global): 0.029/0.191
- branches (local/global): 1951/11379
- fails (local/global): 916/5351
- lns time: 0.033000
- lns branches: 1951
- lns fails: 916
LNS iteration #10 ========================================
- worst case average CPI: 1.169
- worst case efficiency: 0.644
- efficiency to beat: 0.644
- normalized lower bound 819
--- checking solution
--- solution ok! Maximum rounding error: 0.000050
- solution found, obj var is: 820
- de-normalized obj var: 0.649200
- time (local/global): 0.038/0.233
- branches (local/global): 2862/14241
- fails (local/global): 1361/6712
- lns time: 0.042000
- lns branches: 2862
- lns fails: 1361
LNS iteration #11 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.644
- efficiency to beat: 0.644
- normalized lower bound 821
--- checking solution
--- solution ok! Maximum rounding error: 0.000061
- solution found, obj var is: 831
- de-normalized obj var: 0.649860
- time (local/global): 2.556/2.793
- branches (local/global): 220823/235064
- fails (local/global): 108139/114851
- lns time: 2.560000
- lns branches: 220823
- lns fails: 108139
LNS iteration #12 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 829
--- checking solution
--- solution ok! Maximum rounding error: 0.000047
- solution found, obj var is: 959
- de-normalized obj var: 0.657540
- time (local/global): 0.001/2.798
- branches (local/global): 22/235086
- fails (local/global): 0/114851
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #13 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 836
--- checking solution
--- solution ok! Maximum rounding error: 0.000000
- solution found, obj var is: 865
- de-normalized obj var: 0.651900
- time (local/global): 0.020/2.822
- branches (local/global): 2498/237584
- fails (local/global): 1200/116051
- lns time: 0.024000
- lns branches: 2498
- lns fails: 1200
LNS iteration #14 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 248027
- lns fails: 121368
LNS iteration #15 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 211379
- lns fails: 103471
LNS iteration #16 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 264764
- lns fails: 129401
LNS iteration #17 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 265937
- lns fails: 129981
LNS iteration #18 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 230878
- lns fails: 112912
LNS iteration #19 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 215463
- lns fails: 105540
LNS iteration #20 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 208517
- lns fails: 102240
LNS iteration #21 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 187029
- lns fails: 91440
LNS iteration #22 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
- lns time: 3.004000
- lns branches: 216059
- lns fails: 105996
LNS iteration #23 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 838
--- checking solution
--- solution ok! Maximum rounding error: 0.000037
- solution found, obj var is: 841
- de-normalized obj var: 0.650460
- time (local/global): 1.176/31.038
- branches (local/global): 114482/2400119
- fails (local/global): 55737/1174137
- lns time: 1.180000
- lns branches: 114482
- lns fails: 55737
LNS iteration #24 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.646
- efficiency to beat: 0.646
- normalized lower bound 844
--- checking solution
--- solution ok! Maximum rounding error: 0.000076
- solution found, obj var is: 855
- de-normalized obj var: 0.651300
- time (local/global): 0.002/31.044
- branches (local/global): 130/2400249
- fails (local/global): 55/1174192
- lns time: 0.006000
- lns branches: 130
- lns fails: 55
LNS iteration #25 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.647
- efficiency to beat: 0.647
- normalized lower bound 860
--- checking solution
--- solution ok! Maximum rounding error: 0.000089
- solution found, obj var is: 860
- de-normalized obj var: 0.651600
- time (local/global): 0.019/31.067
- branches (local/global): 1351/2401600
- fails (local/global): 628/1174820
- lns time: 0.023000
- lns branches: 1351
- lns fails: 628
LNS iteration #26 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.647
- efficiency to beat: 0.647
- normalized lower bound 867
--- checking solution
--- solution ok! Maximum rounding error: 0.000057
- solution found, obj var is: 883
- de-normalized obj var: 0.652980
- time (local/global): 0.024/31.095
- branches (local/global): 1880/2403480
- fails (local/global): 881/1175701
- lns time: 0.028000
- lns branches: 1880
- lns fails: 881
LNS iteration #27 ========================================
- worst case average CPI: 0.765
- worst case efficiency: 0.647
- efficiency to beat: 0.647
- normalized lower bound 872
- lns time: 3.004000
- lns branches: 224778
- lns fails: 110283
LNS iteration #28 ========================================
- worst case average CPI: 0.765
- worst case efficiency: 0.647
- efficiency to beat: 0.647
- normalized lower bound 872
--- checking solution
--- solution ok! Maximum rounding error: 0.000068
- solution found, obj var is: 873
- de-normalized obj var: 0.652380
- time (local/global): 0.010/34.113
- branches (local/global): 885/2629143
- fails (local/global): 407/1286391
- lns time: 0.014000
- lns branches: 885
- lns fails: 407
LNS iteration #29 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.648
- efficiency to beat: 0.648
- normalized lower bound 892
--- checking solution
--- solution ok! Maximum rounding error: 0.000055
- solution found, obj var is: 937
- de-normalized obj var: 0.656220
- time (local/global): 0.003/34.120
- branches (local/global): 243/2629386
- fails (local/global): 103/1286494
- lns time: 0.007000
- lns branches: 243
- lns fails: 103
LNS iteration #30 ========================================
- worst case average CPI: 0.871
- worst case efficiency: 0.649
- efficiency to beat: 0.649
- normalized lower bound 901
--- checking solution
--- solution ok! Maximum rounding error: 0.000044
- solution found, obj var is: 904
- de-normalized obj var: 0.654240
- time (local/global): 0.017/34.141
- branches (local/global): 1303/2630689
- fails (local/global): 605/1287099
- lns time: 0.021000
- lns branches: 1303
- lns fails: 605
LNS iteration #31 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.649
- efficiency to beat: 0.649
- normalized lower bound 907
- lns time: 3.004000
- lns branches: 199842
- lns fails: 98011
LNS iteration #32 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.649
- efficiency to beat: 0.649
- normalized lower bound 907
--- checking solution
--- solution ok! Maximum rounding error: 0.000025
- solution found, obj var is: 919
- de-normalized obj var: 0.655140
- time (local/global): 0.011/37.160
- branches (local/global): 1115/2831646
- fails (local/global): 511/1385621
- lns time: 0.015000
- lns branches: 1115
- lns fails: 511
LNS iteration #33 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.650
- efficiency to beat: 0.650
- normalized lower bound 910
--- checking solution
--- solution ok! Maximum rounding error: 0.000072
- solution found, obj var is: 913
- de-normalized obj var: 0.654780
- time (local/global): 0.001/37.165
- branches (local/global): 21/2831667
- fails (local/global): 1/1385622
- lns time: 0.005000
- lns branches: 21
- lns fails: 1
LNS iteration #34 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.650
- efficiency to beat: 0.650
- normalized lower bound 917
- lns time: 3.004000
- lns branches: 335887
- lns fails: 164137
LNS iteration #35 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.650
- efficiency to beat: 0.650
- normalized lower bound 917
- lns time: 3.004000
- lns branches: 284482
- lns fails: 139053
LNS iteration #36 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.650
- efficiency to beat: 0.650
- normalized lower bound 917
- lns time: 3.004000
- lns branches: 265310
- lns fails: 129701
LNS iteration #37 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.650
- efficiency to beat: 0.650
- normalized lower bound 917
--- checking solution
--- solution ok! Maximum rounding error: 0.000117
- solution found, obj var is: 928
- de-normalized obj var: 0.655680
- time (local/global): 0.536/46.717
- branches (local/global): 52988/3770334
- fails (local/global): 25700/1844213
- lns time: 0.540000
- lns branches: 52988
- lns fails: 25700
LNS iteration #38 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.650
- efficiency to beat: 0.650
- normalized lower bound 920
- lns time: 3.004000
- lns branches: 205062
- lns fails: 100545
LNS iteration #39 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.650
- efficiency to beat: 0.650
- normalized lower bound 920
--- checking solution
--- solution ok! Maximum rounding error: 0.000110
- solution found, obj var is: 922
- de-normalized obj var: 0.655320
- time (local/global): 0.004/49.729
- branches (local/global): 274/3975670
- fails (local/global): 122/1944880
- lns time: 0.008000
- lns branches: 274
- lns fails: 122
LNS iteration #40 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.651
- efficiency to beat: 0.651
- normalized lower bound 928
- lns time: 3.004000
- lns branches: 227723
- lns fails: 111440
LNS iteration #41 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.651
- efficiency to beat: 0.651
- normalized lower bound 928
- lns time: 3.004000
- lns branches: 205670
- lns fails: 100830
LNS iteration #42 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.651
- efficiency to beat: 0.651
- normalized lower bound 928
--- checking solution
--- solution ok! Maximum rounding error: 0.000211
- solution found, obj var is: 950
- de-normalized obj var: 0.657000
- time (local/global): 0.003/55.744
- branches (local/global): 238/4409301
- fails (local/global): 102/2157252
- lns time: 0.007000
- lns branches: 238
- lns fails: 102
LNS iteration #43 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.651
- efficiency to beat: 0.651
- normalized lower bound 930
--- checking solution
--- solution ok! Maximum rounding error: 0.000027
- solution found, obj var is: 1031
- de-normalized obj var: 0.661860
- time (local/global): 0.001/55.749
- branches (local/global): 23/4409324
- fails (local/global): 0/2157252
- lns time: 0.005000
- lns branches: 23
- lns fails: 0
LNS iteration #44 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.652
- efficiency to beat: 0.652
- normalized lower bound 947
- lns time: 3.004000
- lns branches: 228420
- lns fails: 112827
LNS iteration #45 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.652
- efficiency to beat: 0.652
- normalized lower bound 947
--- checking solution
--- solution ok! Maximum rounding error: 0.000026
- solution found, obj var is: 951
- de-normalized obj var: 0.657060
- time (local/global): 0.052/58.809
- branches (local/global): 4477/4642221
- fails (local/global): 2125/2272204
- lns time: 0.056000
- lns branches: 4477
- lns fails: 2125
LNS iteration #46 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.652
- efficiency to beat: 0.652
- normalized lower bound 949
--- checking solution
--- solution ok! Maximum rounding error: 0.000048
- solution found, obj var is: 949
- de-normalized obj var: 0.656940
- time (local/global): 0.172/58.985
- branches (local/global): 13951/4656172
- fails (local/global): 6703/2278907
- lns time: 0.176000
- lns branches: 13951
- lns fails: 6703
LNS iteration #47 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.653
- efficiency to beat: 0.653
- normalized lower bound 960
--- checking solution
--- solution ok! Maximum rounding error: 0.000004
- solution found, obj var is: 962
- de-normalized obj var: 0.657720
- time (local/global): 0.001/58.990
- branches (local/global): 31/4656203
- fails (local/global): 5/2278912
- lns time: 0.005000
- lns branches: 31
- lns fails: 5
LNS iteration #48 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.653
- efficiency to beat: 0.653
- normalized lower bound 975
- lns time: 3.004000
- lns branches: 215491
- lns fails: 105562
LNS iteration #49 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.653
- efficiency to beat: 0.653
- normalized lower bound 975
- lns time: 3.004000
- lns branches: 324739
- lns fails: 158704
LNS iteration #50 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.653
- efficiency to beat: 0.653
- normalized lower bound 975
- lns time: 3.004000
- lns branches: 270317
- lns fails: 132274
LNS iteration #51 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.653
- efficiency to beat: 0.653
- normalized lower bound 975
--- checking solution
--- solution ok! Maximum rounding error: 0.000065
- solution found, obj var is: 982
- de-normalized obj var: 0.658920
- time (local/global): 1.084/69.090
- branches (local/global): 113673/5580423
- fails (local/global): 55363/2730815
- lns time: 1.088000
- lns branches: 113673
- lns fails: 55363
LNS iteration #52 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 993
- lns time: 3.004000
- lns branches: 237716
- lns fails: 116449
LNS iteration #53 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 993
--- checking solution
--- solution ok! Maximum rounding error: 0.000052
- solution found, obj var is: 996
- de-normalized obj var: 0.659760
- time (local/global): 0.258/72.356
- branches (local/global): 20534/5838673
- fails (local/global): 9903/2857167
- lns time: 0.262000
- lns branches: 20534
- lns fails: 9903
LNS iteration #54 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 1002
- lns time: 3.004000
- lns branches: 291766
- lns fails: 142612
LNS iteration #55 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 1002
- lns time: 3.004000
- lns branches: 229272
- lns fails: 112201
LNS iteration #56 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 1002
- lns time: 3.004000
- lns branches: 275953
- lns fails: 134872
LNS iteration #57 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 1002
- lns time: 3.004000
- lns branches: 294038
- lns fails: 143842
LNS iteration #58 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 1002
--- checking solution
--- solution ok! Maximum rounding error: 0.000044
- solution found, obj var is: 1005
- de-normalized obj var: 0.660300
- time (local/global): 1.629/86.005
- branches (local/global): 163695/7093397
- fails (local/global): 79686/3470380
- lns time: 1.633000
- lns branches: 163695
- lns fails: 79686
LNS iteration #59 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.655
- efficiency to beat: 0.655
- normalized lower bound 1006
--- checking solution
--- solution ok! Maximum rounding error: 0.000028
- solution found, obj var is: 1019
- de-normalized obj var: 0.661140
- time (local/global): 0.007/86.016
- branches (local/global): 625/7094022
- fails (local/global): 285/3470665
- lns time: 0.011000
- lns branches: 625
- lns fails: 285
LNS iteration #60 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.656
- efficiency to beat: 0.656
- normalized lower bound 1010
--- checking solution
--- solution ok! Maximum rounding error: 0.000063
- solution found, obj var is: 1089
- de-normalized obj var: 0.665340
- time (local/global): 0.013/86.033
- branches (local/global): 1303/7095325
- fails (local/global): 600/3471265
- lns time: 0.017000
- lns branches: 1303
- lns fails: 600
LNS iteration #61 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.656
- efficiency to beat: 0.656
- normalized lower bound 1013
- lns time: 3.004000
- lns branches: 287319
- lns fails: 140541
LNS iteration #62 ========================================
- worst case average CPI: 0.658
- worst case efficiency: 0.656
- efficiency to beat: 0.656
- normalized lower bound 1013
- lns time: 0.963000
- lns branches: 87410
- lns fails: 42673

LNS optimization is over========================================
initial worst case average CPI: 1.966
final worst case average CPI: 0.658
initial worst case efficiency: 0.629
final worst case efficiency: 0.656
number of iterations: 63
total time: 90004.000000
total branches: 7470054
total fails: 3654479

Final Solution: 0->34, 1->23, 2->38, 3->5, 4->45, 5->20, 6->1, 7->18, 8->17, 9->21, 10->2, 11->25, 12->4, 13->34, 14->6, 15->33, 16->37, 17->22, 18->31, 19->0, 20->29, 21->12, 22->42, 23->25, 24->38, 25->3, 26->25, 27->32, 28->37, 29->21, 30->15, 31->34, 32->0, 33->31, 34->21, 35->4, 36->7, 37->22, 38->38, 39->0, 40->7, 41->3, 42->11, 43->3, 44->34, 45->29, 46->15, 47->38, 48->24, 49->42, 50->20, 51->27, 52->38, 53->29, 54->22, 55->23, 56->19, 57->2, 58->34, 59->24, 60->30, 61->17, 62->6, 63->26, 64->7, 65->46, 66->30, 67->43, 68->2, 69->32, 70->11, 71->1, 72->26, 73->11, 74->11, 75->44, 76->40, 77->33, 78->23, 79->39, 80->45, 81->46, 82->14, 83->42, 84->29, 85->36, 86->35, 87->33, 88->13, 89->32, 90->30, 91->8, 92->35, 93->28, 94->24, 95->17, 96->29, 97->19, 98->32, 99->14, 100->14, 101->46, 102->37, 103->8, 104->37, 105->35, 106->33, 107->29, 108->1, 109->10, 110->0, 111->33, 112->19, 113->14, 114->10, 115->18, 116->44, 117->25, 118->37, 119->47, 120->31, 121->6, 122->24, 123->15, 124->8, 125->35, 126->3, 127->4, 128->19, 129->8, 130->15, 131->21, 132->32, 133->38, 134->0, 135->34, 136->32, 137->33, 138->10, 139->42, 140->45, 141->47, 142->35, 143->4, 144->41, 145->26, 146->27, 147->46, 148->2, 149->43, 150->30, 151->30, 152->24, 153->9, 154->22, 155->47, 156->37, 157->26, 158->39, 159->42, 160->43, 161->47, 162->9, 163->35, 164->39, 165->13, 166->20, 167->11, 168->47, 169->20, 170->22, 171->9, 172->26, 173->46, 174->10, 175->8, 176->45, 177->18, 178->2, 179->12, 180->13, 181->28, 182->16, 183->9, 184->42, 185->6, 186->5, 187->5, 188->45, 189->1, 190->10, 191->19, 192->21, 193->36, 194->44, 195->31, 196->47, 197->40, 198->28, 199->8, 200->12, 201->43, 202->3, 203->2, 204->39, 205->31, 206->1, 207->13, 208->11, 209->12, 210->13, 211->14, 212->41, 213->46, 214->23, 215->0, 216->13, 217->15, 218->7, 219->3, 220->1, 221->7, 222->20, 223->7, 224->31, 225->12, 226->39, 227->16, 228->22, 229->16, 230->6, 231->18, 232->44, 233->4, 234->41, 235->26, 236->12, 237->9, 238->6, 239->44, 240->25, 241->15, 242->41, 243->27, 244->27, 245->30, 246->16, 247->41, 248->45, 249->18, 250->14, 251->4, 252->20, 253->9, 254->16, 255->43, 256->40, 257->5, 258->25, 259->17, 260->5, 261->17, 262->40, 263->44, 264->17, 265->27, 266->43, 267->41, 268->28, 269->10, 270->19, 271->18, 272->23, 273->36, 274->28, 275->40, 276->16, 277->28, 278->40, 279->5, 280->36, 281->36, 282->27, 283->21, 284->24, 285->39, 286->23, 287->36

Average CPI map:
	3.47,	1.70,	2.31,	1.25,	1.76,	1.20
	1.22,	2.10,	2.14,	2.18,	1.99,	1.80
	1.11,	3.52,	2.59,	1.32,	2.96,	1.95
	2.06,	2.79,	2.84,	2.44,	2.25,	1.79
	1.13,	2.38,	3.40,	3.23,	2.95,	1.99
	1.88,	2.77,	4.63,	2.94,	3.18,	1.53
	1.60,	2.21,	2.74,	2.31,	1.93,	0.66
	1.27,	1.45,	1.88,	1.93,	1.56,	1.68

Efficiency map:
	0.997,	0.937,	0.946,	0.887,	0.930,	0.996
	0.799,	0.663,	0.656,	0.660,	0.656,	0.835
	0.805,	0.660,	0.657,	0.662,	0.660,	0.823
	0.823,	0.658,	0.662,	0.657,	0.659,	0.817
	0.787,	0.661,	0.659,	0.656,	0.663,	0.823
	0.804,	0.657,	0.657,	0.657,	0.690,	0.806
	0.872,	0.664,	0.663,	0.665,	0.656,	0.724
	0.815,	0.661,	0.662,	0.665,	0.668,	0.829
