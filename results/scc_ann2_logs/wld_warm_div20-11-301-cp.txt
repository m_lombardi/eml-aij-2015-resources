==============================================================================
LNS optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --prec 10000 --step 0 --global-tlim 90 --global-iter 1000 --local-tlim 3 --local-nsol 1 --restart-base 100 --restart-strat luby --seed 301

Starting the Optimization Process
reserved_tasks None
Finding a first solution
LNS iteration #0 ========================================
- worst case average CPI: 1.908
- worst case efficiency: 0.627
- efficiency to beat: 0.627
- normalized lower bound 538
--- checking solution
--- solution ok! Maximum rounding error: 0.000028
- solution found, obj var is: 574
- de-normalized obj var: 0.634440
- time (local/global): 0.000/0.000
- branches (local/global): 26/26
- fails (local/global): 2/2
- lns time: 0.005000
- lns branches: 26
- lns fails: 2
LNS iteration #1 ========================================
- worst case average CPI: 1.687
- worst case efficiency: 0.628
- efficiency to beat: 0.628
- normalized lower bound 546
--- checking solution
--- solution ok! Maximum rounding error: 0.000035
- solution found, obj var is: 551
- de-normalized obj var: 0.633060
- time (local/global): 0.008/0.013
- branches (local/global): 448/474
- fails (local/global): 200/202
- lns time: 0.012000
- lns branches: 448
- lns fails: 200
LNS iteration #2 ========================================
- worst case average CPI: 1.553
- worst case efficiency: 0.633
- efficiency to beat: 0.633
- normalized lower bound 635
--- checking solution
--- solution ok! Maximum rounding error: 0.000025
- solution found, obj var is: 647
- de-normalized obj var: 0.638820
- time (local/global): 0.004/0.021
- branches (local/global): 295/769
- fails (local/global): 129/331
- lns time: 0.008000
- lns branches: 295
- lns fails: 129
LNS iteration #3 ========================================
- worst case average CPI: 1.601
- worst case efficiency: 0.633
- efficiency to beat: 0.633
- normalized lower bound 640
--- checking solution
--- solution ok! Maximum rounding error: 0.000041
- solution found, obj var is: 652
- de-normalized obj var: 0.639120
- time (local/global): 0.026/0.051
- branches (local/global): 2776/3545
- fails (local/global): 1307/1638
- lns time: 0.030000
- lns branches: 2776
- lns fails: 1307
LNS iteration #4 ========================================
- worst case average CPI: 1.601
- worst case efficiency: 0.635
- efficiency to beat: 0.635
- normalized lower bound 659
--- checking solution
--- solution ok! Maximum rounding error: 0.000043
- solution found, obj var is: 663
- de-normalized obj var: 0.639780
- time (local/global): 0.003/0.058
- branches (local/global): 237/3782
- fails (local/global): 100/1738
- lns time: 0.007000
- lns branches: 237
- lns fails: 100
LNS iteration #5 ========================================
- worst case average CPI: 1.553
- worst case efficiency: 0.636
- efficiency to beat: 0.636
- normalized lower bound 678
--- checking solution
--- solution ok! Maximum rounding error: 0.000030
- solution found, obj var is: 679
- de-normalized obj var: 0.640740
- time (local/global): 0.001/0.063
- branches (local/global): 31/3813
- fails (local/global): 5/1743
- lns time: 0.005000
- lns branches: 31
- lns fails: 5
LNS iteration #6 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.637
- efficiency to beat: 0.637
- normalized lower bound 704
--- checking solution
--- solution ok! Maximum rounding error: 0.000050
- solution found, obj var is: 705
- de-normalized obj var: 0.642300
- time (local/global): 0.027/0.094
- branches (local/global): 2081/5894
- fails (local/global): 981/2724
- lns time: 0.032000
- lns branches: 2081
- lns fails: 981
LNS iteration #7 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.638
- efficiency to beat: 0.638
- normalized lower bound 718
- lns time: 3.004000
- lns branches: 261399
- lns fails: 129088
LNS iteration #8 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.638
- efficiency to beat: 0.638
- normalized lower bound 718
--- checking solution
--- solution ok! Maximum rounding error: 0.000133
- solution found, obj var is: 753
- de-normalized obj var: 0.645180
- time (local/global): 0.018/3.121
- branches (local/global): 1460/268753
- fails (local/global): 676/132488
- lns time: 0.022000
- lns branches: 1460
- lns fails: 676
LNS iteration #9 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 728
--- checking solution
--- solution ok! Maximum rounding error: 0.000006
- solution found, obj var is: 767
- de-normalized obj var: 0.646020
- time (local/global): 0.001/3.126
- branches (local/global): 20/268773
- fails (local/global): 0/132488
- lns time: 0.005000
- lns branches: 20
- lns fails: 0
LNS iteration #10 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 737
--- checking solution
--- solution ok! Maximum rounding error: 0.000024
- solution found, obj var is: 831
- de-normalized obj var: 0.649860
- time (local/global): 0.001/3.131
- branches (local/global): 22/268795
- fails (local/global): 0/132488
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #11 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.639
- efficiency to beat: 0.639
- normalized lower bound 739
--- checking solution
--- solution ok! Maximum rounding error: 0.000025
- solution found, obj var is: 756
- de-normalized obj var: 0.645360
- time (local/global): 0.001/3.136
- branches (local/global): 19/268814
- fails (local/global): 0/132488
- lns time: 0.005000
- lns branches: 19
- lns fails: 0
LNS iteration #12 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 745
- lns time: 3.004000
- lns branches: 233541
- lns fails: 114347
LNS iteration #13 ========================================
- worst case average CPI: 1.385
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 745
--- checking solution
--- solution ok! Maximum rounding error: 0.000108
- solution found, obj var is: 1123
- de-normalized obj var: 0.667380
- time (local/global): 0.001/6.145
- branches (local/global): 22/502377
- fails (local/global): 0/246835
- lns time: 0.005000
- lns branches: 22
- lns fails: 0
LNS iteration #14 ========================================
- worst case average CPI: 1.318
- worst case efficiency: 0.640
- efficiency to beat: 0.640
- normalized lower bound 756
--- checking solution
--- solution ok! Maximum rounding error: 0.000174
- solution found, obj var is: 778
- de-normalized obj var: 0.646680
- time (local/global): 0.003/6.152
- branches (local/global): 259/502636
- fails (local/global): 112/246947
- lns time: 0.007000
- lns branches: 259
- lns fails: 112
LNS iteration #15 ========================================
- worst case average CPI: 1.318
- worst case efficiency: 0.641
- efficiency to beat: 0.641
- normalized lower bound 765
--- checking solution
--- solution ok! Maximum rounding error: 0.000043
- solution found, obj var is: 771
- de-normalized obj var: 0.646260
- time (local/global): 0.012/6.168
- branches (local/global): 1091/503727
- fails (local/global): 501/247448
- lns time: 0.016000
- lns branches: 1091
- lns fails: 501
LNS iteration #16 ========================================
- worst case average CPI: 1.318
- worst case efficiency: 0.642
- efficiency to beat: 0.642
- normalized lower bound 778
- lns time: 3.004000
- lns branches: 218019
- lns fails: 106849
LNS iteration #17 ========================================
- worst case average CPI: 1.318
- worst case efficiency: 0.642
- efficiency to beat: 0.642
- normalized lower bound 778
- lns time: 3.004000
- lns branches: 196234
- lns fails: 96102
LNS iteration #18 ========================================
- worst case average CPI: 1.318
- worst case efficiency: 0.642
- efficiency to beat: 0.642
- normalized lower bound 778
--- checking solution
--- solution ok! Maximum rounding error: 0.000010
- solution found, obj var is: 779
- de-normalized obj var: 0.646740
- time (local/global): 0.156/12.336
- branches (local/global): 10906/928886
- fails (local/global): 5225/455624
- lns time: 0.160000
- lns branches: 10906
- lns fails: 5225
LNS iteration #19 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.643
- efficiency to beat: 0.643
- normalized lower bound 794
--- checking solution
--- solution ok! Maximum rounding error: 0.000180
- solution found, obj var is: 804
- de-normalized obj var: 0.648240
- time (local/global): 0.140/12.480
- branches (local/global): 10579/939465
- fails (local/global): 5068/460692
- lns time: 0.144000
- lns branches: 10579
- lns fails: 5068
LNS iteration #20 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 170678
- lns fails: 83351
LNS iteration #21 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 243488
- lns fails: 119135
LNS iteration #22 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 244070
- lns fails: 119410
LNS iteration #23 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 295356
- lns fails: 145641
LNS iteration #24 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 255645
- lns fails: 125178
LNS iteration #25 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 197413
- lns fails: 96714
LNS iteration #26 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 206691
- lns fails: 101236
LNS iteration #27 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 224885
- lns fails: 110052
LNS iteration #28 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 206654
- lns fails: 101342
LNS iteration #29 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 186821
- lns fails: 91542
LNS iteration #30 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 230019
- lns fails: 112572
LNS iteration #31 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 241264
- lns fails: 118137
LNS iteration #32 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 228418
- lns fails: 111722
LNS iteration #33 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 224017
- lns fails: 109768
LNS iteration #34 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 181440
- lns fails: 88679
LNS iteration #35 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 213311
- lns fails: 104504
LNS iteration #36 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 209424
- lns fails: 103497
LNS iteration #37 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 219623
- lns fails: 107480
LNS iteration #38 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 274408
- lns fails: 135407
LNS iteration #39 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 286474
- lns fails: 141427
LNS iteration #40 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 217627
- lns fails: 106626
LNS iteration #41 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 243957
- lns fails: 119262
LNS iteration #42 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 222798
- lns fails: 109344
LNS iteration #43 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 208908
- lns fails: 102417
LNS iteration #44 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 3.004000
- lns branches: 199851
- lns fails: 97832
LNS iteration #45 ========================================
- worst case average CPI: 1.039
- worst case efficiency: 0.645
- efficiency to beat: 0.645
- normalized lower bound 837
- lns time: 2.420000
- lns branches: 161864
- lns fails: 79050

LNS optimization is over========================================
initial worst case average CPI: 1.908
final worst case average CPI: 1.039
initial worst case efficiency: 0.627
final worst case efficiency: 0.645
number of iterations: 46
total time: 90004.000000
total branches: 6734569
total fails: 3302017

Final Solution: 0->0, 1->26, 2->3, 3->7, 4->12, 5->36, 6->3, 7->1, 8->17, 9->20, 10->47, 11->9, 12->25, 13->22, 14->24, 15->29, 16->38, 17->38, 18->12, 19->40, 20->38, 21->28, 22->46, 23->14, 24->39, 25->26, 26->40, 27->4, 28->32, 29->41, 30->9, 31->26, 32->33, 33->22, 34->20, 35->45, 36->15, 37->0, 38->36, 39->39, 40->31, 41->13, 42->46, 43->16, 44->2, 45->2, 46->24, 47->0, 48->35, 49->42, 50->35, 51->14, 52->21, 53->46, 54->4, 55->31, 56->21, 57->41, 58->25, 59->10, 60->1, 61->22, 62->32, 63->47, 64->22, 65->43, 66->4, 67->2, 68->25, 69->11, 70->38, 71->47, 72->22, 73->13, 74->29, 75->4, 76->10, 77->32, 78->22, 79->20, 80->38, 81->32, 82->43, 83->0, 84->39, 85->17, 86->5, 87->4, 88->31, 89->28, 90->4, 91->2, 92->35, 93->27, 94->35, 95->7, 96->9, 97->43, 98->37, 99->39, 100->18, 101->47, 102->21, 103->20, 104->30, 105->5, 106->15, 107->10, 108->6, 109->8, 110->43, 111->20, 112->32, 113->13, 114->37, 115->30, 116->11, 117->23, 118->42, 119->7, 120->33, 121->27, 122->46, 123->35, 124->8, 125->40, 126->27, 127->15, 128->34, 129->26, 130->46, 131->10, 132->7, 133->23, 134->31, 135->45, 136->9, 137->39, 138->33, 139->14, 140->6, 141->11, 142->21, 143->11, 144->46, 145->27, 146->42, 147->27, 148->37, 149->19, 150->8, 151->33, 152->34, 153->21, 154->9, 155->5, 156->32, 157->40, 158->30, 159->3, 160->7, 161->42, 162->19, 163->7, 164->27, 165->3, 166->37, 167->37, 168->2, 169->15, 170->24, 171->25, 172->0, 173->30, 174->26, 175->45, 176->24, 177->33, 178->44, 179->25, 180->23, 181->34, 182->18, 183->28, 184->34, 185->23, 186->6, 187->16, 188->14, 189->45, 190->43, 191->9, 192->12, 193->38, 194->1, 195->23, 196->13, 197->35, 198->42, 199->36, 200->29, 201->18, 202->3, 203->44, 204->18, 205->19, 206->12, 207->16, 208->44, 209->13, 210->29, 211->44, 212->6, 213->6, 214->8, 215->1, 216->15, 217->28, 218->23, 219->28, 220->19, 221->42, 222->19, 223->41, 224->34, 225->28, 226->33, 227->10, 228->14, 229->1, 230->30, 231->5, 232->31, 233->34, 234->1, 235->29, 236->45, 237->24, 238->20, 239->5, 240->31, 241->47, 242->19, 243->44, 244->12, 245->2, 246->26, 247->44, 248->3, 249->45, 250->15, 251->40, 252->36, 253->13, 254->10, 255->6, 256->0, 257->12, 258->40, 259->25, 260->8, 261->16, 262->18, 263->17, 264->47, 265->24, 266->36, 267->16, 268->39, 269->41, 270->29, 271->21, 272->11, 273->16, 274->41, 275->8, 276->36, 277->37, 278->14, 279->11, 280->17, 281->43, 282->30, 283->41, 284->5, 285->18, 286->17, 287->17

Average CPI map:
	2.31,	2.43,	2.25,	1.79,	2.18,	2.60
	2.14,	2.10,	2.08,	2.06,	1.82,	1.45
	1.62,	2.03,	2.15,	1.99,	2.13,	1.42
	1.61,	2.28,	2.44,	2.10,	2.11,	1.94
	1.57,	2.13,	3.04,	2.57,	2.43,	1.48
	2.30,	1.94,	1.94,	2.67,	2.11,	1.82
	1.67,	1.93,	2.19,	2.07,	1.91,	1.32
	1.92,	1.91,	1.65,	1.84,	1.51,	1.04

Efficiency map:
	0.997,	0.941,	0.943,	0.920,	0.935,	0.996
	0.840,	0.662,	0.652,	0.656,	0.650,	0.821
	0.817,	0.647,	0.649,	0.662,	0.647,	0.806
	0.808,	0.647,	0.646,	0.646,	0.649,	0.820
	0.804,	0.648,	0.649,	0.645,	0.648,	0.769
	0.826,	0.646,	0.645,	0.647,	0.649,	0.816
	0.872,	0.660,	0.648,	0.649,	0.655,	0.774
	0.840,	0.677,	0.647,	0.658,	0.667,	0.798
