LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
ANNs For the Platform:
3,2,1,0.413586,-0.0266769,-0.0683771,-5.1002,70.5254,-0.257939,-72.3361,-14.3957,-55.9961,0.000416225,-55.1984
3,2,1,7.29748,1.94844,3.18138,9.63196,-4.67735,-4.13678,1.22405,-8.03403,0.0803394,-0.125838,0.598848
3,2,1,6.43996,0.383984,0.598454,7.79024,-4.25175,0.123161,-0.278344,-5.71253,1.47885,1.00258,0.327892
3,2,1,3.45395,0.140329,0.51254,4.25255,4.08469,0.00326531,0.722664,3.88666,1.2829,-0.290289,-0.187123
3,2,1,1.76581,-0.7465,16.0398,1.01103,2.42771,0.96084,1.00318,4.14242,-0.00284347,0.244904,0.56299
3,2,1,1.87058,0.359859,-0.0851999,0.818043,3.18969,0.847611,-7.29654,-2.24823,0.00245596,0.000486788,0.794563
3,2,1,3.28113,0.579268,0.142679,3.72653,-0.240895,-0.149383,25.0414,7.13641,0.488705,-0.00083541,0.320468
3,2,1,-4.78997,-2.53518,1.7971,11.1628,-3.08773,-0.470276,-0.0894822,-2.63871,1.46314,-0.463195,-1.12076
3,2,1,-0.392198,-0.00544948,0.0100489,-0.634573,-1.21109,-0.0856362,-0.013156,-1.60668,1.54201,-2.33195,-0.462253
3,2,1,3.15031,0.401017,0.160161,2.51156,5.71314,0.59291,-11.6364,2.59774,0.430465,0.00737346,0.36306
3,2,1,2.64111,0.312028,0.0803827,2.35273,-4.25958,3.48188,-1.59773,-1.57632,0.565527,0.0271972,0.266241
3,2,1,2.2658,0.0425076,0.0191991,-0.583848,2.82415,0.533705,0.0754136,3.33659,-0.0169351,0.54785,0.257008
3,2,1,6.01014,0.654767,0.24854,5.54419,5.68574,0.00137666,0.00103835,-1.46229,0.258518,0.00962319,0.54621
3,2,1,-0.828282,0.684875,0.223674,-1.44779,5.0064,0.272371,0.00344824,2.89438,0.397142,0.379767,0.797684
3,2,1,-0.166745,-1.25514,-0.967701,-3.82155,3.34568,0.34907,0.176897,2.37943,0.567319,0.42026,0.93611
3,2,1,-10.035,3.55243,8.66202,-21.5626,10.92,0.228744,0.12852,5.40188,0.871264,0.345634,1.3209
3,2,1,4.50278,0.709934,0.227356,3.41616,1.455,0.115548,0.0125181,-1.15221,0.345467,0.222127,0.602264
3,2,1,3.51765,0.552987,0.202187,3.41883,0.53704,0.0709757,-0.0346297,-0.38017,0.320095,-0.0397752,0.474568
3,2,1,3.62936,0.506661,0.201819,3.49179,3.4019,-3.09955,1.3332,0.961184,0.32649,-0.0126443,0.488413
3,2,1,3.50925,0.448618,0.127508,2.4823,1.78455,1.6893,1.46456,-0.207671,0.400287,0.00684206,0.402337
3,2,1,-0.52883,0.00691432,0.00617163,-0.194006,1.73284,0.0488672,0.0179273,1.25978,0.716787,0.92667,0.162242
3,2,1,2.48239,0.190378,0.120167,1.91689,1.21532,1.48543,0.644574,2.68198,0.547395,-0.0744913,0.328097
3,2,1,2.55861,2.02297,0.650126,2.19109,2.44652,0.0836318,0.0133745,2.4369,0.0418722,0.798841,-0.0334834
3,2,1,5.07371,0.556895,0.217981,4.66725,-0.00829845,-0.00544544,0.00934569,-0.672651,0.280461,-0.520981,0.217266
3,2,1,1.56588,0.201966,0.0665967,1.82578,1.97654,0.236717,0.0903453,1.08195,1.02748,-0.229836,0.011785
3,2,1,2.41728,0.208205,-0.0012583,2.05484,1.41976,-1.30194,1.45365,1.79277,0.56727,0.031184,0.207229
3,2,1,3.71833,0.202336,0.172606,2.43155,5.03284,0.375118,-0.790998,-3.83455,0.400925,0.372075,0.754779
3,2,1,-1.25583,1.47411,0.819895,-2.78448,4.36107,0.113478,0.107247,2.55416,0.385214,0.384141,0.797017
3,2,1,1.71562,-0.478906,-0.61026,-1.17859,3.1361,0.360865,0.0914124,2.2536,-0.0127033,0.431384,0.365879
3,2,1,-1.75501,-0.0880109,-0.026461,-2.68365,2.88327,0.072433,0.0245574,1.95701,-5.87306,-0.563325,-4.48698
3,2,1,1.87886,0.47325,-0.0511434,1.79719,1.94184,0.328234,0.0220624,2.54301,-0.401251,1.77114,-0.565042
3,2,1,-1.60981,1.67946,0.217969,-1.01084,4.35765,0.130722,0.127348,2.65259,0.0709006,0.384957,0.481341
3,2,1,6.90393,1.27603,0.349979,4.84299,12.4465,-4.65212,-2.15462,0.408255,0.295914,0.0666312,0.4354
3,2,1,3.04491,0.18183,0.111428,2.10543,3.32281,-0.49945,-3.78487,-7.88748,0.444834,-4.36795,-4.01116
3,2,1,2.91407,0.251062,0.0435786,2.27032,0.308764,1.59867,0.766589,0.134898,0.475285,0.00569166,0.326702
3,2,1,1.44846,0.205113,0.0448314,1.45909,1.93039,0.286882,0.0586866,1.17886,1.39166,-0.600981,0.0448012
3,2,1,-0.702729,1.84783,-0.00894741,-12.1377,-140.908,-69.0514,-27.175,-146.219,-4.51728,-0.14351,-3.88527
3,2,1,4.2004,0.255152,0.0750175,2.63627,-1.17866,1.67307,0.209535,-1.03514,0.371915,0.0747402,0.498851
3,2,1,3.28015,0.190841,0.175032,2.31848,-0.821253,-1.83022,1.5009,-1.6155,0.417221,-0.0239081,0.360136
3,2,1,2.0525,0.0719707,0.0148549,2.1486,2.00198,1.73479,2.09334,1.44082,0.954066,0.00952719,-0.152697
3,2,1,3.31406,0.407717,0.0380361,2.47187,-0.266021,-1.73458,0.970613,-1.79463,0.415264,-0.0144649,0.374956
3,2,1,6.25099,-3.17047,1.46865,-0.279767,-2.91952,-0.395169,-0.0444215,-3.76202,-0.00491872,-1.23429,-0.425145
3,2,1,1.59636,0.096013,0.0630313,0.826005,1.46151,0.077228,0.0482484,1.68899,-0.289647,1.15304,-0.0688712
3,2,1,33.0172,0.520442,0.135338,28.5555,-3.97298,-0.0574545,-0.0193135,3.34452,0.297691,-5.9863,6.38072
3,2,1,2.32742,0.056139,0.0279748,2.30198,0.254535,0.117712,2.12797,1.7883,0.827797,-0.00209653,-0.0205758
3,2,1,-1.76524,-0.0296866,-0.00748622,-2.47523,4.4267,-0.377196,-4.83785,1.3517,-2.37434,-0.00716866,-1.55224
3,2,1,1.2047,-1.47491,1.03821,1.50617,3.23679,0.3845,-0.018138,2.47251,0.0419137,0.407618,0.357451
3,2,1,3.93828,-0.00820725,-0.00812648,-1.1651,2.06372,0.114124,0.0339398,2.70327,-0.0635318,1.17159,-0.373077
Workload Information:
2.32528, 2.69661, 2.33489, 2.61968, 1.39219, 0.631348, 2.14457, 2.09786, 2.0156, 1.40994, 1.98032, 2.35171, 2.14681, 5.51269, 0.508952, 0.535891, 0.5, 2.79566, 3.22107, 2.76887, 0.692256, 1.46167, 0.850599, 3.00553, 2.23892, 1.87871, 1.2304, 2.62408, 1.53786, 2.49003, 2.48813, 1.34152, 1.20766, 2.56005, 2.63035, 1.77229, 2.66158, 1.90349, 1.20222, 0.5, 2.9669, 2.76581, 1.98381, 1.59411, 2.21138, 1.7083, 2.1388, 2.3636, 2.19001, 0.5, 4.03973, 1.23369, 3.32551, 0.711065, 3.24643, 1.1781, 4.52644, 1.21575, 0.964015, 0.869259, 0.938447, 3.922, 0.843693, 1.07256, 1.25476, 3.96854, 1.39503, 3.56897, 0.5, 2.38786, 2.90278, 1.24536, 2.93462, 0.896481, 1.06807, 2.45856, 1.87207, 2.7702, 1.69628, 2.78384, 1.12446, 0.5, 4.4353, 1.46012, 2.38702, 1.72511, 2.41288, 0.580633, 2.60767, 2.28668, 3.58483, 1.62044, 2.45795, 1.4516, 1.25484, 1.63034, 0.91103, 2.63715, 3.19185, 2.24391, 1.0621, 1.95395, 1.32327, 1.66125, 1.74398, 2.48313, 2.38041, 2.40796, 2.4193, 1.97648, 3.89831, 1.45677, 1.43626, 0.812887, 1.06923, 2.69108, 2.69001, 2.40738, 0.5, 2.6423, 2.42072, 2.2702, 2.33269, 1.11317, 2.14266, 1.72055, 1.81811, 2.32174, 0.5, 1.62056, 3.11018, 2.62942, 0.707084, 5.29154, 0.5, 1.5014, 0.779448, 3.22052, 0.5, 1.39923, 2.29096, 2.96028, 0.653012, 4.19651, 2.63646, 4.68393, 16.8611, 9.38735, 10.584, 15.8471, 12.4193, 11.3196, 8.5879, 5.72528, 10.3222, 11.6257, 2.31026, 0.923478, 2.2913, 2.17043, 2.2971, 2.00743, 2.10152, 2.2143, 1.84459, 1.79931, 2.20512, 1.83515, 2.41186, 0.899128, 2.56038, 3.06383, 0.544909, 2.51989, 0.713665, 2.36931, 5.49921, 0.839979, 0.5, 2.07783, 5.95985, 14.7485, 5.49833, 11.4358, 10.0437, 12.3138, 10.8716, 6.94639, 11.4038, 9.4133, 11.2042, 10.1607, 2.11606, 2.1784, 1.63394, 2.49925, 1.77286, 1.79949, 2.09519, 0.5, 3.13735, 2.57248, 0.589622, 3.10535, 0.5, 2.33476, 2.6411, 2.10101, 1.2168, 3.20633, 0.714263, 1.016, 4.23816, 2.98103, 2.23276, 0.817779, 2.06887, 21.1907, 15.5591, 1.76883, 12.7602, 6.65228, 9.39986, 0.989694, 12.6479, 13.8169, 5.73979, 17.4058, 0.553682, 0.732482, 4.94519, 3.15069, 2.11795, 0.5, 2.1616, 1.82805, 1.98405, 2.41493, 1.33159, 2.27978, 1.97777, 1.89826, 2.04107, 2.08565, 1.86146, 2.1358, 1.84052, 2.09338, 3.1304, 0.617474, 1.88191, 2.43632, 1.05405, 2.49331, 2.40819, 2.42397, 2.30645, 1.31403, 1.10261, 2.56829, 2.2386, 2.10825, 2.51723, 1.46503, 1.65952, 0.5, 3.22461, 0.849226, 3.22171, 2.54493, 1.44435, 2.97665, 2.08928, 0.529936, 4.44981, 0.509979, 2.21915, 2.3045, 1.9667, 1.74361, 1.68983, 2.07621, 0.917113, 3.85127, 2.09248, 1.7479, 0.810541, 2.58069
cpi min/max: 0.5/21.1907
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 30672, operands = 74976
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 501, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.611858


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case efficiency: 0.965273
[1 sec, 20028 itr]: obj = (0.0399392), mov = 20028, inf = 94.9%, acc = 4.1%, imp = 157

worst case efficiency: 0.623964
[2 sec, 42069 itr]: obj = (0.0737076), mov = 42069, inf = 97.5%, acc = 2.1%, imp = 185

worst case efficiency: 0.644225
[3 sec, 53885 itr]: obj = (0.0766744), mov = 53885, inf = 98%, acc = 1.7%, imp = 210

worst case efficiency: 0.646005
[4 sec, 64154 itr]: obj = (0.0806839), mov = 64154, inf = 98.3%, acc = 1.4%, imp = 235

worst case efficiency: 0.64841
[5 sec, 72418 itr]: obj = (0.0824016), mov = 72418, inf = 98.5%, acc = 1.3%, imp = 250

worst case efficiency: 0.649441
[6 sec, 78874 itr]: obj = (0.084569), mov = 78874, inf = 98.6%, acc = 1.2%, imp = 268

worst case efficiency: 0.650741
[7 sec, 83923 itr]: obj = (0.0881363), mov = 83923, inf = 98.7%, acc = 1.2%, imp = 289

worst case efficiency: 0.652882
[8 sec, 88433 itr]: obj = (0.094081), mov = 88433, inf = 98.7%, acc = 1.2%, imp = 314

worst case efficiency: 0.656449
[9 sec, 92577 itr]: obj = (0.0981396), mov = 92577, inf = 98.7%, acc = 1.1%, imp = 338

worst case efficiency: 0.658884
[10 sec, 96023 itr]: obj = (0.100478), mov = 96023, inf = 98.8%, acc = 1.1%, imp = 354

worst case efficiency: 0.660287
[11 sec, 99433 itr]: obj = (0.102591), mov = 99433, inf = 98.8%, acc = 1.1%, imp = 377

worst case efficiency: 0.661554
[12 sec, 102954 itr]: obj = (0.105753), mov = 102954, inf = 98.8%, acc = 1.1%, imp = 400

worst case efficiency: 0.663452
[13 sec, 106510 itr]: obj = (0.109384), mov = 106510, inf = 98.8%, acc = 1.1%, imp = 414

worst case efficiency: 0.66563
[14 sec, 110013 itr]: obj = (0.109929), mov = 110013, inf = 98.9%, acc = 1%, imp = 423

worst case efficiency: 0.665957
[15 sec, 113412 itr]: obj = (0.111368), mov = 113412, inf = 98.9%, acc = 1%, imp = 445

worst case efficiency: 0.666821
[16 sec, 116823 itr]: obj = (0.114563), mov = 116823, inf = 98.9%, acc = 1%, imp = 466

worst case efficiency: 0.668738
[17 sec, 120392 itr]: obj = (0.115545), mov = 120392, inf = 98.9%, acc = 1%, imp = 476

worst case efficiency: 0.669327
[18 sec, 123896 itr]: obj = (0.118204), mov = 123896, inf = 98.9%, acc = 1%, imp = 501

worst case efficiency: 0.670922
[19 sec, 127449 itr]: obj = (0.119653), mov = 127449, inf = 99%, acc = 1%, imp = 517

worst case efficiency: 0.671792
[20 sec, 131250 itr]: obj = (0.121325), mov = 131250, inf = 99%, acc = 1%, imp = 532

worst case efficiency: 0.672795
[21 sec, 135379 itr]: obj = (0.122345), mov = 135379, inf = 99%, acc = 0.9%, imp = 548

worst case efficiency: 0.673407
[22 sec, 139733 itr]: obj = (0.122711), mov = 139733, inf = 99%, acc = 0.9%, imp = 556

worst case efficiency: 0.673627
[23 sec, 143168 itr]: obj = (0.122855), mov = 143168, inf = 99%, acc = 0.9%, imp = 562

worst case efficiency: 0.673713
[24 sec, 147221 itr]: obj = (0.122855), mov = 147221, inf = 99.1%, acc = 0.9%, imp = 562

worst case efficiency: 0.673713
[25 sec, 151825 itr]: obj = (0.122855), mov = 151825, inf = 99.1%, acc = 0.9%, imp = 562

worst case efficiency: 0.673713
[26 sec, 156865 itr]: obj = (0.122855), mov = 156865, inf = 99.1%, acc = 0.8%, imp = 562

worst case efficiency: 0.673713
[27 sec, 162835 itr]: obj = (0.122855), mov = 162835, inf = 99.2%, acc = 0.8%, imp = 562

worst case efficiency: 0.673713
[28 sec, 168710 itr]: obj = (0.122855), mov = 168710, inf = 99.2%, acc = 0.8%, imp = 562

worst case efficiency: 0.673713
[29 sec, 175434 itr]: obj = (0.122855), mov = 175434, inf = 99.2%, acc = 0.7%, imp = 562

worst case efficiency: 0.673713
[30 sec, 182826 itr]: obj = (0.122855), mov = 182826, inf = 99.3%, acc = 0.7%, imp = 562

worst case efficiency: 0.673713
[31 sec, 190722 itr]: obj = (0.122855), mov = 190722, inf = 99.3%, acc = 0.7%, imp = 562

worst case efficiency: 0.673713
[32 sec, 199139 itr]: obj = (0.122855), mov = 199139, inf = 99.3%, acc = 0.7%, imp = 562

worst case efficiency: 0.673713
[33 sec, 209153 itr]: obj = (0.123241), mov = 209153, inf = 99.4%, acc = 0.6%, imp = 568

worst case efficiency: 0.673944
[34 sec, 219933 itr]: obj = (0.123983), mov = 219933, inf = 99.4%, acc = 0.6%, imp = 575

worst case efficiency: 0.67439
[35 sec, 231226 itr]: obj = (0.123983), mov = 231226, inf = 99.4%, acc = 0.6%, imp = 575

worst case efficiency: 0.67439
[36 sec, 242131 itr]: obj = (0.123983), mov = 242131, inf = 99.4%, acc = 0.6%, imp = 575

worst case efficiency: 0.67439
[37 sec, 251076 itr]: obj = (0.123983), mov = 251076, inf = 99.5%, acc = 0.6%, imp = 575

worst case efficiency: 0.67439
[38 sec, 262498 itr]: obj = (0.123983), mov = 262498, inf = 99.5%, acc = 0.5%, imp = 575

worst case efficiency: 0.67439
[39 sec, 272739 itr]: obj = (0.123983), mov = 272739, inf = 99.5%, acc = 0.5%, imp = 575

worst case efficiency: 0.67439
[40 sec, 284007 itr]: obj = (0.123983), mov = 284007, inf = 99.5%, acc = 0.5%, imp = 575

worst case efficiency: 0.67439
[41 sec, 295316 itr]: obj = (0.123983), mov = 295316, inf = 99.5%, acc = 0.5%, imp = 575

worst case efficiency: 0.67439
[42 sec, 306513 itr]: obj = (0.123983), mov = 306513, inf = 99.6%, acc = 0.5%, imp = 575

worst case efficiency: 0.67439
[43 sec, 318495 itr]: obj = (0.123983), mov = 318495, inf = 99.6%, acc = 0.5%, imp = 575

worst case efficiency: 0.67439
[44 sec, 330327 itr]: obj = (0.123983), mov = 330327, inf = 99.6%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[45 sec, 341364 itr]: obj = (0.123983), mov = 341364, inf = 99.6%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[46 sec, 352339 itr]: obj = (0.123983), mov = 352339, inf = 99.6%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[47 sec, 363366 itr]: obj = (0.123983), mov = 363366, inf = 99.6%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[48 sec, 374242 itr]: obj = (0.123983), mov = 374242, inf = 99.6%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[49 sec, 385316 itr]: obj = (0.123983), mov = 385316, inf = 99.6%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[50 sec, 397668 itr]: obj = (0.123983), mov = 397668, inf = 99.7%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[51 sec, 410405 itr]: obj = (0.123983), mov = 410405, inf = 99.7%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[52 sec, 422544 itr]: obj = (0.123983), mov = 422544, inf = 99.7%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[53 sec, 434022 itr]: obj = (0.123983), mov = 434022, inf = 99.7%, acc = 0.4%, imp = 575

worst case efficiency: 0.67439
[54 sec, 445200 itr]: obj = (0.123983), mov = 445200, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[55 sec, 457025 itr]: obj = (0.123983), mov = 457025, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[56 sec, 467854 itr]: obj = (0.123983), mov = 467854, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[57 sec, 479150 itr]: obj = (0.123983), mov = 479150, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[58 sec, 486798 itr]: obj = (0.123983), mov = 486798, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[59 sec, 494170 itr]: obj = (0.123983), mov = 494170, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[60 sec, 504540 itr]: obj = (0.123983), mov = 504540, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[61 sec, 514927 itr]: obj = (0.123983), mov = 514927, inf = 99.7%, acc = 0.3%, imp = 575

worst case efficiency: 0.67439
[62 sec, 525502 itr]: obj = (0.124306), mov = 525502, inf = 99.7%, acc = 0.3%, imp = 576

worst case efficiency: 0.674583
[63 sec, 537350 itr]: obj = (0.124893), mov = 537350, inf = 99.7%, acc = 0.3%, imp = 588

worst case efficiency: 0.674936
[64 sec, 549041 itr]: obj = (0.125302), mov = 549041, inf = 99.7%, acc = 0.3%, imp = 595

worst case efficiency: 0.675181
[65 sec, 560454 itr]: obj = (0.125341), mov = 560454, inf = 99.8%, acc = 0.3%, imp = 598

worst case efficiency: 0.675205
[66 sec, 570777 itr]: obj = (0.125566), mov = 570777, inf = 99.8%, acc = 0.3%, imp = 604

worst case efficiency: 0.67534
[67 sec, 583185 itr]: obj = (0.125707), mov = 583185, inf = 99.8%, acc = 0.3%, imp = 610

worst case efficiency: 0.675424
[68 sec, 593995 itr]: obj = (0.125863), mov = 593995, inf = 99.8%, acc = 0.3%, imp = 621

worst case efficiency: 0.675518
[69 sec, 604784 itr]: obj = (0.12592), mov = 604784, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[70 sec, 615486 itr]: obj = (0.12592), mov = 615486, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[71 sec, 625949 itr]: obj = (0.12592), mov = 625949, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[72 sec, 637045 itr]: obj = (0.12592), mov = 637045, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[73 sec, 648460 itr]: obj = (0.12592), mov = 648460, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[74 sec, 659063 itr]: obj = (0.12592), mov = 659063, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[75 sec, 668787 itr]: obj = (0.12592), mov = 668787, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[76 sec, 679459 itr]: obj = (0.12592), mov = 679459, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[77 sec, 691767 itr]: obj = (0.12592), mov = 691767, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[78 sec, 703349 itr]: obj = (0.12592), mov = 703349, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[79 sec, 715248 itr]: obj = (0.12592), mov = 715248, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[80 sec, 724862 itr]: obj = (0.12592), mov = 724862, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[81 sec, 735042 itr]: obj = (0.12592), mov = 735042, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[82 sec, 744101 itr]: obj = (0.12592), mov = 744101, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[83 sec, 754301 itr]: obj = (0.12592), mov = 754301, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[84 sec, 764578 itr]: obj = (0.12592), mov = 764578, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[85 sec, 774890 itr]: obj = (0.12592), mov = 774890, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[86 sec, 784086 itr]: obj = (0.12592), mov = 784086, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[87 sec, 793586 itr]: obj = (0.12592), mov = 793586, inf = 99.8%, acc = 0.3%, imp = 625

worst case efficiency: 0.675552
[88 sec, 803427 itr]: obj = (0.12592), mov = 803427, inf = 99.8%, acc = 0.2%, imp = 625

worst case efficiency: 0.675552
[89 sec, 814145 itr]: obj = (0.12592), mov = 814145, inf = 99.8%, acc = 0.2%, imp = 625

worst case efficiency: 0.675552
[90 sec, 824576 itr]: obj = (0.12592), mov = 824576, inf = 99.8%, acc = 0.2%, imp = 625

worst case efficiency: 0.675552
[90 sec, 824576 itr]: obj = (0.12592), mov = 824576, inf = 99.8%, acc = 0.2%, imp = 625

worst case efficiency: 0.675552
824576 iterations, 824576 moves performed in 90 seconds
Feasible solution: obj = (0.12592)

Run output...
FINAL SOLUTION
Worst case efficiency: 0.675552
Mapping: 0->24, 1->10, 2->13, 3->8, 4->37, 5->27, 6->33, 7->37, 8->29, 9->17, 10->1, 11->20, 12->42, 13->43, 14->35, 15->0, 16->6, 17->30, 18->16, 19->11, 20->5, 21->27, 22->4, 23->46, 24->28, 25->23, 26->40, 27->14, 28->2, 29->44, 30->9, 31->11, 32->46, 33->40, 34->37, 35->41, 36->37, 37->19, 38->33, 39->5, 40->8, 41->1, 42->35, 43->30, 44->5, 45->36, 46->39, 47->18, 48->24, 49->23, 50->17, 51->6, 52->17, 53->11, 54->39, 55->20, 56->25, 57->42, 58->1, 59->25, 60->24, 61->35, 62->4, 63->11, 64->5, 65->13, 66->20, 67->29, 68->4, 69->25, 70->20, 71->44, 72->37, 73->35, 74->23, 75->26, 76->38, 77->22, 78->42, 79->14, 80->40, 81->47, 82->15, 83->43, 84->22, 85->46, 86->8, 87->42, 88->22, 89->7, 90->38, 91->22, 92->15, 93->8, 94->45, 95->28, 96->47, 97->25, 98->42, 99->35, 100->41, 101->6, 102->34, 103->46, 104->3, 105->16, 106->28, 107->39, 108->12, 109->9, 110->12, 111->1, 112->31, 113->17, 114->5, 115->31, 116->21, 117->12, 118->41, 119->45, 120->12, 121->39, 122->10, 123->0, 124->33, 125->43, 126->27, 127->10, 128->30, 129->26, 130->25, 131->20, 132->26, 133->40, 134->5, 135->29, 136->36, 137->30, 138->4, 139->41, 140->6, 141->13, 142->0, 143->10, 144->24, 145->34, 146->14, 147->32, 148->31, 149->15, 150->37, 151->16, 152->26, 153->12, 154->40, 155->33, 156->16, 157->28, 158->4, 159->13, 160->10, 161->43, 162->2, 163->14, 164->30, 165->41, 166->26, 167->43, 168->19, 169->0, 170->7, 171->18, 172->0, 173->31, 174->3, 175->40, 176->32, 177->47, 178->3, 179->47, 180->2, 181->45, 182->16, 183->32, 184->13, 185->26, 186->31, 187->38, 188->13, 189->14, 190->12, 191->20, 192->27, 193->19, 194->2, 195->3, 196->45, 197->38, 198->23, 199->36, 200->1, 201->21, 202->23, 203->9, 204->24, 205->34, 206->33, 207->30, 208->2, 209->22, 210->23, 211->47, 212->38, 213->9, 214->45, 215->47, 216->29, 217->19, 218->3, 219->18, 220->14, 221->15, 222->15, 223->1, 224->21, 225->28, 226->34, 227->27, 228->4, 229->7, 230->32, 231->44, 232->2, 233->28, 234->25, 235->11, 236->19, 237->21, 238->11, 239->36, 240->36, 241->17, 242->32, 243->31, 244->19, 245->45, 246->8, 247->17, 248->21, 249->10, 250->46, 251->43, 252->15, 253->44, 254->34, 255->18, 256->27, 257->18, 258->46, 259->8, 260->16, 261->44, 262->33, 263->24, 264->6, 265->0, 266->7, 267->44, 268->42, 269->38, 270->29, 271->7, 272->9, 273->41, 274->21, 275->36, 276->22, 277->9, 278->3, 279->32, 280->35, 281->7, 282->29, 283->39, 284->39, 285->6, 286->18, 287->34
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	0.707685	1.88233	2.42799	3.83046	0.923213	1.03794
	1.56434	2.3095	2.30998	2.49079	2.41035	1.50894
	4.67921	5.48026	7.77613	6.64109	4.51183	2.26328
	1.95747	5.255	3.43628	4.65092	2.46847	1.14097
	1.67587	2.61542	4.64883	4.28991	3.58168	1.91939
	2.00932	5.03143	5.84204	3.71225	3.17844	1.87416
	1.29255	4.02265	3.49765	2.66786	3.81633	1.17715
	2.00884	2.49538	2.05615	4.13118	1.76401	1.0271
Efficiency map:
	0.996737	0.936225	0.956214	0.986781	0.927299	0.995895
	0.817756	0.675778	0.676939	0.676984	0.682975	0.82889
	0.935517	0.712285	0.845967	0.676562	0.705457	0.834665
	0.831576	0.737336	0.689969	0.739551	0.675564	0.799937
	0.81473	0.675552	0.700036	0.680205	0.683447	0.818082
	0.817653	0.715527	0.684451	0.68423	0.69359	0.823607
	0.871815	0.700482	0.68913	0.688793	0.714398	0.769067
	0.845659	0.779021	0.67556	0.795028	0.675552	0.798
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 0 --global-tlim 90 --seed 501

Starting the Optimization Process
