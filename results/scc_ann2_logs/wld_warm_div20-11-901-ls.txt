LocalSolver 4.0 (MacOS64, build 20131224)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
ANNs For the Platform:
3,2,1,0.413586,-0.0266769,-0.0683771,-5.1002,70.5254,-0.257939,-72.3361,-14.3957,-55.9961,0.000416225,-55.1984
3,2,1,7.29748,1.94844,3.18138,9.63196,-4.67735,-4.13678,1.22405,-8.03403,0.0803394,-0.125838,0.598848
3,2,1,6.43996,0.383984,0.598454,7.79024,-4.25175,0.123161,-0.278344,-5.71253,1.47885,1.00258,0.327892
3,2,1,3.45395,0.140329,0.51254,4.25255,4.08469,0.00326531,0.722664,3.88666,1.2829,-0.290289,-0.187123
3,2,1,1.76581,-0.7465,16.0398,1.01103,2.42771,0.96084,1.00318,4.14242,-0.00284347,0.244904,0.56299
3,2,1,1.87058,0.359859,-0.0851999,0.818043,3.18969,0.847611,-7.29654,-2.24823,0.00245596,0.000486788,0.794563
3,2,1,3.28113,0.579268,0.142679,3.72653,-0.240895,-0.149383,25.0414,7.13641,0.488705,-0.00083541,0.320468
3,2,1,-4.78997,-2.53518,1.7971,11.1628,-3.08773,-0.470276,-0.0894822,-2.63871,1.46314,-0.463195,-1.12076
3,2,1,-0.392198,-0.00544948,0.0100489,-0.634573,-1.21109,-0.0856362,-0.013156,-1.60668,1.54201,-2.33195,-0.462253
3,2,1,3.15031,0.401017,0.160161,2.51156,5.71314,0.59291,-11.6364,2.59774,0.430465,0.00737346,0.36306
3,2,1,2.64111,0.312028,0.0803827,2.35273,-4.25958,3.48188,-1.59773,-1.57632,0.565527,0.0271972,0.266241
3,2,1,2.2658,0.0425076,0.0191991,-0.583848,2.82415,0.533705,0.0754136,3.33659,-0.0169351,0.54785,0.257008
3,2,1,6.01014,0.654767,0.24854,5.54419,5.68574,0.00137666,0.00103835,-1.46229,0.258518,0.00962319,0.54621
3,2,1,-0.828282,0.684875,0.223674,-1.44779,5.0064,0.272371,0.00344824,2.89438,0.397142,0.379767,0.797684
3,2,1,-0.166745,-1.25514,-0.967701,-3.82155,3.34568,0.34907,0.176897,2.37943,0.567319,0.42026,0.93611
3,2,1,-10.035,3.55243,8.66202,-21.5626,10.92,0.228744,0.12852,5.40188,0.871264,0.345634,1.3209
3,2,1,4.50278,0.709934,0.227356,3.41616,1.455,0.115548,0.0125181,-1.15221,0.345467,0.222127,0.602264
3,2,1,3.51765,0.552987,0.202187,3.41883,0.53704,0.0709757,-0.0346297,-0.38017,0.320095,-0.0397752,0.474568
3,2,1,3.62936,0.506661,0.201819,3.49179,3.4019,-3.09955,1.3332,0.961184,0.32649,-0.0126443,0.488413
3,2,1,3.50925,0.448618,0.127508,2.4823,1.78455,1.6893,1.46456,-0.207671,0.400287,0.00684206,0.402337
3,2,1,-0.52883,0.00691432,0.00617163,-0.194006,1.73284,0.0488672,0.0179273,1.25978,0.716787,0.92667,0.162242
3,2,1,2.48239,0.190378,0.120167,1.91689,1.21532,1.48543,0.644574,2.68198,0.547395,-0.0744913,0.328097
3,2,1,2.55861,2.02297,0.650126,2.19109,2.44652,0.0836318,0.0133745,2.4369,0.0418722,0.798841,-0.0334834
3,2,1,5.07371,0.556895,0.217981,4.66725,-0.00829845,-0.00544544,0.00934569,-0.672651,0.280461,-0.520981,0.217266
3,2,1,1.56588,0.201966,0.0665967,1.82578,1.97654,0.236717,0.0903453,1.08195,1.02748,-0.229836,0.011785
3,2,1,2.41728,0.208205,-0.0012583,2.05484,1.41976,-1.30194,1.45365,1.79277,0.56727,0.031184,0.207229
3,2,1,3.71833,0.202336,0.172606,2.43155,5.03284,0.375118,-0.790998,-3.83455,0.400925,0.372075,0.754779
3,2,1,-1.25583,1.47411,0.819895,-2.78448,4.36107,0.113478,0.107247,2.55416,0.385214,0.384141,0.797017
3,2,1,1.71562,-0.478906,-0.61026,-1.17859,3.1361,0.360865,0.0914124,2.2536,-0.0127033,0.431384,0.365879
3,2,1,-1.75501,-0.0880109,-0.026461,-2.68365,2.88327,0.072433,0.0245574,1.95701,-5.87306,-0.563325,-4.48698
3,2,1,1.87886,0.47325,-0.0511434,1.79719,1.94184,0.328234,0.0220624,2.54301,-0.401251,1.77114,-0.565042
3,2,1,-1.60981,1.67946,0.217969,-1.01084,4.35765,0.130722,0.127348,2.65259,0.0709006,0.384957,0.481341
3,2,1,6.90393,1.27603,0.349979,4.84299,12.4465,-4.65212,-2.15462,0.408255,0.295914,0.0666312,0.4354
3,2,1,3.04491,0.18183,0.111428,2.10543,3.32281,-0.49945,-3.78487,-7.88748,0.444834,-4.36795,-4.01116
3,2,1,2.91407,0.251062,0.0435786,2.27032,0.308764,1.59867,0.766589,0.134898,0.475285,0.00569166,0.326702
3,2,1,1.44846,0.205113,0.0448314,1.45909,1.93039,0.286882,0.0586866,1.17886,1.39166,-0.600981,0.0448012
3,2,1,-0.702729,1.84783,-0.00894741,-12.1377,-140.908,-69.0514,-27.175,-146.219,-4.51728,-0.14351,-3.88527
3,2,1,4.2004,0.255152,0.0750175,2.63627,-1.17866,1.67307,0.209535,-1.03514,0.371915,0.0747402,0.498851
3,2,1,3.28015,0.190841,0.175032,2.31848,-0.821253,-1.83022,1.5009,-1.6155,0.417221,-0.0239081,0.360136
3,2,1,2.0525,0.0719707,0.0148549,2.1486,2.00198,1.73479,2.09334,1.44082,0.954066,0.00952719,-0.152697
3,2,1,3.31406,0.407717,0.0380361,2.47187,-0.266021,-1.73458,0.970613,-1.79463,0.415264,-0.0144649,0.374956
3,2,1,6.25099,-3.17047,1.46865,-0.279767,-2.91952,-0.395169,-0.0444215,-3.76202,-0.00491872,-1.23429,-0.425145
3,2,1,1.59636,0.096013,0.0630313,0.826005,1.46151,0.077228,0.0482484,1.68899,-0.289647,1.15304,-0.0688712
3,2,1,33.0172,0.520442,0.135338,28.5555,-3.97298,-0.0574545,-0.0193135,3.34452,0.297691,-5.9863,6.38072
3,2,1,2.32742,0.056139,0.0279748,2.30198,0.254535,0.117712,2.12797,1.7883,0.827797,-0.00209653,-0.0205758
3,2,1,-1.76524,-0.0296866,-0.00748622,-2.47523,4.4267,-0.377196,-4.83785,1.3517,-2.37434,-0.00716866,-1.55224
3,2,1,1.2047,-1.47491,1.03821,1.50617,3.23679,0.3845,-0.018138,2.47251,0.0419137,0.407618,0.357451
3,2,1,3.93828,-0.00820725,-0.00812648,-1.1651,2.06372,0.114124,0.0339398,2.70327,-0.0635318,1.17159,-0.373077
Workload Information:
0.5, 3.81296, 3.87583, 2.27713, 1.03321, 0.500884, 1.62568, 1.80856, 1.09966, 2.84771, 1.76371, 2.85469, 1.17437, 2.58049, 1.80979, 2.67232, 1.28661, 2.47642, 1.95892, 1.85795, 1.92912, 2.19539, 2.04301, 2.01562, 2.20498, 2.19371, 2.01148, 1.50578, 2.07573, 2.00832, 0.84475, 3.40524, 2.69364, 1.4952, 0.5, 3.06118, 2.70627, 2.36264, 1.8097, 1.10072, 1.22972, 2.79096, 0.647386, 1.61021, 2.33738, 0.5, 1.14249, 5.76253, 1.72541, 2.42155, 1.60517, 1.01494, 3.29655, 1.93639, 4.99356, 1.79488, 1.67445, 0.580958, 2.14963, 0.806525, 2.0154, 2.61515, 1.24017, 1.8084, 1.91464, 2.40624, 0.806722, 1.47745, 2.20499, 3.51697, 3.2042, 0.789676, 0.59456, 0.96265, 2.6067, 2.31696, 4.87612, 0.643004, 3.43285, 0.620382, 1.21686, 3.3258, 1.96387, 1.44024, 0.94898, 0.5, 2.9513, 2.93378, 2.57022, 2.09572, 0.5, 0.803253, 2.6402, 1.36485, 2.10134, 4.59036, 4.34096, 0.763328, 2.03923, 3.41068, 0.675672, 0.770143, 1.67581, 1.22702, 1.90911, 5.07827, 1.60978, 0.5, 0.5, 2.2436, 1.37725, 3.70589, 2.56693, 1.60633, 1.28532, 3.34844, 2.04848, 1.70325, 3.11451, 0.5, 3.8257, 2.66265, 2.54595, 1.1641, 0.5, 1.30161, 2.53083, 2.19387, 2.97221, 2.09707, 1.40411, 0.801911, 2.8914, 2.64037, 3.34792, 2.11958, 0.5, 0.500726, 3.2597, 3.62041, 2.92232, 0.5, 0.666623, 1.03095, 0.5, 2.42712, 1.97779, 3.12511, 1.82064, 2.14934, 4.48429, 1.75809, 1.56529, 1.12895, 1.55094, 1.51243, 1.7981, 2.43541, 2.2263, 2.13666, 1.52789, 1.87564, 3.47137, 0.836303, 3.2841, 0.5, 3.22641, 0.681832, 2.96647, 1.01755, 2.6172, 1.64574, 2.97296, 0.780062, 2.58977, 2.69655, 2.12138, 1.87539, 1.42695, 1.28997, 2.12806, 2.34382, 2.54858, 1.79019, 2.66604, 0.523303, 2.29381, 2.06349, 1.60978, 1.89733, 1.89669, 2.23891, 2.79868, 3.03558, 2.81649, 1.137, 0.5, 1.71225, 1.36767, 2.66207, 0.611308, 2.16961, 2.12982, 3.05952, 2.52021, 2.09371, 2.37688, 2.11109, 0.698712, 2.19939, 0.5, 1.38675, 4.78868, 0.827206, 0.83915, 3.65821, 0.5, 3.40403, 3.53304, 2.72326, 1.07935, 0.760316, 1.82307, 1.24257, 1.66177, 2.36872, 2.60941, 2.29446, 1.9379, 1.82532, 2.37006, 2.32553, 2.08922, 1.45196, 2.46326, 1.26431, 0.5, 1.20221, 5.76015, 0.810084, 0.632594, 0.603053, 3.04023, 1.39346, 0.89507, 5.43559, 4.12522, 1.91597, 0.5, 0.776778, 3.94175, 0.740289, 3.29175, 4.13465, 1.61764, 1.51594, 0.793463, 0.646556, 3.12918, 4.30524, 1.54304, 0.728343, 0.5, 1.7942, 0.5, 0.54231, 1.24576, 3.8789, 4.25341, 1.57962, 1.22202, 4.17073, 0.58695, 2.40113, 0.756322, 2.86285, 0.5, 2.51082, 2.69678, 1.02573, 2.18458, 3.08209, 3.15324, 1.74242, 2.93218, 1.25367, 0.863003, 2.05549
cpi min/max: 0.5/5.76253
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 30665, operands = 74976
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 901, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.608788


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case efficiency: 0.965273
[1 sec, 2012 itr]: infeas = (36, 36, 46), mov = 2012, inf = 71.3%, acc = 25.2%, imp = 137

worst case efficiency: 0.617576
[2 sec, 30530 itr]: obj = (0.0508425), mov = 30530, inf = 96.9%, acc = 2.6%, imp = 200

worst case efficiency: 0.630506
[3 sec, 53725 itr]: obj = (0.067953), mov = 53725, inf = 98.2%, acc = 1.6%, imp = 233

worst case efficiency: 0.640772
[4 sec, 64260 itr]: obj = (0.0760174), mov = 64260, inf = 98.4%, acc = 1.4%, imp = 267

worst case efficiency: 0.64561
[5 sec, 71132 itr]: obj = (0.0780856), mov = 71132, inf = 98.5%, acc = 1.3%, imp = 289

worst case efficiency: 0.646851
[6 sec, 76890 itr]: obj = (0.0800659), mov = 76890, inf = 98.6%, acc = 1.3%, imp = 313

worst case efficiency: 0.64804
[7 sec, 82299 itr]: obj = (0.0812018), mov = 82299, inf = 98.7%, acc = 1.2%, imp = 333

worst case efficiency: 0.648721
[8 sec, 87543 itr]: obj = (0.0813113), mov = 87543, inf = 98.7%, acc = 1.1%, imp = 337

worst case efficiency: 0.648787
[9 sec, 92981 itr]: obj = (0.0813113), mov = 92981, inf = 98.8%, acc = 1.1%, imp = 337

worst case efficiency: 0.648787
[10 sec, 98549 itr]: obj = (0.0815563), mov = 98549, inf = 98.9%, acc = 1%, imp = 341

worst case efficiency: 0.648934
[11 sec, 104803 itr]: obj = (0.083946), mov = 104803, inf = 98.9%, acc = 1%, imp = 378

worst case efficiency: 0.650368
[12 sec, 111671 itr]: obj = (0.0848907), mov = 111671, inf = 99%, acc = 1%, imp = 395

worst case efficiency: 0.650934
[13 sec, 119599 itr]: obj = (0.0854162), mov = 119599, inf = 99%, acc = 0.9%, imp = 419

worst case efficiency: 0.65125
[14 sec, 129224 itr]: obj = (0.0858346), mov = 129224, inf = 99.1%, acc = 0.9%, imp = 430

worst case efficiency: 0.651501
[15 sec, 141707 itr]: obj = (0.0863507), mov = 141707, inf = 99.1%, acc = 0.8%, imp = 452

worst case efficiency: 0.65181
[16 sec, 158760 itr]: obj = (0.0870741), mov = 158760, inf = 99.1%, acc = 0.8%, imp = 466

worst case efficiency: 0.652244
[17 sec, 181121 itr]: obj = (0.0871128), mov = 181121, inf = 99.2%, acc = 0.7%, imp = 470

worst case efficiency: 0.652268
[18 sec, 215706 itr]: obj = (0.08716), mov = 215706, inf = 99.2%, acc = 0.7%, imp = 474

worst case efficiency: 0.652296
[19 sec, 253919 itr]: obj = (0.087172), mov = 253919, inf = 99.2%, acc = 0.6%, imp = 477

worst case efficiency: 0.652303
[20 sec, 292373 itr]: obj = (0.0871994), mov = 292373, inf = 99.1%, acc = 0.6%, imp = 478

worst case efficiency: 0.65232
[21 sec, 331928 itr]: obj = (0.0872582), mov = 331928, inf = 99.1%, acc = 0.6%, imp = 483

worst case efficiency: 0.652355
[22 sec, 371583 itr]: obj = (0.0873194), mov = 371583, inf = 99.1%, acc = 0.6%, imp = 484

worst case efficiency: 0.652392
[23 sec, 411775 itr]: obj = (0.0873194), mov = 411775, inf = 99.1%, acc = 0.6%, imp = 485

worst case efficiency: 0.652392
[24 sec, 452381 itr]: obj = (0.0873194), mov = 452381, inf = 99%, acc = 0.6%, imp = 486

worst case efficiency: 0.652392
[25 sec, 492808 itr]: obj = (0.0873194), mov = 492808, inf = 99%, acc = 0.6%, imp = 487

worst case efficiency: 0.652392
[26 sec, 533173 itr]: obj = (0.0873194), mov = 533173, inf = 99%, acc = 0.5%, imp = 488

worst case efficiency: 0.652392
[27 sec, 573859 itr]: obj = (0.0873473), mov = 573859, inf = 99%, acc = 0.5%, imp = 491

worst case efficiency: 0.652408
[28 sec, 614506 itr]: obj = (0.0873554), mov = 614506, inf = 99%, acc = 0.5%, imp = 494

worst case efficiency: 0.652413
[29 sec, 654541 itr]: obj = (0.0873554), mov = 654541, inf = 99%, acc = 0.5%, imp = 494

worst case efficiency: 0.652413
[30 sec, 695225 itr]: obj = (0.0873778), mov = 695225, inf = 99%, acc = 0.5%, imp = 496

worst case efficiency: 0.652427
[31 sec, 736179 itr]: obj = (0.0873856), mov = 736179, inf = 99%, acc = 0.5%, imp = 499

worst case efficiency: 0.652431
[32 sec, 776709 itr]: obj = (0.0873856), mov = 776709, inf = 99%, acc = 0.5%, imp = 499

worst case efficiency: 0.652431
[33 sec, 817192 itr]: obj = (0.0873899), mov = 817192, inf = 99%, acc = 0.5%, imp = 501

worst case efficiency: 0.652434
[34 sec, 858031 itr]: obj = (0.0873899), mov = 858031, inf = 99%, acc = 0.5%, imp = 501

worst case efficiency: 0.652434
[35 sec, 899246 itr]: obj = (0.0873999), mov = 899246, inf = 99%, acc = 0.5%, imp = 502

worst case efficiency: 0.65244
[36 sec, 939824 itr]: obj = (0.0874067), mov = 939824, inf = 99%, acc = 0.5%, imp = 503

worst case efficiency: 0.652444
[37 sec, 980928 itr]: obj = (0.0874318), mov = 980928, inf = 99%, acc = 0.5%, imp = 505

worst case efficiency: 0.652459
[38 sec, 1022331 itr]: obj = (0.0874378), mov = 1022331, inf = 99%, acc = 0.5%, imp = 506

worst case efficiency: 0.652463
[39 sec, 1063313 itr]: obj = (0.0874427), mov = 1063313, inf = 99%, acc = 0.5%, imp = 507

worst case efficiency: 0.652466
[40 sec, 1104085 itr]: obj = (0.0874427), mov = 1104085, inf = 99%, acc = 0.5%, imp = 507

worst case efficiency: 0.652466
[41 sec, 1144664 itr]: obj = (0.0874427), mov = 1144664, inf = 99%, acc = 0.5%, imp = 508

worst case efficiency: 0.652466
[42 sec, 1186194 itr]: obj = (0.0874427), mov = 1186194, inf = 99%, acc = 0.5%, imp = 508

worst case efficiency: 0.652466
[43 sec, 1227314 itr]: obj = (0.0874427), mov = 1227314, inf = 99%, acc = 0.5%, imp = 508

worst case efficiency: 0.652466
[44 sec, 1268704 itr]: obj = (0.0874959), mov = 1268704, inf = 99%, acc = 0.5%, imp = 511

worst case efficiency: 0.652498
[45 sec, 1309783 itr]: obj = (0.0875084), mov = 1309783, inf = 99%, acc = 0.5%, imp = 513

worst case efficiency: 0.652505
[46 sec, 1351070 itr]: obj = (0.0875632), mov = 1351070, inf = 99%, acc = 0.5%, imp = 518

worst case efficiency: 0.652538
[47 sec, 1392751 itr]: obj = (0.0875632), mov = 1392751, inf = 99%, acc = 0.5%, imp = 518

worst case efficiency: 0.652538
[48 sec, 1434321 itr]: obj = (0.0876081), mov = 1434321, inf = 99%, acc = 0.5%, imp = 519

worst case efficiency: 0.652565
[49 sec, 1476335 itr]: obj = (0.0877094), mov = 1476335, inf = 99%, acc = 0.5%, imp = 527

worst case efficiency: 0.652626
[50 sec, 1517645 itr]: obj = (0.0878474), mov = 1517645, inf = 99%, acc = 0.5%, imp = 535

worst case efficiency: 0.652708
[51 sec, 1558734 itr]: obj = (0.0878474), mov = 1558734, inf = 99%, acc = 0.5%, imp = 536

worst case efficiency: 0.652708
[52 sec, 1599905 itr]: obj = (0.0878474), mov = 1599905, inf = 99%, acc = 0.5%, imp = 536

worst case efficiency: 0.652708
[53 sec, 1642575 itr]: obj = (0.0879155), mov = 1642575, inf = 99%, acc = 0.5%, imp = 542

worst case efficiency: 0.652749
[54 sec, 1684545 itr]: obj = (0.0879172), mov = 1684545, inf = 99%, acc = 0.5%, imp = 543

worst case efficiency: 0.65275
[55 sec, 1726154 itr]: obj = (0.0879172), mov = 1726154, inf = 99%, acc = 0.5%, imp = 543

worst case efficiency: 0.65275
[56 sec, 1767902 itr]: obj = (0.0879427), mov = 1767902, inf = 99%, acc = 0.5%, imp = 546

worst case efficiency: 0.652766
[57 sec, 1810840 itr]: obj = (0.0879448), mov = 1810840, inf = 99%, acc = 0.5%, imp = 549

worst case efficiency: 0.652767
[58 sec, 1854224 itr]: obj = (0.087996), mov = 1854224, inf = 99%, acc = 0.5%, imp = 551

worst case efficiency: 0.652798
[59 sec, 1896712 itr]: obj = (0.087996), mov = 1896712, inf = 99%, acc = 0.5%, imp = 551

worst case efficiency: 0.652798
[60 sec, 1939958 itr]: obj = (0.0879979), mov = 1939958, inf = 99%, acc = 0.5%, imp = 552

worst case efficiency: 0.652799
[61 sec, 1983028 itr]: obj = (0.0879979), mov = 1983028, inf = 99%, acc = 0.5%, imp = 552

worst case efficiency: 0.652799
[62 sec, 2025776 itr]: obj = (0.0880161), mov = 2025776, inf = 99%, acc = 0.5%, imp = 554

worst case efficiency: 0.65281
[63 sec, 2068303 itr]: obj = (0.0880161), mov = 2068303, inf = 99%, acc = 0.5%, imp = 554

worst case efficiency: 0.65281
[64 sec, 2111242 itr]: obj = (0.0880307), mov = 2111242, inf = 99%, acc = 0.5%, imp = 556

worst case efficiency: 0.652818
[65 sec, 2153556 itr]: obj = (0.0880307), mov = 2153556, inf = 99%, acc = 0.5%, imp = 556

worst case efficiency: 0.652818
[66 sec, 2195459 itr]: obj = (0.0880307), mov = 2195459, inf = 99%, acc = 0.5%, imp = 556

worst case efficiency: 0.652818
[67 sec, 2237809 itr]: obj = (0.0880371), mov = 2237809, inf = 99%, acc = 0.5%, imp = 558

worst case efficiency: 0.652822
[68 sec, 2280744 itr]: obj = (0.088065), mov = 2280744, inf = 99%, acc = 0.5%, imp = 560

worst case efficiency: 0.652839
[69 sec, 2323548 itr]: obj = (0.088065), mov = 2323548, inf = 98.9%, acc = 0.5%, imp = 560

worst case efficiency: 0.652839
[70 sec, 2366251 itr]: obj = (0.088065), mov = 2366251, inf = 98.9%, acc = 0.5%, imp = 560

worst case efficiency: 0.652839
[71 sec, 2409767 itr]: obj = (0.0881425), mov = 2409767, inf = 99%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[72 sec, 2452509 itr]: obj = (0.0881425), mov = 2452509, inf = 98.9%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[73 sec, 2494848 itr]: obj = (0.0881425), mov = 2494848, inf = 98.9%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[74 sec, 2537774 itr]: obj = (0.0881425), mov = 2537774, inf = 98.9%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[75 sec, 2581224 itr]: obj = (0.0881425), mov = 2581224, inf = 98.9%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[76 sec, 2624266 itr]: obj = (0.0881425), mov = 2624266, inf = 98.9%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[77 sec, 2667369 itr]: obj = (0.0881425), mov = 2667369, inf = 98.9%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[78 sec, 2709993 itr]: obj = (0.0881425), mov = 2709993, inf = 98.9%, acc = 0.4%, imp = 562

worst case efficiency: 0.652885
[79 sec, 2752415 itr]: obj = (0.0881571), mov = 2752415, inf = 98.9%, acc = 0.4%, imp = 567

worst case efficiency: 0.652894
[80 sec, 2795723 itr]: obj = (0.0881571), mov = 2795723, inf = 98.9%, acc = 0.4%, imp = 567

worst case efficiency: 0.652894
[81 sec, 2838873 itr]: obj = (0.0882511), mov = 2838873, inf = 98.9%, acc = 0.4%, imp = 573

worst case efficiency: 0.652951
[82 sec, 2881501 itr]: obj = (0.0882511), mov = 2881501, inf = 98.9%, acc = 0.4%, imp = 573

worst case efficiency: 0.652951
[83 sec, 2924674 itr]: obj = (0.0883127), mov = 2924674, inf = 98.9%, acc = 0.4%, imp = 577

worst case efficiency: 0.652988
[84 sec, 2967087 itr]: obj = (0.0883127), mov = 2967087, inf = 98.9%, acc = 0.4%, imp = 577

worst case efficiency: 0.652988
[85 sec, 3010486 itr]: obj = (0.0883176), mov = 3010486, inf = 98.9%, acc = 0.4%, imp = 578

worst case efficiency: 0.652991
[86 sec, 3053438 itr]: obj = (0.0883176), mov = 3053438, inf = 98.9%, acc = 0.4%, imp = 579

worst case efficiency: 0.652991
[87 sec, 3096186 itr]: obj = (0.0883583), mov = 3096186, inf = 98.9%, acc = 0.4%, imp = 581

worst case efficiency: 0.653015
[88 sec, 3139256 itr]: obj = (0.0883792), mov = 3139256, inf = 98.9%, acc = 0.4%, imp = 584

worst case efficiency: 0.653027
[89 sec, 3181553 itr]: obj = (0.0883827), mov = 3181553, inf = 98.9%, acc = 0.4%, imp = 586

worst case efficiency: 0.65303
[90 sec, 3224446 itr]: obj = (0.0883876), mov = 3224446, inf = 98.9%, acc = 0.4%, imp = 587

worst case efficiency: 0.653033
[90 sec, 3224446 itr]: obj = (0.0883876), mov = 3224446, inf = 98.9%, acc = 0.4%, imp = 587

worst case efficiency: 0.653033
3224446 iterations, 3224446 moves performed in 90 seconds
Feasible solution: obj = (0.0883876)

Run output...
FINAL SOLUTION
Worst case efficiency: 0.653033
Mapping: 0->47, 1->33, 2->26, 3->28, 4->17, 5->5, 6->12, 7->18, 8->15, 9->20, 10->4, 11->32, 12->23, 13->16, 14->36, 15->28, 16->16, 17->17, 18->45, 19->44, 20->39, 21->45, 22->39, 23->7, 24->19, 25->41, 26->46, 27->7, 28->46, 29->3, 30->44, 31->9, 32->40, 33->40, 34->2, 35->33, 36->37, 37->10, 38->36, 39->4, 40->5, 41->31, 42->42, 43->35, 44->3, 45->47, 46->11, 47->32, 48->43, 49->15, 50->36, 51->17, 52->38, 53->43, 54->32, 55->46, 56->15, 57->0, 58->31, 59->9, 60->18, 61->34, 62->2, 63->42, 64->40, 65->4, 66->46, 67->40, 68->25, 69->34, 70->33, 71->30, 72->23, 73->20, 74->13, 75->44, 76->26, 77->34, 78->20, 79->24, 80->5, 81->34, 82->22, 83->19, 84->43, 85->24, 86->28, 87->37, 88->4, 89->12, 90->6, 91->1, 92->8, 93->3, 94->39, 95->27, 96->14, 97->35, 98->16, 99->31, 100->23, 101->30, 102->15, 103->6, 104->27, 105->32, 106->4, 107->0, 108->41, 109->41, 110->2, 111->19, 112->7, 113->30, 114->7, 115->13, 116->44, 117->24, 118->7, 119->41, 120->39, 121->9, 122->22, 123->23, 124->47, 125->47, 126->3, 127->45, 128->26, 129->41, 130->29, 131->9, 132->16, 133->38, 134->32, 135->28, 136->36, 137->1, 138->8, 139->37, 140->33, 141->42, 142->36, 143->24, 144->0, 145->44, 146->40, 147->31, 148->21, 149->21, 150->27, 151->36, 152->2, 153->35, 154->5, 155->40, 156->30, 157->28, 158->10, 159->21, 160->16, 161->8, 162->9, 163->11, 164->28, 165->11, 166->26, 167->2, 168->3, 169->25, 170->24, 171->45, 172->10, 173->46, 174->14, 175->14, 176->11, 177->14, 178->6, 179->30, 180->22, 181->22, 182->37, 183->38, 184->26, 185->17, 186->38, 187->21, 188->34, 189->13, 190->21, 191->12, 192->18, 193->33, 194->13, 195->46, 196->5, 197->12, 198->23, 199->13, 200->30, 201->12, 202->22, 203->25, 204->15, 205->37, 206->39, 207->12, 208->23, 209->11, 210->1, 211->17, 212->33, 213->18, 214->4, 215->26, 216->6, 217->38, 218->25, 219->15, 220->1, 221->18, 222->1, 223->43, 224->5, 225->10, 226->19, 227->29, 228->22, 229->7, 230->10, 231->37, 232->20, 233->35, 234->32, 235->29, 236->2, 237->45, 238->18, 239->41, 240->17, 241->0, 242->19, 243->10, 244->6, 245->16, 246->21, 247->43, 248->43, 249->42, 250->31, 251->29, 252->27, 253->38, 254->14, 255->45, 256->47, 257->35, 258->14, 259->31, 260->34, 261->42, 262->1, 263->39, 264->0, 265->0, 266->35, 267->13, 268->27, 269->6, 270->3, 271->20, 272->47, 273->19, 274->42, 275->24, 276->9, 277->8, 278->27, 279->44, 280->29, 281->25, 282->8, 283->11, 284->20, 285->25, 286->29, 287->8
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	0.53772	0.867734	0.977425	2.07165	1.71497	1.11003
	1.02144	2.05225	2.58251	1.94128	2.28236	1.42366
	1.99221	2.86832	2.70825	2.01916	2.62687	1.17787
	2.32838	2.56698	2.73922	2.36534	2.1749	0.945848
	1.55577	2.35848	3.5458	3.53762	2.62331	1.45846
	1.14425	3.28723	4.08337	3.47082	2.20896	1.14113
	1.35823	2.70471	2.9266	2.34504	1.84519	1.39074
	0.869539	1.37822	1.7535	1.78534	1.43431	0.697003
Efficiency map:
	0.996735	0.924535	0.839626	0.933401	0.928834	0.995974
	0.780309	0.65797	0.68144	0.653338	0.668653	0.817725
	0.828782	0.653421	0.660062	0.662223	0.654187	0.799827
	0.831161	0.653036	0.657616	0.654177	0.654028	0.79202
	0.803377	0.659215	0.662516	0.661382	0.653596	0.764468
	0.756023	0.665645	0.653033	0.672946	0.653078	0.785811
	0.871815	0.67093	0.668378	0.666818	0.653083	0.77538
	0.798188	0.660529	0.653983	0.653977	0.664237	0.779515
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --global-tlim 90 --seed 901

Starting the Optimization Process
