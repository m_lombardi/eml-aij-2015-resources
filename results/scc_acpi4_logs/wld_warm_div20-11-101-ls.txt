LocalSolver 4.0 (MacOS64, build 20140127)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Average CPI weights for the platform:
 0.01 0.95 1.18 1.22 0.86 0.02 5.23 13.77 13.98 14 13.53 5.18 6.12 15.37 14.75 15.8 15 6.08 6.76 15.7 16.5 16.35 15.54 6.92 6.64 15.14 15.95 16.76 15.72 6.48 6.77 15.37 16.26 16.22 15.08 6.32 6.23 15.19 15.38 15.41 15 6.24 7.22 14.13 15.41 15.59 14.88 7.08

Min CPI weights for the platform:
 -0 -0.19 -0.35 -0.3 -0.19 -0.01 -1.43 -1.59 -0.52 -0.4 -1.18 -1.53 -1.11 -1.28 1.63 0 -0.49 -1.16 -1.93 -0.9 -0 0.23 -0.54 -1.87 -1.62 0.11 0.85 -0.64 -0.74 -1.44 -1.91 -0.74 -0.19 0.01 -0.4 -1.36 -1.75 -1.25 0.14 -0.03 -1.18 -1.73 -2.05 -0.84 -1.57 -1.64 -2 -1.74

Neighbor CPI weights for the platform:
 0.01 0.66 0.62 0.57 0.59 0.03 1.86 2.89 2.24 2.1 2.99 1.83 1.47 2.64 1.51 1.25 2.13 1.62 1.74 1.94 0.92 0.67 1.79 1.48 1.59 1.34 0.85 0.82 1.92 1.56 1.55 1.99 1 1.01 1.67 1.52 1.71 2.17 1.41 0.97 2.06 1.83 0.58 1.28 0.61 0.81 1.22 0.7

Other CPI weights for the platform:
 0.01 0.47 0.55 0.61 0.47 0.02 0.48 0.48 0.68 0.64 0.32 0.28 0.59 0.63 0.61 0.78 0.68 0.63 0.47 0.56 0.4 0.65 0.4 0.51 0.46 0.46 0.63 0.55 0.53 0.42 0.5 0.55 0.2 0.62 0.26 0.35 0.28 0.37 0.46 0.3 0.1 0.27 0.39 0.07 0.26 0.3 0.46 0.29

Workload Information:
0.5, 3.81296, 3.87583, 2.27713, 1.03321, 0.500884, 1.62568, 1.80856, 1.09966, 2.84771, 1.76371, 2.85469, 1.17437, 2.58049, 1.80979, 2.67232, 1.28661, 2.47642, 1.95892, 1.85795, 1.92912, 2.19539, 2.04301, 2.01562, 2.20498, 2.19371, 2.01148, 1.50578, 2.07573, 2.00832, 0.84475, 3.40524, 2.69364, 1.4952, 0.5, 3.06118, 2.70627, 2.36264, 1.8097, 1.10072, 1.22972, 2.79096, 0.647386, 1.61021, 2.33738, 0.5, 1.14249, 5.76253, 1.72541, 2.42155, 1.60517, 1.01494, 3.29655, 1.93639, 4.99356, 1.79488, 1.67445, 0.580958, 2.14963, 0.806525, 2.0154, 2.61515, 1.24017, 1.8084, 1.91464, 2.40624, 0.806722, 1.47745, 2.20499, 3.51697, 3.2042, 0.789676, 0.59456, 0.96265, 2.6067, 2.31696, 4.87612, 0.643004, 3.43285, 0.620382, 1.21686, 3.3258, 1.96387, 1.44024, 0.94898, 0.5, 2.9513, 2.93378, 2.57022, 2.09572, 0.5, 0.803253, 2.6402, 1.36485, 2.10134, 4.59036, 4.34096, 0.763328, 2.03923, 3.41068, 0.675672, 0.770143, 1.67581, 1.22702, 1.90911, 5.07827, 1.60978, 0.5, 0.5, 2.2436, 1.37725, 3.70589, 2.56693, 1.60633, 1.28532, 3.34844, 2.04848, 1.70325, 3.11451, 0.5, 3.8257, 2.66265, 2.54595, 1.1641, 0.5, 1.30161, 2.53083, 2.19387, 2.97221, 2.09707, 1.40411, 0.801911, 2.8914, 2.64037, 3.34792, 2.11958, 0.5, 0.500726, 3.2597, 3.62041, 2.92232, 0.5, 0.666623, 1.03095, 0.5, 2.42712, 1.97779, 3.12511, 1.82064, 2.14934, 4.48429, 1.75809, 1.56529, 1.12895, 1.55094, 1.51243, 1.7981, 2.43541, 2.2263, 2.13666, 1.52789, 1.87564, 3.47137, 0.836303, 3.2841, 0.5, 3.22641, 0.681832, 2.96647, 1.01755, 2.6172, 1.64574, 2.97296, 0.780062, 2.58977, 2.69655, 2.12138, 1.87539, 1.42695, 1.28997, 2.12806, 2.34382, 2.54858, 1.79019, 2.66604, 0.523303, 2.29381, 2.06349, 1.60978, 1.89733, 1.89669, 2.23891, 2.79868, 3.03558, 2.81649, 1.137, 0.5, 1.71225, 1.36767, 2.66207, 0.611308, 2.16961, 2.12982, 3.05952, 2.52021, 2.09371, 2.37688, 2.11109, 0.698712, 2.19939, 0.5, 1.38675, 4.78868, 0.827206, 0.83915, 3.65821, 0.5, 3.40403, 3.53304, 2.72326, 1.07935, 0.760316, 1.82307, 1.24257, 1.66177, 2.36872, 2.60941, 2.29446, 1.9379, 1.82532, 2.37006, 2.32553, 2.08922, 1.45196, 2.46326, 1.26431, 0.5, 1.20221, 5.76015, 0.810084, 0.632594, 0.603053, 3.04023, 1.39346, 0.89507, 5.43559, 4.12522, 1.91597, 0.5, 0.776778, 3.94175, 0.740289, 3.29175, 4.13465, 1.61764, 1.51594, 0.793463, 0.646556, 3.12918, 4.30524, 1.54304, 0.728343, 0.5, 1.7942, 0.5, 0.54231, 1.24576, 3.8789, 4.25341, 1.57962, 1.22202, 4.17073, 0.58695, 2.40113, 0.756322, 2.86285, 0.5, 2.51082, 2.69678, 1.02573, 2.18458, 3.08209, 3.15324, 1.74242, 2.93218, 1.25367, 0.863003, 2.05549
cpi min/max: 0.5/5.76253
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 43151, operands = 114423
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 13812 itr]: obj = (-14.7634), mov = 13812, inf = 43.3%, acc = 19%, imp = 578

worst case avg CPI: -240.86
[2 sec, 66162 itr]: obj = (-14.532), mov = 66162, inf = 23.9%, acc = 15.2%, imp = 1031

worst case avg CPI: -236.81
[3 sec, 116891 itr]: obj = (-14.4468), mov = 116891, inf = 19.4%, acc = 14.7%, imp = 1462

worst case avg CPI: -235.319
[4 sec, 165118 itr]: obj = (-14.4468), mov = 165118, inf = 17.2%, acc = 15.7%, imp = 2029

worst case avg CPI: -235.319
[5 sec, 222934 itr]: obj = (-14.4437), mov = 222934, inf = 15.7%, acc = 15.1%, imp = 2197

worst case avg CPI: -235.265
[6 sec, 290842 itr]: obj = (-14.4329), mov = 290842, inf = 14.6%, acc = 13.8%, imp = 2285

worst case avg CPI: -235.075
[7 sec, 356091 itr]: obj = (-14.4246), mov = 356091, inf = 14%, acc = 13.1%, imp = 2344

worst case avg CPI: -234.931
[8 sec, 425208 itr]: obj = (-14.418), mov = 425208, inf = 13.5%, acc = 12.4%, imp = 2402

worst case avg CPI: -234.815
[9 sec, 497669 itr]: obj = (-14.4159), mov = 497669, inf = 13.1%, acc = 11.7%, imp = 2433

worst case avg CPI: -234.779
[10 sec, 564964 itr]: obj = (-14.4157), mov = 564964, inf = 12.9%, acc = 11.4%, imp = 2440

worst case avg CPI: -234.775
[11 sec, 635006 itr]: obj = (-14.4144), mov = 635006, inf = 12.7%, acc = 11%, imp = 2451

worst case avg CPI: -234.752
[12 sec, 709655 itr]: obj = (-14.4137), mov = 709655, inf = 12.5%, acc = 10.7%, imp = 2458

worst case avg CPI: -234.74
[13 sec, 788836 itr]: obj = (-14.4115), mov = 788836, inf = 12.5%, acc = 10.3%, imp = 2497

worst case avg CPI: -234.701
[14 sec, 867403 itr]: obj = (-14.4098), mov = 867403, inf = 12.4%, acc = 10%, imp = 2522

worst case avg CPI: -234.672
[15 sec, 945824 itr]: obj = (-14.4097), mov = 945824, inf = 12.2%, acc = 9.6%, imp = 2531

worst case avg CPI: -234.67
[16 sec, 1026480 itr]: obj = (-14.4097), mov = 1026480, inf = 12.1%, acc = 9.3%, imp = 2539

worst case avg CPI: -234.67
[17 sec, 1105607 itr]: obj = (-14.4095), mov = 1105607, inf = 12%, acc = 9.1%, imp = 2547

worst case avg CPI: -234.667
[18 sec, 1184443 itr]: obj = (-14.4082), mov = 1184443, inf = 12%, acc = 8.9%, imp = 2563

worst case avg CPI: -234.643
[19 sec, 1262591 itr]: obj = (-14.4082), mov = 1262591, inf = 11.9%, acc = 8.8%, imp = 2565

worst case avg CPI: -234.643
[20 sec, 1341139 itr]: obj = (-14.408), mov = 1341139, inf = 11.9%, acc = 8.6%, imp = 2574

worst case avg CPI: -234.64
[21 sec, 1420218 itr]: obj = (-14.4072), mov = 1420218, inf = 11.9%, acc = 8.5%, imp = 2589

worst case avg CPI: -234.626
[22 sec, 1502128 itr]: obj = (-14.407), mov = 1502128, inf = 11.9%, acc = 8.3%, imp = 2607

worst case avg CPI: -234.622
[23 sec, 1583874 itr]: obj = (-14.407), mov = 1583874, inf = 11.8%, acc = 8.2%, imp = 2609

worst case avg CPI: -234.622
[24 sec, 1666842 itr]: obj = (-14.407), mov = 1666842, inf = 11.8%, acc = 8%, imp = 2616

worst case avg CPI: -234.622
[25 sec, 1748842 itr]: obj = (-14.4069), mov = 1748842, inf = 11.8%, acc = 7.9%, imp = 2626

worst case avg CPI: -234.621
[26 sec, 1828592 itr]: obj = (-14.4069), mov = 1828592, inf = 11.8%, acc = 7.8%, imp = 2635

worst case avg CPI: -234.621
[27 sec, 1910406 itr]: obj = (-14.4069), mov = 1910406, inf = 11.7%, acc = 7.7%, imp = 2643

worst case avg CPI: -234.621
[28 sec, 1992082 itr]: obj = (-14.4069), mov = 1992082, inf = 11.7%, acc = 7.7%, imp = 2647

worst case avg CPI: -234.621
[29 sec, 2074700 itr]: obj = (-14.4069), mov = 2074700, inf = 11.7%, acc = 7.6%, imp = 2652

worst case avg CPI: -234.621
[30 sec, 2155355 itr]: obj = (-14.4069), mov = 2155355, inf = 11.7%, acc = 7.5%, imp = 2659

worst case avg CPI: -234.621
[31 sec, 2237168 itr]: obj = (-14.4069), mov = 2237168, inf = 11.7%, acc = 7.4%, imp = 2665

worst case avg CPI: -234.621
[32 sec, 2319447 itr]: obj = (-14.4069), mov = 2319447, inf = 11.6%, acc = 7.3%, imp = 2672

worst case avg CPI: -234.621
[33 sec, 2404224 itr]: obj = (-14.4069), mov = 2404224, inf = 11.6%, acc = 7.3%, imp = 2682

worst case avg CPI: -234.62
[34 sec, 2488124 itr]: obj = (-14.4069), mov = 2488124, inf = 11.6%, acc = 7.2%, imp = 2685

worst case avg CPI: -234.62
[35 sec, 2571793 itr]: obj = (-14.4069), mov = 2571793, inf = 11.6%, acc = 7.1%, imp = 2698

worst case avg CPI: -234.62
[36 sec, 2652680 itr]: obj = (-14.4068), mov = 2652680, inf = 11.6%, acc = 7.1%, imp = 2710

worst case avg CPI: -234.619
[37 sec, 2733733 itr]: obj = (-14.4068), mov = 2733733, inf = 11.6%, acc = 7%, imp = 2713

worst case avg CPI: -234.619
[38 sec, 2812577 itr]: obj = (-14.4068), mov = 2812577, inf = 11.6%, acc = 7%, imp = 2715

worst case avg CPI: -234.619
[39 sec, 2882066 itr]: obj = (-14.4068), mov = 2882066, inf = 11.6%, acc = 7%, imp = 2721

worst case avg CPI: -234.619
[40 sec, 2952820 itr]: obj = (-14.4068), mov = 2952820, inf = 11.6%, acc = 7%, imp = 2729

worst case avg CPI: -234.619
[41 sec, 3030585 itr]: obj = (-14.4068), mov = 3030585, inf = 11.6%, acc = 6.9%, imp = 2735

worst case avg CPI: -234.619
[42 sec, 3111796 itr]: obj = (-14.4068), mov = 3111796, inf = 11.6%, acc = 6.9%, imp = 2739

worst case avg CPI: -234.619
[43 sec, 3191935 itr]: obj = (-14.4068), mov = 3191935, inf = 11.6%, acc = 6.9%, imp = 2742

worst case avg CPI: -234.619
[44 sec, 3269837 itr]: obj = (-14.4068), mov = 3269837, inf = 11.6%, acc = 6.9%, imp = 2748

worst case avg CPI: -234.619
[45 sec, 3343467 itr]: obj = (-14.4068), mov = 3343467, inf = 11.6%, acc = 6.8%, imp = 2754

worst case avg CPI: -234.619
[46 sec, 3419833 itr]: obj = (-14.4068), mov = 3419833, inf = 11.6%, acc = 6.8%, imp = 2761

worst case avg CPI: -234.619
[47 sec, 3500663 itr]: obj = (-14.4068), mov = 3500663, inf = 11.6%, acc = 6.8%, imp = 2766

worst case avg CPI: -234.619
[48 sec, 3576275 itr]: obj = (-14.4068), mov = 3576275, inf = 11.5%, acc = 6.8%, imp = 2774

worst case avg CPI: -234.618
[49 sec, 3641890 itr]: obj = (-14.4067), mov = 3641890, inf = 11.5%, acc = 6.8%, imp = 2779

worst case avg CPI: -234.617
[50 sec, 3709517 itr]: obj = (-14.4066), mov = 3709517, inf = 11.5%, acc = 6.8%, imp = 2786

worst case avg CPI: -234.616
[51 sec, 3790409 itr]: obj = (-14.4066), mov = 3790409, inf = 11.5%, acc = 6.7%, imp = 2791

worst case avg CPI: -234.616
[52 sec, 3873617 itr]: obj = (-14.4066), mov = 3873617, inf = 11.4%, acc = 6.7%, imp = 2796

worst case avg CPI: -234.616
[53 sec, 3952166 itr]: obj = (-14.4065), mov = 3952166, inf = 11.4%, acc = 6.7%, imp = 2808

worst case avg CPI: -234.614
[54 sec, 4029817 itr]: obj = (-14.406), mov = 4029817, inf = 11.4%, acc = 6.7%, imp = 2814

worst case avg CPI: -234.604
[55 sec, 4107698 itr]: obj = (-14.406), mov = 4107698, inf = 11.4%, acc = 6.6%, imp = 2822

worst case avg CPI: -234.604
[56 sec, 4191856 itr]: obj = (-14.406), mov = 4191856, inf = 11.4%, acc = 6.6%, imp = 2833

worst case avg CPI: -234.604
[57 sec, 4277362 itr]: obj = (-14.406), mov = 4277362, inf = 11.4%, acc = 6.6%, imp = 2840

worst case avg CPI: -234.604
[58 sec, 4360908 itr]: obj = (-14.4058), mov = 4360908, inf = 11.3%, acc = 6.5%, imp = 2852

worst case avg CPI: -234.602
[59 sec, 4441025 itr]: obj = (-14.4058), mov = 4441025, inf = 11.3%, acc = 6.5%, imp = 2861

worst case avg CPI: -234.601
[60 sec, 4517127 itr]: obj = (-14.4058), mov = 4517127, inf = 11.3%, acc = 6.5%, imp = 2867

worst case avg CPI: -234.601
[61 sec, 4593958 itr]: obj = (-14.4058), mov = 4593958, inf = 11.3%, acc = 6.5%, imp = 2878

worst case avg CPI: -234.601
[62 sec, 4678639 itr]: obj = (-14.4056), mov = 4678639, inf = 11.3%, acc = 6.4%, imp = 2888

worst case avg CPI: -234.599
[63 sec, 4762941 itr]: obj = (-14.4056), mov = 4762941, inf = 11.3%, acc = 6.4%, imp = 2896

worst case avg CPI: -234.599
[64 sec, 4838577 itr]: obj = (-14.4056), mov = 4838577, inf = 11.3%, acc = 6.4%, imp = 2907

worst case avg CPI: -234.599
[65 sec, 4921322 itr]: obj = (-14.4056), mov = 4921322, inf = 11.2%, acc = 6.4%, imp = 2913

worst case avg CPI: -234.599
[66 sec, 5001839 itr]: obj = (-14.4056), mov = 5001839, inf = 11.2%, acc = 6.4%, imp = 2930

worst case avg CPI: -234.599
[67 sec, 5084929 itr]: obj = (-14.4055), mov = 5084929, inf = 11.2%, acc = 6.4%, imp = 2944

worst case avg CPI: -234.596
[68 sec, 5168195 itr]: obj = (-14.4054), mov = 5168195, inf = 11.2%, acc = 6.3%, imp = 2952

worst case avg CPI: -234.595
[69 sec, 5250988 itr]: obj = (-14.4054), mov = 5250988, inf = 11.2%, acc = 6.3%, imp = 2961

worst case avg CPI: -234.595
[70 sec, 5333386 itr]: obj = (-14.4054), mov = 5333386, inf = 11.2%, acc = 6.3%, imp = 2967

worst case avg CPI: -234.595
[71 sec, 5417840 itr]: obj = (-14.4054), mov = 5417840, inf = 11.2%, acc = 6.3%, imp = 2975

worst case avg CPI: -234.595
[72 sec, 5504803 itr]: obj = (-14.4054), mov = 5504803, inf = 11.2%, acc = 6.3%, imp = 2986

worst case avg CPI: -234.595
[73 sec, 5587857 itr]: obj = (-14.4054), mov = 5587857, inf = 11.2%, acc = 6.2%, imp = 2999

worst case avg CPI: -234.595
[74 sec, 5672548 itr]: obj = (-14.4054), mov = 5672548, inf = 11.2%, acc = 6.2%, imp = 3012

worst case avg CPI: -234.595
[75 sec, 5755894 itr]: obj = (-14.4054), mov = 5755894, inf = 11.2%, acc = 6.2%, imp = 3018

worst case avg CPI: -234.595
[76 sec, 5842078 itr]: obj = (-14.4053), mov = 5842078, inf = 11.2%, acc = 6.2%, imp = 3026

worst case avg CPI: -234.592
[77 sec, 5927723 itr]: obj = (-14.4053), mov = 5927723, inf = 11.2%, acc = 6.2%, imp = 3031

worst case avg CPI: -234.592
[78 sec, 6012023 itr]: obj = (-14.4053), mov = 6012023, inf = 11.2%, acc = 6.1%, imp = 3040

worst case avg CPI: -234.592
[79 sec, 6096663 itr]: obj = (-14.4048), mov = 6096663, inf = 11.2%, acc = 6.1%, imp = 3058

worst case avg CPI: -234.584
[80 sec, 6183544 itr]: obj = (-14.4046), mov = 6183544, inf = 11.2%, acc = 6.1%, imp = 3074

worst case avg CPI: -234.581
[81 sec, 6267724 itr]: obj = (-14.4046), mov = 6267724, inf = 11.2%, acc = 6.1%, imp = 3083

worst case avg CPI: -234.581
[82 sec, 6353635 itr]: obj = (-14.4046), mov = 6353635, inf = 11.1%, acc = 6.1%, imp = 3095

worst case avg CPI: -234.581
[83 sec, 6440407 itr]: obj = (-14.4046), mov = 6440407, inf = 11.1%, acc = 6%, imp = 3105

worst case avg CPI: -234.581
[84 sec, 6527646 itr]: obj = (-14.4046), mov = 6527646, inf = 11.1%, acc = 6%, imp = 3115

worst case avg CPI: -234.581
[85 sec, 6615792 itr]: obj = (-14.4046), mov = 6615792, inf = 11.1%, acc = 6%, imp = 3127

worst case avg CPI: -234.581
[86 sec, 6702341 itr]: obj = (-14.4046), mov = 6702341, inf = 11.1%, acc = 6%, imp = 3134

worst case avg CPI: -234.581
[87 sec, 6788171 itr]: obj = (-14.4046), mov = 6788171, inf = 11.1%, acc = 6%, imp = 3146

worst case avg CPI: -234.581
[88 sec, 6870892 itr]: obj = (-14.4046), mov = 6870892, inf = 11.1%, acc = 6%, imp = 3159

worst case avg CPI: -234.581
[89 sec, 6950736 itr]: obj = (-14.4046), mov = 6950736, inf = 11.1%, acc = 5.9%, imp = 3161

worst case avg CPI: -234.581
[90 sec, 7039175 itr]: obj = (-14.4046), mov = 7039175, inf = 11.1%, acc = 5.9%, imp = 3170

worst case avg CPI: -234.581
[90 sec, 7039175 itr]: obj = (-14.4046), mov = 7039175, inf = 11.1%, acc = 5.9%, imp = 3170

worst case avg CPI: -234.581
7039175 iterations, 7039175 moves performed in 90 seconds
Feasible solution: obj = (-14.4046)

Run output...
FINAL SOLUTION
Worst case avg CPI: -234.581
Mapping: 0->46, 1->26, 2->22, 3->34, 4->29, 5->41, 6->17, 7->7, 8->3, 9->19, 10->24, 11->39, 12->2, 13->38, 14->7, 15->33, 16->29, 17->21, 18->10, 19->35, 20->36, 21->37, 22->18, 23->30, 24->37, 25->37, 26->40, 27->40, 28->7, 29->9, 30->4, 31->28, 32->19, 33->12, 34->42, 35->20, 36->38, 37->32, 38->18, 39->44, 40->45, 41->13, 42->6, 43->40, 44->13, 45->4, 46->44, 47->14, 48->40, 49->19, 50->23, 51->2, 52->38, 53->11, 54->15, 55->11, 56->43, 57->5, 58->8, 59->6, 60->10, 61->22, 62->1, 63->18, 64->30, 65->28, 66->47, 67->12, 68->39, 69->32, 70->27, 71->41, 72->41, 73->2, 74->13, 75->39, 76->21, 77->5, 78->26, 79->6, 80->17, 81->21, 82->30, 83->36, 84->47, 85->3, 86->15, 87->27, 88->38, 89->8, 90->6, 91->0, 92->20, 93->12, 94->9, 95->15, 96->20, 97->4, 98->24, 99->16, 100->44, 101->47, 102->23, 103->43, 104->36, 105->21, 106->17, 107->1, 108->47, 109->28, 110->10, 111->32, 112->32, 113->45, 114->35, 115->19, 116->8, 117->29, 118->15, 119->0, 120->26, 121->22, 122->32, 123->44, 124->45, 125->23, 126->31, 127->34, 128->16, 129->18, 130->46, 131->47, 132->20, 133->16, 134->16, 135->9, 136->42, 137->2, 138->19, 139->33, 140->21, 141->46, 142->6, 143->1, 144->4, 145->15, 146->9, 147->19, 148->10, 149->17, 150->38, 151->18, 152->45, 153->44, 154->30, 155->4, 156->11, 157->27, 158->34, 159->8, 160->7, 161->29, 162->13, 163->42, 164->20, 165->35, 166->28, 167->42, 168->13, 169->30, 170->25, 171->17, 172->31, 173->1, 174->25, 175->32, 176->11, 177->10, 178->24, 179->45, 180->18, 181->21, 182->34, 183->11, 184->22, 185->42, 186->39, 187->9, 188->40, 189->23, 190->23, 191->39, 192->22, 193->38, 194->16, 195->43, 196->0, 197->23, 198->43, 199->25, 200->1, 201->8, 202->8, 203->14, 204->33, 205->10, 206->34, 207->9, 208->6, 209->37, 210->0, 211->17, 212->27, 213->45, 214->42, 215->14, 216->0, 217->31, 218->14, 219->15, 220->43, 221->4, 222->36, 223->43, 224->12, 225->37, 226->22, 227->31, 228->24, 229->12, 230->39, 231->27, 232->36, 233->44, 234->33, 235->7, 236->46, 237->35, 238->14, 239->2, 240->5, 241->0, 242->28, 243->24, 244->46, 245->33, 246->20, 247->24, 248->41, 249->5, 250->26, 251->5, 252->31, 253->26, 254->35, 255->29, 256->3, 257->35, 258->25, 259->28, 260->30, 261->5, 262->1, 263->29, 264->2, 265->3, 266->11, 267->26, 268->13, 269->3, 270->47, 271->33, 272->46, 273->34, 274->3, 275->27, 276->41, 277->25, 278->31, 279->12, 280->37, 281->16, 282->14, 283->7, 284->25, 285->36, 286->41, 287->40
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	0.567718	0.777081	0.827129	0.878563	0.81347	0.683661
	0.656605	1.70478	2.12165	2.0636	1.85688	1.78112
	1.47505	3.07105	4.15445	3.46668	3.04496	1.60569
	1.94072	2.94936	3.39051	3.50379	2.87129	1.68148
	1.7462	2.7402	3.8378	3.09174	3.10449	1.53481
	1.66761	2.86513	2.89915	3.48042	2.33731	1.18495
	1.74074	2.22446	3.11223	2.3799	1.75303	0.624687
	0.646765	1.28801	1.11065	1.16975	0.731021	0.841629
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --global-tlim 90 --seed 101 --use-acpi-model

Starting the Optimization Process
