LocalSolver 4.0 (MacOS64, build 20140127)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Average CPI weights for the platform:
 0.01 0.95 1.18 1.22 0.86 0.02 5.23 13.77 13.98 14 13.53 5.18 6.12 15.37 14.75 15.8 15 6.08 6.76 15.7 16.5 16.35 15.54 6.92 6.64 15.14 15.95 16.76 15.72 6.48 6.77 15.37 16.26 16.22 15.08 6.32 6.23 15.19 15.38 15.41 15 6.24 7.22 14.13 15.41 15.59 14.88 7.08

Min CPI weights for the platform:
 -0 -0.19 -0.35 -0.3 -0.19 -0.01 -1.43 -1.59 -0.52 -0.4 -1.18 -1.53 -1.11 -1.28 1.63 0 -0.49 -1.16 -1.93 -0.9 -0 0.23 -0.54 -1.87 -1.62 0.11 0.85 -0.64 -0.74 -1.44 -1.91 -0.74 -0.19 0.01 -0.4 -1.36 -1.75 -1.25 0.14 -0.03 -1.18 -1.73 -2.05 -0.84 -1.57 -1.64 -2 -1.74

Neighbor CPI weights for the platform:
 0.01 0.66 0.62 0.57 0.59 0.03 1.86 2.89 2.24 2.1 2.99 1.83 1.47 2.64 1.51 1.25 2.13 1.62 1.74 1.94 0.92 0.67 1.79 1.48 1.59 1.34 0.85 0.82 1.92 1.56 1.55 1.99 1 1.01 1.67 1.52 1.71 2.17 1.41 0.97 2.06 1.83 0.58 1.28 0.61 0.81 1.22 0.7

Other CPI weights for the platform:
 0.01 0.47 0.55 0.61 0.47 0.02 0.48 0.48 0.68 0.64 0.32 0.28 0.59 0.63 0.61 0.78 0.68 0.63 0.47 0.56 0.4 0.65 0.4 0.51 0.46 0.46 0.63 0.55 0.53 0.42 0.5 0.55 0.2 0.62 0.26 0.35 0.28 0.37 0.46 0.3 0.1 0.27 0.39 0.07 0.26 0.3 0.46 0.29

Workload Information:
0.5, 0.793797, 3.01448, 2.89409, 2.97166, 1.82598, 1.46422, 1.02987, 4.33222, 3.15438, 0.5, 1.51931, 2.09408, 1.95425, 1.74965, 2.13165, 2.02029, 2.05007, 2.40715, 0.751014, 2.93856, 1.5686, 1.81978, 2.51489, 0.810358, 3.37159, 2.01811, 0.5, 3.8527, 1.44724, 1.43318, 2.33024, 1.63724, 1.9352, 2.17271, 2.49144, 2.62915, 2.43595, 1.70695, 1.6611, 2.1134, 1.45345, 2.13604, 2.94479, 1.10006, 2.21292, 2.90612, 0.700066, 2.24627, 2.07499, 0.712951, 3.80092, 1.44741, 1.71746, 1.98396, 2.25489, 2.0251, 1.72142, 1.96386, 2.05077, 0.724777, 5.76477, 1.8992, 2.38764, 0.513167, 0.710449, 31.2107, 27.2561, 18.7677, 35, 11.0826, 8.68286, 1.51957, 3.17254, 0.5, 1.02075, 1.81243, 3.9747, 1.72133, 0.667203, 2.55119, 2.14292, 2.30777, 2.60958, 0.631906, 2.19898, 4.19696, 1.52562, 0.851563, 2.59497, 3.36286, 3.496, 0.975857, 1.57842, 2.08687, 0.5, 28.3048, 15.5874, 35, 25.0792, 12.9663, 15.0623, 1.94871, 2.30428, 2.28502, 1.8023, 1.39475, 2.26493, 3.11261, 1.48502, 2.7242, 1.50319, 2.67498, 0.5, 0.94498, 1.14016, 2.84732, 3.11272, 2.697, 1.25782, 1.49547, 2.60328, 2.37214, 1.91276, 1.0462, 2.57016, 0.556693, 2.96248, 3.93222, 0.5, 1.54412, 2.50448, 20.8662, 35, 16.4223, 16.63, 24.1447, 18.937, 35, 18.8404, 13.9087, 21.4826, 32.0696, 10.6987, 2.03447, 3.84498, 3.47505, 0.840083, 1.07748, 0.727933, 3.5496, 3.73055, 1.03076, 0.5, 2.33398, 0.855113, 1.93585, 0.915647, 0.553338, 1.61452, 1.35227, 5.62837, 3.13578, 3.41479, 0.603717, 1.40872, 0.5, 2.93699, 1.64791, 2.04324, 0.971774, 0.682902, 1.77925, 4.87493, 0.679022, 1.01475, 0.78205, 2.11252, 0.933492, 6.47816, 1.34254, 1.35093, 0.5, 3.04181, 3.12938, 2.63533, 0.5, 1.80789, 4.25635, 2.83402, 0.859458, 1.74228, 2.29634, 1.85749, 1.87562, 2.42096, 1.42836, 2.12123, 3.7915, 2.21285, 0.575129, 0.780089, 4.14043, 0.5, 0.5, 0.855756, 1.6246, 2.73087, 3.7941, 2.49466, 3.98145, 0.5, 1.82333, 1.23156, 2.84167, 1.62198, 4.5138, 2.24199, 1.19583, 2.07482, 0.739494, 1.23407, 1.85496, 0.557577, 2.51568, 1.41475, 4.12757, 1.52947, 1.92157, 2.25752, 1.90951, 2.10541, 1.70878, 2.09721, 0.957288, 0.997952, 2.95043, 3.22083, 3.20122, 0.672285, 3.6616, 3.14231, 2.4321, 1.26004, 0.5, 1.00395, 0.996237, 3.61131, 0.889852, 2.02482, 2.91463, 1.56314, 0.5, 2.30284, 5.09568, 1.76483, 0.697285, 1.63936, 2.35738, 1.92499, 1.23851, 2.49156, 2.46238, 1.52517, 2.20721, 2.20715, 1.75975, 2.13094, 1.83949, 1.85546, 2.53771, 1.97268, 2.60817, 0.5, 2.55362, 1.82783, 0.623567, 3.18571, 0.742433, 2.03774, 0.739147, 4.6714, 1.30779, 2.98554, 3.00575, 2.50208, 1.69884, 0.5
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 43150, operands = 114423
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.041274


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 11402 itr]: obj = (-13.3874), mov = 11402, inf = 39.4%, acc = 26.3%, imp = 626

worst case avg CPI: -216.779
[2 sec, 48322 itr]: obj = (-12.4757), mov = 48322, inf = 21.4%, acc = 26.5%, imp = 1350

worst case avg CPI: -200.824
[3 sec, 95712 itr]: obj = (-12.308), mov = 95712, inf = 17.5%, acc = 22.3%, imp = 1600

worst case avg CPI: -197.889
[4 sec, 135259 itr]: obj = (-12.3027), mov = 135259, inf = 16.3%, acc = 22.6%, imp = 2490

worst case avg CPI: -197.796
[5 sec, 182651 itr]: obj = (-12.3027), mov = 182651, inf = 15.2%, acc = 21.2%, imp = 3103

worst case avg CPI: -197.796
[6 sec, 235234 itr]: obj = (-12.3027), mov = 235234, inf = 14.6%, acc = 19.6%, imp = 3205

worst case avg CPI: -197.796
[7 sec, 287734 itr]: obj = (-12.0685), mov = 287734, inf = 14.2%, acc = 18.8%, imp = 3558

worst case avg CPI: -193.699
[8 sec, 345822 itr]: obj = (-12.0211), mov = 345822, inf = 13.6%, acc = 17.5%, imp = 3708

worst case avg CPI: -192.869
[9 sec, 413898 itr]: obj = (-11.9789), mov = 413898, inf = 13.3%, acc = 15.9%, imp = 3901

worst case avg CPI: -192.131
[10 sec, 482132 itr]: obj = (-11.9749), mov = 482132, inf = 13%, acc = 14.7%, imp = 3937

worst case avg CPI: -192.061
[11 sec, 556907 itr]: obj = (-11.9497), mov = 556907, inf = 13%, acc = 13.5%, imp = 4082

worst case avg CPI: -191.621
[12 sec, 641712 itr]: obj = (-11.9478), mov = 641712, inf = 13%, acc = 12.1%, imp = 4123

worst case avg CPI: -191.586
[13 sec, 723031 itr]: obj = (-11.9419), mov = 723031, inf = 12.9%, acc = 11.2%, imp = 4194

worst case avg CPI: -191.483
[14 sec, 806927 itr]: obj = (-11.9391), mov = 806927, inf = 13%, acc = 10.4%, imp = 4247

worst case avg CPI: -191.435
[15 sec, 889838 itr]: obj = (-11.9338), mov = 889838, inf = 13%, acc = 9.8%, imp = 4317

worst case avg CPI: -191.341
[16 sec, 972254 itr]: obj = (-11.9311), mov = 972254, inf = 13.1%, acc = 9.2%, imp = 4368

worst case avg CPI: -191.294
[17 sec, 1056574 itr]: obj = (-11.9301), mov = 1056574, inf = 13.1%, acc = 8.8%, imp = 4391

worst case avg CPI: -191.277
[18 sec, 1131088 itr]: obj = (-11.9295), mov = 1131088, inf = 13%, acc = 8.4%, imp = 4409

worst case avg CPI: -191.267
[19 sec, 1209343 itr]: obj = (-11.929), mov = 1209343, inf = 13%, acc = 8%, imp = 4429

worst case avg CPI: -191.257
[20 sec, 1289464 itr]: obj = (-11.9281), mov = 1289464, inf = 12.9%, acc = 7.7%, imp = 4456

worst case avg CPI: -191.242
[21 sec, 1368281 itr]: obj = (-11.9271), mov = 1368281, inf = 12.8%, acc = 7.4%, imp = 4482

worst case avg CPI: -191.225
[22 sec, 1450983 itr]: obj = (-11.927), mov = 1450983, inf = 12.8%, acc = 7.1%, imp = 4494

worst case avg CPI: -191.222
[23 sec, 1531822 itr]: obj = (-11.9267), mov = 1531822, inf = 12.7%, acc = 6.9%, imp = 4507

worst case avg CPI: -191.218
[24 sec, 1605976 itr]: obj = (-11.9266), mov = 1605976, inf = 12.7%, acc = 6.7%, imp = 4512

worst case avg CPI: -191.216
[25 sec, 1685341 itr]: obj = (-11.9264), mov = 1685341, inf = 12.6%, acc = 6.5%, imp = 4520

worst case avg CPI: -191.212
[26 sec, 1760668 itr]: obj = (-11.9262), mov = 1760668, inf = 12.6%, acc = 6.3%, imp = 4530

worst case avg CPI: -191.208
[27 sec, 1844171 itr]: obj = (-11.9262), mov = 1844171, inf = 12.6%, acc = 6.1%, imp = 4536

worst case avg CPI: -191.208
[28 sec, 1916392 itr]: obj = (-11.9261), mov = 1916392, inf = 12.6%, acc = 6%, imp = 4543

worst case avg CPI: -191.207
[29 sec, 1993722 itr]: obj = (-11.9259), mov = 1993722, inf = 12.6%, acc = 5.9%, imp = 4552

worst case avg CPI: -191.203
[30 sec, 2068112 itr]: obj = (-11.9243), mov = 2068112, inf = 12.6%, acc = 5.8%, imp = 4580

worst case avg CPI: -191.176
[31 sec, 2151120 itr]: obj = (-11.9237), mov = 2151120, inf = 12.6%, acc = 5.6%, imp = 4605

worst case avg CPI: -191.165
[32 sec, 2234223 itr]: obj = (-11.9231), mov = 2234223, inf = 12.6%, acc = 5.5%, imp = 4627

worst case avg CPI: -191.154
[33 sec, 2318504 itr]: obj = (-11.9221), mov = 2318504, inf = 12.6%, acc = 5.4%, imp = 4647

worst case avg CPI: -191.137
[34 sec, 2396208 itr]: obj = (-11.922), mov = 2396208, inf = 12.6%, acc = 5.3%, imp = 4655

worst case avg CPI: -191.135
[35 sec, 2474615 itr]: obj = (-11.9219), mov = 2474615, inf = 12.6%, acc = 5.2%, imp = 4668

worst case avg CPI: -191.133
[36 sec, 2553340 itr]: obj = (-11.9218), mov = 2553340, inf = 12.6%, acc = 5.1%, imp = 4673

worst case avg CPI: -191.132
[37 sec, 2631525 itr]: obj = (-11.9214), mov = 2631525, inf = 12.6%, acc = 5.1%, imp = 4690

worst case avg CPI: -191.124
[38 sec, 2707881 itr]: obj = (-11.9214), mov = 2707881, inf = 12.6%, acc = 5%, imp = 4697

worst case avg CPI: -191.124
[39 sec, 2785663 itr]: obj = (-11.9206), mov = 2785663, inf = 12.6%, acc = 5%, imp = 4722

worst case avg CPI: -191.111
[40 sec, 2864811 itr]: obj = (-11.9202), mov = 2864811, inf = 12.6%, acc = 4.9%, imp = 4739

worst case avg CPI: -191.104
[41 sec, 2943874 itr]: obj = (-11.92), mov = 2943874, inf = 12.5%, acc = 4.9%, imp = 4755

worst case avg CPI: -191.1
[42 sec, 3023838 itr]: obj = (-11.9195), mov = 3023838, inf = 12.5%, acc = 4.8%, imp = 4770

worst case avg CPI: -191.092
[43 sec, 3104346 itr]: obj = (-11.9194), mov = 3104346, inf = 12.5%, acc = 4.8%, imp = 4781

worst case avg CPI: -191.089
[44 sec, 3175388 itr]: obj = (-11.9192), mov = 3175388, inf = 12.5%, acc = 4.7%, imp = 4793

worst case avg CPI: -191.085
[45 sec, 3255062 itr]: obj = (-11.9177), mov = 3255062, inf = 12.4%, acc = 4.7%, imp = 4828

worst case avg CPI: -191.06
[46 sec, 3333260 itr]: obj = (-11.9171), mov = 3333260, inf = 12.4%, acc = 4.7%, imp = 4847

worst case avg CPI: -191.049
[47 sec, 3405151 itr]: obj = (-11.9164), mov = 3405151, inf = 12.4%, acc = 4.6%, imp = 4872

worst case avg CPI: -191.036
[48 sec, 3478200 itr]: obj = (-11.9157), mov = 3478200, inf = 12.4%, acc = 4.6%, imp = 4901

worst case avg CPI: -191.025
[49 sec, 3549927 itr]: obj = (-11.9151), mov = 3549927, inf = 12.4%, acc = 4.6%, imp = 4916

worst case avg CPI: -191.015
[50 sec, 3619234 itr]: obj = (-11.9128), mov = 3619234, inf = 12.4%, acc = 4.6%, imp = 4955

worst case avg CPI: -190.975
[51 sec, 3691147 itr]: obj = (-11.9123), mov = 3691147, inf = 12.3%, acc = 4.5%, imp = 4981

worst case avg CPI: -190.966
[52 sec, 3769682 itr]: obj = (-11.9118), mov = 3769682, inf = 12.3%, acc = 4.5%, imp = 4995

worst case avg CPI: -190.957
[53 sec, 3850562 itr]: obj = (-11.9118), mov = 3850562, inf = 12.3%, acc = 4.5%, imp = 5009

worst case avg CPI: -190.956
[54 sec, 3927130 itr]: obj = (-11.9118), mov = 3927130, inf = 12.3%, acc = 4.5%, imp = 5026

worst case avg CPI: -190.956
[55 sec, 4007956 itr]: obj = (-11.9118), mov = 4007956, inf = 12.3%, acc = 4.5%, imp = 5041

worst case avg CPI: -190.956
[56 sec, 4082799 itr]: obj = (-11.9108), mov = 4082799, inf = 12.3%, acc = 4.5%, imp = 5064

worst case avg CPI: -190.94
[57 sec, 4167713 itr]: obj = (-11.9107), mov = 4167713, inf = 12.3%, acc = 4.5%, imp = 5079

worst case avg CPI: -190.937
[58 sec, 4253435 itr]: obj = (-11.9095), mov = 4253435, inf = 12.2%, acc = 4.4%, imp = 5116

worst case avg CPI: -190.916
[59 sec, 4339230 itr]: obj = (-11.9091), mov = 4339230, inf = 12.2%, acc = 4.4%, imp = 5141

worst case avg CPI: -190.909
[60 sec, 4418875 itr]: obj = (-11.9084), mov = 4418875, inf = 12.2%, acc = 4.4%, imp = 5173

worst case avg CPI: -190.898
[61 sec, 4503232 itr]: obj = (-11.9084), mov = 4503232, inf = 12.2%, acc = 4.4%, imp = 5189

worst case avg CPI: -190.897
[62 sec, 4585252 itr]: obj = (-11.9079), mov = 4585252, inf = 12.2%, acc = 4.4%, imp = 5214

worst case avg CPI: -190.889
[63 sec, 4665824 itr]: obj = (-11.9078), mov = 4665824, inf = 12.2%, acc = 4.4%, imp = 5219

worst case avg CPI: -190.887
[64 sec, 4746561 itr]: obj = (-11.9078), mov = 4746561, inf = 12.2%, acc = 4.4%, imp = 5229

worst case avg CPI: -190.887
[65 sec, 4828521 itr]: obj = (-11.9078), mov = 4828521, inf = 12.2%, acc = 4.3%, imp = 5234

worst case avg CPI: -190.887
[66 sec, 4913331 itr]: obj = (-11.9078), mov = 4913331, inf = 12.1%, acc = 4.3%, imp = 5254

worst case avg CPI: -190.886
[67 sec, 4999734 itr]: obj = (-11.9078), mov = 4999734, inf = 12.1%, acc = 4.3%, imp = 5266

worst case avg CPI: -190.886
[68 sec, 5085949 itr]: obj = (-11.9078), mov = 5085949, inf = 12.1%, acc = 4.3%, imp = 5277

worst case avg CPI: -190.886
[69 sec, 5166278 itr]: obj = (-11.9076), mov = 5166278, inf = 12.1%, acc = 4.3%, imp = 5303

worst case avg CPI: -190.882
[70 sec, 5243419 itr]: obj = (-11.9075), mov = 5243419, inf = 12.1%, acc = 4.3%, imp = 5321

worst case avg CPI: -190.882
[71 sec, 5325313 itr]: obj = (-11.9075), mov = 5325313, inf = 12.1%, acc = 4.3%, imp = 5337

worst case avg CPI: -190.881
[72 sec, 5407136 itr]: obj = (-11.9075), mov = 5407136, inf = 12.1%, acc = 4.2%, imp = 5342

worst case avg CPI: -190.881
[73 sec, 5490547 itr]: obj = (-11.9075), mov = 5490547, inf = 12.1%, acc = 4.2%, imp = 5351

worst case avg CPI: -190.881
[74 sec, 5578223 itr]: obj = (-11.9075), mov = 5578223, inf = 12%, acc = 4.2%, imp = 5360

worst case avg CPI: -190.881
[75 sec, 5656911 itr]: obj = (-11.9075), mov = 5656911, inf = 12%, acc = 4.2%, imp = 5376

worst case avg CPI: -190.881
[76 sec, 5738708 itr]: obj = (-11.9075), mov = 5738708, inf = 12%, acc = 4.2%, imp = 5382

worst case avg CPI: -190.881
[77 sec, 5816377 itr]: obj = (-11.9075), mov = 5816377, inf = 12%, acc = 4.2%, imp = 5389

worst case avg CPI: -190.881
[78 sec, 5888606 itr]: obj = (-11.9075), mov = 5888606, inf = 12%, acc = 4.1%, imp = 5402

worst case avg CPI: -190.881
[79 sec, 5964606 itr]: obj = (-11.9075), mov = 5964606, inf = 11.9%, acc = 4.1%, imp = 5419

worst case avg CPI: -190.881
[80 sec, 6046436 itr]: obj = (-11.9075), mov = 6046436, inf = 11.9%, acc = 4.1%, imp = 5437

worst case avg CPI: -190.881
[81 sec, 6120998 itr]: obj = (-11.9075), mov = 6120998, inf = 11.9%, acc = 4.1%, imp = 5446

worst case avg CPI: -190.881
[82 sec, 6193703 itr]: obj = (-11.9075), mov = 6193703, inf = 11.9%, acc = 4.1%, imp = 5459

worst case avg CPI: -190.881
[83 sec, 6266656 itr]: obj = (-11.9075), mov = 6266656, inf = 11.9%, acc = 4.1%, imp = 5468

worst case avg CPI: -190.881
[84 sec, 6345097 itr]: obj = (-11.9074), mov = 6345097, inf = 11.9%, acc = 4.1%, imp = 5478

worst case avg CPI: -190.88
[85 sec, 6427919 itr]: obj = (-11.9074), mov = 6427919, inf = 11.8%, acc = 4.1%, imp = 5486

worst case avg CPI: -190.88
[86 sec, 6514820 itr]: obj = (-11.9073), mov = 6514820, inf = 11.8%, acc = 4.1%, imp = 5499

worst case avg CPI: -190.878
[87 sec, 6594159 itr]: obj = (-11.9073), mov = 6594159, inf = 11.8%, acc = 4%, imp = 5514

worst case avg CPI: -190.877
[88 sec, 6668723 itr]: obj = (-11.9072), mov = 6668723, inf = 11.8%, acc = 4%, imp = 5527

worst case avg CPI: -190.876
[89 sec, 6749738 itr]: obj = (-11.9072), mov = 6749738, inf = 11.8%, acc = 4%, imp = 5540

worst case avg CPI: -190.875
[90 sec, 6833149 itr]: obj = (-11.9072), mov = 6833149, inf = 11.8%, acc = 4%, imp = 5549

worst case avg CPI: -190.875
[90 sec, 6833149 itr]: obj = (-11.9072), mov = 6833149, inf = 11.8%, acc = 4%, imp = 5549

worst case avg CPI: -190.875
6833149 iterations, 6833149 moves performed in 90 seconds
Feasible solution: obj = (-11.9072)

Run output...
FINAL SOLUTION
Worst case avg CPI: -190.875
Mapping: 0->18, 1->29, 2->45, 3->22, 4->19, 5->46, 6->3, 7->33, 8->34, 9->44, 10->18, 11->36, 12->1, 13->2, 14->41, 15->13, 16->37, 17->14, 18->22, 19->17, 20->31, 21->11, 22->35, 23->39, 24->23, 25->20, 26->1, 27->29, 28->46, 29->27, 30->16, 31->10, 32->38, 33->22, 34->6, 35->45, 36->9, 37->32, 38->41, 39->38, 40->12, 41->28, 42->25, 43->34, 44->0, 45->8, 46->32, 47->36, 48->8, 49->9, 50->0, 51->7, 52->30, 53->35, 54->25, 55->10, 56->21, 57->38, 58->14, 59->6, 60->30, 61->40, 62->22, 63->44, 64->47, 65->47, 66->33, 67->28, 68->32, 69->13, 70->34, 71->19, 72->33, 73->20, 74->11, 75->11, 76->39, 77->44, 78->41, 79->17, 80->46, 81->15, 82->15, 83->26, 84->42, 85->26, 86->7, 87->5, 88->18, 89->34, 90->32, 91->40, 92->5, 93->16, 94->10, 95->0, 96->16, 97->38, 98->15, 99->27, 100->38, 101->9, 102->8, 103->21, 104->13, 105->10, 106->47, 107->21, 108->19, 109->11, 110->37, 111->30, 112->2, 113->11, 114->23, 115->3, 116->9, 117->19, 118->32, 119->27, 120->4, 121->9, 122->39, 123->1, 124->4, 125->37, 126->17, 127->2, 128->45, 129->24, 130->0, 131->13, 132->22, 133->14, 134->37, 135->20, 136->25, 137->31, 138->26, 139->8, 140->10, 141->39, 142->21, 143->19, 144->2, 145->45, 146->46, 147->5, 148->24, 149->47, 150->43, 151->45, 152->30, 153->47, 154->37, 155->28, 156->6, 157->17, 158->23, 159->36, 160->36, 161->40, 162->31, 163->9, 164->42, 165->36, 166->0, 167->13, 168->35, 169->25, 170->3, 171->23, 172->41, 173->20, 174->5, 175->29, 176->29, 177->6, 178->3, 179->7, 180->33, 181->36, 182->18, 183->31, 184->43, 185->44, 186->23, 187->35, 188->19, 189->37, 190->23, 191->27, 192->31, 193->41, 194->35, 195->8, 196->28, 197->6, 198->44, 199->14, 200->24, 201->47, 202->45, 203->29, 204->3, 205->4, 206->17, 207->2, 208->7, 209->10, 210->40, 211->24, 212->1, 213->3, 214->46, 215->42, 216->34, 217->25, 218->4, 219->31, 220->17, 221->42, 222->39, 223->42, 224->1, 225->28, 226->43, 227->42, 228->13, 229->43, 230->26, 231->12, 232->27, 233->6, 234->4, 235->16, 236->43, 237->7, 238->20, 239->33, 240->46, 241->7, 242->12, 243->18, 244->33, 245->11, 246->18, 247->20, 248->16, 249->39, 250->8, 251->24, 252->16, 253->15, 254->40, 255->41, 256->0, 257->27, 258->25, 259->26, 260->4, 261->15, 262->12, 263->30, 264->32, 265->2, 266->35, 267->14, 268->14, 269->26, 270->1, 271->12, 272->22, 273->24, 274->21, 275->12, 276->5, 277->44, 278->5, 279->21, 280->30, 281->34, 282->28, 283->43, 284->40, 285->15, 286->38, 287->29
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	0.842403	2.15028	2.42737	1.0402	1.13151	0.897764
	2.08172	4.10555	5.09732	4.77197	4.14627	1.01306
	2.1523	7.79662	7.53287	7.7912	5.61737	0.875776
	0.767973	5.47249	5.81026	7.20921	5.43499	0.725173
	0.785959	5.81788	7.58309	5.47912	5.61926	0.681767
	1.16174	5.40404	5.39615	6.04583	5.0233	1.7714
	1.3243	4.81749	5.87871	5.34364	4.49534	1.76325
	1.02979	3.16667	3.18821	3.52568	3.0347	0.771066
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 15 --global-tlim 90 --seed 101 --use-acpi-model

Starting the Optimization Process
