LocalSolver 4.0 (MacOS64, build 20140127)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Average CPI weights for the platform:
 0.5 0.75 0.75 0.75 0.75 0.5 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.5 0.75 0.75 0.75 0.75 0.5

Min CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Neighbor CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Other CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Workload Information:
2.55359, 2.36117, 0.573532, 2.29872, 2.64543, 1.56757, 1.64625, 1.52824, 1.43803, 0.5, 3.64814, 3.23935, 2.51107, 2.40504, 2.72906, 2.62251, 0.5, 1.23232, 2.20879, 1.20027, 0.5, 5.53224, 0.994675, 1.56401, 1.61236, 0.5, 4.21228, 2.68281, 0.831039, 2.16151, 4.10149, 1.69636, 1.32855, 1.90991, 2.46369, 0.5, 2.06286, 2.17432, 2.44801, 2.31855, 1.94914, 1.04712, 0.5, 3.40758, 0.692599, 0.581298, 3.95491, 2.86361, 3.40809, 0.55863, 0.5, 4.06533, 0.728905, 2.73904, 1.09989, 0.5, 3.11603, 4.30389, 2.12124, 0.858948, 1.79955, 2.07464, 1.89003, 1.92703, 2.08784, 2.2209, 0.91615, 2.83479, 2.3763, 0.840966, 2.02627, 3.00552, 0.930088, 4.13549, 0.907767, 3.06438, 0.894031, 2.06824, 0.5, 3.39243, 2.32285, 0.984231, 4.27939, 0.521107, 1.92992, 2.6927, 0.606388, 3.40636, 2.86463, 0.5, 0.769231, 2.15511, 2.37213, 2.15005, 2.14917, 2.40431, 2.008, 2.01354, 1.8307, 2.11466, 1.93379, 2.09931, 2.78209, 2.86996, 0.624814, 2.52425, 2.69888, 0.5, 2.60002, 0.5, 2.55397, 2.20386, 2.44118, 1.70097, 2.19957, 3.88385, 0.909231, 0.61331, 1.61421, 2.77983, 1.52734, 2.40316, 2.38423, 1.9802, 1.84871, 1.85637, 1.69252, 0.5, 2.40423, 2.1761, 1.91143, 3.31573, 1.42531, 4.22734, 0.962699, 0.5, 0.680057, 4.20459, 3.36067, 3.62207, 2.07557, 0.698655, 0.5, 1.74304, 1.10969, 0.896667, 2.3006, 2.96707, 1.81721, 2.90877, 2.8252, 3.51572, 1.32983, 1.039, 1.93841, 1.35184, 3.35032, 2.99665, 2.84917, 0.87015, 1.43371, 0.5, 1.04984, 2.15255, 2.28473, 2.20354, 1.94725, 2.36208, 3.17786, 2.84797, 0.703695, 0.695322, 0.564924, 4.01022, 1.0335, 1.27125, 0.589603, 4.29137, 2.09966, 2.71461, 1.30416, 1.33529, 2.18506, 1.33513, 5.34037, 0.5, 2.64882, 3.53865, 0.5, 3.29267, 0.756623, 1.26323, 0.5, 2.27668, 1.685, 1.8148, 2.35731, 3.36622, 2.52334, 2.31358, 2.47449, 2.05636, 0.951763, 1.68047, 4.83694, 8.8023, 13.8034, 0.83421, 27.4394, 4.28373, 13.8001, 12.8991, 9.04167, 0.5, 10.4205, 13.3387, 2.16182, 2.17339, 2.40504, 2.0239, 1.43849, 1.79737, 2.93771, 2.80105, 2.00619, 1.16265, 0.5, 2.59241, 2.07594, 2.18482, 1.40789, 2.3826, 2.13165, 1.8171, 1.50139, 3.08218, 1.8065, 0.603463, 2.5178, 2.48866, 14.6043, 10.687, 0.5, 14.1735, 7.84635, 12.1889, 17.4451, 4.65895, 16.1089, 7.99628, 11.6184, 2.17231, 3.15289, 1.3518, 1.20014, 3.24318, 2.55199, 0.5, 0.5, 1.5547, 2.47587, 2.08944, 2.69116, 2.68883, 1.34601, 1.47435, 2.67901, 1.99912, 2.6342, 1.86732, 0.953892, 1.68086, 2.14936, 2.77665, 1.54645, 2.89279, 2.29255, 7.04134, 20.331, 6.35332, 7.11756, 16.8643, 11.703, 8.39222, 10.1196, 9.35711, 12.3621, 8.06594
cpi min/max: 0.5/27.4394
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 42806, operands = 113952
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.0608113


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 118287 itr]: obj = (-0.737317), mov = 118287, inf = 21.8%, acc = 13.2%, imp = 798

worst case avg CPI: 4.59696
[2 sec, 413269 itr]: obj = (-0.737317), mov = 413269, inf = 13.4%, acc = 12.7%, imp = 1005

worst case avg CPI: 4.59696
[3 sec, 707048 itr]: obj = (-0.737317), mov = 707048, inf = 11.6%, acc = 12.5%, imp = 1017

worst case avg CPI: 4.59696
[4 sec, 1007321 itr]: obj = (-0.737317), mov = 1007321, inf = 10.8%, acc = 12.5%, imp = 1017

worst case avg CPI: 4.59696
[5 sec, 1306286 itr]: obj = (-0.737317), mov = 1306286, inf = 10.3%, acc = 12.5%, imp = 1018

worst case avg CPI: 4.59696
[6 sec, 1600981 itr]: obj = (-0.737317), mov = 1600981, inf = 10%, acc = 12.6%, imp = 1444

worst case avg CPI: 4.59696
[7 sec, 1898898 itr]: obj = (-0.737317), mov = 1898898, inf = 9.7%, acc = 12.7%, imp = 1882

worst case avg CPI: 4.59696
[8 sec, 2197810 itr]: obj = (-0.737157), mov = 2197810, inf = 9.5%, acc = 12.6%, imp = 1919

worst case avg CPI: 4.59975
[9 sec, 2501432 itr]: obj = (-0.737113), mov = 2501432, inf = 9.4%, acc = 12.6%, imp = 1930

worst case avg CPI: 4.60052
[10 sec, 2796814 itr]: obj = (-0.737113), mov = 2796814, inf = 9.2%, acc = 12.6%, imp = 1930

worst case avg CPI: 4.60052
[11 sec, 3097972 itr]: obj = (-0.737097), mov = 3097972, inf = 9.1%, acc = 12.6%, imp = 1935

worst case avg CPI: 4.6008
[12 sec, 3397824 itr]: obj = (-0.737097), mov = 3397824, inf = 9.1%, acc = 12.5%, imp = 1936

worst case avg CPI: 4.6008
[13 sec, 3691182 itr]: obj = (-0.737097), mov = 3691182, inf = 9%, acc = 12.5%, imp = 1936

worst case avg CPI: 4.6008
[14 sec, 3986301 itr]: obj = (-0.737097), mov = 3986301, inf = 8.9%, acc = 12.5%, imp = 1936

worst case avg CPI: 4.6008
[15 sec, 4284818 itr]: obj = (-0.737097), mov = 4284818, inf = 8.9%, acc = 12.5%, imp = 1936

worst case avg CPI: 4.6008
[16 sec, 4585421 itr]: obj = (-0.737097), mov = 4585421, inf = 8.8%, acc = 12.5%, imp = 1936

worst case avg CPI: 4.6008
[17 sec, 4898605 itr]: obj = (-0.737097), mov = 4898605, inf = 8.8%, acc = 12.5%, imp = 1936

worst case avg CPI: 4.6008
[18 sec, 5206586 itr]: obj = (-0.737097), mov = 5206586, inf = 8.8%, acc = 12.5%, imp = 1936

worst case avg CPI: 4.6008
[19 sec, 5506020 itr]: obj = (-0.736871), mov = 5506020, inf = 8.7%, acc = 12.5%, imp = 2366

worst case avg CPI: 4.60476
[20 sec, 5815226 itr]: obj = (-0.736842), mov = 5815226, inf = 8.7%, acc = 12.5%, imp = 2374

worst case avg CPI: 4.60526
[21 sec, 6126380 itr]: obj = (-0.736842), mov = 6126380, inf = 8.7%, acc = 12.5%, imp = 2374

worst case avg CPI: 4.60526
[22 sec, 6434100 itr]: obj = (-0.736811), mov = 6434100, inf = 8.6%, acc = 12.5%, imp = 2382

worst case avg CPI: 4.60581
[23 sec, 6741161 itr]: obj = (-0.736793), mov = 6741161, inf = 8.6%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[24 sec, 7044465 itr]: obj = (-0.736793), mov = 7044465, inf = 8.6%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[25 sec, 7347192 itr]: obj = (-0.736793), mov = 7347192, inf = 8.6%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[26 sec, 7648765 itr]: obj = (-0.736793), mov = 7648765, inf = 8.6%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[27 sec, 7947507 itr]: obj = (-0.736793), mov = 7947507, inf = 8.6%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[28 sec, 8250040 itr]: obj = (-0.736793), mov = 8250040, inf = 8.5%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[29 sec, 8548307 itr]: obj = (-0.736793), mov = 8548307, inf = 8.5%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[30 sec, 8847745 itr]: obj = (-0.736793), mov = 8847745, inf = 8.5%, acc = 12.5%, imp = 2387

worst case avg CPI: 4.60613
[31 sec, 9147038 itr]: obj = (-0.736793), mov = 9147038, inf = 8.5%, acc = 12.4%, imp = 2387

worst case avg CPI: 4.60613
[32 sec, 9446356 itr]: obj = (-0.736793), mov = 9446356, inf = 8.5%, acc = 12.4%, imp = 2387

worst case avg CPI: 4.60613
[33 sec, 9749198 itr]: obj = (-0.736793), mov = 9749198, inf = 8.5%, acc = 12.4%, imp = 2387

worst case avg CPI: 4.60613
[34 sec, 10050229 itr]: obj = (-0.736793), mov = 10050229, inf = 8.5%, acc = 12.5%, imp = 2728

worst case avg CPI: 4.60613
[35 sec, 10350501 itr]: obj = (-0.736793), mov = 10350501, inf = 8.5%, acc = 12.5%, imp = 3175

worst case avg CPI: 4.60613
[36 sec, 10651125 itr]: obj = (-0.736793), mov = 10651125, inf = 8.5%, acc = 12.5%, imp = 3250

worst case avg CPI: 4.60613
[37 sec, 10949619 itr]: obj = (-0.736793), mov = 10949619, inf = 8.5%, acc = 12.6%, imp = 3692

worst case avg CPI: 4.60613
[38 sec, 11256505 itr]: obj = (-0.736793), mov = 11256505, inf = 8.4%, acc = 12.6%, imp = 3712

worst case avg CPI: 4.60613
[39 sec, 11557084 itr]: obj = (-0.736793), mov = 11557084, inf = 8.4%, acc = 12.6%, imp = 3723

worst case avg CPI: 4.60613
[40 sec, 11860460 itr]: obj = (-0.736793), mov = 11860460, inf = 8.4%, acc = 12.6%, imp = 3723

worst case avg CPI: 4.60613
[41 sec, 12164102 itr]: obj = (-0.736793), mov = 12164102, inf = 8.4%, acc = 12.6%, imp = 3740

worst case avg CPI: 4.60613
[42 sec, 12473243 itr]: obj = (-0.736793), mov = 12473243, inf = 8.4%, acc = 12.5%, imp = 3741

worst case avg CPI: 4.60613
[43 sec, 12775018 itr]: obj = (-0.736793), mov = 12775018, inf = 8.4%, acc = 12.5%, imp = 3741

worst case avg CPI: 4.60613
[44 sec, 13080956 itr]: obj = (-0.736793), mov = 13080956, inf = 8.4%, acc = 12.5%, imp = 3751

worst case avg CPI: 4.60613
[45 sec, 13384836 itr]: obj = (-0.736793), mov = 13384836, inf = 8.4%, acc = 12.5%, imp = 3751

worst case avg CPI: 4.60613
[46 sec, 13695040 itr]: obj = (-0.736793), mov = 13695040, inf = 8.4%, acc = 12.5%, imp = 3751

worst case avg CPI: 4.60613
[47 sec, 14002434 itr]: obj = (-0.736793), mov = 14002434, inf = 8.4%, acc = 12.5%, imp = 3754

worst case avg CPI: 4.60613
[48 sec, 14309891 itr]: obj = (-0.736793), mov = 14309891, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[49 sec, 14619267 itr]: obj = (-0.736793), mov = 14619267, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[50 sec, 14928853 itr]: obj = (-0.736793), mov = 14928853, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[51 sec, 15239389 itr]: obj = (-0.736793), mov = 15239389, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[52 sec, 15546550 itr]: obj = (-0.736793), mov = 15546550, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[53 sec, 15856306 itr]: obj = (-0.736793), mov = 15856306, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[54 sec, 16168056 itr]: obj = (-0.736793), mov = 16168056, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[55 sec, 16481065 itr]: obj = (-0.736793), mov = 16481065, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[56 sec, 16791739 itr]: obj = (-0.736793), mov = 16791739, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[57 sec, 17103371 itr]: obj = (-0.736793), mov = 17103371, inf = 8.4%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[58 sec, 17411784 itr]: obj = (-0.736793), mov = 17411784, inf = 8.3%, acc = 12.5%, imp = 3759

worst case avg CPI: 4.60613
[59 sec, 17719083 itr]: obj = (-0.736775), mov = 17719083, inf = 8.3%, acc = 12.5%, imp = 3766

worst case avg CPI: 4.60643
[60 sec, 18027322 itr]: obj = (-0.736775), mov = 18027322, inf = 8.3%, acc = 12.5%, imp = 3767

worst case avg CPI: 4.60643
[61 sec, 18333050 itr]: obj = (-0.736761), mov = 18333050, inf = 8.3%, acc = 12.5%, imp = 3769

worst case avg CPI: 4.60668
[62 sec, 18641105 itr]: obj = (-0.736761), mov = 18641105, inf = 8.3%, acc = 12.5%, imp = 3769

worst case avg CPI: 4.60668
[63 sec, 18946016 itr]: obj = (-0.736761), mov = 18946016, inf = 8.3%, acc = 12.5%, imp = 3769

worst case avg CPI: 4.60668
[64 sec, 19250381 itr]: obj = (-0.736761), mov = 19250381, inf = 8.3%, acc = 12.5%, imp = 3769

worst case avg CPI: 4.60668
[65 sec, 19555279 itr]: obj = (-0.736761), mov = 19555279, inf = 8.3%, acc = 12.5%, imp = 3769

worst case avg CPI: 4.60668
[66 sec, 19865195 itr]: obj = (-0.736761), mov = 19865195, inf = 8.3%, acc = 12.5%, imp = 3769

worst case avg CPI: 4.60668
[67 sec, 20177175 itr]: obj = (-0.736746), mov = 20177175, inf = 8.3%, acc = 12.5%, imp = 3775

worst case avg CPI: 4.60694
[68 sec, 20494058 itr]: obj = (-0.736709), mov = 20494058, inf = 8.3%, acc = 12.5%, imp = 3784

worst case avg CPI: 4.60759
[69 sec, 20803279 itr]: obj = (-0.736709), mov = 20803279, inf = 8.3%, acc = 12.5%, imp = 3784

worst case avg CPI: 4.60759
[70 sec, 21102761 itr]: obj = (-0.736709), mov = 21102761, inf = 8.3%, acc = 12.5%, imp = 3784

worst case avg CPI: 4.60759
[71 sec, 21409358 itr]: obj = (-0.736709), mov = 21409358, inf = 8.3%, acc = 12.5%, imp = 3784

worst case avg CPI: 4.60759
[72 sec, 21718740 itr]: obj = (-0.736709), mov = 21718740, inf = 8.3%, acc = 12.5%, imp = 3784

worst case avg CPI: 4.60759
[73 sec, 22028614 itr]: obj = (-0.736704), mov = 22028614, inf = 8.3%, acc = 12.5%, imp = 3785

worst case avg CPI: 4.60769
[74 sec, 22339503 itr]: obj = (-0.736701), mov = 22339503, inf = 8.3%, acc = 12.5%, imp = 3786

worst case avg CPI: 4.60773
[75 sec, 22652042 itr]: obj = (-0.736701), mov = 22652042, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[76 sec, 22962821 itr]: obj = (-0.736701), mov = 22962821, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[77 sec, 23273493 itr]: obj = (-0.736701), mov = 23273493, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[78 sec, 23579762 itr]: obj = (-0.736701), mov = 23579762, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[79 sec, 23881577 itr]: obj = (-0.736701), mov = 23881577, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[80 sec, 24186963 itr]: obj = (-0.736701), mov = 24186963, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[81 sec, 24492589 itr]: obj = (-0.736701), mov = 24492589, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[82 sec, 24800532 itr]: obj = (-0.736701), mov = 24800532, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[83 sec, 25105177 itr]: obj = (-0.736701), mov = 25105177, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[84 sec, 25415311 itr]: obj = (-0.736701), mov = 25415311, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[85 sec, 25717063 itr]: obj = (-0.736701), mov = 25717063, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[86 sec, 26024973 itr]: obj = (-0.736701), mov = 26024973, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[87 sec, 26325763 itr]: obj = (-0.736701), mov = 26325763, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[88 sec, 26606666 itr]: obj = (-0.736701), mov = 26606666, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[89 sec, 26909586 itr]: obj = (-0.736701), mov = 26909586, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[90 sec, 27226016 itr]: obj = (-0.736701), mov = 27226016, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
[90 sec, 27226016 itr]: obj = (-0.736701), mov = 27226016, inf = 8.3%, acc = 12.5%, imp = 3787

worst case avg CPI: 4.60773
27226016 iterations, 27226016 moves performed in 90 seconds
Feasible solution: obj = (-0.736701)

Run output...
FINAL SOLUTION
Worst case avg CPI: 4.60773
Mapping: 0->10, 1->23, 2->24, 3->41, 4->22, 5->0, 6->20, 7->47, 8->43, 9->1, 10->32, 11->25, 12->22, 13->31, 14->15, 15->38, 16->41, 17->44, 18->20, 19->5, 20->35, 21->37, 22->35, 23->2, 24->18, 25->30, 26->32, 27->15, 28->18, 29->19, 30->32, 31->44, 32->0, 33->19, 34->8, 35->42, 36->12, 37->13, 38->38, 39->43, 40->45, 41->23, 42->41, 43->25, 44->4, 45->43, 46->31, 47->25, 48->16, 49->11, 50->23, 51->34, 52->17, 53->9, 54->45, 55->36, 56->28, 57->25, 58->24, 59->35, 60->45, 61->35, 62->13, 63->12, 64->13, 65->24, 66->21, 67->15, 68->26, 69->44, 70->36, 71->37, 72->5, 73->39, 74->45, 75->28, 76->46, 77->11, 78->36, 79->14, 80->33, 81->0, 82->37, 83->3, 84->6, 85->9, 86->29, 87->16, 88->14, 89->6, 90->24, 91->13, 92->33, 93->11, 94->45, 95->26, 96->13, 97->45, 98->6, 99->1, 100->2, 101->1, 102->31, 103->28, 104->6, 105->38, 106->38, 107->35, 108->27, 109->0, 110->27, 111->11, 112->10, 113->44, 114->46, 115->10, 116->36, 117->2, 118->21, 119->27, 120->1, 121->26, 122->33, 123->47, 124->11, 125->6, 126->47, 127->4, 128->40, 129->46, 130->42, 131->37, 132->44, 133->15, 134->29, 135->0, 136->43, 137->34, 138->28, 139->28, 140->18, 141->24, 142->17, 143->41, 144->23, 145->42, 146->19, 147->7, 148->3, 149->39, 150->15, 151->14, 152->30, 153->12, 154->3, 155->42, 156->7, 157->14, 158->8, 159->24, 160->12, 161->2, 162->47, 163->3, 164->19, 165->19, 166->41, 167->36, 168->7, 169->8, 170->46, 171->23, 172->46, 173->39, 174->36, 175->4, 176->12, 177->32, 178->18, 179->37, 180->2, 181->20, 182->42, 183->1, 184->33, 185->43, 186->9, 187->34, 188->18, 189->16, 190->4, 191->21, 192->47, 193->26, 194->30, 195->44, 196->33, 197->7, 198->9, 199->40, 200->31, 201->3, 202->17, 203->20, 204->34, 205->37, 206->10, 207->30, 208->21, 209->32, 210->9, 211->33, 212->39, 213->18, 214->26, 215->31, 216->3, 217->21, 218->40, 219->17, 220->5, 221->2, 222->34, 223->16, 224->41, 225->47, 226->29, 227->22, 228->17, 229->23, 230->35, 231->6, 232->12, 233->17, 234->29, 235->7, 236->4, 237->11, 238->22, 239->10, 240->22, 241->25, 242->29, 243->8, 244->26, 245->14, 246->13, 247->39, 248->40, 249->38, 250->28, 251->4, 252->25, 253->5, 254->43, 255->9, 256->27, 257->46, 258->30, 259->30, 260->10, 261->5, 262->31, 263->14, 264->42, 265->5, 266->8, 267->29, 268->8, 269->21, 270->20, 271->0, 272->40, 273->22, 274->1, 275->39, 276->40, 277->27, 278->20, 279->16, 280->32, 281->19, 282->7, 283->16, 284->27, 285->38, 286->15, 287->34
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.09354	1.52048	1.28544	1.77457	1.19988	1.41407
	1.52073	4.60777	4.60792	4.60786	4.60776	1.57216
	1.53064	4.62674	4.60786	4.61023	4.60895	1.3496
	1.26977	4.62076	4.69261	5.87895	4.60795	1.31635
	1.20895	4.60905	4.62121	4.60779	4.60858	1.0116
	1.06729	4.60773	4.6091	4.61266	4.60819	1.05603
	1.22185	4.6083	4.60784	4.60798	4.61228	1.4992
	1.36517	1.11968	1.45179	1.65318	1.17305	1.31891
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 14 --global-tlim 90 --seed 101 --use-acpi-model

Starting the Optimization Process
