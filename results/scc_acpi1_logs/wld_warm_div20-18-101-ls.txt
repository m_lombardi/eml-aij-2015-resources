LocalSolver 4.0 (MacOS64, build 20140127)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Average CPI weights for the platform:
 0.5 0.75 0.75 0.75 0.75 0.5 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.5 0.75 0.75 0.75 0.75 0.5

Min CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Neighbor CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Other CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Workload Information:
3.16677, 0.552865, 1.32298, 3.12211, 0.731569, 3.10371, 1.2612, 0.5, 1.46763, 5.97021, 1.38502, 1.41594, 1.50121, 2.0939, 2.53628, 2.04798, 2.16037, 1.66026, 0.807233, 2.79768, 0.743508, 0.5, 3.57748, 3.57409, 1.7275, 1.71631, 2.40276, 2.18868, 1.87854, 2.08621, 2.08286, 2.98595, 2.43447, 3.4015, 0.5, 0.595212, 0.667261, 1.24142, 4.04406, 1.88579, 0.882948, 3.27852, 2.27525, 1.69474, 1.76537, 1.65922, 2.29413, 2.31128, 1.11371, 0.787628, 2.89236, 3.54444, 0.5, 3.16187, 2.00172, 2.11386, 2.1298, 1.90754, 1.92052, 1.92656, 1.97965, 2.6878, 1.11997, 2.65222, 0.5, 3.06036, 2.85316, 2.12014, 2.93418, 1.65542, 0.5, 1.9371, 11.7512, 11.9685, 8.63206, 9.59966, 10.0474, 8.00112, 2.16075, 1.94883, 1.92622, 2.28592, 1.25839, 2.41989, 2.0228, 4.79308, 0.951753, 1.1168, 2.42198, 0.693589, 0.588691, 3.16218, 3.69829, 0.75144, 0.5, 3.2994, 1.06102, 0.934424, 3.79943, 1.03297, 2.03273, 3.13943, 2.77266, 2.66219, 1.96432, 3.0942, 1.00663, 0.5, 1.66585, 2.54982, 2.63626, 1.83538, 2.36083, 0.951861, 17.8815, 12.1507, 6.89925, 10.3983, 6.05457, 6.61562, 3.76776, 1.84813, 2.14991, 0.94212, 0.5, 2.79209, 1.46509, 2.82923, 2.87292, 3.49018, 0.738201, 0.604373, 2.32148, 1.91172, 2.44474, 1.52231, 1.79154, 2.0082, 1.82272, 1.35783, 2.42845, 2.71231, 2.09494, 1.58374, 11.9625, 15.0758, 11.5414, 1.46537, 15.4325, 4.52245, 8.58849, 16.4922, 17.5132, 3.58395, 5.02879, 8.79342, 1.9893, 2.23152, 1.61701, 1.97846, 2.18059, 2.00313, 2.16208, 2.00138, 2.23112, 2.2001, 2.22482, 1.18049, 2.2247, 2.4661, 2.95476, 0.879442, 2.42377, 1.05124, 3.9735, 2.64003, 1.79781, 0.5, 1.64795, 1.44072, 2.092, 0.842462, 1.42652, 0.603459, 2.35185, 4.6837, 0.5, 4.74097, 1.55172, 1.10802, 1.84743, 2.25187, 2.46553, 1.04171, 2.9721, 2.18323, 0.5, 2.83742, 0.5, 4.53402, 1.12206, 0.562305, 1.3333, 3.94831, 2.77443, 1.30841, 2.38395, 1.58606, 2.39146, 1.55568, 2.45593, 1.43254, 1.45231, 1.51227, 2.53621, 2.61074, 3.46184, 2.39246, 2.86093, 0.5, 1.67423, 1.11054, 5.82139, 1.44459, 2.9009, 0.583379, 0.5, 0.749746, 2.29182, 1.98772, 1.88796, 2.34933, 2.38518, 1.09798, 0.5, 1.14938, 3.04879, 3.2407, 2.41068, 1.65045, 2.87753, 3.62579, 0.719462, 0.5, 0.990044, 3.28717, 2.46692, 0.736493, 2.16848, 4.80788, 0.752042, 1.06818, 2.36438, 2.33041, 1.48881, 1.88913, 2.31491, 1.61235, 2.53878, 3.06425, 3.05494, 1.07348, 1.49652, 0.772033, 1.24126, 1.55978, 2.39673, 3.00719, 3.29503, 0.5, 4.29793, 1.37301, 1.61013, 3.50633, 0.5, 0.712598, 2.03801, 1.26336, 0.5, 3.60041, 0.87444, 3.72378, 1.72439, 1.3915, 3.27714, 1.21099, 1.20117, 3.19481
cpi min/max: 0.5/17.8815
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 42811, operands = 113952
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.00187971


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 147292 itr]: obj = (-0.775505), mov = 147292, inf = 20%, acc = 15.7%, imp = 875

worst case avg CPI: 3.92866
[2 sec, 471072 itr]: obj = (-0.774685), mov = 471072, inf = 13.1%, acc = 14%, imp = 1022

worst case avg CPI: 3.94301
[3 sec, 803809 itr]: obj = (-0.774613), mov = 803809, inf = 11.8%, acc = 13.6%, imp = 1033

worst case avg CPI: 3.94427
[4 sec, 1132077 itr]: obj = (-0.774569), mov = 1132077, inf = 11.3%, acc = 13.4%, imp = 1039

worst case avg CPI: 3.94504
[5 sec, 1467060 itr]: obj = (-0.774569), mov = 1467060, inf = 10.9%, acc = 13.3%, imp = 1039

worst case avg CPI: 3.94504
[6 sec, 1797788 itr]: obj = (-0.774569), mov = 1797788, inf = 10.7%, acc = 13.2%, imp = 1039

worst case avg CPI: 3.94504
[7 sec, 2133785 itr]: obj = (-0.774569), mov = 2133785, inf = 10.6%, acc = 13.2%, imp = 1039

worst case avg CPI: 3.94504
[8 sec, 2454791 itr]: obj = (-0.774569), mov = 2454791, inf = 10.5%, acc = 13.1%, imp = 1039

worst case avg CPI: 3.94504
[9 sec, 2781184 itr]: obj = (-0.774569), mov = 2781184, inf = 10.4%, acc = 13.1%, imp = 1039

worst case avg CPI: 3.94504
[10 sec, 3106209 itr]: obj = (-0.774552), mov = 3106209, inf = 10.3%, acc = 13.1%, imp = 1042

worst case avg CPI: 3.94535
[11 sec, 3418539 itr]: obj = (-0.774524), mov = 3418539, inf = 10.2%, acc = 13.1%, imp = 1047

worst case avg CPI: 3.94582
[12 sec, 3733962 itr]: obj = (-0.774524), mov = 3733962, inf = 10.1%, acc = 13.1%, imp = 1047

worst case avg CPI: 3.94582
[13 sec, 4051287 itr]: obj = (-0.774524), mov = 4051287, inf = 10.1%, acc = 13.1%, imp = 1047

worst case avg CPI: 3.94582
[14 sec, 4359653 itr]: obj = (-0.774524), mov = 4359653, inf = 10%, acc = 13.1%, imp = 1047

worst case avg CPI: 3.94582
[15 sec, 4686910 itr]: obj = (-0.774522), mov = 4686910, inf = 10%, acc = 13.1%, imp = 1048

worst case avg CPI: 3.94586
[16 sec, 5020167 itr]: obj = (-0.774522), mov = 5020167, inf = 9.9%, acc = 13.1%, imp = 1048

worst case avg CPI: 3.94586
[17 sec, 5353870 itr]: obj = (-0.774508), mov = 5353870, inf = 9.8%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[18 sec, 5679617 itr]: obj = (-0.774508), mov = 5679617, inf = 9.8%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[19 sec, 6003574 itr]: obj = (-0.774508), mov = 6003574, inf = 9.7%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[20 sec, 6325532 itr]: obj = (-0.774508), mov = 6325532, inf = 9.7%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[21 sec, 6645189 itr]: obj = (-0.774508), mov = 6645189, inf = 9.6%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[22 sec, 6968845 itr]: obj = (-0.774508), mov = 6968845, inf = 9.6%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[23 sec, 7282375 itr]: obj = (-0.774508), mov = 7282375, inf = 9.6%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[24 sec, 7592438 itr]: obj = (-0.774508), mov = 7592438, inf = 9.5%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[25 sec, 7923994 itr]: obj = (-0.774508), mov = 7923994, inf = 9.5%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[26 sec, 8247338 itr]: obj = (-0.774508), mov = 8247338, inf = 9.5%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[27 sec, 8564602 itr]: obj = (-0.774508), mov = 8564602, inf = 9.5%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[28 sec, 8886251 itr]: obj = (-0.774508), mov = 8886251, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[29 sec, 9208135 itr]: obj = (-0.774508), mov = 9208135, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[30 sec, 9533685 itr]: obj = (-0.774508), mov = 9533685, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[31 sec, 9850701 itr]: obj = (-0.774508), mov = 9850701, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[32 sec, 10168591 itr]: obj = (-0.774508), mov = 10168591, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[33 sec, 10489408 itr]: obj = (-0.774508), mov = 10489408, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[34 sec, 10788909 itr]: obj = (-0.774508), mov = 10788909, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[35 sec, 11103008 itr]: obj = (-0.774508), mov = 11103008, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[36 sec, 11419925 itr]: obj = (-0.774508), mov = 11419925, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[37 sec, 11735197 itr]: obj = (-0.774508), mov = 11735197, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[38 sec, 12050739 itr]: obj = (-0.774508), mov = 12050739, inf = 9.4%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[39 sec, 12369160 itr]: obj = (-0.774508), mov = 12369160, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[40 sec, 12686577 itr]: obj = (-0.774508), mov = 12686577, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[41 sec, 12993522 itr]: obj = (-0.774508), mov = 12993522, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[42 sec, 13310366 itr]: obj = (-0.774508), mov = 13310366, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[43 sec, 13635116 itr]: obj = (-0.774508), mov = 13635116, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[44 sec, 13967216 itr]: obj = (-0.774508), mov = 13967216, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[45 sec, 14291464 itr]: obj = (-0.774508), mov = 14291464, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[46 sec, 14620150 itr]: obj = (-0.774508), mov = 14620150, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[47 sec, 14951159 itr]: obj = (-0.774508), mov = 14951159, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[48 sec, 15290083 itr]: obj = (-0.774508), mov = 15290083, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[49 sec, 15624933 itr]: obj = (-0.774508), mov = 15624933, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[50 sec, 15951631 itr]: obj = (-0.774508), mov = 15951631, inf = 9.3%, acc = 13.1%, imp = 1052

worst case avg CPI: 3.94611
[51 sec, 16268599 itr]: obj = (-0.774508), mov = 16268599, inf = 9.3%, acc = 13%, imp = 1052

worst case avg CPI: 3.94611
[52 sec, 16581993 itr]: obj = (-0.774508), mov = 16581993, inf = 9.3%, acc = 13%, imp = 1052

worst case avg CPI: 3.94611
[53 sec, 16911766 itr]: obj = (-0.774508), mov = 16911766, inf = 9.3%, acc = 13%, imp = 1052

worst case avg CPI: 3.94611
[54 sec, 17228290 itr]: obj = (-0.774508), mov = 17228290, inf = 9.3%, acc = 13.1%, imp = 1431

worst case avg CPI: 3.94611
[55 sec, 17556214 itr]: obj = (-0.774259), mov = 17556214, inf = 9.3%, acc = 13.1%, imp = 1469

worst case avg CPI: 3.95046
[56 sec, 17889526 itr]: obj = (-0.774157), mov = 17889526, inf = 9.3%, acc = 13.1%, imp = 1476

worst case avg CPI: 3.95225
[57 sec, 18222581 itr]: obj = (-0.774157), mov = 18222581, inf = 9.2%, acc = 13.1%, imp = 1476

worst case avg CPI: 3.95225
[58 sec, 18556829 itr]: obj = (-0.774157), mov = 18556829, inf = 9.2%, acc = 13.1%, imp = 1476

worst case avg CPI: 3.95225
[59 sec, 18860352 itr]: obj = (-0.774157), mov = 18860352, inf = 9.2%, acc = 13.1%, imp = 1476

worst case avg CPI: 3.95225
[60 sec, 19171472 itr]: obj = (-0.774157), mov = 19171472, inf = 9.2%, acc = 13.1%, imp = 1476

worst case avg CPI: 3.95225
[61 sec, 19495738 itr]: obj = (-0.774157), mov = 19495738, inf = 9.2%, acc = 13.1%, imp = 1675

worst case avg CPI: 3.95225
[62 sec, 19830702 itr]: obj = (-0.774157), mov = 19830702, inf = 9.2%, acc = 13.1%, imp = 1905

worst case avg CPI: 3.95225
[63 sec, 20171857 itr]: obj = (-0.774086), mov = 20171857, inf = 9.2%, acc = 13.1%, imp = 1929

worst case avg CPI: 3.9535
[64 sec, 20496170 itr]: obj = (-0.773995), mov = 20496170, inf = 9.2%, acc = 13.1%, imp = 1942

worst case avg CPI: 3.95508
[65 sec, 20824675 itr]: obj = (-0.773981), mov = 20824675, inf = 9.2%, acc = 13.1%, imp = 1945

worst case avg CPI: 3.95534
[66 sec, 21143876 itr]: obj = (-0.773969), mov = 21143876, inf = 9.2%, acc = 13.1%, imp = 1947

worst case avg CPI: 3.95555
[67 sec, 21463115 itr]: obj = (-0.773969), mov = 21463115, inf = 9.2%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[68 sec, 21801725 itr]: obj = (-0.773969), mov = 21801725, inf = 9.2%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[69 sec, 22130815 itr]: obj = (-0.773969), mov = 22130815, inf = 9.2%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[70 sec, 22478237 itr]: obj = (-0.773969), mov = 22478237, inf = 9.2%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[71 sec, 22814046 itr]: obj = (-0.773969), mov = 22814046, inf = 9.2%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[72 sec, 23132977 itr]: obj = (-0.773969), mov = 23132977, inf = 9.2%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[73 sec, 23466715 itr]: obj = (-0.773969), mov = 23466715, inf = 9.1%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[74 sec, 23790146 itr]: obj = (-0.773969), mov = 23790146, inf = 9.1%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[75 sec, 24111490 itr]: obj = (-0.773969), mov = 24111490, inf = 9.1%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[76 sec, 24443098 itr]: obj = (-0.773969), mov = 24443098, inf = 9.1%, acc = 13.1%, imp = 1948

worst case avg CPI: 3.95555
[77 sec, 24776409 itr]: obj = (-0.773969), mov = 24776409, inf = 9.1%, acc = 13.1%, imp = 2365

worst case avg CPI: 3.95555
[78 sec, 25111079 itr]: obj = (-0.773969), mov = 25111079, inf = 9.1%, acc = 13.1%, imp = 2398

worst case avg CPI: 3.95555
[79 sec, 25439433 itr]: obj = (-0.773969), mov = 25439433, inf = 9.1%, acc = 13.2%, imp = 2427

worst case avg CPI: 3.95555
[80 sec, 25777007 itr]: obj = (-0.773969), mov = 25777007, inf = 9.1%, acc = 13.2%, imp = 2433

worst case avg CPI: 3.95555
[81 sec, 26116716 itr]: obj = (-0.773969), mov = 26116716, inf = 9.1%, acc = 13.2%, imp = 2433

worst case avg CPI: 3.95555
[82 sec, 26457524 itr]: obj = (-0.773969), mov = 26457524, inf = 9.1%, acc = 13.2%, imp = 2433

worst case avg CPI: 3.95555
[83 sec, 26801282 itr]: obj = (-0.773969), mov = 26801282, inf = 9.1%, acc = 13.2%, imp = 2433

worst case avg CPI: 3.95555
[84 sec, 27142217 itr]: obj = (-0.773969), mov = 27142217, inf = 9.1%, acc = 13.2%, imp = 2433

worst case avg CPI: 3.95555
[85 sec, 27480827 itr]: obj = (-0.773969), mov = 27480827, inf = 9.1%, acc = 13.2%, imp = 2433

worst case avg CPI: 3.95555
[86 sec, 27819126 itr]: obj = (-0.773969), mov = 27819126, inf = 9.1%, acc = 13.2%, imp = 2442

worst case avg CPI: 3.95555
[87 sec, 28160472 itr]: obj = (-0.773969), mov = 28160472, inf = 9.1%, acc = 13.2%, imp = 2442

worst case avg CPI: 3.95555
[88 sec, 28497560 itr]: obj = (-0.773969), mov = 28497560, inf = 9.1%, acc = 13.2%, imp = 2442

worst case avg CPI: 3.95555
[89 sec, 28840807 itr]: obj = (-0.773969), mov = 28840807, inf = 9.1%, acc = 13.2%, imp = 2442

worst case avg CPI: 3.95555
[90 sec, 29182258 itr]: obj = (-0.773969), mov = 29182258, inf = 9.1%, acc = 13.2%, imp = 2442

worst case avg CPI: 3.95555
[90 sec, 29182258 itr]: obj = (-0.773969), mov = 29182258, inf = 9.1%, acc = 13.2%, imp = 2442

worst case avg CPI: 3.95555
29182258 iterations, 29182258 moves performed in 90 seconds
Feasible solution: obj = (-0.773969)

Run output...
FINAL SOLUTION
Worst case avg CPI: 3.95555
Mapping: 0->16, 1->24, 2->32, 3->20, 4->6, 5->15, 6->4, 7->6, 8->41, 9->15, 10->44, 11->23, 12->46, 13->12, 14->9, 15->18, 16->45, 17->44, 18->29, 19->38, 20->32, 21->23, 22->33, 23->40, 24->11, 25->29, 26->14, 27->39, 28->44, 29->18, 30->4, 31->20, 32->28, 33->34, 34->23, 35->37, 36->47, 37->44, 38->25, 39->36, 40->12, 41->25, 42->2, 43->8, 44->32, 45->37, 46->17, 47->24, 48->35, 49->36, 50->9, 51->25, 52->27, 53->33, 54->35, 55->37, 56->5, 57->1, 58->11, 59->43, 60->41, 61->31, 62->2, 63->38, 64->6, 65->19, 66->19, 67->23, 68->15, 69->47, 70->29, 71->42, 72->26, 73->22, 74->19, 75->38, 76->31, 77->9, 78->8, 79->29, 80->5, 81->39, 82->3, 83->26, 84->1, 85->20, 86->46, 87->17, 88->26, 89->29, 90->42, 91->40, 92->40, 93->0, 94->30, 95->21, 96->0, 97->8, 98->21, 99->1, 100->27, 101->34, 102->9, 103->38, 104->41, 105->10, 106->0, 107->17, 108->3, 109->16, 110->19, 111->5, 112->39, 113->11, 114->8, 115->39, 116->34, 117->7, 118->40, 119->33, 120->34, 121->29, 122->0, 123->6, 124->45, 125->31, 126->44, 127->7, 128->7, 129->16, 130->1, 131->45, 132->42, 133->30, 134->28, 135->35, 136->24, 137->36, 138->42, 139->46, 140->14, 141->15, 142->24, 143->35, 144->14, 145->13, 146->28, 147->44, 148->27, 149->20, 150->16, 151->37, 152->32, 153->21, 154->21, 155->10, 156->37, 157->24, 158->3, 159->47, 160->42, 161->13, 162->18, 163->17, 164->3, 165->47, 166->4, 167->23, 168->14, 169->14, 170->10, 171->18, 172->26, 173->5, 174->40, 175->7, 176->12, 177->45, 178->24, 179->17, 180->27, 181->41, 182->2, 183->23, 184->43, 185->9, 186->30, 187->25, 188->36, 189->2, 190->37, 191->14, 192->28, 193->3, 194->38, 195->6, 196->36, 197->10, 198->45, 199->25, 200->6, 201->12, 202->47, 203->19, 204->31, 205->41, 206->39, 207->43, 208->28, 209->12, 210->28, 211->5, 212->11, 213->1, 214->7, 215->19, 216->33, 217->39, 218->9, 219->30, 220->11, 221->46, 222->15, 223->46, 224->31, 225->43, 226->36, 227->0, 228->32, 229->13, 230->27, 231->22, 232->22, 233->11, 234->35, 235->30, 236->10, 237->34, 238->26, 239->2, 240->16, 241->33, 242->41, 243->17, 244->0, 245->34, 246->7, 247->18, 248->27, 249->20, 250->4, 251->5, 252->26, 253->22, 254->18, 255->47, 256->22, 257->45, 258->31, 259->16, 260->38, 261->32, 262->13, 263->46, 264->42, 265->13, 266->22, 267->10, 268->33, 269->4, 270->21, 271->43, 272->8, 273->20, 274->3, 275->2, 276->13, 277->8, 278->43, 279->25, 280->30, 281->21, 282->1, 283->4, 284->40, 285->12, 286->35, 287->15
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.11813	1.48969	1.38213	1.38568	1.36874	1.57389
	0.996497	3.95728	4.25749	3.95784	3.95596	1.47073
	1.35061	4.02682	3.95606	3.9561	3.95617	1.30884
	1.56684	3.95681	3.9563	3.95555	3.95751	1.05334
	1.77168	3.95707	3.96532	4.01894	3.95559	1.25235
	0.905923	3.9569	4.11839	3.95627	3.95597	1.32044
	1.20556	4.1162	3.95647	3.96042	3.95663	1.38032
	1.68197	1.38681	1.51595	0.979516	1.18966	1.62061
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 18 --global-tlim 90 --seed 101 --use-acpi-model

Starting the Optimization Process
