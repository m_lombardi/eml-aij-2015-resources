LocalSolver 4.0 (MacOS64, build 20140127)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Average CPI weights for the platform:
 0.5 0.75 0.75 0.75 0.75 0.5 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.5 0.75 0.75 0.75 0.75 0.5

Min CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Neighbor CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Other CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Workload Information:
1.94365, 1.72274, 3.0003, 2.67473, 0.708817, 1.94976, 2.19814, 1.98059, 1.65253, 2.4329, 1.55026, 2.18558, 1.48288, 2.34618, 2.63831, 0.569931, 2.33748, 2.62522, 0.804851, 1.52861, 1.80011, 2.60212, 2.33104, 2.93327, 1.94532, 4.65093, 0.878058, 0.5, 1.06048, 2.96521, 2.23336, 2.27321, 1.83764, 2.40798, 2.09804, 1.14978, 2.18893, 1.90383, 1.76951, 2.21741, 1.98643, 1.93389, 1.55668, 2.88121, 2.46075, 0.911159, 3.03305, 1.15715, 0.591041, 1.89129, 1.07596, 6.14979, 0.665151, 1.62677, 0.767756, 4.62481, 4.39743, 1.19803, 0.511976, 0.5, 1.96853, 2.16397, 1.90805, 2.06591, 1.81344, 2.0801, 3.14364, 2.68947, 3.07273, 0.992259, 1.12898, 0.972916, 1.28574, 3.11346, 2.32277, 3.49911, 0.5, 1.27892, 2.49159, 1.16019, 1.80184, 1.58221, 2.66877, 2.29539, 3.32212, 0.892106, 1.63738, 3.35417, 0.746758, 2.04747, 3.13617, 0.50463, 0.5, 2.24314, 3.80159, 1.81447, 3.23507, 1.39096, 1.87515, 0.5, 4.3616, 0.637222, 1.13916, 0.822893, 4.33047, 0.600166, 4.60731, 0.5, 1.53706, 1.61894, 2.89586, 3.09672, 1.62184, 1.22957, 2.25583, 2.5967, 1.38999, 1.90329, 1.28025, 2.57393, 5.29231, 1.79515, 1.08895, 2.52152, 0.5, 0.802071, 1.39208, 4.15828, 1.19659, 3.60276, 0.832847, 0.817443, 1.42375, 3.38068, 3.5672, 1.98301, 0.5, 1.14536, 2.08175, 1.67462, 1.4119, 2.48904, 2.36101, 1.98167, 0.831055, 2.3706, 2.51188, 1.69479, 2.39317, 2.1985, 2.14689, 1.49416, 2.03144, 2.24304, 1.86527, 2.21921, 0.5, 3.26911, 3.04269, 1.6405, 0.513251, 3.03445, 2.40604, 1.29186, 1.22159, 4.02385, 2.55665, 0.5, 0.5, 1.05656, 1.97776, 3.13619, 0.881539, 4.44794, 3.45001, 1.15696, 1.77274, 2.26232, 0.5, 2.85797, 2.69944, 2.17428, 2.35897, 0.949505, 1.29318, 2.52463, 2.31628, 1.77659, 2.19046, 1.3835, 2.0286, 2.30457, 2.11015, 1.89318, 1.85647, 1.92611, 2.07169, 2.1424, 13.0536, 21.2619, 23.5566, 26.8842, 25.3488, 21.8948, 1.7748, 0.834071, 3.4189, 0.867087, 3.53284, 1.57231, 3.08433, 0.5, 0.925509, 1.9174, 3.21161, 2.36115, 3.31126, 0.804972, 5.2321, 0.809171, 1.18812, 0.654377, 3.01691, 2.54381, 1.23675, 1.63031, 2.58394, 0.988287, 0.619142, 2.15659, 3.61914, 1.34939, 3.50477, 0.750974, 1.35392, 1.40387, 2.36883, 1.9597, 2.43715, 2.47654, 1.52286, 2.39294, 2.11697, 2.54111, 2.11235, 1.31377, 2.14366, 1.8421, 2.27972, 2.50482, 1.00258, 2.22712, 4.00905, 0.693261, 2.39196, 3.76351, 0.5, 0.642218, 2.10021, 2.42915, 1.13832, 3.29873, 0.550335, 2.48325, 1.22267, 2.23415, 4.01466, 1.01903, 3.00948, 0.5, 3.52561, 1.02266, 1.11455, 2.19579, 2.65783, 1.48356, 1.83991, 1.85062, 2.77056, 0.5, 2.90158, 2.13732, 1.16738, 1.15033, 2.96474, 3.88986, 1.97515, 0.852553
cpi min/max: 0.5/26.8842
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 42815, operands = 113952
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.149745


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 121000 itr]: obj = (-0.827812), mov = 121000, inf = 22.7%, acc = 14.2%, imp = 631

worst case avg CPI: 3.01329
[2 sec, 413491 itr]: obj = (-0.827777), mov = 413491, inf = 13.8%, acc = 15%, imp = 807

worst case avg CPI: 3.0139
[3 sec, 709868 itr]: obj = (-0.827693), mov = 709868, inf = 11.8%, acc = 15.2%, imp = 822

worst case avg CPI: 3.01538
[4 sec, 1005246 itr]: obj = (-0.827647), mov = 1005246, inf = 10.9%, acc = 15.3%, imp = 833

worst case avg CPI: 3.01618
[5 sec, 1303736 itr]: obj = (-0.827638), mov = 1303736, inf = 10.4%, acc = 15.3%, imp = 837

worst case avg CPI: 3.01633
[6 sec, 1601183 itr]: obj = (-0.827632), mov = 1601183, inf = 10.1%, acc = 15.3%, imp = 840

worst case avg CPI: 3.01644
[7 sec, 1900220 itr]: obj = (-0.827632), mov = 1900220, inf = 9.9%, acc = 15.3%, imp = 841

worst case avg CPI: 3.01644
[8 sec, 2201347 itr]: obj = (-0.827632), mov = 2201347, inf = 9.7%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[9 sec, 2493598 itr]: obj = (-0.827632), mov = 2493598, inf = 9.6%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[10 sec, 2792200 itr]: obj = (-0.827632), mov = 2792200, inf = 9.5%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[11 sec, 3093056 itr]: obj = (-0.827632), mov = 3093056, inf = 9.5%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[12 sec, 3397999 itr]: obj = (-0.827632), mov = 3397999, inf = 9.4%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[13 sec, 3696569 itr]: obj = (-0.827632), mov = 3696569, inf = 9.3%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[14 sec, 3996783 itr]: obj = (-0.827632), mov = 3996783, inf = 9.3%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[15 sec, 4299959 itr]: obj = (-0.827632), mov = 4299959, inf = 9.3%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[16 sec, 4604029 itr]: obj = (-0.827632), mov = 4604029, inf = 9.2%, acc = 15.4%, imp = 841

worst case avg CPI: 3.01644
[17 sec, 4911420 itr]: obj = (-0.827624), mov = 4911420, inf = 9.2%, acc = 15.4%, imp = 843

worst case avg CPI: 3.01657
[18 sec, 5218806 itr]: obj = (-0.827621), mov = 5218806, inf = 9.2%, acc = 15.4%, imp = 845

worst case avg CPI: 3.01664
[19 sec, 5535276 itr]: obj = (-0.827621), mov = 5535276, inf = 9.1%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[20 sec, 5864042 itr]: obj = (-0.827621), mov = 5864042, inf = 9.1%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[21 sec, 6193890 itr]: obj = (-0.827621), mov = 6193890, inf = 9.1%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[22 sec, 6506299 itr]: obj = (-0.827621), mov = 6506299, inf = 9%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[23 sec, 6828763 itr]: obj = (-0.827621), mov = 6828763, inf = 9%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[24 sec, 7151483 itr]: obj = (-0.827621), mov = 7151483, inf = 9%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[25 sec, 7467326 itr]: obj = (-0.827621), mov = 7467326, inf = 9%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[26 sec, 7792256 itr]: obj = (-0.827621), mov = 7792256, inf = 9%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[27 sec, 8107873 itr]: obj = (-0.827621), mov = 8107873, inf = 9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[28 sec, 8424706 itr]: obj = (-0.827621), mov = 8424706, inf = 9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[29 sec, 8743731 itr]: obj = (-0.827621), mov = 8743731, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[30 sec, 9053959 itr]: obj = (-0.827621), mov = 9053959, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[31 sec, 9375161 itr]: obj = (-0.827621), mov = 9375161, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[32 sec, 9695930 itr]: obj = (-0.827621), mov = 9695930, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[33 sec, 10010779 itr]: obj = (-0.827621), mov = 10010779, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[34 sec, 10312821 itr]: obj = (-0.827621), mov = 10312821, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[35 sec, 10634890 itr]: obj = (-0.827621), mov = 10634890, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[36 sec, 10950871 itr]: obj = (-0.827621), mov = 10950871, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[37 sec, 11280282 itr]: obj = (-0.827621), mov = 11280282, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[38 sec, 11600633 itr]: obj = (-0.827621), mov = 11600633, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[39 sec, 11919528 itr]: obj = (-0.827621), mov = 11919528, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[40 sec, 12239586 itr]: obj = (-0.827621), mov = 12239586, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[41 sec, 12558252 itr]: obj = (-0.827621), mov = 12558252, inf = 8.9%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[42 sec, 12888417 itr]: obj = (-0.827621), mov = 12888417, inf = 8.8%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[43 sec, 13210385 itr]: obj = (-0.827621), mov = 13210385, inf = 8.8%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[44 sec, 13545237 itr]: obj = (-0.827621), mov = 13545237, inf = 8.8%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[45 sec, 13869327 itr]: obj = (-0.827621), mov = 13869327, inf = 8.8%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[46 sec, 14204614 itr]: obj = (-0.827621), mov = 14204614, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[47 sec, 14539031 itr]: obj = (-0.827621), mov = 14539031, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[48 sec, 14878886 itr]: obj = (-0.827621), mov = 14878886, inf = 8.8%, acc = 15.3%, imp = 846

worst case avg CPI: 3.01664
[49 sec, 15203740 itr]: obj = (-0.827621), mov = 15203740, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[50 sec, 15514344 itr]: obj = (-0.827621), mov = 15514344, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[51 sec, 15836288 itr]: obj = (-0.827621), mov = 15836288, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[52 sec, 16169746 itr]: obj = (-0.827621), mov = 16169746, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[53 sec, 16494242 itr]: obj = (-0.827621), mov = 16494242, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[54 sec, 16791878 itr]: obj = (-0.827621), mov = 16791878, inf = 8.8%, acc = 15.4%, imp = 846

worst case avg CPI: 3.01664
[55 sec, 17106264 itr]: obj = (-0.827621), mov = 17106264, inf = 8.8%, acc = 15.4%, imp = 1179

worst case avg CPI: 3.01664
[56 sec, 17426620 itr]: obj = (-0.827611), mov = 17426620, inf = 8.8%, acc = 15.4%, imp = 1195

worst case avg CPI: 3.0168
[57 sec, 17766187 itr]: obj = (-0.827603), mov = 17766187, inf = 8.8%, acc = 15.4%, imp = 1198

worst case avg CPI: 3.01694
[58 sec, 18107183 itr]: obj = (-0.827603), mov = 18107183, inf = 8.8%, acc = 15.4%, imp = 1198

worst case avg CPI: 3.01694
[59 sec, 18447977 itr]: obj = (-0.827603), mov = 18447977, inf = 8.8%, acc = 15.4%, imp = 1198

worst case avg CPI: 3.01694
[60 sec, 18787960 itr]: obj = (-0.827598), mov = 18787960, inf = 8.8%, acc = 15.4%, imp = 1201

worst case avg CPI: 3.01703
[61 sec, 19131412 itr]: obj = (-0.827598), mov = 19131412, inf = 8.8%, acc = 15.4%, imp = 1201

worst case avg CPI: 3.01703
[62 sec, 19474682 itr]: obj = (-0.827596), mov = 19474682, inf = 8.8%, acc = 15.4%, imp = 1202

worst case avg CPI: 3.01707
[63 sec, 19799802 itr]: obj = (-0.827596), mov = 19799802, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[64 sec, 20140209 itr]: obj = (-0.827596), mov = 20140209, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[65 sec, 20474291 itr]: obj = (-0.827596), mov = 20474291, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[66 sec, 20793387 itr]: obj = (-0.827596), mov = 20793387, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[67 sec, 21115263 itr]: obj = (-0.827596), mov = 21115263, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[68 sec, 21432290 itr]: obj = (-0.827596), mov = 21432290, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[69 sec, 21771061 itr]: obj = (-0.827596), mov = 21771061, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[70 sec, 22095053 itr]: obj = (-0.827596), mov = 22095053, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[71 sec, 22428740 itr]: obj = (-0.827596), mov = 22428740, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[72 sec, 22753933 itr]: obj = (-0.827596), mov = 22753933, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[73 sec, 23074166 itr]: obj = (-0.827596), mov = 23074166, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[74 sec, 23392404 itr]: obj = (-0.827596), mov = 23392404, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[75 sec, 23713695 itr]: obj = (-0.827596), mov = 23713695, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[76 sec, 24035075 itr]: obj = (-0.827596), mov = 24035075, inf = 8.8%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[77 sec, 24351120 itr]: obj = (-0.827596), mov = 24351120, inf = 8.7%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[78 sec, 24680375 itr]: obj = (-0.827596), mov = 24680375, inf = 8.7%, acc = 15.4%, imp = 1203

worst case avg CPI: 3.01707
[79 sec, 25007604 itr]: obj = (-0.827596), mov = 25007604, inf = 8.7%, acc = 15.4%, imp = 1470

worst case avg CPI: 3.01707
[80 sec, 25339791 itr]: obj = (-0.827596), mov = 25339791, inf = 8.7%, acc = 15.4%, imp = 1569

worst case avg CPI: 3.01707
[81 sec, 25674733 itr]: obj = (-0.827596), mov = 25674733, inf = 8.7%, acc = 15.4%, imp = 1570

worst case avg CPI: 3.01707
[82 sec, 26011603 itr]: obj = (-0.827596), mov = 26011603, inf = 8.7%, acc = 15.4%, imp = 1926

worst case avg CPI: 3.01707
[83 sec, 26330890 itr]: obj = (-0.827596), mov = 26330890, inf = 8.7%, acc = 15.4%, imp = 1932

worst case avg CPI: 3.01707
[84 sec, 26657449 itr]: obj = (-0.827596), mov = 26657449, inf = 8.7%, acc = 15.4%, imp = 1933

worst case avg CPI: 3.01707
[85 sec, 26987368 itr]: obj = (-0.827596), mov = 26987368, inf = 8.7%, acc = 15.4%, imp = 1946

worst case avg CPI: 3.01707
[86 sec, 27304831 itr]: obj = (-0.827596), mov = 27304831, inf = 8.7%, acc = 15.4%, imp = 1946

worst case avg CPI: 3.01707
[87 sec, 27625988 itr]: obj = (-0.827596), mov = 27625988, inf = 8.7%, acc = 15.4%, imp = 1946

worst case avg CPI: 3.01707
[88 sec, 27943064 itr]: obj = (-0.827595), mov = 27943064, inf = 8.7%, acc = 15.4%, imp = 1949

worst case avg CPI: 3.0171
[89 sec, 28272803 itr]: obj = (-0.827595), mov = 28272803, inf = 8.7%, acc = 15.4%, imp = 1949

worst case avg CPI: 3.0171
[90 sec, 28602129 itr]: obj = (-0.827595), mov = 28602129, inf = 8.7%, acc = 15.4%, imp = 1949

worst case avg CPI: 3.0171
[90 sec, 28602129 itr]: obj = (-0.827595), mov = 28602129, inf = 8.7%, acc = 15.4%, imp = 1949

worst case avg CPI: 3.0171
28602129 iterations, 28602129 moves performed in 90 seconds
Feasible solution: obj = (-0.827595)

Run output...
FINAL SOLUTION
Worst case avg CPI: 3.0171
Mapping: 0->47, 1->30, 2->8, 3->31, 4->17, 5->34, 6->4, 7->44, 8->42, 9->13, 10->17, 11->44, 12->17, 13->19, 14->38, 15->29, 16->40, 17->26, 18->44, 19->44, 20->35, 21->15, 22->13, 23->38, 24->21, 25->26, 26->36, 27->36, 28->33, 29->7, 30->19, 31->37, 32->1, 33->10, 34->4, 35->3, 36->45, 37->0, 38->6, 39->22, 40->17, 41->30, 42->18, 43->39, 44->10, 45->23, 46->15, 47->2, 48->14, 49->5, 50->5, 51->40, 52->6, 53->3, 54->2, 55->39, 56->19, 57->36, 58->29, 59->22, 60->41, 61->11, 62->41, 63->24, 64->33, 65->23, 66->9, 67->32, 68->39, 69->42, 70->42, 71->18, 72->22, 73->38, 74->8, 75->10, 76->5, 77->42, 78->7, 79->29, 80->4, 81->46, 82->32, 83->26, 84->16, 85->46, 86->47, 87->9, 88->23, 89->21, 90->9, 91->36, 92->29, 93->40, 94->10, 95->28, 96->7, 97->33, 98->0, 99->0, 100->37, 101->6, 102->23, 103->0, 104->13, 105->29, 106->19, 107->14, 108->12, 109->41, 110->9, 111->31, 112->47, 113->18, 114->13, 115->13, 116->4, 117->1, 118->35, 119->16, 120->20, 121->34, 122->2, 123->15, 124->46, 125->1, 126->44, 127->13, 128->46, 129->15, 130->41, 131->35, 132->35, 133->31, 134->38, 135->11, 136->43, 137->45, 138->44, 139->18, 140->2, 141->8, 142->27, 143->22, 144->24, 145->16, 146->25, 147->21, 148->16, 149->6, 150->0, 151->22, 152->35, 153->40, 154->3, 155->19, 156->24, 157->15, 158->26, 159->29, 160->34, 161->31, 162->20, 163->11, 164->24, 165->16, 166->9, 167->17, 168->1, 169->43, 170->43, 171->7, 172->14, 173->25, 174->10, 175->30, 176->21, 177->25, 178->42, 179->37, 180->32, 181->33, 182->40, 183->30, 184->2, 185->27, 186->20, 187->2, 188->3, 189->47, 190->36, 191->19, 192->11, 193->28, 194->14, 195->12, 196->23, 197->43, 198->34, 199->22, 200->14, 201->33, 202->21, 203->28, 204->43, 205->28, 206->16, 207->36, 208->32, 209->45, 210->15, 211->45, 212->46, 213->24, 214->26, 215->37, 216->38, 217->28, 218->27, 219->4, 220->21, 221->12, 222->9, 223->27, 224->11, 225->47, 226->39, 227->0, 228->47, 229->45, 230->7, 231->5, 232->32, 233->6, 234->30, 235->46, 236->20, 237->34, 238->39, 239->27, 240->30, 241->31, 242->24, 243->38, 244->5, 245->41, 246->28, 247->18, 248->26, 249->39, 250->11, 251->25, 252->8, 253->23, 254->8, 255->25, 256->42, 257->12, 258->4, 259->20, 260->12, 261->20, 262->1, 263->10, 264->17, 265->37, 266->37, 267->5, 268->32, 269->1, 270->31, 271->18, 272->34, 273->6, 274->7, 275->14, 276->12, 277->43, 278->40, 279->33, 280->25, 281->35, 282->3, 283->3, 284->27, 285->8, 286->41, 287->45
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.37284	1.01556	1.24926	1.525	1.7329	1.32467
	1.36952	3.01751	3.01716	3.01723	3.01712	1.63139
	1.28967	3.01754	4.81153	3.01882	3.0171	1.24184
	1.38309	3.01801	3.01856	5.66621	4.79015	1.27369
	1.44215	3.01906	3.01759	3.01714	4.89753	0.830459
	1.43998	3.01752	3.01746	5.63723	3.39767	1.58172
	0.996068	3.01712	3.01744	3.01744	3.01716	1.60288
	1.00878	1.55036	1.66224	1.40262	1.08338	1.47264
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 16 --global-tlim 90 --seed 101 --use-acpi-model

Starting the Optimization Process
