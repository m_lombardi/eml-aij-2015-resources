LocalSolver 4.0 (MacOS64, build 20140127)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Average CPI weights for the platform:
 0.5 0.75 0.75 0.75 0.75 0.5 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.5 0.75 0.75 0.75 0.75 0.5

Min CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Neighbor CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Other CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Workload Information:
2.90519, 3.00187, 2.59408, 0.764911, 0.602349, 2.1316, 0.945185, 2.18256, 2.29122, 2.06357, 2.25563, 2.26183, 1.9514, 2.07615, 1.46262, 2.1898, 2.08454, 2.23549, 0.578052, 2.78771, 2.71554, 1.39733, 2.5547, 1.96666, 1.77915, 2.29415, 2.28815, 1.52202, 2.04631, 2.07021, 0.705144, 3.52595, 0.577794, 4.35465, 2.00297, 0.8335, 2.27118, 1.69393, 2.16483, 2.14014, 1.47659, 2.25333, 2.1552, 2.09153, 4.46658, 0.968409, 1.81829, 0.5, 0.671436, 0.5, 3.58689, 5.41061, 1.06563, 0.765436, 2.44583, 0.5, 2.00023, 2.50143, 3.50006, 1.05246, 0.830313, 0.692026, 1.66722, 2.58658, 3.77569, 2.44817, 1.53474, 2.44819, 2.51045, 2.51047, 0.5, 2.49615, 2.07622, 1.75409, 2.42361, 1.82052, 1.43757, 2.48798, 0.674821, 1.93223, 1.22573, 2.59825, 3.23437, 2.3346, 2.02062, 1.56531, 0.685947, 2.50408, 2.53617, 2.68787, 2.18405, 1.74593, 2.38926, 1.61039, 2.05674, 2.01363, 2.79678, 2.81147, 0.814836, 0.5, 2.45418, 2.62273, 0.605606, 3.33616, 0.5, 3.92751, 2.85574, 0.774989, 4.03938, 1.63878, 2.82253, 0.5, 1.6534, 1.3459, 1.54685, 0.5, 2.79143, 2.7573, 1.90179, 2.50264, 3.17346, 1.34045, 1.67046, 0.944843, 3.21848, 1.65231, 0.5, 2.50508, 3.06415, 1.40992, 2.53316, 1.98769, 2.20824, 1.55341, 0.511615, 3.33439, 2.34796, 2.04439, 2.065, 2.08406, 2.0553, 2.22587, 1.59248, 1.97728, 1.10884, 2.33107, 0.734037, 1.20233, 5.75006, 0.873654, 2.11678, 2.13263, 1.99938, 2.2624, 1.61199, 1.87683, 1.92695, 2.1557, 2.19942, 2.03818, 1.83717, 1.84259, 1.06903, 1.63587, 2.1269, 2.24311, 3.16967, 1.75542, 2.27664, 2.31265, 2.80435, 1.44689, 1.45856, 1.7009, 3.03613, 0.892094, 1.26678, 3.03981, 2.10236, 1.66282, 0.894979, 0.940761, 1.88205, 0.974263, 2.63175, 4.67619, 2.46838, 1.70812, 2.64584, 1.40925, 1.87971, 1.88871, 35, 27.3053, 9.19818, 23.0218, 9.20599, 28.2687, 3.11393, 1.54415, 2.22701, 2.23798, 2.37693, 0.5, 35, 12.4549, 16.6137, 25.8873, 23.624, 18.4201, 3.5718, 2.28371, 0.5, 0.566681, 1.96172, 3.11608, 4.6502, 0.651365, 0.5, 0.509514, 2.67246, 3.01647, 0.959727, 0.5, 3.60859, 0.857319, 1.36745, 4.70692, 1.79929, 2.43769, 1.7069, 1.48299, 1.99055, 2.58259, 1.43298, 1.11253, 3.52307, 3.52528, 1.46405, 0.942092, 0.664891, 1.93508, 3.20626, 3.19041, 1.50792, 1.49543, 2.18655, 1.25744, 2.12228, 2.26964, 2.25767, 1.90641, 0.52902, 2.58977, 3.92245, 0.5, 0.607089, 3.85167, 0.5, 2.51582, 3.21189, 1.11241, 1.68973, 2.97016, 30.6793, 35, 4.2256, 22.2669, 31.2751, 8.55314, 0.766831, 3.50378, 1.21244, 1.36904, 1.13738, 4.01053, 28.0884, 20.8486, 15.6235, 35, 19.1319, 13.3075, 3.14793, 0.715389, 2.92986, 3.64648, 1.06034, 0.5
cpi min/max: 0.5/35
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 42811, operands = 113952
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0.756273


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 90801 itr]: obj = (-0.74803), mov = 90801, inf = 42.1%, acc = 25.2%, imp = 302

worst case avg CPI: 4.40948
[2 sec, 245679 itr]: obj = (-0.743833), mov = 245679, inf = 25%, acc = 30.5%, imp = 474

worst case avg CPI: 4.48293
[3 sec, 417997 itr]: obj = (-0.743358), mov = 417997, inf = 20.1%, acc = 31.1%, imp = 485

worst case avg CPI: 4.49123
[4 sec, 597718 itr]: obj = (-0.743358), mov = 597718, inf = 18%, acc = 31.4%, imp = 485

worst case avg CPI: 4.49123
[5 sec, 777017 itr]: obj = (-0.743205), mov = 777017, inf = 16.9%, acc = 31.6%, imp = 489

worst case avg CPI: 4.49392
[6 sec, 951639 itr]: obj = (-0.742567), mov = 951639, inf = 16.2%, acc = 31.8%, imp = 491

worst case avg CPI: 4.50507
[7 sec, 1129903 itr]: obj = (-0.742567), mov = 1129903, inf = 15.7%, acc = 31.8%, imp = 491

worst case avg CPI: 4.50507
[8 sec, 1311546 itr]: obj = (-0.74232), mov = 1311546, inf = 15.3%, acc = 31.8%, imp = 494

worst case avg CPI: 4.5094
[9 sec, 1491690 itr]: obj = (-0.74232), mov = 1491690, inf = 15%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[10 sec, 1662997 itr]: obj = (-0.74232), mov = 1662997, inf = 14.8%, acc = 31.8%, imp = 494

worst case avg CPI: 4.5094
[11 sec, 1835716 itr]: obj = (-0.74232), mov = 1835716, inf = 14.6%, acc = 31.8%, imp = 494

worst case avg CPI: 4.5094
[12 sec, 2011019 itr]: obj = (-0.74232), mov = 2011019, inf = 14.4%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[13 sec, 2183764 itr]: obj = (-0.74232), mov = 2183764, inf = 14.3%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[14 sec, 2356438 itr]: obj = (-0.74232), mov = 2356438, inf = 14.2%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[15 sec, 2533340 itr]: obj = (-0.74232), mov = 2533340, inf = 14.1%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[16 sec, 2709108 itr]: obj = (-0.74232), mov = 2709108, inf = 14%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[17 sec, 2888418 itr]: obj = (-0.74232), mov = 2888418, inf = 13.9%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[18 sec, 3068232 itr]: obj = (-0.74232), mov = 3068232, inf = 13.9%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[19 sec, 3249371 itr]: obj = (-0.74232), mov = 3249371, inf = 13.8%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[20 sec, 3426614 itr]: obj = (-0.74232), mov = 3426614, inf = 13.8%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[21 sec, 3603604 itr]: obj = (-0.74232), mov = 3603604, inf = 13.7%, acc = 31.7%, imp = 494

worst case avg CPI: 4.5094
[22 sec, 3790250 itr]: obj = (-0.731644), mov = 3790250, inf = 13.6%, acc = 31.6%, imp = 711

worst case avg CPI: 4.69623
[23 sec, 3981605 itr]: obj = (-0.72987), mov = 3981605, inf = 13.6%, acc = 31.4%, imp = 731

worst case avg CPI: 4.72727
[24 sec, 4173586 itr]: obj = (-0.729587), mov = 4173586, inf = 13.5%, acc = 31.3%, imp = 737

worst case avg CPI: 4.73222
[25 sec, 4365575 itr]: obj = (-0.729587), mov = 4365575, inf = 13.5%, acc = 31.1%, imp = 737

worst case avg CPI: 4.73222
[26 sec, 4551934 itr]: obj = (-0.729539), mov = 4551934, inf = 13.4%, acc = 30.9%, imp = 739

worst case avg CPI: 4.73306
[27 sec, 4743940 itr]: obj = (-0.729539), mov = 4743940, inf = 13.4%, acc = 30.8%, imp = 739

worst case avg CPI: 4.73306
[28 sec, 4938160 itr]: obj = (-0.729539), mov = 4938160, inf = 13.3%, acc = 30.6%, imp = 739

worst case avg CPI: 4.73306
[29 sec, 5131260 itr]: obj = (-0.729539), mov = 5131260, inf = 13.3%, acc = 30.5%, imp = 739

worst case avg CPI: 4.73306
[30 sec, 5324691 itr]: obj = (-0.729539), mov = 5324691, inf = 13.3%, acc = 30.4%, imp = 739

worst case avg CPI: 4.73306
[31 sec, 5516018 itr]: obj = (-0.729539), mov = 5516018, inf = 13.2%, acc = 30.3%, imp = 739

worst case avg CPI: 4.73306
[32 sec, 5706457 itr]: obj = (-0.729539), mov = 5706457, inf = 13.2%, acc = 30.2%, imp = 739

worst case avg CPI: 4.73306
[33 sec, 5899777 itr]: obj = (-0.729539), mov = 5899777, inf = 13.1%, acc = 30.1%, imp = 739

worst case avg CPI: 4.73306
[34 sec, 6095699 itr]: obj = (-0.729539), mov = 6095699, inf = 13.1%, acc = 30%, imp = 739

worst case avg CPI: 4.73306
[35 sec, 6287912 itr]: obj = (-0.729539), mov = 6287912, inf = 13.1%, acc = 29.9%, imp = 739

worst case avg CPI: 4.73306
[36 sec, 6480319 itr]: obj = (-0.729539), mov = 6480319, inf = 13%, acc = 29.9%, imp = 930

worst case avg CPI: 4.73306
[37 sec, 6672338 itr]: obj = (-0.729539), mov = 6672338, inf = 13%, acc = 29.8%, imp = 941

worst case avg CPI: 4.73306
[38 sec, 6869252 itr]: obj = (-0.729539), mov = 6869252, inf = 13%, acc = 29.7%, imp = 953

worst case avg CPI: 4.73306
[39 sec, 7065378 itr]: obj = (-0.729339), mov = 7065378, inf = 13%, acc = 29.7%, imp = 969

worst case avg CPI: 4.73657
[40 sec, 7262337 itr]: obj = (-0.729339), mov = 7262337, inf = 12.9%, acc = 29.6%, imp = 970

worst case avg CPI: 4.73657
[41 sec, 7457439 itr]: obj = (-0.729339), mov = 7457439, inf = 12.9%, acc = 29.5%, imp = 970

worst case avg CPI: 4.73657
[42 sec, 7651565 itr]: obj = (-0.729339), mov = 7651565, inf = 12.9%, acc = 29.5%, imp = 970

worst case avg CPI: 4.73657
[43 sec, 7852542 itr]: obj = (-0.729339), mov = 7852542, inf = 12.9%, acc = 29.4%, imp = 970

worst case avg CPI: 4.73657
[44 sec, 8051060 itr]: obj = (-0.729339), mov = 8051060, inf = 12.8%, acc = 29.4%, imp = 970

worst case avg CPI: 4.73657
[45 sec, 8252653 itr]: obj = (-0.729339), mov = 8252653, inf = 12.8%, acc = 29.3%, imp = 970

worst case avg CPI: 4.73657
[46 sec, 8451096 itr]: obj = (-0.729339), mov = 8451096, inf = 12.8%, acc = 29.3%, imp = 1139

worst case avg CPI: 4.73657
[47 sec, 8643984 itr]: obj = (-0.729339), mov = 8643984, inf = 12.8%, acc = 29.4%, imp = 1324

worst case avg CPI: 4.73657
[48 sec, 8843489 itr]: obj = (-0.729339), mov = 8843489, inf = 12.8%, acc = 29.4%, imp = 1349

worst case avg CPI: 4.73657
[49 sec, 9045773 itr]: obj = (-0.729339), mov = 9045773, inf = 12.7%, acc = 29.3%, imp = 1357

worst case avg CPI: 4.73657
[50 sec, 9244626 itr]: obj = (-0.729339), mov = 9244626, inf = 12.7%, acc = 29.3%, imp = 1357

worst case avg CPI: 4.73657
[51 sec, 9447264 itr]: obj = (-0.729339), mov = 9447264, inf = 12.7%, acc = 29.3%, imp = 1357

worst case avg CPI: 4.73657
[52 sec, 9647966 itr]: obj = (-0.729339), mov = 9647966, inf = 12.7%, acc = 29.2%, imp = 1525

worst case avg CPI: 4.73657
[53 sec, 9845450 itr]: obj = (-0.729339), mov = 9845450, inf = 12.6%, acc = 29.2%, imp = 1575

worst case avg CPI: 4.73657
[54 sec, 10048875 itr]: obj = (-0.729339), mov = 10048875, inf = 12.6%, acc = 29.2%, imp = 1587

worst case avg CPI: 4.73657
[55 sec, 10254956 itr]: obj = (-0.729339), mov = 10254956, inf = 12.6%, acc = 29.1%, imp = 1587

worst case avg CPI: 4.73657
[56 sec, 10456582 itr]: obj = (-0.729339), mov = 10456582, inf = 12.6%, acc = 29.1%, imp = 1587

worst case avg CPI: 4.73657
[57 sec, 10658417 itr]: obj = (-0.729339), mov = 10658417, inf = 12.5%, acc = 29.1%, imp = 1587

worst case avg CPI: 4.73657
[58 sec, 10858350 itr]: obj = (-0.729339), mov = 10858350, inf = 12.5%, acc = 29.1%, imp = 1777

worst case avg CPI: 4.73657
[59 sec, 11061666 itr]: obj = (-0.729339), mov = 11061666, inf = 12.5%, acc = 29%, imp = 1786

worst case avg CPI: 4.73657
[60 sec, 11267355 itr]: obj = (-0.729339), mov = 11267355, inf = 12.5%, acc = 29%, imp = 2012

worst case avg CPI: 4.73657
[61 sec, 11475349 itr]: obj = (-0.729339), mov = 11475349, inf = 12.4%, acc = 29%, imp = 2017

worst case avg CPI: 4.73657
[62 sec, 11683093 itr]: obj = (-0.729339), mov = 11683093, inf = 12.4%, acc = 29%, imp = 2024

worst case avg CPI: 4.73657
[63 sec, 11890542 itr]: obj = (-0.729339), mov = 11890542, inf = 12.4%, acc = 28.9%, imp = 2024

worst case avg CPI: 4.73657
[64 sec, 12094408 itr]: obj = (-0.729339), mov = 12094408, inf = 12.4%, acc = 28.9%, imp = 2024

worst case avg CPI: 4.73657
[65 sec, 12300545 itr]: obj = (-0.729339), mov = 12300545, inf = 12.3%, acc = 28.9%, imp = 2024

worst case avg CPI: 4.73657
[66 sec, 12508146 itr]: obj = (-0.729339), mov = 12508146, inf = 12.3%, acc = 28.9%, imp = 2024

worst case avg CPI: 4.73657
[67 sec, 12711570 itr]: obj = (-0.729339), mov = 12711570, inf = 12.3%, acc = 28.9%, imp = 2196

worst case avg CPI: 4.73657
[68 sec, 12914375 itr]: obj = (-0.729339), mov = 12914375, inf = 12.3%, acc = 28.8%, imp = 2219

worst case avg CPI: 4.73657
[69 sec, 13123146 itr]: obj = (-0.729339), mov = 13123146, inf = 12.3%, acc = 28.8%, imp = 2219

worst case avg CPI: 4.73657
[70 sec, 13329200 itr]: obj = (-0.729339), mov = 13329200, inf = 12.3%, acc = 28.8%, imp = 2219

worst case avg CPI: 4.73657
[71 sec, 13526234 itr]: obj = (-0.729339), mov = 13526234, inf = 12.3%, acc = 28.8%, imp = 2362

worst case avg CPI: 4.73657
[72 sec, 13732760 itr]: obj = (-0.729339), mov = 13732760, inf = 12.2%, acc = 28.8%, imp = 2436

worst case avg CPI: 4.73657
[73 sec, 13941212 itr]: obj = (-0.729339), mov = 13941212, inf = 12.2%, acc = 28.8%, imp = 2436

worst case avg CPI: 4.73657
[74 sec, 14154590 itr]: obj = (-0.729339), mov = 14154590, inf = 12.2%, acc = 28.8%, imp = 2436

worst case avg CPI: 4.73657
[75 sec, 14366891 itr]: obj = (-0.729339), mov = 14366891, inf = 12.2%, acc = 28.8%, imp = 2628

worst case avg CPI: 4.73657
[76 sec, 14583460 itr]: obj = (-0.729339), mov = 14583460, inf = 12.2%, acc = 28.8%, imp = 2639

worst case avg CPI: 4.73657
[77 sec, 14795255 itr]: obj = (-0.729339), mov = 14795255, inf = 12.1%, acc = 28.8%, imp = 2811

worst case avg CPI: 4.73657
[78 sec, 15002035 itr]: obj = (-0.729339), mov = 15002035, inf = 12.1%, acc = 28.7%, imp = 2844

worst case avg CPI: 4.73657
[79 sec, 15212557 itr]: obj = (-0.729339), mov = 15212557, inf = 12.1%, acc = 28.7%, imp = 2846

worst case avg CPI: 4.73657
[80 sec, 15424486 itr]: obj = (-0.729339), mov = 15424486, inf = 12.1%, acc = 28.7%, imp = 2846

worst case avg CPI: 4.73657
[81 sec, 15635579 itr]: obj = (-0.729339), mov = 15635579, inf = 12.1%, acc = 28.7%, imp = 2846

worst case avg CPI: 4.73657
[82 sec, 15847346 itr]: obj = (-0.729339), mov = 15847346, inf = 12.1%, acc = 28.7%, imp = 3030

worst case avg CPI: 4.73657
[83 sec, 16058409 itr]: obj = (-0.729339), mov = 16058409, inf = 12%, acc = 28.7%, imp = 3057

worst case avg CPI: 4.73657
[84 sec, 16271761 itr]: obj = (-0.729339), mov = 16271761, inf = 12%, acc = 28.7%, imp = 3057

worst case avg CPI: 4.73657
[85 sec, 16486780 itr]: obj = (-0.729333), mov = 16486780, inf = 12%, acc = 28.7%, imp = 3060

worst case avg CPI: 4.73667
[86 sec, 16703913 itr]: obj = (-0.729315), mov = 16703913, inf = 12%, acc = 28.7%, imp = 3062

worst case avg CPI: 4.73699
[87 sec, 16919586 itr]: obj = (-0.729315), mov = 16919586, inf = 12%, acc = 28.6%, imp = 3062

worst case avg CPI: 4.73699
[88 sec, 17133684 itr]: obj = (-0.729315), mov = 17133684, inf = 12%, acc = 28.6%, imp = 3062

worst case avg CPI: 4.73699
[89 sec, 17345670 itr]: obj = (-0.729315), mov = 17345670, inf = 12%, acc = 28.6%, imp = 3062

worst case avg CPI: 4.73699
[90 sec, 17562724 itr]: obj = (-0.729315), mov = 17562724, inf = 11.9%, acc = 28.6%, imp = 3062

worst case avg CPI: 4.73699
[90 sec, 17562724 itr]: obj = (-0.729315), mov = 17562724, inf = 11.9%, acc = 28.6%, imp = 3062

worst case avg CPI: 4.73699
17562724 iterations, 17562724 moves performed in 90 seconds
Feasible solution: obj = (-0.729315)

Run output...
FINAL SOLUTION
Worst case avg CPI: 4.73699
Mapping: 0->11, 1->36, 2->29, 3->46, 4->26, 5->21, 6->2, 7->4, 8->5, 9->33, 10->6, 11->34, 12->16, 13->42, 14->1, 15->28, 16->38, 17->25, 18->30, 19->7, 20->35, 21->9, 22->9, 23->26, 24->12, 25->45, 26->4, 27->37, 28->29, 29->8, 30->16, 31->15, 32->18, 33->31, 34->3, 35->3, 36->28, 37->8, 38->2, 39->30, 40->32, 41->0, 42->20, 43->8, 44->15, 45->42, 46->6, 47->43, 48->45, 49->36, 50->13, 51->10, 52->34, 53->7, 54->40, 55->46, 56->37, 57->39, 58->28, 59->37, 60->1, 61->44, 62->43, 63->24, 64->15, 65->18, 66->20, 67->7, 68->6, 69->38, 70->5, 71->9, 72->9, 73->47, 74->43, 75->35, 76->25, 77->43, 78->8, 79->32, 80->46, 81->14, 82->38, 83->35, 84->29, 85->23, 86->36, 87->43, 88->7, 89->46, 90->37, 91->11, 92->6, 93->41, 94->2, 95->17, 96->17, 97->47, 98->18, 99->24, 100->28, 101->8, 102->11, 103->22, 104->19, 105->10, 106->47, 107->45, 108->31, 109->30, 110->23, 111->41, 112->42, 113->45, 114->23, 115->24, 116->5, 117->19, 118->41, 119->28, 120->5, 121->46, 122->42, 123->27, 124->39, 125->34, 126->1, 127->40, 128->29, 129->44, 130->35, 131->29, 132->21, 133->41, 134->2, 135->22, 136->20, 137->29, 138->21, 139->18, 140->33, 141->7, 142->24, 143->44, 144->40, 145->1, 146->35, 147->25, 148->10, 149->44, 150->33, 151->0, 152->38, 153->14, 154->0, 155->25, 156->5, 157->0, 158->16, 159->38, 160->32, 161->40, 162->44, 163->44, 164->45, 165->27, 166->4, 167->1, 168->14, 169->25, 170->16, 171->42, 172->37, 173->26, 174->18, 175->17, 176->40, 177->22, 178->19, 179->1, 180->12, 181->23, 182->17, 183->30, 184->21, 185->10, 186->34, 187->21, 188->45, 189->43, 190->24, 191->20, 192->40, 193->19, 194->13, 195->33, 196->15, 197->37, 198->12, 199->11, 200->2, 201->11, 202->32, 203->17, 204->26, 205->22, 206->38, 207->16, 208->34, 209->21, 210->13, 211->35, 212->23, 213->19, 214->32, 215->39, 216->10, 217->11, 218->47, 219->3, 220->3, 221->41, 222->30, 223->12, 224->31, 225->14, 226->26, 227->13, 228->12, 229->3, 230->36, 231->14, 232->6, 233->19, 234->3, 235->6, 236->13, 237->15, 238->27, 239->47, 240->0, 241->16, 242->22, 243->39, 244->27, 245->33, 246->42, 247->27, 248->5, 249->30, 250->46, 251->36, 252->20, 253->36, 254->15, 255->23, 256->0, 257->13, 258->26, 259->4, 260->39, 261->41, 262->47, 263->12, 264->7, 265->9, 266->31, 267->27, 268->25, 269->31, 270->2, 271->22, 272->4, 273->17, 274->34, 275->10, 276->8, 277->14, 278->28, 279->20, 280->32, 281->39, 282->33, 283->4, 284->9, 285->31, 286->18, 287->24
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.57094	1.42371	1.44537	1.64818	2.014	2.13422
	2.01279	6.90711	6.20694	7.74238	4.73752	1.61504
	1.84292	4.73976	5.05437	4.73699	5.91378	1.5756
	1.67022	5.96904	7.2426	4.8608	4.81256	1.31257
	1.25979	6.72333	6.85623	4.94737	4.7569	2.29287
	1.42677	4.73797	4.7861	5.65014	5.36826	2.07026
	1.73182	6.08101	4.74677	4.75764	7.36152	1.61574
	1.66698	1.83202	1.2763	1.6432	1.46277	1.75885
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 12 --global-tlim 90 --seed 101 --use-acpi-model

Starting the Optimization Process
