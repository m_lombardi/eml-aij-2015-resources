LocalSolver 4.0 (MacOS64, build 20140127)
Copyright (C) 2013 Bouygues SA, Aix-Marseille University, CNRS.
All rights reserved (see Terms and Conditions for details).

Load localsolver_res/tawd_ls.lsp...
Run input...
Average CPI weights for the platform:
 0.5 0.75 0.75 0.75 0.75 0.5 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.75 1 1 1 1 0.75 0.5 0.75 0.75 0.75 0.75 0.5

Min CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Neighbor CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Other CPI weights for the platform:
 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0

Workload Information:
0.5, 3.81296, 3.87583, 2.27713, 1.03321, 0.500884, 1.62568, 1.80856, 1.09966, 2.84771, 1.76371, 2.85469, 1.17437, 2.58049, 1.80979, 2.67232, 1.28661, 2.47642, 1.95892, 1.85795, 1.92912, 2.19539, 2.04301, 2.01562, 2.20498, 2.19371, 2.01148, 1.50578, 2.07573, 2.00832, 0.84475, 3.40524, 2.69364, 1.4952, 0.5, 3.06118, 2.70627, 2.36264, 1.8097, 1.10072, 1.22972, 2.79096, 0.647386, 1.61021, 2.33738, 0.5, 1.14249, 5.76253, 1.72541, 2.42155, 1.60517, 1.01494, 3.29655, 1.93639, 4.99356, 1.79488, 1.67445, 0.580958, 2.14963, 0.806525, 2.0154, 2.61515, 1.24017, 1.8084, 1.91464, 2.40624, 0.806722, 1.47745, 2.20499, 3.51697, 3.2042, 0.789676, 0.59456, 0.96265, 2.6067, 2.31696, 4.87612, 0.643004, 3.43285, 0.620382, 1.21686, 3.3258, 1.96387, 1.44024, 0.94898, 0.5, 2.9513, 2.93378, 2.57022, 2.09572, 0.5, 0.803253, 2.6402, 1.36485, 2.10134, 4.59036, 4.34096, 0.763328, 2.03923, 3.41068, 0.675672, 0.770143, 1.67581, 1.22702, 1.90911, 5.07827, 1.60978, 0.5, 0.5, 2.2436, 1.37725, 3.70589, 2.56693, 1.60633, 1.28532, 3.34844, 2.04848, 1.70325, 3.11451, 0.5, 3.8257, 2.66265, 2.54595, 1.1641, 0.5, 1.30161, 2.53083, 2.19387, 2.97221, 2.09707, 1.40411, 0.801911, 2.8914, 2.64037, 3.34792, 2.11958, 0.5, 0.500726, 3.2597, 3.62041, 2.92232, 0.5, 0.666623, 1.03095, 0.5, 2.42712, 1.97779, 3.12511, 1.82064, 2.14934, 4.48429, 1.75809, 1.56529, 1.12895, 1.55094, 1.51243, 1.7981, 2.43541, 2.2263, 2.13666, 1.52789, 1.87564, 3.47137, 0.836303, 3.2841, 0.5, 3.22641, 0.681832, 2.96647, 1.01755, 2.6172, 1.64574, 2.97296, 0.780062, 2.58977, 2.69655, 2.12138, 1.87539, 1.42695, 1.28997, 2.12806, 2.34382, 2.54858, 1.79019, 2.66604, 0.523303, 2.29381, 2.06349, 1.60978, 1.89733, 1.89669, 2.23891, 2.79868, 3.03558, 2.81649, 1.137, 0.5, 1.71225, 1.36767, 2.66207, 0.611308, 2.16961, 2.12982, 3.05952, 2.52021, 2.09371, 2.37688, 2.11109, 0.698712, 2.19939, 0.5, 1.38675, 4.78868, 0.827206, 0.83915, 3.65821, 0.5, 3.40403, 3.53304, 2.72326, 1.07935, 0.760316, 1.82307, 1.24257, 1.66177, 2.36872, 2.60941, 2.29446, 1.9379, 1.82532, 2.37006, 2.32553, 2.08922, 1.45196, 2.46326, 1.26431, 0.5, 1.20221, 5.76015, 0.810084, 0.632594, 0.603053, 3.04023, 1.39346, 0.89507, 5.43559, 4.12522, 1.91597, 0.5, 0.776778, 3.94175, 0.740289, 3.29175, 4.13465, 1.61764, 1.51594, 0.793463, 0.646556, 3.12918, 4.30524, 1.54304, 0.728343, 0.5, 1.7942, 0.5, 0.54231, 1.24576, 3.8789, 4.25341, 1.57962, 1.22202, 4.17073, 0.58695, 2.40113, 0.756322, 2.86285, 0.5, 2.51082, 2.69678, 1.02573, 2.18458, 3.08209, 3.15324, 1.74242, 2.93218, 1.25367, 0.863003, 2.05549
cpi min/max: 0.5/5.76253
neighbors of 0: 1 6, number of others: 45
neighbors of 1: 0 2 6 7, number of others: 43
neighbors of 2: 1 3 7 8, number of others: 43
neighbors of 3: 2 4 8 9, number of others: 43
neighbors of 4: 3 5 9 10, number of others: 43
neighbors of 5: 4 10 11, number of others: 44
neighbors of 6: 0 1 7 12, number of others: 43
neighbors of 7: 1 2 6 8 12 13, number of others: 41
neighbors of 8: 2 3 7 9 13 14, number of others: 41
neighbors of 9: 3 4 8 10 14 15, number of others: 41
neighbors of 10: 4 5 9 11 15 16, number of others: 41
neighbors of 11: 5 10 16 17, number of others: 43
neighbors of 12: 6 7 13 18, number of others: 43
neighbors of 13: 7 8 12 14 18 19, number of others: 41
neighbors of 14: 8 9 13 15 19 20, number of others: 41
neighbors of 15: 9 10 14 16 20 21, number of others: 41
neighbors of 16: 10 11 15 17 21 22, number of others: 41
neighbors of 17: 11 16 22 23, number of others: 43
neighbors of 18: 12 13 19 24, number of others: 43
neighbors of 19: 13 14 18 20 24 25, number of others: 41
neighbors of 20: 14 15 19 21 25 26, number of others: 41
neighbors of 21: 15 16 20 22 26 27, number of others: 41
neighbors of 22: 16 17 21 23 27 28, number of others: 41
neighbors of 23: 17 22 28 29, number of others: 43
neighbors of 24: 18 19 25 30, number of others: 43
neighbors of 25: 19 20 24 26 30 31, number of others: 41
neighbors of 26: 20 21 25 27 31 32, number of others: 41
neighbors of 27: 21 22 26 28 32 33, number of others: 41
neighbors of 28: 22 23 27 29 33 34, number of others: 41
neighbors of 29: 23 28 34 35, number of others: 43
neighbors of 30: 24 25 31 36, number of others: 43
neighbors of 31: 25 26 30 32 36 37, number of others: 41
neighbors of 32: 26 27 31 33 37 38, number of others: 41
neighbors of 33: 27 28 32 34 38 39, number of others: 41
neighbors of 34: 28 29 33 35 39 40, number of others: 41
neighbors of 35: 29 34 40 41, number of others: 43
neighbors of 36: 30 31 37 42, number of others: 43
neighbors of 37: 31 32 36 38 42 43, number of others: 41
neighbors of 38: 32 33 37 39 43 44, number of others: 41
neighbors of 39: 33 34 38 40 44 45, number of others: 41
neighbors of 40: 34 35 39 41 45 46, number of others: 41
neighbors of 41: 35 40 46 47, number of others: 43
neighbors of 42: 36 37 43, number of others: 44
neighbors of 43: 37 38 42 44, number of others: 43
neighbors of 44: 38 39 43 45, number of others: 43
neighbors of 45: 39 40 44 46, number of others: 43
neighbors of 46: 40 41 45 47, number of others: 43
neighbors of 47: 41 46, number of others: 45
Run model...
Preprocess model 0% ...Preprocess model 16% ...Preprocess model 32% ...Preprocess model 48% ...Preprocess model 66% ...Preprocess model 84% ...Preprocess model 100% ...
Close model 0% ...Close model 9% ...Close model 18% ...Close model 27% ...Close model 36% ...Close model 45% ...Close model 54% ...Close model 63% ...Close model 72% ...Close model 81% ...Close model 91% ...Close model 100% ...
Run param...
Run solver...
Initialize threads 0% ...Initialize threads 100% ...
Push initial solutions 0% ...Push initial solutions 100% ...

Model: 
  expressions = 42810, operands = 113952
  decisions = 13824, constraints = 336, objectives = 1

Param: 
  time limit = 90 sec, no iteration limit
  seed = 101, nb threads = 1, annealing level = 1

Objectives:
  Obj 0: maximize, no bound

Phases:
  Phase 0: time limit = 90 sec, no iteration limit, optimized objective = 0


Computed bounds:
  Obj 0: upper bound = 0


Phase 0:
[0 sec, 0 itr]: infeas = (336, 336, 576), mov = 0, inf < 0.1%, acc < 0.1%, imp = 0

worst case avg CPI: 17.5
[1 sec, 140558 itr]: obj = (-0.836445), mov = 140558, inf = 21.8%, acc = 10%, imp = 852

worst case avg CPI: 2.86221
[2 sec, 449882 itr]: obj = (-0.836183), mov = 449882, inf = 13.9%, acc = 9.7%, imp = 907

worst case avg CPI: 2.8668
[3 sec, 757562 itr]: obj = (-0.836183), mov = 757562, inf = 12%, acc = 9.7%, imp = 907

worst case avg CPI: 2.8668
[4 sec, 1066801 itr]: obj = (-0.836183), mov = 1066801, inf = 11.1%, acc = 9.8%, imp = 1308

worst case avg CPI: 2.8668
[5 sec, 1381800 itr]: obj = (-0.836183), mov = 1381800, inf = 10.6%, acc = 9.8%, imp = 1375

worst case avg CPI: 2.8668
[6 sec, 1679720 itr]: obj = (-0.836183), mov = 1679720, inf = 10.3%, acc = 9.8%, imp = 1377

worst case avg CPI: 2.8668
[7 sec, 1982775 itr]: obj = (-0.836183), mov = 1982775, inf = 10.1%, acc = 9.8%, imp = 1379

worst case avg CPI: 2.8668
[8 sec, 2271432 itr]: obj = (-0.836183), mov = 2271432, inf = 9.9%, acc = 9.8%, imp = 1379

worst case avg CPI: 2.8668
[9 sec, 2574570 itr]: obj = (-0.836183), mov = 2574570, inf = 9.8%, acc = 9.8%, imp = 1380

worst case avg CPI: 2.8668
[10 sec, 2862079 itr]: obj = (-0.836183), mov = 2862079, inf = 9.7%, acc = 9.8%, imp = 1387

worst case avg CPI: 2.8668
[11 sec, 3153488 itr]: obj = (-0.836183), mov = 3153488, inf = 9.7%, acc = 9.8%, imp = 1388

worst case avg CPI: 2.8668
[12 sec, 3444608 itr]: obj = (-0.836183), mov = 3444608, inf = 9.6%, acc = 9.8%, imp = 1389

worst case avg CPI: 2.8668
[13 sec, 3754217 itr]: obj = (-0.836183), mov = 3754217, inf = 9.5%, acc = 9.8%, imp = 1389

worst case avg CPI: 2.8668
[14 sec, 4054837 itr]: obj = (-0.836183), mov = 4054837, inf = 9.5%, acc = 9.8%, imp = 1389

worst case avg CPI: 2.8668
[15 sec, 4354352 itr]: obj = (-0.836183), mov = 4354352, inf = 9.4%, acc = 9.8%, imp = 1389

worst case avg CPI: 2.8668
[16 sec, 4638462 itr]: obj = (-0.836183), mov = 4638462, inf = 9.4%, acc = 9.8%, imp = 1390

worst case avg CPI: 2.8668
[17 sec, 4953083 itr]: obj = (-0.836183), mov = 4953083, inf = 9.4%, acc = 9.8%, imp = 1390

worst case avg CPI: 2.8668
[18 sec, 5249084 itr]: obj = (-0.836183), mov = 5249084, inf = 9.3%, acc = 9.8%, imp = 1390

worst case avg CPI: 2.8668
[19 sec, 5545373 itr]: obj = (-0.83618), mov = 5545373, inf = 9.3%, acc = 9.8%, imp = 1391

worst case avg CPI: 2.86685
[20 sec, 5856194 itr]: obj = (-0.83618), mov = 5856194, inf = 9.3%, acc = 9.8%, imp = 1391

worst case avg CPI: 2.86685
[21 sec, 6168904 itr]: obj = (-0.83618), mov = 6168904, inf = 9.3%, acc = 9.8%, imp = 1392

worst case avg CPI: 2.86685
[22 sec, 6480488 itr]: obj = (-0.83618), mov = 6480488, inf = 9.2%, acc = 9.8%, imp = 1392

worst case avg CPI: 2.86685
[23 sec, 6784017 itr]: obj = (-0.836177), mov = 6784017, inf = 9.2%, acc = 9.8%, imp = 1394

worst case avg CPI: 2.8669
[24 sec, 7096857 itr]: obj = (-0.836175), mov = 7096857, inf = 9.2%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[25 sec, 7403817 itr]: obj = (-0.836175), mov = 7403817, inf = 9.2%, acc = 9.8%, imp = 1396

worst case avg CPI: 2.86694
[26 sec, 7712901 itr]: obj = (-0.836175), mov = 7712901, inf = 9.2%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[27 sec, 8020238 itr]: obj = (-0.836175), mov = 8020238, inf = 9.2%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[28 sec, 8327423 itr]: obj = (-0.836175), mov = 8327423, inf = 9.2%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[29 sec, 8632158 itr]: obj = (-0.836175), mov = 8632158, inf = 9.2%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[30 sec, 8946865 itr]: obj = (-0.836175), mov = 8946865, inf = 9.2%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[31 sec, 9260870 itr]: obj = (-0.836175), mov = 9260870, inf = 9.2%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[32 sec, 9571628 itr]: obj = (-0.836175), mov = 9571628, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[33 sec, 9886512 itr]: obj = (-0.836175), mov = 9886512, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[34 sec, 10201866 itr]: obj = (-0.836175), mov = 10201866, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[35 sec, 10519018 itr]: obj = (-0.836175), mov = 10519018, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[36 sec, 10830025 itr]: obj = (-0.836175), mov = 10830025, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[37 sec, 11144106 itr]: obj = (-0.836175), mov = 11144106, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[38 sec, 11462630 itr]: obj = (-0.836175), mov = 11462630, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[39 sec, 11767329 itr]: obj = (-0.836175), mov = 11767329, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[40 sec, 12074023 itr]: obj = (-0.836175), mov = 12074023, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[41 sec, 12381992 itr]: obj = (-0.836175), mov = 12381992, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[42 sec, 12688172 itr]: obj = (-0.836175), mov = 12688172, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[43 sec, 12993093 itr]: obj = (-0.836175), mov = 12993093, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[44 sec, 13303745 itr]: obj = (-0.836175), mov = 13303745, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[45 sec, 13611521 itr]: obj = (-0.836175), mov = 13611521, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[46 sec, 13922502 itr]: obj = (-0.836175), mov = 13922502, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[47 sec, 14232163 itr]: obj = (-0.836175), mov = 14232163, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[48 sec, 14539848 itr]: obj = (-0.836175), mov = 14539848, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[49 sec, 14845907 itr]: obj = (-0.836175), mov = 14845907, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[50 sec, 15149176 itr]: obj = (-0.836175), mov = 15149176, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[51 sec, 15458568 itr]: obj = (-0.836175), mov = 15458568, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[52 sec, 15769915 itr]: obj = (-0.836175), mov = 15769915, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[53 sec, 16086855 itr]: obj = (-0.836175), mov = 16086855, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[54 sec, 16400924 itr]: obj = (-0.836175), mov = 16400924, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[55 sec, 16712779 itr]: obj = (-0.836175), mov = 16712779, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[56 sec, 17026215 itr]: obj = (-0.836175), mov = 17026215, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[57 sec, 17342378 itr]: obj = (-0.836175), mov = 17342378, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[58 sec, 17657687 itr]: obj = (-0.836175), mov = 17657687, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[59 sec, 17973001 itr]: obj = (-0.836175), mov = 17973001, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[60 sec, 18284320 itr]: obj = (-0.836175), mov = 18284320, inf = 9.1%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[61 sec, 18599281 itr]: obj = (-0.836175), mov = 18599281, inf = 9%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[62 sec, 18911340 itr]: obj = (-0.836175), mov = 18911340, inf = 9%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[63 sec, 19224127 itr]: obj = (-0.836175), mov = 19224127, inf = 9%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[64 sec, 19541323 itr]: obj = (-0.836175), mov = 19541323, inf = 9%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[65 sec, 19856193 itr]: obj = (-0.836175), mov = 19856193, inf = 9%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[66 sec, 20170312 itr]: obj = (-0.836175), mov = 20170312, inf = 9%, acc = 9.9%, imp = 1396

worst case avg CPI: 2.86694
[67 sec, 20479275 itr]: obj = (-0.836175), mov = 20479275, inf = 9%, acc = 9.9%, imp = 1841

worst case avg CPI: 2.86694
[68 sec, 20762225 itr]: obj = (-0.836172), mov = 20762225, inf = 9%, acc = 9.9%, imp = 1856

worst case avg CPI: 2.86699
[69 sec, 21076389 itr]: obj = (-0.836163), mov = 21076389, inf = 9%, acc = 9.9%, imp = 1860

worst case avg CPI: 2.86715
[70 sec, 21391874 itr]: obj = (-0.836163), mov = 21391874, inf = 9%, acc = 9.9%, imp = 1860

worst case avg CPI: 2.86715
[71 sec, 21706422 itr]: obj = (-0.836161), mov = 21706422, inf = 9%, acc = 9.9%, imp = 1861

worst case avg CPI: 2.86718
[72 sec, 22022043 itr]: obj = (-0.836161), mov = 22022043, inf = 9%, acc = 9.9%, imp = 1861

worst case avg CPI: 2.86718
[73 sec, 22339127 itr]: obj = (-0.836161), mov = 22339127, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[74 sec, 22656238 itr]: obj = (-0.836161), mov = 22656238, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[75 sec, 22972238 itr]: obj = (-0.836161), mov = 22972238, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[76 sec, 23289835 itr]: obj = (-0.836161), mov = 23289835, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[77 sec, 23606055 itr]: obj = (-0.836161), mov = 23606055, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[78 sec, 23921803 itr]: obj = (-0.836161), mov = 23921803, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[79 sec, 24236310 itr]: obj = (-0.836161), mov = 24236310, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[80 sec, 24549245 itr]: obj = (-0.836161), mov = 24549245, inf = 9%, acc = 9.9%, imp = 1862

worst case avg CPI: 2.86718
[81 sec, 24861501 itr]: obj = (-0.836161), mov = 24861501, inf = 9%, acc = 9.9%, imp = 2300

worst case avg CPI: 2.86718
[82 sec, 25176614 itr]: obj = (-0.836161), mov = 25176614, inf = 9%, acc = 9.9%, imp = 2318

worst case avg CPI: 2.86718
[83 sec, 25488897 itr]: obj = (-0.836161), mov = 25488897, inf = 9%, acc = 9.9%, imp = 2319

worst case avg CPI: 2.86718
[84 sec, 25805197 itr]: obj = (-0.836161), mov = 25805197, inf = 9%, acc = 9.9%, imp = 2322

worst case avg CPI: 2.86718
[85 sec, 26120380 itr]: obj = (-0.836161), mov = 26120380, inf = 9%, acc = 9.9%, imp = 2322

worst case avg CPI: 2.86718
[86 sec, 26436149 itr]: obj = (-0.836161), mov = 26436149, inf = 9%, acc = 9.9%, imp = 2323

worst case avg CPI: 2.86718
[87 sec, 26746685 itr]: obj = (-0.836161), mov = 26746685, inf = 9%, acc = 9.9%, imp = 2323

worst case avg CPI: 2.86718
[88 sec, 27059430 itr]: obj = (-0.836161), mov = 27059430, inf = 9%, acc = 9.9%, imp = 2323

worst case avg CPI: 2.86718
[89 sec, 27374163 itr]: obj = (-0.836161), mov = 27374163, inf = 9%, acc = 9.9%, imp = 2323

worst case avg CPI: 2.86718
[90 sec, 27687730 itr]: obj = (-0.836161), mov = 27687730, inf = 9%, acc = 9.9%, imp = 2323

worst case avg CPI: 2.86718
[90 sec, 27687730 itr]: obj = (-0.836161), mov = 27687730, inf = 9%, acc = 9.9%, imp = 2323

worst case avg CPI: 2.86718
27687730 iterations, 27687730 moves performed in 90 seconds
Feasible solution: obj = (-0.836161)

Run output...
FINAL SOLUTION
Worst case avg CPI: 2.86718
Mapping: 0->4, 1->16, 2->22, 3->31, 4->41, 5->44, 6->5, 7->44, 8->11, 9->22, 10->42, 11->39, 12->30, 13->16, 14->6, 15->31, 16->3, 17->33, 18->13, 19->42, 20->26, 21->40, 22->20, 23->28, 24->14, 25->10, 26->19, 27->0, 28->26, 29->33, 30->47, 31->22, 32->21, 33->11, 34->23, 35->37, 36->37, 37->10, 38->41, 39->2, 40->1, 41->27, 42->1, 43->47, 44->14, 45->17, 46->45, 47->19, 48->6, 49->39, 50->30, 51->47, 52->20, 53->19, 54->28, 55->2, 56->3, 57->5, 58->8, 59->36, 60->32, 61->40, 62->43, 63->1, 64->41, 65->25, 66->24, 67->24, 68->34, 69->14, 70->15, 71->29, 72->23, 73->3, 74->34, 75->38, 76->13, 77->46, 78->32, 79->44, 80->36, 81->8, 82->28, 83->43, 84->0, 85->11, 86->33, 87->26, 88->14, 89->25, 90->46, 91->17, 92->10, 93->4, 94->37, 95->38, 96->27, 97->2, 98->33, 99->40, 100->41, 101->44, 102->35, 103->35, 104->24, 105->37, 106->29, 107->44, 108->23, 109->8, 110->43, 111->26, 112->16, 113->42, 114->12, 115->9, 116->9, 117->1, 118->31, 119->47, 120->7, 121->32, 122->38, 123->36, 124->0, 125->35, 126->19, 127->13, 128->7, 129->39, 130->0, 131->4, 132->27, 133->40, 134->16, 135->10, 136->2, 137->30, 138->39, 139->28, 140->27, 141->42, 142->0, 143->23, 144->5, 145->28, 146->22, 147->34, 148->17, 149->13, 150->32, 151->4, 152->30, 153->43, 154->29, 155->4, 156->23, 157->39, 158->21, 159->37, 160->5, 161->2, 162->34, 163->45, 164->40, 165->11, 166->13, 167->3, 168->21, 169->45, 170->26, 171->17, 172->20, 173->6, 174->9, 175->15, 176->37, 177->29, 178->46, 179->47, 180->25, 181->15, 182->21, 183->42, 184->20, 185->42, 186->33, 187->9, 188->35, 189->12, 190->1, 191->32, 192->13, 193->19, 194->31, 195->12, 196->30, 197->35, 198->1, 199->7, 200->45, 201->27, 202->10, 203->40, 204->16, 205->7, 206->16, 207->38, 208->41, 209->31, 210->12, 211->24, 212->9, 213->46, 214->45, 215->8, 216->18, 217->25, 218->8, 219->15, 220->43, 221->18, 222->3, 223->6, 224->5, 225->9, 226->7, 227->8, 228->19, 229->47, 230->32, 231->14, 232->27, 233->24, 234->21, 235->2, 236->11, 237->18, 238->10, 239->5, 240->23, 241->18, 242->7, 243->30, 244->41, 245->33, 246->31, 247->46, 248->36, 249->36, 250->26, 251->24, 252->25, 253->39, 254->18, 255->29, 256->43, 257->35, 258->38, 259->21, 260->12, 261->17, 262->12, 263->36, 264->11, 265->45, 266->46, 267->25, 268->14, 269->17, 270->29, 271->20, 272->4, 273->22, 274->6, 275->34, 276->18, 277->38, 278->22, 279->0, 280->28, 281->15, 282->15, 283->6, 284->34, 285->3, 286->44, 287->20
Number of tasks per core:
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
	6	6	6	6	6	6
Average CPI map:
	1.00854	1.44219	1.21648	1.28038	1.08737	1.11773
	1.34276	2.86722	2.86745	2.86793	2.86768	0.76581
	1.14378	2.86722	2.86808	2.86719	2.86756	1.1796
	0.86387	2.86912	2.86746	2.86725	2.86741	0.8427
	1.29538	2.86745	2.86725	2.86741	2.86753	1.42729
	1.12317	2.86751	2.86736	2.86745	2.8672	1.36217
	1.04308	2.86752	2.86739	2.86718	2.86753	1.17117
	1.34025	1.17657	0.843829	0.831519	1.09315	1.18086
==============================================================================
Local Solver optimizer for EML demonstration
==============================================================================

Option values: --wld-idx 11 --global-tlim 90 --seed 101 --use-acpi-model

Starting the Optimization Process
